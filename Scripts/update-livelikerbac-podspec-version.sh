# updates the LiveLikeSwift.podspec version based on the version of the release branch `release/*.*`
VERSION=$(git describe --all --match "*.*" --match "release/*.*" --abbrev=0 | sed -E 's/.*\/(.*)/\1/g')
if [[ ! -z "$VERSION" ]] 
then
	plutil -replace CFBundleShortVersionString -string $VERSION Sources/EngagementSDK/Info.plist
    PODSPEC_FILE_PATH="LiveLikeRBAC.podspec"
    PODSPEC_VERSION_REPLACEMENT_REGEX="(spec\.version.*[\"'])[.[:digit:]]+"
    sed -E -i.bak -e "s/$PODSPEC_VERSION_REPLACEMENT_REGEX/\1$VERSION/" $PODSPEC_FILE_PATH
    rm $PODSPEC_FILE_PATH.bak
fi
