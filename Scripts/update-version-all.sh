# This is used to update the spec version on all podspecs

cd ..
./Scripts/update-livelikeversion-swift.sh
./Scripts/update-livelikecore-podspec-version.sh
./Scripts/update-livelikerbac-podspec-version.sh
./Scripts/update-framework-version.sh
./Scripts/update-livelikeswift-podspec-version.sh
