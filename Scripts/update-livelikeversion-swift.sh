# This will update the livelikeversion property of Version.swift

VERSION=$(git describe --all --match "*.*" --match "release/*.*" --abbrev=0 | sed -E 's/.*\/(.*)/\1/g')
if [[ ! -z "$VERSION" ]]
then
    echo "public let livelikeversion = \"$VERSION\"" > 'Sources/LiveLikeCore/Version.swift'
fi
