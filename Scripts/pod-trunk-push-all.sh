# This is used to push all podspecs
# Note: the order of the push matters (dependencies of a podspec should be pushed first)

pod trunk push LiveLikeCore.podspec --allow-warnings --synchronous --verbose
pod trunk push LiveLikeRBAC.podspec --allow-warnings --synchronous --verbose
pod trunk push LiveLikeSwift.podspec --allow-warnings --synchronous --verbose
pod trunk push EngagementSDK.podspec --allow-warnings --synchronous --verbose
