# This expression uses `git describe`, but it's prone to being screwed up by valid branch names like "fu.bar", should rethink and improve when able
VERSION=$(git describe --all --match "*.*" --match "release/*.*" --abbrev=0 | sed -E 's/.*\/(.*)/\1/g')
if [[ ! -z "$VERSION" ]] 
then
	plutil -replace CFBundleShortVersionString -string $VERSION Sources/EngagementSDK/Info.plist
    PODSPEC_FILE_PATH="EngagementSDK.podspec"
    PODSPEC_VERSION_REPLACEMENT_REGEX="(spec\.version.*[\"'])[.[:digit:]]+"
    sed -E -i.bak -e "s/$PODSPEC_VERSION_REPLACEMENT_REGEX/\1$VERSION/" $PODSPEC_FILE_PATH
    rm $PODSPEC_FILE_PATH.bak
fi
