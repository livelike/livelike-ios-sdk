# https://github.com/messeb/iOS-project-env-setup
# Checks if executable exists in current path
RUBY := $(shell command -v ruby 2>/dev/null)
HOMEBREW := $(shell command -v brew 2>/dev/null)
BUNDLER := $(shell command -v bundle 2>/dev/null)

# Default target, if no is provided
default: setup

# Steps for project environment setup
setup: \
	pre_setup \
	check_for_ruby \
	check_for_homebrew \
	update_homebrew \
	install_bundler_gem \
	install_ruby_gems \
	install_swiftlint \
	install_swiftformat

# Pre-setup steps
pre_setup:
	$(info iOS project setup ...)

# Check if Ruby is installed
check_for_ruby:
	$(info Checking for Ruby ...)

ifeq ($(RUBY),)
	$(error Ruby is not installed)
endif

# Check if Homebrew is available
check_for_homebrew:
	$(info Checking for Homebrew ...)

ifeq ($(HOMEBREW),)
	$(error Homebrew is not installed)
endif

# Install Bundler Gem
install_bundler_gem:
	$(info Checking and install bundler ...)

ifeq ($(BUNDLER),)
	gem install bundler -v '~> 1.17.3'
else
	bundle version
	gem install bundler --force
	# Bitrise does not like update. Must uninstall/install if you want to change version.
	# gem update bundler '~> 1.17.3'
endif

# Update Homebrew
update_homebrew:
	$(info Update Homebrew ...)
	brew update --verbose

install_swiftlint:
	$(info Install Swiftlint ... )
	brew unlink swiftlint || true
	brew install swiftlint
	brew link --overwrite swiftlint

install_swiftformat:
	$(info Install swiftformat ... )
	brew unlink swiftformat || true
	brew install swiftformat
	brew link --overwrite swiftformat

# Install Ruby Gems
install_ruby_gems:
	$(info Install RubyGems ...)
	bundle install --path vendor/bundle

