// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "EngagementSDK",
    defaultLocalization: "en",
    platforms: [
        .iOS(.v13),
        .macOS(.v10_12),
        .tvOS(.v10),
        .watchOS(.v3)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "EngagementSDK",
            targets: ["EngagementSDK"]
        ),
        .library(
            name: "LiveLikeSwift",
            targets: ["LiveLikeSwift"]
        ),
        .library(
            name: "LiveLikeRBAC",
            targets: ["LiveLikeRBAC"]
        ),
        .library(
            name: "LiveLikeWidgetPublish",
            targets: ["LiveLikeWidgetPublish"]
        )
    ],
    dependencies: [
        /// Need to stay above 7.2.0 to have Privacy Manifest
        .package(name: "PubNub", url: "https://github.com/pubnub/swift.git", .upToNextMajor(from: "7.2.0")),
        /// Need to stay above `4.4.0` to have a Privacy Manifest
        .package(name: "Lottie", url: "https://github.com/airbnb/lottie-ios.git", .upToNextMinor(from: "4.4.0"))
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "EngagementSDK",
            dependencies: ["Lottie", "LiveLikeSwift"],
            exclude: [
                "Info.plist"
            ],
            resources: [
                .process("Resources")
            ]
        ),
        .target(
            name: "LiveLikeCore",
            dependencies: []
        ),
        .target(
            name: "LiveLikeRBAC",
            dependencies: ["LiveLikeCore"]
        ),
        .target(
            name: "LiveLikeSwift",
            dependencies: ["LiveLikeCore", "PubNub"],
            resources: [
                .process("./PrivacyInfo.xcprivacy")
            ]
        ),
        .target(
            name: "LiveLikeWidgetPublish",
            dependencies: []
        ),
        .testTarget(
            name: "EngagementSDKTests",
            dependencies: ["EngagementSDK", "LiveLikeCore"],
            resources: [
                .process("Resources")
            ]
        ),
        .testTarget(
            name: "LiveLikeSwiftUnitTests",
            dependencies: ["LiveLikeSwift", "PubNub"]
        ),
        .testTarget(
            name: "LiveLikeSwiftEndToEndTests",
            dependencies: ["LiveLikeSwift", "LiveLikeWidgetPublish", "LiveLikeRBAC"],
            resources: [
                .copy("Resources/config.json")
            ]
        ),
    ]
)
