## Contributing

### Setup 

1. In the Terminal, go to the root directory of the project. Should contain the `makefile` and `Cartfile`.
2. Execute `make`. This will install all of the development dependencies.  [SwiftLint](https://github.com/realm/SwiftLint),  [SwiftFormat](https://github.com/nicklockwood/SwiftFormat), [XcodeGen](https://github.com/yonaskolb/XcodeGen)
3. Then execute `./carthage-build.sh update --platform iOS`. This will install the projects framework dependencies using [Carthage](https://github.com/Carthage/Carthage).
4. Open the `LiveLike.xcworkspace` in Xcode.
5. Select the `LiveLikeTestApp` target and Run the project.

### Project Structure

The project is comprised of a workspace containing two project files.

1. EngagementSDK - This contains the source for the EngagementSDK.framework. 
2. LiveLikeTestApp - This contains the source for the Test App which is used to functionally test the EngagementSDK.framework. 
