//
//  File.swift
//  
//
//  Created by Jelzon Monzon on 4/7/22.
//

@testable import EngagementSDK
@testable import LiveLikeSwift
import Foundation
import XCTest

class ProfileVendorTests: XCTestCase {

    func test_currentNickname_notNil_when_whenInitialNickname_fulfills() {
        let e = expectation(description: "Current Nickname not nil")
        let accessTokenVendor = MockAccessTokenVendor()
        let coreAPI = MockLiveLikeRestAPIServices()

        let sut = ProfileVendor(
            accessTokenVendor: accessTokenVendor,
            coreAPI: coreAPI
        )
        sut.whenInitialNickname.then {
            XCTAssertNotNil(sut.currentNickname)
            e.fulfill()
        }
        waitForExpectations(timeout: 1.0)
    }

}
