//
//  AlertWidgetViewModelTests.swift
//  EngagementSDKTests
//
//  Created by Jelzon Monzon on 7/2/20.
//

@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift
import XCTest

class AlertWidgetViewModelTests: XCTestCase {

    let alertWidgetResource = AlertCreated(
        baseData: WidgetMetadataResourceBuilder().build(),
        url: URL(string: "https://google.com")!,
        linkLabel: nil,
        linkURL: nil,
        text: nil,
        title: nil,
        imageURL: nil,
        rewardsURL: nil,
        sponsors: [MockObjects.sponsorResource]
    )

    /// Tests resource to model conversion
    func test_model_data_matches_alert_resource() {
        let model: AlertWidgetModel = {
            let builder = AlertModelBuilder()
            return builder.build(from: alertWidgetResource)
        }()

        XCTAssert(model.id == alertWidgetResource.id)
        XCTAssert(model.customData == alertWidgetResource.customData)
        XCTAssert(model.kind == alertWidgetResource.kind)
        XCTAssert(model.publishedAt == alertWidgetResource.publishedAt)
        XCTAssert(model.createdAt == alertWidgetResource.createdAt)
        XCTAssert(model.linkURL == alertWidgetResource.linkURL)
        XCTAssert(model.imageURL == alertWidgetResource.imageURL)
        XCTAssert(model.text == alertWidgetResource.text)
        XCTAssert(model.linkLabel == alertWidgetResource.linkLabel)

        model.sponsors.enumerated().forEach { index, sponsor in
            XCTAssert(sponsor.id == alertWidgetResource.sponsors[index].id)
            XCTAssert(sponsor.clientID == alertWidgetResource.sponsors[index].clientID)
            XCTAssert(sponsor.name == alertWidgetResource.sponsors[index].name)
            XCTAssert(sponsor.logoURL == alertWidgetResource.sponsors[index].logoURL)
        }
    }

    /// Tests alert model register impression request
    func test_alertModel_registerImpression_request() {
        let e = expectation(description: "registerImpression called on alert model")

        let livelikeAPI = MockLiveLikeRestAPIServices()
        livelikeAPI.createImpressionCompletion = { _, _, _ in
            e.fulfill()
            return Promise()
        }

        let resource = alertWidgetResource
        let model: AlertWidgetModel = {
            let builder = AlertModelBuilder()
            builder.livelikeAPI = livelikeAPI
            return builder.build(from: resource)
        }()

        model.registerImpression()

        waitForExpectations(timeout: 1.0, handler: nil)
    }
}

class AlertModelBuilder {

    var eventRecorder: EventRecorder = MockEventRecorder()
    var userProfile: UserProfileProtocol = MockUserProfile()
    var livelikeAPI: MockLiveLikeRestAPIServices = MockLiveLikeRestAPIServices()

    func build(from resource: AlertCreated) -> AlertWidgetModel {
        let model = AlertWidgetModel(
            data: resource,
            eventRecorder: eventRecorder,
            livelikeAPI: livelikeAPI,
            userProfile: userProfile
        )
        return model
    }
}
