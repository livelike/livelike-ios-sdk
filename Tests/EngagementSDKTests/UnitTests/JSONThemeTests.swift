//
//  JSONThemeTests.swift
//  EngagementSDKTests
//
//  Created by Mike Moloksher on 5/19/21.
//

@testable import EngagementSDK
import XCTest

class JSONThemeTests: XCTestCase {
    
    let jsonWidgetTitleFallbackFont = """
    {
        "widgets": {
            "alert": {
                "title": {
                    "fontFamily": [
                        "Verdsdfsdfana"
                    ],
                    "fontSize": 20,
                    "fontWeight": "bold"
                }
            }
        },
        "version": 1
    }
    """
    
    let jsonWidgetValidFontProperties = """
    {
        "widgets": {
            "alert": {
                "title": {
                    "fontFamily": [
                        "Verdana"
                    ],
                    "fontSize": 23,
                    "fontWeight": "bold"
                }
            }
        },
        "version": 1
    }
    """
    
    let jsonOptionStringFallBackFont = """
    {
        "widgets": {
            "poll": {
                "selectedOptionDescription": {
                    "fontFamily": [
                        "sans-serif"
                    ],
                    "fontSize": 16,
                    "fontWeight": "normal",
                },
            },
        },
        "version": 1
    }
    """
    
    let jsonOptionStringFont = """
    {
        "widgets": {
            "poll": {
                "selectedOptionDescription": {
                    "fontFamily": [
                        "Verdana"
                    ],
                    "fontSize": 16,
                    "fontWeight": "normal",
                },
            },
        },
        "version": 1
    }
    """
    
    // MARK: Widget Title Tests
    
    /// Tests JSON font conversion functionality to make sure when an invalid font is mentioned that
    /// we do not discard font size and weight
    func test_json_widget_title_font_fallback() {
        do {
            let jsonData = jsonWidgetTitleFallbackFont.data(using: .utf8)
            let jsonObject = try JSONSerialization.jsonObject(with: jsonData!, options: [])
            let theme = try Theme.create(fromJSONObject: jsonObject)
            
            let defaultTitleFont = UIFont.preferredFont(forTextStyle: .caption1).livelike_bold()
            let font = theme.widgets.alert.title.font

            XCTAssertTrue(font.familyName == defaultTitleFont.familyName, "Font Family does not match JSON")
            XCTAssertTrue(font.pointSize == 20.0, "Font Size does not match JSON")
            if let fontWeight = font.fontDescriptor.object(forKey: UIFontDescriptor.AttributeName.face) as? String {
                XCTAssertTrue(fontWeight == "Bold", "Font Weight does not match JSON")
            }
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
    /// Test to make sure a valid font is beig decoded correctly
    func test_json_widget_title_valid_font() {
        do {
            let jsonData = jsonWidgetValidFontProperties.data(using: .utf8)
            let jsonObject = try JSONSerialization.jsonObject(with: jsonData!, options: [])
            let theme = try Theme.create(fromJSONObject: jsonObject)
            
            let font = theme.widgets.alert.title.font
            
            XCTAssertTrue(font.familyName == "Verdana", "Font Family does not match JSON")
            XCTAssertTrue(font.pointSize == 23.0, "Font Size does not match JSON")
            if let fontWeight = font.fontDescriptor.object(forKey: UIFontDescriptor.AttributeName.face) as? String {
                XCTAssertTrue(fontWeight == "Regular", "Font Weight does not match JSON")
            }
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
    // MARK: Widget Options Tests
    
    /// Test to see that if font not found, we are using the default font instead
    func test_json_option_description_font_properties() {
        do {
            let jsonData = jsonOptionStringFont.data(using: .utf8)
            let jsonObject = try JSONSerialization.jsonObject(with: jsonData!, options: [])
            let theme = try Theme.create(fromJSONObject: jsonObject)
            
            let font = theme.widgets.poll.selectedOption.description.font
            
            XCTAssertTrue(font.fontName == "Verdana", "Font Family does not match JSON")
            XCTAssertTrue(font.pointSize == 16.0, "Font Size does not match JSON")
            if let fontWeight = font.fontDescriptor.object(forKey: UIFontDescriptor.AttributeName.face) as? String {
                XCTAssertTrue(fontWeight == "Regular", "Font Weight does not match JSON")
            }
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
    /// Test to see that if font not found, we are using the default font instead
    func test_json_option_description_fallback_font() {
        do {
            let jsonData = jsonOptionStringFallBackFont.data(using: .utf8)
            let jsonObject = try JSONSerialization.jsonObject(with: jsonData!, options: [])
            let theme = try Theme.create(fromJSONObject: jsonObject)
            
            let font = theme.widgets.poll.selectedOption.description.font
            let defaultFont = UIFont.preferredFont(forTextStyle: .subheadline)
            
            XCTAssertTrue(font.fontName == defaultFont.fontName, "Font Family does not match JSON")
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
}
