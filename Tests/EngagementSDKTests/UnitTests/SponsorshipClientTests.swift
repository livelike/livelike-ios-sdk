//
//  SponsorshipClientTests.swift
//  EngagementSDKTests
//
//  Created by Keval Shah on 14/01/22.
//

import XCTest
@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift

class SponsorshipClientTests: XCTestCase {
    
    let sponsorshipJSON = """
    {
        "id": "livelikeSponsorID",
        "url": "http://www.livelike.com",
        "clickthrough_url": "http://www.livelike.com",
        "logo_url": "https://www.google.com/logo.png",
        "client_id": "123456",
        "name": "Sponsor Name",
        "brand_color": null
    }
    """
    
    func test_model_data_matches_sponsorship_resource() {
        
        do {
            
            guard let sponsorData = sponsorshipJSON.data(using: String.Encoding.utf8) else {
                XCTFail("Failed to decode sponsorship json")
                return
            }

            let decoder = LLJSONDecoder()
            let sponsor = try decoder.decode(Sponsor.self, from: sponsorData)
            XCTAssert(sponsor.id == "livelikeSponsorID")
            XCTAssert(sponsor.logoURL.absoluteString == "https://www.google.com/logo.png")
            XCTAssert(sponsor.clickthroughURL?.absoluteString == "http://www.livelike.com")
            XCTAssert(sponsor.clientID == "123456")
            XCTAssert(sponsor.name == "Sponsor Name")
            XCTAssert(sponsor.brandColor == nil)
            
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
    func test_get_sponsor_for_program_success() {
        let e = expectation(description: "Successfully retrieved a sponsor for a programID")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getSponsorshipCompletion = .success(
            [Sponsor(id: "1", clientID: "12", name: "Pepsi Co", logoURL: MockObjects.mockURL),
             Sponsor(id: "2", clientID: "13", name: "Red Bull", logoURL: MockObjects.mockURL)]
        )
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockSponsorshipClient = MockSponsorshipClient(coreAPI: mockLLRestAPIServices)
            return builder.build()
        }()
        
        sdk.sponsorship.getBy(programID: "111") { result in
            switch result {
            case .success(let result):
                if let firstSponsor = result.first {
                    if firstSponsor.id == "1", firstSponsor.name == "Pepsi Co" {
                        e.fulfill()
                    }
                }
            case .failure:
                XCTFail("Failed retrieving a sponsor for a programID")
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_get_sponsor_for_program_fail() {
        let e = expectation(description: "Successfully failed retrieving a sponsor for a programID")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        
        mockLLRestAPIServices.getSponsorshipCompletion = .failure(SponsorError.getSponsorFailed(error: "Getting sponsors failed"))
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockSponsorshipClient = MockSponsorshipClient(coreAPI: mockLLRestAPIServices)
            return builder.build()
        }()
        
        sdk.sponsorship.getBy(programID: "111") { result in
            switch result {
            case .success:
                XCTFail("Retrieving a sponsor should fail here")
            case .failure:
                e.fulfill()
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_get_sponsor_for_application_success() {
        let e = expectation(description: "Successfully retrieved sponsors for applications")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getSponsorsListCompletion = .success(
            PaginatedResult(
                previous: nil,
                count: 2,
                next: nil,
                items: [Sponsor(id: "1", clientID: "12", name: "Pepsi Co", logoURL: MockObjects.mockURL),
                        Sponsor(id: "2", clientID: "13", name: "Red Bull", logoURL: MockObjects.mockURL)]
            )
        )
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockSponsorshipClient = MockSponsorshipClient(coreAPI: mockLLRestAPIServices)
            return builder.build()
        }()
        
        sdk.sponsorship.getByApplication(page: .first) { result in
            switch result {
            case .success(let result):
                if let firstSponsor = result.items.first {
                    if firstSponsor.id == "1", firstSponsor.name == "Pepsi Co" {
                        e.fulfill()
                    }
                }
            case .failure:
                XCTFail("Failed retrieving sponsors for application")
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_get_sponsor_for_application_fail() {
        let e = expectation(description: "Successfully failed to retriev sponsors for applications")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        
        mockLLRestAPIServices.getSponsorsListCompletion = .failure(SponsorError.getSponsorFailed(error: "Failure to get sponsors"))
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockSponsorshipClient = MockSponsorshipClient(coreAPI: mockLLRestAPIServices)
            return builder.build()
        }()
        
        sdk.sponsorship.getByApplication(page: .first) { result in
            switch result {
            case .success:
                XCTFail("Retrieving a sponsor should fail here")
            case .failure:
                e.fulfill()
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_get_sponsor_for_chatroom_success() {
        let e = expectation(description: "Successfully retrieved sponsors for chatroom")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getSponsorsListCompletion = .success(
            PaginatedResult(
                previous: nil,
                count: 2,
                next: nil,
                items: [Sponsor(id: "1", clientID: "12", name: "Pepsi Co", logoURL: MockObjects.mockURL),
                        Sponsor(id: "2", clientID: "13", name: "Red Bull", logoURL: MockObjects.mockURL)]
            )
        )
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockSponsorshipClient = MockSponsorshipClient(coreAPI: mockLLRestAPIServices)
            return builder.build()
        }()
        
        sdk.sponsorship.getBy(chatRoomID: "111", page: .first) { result in
            switch result {
            case .success(let result):
                if let firstSponsor = result.items.first {
                    if firstSponsor.id == "1", firstSponsor.name == "Pepsi Co" {
                        e.fulfill()
                    }
                }
            case .failure:
                XCTFail("Failed retrieving a sponsor for chatroom")
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_get_sponsor_for_chatroom_fail() {
        let e = expectation(description: "Successfully failed retrieving a sponsor for Chatroom")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        
        mockLLRestAPIServices.getSponsorsListCompletion = .failure(SponsorError.getSponsorFailed(error: "Failure to get sponsors"))
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockSponsorshipClient = MockSponsorshipClient(coreAPI: mockLLRestAPIServices)
            return builder.build()
        }()
        
        sdk.sponsorship.getBy(chatRoomID: "111", page: .first) { result in
            switch result {
            case .success:
                XCTFail("Retrieving a sponsor should fail here")
            case .failure:
                e.fulfill()
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }

}
