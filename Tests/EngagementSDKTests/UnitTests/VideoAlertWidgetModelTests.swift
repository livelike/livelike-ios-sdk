//
//  VideoAlertWidgetModelTests.swift
//  EngagementSDKTests
//
//  Created by Mike Moloksher on 8/11/21.
//

@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift
import XCTest

class VideoAlertWidgetModelTests: XCTestCase {

    var resource: VideoWidgetResource?

    override func setUpWithError() throws {
        do {
            let resource: VideoWidgetResource = try Helper.jsonDecode(filename: "VideoAlertWidgetResourceSample")
            self.resource = resource
        } catch {
            XCTFail("\(error)")
        }
    }

    /// Tests conversion of a resource into a model
    func test_model_data_matches_video_alert_resource() {
        guard let resource = resource else {
            XCTFail("Resource Missing")
            return
        }

        let model: VideoAlertWidgetModel = {
            let builder = VideoAlertWidgetBuilder()
            return builder.build(from: resource)
        }()

        // meta data
        XCTAssert(model.id == resource.id)
        XCTAssert(model.customData == resource.customData)
        XCTAssert(model.kind == resource.kind)
        XCTAssert(model.publishedAt == resource.publishedAt)
        XCTAssert(model.createdAt == resource.createdAt)
        XCTAssert(model.interactionTimeInterval == resource.timeout)

        // data
        XCTAssert(model.title == resource.title)
        XCTAssert(model.text == resource.text)
        XCTAssert(model.linkText == resource.linkLabel)
        XCTAssert(model.linkURL == resource.linkURL)
        XCTAssert(model.videoURL == resource.videoURL)

        model.sponsors.enumerated().forEach { index, sponsor in
            XCTAssert(sponsor.id == resource.sponsors[index].id)
            XCTAssert(sponsor.clientID == resource.sponsors[index].clientID)
            XCTAssert(sponsor.name == resource.sponsors[index].name)
            XCTAssert(sponsor.logoURL == resource.sponsors[index].logoURL)
        }
    }

    func test_videoAlert_registerImpression_makes_livelikeAPI_request() {
        guard let resource = resource else {
            XCTFail("Resource Missing")
            return
        }

        let e = expectation(description: "createImpression called")

        let livelikeAPI = MockLiveLikeRestAPIServices()
        livelikeAPI.createImpressionCompletion = { _, _, _ in
            e.fulfill()
            return Promise()
        }

        let model: VideoAlertWidgetModel = {
            let builder = VideoAlertWidgetBuilder()
            builder.livelikeAPI = livelikeAPI
            return builder.build(from: resource)
        }()

        model.registerImpression()

        waitForExpectations(timeout: 1.0, handler: nil)
    }
}

class VideoAlertWidgetBuilder {

    var eventRecorder: EventRecorder = MockEventRecorder()
    var widgetClient: PubSubWidgetClient = MockWidgetClient()
    var userProfile: UserProfileProtocol = MockUserProfile()
    var leaderboardsManager: LeaderboardsManager = LeaderboardsManager()
    var livelikeAPI: MockLiveLikeRestAPIServices = MockLiveLikeRestAPIServices()
    var widgetInteractionRepo: WidgetInteractionRepository = MockWidgetInteractionRepository()

    func build(from resource: VideoWidgetResource) -> VideoAlertWidgetModel {
        let model = VideoAlertWidgetModel(
            resource: resource,
            eventRecorder: eventRecorder,
            widgetAPI: livelikeAPI,
            userProfile: userProfile
        )
        return model
    }
}
