//
//  BadgeClientTests.swift
//  EngagementSDKTests
//
//  Created by Mike Moloksher on 7/13/21.
//

import XCTest
@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift

class BadgeClientTests: XCTestCase {

    let profileBadgeResources: [ProfileBadge] = [
        ProfileBadge(awardedAt: Date(), badge: Badge(id: "1", name: "Badge 1", description: "Description 1", badgeIconURL: MockObjects.mockURL, registeredLinks: [MockObjects.mockRegisteredLink])),
        ProfileBadge(awardedAt: Date(), badge: Badge(id: "2", name: "Badge 2", description: nil, badgeIconURL: MockObjects.mockURL, registeredLinks: [MockObjects.mockRegisteredLink])),
        ProfileBadge(awardedAt: Date(), badge: Badge(id: "3", name: "Badge 3", description: "Description 3", badgeIconURL: MockObjects.mockURL, registeredLinks: [MockObjects.mockRegisteredLink]))
    ]

    let applicationBadgeResources: [Badge] = [
        Badge(id: "1", name: "Badge 1", description: "Description 1", badgeIconURL: MockObjects.mockURL, registeredLinks: [MockObjects.mockRegisteredLink]),
        Badge(id: "2", name: "Badge 2", description: nil, badgeIconURL: MockObjects.mockURL, registeredLinks: [MockObjects.mockRegisteredLink]),
        Badge(id: "3", name: "Badge 3", description: "Description 3", badgeIconURL: MockObjects.mockURL, registeredLinks: [MockObjects.mockRegisteredLink])
    ]

    let badgeProfilesResource: [BadgeProfile] = [
        BadgeProfile(awardedAt: Date(), badgeID: "1", profile: MockUserProfile.mockUserProfileResource()),
        BadgeProfile(awardedAt: Date(), badgeID: "1", profile: MockUserProfile.mockUserProfileResource(id: "2", nickname: "User 2")),
        BadgeProfile(awardedAt: Date(), badgeID: "1", profile: MockUserProfile.mockUserProfileResource(id: "3", nickname: "User 3")),
        BadgeProfile(awardedAt: Date(), badgeID: "1", profile: MockUserProfile.mockUserProfileResource(id: "4", nickname: "User 4"))
    ]

    /// Test for successful parsing of the `sdk.badges.getBy(profileID: String)`
    func test_get_profile_badges_on_sdk_success() {
        let e = expectation(description: "Successfully retrieved a profile badges the SDK level")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getProfileBadgesPromise = Promise(
            value: PaginatedResult<ProfileBadge>(
                previous: nil,
                count: profileBadgeResources.count,
                next: nil,
                items: profileBadgeResources
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockBadgeClient = InternalBadgesClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                badgesAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()

        sdk.badges.getProfileBadgesBy(profileID: "123", page: .first) { result in
            switch result {
            case .success(let result):
                let secondBadge = result.items[1]
                if secondBadge.badge.id == "2", secondBadge.badge.name == "Badge 2" {
                    e.fulfill()
                }
            case .failure(let error):
                XCTFail("Failed retrieving a profile badge for profile ID the SDK level - \(error)")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    /// Test for successful failure while parsing of the `sdk.badges.getBy(profileID: String)`
    func test_get_profile_badges_on_sdk_failure() {
        let e = expectation(description: "Successfully failed retrieving badges by profile")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getProfileBadgesPromise = Promise(error: BadgeError.invalidUserProfileDetailURL)

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockBadgeClient = InternalBadgesClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                badgesAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()

        sdk.badges.getProfileBadgesBy(profileID: "123", page: .first) { result in
            switch result {
            case .success:
                XCTFail("Failed failing retrieving a profile badge for profile ID the SDK level")
            case .failure:
                e.fulfill()
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    /// Test empty result set returns an empty array in `sdk.badges.getBy(profileID: String)`
    func test_get_profile_badges_on_sdk_returns_empty_array_on_no_badges() {
        let e = expectation(description: "Successfully returned an empty array when there are no results in a page")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getProfileBadgesPromise = Promise(
            value: PaginatedResult<ProfileBadge>(
                previous: nil,
                count: 0,
                next: nil,
                items: []
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockBadgeClient = InternalBadgesClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                badgesAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()

        sdk.badges.getProfileBadgesBy(profileID: "123", page: .first) { result in
            switch result {
            case .success(let result):
                if result.count == 0, result.items.count == 0 {
                    e.fulfill()
                } else {
                    XCTFail("Should return an empty array if there are no badges")
                }
            case .failure:
                XCTFail("Should return an empty array if there are no badges")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    /// Tests whether the `hasNext` & `hasPrevious` is correctly assigned
    func test_get_profile_badges_on_sdk_next_previous_page() {
        let e = expectation(description: "Successfully returned `nil` when there are no results in a page")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getProfileBadgesPromise = Promise(
            value: PaginatedResult<ProfileBadge>(
                previous: MockObjects.mockURL,
                count: 0,
                next: MockObjects.mockURL,
                items: []
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockBadgeClient = InternalBadgesClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                badgesAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()

        sdk.badges.getProfileBadgesBy(profileID: "123", page: .first) { result in
            switch result {
            case .success(let result):
                if result.hasNext, result.hasPrevious {
                    e.fulfill()
                } else {
                    XCTFail("Both previous and next urls should be present")
                }
            case .failure:
                XCTFail("Both previous and next urls should be present")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    /// Tests that passing `.next` pagination on a function that never called `.first` returns an error
    func test_get_profile_badges_on_sdk_next_next_page_before_first() {
        let e = expectation(description: "Successfully returned an error when calling `.next` prior to calling `.first`")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getProfileBadgesPromise = Promise(
            value: PaginatedResult<ProfileBadge>(
                previous: nil,
                count: 0,
                next: nil,
                items: []
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockBadgeClient = InternalBadgesClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                badgesAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()

        sdk.badges.getProfileBadgesBy(profileID: "123", page: .next) { result in
            switch result {
            case .success:
                XCTFail("Calling `.next` page before first calling `.first` should come back as error")
            case .failure(let error):
                let nextPageError = BadgeError.getBadgesByProfileFailure(
                    error: PaginationErrors.nextPageUnavailable.localizedDescription
                ).localizedDescription
                if error.localizedDescription == nextPageError {
                    e.fulfill()
                }

            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    /// Tests that we show an error if the backend passes an invalid `profileDetailURL`
    func test_get_profile_badges_on_sdk_with_invalid_profile_detail_url() {
        let e = expectation(description: "Successfully returned an error when the backend returned an invalid `profileDetailURL`")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getProfileBadgesPromise = Promise(
            value: PaginatedResult<ProfileBadge>(
                previous: nil,
                count: 0,
                next: nil,
                items: []
            )
        )

        mockLLRestAPIServices.whenApplicationConfig = {
            let configuration = ApplicationConfiguration(
                name: "test",
                clientId: "client-id",
                pubnubPublishKey: nil,
                pubnubSubscribeKey: "",
                sessionsUrl: URL(string: "https://livelike.com/")!,
                profileUrl: URL(string: "https://livelike.com/")!,
                profileDetailUrlTemplate: "Hello bad url",
                stickerPacksUrl: URL(string: "https://livelike.com/")!,
                programsUrl: URL(string: "https://livelike.com/")!,
                programDetailUrlTemplate: "",
                chatRoomDetailUrlTemplate: "",
                mixpanelToken: nil,
                analyticsProperties: [:],
                pubnubOrigin: nil,
                organizationId: "",
                organizationName: "",
                createChatRoomUrl: "",
                widgetDetailUrlTemplate: "",
                leaderboardDetailUrlTemplate: "",
                pubnubHeartbeatInterval: 0,
                pubnubPresenceTimeout: 0,
                badgesUrl: MockObjects.mockURL,
                rewardItemsUrl: MockObjects.mockURL,
                chatRoomsInvitationsUrl: URL(string: "https://livelike.com/")!,
                chatRoomInvitationDetailUrlTemplate: "Hello Bad Url",
                createChatRoomInvitationUrl: URL(string: "https://livelike.com/")!,
                profileChatRoomReceivedInvitationsUrlTemplate: "Hello Bad Url",
                profileChatRoomSentInvitationsUrlTemplate: "Hello Bad Url",
                profileSearchUrl: "Hello Bad Url",
                rewardTransactionsUrl: MockObjects.mockURL,
                pinnedMessagesUrl: URL(string: "https://livelike.com/")!,
                sponsorsUrl: MockObjects.mockURL,
                redemptionKeysUrl: MockObjects.mockURL,
                redemptionKeyDetailByCodeUrlTemplate: MockObjects.mockString,
                redemptionKeyDetailUrlTemplate: MockObjects.mockString,
                questsUrl: MockObjects.mockURL,
                userQuestsUrlTemplate: MockObjects.mockString,
                userQuestDetailUrlTemplate: MockObjects.mockString,
                userQuestsUrl: MockObjects.mockURL,
                userQuestTaskProgressUrl: MockObjects.mockURL,
                userQuestRewardsUrl: MockObjects.mockURL,
                reactionSpaceDetailUrlTemplate: MockObjects.mockString,
                reactionSpacesUrl: MockObjects.mockURL,
                reactionPackDetailUrlTemplate: MockObjects.mockString,
                reactionPacksUrl: MockObjects.mockURL,
                invokedActionsUrl: MockObjects.mockURL,
                rewardActionsUrl: MockObjects.mockURL,
                profileLeaderboardsUrlTemplate: MockObjects.mockString,
                profileLeaderboardViewsUrlTemplate: MockObjects.mockString,
                chatRoomEventsUrl: MockObjects.mockURL,
                apiPollingInterval: 10,
                programCustomIdUrlTemplate: MockObjects.mockString,
                badgeProfilesUrl: MockObjects.mockURL,
                commentBoardsUrl: MockObjects.mockURL,
                commentBoardDetailUrlTemplate: MockObjects.mockString,
                profileRelationshipsUrl: MockObjects.mockURL,
                profileRelationshipTypesUrl: MockObjects.mockURL,
                profileRelationshipDetailUrlTemplate: MockObjects.mockString,
                commentBoardBanUrl: MockObjects.mockURL,
                commentBoardBanDetailUrlTemplate: MockObjects.mockString,
                commentReportUrl: MockObjects.mockURL,
                commentReportDetailUrlTemplate: MockObjects.mockString,
                autoclaimInteractionRewards: false,
                chatRoomMembershipsUrl: MockObjects.mockURL,
                savedContractAddressesUrl: MockObjects.mockURL,
                textPollsUrl: MockObjects.mockURL,
                imagePollsUrl: MockObjects.mockURL,
                textPredictionsUrl: MockObjects.mockURL,
                imagePredictionsUrl: MockObjects.mockURL,
                alertsUrl: MockObjects.mockURL,
                textAsksUrl: MockObjects.mockURL,
                textQuizzesUrl: MockObjects.mockURL,
                imageQuizzesUrl: MockObjects.mockURL,
                richPostsUrl: MockObjects.mockURL,
                imageNumberPredictionsUrl: MockObjects.mockURL
            )
            let promise = Promise<ApplicationConfiguration>(value: configuration)
            return promise
        }()

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockBadgeClient = InternalBadgesClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                badgesAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()

        sdk.badges.getProfileBadgesBy(profileID: "123", page: .first) { result in
            switch result {
            case .success:
                XCTFail("The backend has an invalid `profileDetailURL`, this shold fail")
            case .failure(let error):
                let badProfileDetailURLTemplate = BadgeError.getBadgesByProfileFailure(
                    error: BadgeError.invalidUserProfileDetailURL.localizedDescription
                ).localizedDescription
                if error.localizedDescription == badProfileDetailURLTemplate {
                    e.fulfill()
                }

            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    // MARK: - Application Badges

    /// Test for successful parsing of the `sdk.badges.getApplicationBadges()`
    func test_get_application_badges_on_sdk_success() {
        let e = expectation(description: "Successfully retrieved application badges on the SDK level")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getApplicationBadgesPromise = Promise(
            value: PaginatedResult<Badge>(
                previous: nil,
                count: applicationBadgeResources.count,
                next: nil,
                items: applicationBadgeResources
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockBadgeClient = InternalBadgesClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                badgesAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()

        sdk.badges.getApplicationBadges(page: .first) { result in
            switch result {
            case .success(let result):
                let secondBadge = result.items[1]
                if secondBadge.id == "2", secondBadge.name == "Badge 2" {
                    e.fulfill()
                }
            case .failure(let error):
                XCTFail("Failed retrieving a application badges on the SDK level - \(error)")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    /// Test for successful failure while parsing of the `sdk.badges.getApplicationBadges()`
    func test_get_application_badges_on_sdk_failure() {
        let e = expectation(description: "Successfully failed retrieving application badges")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getApplicationBadgesPromise = Promise(error: BadgeError.getApplicationBadgesFailure(error: "error"))

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockBadgeClient = InternalBadgesClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                badgesAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()

        sdk.badges.getApplicationBadges(page: .first) { result in
            switch result {
            case .success:
                XCTFail("Failed failing retrieving application badges on the SDK level")
            case .failure:
                e.fulfill()
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    /// Test empty result set returns an empty array in `sdk.badges.getApplicationBadges()`
    func test_get_application_badges_on_sdk_returns_empty_array_on_no_badges() {
        let e = expectation(description: "Successfully returned an empty array when there are no results in a page")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getApplicationBadgesPromise = Promise(
            value: PaginatedResult<Badge>(
                previous: nil,
                count: 0,
                next: nil,
                items: []
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockBadgeClient = InternalBadgesClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                badgesAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()

        sdk.badges.getApplicationBadges(page: .first) { result in
            switch result {
            case .success(let result):
                if result.count == 0, result.items.count == 0 {
                    e.fulfill()
                } else {
                    XCTFail("Should return an empty array if there are no badges")
                }
            case .failure:
                XCTFail("Should return an empty array if there are no badges")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    /// Tests whether the `hasNext` & `hasPrevious` is correctly assigned for `sdk.badges.getApplicationBadges()`
    func test_get_application_badges_on_sdk_next_previous_page() {
        let e = expectation(description: "Successfully returned `nil` when there are no results in a page")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getApplicationBadgesPromise = Promise(
            value: PaginatedResult<Badge>(
                previous: MockObjects.mockURL,
                count: 0,
                next: MockObjects.mockURL,
                items: []
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockBadgeClient = InternalBadgesClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                badgesAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()

        sdk.badges.getApplicationBadges(page: .first) { result in
            switch result {
            case .success(let result):
                if result.hasNext, result.hasPrevious {
                    e.fulfill()
                } else {
                    XCTFail("Both previous and next urls should be present")
                }
            case .failure:
                XCTFail("Both previous and next urls should be present")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    // MARK: Profiles for Badge

    /// Test for successful parsing of the `sdk.badges.getBy(profileID: String)`
    func test_get_badge_profiles_on_sdk_success() {
        let e = expectation(description: "Successfully retrieved badge profiles on the SDK level")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getBadgeProfilesPromise = Promise(
            value: PaginatedResult<BadgeProfile>(
                previous: nil,
                count: badgeProfilesResource.count,
                next: nil,
                items: badgeProfilesResource
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockBadgeClient = InternalBadgesClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                badgesAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()

        sdk.badges.getBadgeProfiles(badgeID: "1", page: .first) { result in
            switch result {
            case .success(let result):
                let secondBadge = result.items[1]
                if secondBadge.profile.id == "2", secondBadge.profile.nickname == "User 2" {
                    e.fulfill()
                }
            case .failure(let error):
                XCTFail("Failed retrieving a badge profile for badge ID the SDK level - \(error)")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    /// Test for successful failure while parsing of the `sdk.badges.getBy(profileID: String)`
    func test_get_badge_profiles_on_sdk_failure() {
        let e = expectation(description: "Successfully failed retrieving profiles by badge")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getBadgeProfilesPromise = Promise(error: BadgeError.getBadgeProfilesFailure(error: "Invalid URL"))

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockBadgeClient = InternalBadgesClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                badgesAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()

        sdk.badges.getBadgeProfiles(badgeID: "1", page: .first) { result in
            switch result {
            case .success:
                XCTFail("Failed failing retrieving a badge profile for badge ID at the SDK level")
            case .failure:
                e.fulfill()
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }
}
