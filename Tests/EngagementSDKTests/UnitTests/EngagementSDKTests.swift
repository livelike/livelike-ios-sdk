//
//  EngagementSDKTests.swift
//  EngagementSDKTests
//
//  Created by Xavi Matos on 7/26/19.
//

@testable import EngagementSDK
@testable import LiveLikeSwift
@testable import LiveLikeCore
import XCTest

class EngagementSDKTests: XCTestCase {
    /// valid JSON taken from server on 4/18/2020
    let validWidgetJson = """
    {
       "client_id":"ex-client-id-1",
       "id":"bfbf9cc2-7cd6-467b-8391-a8d0ab35cc64",
       "url":"https://cf-blast.livelikecdn.com/api/v1/text-polls/bfbf9cc2-7cd6-467b-8391-a8d0ab35cc64/",
       "kind":"text-poll",
       "program_id":"8f34411b-159c-4840-b8ca-794785dfc1ca",
       "created_at":"2019-05-23T12:00:28.359868Z",
       "published_at":"2019-05-23T12:00:42.762025Z",
       "scheduled_at":null,
       "question":"sd",
       "timeout":"P0DT00H00M07S",
       "options":[
          {
             "id":"6ae8019b-8552-438a-9bd2-86e53bde76d6",
             "description":"s",
             "vote_count":0,
             "vote_url":"https://vc3e1qa5ef.execute-api.us-east-1.amazonaws.com/services/votes/text-poll/bfbf9cc2-7cd6-467b-8391-a8d0ab35cc64/6ae8019b-8552-438a-9bd2-86e53bde76d6",
             "translations":{

             },
             "translatable_fields":[
                "description"
             ]
          },
          {
             "id":"08962181-2129-4cd7-b0f6-33c775c7d4a5",
             "description":"d",
             "vote_count":1,
             "vote_url":"https://vc3e1qa5ef.execute-api.us-east-1.amazonaws.com/services/votes/text-poll/bfbf9cc2-7cd6-467b-8391-a8d0ab35cc64/08962181-2129-4cd7-b0f6-33c775c7d4a5",
             "translations":{

             },
             "translatable_fields":[
                "description"
             ]
          }
       ],
       "subscribe_channel":"text_poll_bfbf9cc2_7cd6_467b_8391_a8d0ab35cc64",
       "program_date_time":"2019-05-23T11:59:55.119000Z",
       "publish_delay":"P0DT00H00M00S",
       "widget_interactions_url_template":"https://cf-blast.livelikecdn.com/api/v1/text-polls/bfbf9cc2-7cd6-467b-8391-a8d0ab35cc64/interactions/",
       "impression_url":"https://blast-collector-prod-us-east-2.livelikecdn.com/services/impressions/text-poll/bfbf9cc2-7cd6-467b-8391-a8d0ab35cc64",
       "impression_count":0,
       "unique_impression_count":0,
       "engagement_count":1,
       "engagement_percent":"0.000",
       "status":"published",
       "created_by":{
          "id":"1bf5b2ff-d82b-4400-b4eb-38a8d7bf538c",
          "name":"Ben Wilber",
          "image_url":"https://cf-blast-storage.livelikecdn.com/assets/b8d6c072-902d-4616-9575-b596817029ac.png"
       },
       "schedule_url":"https://cf-blast.livelikecdn.com/api/v1/text-polls/bfbf9cc2-7cd6-467b-8391-a8d0ab35cc64/schedule/",
       "rewards_url":null,
       "translations":{

       },
       "translatable_fields":[
          "question"
       ],
       "reactions":[

       ],
       "custom_data":null,
       "sponsors": [
        {
          "id": "08e0ace4-859d-4e8a-ba87-944111df95b4",
          "logo_url": "/media/assets/8d234c60-1243-4182-9ea4-bb1d2f574373.png",
          "client_id": "IOWGz4LAfkVNfdLUS6c0B1iFewR2BFrm6HgEOchI",
          "name": "Pepsi"
        }],
        "widgetAttributes": []
    }
    """

    override func setUp() {}

    override func tearDown() {}

    func testBadClientIDSetupError() {
        let e = expectation(description: "Received Error through delegate")
        let badClientID = "sillyness"
        let sdk = EngagementSDK(config: EngagementSDKConfig(clientID: badClientID))
        let sdkDelegate = MockEngagementSDKDelegate()
        sdkDelegate.setupFailedWithErrorCompletion = { _, error in
            let setupError = error as? EngagementSDK.SetupError
            XCTAssertNotNil(setupError, "Wrong error type received, expected EngagementSDK.SetupError")
            XCTAssertEqual(setupError, EngagementSDK.SetupError.invalidClientID(badClientID))
            e.fulfill()
        }
        sdk.delegate = sdkDelegate
        waitForExpectations(timeout: 20, handler: nil)
    }

    func test_error_raised_with_forbidden_profile_response() {
        let e = expectation(description: "The error is bubbled up from server response.")

        let accessTokenVendor = MockAccessTokenVendor()
        accessTokenVendor.whenAccessToken = Promise(error: NetworkClientError.forbidden())

        let sdkBuilder = EngagementSDKBuilder()
        sdkBuilder.accessTokenVendor = accessTokenVendor
        let sdk = sdkBuilder.build()

        let sdkDelegate = MockEngagementSDKDelegate()
        sdkDelegate.setupFailedWithErrorCompletion = { sdk, error in
            switch error {
            case NetworkClientError.forbidden:
                e.fulfill()
            default:
                break
            }
        }
        sdk.delegate = sdkDelegate

        let session = sdk.contentSession(config: SessionConfiguration(programID: ""))
        let chat = ChatViewController()
        chat.session = session

        waitForExpectations(timeout: 5, handler: nil)
    }

    // a token is malformed if not in UUID format
    func test_error_raised_with_unauthorized_profile_response() {
        let e = expectation(description: "The error is bubbled up from server response.")

        let accessTokenVendor = MockAccessTokenVendor()
        accessTokenVendor.whenAccessToken = Promise(error: NetworkClientError.unauthorized())

        let sdkBuilder = EngagementSDKBuilder()
        sdkBuilder.accessTokenVendor = accessTokenVendor
        let sdk = sdkBuilder.build()

        let sdkDelegate = MockEngagementSDKDelegate()
        sdkDelegate.setupFailedWithErrorCompletion = { sdk, error in
            switch error {
            case NetworkClientError.unauthorized:
                e.fulfill()
            default:
                break
            }
        }
        sdk.delegate = sdkDelegate

        let session = sdk.contentSession(config: SessionConfiguration(programID: ""))
        let chat = ChatViewController()
        chat.session = session

        waitForExpectations(timeout: 5, handler: nil)
    }

    func test_sdk_setup_successful_with_forbidden_profile_reponse() {
        let successExpectation = expectation(description: "calls when sdk initialized and ready")
        let mockAPI = MockLiveLikeRestAPIServices()
        var count = 0
        mockAPI.getProfileCompletion = {
            count += 1
            if count == 1 {
                return Promise(error: NetworkClientError.forbidden())
            } else {
                return Promise(value: MockLiveLikeRestAPIServices.profileResource)
            }
        }
        let sdkBuilder = EngagementSDKBuilder()
        sdkBuilder.mockLLRestAPIServices = mockAPI
        let sdk = sdkBuilder.build()
        let session = sdk.contentSession(config: SessionConfiguration(programID: ""))
        let chat = ChatViewController()
        chat.session = session
        sdk.whenInitializedAndReady.then {
            successExpectation.fulfill()
        }.catch {
            print($0)
        }

        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("Wait for expectation timeout error: \(error)")
                return
            }
        }
    }

    func test_sdk_setup_successful_with_unauthorized_profile_reponse() {
        let successExpectation = expectation(description: "calls when sdk initialized and ready")
        
        let sdkBuilder = EngagementSDKBuilder()
        let mockAPI = MockLiveLikeRestAPIServices()
        var count = 0
        mockAPI.getProfileCompletion = {
            count += 1
            if count == 1 {
                return Promise(error: NetworkClientError.unauthorized())
            } else {
                return Promise(value: MockLiveLikeRestAPIServices.profileResource)
            }
        }
        let sdk = sdkBuilder.build()
        let session = sdk.contentSession(config: SessionConfiguration(programID: ""))
        let chat = ChatViewController()
        chat.session = session
        sdk.whenInitializedAndReady.then {
            successExpectation.fulfill()
        }.catch { error in
            XCTFail(String(describing: error))
        }

        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("Wait for expectation timeout error: \(error)")
                return
            }
        }
    }

    func test_invalid_custom_origin_error() {
        let e = expectation(description: "Invalid custom origin error sucessfully raised.")
        let sdkDelegate = MockEngagementSDKDelegate()
        sdkDelegate.setupFailedWithErrorCompletion = { sdk, error in
            switch error {
            case EngagementSDK.SetupError.invalidAPIOrigin:
                e.fulfill()
            default:
                break
            }
        }
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.whenApplicationConfig = Promise(error: NetworkClientError.badRequest())
        
        let sdkBuilder = EngagementSDKBuilder()
        sdkBuilder.mockLLRestAPIServices = mockLLRestAPIServices
        
        sdkBuilder.config = {
            var config = EngagementSDKConfig(clientID: "")
            config.apiOrigin = URL(string: "https://google.com")! // invalid origin but valid url
            return config
        }()
        
        let sdk = sdkBuilder.build()
        sdk.delegate = sdkDelegate
        
        waitForExpectations(timeout: 20, handler: nil)
    }
    
    func test_getting_chat_room_info_success() {
        let e = expectation(description: "Successfully received Chat Room Info")
        let roomID = "roomid_test"
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getChatRoomResourcePromise = Promise(value: ChatRoomResource(
            id: roomID,
            channels: ChatRoomResource.Channels(
                chat: ChatRoomResource.Channel(pubnub: ""),
                reactions: nil,
                control: nil
            ),
            uploadUrl: URL(string: "https://google.com")!,
            title: "Room Test",
            reportMessageUrl: URL(string: "https://livelike.com")!,
            stickerPacksUrl: URL(string: "https://google.com")!,
            reactionPacksUrl: URL(string: "https://google.com")!,
            membershipsUrl: URL(string: "https://google.com")!,
            mutedStatusUrlTemplate: "https://google.com",
            visibility: .everyone,
            customMessagesUrl: URL(string: "https://google.com")!,
            sponsorsUrl: MockObjects.mockURL,
            chatroomMessagesUrl: MockObjects.mockURL,
            chatroomMessagesCountUrl: MockObjects.mockURL,
            deleteMessageUrl: MockObjects.mockURL,
            tokenGates: [],
            reactionSpaceId: MockObjects.mockString,
            chatroomTokenGateAccessUrlTemplate: "https://google.com",
            chatroomAccessRuleUrl: MockObjects.mockURL
        ))
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()
        
        sdk.getChatRoomInfo(roomID: roomID) { result in
            switch result {
            case .success(let chatRoomResource):
                if chatRoomResource.id == roomID {
                    e.fulfill()
                }
            case .failure:
                XCTFail("Getting Chat Room Info Failed")
            }
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_getting_chat_room_info_fail() {
        let e = expectation(description: "Successfully failed to receive Chat Room Info")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getChatRoomResourcePromise = Promise(error: NetworkClientError.badRequest())
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()
        
        sdk.getChatRoomInfo(roomID: "123") { result in
            switch result {
            case .success:
                XCTFail("Testing getting chat room info failure... has failed ")
            case .failure:
                e.fulfill()
            }
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_creating_chat_room_success() {
        let e = expectation(description: "Successfully created a new Chat Room")
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            return builder.build()
        }()
        
        sdk.createChatRoom(title: "Test Room", visibility: .everyone) { result in
            switch result {
            case .success:
                e.fulfill()
            case .failure:
                XCTFail("Creating new Chat Room Failed")
            }
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_creating_chat_room_fail() {
        let e = expectation(description: "Successfully failed to create Chat Room")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.createChatRoomResourcePromise = Promise(error: NetworkClientError.badRequest())
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()
        
        sdk.createChatRoom(title: "Test Room", visibility: .everyone) { result in
            switch result {
            case .success:
                XCTFail("Testing creating chat room failure... has failed ")
            case .failure:
                e.fulfill()
            }
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_creating_token_gated_chat_room_success() {
        let e = expectation(description: "Successfully created a new Token Gated Chat Room")
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            return builder.build()
        }()
        
        sdk.createChatRoom(title: "Test Room", visibility: .members, tokenGates: [TokenGate(contractAddress: "0xe60ede23D15Cc5e1807DeC001DA44f9B24d1e458", networkType: .ethereum, tokenType: .nonfungible, attributes: [NFTAttribute(traitType: "trait1", value: "value1")])]) { result in
            switch result {
            case .success:
                e.fulfill()
            case .failure:
                XCTFail("Creating new Token Gated Chat Room Failed")
            }
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_creating_token_gated_chat_room_fail() {
        let e = expectation(description: "Successfully failed to create Token Gated Chat Room")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.createChatRoomResourcePromise = Promise(error: NetworkClientError.badRequest())
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()
        
        sdk.createChatRoom(title: "Test Room", visibility: .members, tokenGates: [TokenGate(contractAddress: "", networkType: .chiliz, tokenType: .nonfungible, attributes: [NFTAttribute(traitType: "trait1", value: "value1")])]) { result in
            switch result {
            case .success:
                XCTFail("Testing creating token gated chat room failure... has failed ")
            case .failure:
                e.fulfill()
            }
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_get_chat_room_membership_fail() {
        let e = expectation(description: "Successfully failed getting Chat Room Memberships")
        
        let networking = StubNetworking<ChatRoomMembership>(result: .failure(NetworkClientError.badRequest()))
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.networking = networking
            return builder.build()
        }()
        sdk.getChatRoomMemberships(
            roomID: "123",
            page: .first
        ) { result in
            switch result {
            case .success:
                XCTFail("Testing getting chat room memberships failure... has failed ")
            case .failure:
                e.fulfill()
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_get_user_chat_room_membership_success() {
        let e = expectation(description: "Successfully received User's Chat Room Memberships")
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            return builder.build()
        }()
        
        sdk.getUserChatRoomMemberships(page: .first) { result in
            switch result {
            case .success:
                e.fulfill()
            case .failure:
                XCTFail("Failed receiving Chat Room Memberships")
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_get_user_chat_room_membership_fail() {
        let e = expectation(description: "Successfully failed getting User's Chat Room Memberships")
        
        let networking = StubNetworking<ChatRoomMembership>(result: .failure(NetworkClientError.badRequest()))
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.networking = networking
            return builder.build()
        }()
        sdk.getChatRoomMemberships(
            roomID: "123",
            page: .first
        ) { result in
            switch result {
            case .success:
                XCTFail("Testing getting user's chat room memberships failure... has failed ")
            case .failure:
                e.fulfill()
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    // Tests whether EngagementSDK stores the next/previous urls correctly from calls to Get User's Chat Room Memberships
    func test_get_user_chat_room_membership_next_previous_pagination() {
        let e = expectation(description: "EngagementSDK successfuly stored next/previous page urls from Get User's Chat Room Memberships call")
        
        let nextUrl = URL(string: "www.next.org")!
        let previousUrl = URL(string: "www.previous.org")!
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getChatRoomsInfoPromise = Promise(value: UserChatRoomMembershipsResult(chatRooms: [], next: nextUrl, previous: previousUrl))
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()
        
        sdk.getUserChatRoomMemberships(page: .first) { result in
            switch result {
            case .success:
                if sdk.userChatRoomMembershipPagination.next == nextUrl, sdk.userChatRoomMembershipPagination.previous == previousUrl {
                    e.fulfill()
                } else {
                    XCTFail("EngagementSDK failed to store next/previous page urls from Get User's Chat Room Memberships call")
                }
            case .failure:
                XCTFail("EngagementSDK failed to store next/previous page urls from Get User's Chat Room Memberships call")
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_create_chat_room_membership_success() {
        let e = expectation(description: "Successfully created Chat Room Membership")
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            return builder.build()
        }()
        
        sdk.createUserChatRoomMembership(roomID: "123") { result in
            switch result {
            case .success:
                e.fulfill()
            case .failure:
                XCTFail("Failed receiving Chat Room Memberships")
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_create_chat_room_membership_fail() {
        let e = expectation(description: "Successfully failed creating Chat Room Memberships")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getChatRoomMemberPromise = Promise(error: NetworkClientError.badRequest())
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()
        
        sdk.createUserChatRoomMembership(roomID: "123") { result in
            switch result {
            case .success:
                XCTFail("Testing creating chat room memberships failure... has failed ")
            case .failure:
                e.fulfill()
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_add_user_to_chat_room_success() {
        let e = expectation(description: "Successfully Added User to Chat Room")
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            return builder.build()
        }()
        
        sdk.chat.addNewMemberToChatRoom(roomID: "123", profileID: "678") { result in
            switch result {
            case .success:
                e.fulfill()
            case .failure:
                XCTFail("Failed adding user to chatroom")
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_add_user_to_chat_room_fail() {
        let e = expectation(description: "Successfully failed creating adding user to chatroom")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getChatRoomMemberPromise = Promise(error: NetworkClientError.forbidden())
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()
        
        sdk.chat.addNewMemberToChatRoom(roomID: "123", profileID: "678") { result in
            switch result {
            case .success:
                XCTFail("Testing creating chat room memberships failure... has failed ")
            case .failure:
                e.fulfill()
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_chat_room_membership_deletion_success() {
        let e = expectation(description: "Successfully deleted Chat Room Membership")
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            return builder.build()
        }()
        
        sdk.deleteUserChatRoomMembership(
            roomID: "123",
            completion: { result in
                switch result {
                case .success:
                    e.fulfill()
                case .failure:
                    XCTFail("Failed receiving Chat Room Memberships")
                }
            }
        )
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_chat_room_membership_deletion_fail() {
        let e = expectation(description: "Successfully failed creating Chat Room Memberships")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.deleteChatRoomMembershipPromise = Promise(error: NetworkClientError.badRequest())
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()
        
        sdk.deleteUserChatRoomMembership(roomID: "123") { result in
            switch result {
            case .success:
                XCTFail("Testing deleting chat room memberships failure... has failed ")
            case .failure:
                e.fulfill()
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    // MARK: - Gamification
    
    func test_get_leaderboards_success() {
        let e = expectation(description: "Failed getting Leaderboards")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getLeaderboardsPromise = Promise(value: [MockGamification.mockLeaderboardResource(id: "test1")])
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()
        
        sdk.getLeaderboards(programID: "") { result in
            switch result {
            case .success(let leaderboards):
                if let leaderboard = leaderboards.first,
                   leaderboard.id == "test1"
                {
                    e.fulfill()
                } else {
                    XCTFail("Getting Leaderboards results in wrong results")
                }
                
            case .failure:
                XCTFail("Testing getting Leaderboards failed ")
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_get_leaderboards_fail() {
        let e = expectation(description: "Successfully failed getting Leaderboards")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getLeaderboardsPromise = Promise(error: NetworkClientError.badRequest())
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()
        
        sdk.getLeaderboards(programID: "") { result in
            switch result {
            case .success:
                XCTFail("Testing getting leaderboards failure... has failed ")
            case .failure:
                e.fulfill()
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_get_leaderboard_success() {
        let e = expectation(description: "Failed getting Leaderboard")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getLeaderboardPromise = Promise(value: MockGamification.mockLeaderboardResource(id: "test2"))
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()

        sdk.getLeaderboard(leaderboardID: "") { result in
            switch result {
            case .success(let leaderboard):
                if leaderboard.id == "test2" {
                    e.fulfill()
                } else {
                    XCTFail("Getting Leaderboard results in wrong results")
                }
                
            case .failure:
                XCTFail("Testing getting Leaderboard failed ")
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_get_leaderboard_fail() {
        let e = expectation(description: "Successfully failed getting Leaderboards")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getLeaderboardPromise = Promise(error: NetworkClientError.badRequest())
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()
        
        sdk.getLeaderboard(leaderboardID: "") { result in
            switch result {
            case .success:
                XCTFail("Testing getting leaderboards failure... has failed ")
            case .failure:
                e.fulfill()
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_get_leaderboard_entries_with_options_success() {
        let e = expectation(description: "Failed getting Leaderboard Entries with Options")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getLeaderboardEntriesPromise = Promise(value: MockGamification.mockLeaderboardPaginatedEntryResource())
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()
        
        let leaderboardEntriesRequest = GetLeaderboardEntriesRequestOptions(leaderboardID: "123")
        sdk.getLeaderboardEntries(
            page: .first,
            options: leaderboardEntriesRequest
        ) { result in
            switch result {
            case .success:
                e.fulfill()
            case .failure:
                XCTFail("Testing getting Leaderboards Entries with Options failed ")
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_get_leaderboard_entries_with_options_fail() {
        let e = expectation(description: "Successfully failed getting Leaderboard Entries with Options")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getLeaderboardEntriesPromise = Promise(error: NetworkClientError.badRequest())
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()
        
        let leaderboardEntriesRequest = GetLeaderboardEntriesRequestOptions(leaderboardID: "123")
        sdk.getLeaderboardEntries(
            page: .first,
            options: leaderboardEntriesRequest
        ) { result in
            switch result {
            case .success:
                XCTFail("Testing getting Leaderboard Entries with Options failure... has failed ")
            case .failure:
                e.fulfill()
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_get_leaderboard_entries_success() {
        let e = expectation(description: "Failed getting Leaderboard Entries")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getLeaderboardEntriesPromise = Promise(value: MockGamification.mockLeaderboardPaginatedEntryResource())
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()
        
        sdk.getLeaderboardEntries(leaderboardID: "", page: .first) { result in
            switch result {
            case .success:
                e.fulfill()
            case .failure:
                XCTFail("Testing getting Leaderboards failed ")
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_get_leaderboard_entries_fail() {
        let e = expectation(description: "Successfully failed getting Leaderboard Entries")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getLeaderboardEntriesPromise = Promise(error: NetworkClientError.badRequest())
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()
        
        sdk.getLeaderboardEntries(leaderboardID: "", page: .first) { result in
            switch result {
            case .success:
                XCTFail("Testing getting Leaderboard Entries failure... has failed ")
            case .failure:
                e.fulfill()
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    // MARK: -
    
    func test_get_chat_user_muted_status_success() {
        let e = expectation(description: "Successfully received correct chat user muted status")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getChatUserMutedStatusPromise = Promise(value: ChatUserMuteStatusResource(isMuted: true))
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()
        sdk.getChatUserMutedStatus(roomID: "") { result in
            switch result {
            case .success(let mutedStatus):
                if mutedStatus.isMuted == true {
                    e.fulfill()
                } else {
                    XCTFail("Failed getting correct result from getChatUserMutedStatus()")
                }
            case .failure(let error):
                XCTFail("Received an error (\(error.localizedDescription)) when getting chat user muted status")
            }
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_get_chat_user_muted_status_failure() {
        let e = expectation(description: "Successfully failed getChatUserMutedStatus()")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getChatUserMutedStatusPromise = Promise(error: NetworkClientError.badRequest())
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()
        sdk.getChatUserMutedStatus(roomID: "") { result in
            switch result {
            case .success:
                XCTFail("Testing getting getChatUserMutedStatus failure... has failed ")
            case .failure:
                e.fulfill()
            }
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    /// Test setting custom data api signature
    func test_set_custom_data_success() {
        let e = expectation(description: "Setting Custom Data")
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            return builder.build()
        }()
        
        sdk.setUserCustomData(data: "123") { result in
            switch result {
            case .success:
                e.fulfill()
            case .failure:
                XCTFail("Setting custom data failed")
            }
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    /// Test to make sure we can pass an empty string to be able to reset custom data
    func test_empty_custom_data_success() {
        let e = expectation(description: "Custom Data")
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            return builder.build()
        }()
        
        sdk.setUserCustomData(data: "") { result in
            switch result {
            case .success:
                e.fulfill()
            case .failure:
                XCTFail("Setting empty Data string should be allowed")
            }
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_get_sponsor_on_sdk_success() {
        let e = expectation(description: "Successfully retrieved a sponsor for a programID on the SDK level")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getSponsorshipCompletion = .success(
            [Sponsor(id: "1", clientID: "12", name: "Ringside Coffee Co", logoURL: MockObjects.mockURL, brandColorHex: "ffffff"),
             Sponsor(id: "2", clientID: "13", name: "Tredmill Desk", logoURL: MockObjects.mockURL, brandColorHex: "ffffff")]
        )
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockSponsorshipClient = MockSponsorshipClient(coreAPI: mockLLRestAPIServices)
            return builder.build()
        }()
        
        sdk.sponsorship.getBy(programID: "111") { result in
            switch result {
            case .success(let result):
                if let firstSponsor = result.first {
                    if firstSponsor.id == "1", firstSponsor.name == "Ringside Coffee Co" {
                        e.fulfill()
                    }
                }
            case .failure:
                XCTFail("Failed retrieving a sponsor for a programID on the SDK level")
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_get_sponsor_on_sdk_fail() {
        let e = expectation(description: "Successfully failed retrieving a sponsor for a programID on the SDK level")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        
        mockLLRestAPIServices.getSponsorshipCompletion = .failure(SponsorError.getSponsorFailed(error: "test error"))
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockSponsorshipClient = MockSponsorshipClient(coreAPI: mockLLRestAPIServices)
            return builder.build()
        }()
        
        sdk.sponsorship.getBy(programID: "111") { result in
            switch result {
            case .success:
                XCTFail("Retrieving a sponsor should fail here")
            case .failure:
                e.fulfill()
            }
        }
       
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    // MARK: - Remptions Keys
    func test_get_redemption_keys_success() {
        let e = expectation(description: "Successfully retrieved Redemption Keys")
        let page = PaginatedResult<RedemptionKey>(
            previous: nil,
            count: 1,
            next: nil,
            items: [MockObjects.mockRedemptionCode]
        )
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.networking = StubNetworking(result: .success(page))
            return builder.build()
        }()
        
        let tes = GetRedemptionKeysRequestOptions(status: .redeemed)
        sdk.getRedemptionKeys(
            page: .first,
            options: tes
        ) { result in
            
            switch result {
            case .failure:
                XCTFail("Failed retrieving redemption keys")
            case .success(let redemptionKeys):
                if redemptionKeys.count == 1 {
                    e.fulfill()
                }
            }
        }
       
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func test_get_redemption_keys_fail() {
        let e = expectation(description: "Successfully failed retrieving Redemption Keys")
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.networking = StubNetworking<[RedemptionKey]>(
                result: .failure(
                    RedemptionError.invalidRedemptionKeyDetailURL
                )
            )
            return builder.build()
        }()
        
        let tes = GetRedemptionKeysRequestOptions(status: .redeemed)
        sdk.getRedemptionKeys(
            page: .first,
            options: tes
        ) { result in
            
            switch result {
            case .failure:
                e.fulfill()
            case .success:
                XCTFail("Succesfully failed retrieving redemption keys")
            }
        }
       
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func test_redeem_key_by_keyID_success() {
        let e = expectation(description: "Successfully redeemed code by UUID")

        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        let redeemedKeyName = "MikeHasBeautifulHair"
        mockLLRestAPIServices.getRedeemKeyPromise = Promise(
            value: RedemptionKey(
                id: MockObjects.mockString,
                clientId: MockObjects.mockString,
                name: redeemedKeyName,
                description: nil,
                code: MockObjects.mockString,
                createdAt: Date(),
                redeemedAt: nil,
                redeemedBy: nil,
                status: .active,
                assignedTo: nil
            )
        )
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()
        
        sdk.redeemKeyBy(
            redemptionKeyID: "86a8d28c-b04a-4639-bd59-2f895d8546f6"
        ) { result in
                
            switch result {
            case .failure(let error):
                XCTFail("Failed \(error.localizedDescription)")
            case .success(let redeemedKey):
                if redeemedKey.name == redeemedKeyName {
                    e.fulfill()
                }
            }
        }
       
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func test_redeem_key_by_keyID_fail() {
        let e = expectation(description: "Successfully failed redeeming key by keyID")

        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getRedeemKeyPromise = Promise(error: RedemptionError.invalidRedemptionKeyDetailURL)
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()
        
        sdk.redeemKeyBy(
            redemptionKeyID: "86a8d28c-b04a-4639-bd59-2f895d8546f6"
        ) { result in
                
            switch result {
            case .failure:
                e.fulfill()
            case .success:
                XCTFail("Failed failing redeeming key by keyID")
            }
        }
       
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func test_redeem_key_by_code_success() {
        let e = expectation(description: "Successfully redeemed key by redemption code")

        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        let redeemedKeyName = "MikeHasBeautifulHair"
        mockLLRestAPIServices.getRedeemKeyPromise = Promise(
            value: RedemptionKey(
                id: MockObjects.mockString,
                clientId: MockObjects.mockString,
                name: redeemedKeyName,
                description: nil,
                code: MockObjects.mockString,
                createdAt: Date(),
                redeemedAt: nil,
                redeemedBy: nil,
                status: .active,
                assignedTo: nil
            )
        )
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()
        
        sdk.redeemKeyBy(
            redemptionCode: "86a8d28c-b04a-4639-bd59-2f895d8546f6"
        ) { result in
                
            switch result {
            case .failure(let error):
                XCTFail("Failed \(error.localizedDescription)")
            case .success(let redeemedCode):
                if redeemedCode.name == redeemedKeyName {
                    e.fulfill()
                }
            }
        }
       
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func test_redeem_code_by_code_fail() {
        let e = expectation(description: "Successfully failed redeeming key by redemption code")

        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getRedeemKeyPromise = Promise(error: RedemptionError.invalidRedemptionKeyDetailURL)
        
        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()
        
        sdk.redeemKeyBy(
            redemptionCode: "86a8d28c-b04a-4639-bd59-2f895d8546f6"
        ) { result in
                
            switch result {
            case .failure:
                e.fulfill()
            case .success:
                XCTFail("Failed failing redeeming key by redemption code")
            }
        }
       
        waitForExpectations(timeout: 5, handler: nil)
    }
}

class MockEngagementSDKDelegate: EngagementSDKDelegate {
    var setupFailedWithErrorCompletion: ((LiveLike, Error) -> Void)?

    func sdk(_ sdk: LiveLike, setupFailedWithError error: Error) {
        setupFailedWithErrorCompletion?(sdk, error)
    }
}
