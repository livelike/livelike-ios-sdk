//
//  RewardsClientTests.swift
//  EngagementSDKTests
//
//  Created by Mike Moloksher on 9/15/21.
//

import XCTest
@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift

class RewardsClientTests: XCTestCase {

    enum rewardsError: LocalizedError {
        case failure
    }

    let rewardItemResources: [RewardItem] = [
        MockObjects.mockRewardItem,
        MockObjects.mockRewardItem,
        MockObjects.mockRewardItem
    ]

    let rewardItemTransferJSON: String =
        """
        {
            "id": "476aa4f1-9f47-4e7e-bed0-9f2bf8da67cc",
            "reward_item_id": "26894d5d-1efa-4997-bfe0-7e105fdc474d",
            "reward_item_name": "Test Point",
            "reward_item_amount": 6,
            "created_at": "2021-11-05T16:03:44.457Z",
            "sender_profile_id": "b00b0b42-d6e4-4697-b0d7-137269a13805",
            "sender_profile_name": "ben",
            "recipient_profile_id": "5cf11253-b015-4bd2-a199-5f0bb4bcb4d9",
            "recipient_profile_name": "ben"
        }
        """

    let rewardItemJSON: String =
        """
        {
            "id":"26894d5d-1efa-4997-bfe0-7e105fdc474d",
            "url":"https://cf-blast-game-changers.livelikecdn.com/api/v1/reward-items/26894d5d-1efa-4997-bfe0-7e105fdc474d/",
            "client_id":"GaEBcpVrCxiJOSNu4bvX6krEaguxHR9Hlp63tK6L",
            "name":"Game Changer Points",
            "attributes": [
                {
                    "key": "color",
                    "value": "blue"
                },
                {
                    "key": "newkey",
                    "value": "newval"
                }
            ],
            "images": [
                {
                    "id": "60d788c7-f74e-467b-ad2e-ffd3591c0368",
                    "name": "6d651c2c-25b5-489b-a7ef-77e543b50488.gif",
                    "image_url": "https://cf-blast-storage-game-changers.livelikecdn.com/assets/7d980817-aeb9-4fb1-912a-0b5f9dfe3040.gif",
                    "mimetype": "application/octet-stream"
                }
            ]
        }
        """

    let rewardItemBalanceJSON: String =
        """
        {
            "reward_item_id":"26894d5d-1efa-4997-bfe0-7e105fdc474d",
            "reward_item_name":"Game Changer Points",
            "reward_item_balance":0
        }
        """

    let rewardTransactionJSON: String =
        """
        {
            "id":"19e5cd60-a4fe-4b78-86c2-66b5d8173d45",
            "url":"https://cf-blast-game-changers.livelikecdn.com/api/v1/rewards/19e5cd60-a4fe-4b78-86c2-66b5d8173d45/",
            "created_at":"2021-09-10T17:58:08Z",
            "widget_kind":"text-prediction",
            "widget_id":"b00b0b42-d6e4-4697-b0d7-137269a13805",
            "reward_item_id":"26894d5d-1efa-4997-bfe0-7e105fdc474d",
            "reward_item_name":"Game Changer Points",
            "reward_action_id":"56dd8dd2-0f05-4b95-9b8e-587ada5b4185",
            "reward_action_key":"prediction-correct",
            "reward_action_name":"Made a correct prediction",
            "profile_id":"b00b0b42-d6e4-4697-b0d7-137269a13805",
            "profile_nickname":"Colin",
            "reward_item_amount":7
        }
        """

    let decoder = LLJSONDecoder()

    // MARK: - JSON -> Resource conversion tests
    func test_reward_transfer_json_to_resource_conversion() {
        let jsonData = rewardItemTransferJSON.data(using: .utf8)!
        do {
            _ = try decoder.decode(RewardItemTransfer.self, from: jsonData)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    func test_reward_item_json_to_resource_conversion() {
        let jsonData = rewardItemJSON.data(using: .utf8)!
        do {
            _ = try decoder.decode(RewardItem.self, from: jsonData)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    func test_reward_item_balance_json_to_resource_conversion() {
        let jsonData = rewardItemBalanceJSON.data(using: .utf8)!
        do {
            _ = try decoder.decode(RewardItemBalance.self, from: jsonData)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    func test_reward_transaction_json_to_resource_conversion() {
        let jsonData = rewardTransactionJSON.data(using: .utf8)!
        do {
            _ = try decoder.decode(RewardTransaction.self, from: jsonData)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    // MARK: - getApplicationRewardItems tests
    func test_get_application_reward_items_success() {
        let e = expectation(description: "Successfully retrieved a all rewards attached to an application")
        let networking = StubNetworking(result: .success(
            PaginatedResult<RewardItem>(
                previous: nil,
                count: rewardItemResources.count,
                next: nil,
                items: rewardItemResources
            )
        ))

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockRewardsClient = InternalRewardsClient(
                coreAPI: builder.mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                rewardsAPI: builder.mockLLRestAPIServices,
                pubNubService: Promise(value: MockPubSubService()),
                userProfileVendor: builder.userProfileVendor,
                networking: networking
            )

            return builder.build()
        }()

        sdk.rewards.getApplicationRewardItems(page: .first) { result in
            switch result {
            case .success(let result):
                let secondBadge = result[1]
                if secondBadge.id == self.rewardItemResources[1].id, secondBadge.name == self.rewardItemResources[1].name {
                    e.fulfill()
                } else {
                    XCTFail("Failed retrieving all rewards from the Rewards Client due to wrong result")
                }
            case .failure(let error):
                XCTFail("Failed retrieving all rewards from the Rewards Client - \(error)")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_get_application_reward_items_with_options_success() {
        let e = expectation(description: "Successfully retrieved a all rewards attached to an application")

        let networking = StubNetworking(result: .success(
            PaginatedResult<RewardItem>(
                previous: nil,
                count: rewardItemResources.count,
                next: nil,
                items: rewardItemResources
            )
        ))

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockRewardsClient = InternalRewardsClient(
                coreAPI: builder.mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                rewardsAPI: builder.mockLLRestAPIServices,
                pubNubService: Promise(value: MockPubSubService()),
                userProfileVendor: builder.userProfileVendor,
                networking: networking
            )

            return builder.build()
        }()
        let attributes = [RewardItemAttribute(key: "key1", value: "val1"), RewardItemAttribute(key: "key2", value: "val2")]
        let options = GetApplicationRewardItemsRequestOptions(attributes: attributes)

        sdk.rewards.getApplicationRewardItems(
            page: .first,
            options: options
        ) { result in
            switch result {
            case .success(let result):
                let secondBadge = result[1]
                if secondBadge.id == self.rewardItemResources[1].id, secondBadge.name == self.rewardItemResources[1].name {
                    e.fulfill()
                } else {
                    XCTFail("Failed retrieving all rewards from the Rewards Client due to wrong result")
                }
            case .failure(let error):
                XCTFail("Failed retrieving all rewards from the Rewards Client - \(error)")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_get_application_reward_items_fail() {

        let e = expectation(description: "Successfully retrieved a all rewards attached to a client")
        let networking = StubNetworking<PaginatedResult<RewardItem>>(result: .failure(rewardsError.failure))

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockRewardsClient = InternalRewardsClient(
                coreAPI: builder.mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                rewardsAPI: builder.mockLLRestAPIServices,
                pubNubService: Promise(value: MockPubSubService()),
                userProfileVendor: builder.userProfileVendor,
                networking: networking
            )

            return builder.build()
        }()

        sdk.rewards.getApplicationRewardItems(page: .first) { result in
            switch result {
            case .success(_):
                XCTFail("Failed at failing getting all application reward items")
            case .failure(_):
                e.fulfill()
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    // MARK: - transferRewardItemAmount tests
    func test_transfer_item_amount_success() {
        let e = expectation(description: "Successfully tested reward item amount")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()

        let rewardItemAmount = 1
        let rewardItemID = MockObjects.mockString

        mockLLRestAPIServices.transferRewardItemAmountPromise = Promise(
            value: RewardItemTransfer(
                id: MockObjects.mockString,
                rewardItemID: rewardItemID,
                rewardItemName: MockObjects.mockString,
                rewardItemAmount: rewardItemAmount,
                recipientProfileID: MockObjects.mockString,
                recipientProfileName: MockObjects.mockString,
                senderProfileID: MockObjects.mockString,
                senderProfileName: MockObjects.mockString,
                createdAt: Date()
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockRewardsClient = InternalRewardsClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                rewardsAPI: mockLLRestAPIServices,
                pubNubService: Promise(value: MockPubSubService()),
                userProfileVendor: builder.userProfileVendor,
                networking: MockNetworking()
            )

            return builder.build()
        }()

        sdk.rewards.transferRewardItemAmount(
            rewardItemAmount,
            recipientProfileID: MockObjects.mockString,
            rewardItemID: rewardItemID
        ) { result in

            switch result {
            case .success(let result):
                if result.rewardItemID == rewardItemID, result.rewardItemAmount == rewardItemAmount {
                    e.fulfill()
                } else {
                    XCTFail("Failed tranfering reward item amount")
                }
            case .failure(let error):
                XCTFail("Failed tranfering reward item amount from the Rewards Client - \(error)")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_transfer_item_amount_fail() {
        let e = expectation(description: "Successfully tested reward item amount")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()

        let rewardItemAmount = 1
        let rewardItemID = MockObjects.mockString

        mockLLRestAPIServices.transferRewardItemAmountPromise = Promise(error: rewardsError.failure)

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockRewardsClient = InternalRewardsClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                rewardsAPI: mockLLRestAPIServices,
                pubNubService: Promise(value: MockPubSubService()),
                userProfileVendor: builder.userProfileVendor,
                networking: MockNetworking()
            )

            return builder.build()
        }()

        sdk.rewards.transferRewardItemAmount(
            rewardItemAmount,
            recipientProfileID: MockObjects.mockString,
            rewardItemID: rewardItemID
        ) { result in

            switch result {
            case .success(_):
                XCTFail("Failed at failing transfering reward item amount")
            case .failure(_):
                e.fulfill()
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    // MARK: - getRewardItemTransfers tests
    func test_get_reward_item_transfers_success() {
        let e = expectation(description: "Successfully tested getRewardItemTransfers()")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()

        let rewardItemTransfer = MockObjects.mockRewardItemTransfer
        mockLLRestAPIServices.getRewardItemTransfersPromise = Promise(
            value: PaginatedResult<RewardItemTransfer>(
                previous: nil,
                count: 0,
                next: nil,
                items: [rewardItemTransfer]
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockRewardsClient = InternalRewardsClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                rewardsAPI: mockLLRestAPIServices,
                pubNubService: Promise(value: MockPubSubService()),
                userProfileVendor: builder.userProfileVendor,
                networking: MockNetworking()
            )

            return builder.build()
        }()

        sdk.rewards.getRewardItemTransfers(
            page: .first
        ) { result in
            switch result {
            case .success(let result):

                if let firstTransfer = result.first {
                    if firstTransfer.rewardItemID == rewardItemTransfer.rewardItemID {
                        e.fulfill()
                    } else {
                        XCTFail("Failed matching reward item transfer")
                    }
                } else {
                    XCTFail("Failed getting first transfer")
                }
            case .failure(let error):
                XCTFail("Failed tranfering reward item amount from the Rewards Client - \(error)")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_get_reward_item_transfers_with_options_success() {
        let e = expectation(description: "Successfully tested getRewardItemTransfers()")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()

        let rewardItemTransfer = MockObjects.mockRewardItemTransfer
        mockLLRestAPIServices.getRewardItemTransfersPromise = Promise(
            value: PaginatedResult<RewardItemTransfer>(
                previous: nil,
                count: 0,
                next: nil,
                items: [rewardItemTransfer]
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockRewardsClient = InternalRewardsClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                rewardsAPI: mockLLRestAPIServices,
                pubNubService: Promise(value: MockPubSubService()),
                userProfileVendor: builder.userProfileVendor,
                networking: MockNetworking()
            )

            return builder.build()
        }()

        sdk.rewards.getRewardItemTransfers(
            page: .first,
            options: RewardItemTransferRequestOptions(transferType: .received)
        ) { result in

            switch result {
            case .success(let result):

                if let firstTransfer = result.first {
                    if firstTransfer.rewardItemID == rewardItemTransfer.rewardItemID {
                        e.fulfill()
                    } else {
                        XCTFail("Failed matching reward item transfer")
                    }
                } else {
                    XCTFail("Failed getting first transfer")
                }

            case .failure(let error):
                XCTFail("Failed tranfering reward item amount from the Rewards Client - \(error)")
            }

        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_get_reward_item_transfers_with_options_fail() {
        let e = expectation(description: "Successfully tested getRewardItemTransfers() failure")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()

        mockLLRestAPIServices.getRewardItemTransfersPromise = Promise(error: rewardsError.failure)

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockRewardsClient = InternalRewardsClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                rewardsAPI: mockLLRestAPIServices,
                pubNubService: Promise(value: MockPubSubService()),
                userProfileVendor: builder.userProfileVendor,
                networking: MockNetworking()
            )

            return builder.build()
        }()

        sdk.rewards.getRewardItemTransfers(
            page: .first,
            options: RewardItemTransferRequestOptions(transferType: .received)
        ) { result in

            switch result {
            case .success(_):
                XCTFail("Failed at failing getting reward item transfers")
            case .failure(_):
                e.fulfill()
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_get_reward_item_transfers_fail() {
        let e = expectation(description: "Successfully tested getRewardItemTransfers() failure")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()

        mockLLRestAPIServices.getRewardItemTransfersPromise = Promise(error: rewardsError.failure)

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockRewardsClient = InternalRewardsClient(coreAPI: mockLLRestAPIServices, accessTokenVendor: builder.accessTokenVendor, rewardsAPI: mockLLRestAPIServices, pubNubService: Promise(value: MockPubSubService()), userProfileVendor: builder.userProfileVendor, networking: MockNetworking())

            return builder.build()
        }()

        sdk.rewards.getRewardItemTransfers(
            page: .first
        ) { result in

            switch result {
            case .success(_):
                XCTFail("Failed at failing getting reward item transfers")
            case .failure(_):
                e.fulfill()
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    // MARK: - getRewardItemBalances() tests
    func test_reward_item_balances_success() {
        let e = expectation(description: "Successfully tested getRewardItemBalances()")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()

        let rewardItemBalance = RewardItemBalance(rewardItemID: MockObjects.mockString, rewardItemName: MockObjects.mockString, rewardItemBalance: 0)

        mockLLRestAPIServices.getRewardItemBalancesPromise = Promise(
            value: PaginatedResult<RewardItemBalance>(
                previous: nil,
                count: 0,
                next: nil,
                items: [rewardItemBalance]
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockRewardsClient = InternalRewardsClient(coreAPI: mockLLRestAPIServices, accessTokenVendor: builder.accessTokenVendor, rewardsAPI: mockLLRestAPIServices, pubNubService: Promise(value: MockPubSubService()), userProfileVendor: builder.userProfileVendor, networking: MockNetworking())

            return builder.build()
        }()

        sdk.rewards.getRewardItemBalances(page: .first, rewardItemIDs: [rewardItemBalance.rewardItemID]) { result in
            switch result {
            case .success(let result):

                if let firstRewardItemBalance = result.first {
                    if firstRewardItemBalance.rewardItemID == rewardItemBalance.rewardItemID {
                        e.fulfill()
                    } else {
                        XCTFail("Failed matching reward item balance")
                    }
                } else {
                    XCTFail("Failed getting first reward item blanace")
                }

            case .failure(let error):
                XCTFail("Failed gettings reward item balances from the Rewards Client - \(error)")
            }

        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_reward_item_balances_fail() {
        let e = expectation(description: "Successfully tested getRewardItemBalances() failure")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()

        let rewardItemBalance = RewardItemBalance(rewardItemID: MockObjects.mockString, rewardItemName: MockObjects.mockString, rewardItemBalance: 0)
        mockLLRestAPIServices.getRewardItemBalancesPromise = Promise(error: rewardsError.failure)

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockRewardsClient = InternalRewardsClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                rewardsAPI: mockLLRestAPIServices,
                pubNubService: Promise(value: MockPubSubService()),
                userProfileVendor: builder.userProfileVendor,
                networking: MockNetworking()
            )

            return builder.build()
        }()

        sdk.rewards.getRewardItemBalances(page: .first, rewardItemIDs: [rewardItemBalance.rewardItemID]) { result in

            switch result {
            case .success(_):
                XCTFail("Failed at failing getting reward item balances")

            case .failure(_):
                e.fulfill()

            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_get_reward_transactions_success() {
        let e = expectation(description: "Successfully tested getRewardTransactions()")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()

        let rewardTransaction = RewardTransaction(
            id: MockObjects.mockString,
            rewardItemID: MockObjects.mockString,
            rewardItemName: MockObjects.mockString,
            rewardItemAmount: 1,
            profileID: MockObjects.mockString,
            profileNickname: MockObjects.mockString,
            widgetKind: .alert,
            widgetID: MockObjects.mockString,
            rewardActionID: MockObjects.mockString,
            rewardActionKey: MockObjects.mockString,
            rewardActionName: MockObjects.mockString,
            createdAt: Date()
        )

        mockLLRestAPIServices.getRewardTransactionsPromise = Promise(
            value: PaginatedResult<RewardTransaction>(
                previous: nil,
                count: 0,
                next: nil,
                items: [rewardTransaction]
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockRewardsClient = InternalRewardsClient(coreAPI: mockLLRestAPIServices, accessTokenVendor: builder.accessTokenVendor, rewardsAPI: mockLLRestAPIServices, pubNubService: Promise(value: MockPubSubService()), userProfileVendor: builder.userProfileVendor, networking: MockNetworking())

            return builder.build()
        }()

        sdk.rewards.getRewardTransactions(
            page: .first,
            options: RewardTransactionsRequestOptions(widgetIDs: ["1", "1"], widgetKind: [.alert, .alert, .cheerMeter])
        ) { result in
            switch result {
            case .success(let result):

                if let firstRewardTransaction = result.first {
                    if firstRewardTransaction.rewardItemID == rewardTransaction.rewardItemID {
                        e.fulfill()
                    } else {
                        XCTFail("Failed matching reward transaction")
                    }
                } else {
                    XCTFail("Failed getting first reward transaction")
                }

            case .failure(let error):
                XCTFail("Failed gettings reward transaction from the Rewards Client - \(error)")
            }
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_get_reward_transactions_fail() {
        let e = expectation(description: "Successfully tested getRewardTransactions() failure")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()

        mockLLRestAPIServices.getRewardTransactionsPromise = Promise(error: rewardsError.failure)

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockRewardsClient = InternalRewardsClient(coreAPI: mockLLRestAPIServices, accessTokenVendor: builder.accessTokenVendor, rewardsAPI: mockLLRestAPIServices, pubNubService: Promise(value: MockPubSubService()), userProfileVendor: builder.userProfileVendor, networking: MockNetworking())

            return builder.build()
        }()

        sdk.rewards.getRewardTransactions(
            page: .first,
            options: RewardTransactionsRequestOptions(widgetIDs: ["123", "000"], widgetKind: [.alert, .cheerMeter])
        ) { result in

            switch result {
            case .success(_):
                XCTFail("Failed at failing getting reward item balances")

            case .failure(_):
                e.fulfill()

            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_get_reward_transactions_options_success() {
        let e = expectation(description: "Successfully tested reward transactions options")

        let apiOrigin = URL(string: "http://www.api.com")!

        let requestOptions = RewardTransactionsRequestOptions(
            widgetIDs: ["1", "2", "3", "1"],
            widgetKind: [.alert, .alert, .cheerMeter],
            profileIDs: ["3", "2", "1", "1"],
            rewardItemIDs: ["5", "21"],
            rewardActionKeys: ["actionkey1", "actionkey2"],
            createdSince: Date(),
            createdUntil: Date()
        )

        if let rewardTransactionsURL = try? requestOptions.buildURL(base: apiOrigin) {

            if
                rewardTransactionsURL.absoluteString.components(separatedBy: apiOrigin.absoluteString).count - 1 == 1,
                rewardTransactionsURL.absoluteString.components(separatedBy: "widget_id").count - 1 == 3,
                rewardTransactionsURL.absoluteString.components(separatedBy: "widget_kind").count - 1 == 2,
                rewardTransactionsURL.absoluteString.components(separatedBy: "profile_id").count - 1 == 3,
                rewardTransactionsURL.absoluteString.components(separatedBy: "reward_item").count - 1 == 2,
                rewardTransactionsURL.absoluteString.components(separatedBy: "reward_action_key").count - 1 == 2,
                rewardTransactionsURL.absoluteString.components(separatedBy: "created_since").count - 1 == 1,
                rewardTransactionsURL.absoluteString.components(separatedBy: "created_until").count - 1 == 1
            {
                e.fulfill()
            } else {
                XCTFail("URL parameter amounts do not match")
            }

        } else {
            XCTFail("Failed building URL")
        }


        waitForExpectations(timeout: 5.0, handler: nil)
    }

}
