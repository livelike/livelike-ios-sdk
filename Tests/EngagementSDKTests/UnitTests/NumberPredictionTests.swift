//
//  NumberPredictionTests.swift
//  EngagementSDKTests
//
//  Created by Jelzon Monzon on 9/8/21.
//

@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift
import XCTest

class NumberPredictionTests: XCTestCase {
    
    func test_model_data_matches_resource() {
        let resource = NumberPredictionResourceBuilder().build()
        let model: InternalNumberPredictionWidgetModel = {
            let builder = NumberPredictionModelBuilder()
            builder.resource = resource
            return builder.build()
        }()
        
        test_widget_metadata_copied_from_resource(
            metadata: resource,
            model: model
        )
        XCTAssert(resource.options == model.options)
        XCTAssert(resource.question == model.question)
    }
    
    func test_registerImpression() {
        let e = expectation(description: "register impression success")
        let model: InternalNumberPredictionWidgetModel = {
            let builder = NumberPredictionModelBuilder()
            let mockAPI = MockLiveLikeRestAPIServices()
            mockAPI.createImpressionCompletion = { _, _, _ in
                return Promise(value: .init(id: "id"))
            }
            builder.widgetAPI = mockAPI
            return builder.build()
        }()
        
        model.registerImpression { result in
            switch result {
            case .failure(let error):
                XCTFail(error.localizedDescription)
            case .success:
                e.fulfill()
            }
        }
        
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    func test_markAsInteractive() {
        let e = expectation(description: "event recorder record called")
        let model: InternalNumberPredictionWidgetModel = {
            let builder = NumberPredictionModelBuilder()
            let mockEventRecorder = MockEventRecorder()
            mockEventRecorder.recordCompletion = { event in
                e.fulfill()
            }
            builder.eventRecorder = mockEventRecorder
            return builder.build()
        }()
        
        model.markAsInteractive()
        waitForExpectations(timeout: 1.0, handler: nil)
    }
}

class NumberPredictionModelBuilder {
    
    var resource: NumberPrediction
    var eventRecorder: EventRecorder
    var widgetAPI: LiveLikeWidgetAPIProtocol
    var userProfile: UserProfileProtocol
    var widgetInteractionRepo: WidgetInteractionRepository
    var followUpModelFactory: PredictionWidgetModelFactoryProtocol
    var widgetClient: PubSubWidgetClient
    var programSubscribeChannel: String
    
    init() {
        self.resource = NumberPredictionResourceBuilder().build()
        self.eventRecorder = MockEventRecorder()
        self.widgetAPI = MockLiveLikeRestAPIServices()
        self.userProfile = MockUserProfile()
        self.widgetInteractionRepo = MockWidgetInteractionRepository()
        self.followUpModelFactory = MockPredictionWidgetModelFactory()
        self.widgetClient = MockWidgetClient()
        self.programSubscribeChannel = "programSubscribeChannel"
    }
    
    func build() -> InternalNumberPredictionWidgetModel {
        return InternalNumberPredictionWidgetModel(
            resource: resource,
            eventRecorder: eventRecorder,
            widgetAPI: widgetAPI,
            userProfile: userProfile,
            widgetInteractionRepo: widgetInteractionRepo,
            followUpModelFactory: followUpModelFactory,
            widgetClient: widgetClient,
            programSubscribeChannel: programSubscribeChannel
        )
    }
}

class NumberPredictionResourceBuilder {
    
    var baseData: BaseWidgetResource
    var question: String
    var options: [NumberPredictionOption]
    var voteURL: URL
    var widgetInteractionsURLTemplate: String
    var followUps: [NumberPredictionFollowUp]
    var confirmationMessage: String
    
    init() {
        baseData = WidgetMetadataResourceBuilder().build()
        question = "question"
        options = [
            .init(id: "ex-1", imageURL: nil, text: "text 1", correctNumber: nil, url: MockObjects.mockURL),
            .init(id: "ex-2", imageURL: nil, text: "text 2", correctNumber: nil, url: MockObjects.mockURL)
        ]
        voteURL = MockObjects.mockURL
        self.widgetInteractionsURLTemplate = "widgetInteractionsURLTemplate"
        self.followUps = []
        self.confirmationMessage = "confirmationMessage"
    }
    
    func build() -> NumberPrediction {
        return NumberPrediction(
            baseData: baseData,
            question: question,
            options: options,
            voteURL: voteURL,
            widgetInteractionsURLTemplate: widgetInteractionsURLTemplate,
            followUps: followUps,
            confirmationMessage: confirmationMessage
        )
    }
    
}
