//
//  ContentSessionTests.swift
//  EngagementSDKTests
//
//  Created by jelzon on 4/11/19.
//

@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift
import XCTest

class ContentSessionTests: XCTestCase {

    /// valid JSON taken from server on 4/18/2020
    let validWidgetJson = """
    {
       "client_id":"ex-client-id-1",
       "id":"bfbf9cc2-7cd6-467b-8391-a8d0ab35cc64",
       "url":"https://cf-blast.livelikecdn.com/api/v1/text-polls/bfbf9cc2-7cd6-467b-8391-a8d0ab35cc64/",
       "kind":"text-poll",
       "program_id":"8f34411b-159c-4840-b8ca-794785dfc1ca",
       "created_at":"2019-05-23T12:00:28.359868Z",
       "published_at":"2019-05-23T12:00:42.762025Z",
       "scheduled_at":null,
       "question":"sd",
       "timeout":"P0DT00H00M07S",
       "options":[
          {
             "id":"6ae8019b-8552-438a-9bd2-86e53bde76d6",
             "description":"s",
             "vote_count":0,
             "vote_url":"https://vc3e1qa5ef.execute-api.us-east-1.amazonaws.com/services/votes/text-poll/bfbf9cc2-7cd6-467b-8391-a8d0ab35cc64/6ae8019b-8552-438a-9bd2-86e53bde76d6",
             "translations":{

             },
             "translatable_fields":[
                "description"
             ]
          },
          {
             "id":"08962181-2129-4cd7-b0f6-33c775c7d4a5",
             "description":"d",
             "vote_count":1,
             "vote_url":"https://vc3e1qa5ef.execute-api.us-east-1.amazonaws.com/services/votes/text-poll/bfbf9cc2-7cd6-467b-8391-a8d0ab35cc64/08962181-2129-4cd7-b0f6-33c775c7d4a5",
             "translations":{

             },
             "translatable_fields":[
                "description"
             ]
          }
       ],
       "subscribe_channel":"text_poll_bfbf9cc2_7cd6_467b_8391_a8d0ab35cc64",
       "program_date_time":"2019-05-23T11:59:55.119000Z",
       "publish_delay":"P0DT00H00M00S",
       "widget_interactions_url_template":"https://cf-blast.livelikecdn.com/api/v1/text-polls/bfbf9cc2-7cd6-467b-8391-a8d0ab35cc64/interactions/",
       "impression_url":"https://blast-collector-prod-us-east-2.livelikecdn.com/services/impressions/text-poll/bfbf9cc2-7cd6-467b-8391-a8d0ab35cc64",
       "impression_count":0,
       "unique_impression_count":0,
       "engagement_count":1,
       "engagement_percent":"0.000",
       "status":"published",
       "created_by":{
          "id":"1bf5b2ff-d82b-4400-b4eb-38a8d7bf538c",
          "name":"Ben Wilber",
          "image_url":"https://cf-blast-storage.livelikecdn.com/assets/b8d6c072-902d-4616-9575-b596817029ac.png"
       },
       "schedule_url":"https://cf-blast.livelikecdn.com/api/v1/text-polls/bfbf9cc2-7cd6-467b-8391-a8d0ab35cc64/schedule/",
       "rewards_url":null,
       "translations":{

       },
       "translatable_fields":[
          "question"
       ],
       "reactions":[

       ],
       "custom_data":null,
       "sponsors": [
        {
          "id": "08e0ace4-859d-4e8a-ba87-944111df95b4",
          "logo_url": "/media/assets/8d234c60-1243-4182-9ea4-bb1d2f574373.png",
          "client_id": "IOWGz4LAfkVNfdLUS6c0B1iFewR2BFrm6HgEOchI",
          "name": "Pepsi"
        }],
        "widgetAttributes": []
    }
    """

    func test_invalid_session_raises_error() {
        let engagementSDK: LiveLike = {
            let builder = EngagementSDKBuilder()
            var sdkconfig = EngagementSDKConfig(clientID: "xrVpMNZSIbMD0EjORZoQRKB8Q9g6xy7RfZWn8BZM")
            sdkconfig.apiOrigin = URL(string: "https://cf-blast-dev.livelikecdn.com/api/v1/")!
            builder.config = sdkconfig
            return builder.build()
        }()
        let config = SessionConfiguration(programID: "invalid-id")
        let sessionSpy = SessionSpy()
        sessionSpy.errorExpection = expectation(description: "Called from the session delegate when an error occurs")
        _ = engagementSDK.contentSession(config: config, delegate: sessionSpy)

        waitForExpectations(timeout: 20.0) { error in
            if let error = error {
                XCTFail("Wait for expectation timeout error: \(error)")
                return
            }
            XCTAssertNotNil(sessionSpy.error)
        }
    }

    func test_getWidgetModel_byID_for_cheerMeter_is_successful() {
        let e = expectation(description: "Successfully retrieved cheer meter model")

        let livelikeAPI = MockLiveLikeRestAPIServices()
        livelikeAPI.getWidgetCompletion = { id, kind in
            let cheerMeterCreated = CheerMeterCreated(
                baseData: {
                    let builder = WidgetMetadataResourceBuilder()
                    builder.id = id
                    builder.kind = kind
                    builder.programID = "program-id-1"
                    return builder.build()
                }(),
                rewardsUrl: nil,
                question: "question",
                options: [
                    .init(
                        id: "option-id-1",
                        description: "option 1",
                        imageUrl: MockObjects.mockURL,
                        voteUrl: MockObjects.mockURL,
                        voteCount: 0
                    ),
                    .init(
                        id: "option-id-2",
                        description: "option 2",
                        imageUrl: MockObjects.mockURL,
                        voteUrl: MockObjects.mockURL,
                        voteCount: 0
                    )
                ],
                widgetInteractionsUrlTemplate: "",
                sponsors: [MockObjects.sponsorResource]
            )
            return Promise(value: .cheerMeterCreated(cheerMeterCreated))
        }

        let session = ContentSessionBuilder()
            .config(SessionConfiguration(programID: "program-id-1"))
            .livelikeRestAPIService(livelikeAPI)
            .build()

        session.getWidgetModel(byID: "cheer-meter-1", kind: .cheerMeter) { result in
            switch result {
            case .success(let widgetModel):
                switch widgetModel {
                case .cheerMeter:
                    e.fulfill()
                default:
                    XCTFail("wrong widgetModel")
                }
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_createWidgetModel_byID_for_cheerMeter_fails_when_programID_mismatched() {
        let e = expectation(description: "Failed because program id mismatch")

        let livelikeAPI = MockLiveLikeRestAPIServices()
        livelikeAPI.getWidgetCompletion = { id, kind in
            let cheerMeterCreated = CheerMeterCreated(
                baseData: {
                    let builder = WidgetMetadataResourceBuilder()
                    builder.id = id
                    builder.kind = kind
                    return builder.build()
                }(),
                rewardsUrl: nil,
                question: "question",
                options: [
                    .init(
                        id: "option-id-1",
                        description: "option 1",
                        imageUrl: MockObjects.mockURL,
                        voteUrl: MockObjects.mockURL,
                        voteCount: 0
                    ),
                    .init(
                        id: "option-id-2",
                        description: "option 2",
                        imageUrl: MockObjects.mockURL,
                        voteUrl: MockObjects.mockURL,
                        voteCount: 0
                    )
                ],
                widgetInteractionsUrlTemplate: "",
                sponsors: [MockObjects.sponsorResource]
            )
            return Promise(value: .cheerMeterCreated(cheerMeterCreated))
        }

        let session = ContentSessionBuilder()
            .config(SessionConfiguration(programID: "program-id-2"))
            .livelikeRestAPIService(livelikeAPI)
            .build()

        session.getWidgetModel(byID: "cheer-meter-1", kind: .cheerMeter) { result in
            switch result {
            case .success(let widgetModel):
                switch widgetModel {
                case .cheerMeter(let cheermeter):
                    XCTFail("\(cheermeter.kind) should fail")
                default:
                    XCTFail("should fail")
                }

            case .failure:
                e.fulfill()
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_getWidgetModel_fromJSON_for_cheerMeter_isSuccessful() {
        let e = expectation(description: "Successfully retrieved cheer meter model")

        let cheerMeterCreated = CheerMeterCreated(
            baseData: {
                let builder = WidgetMetadataResourceBuilder()
                builder.id = "cheer-meter-1"
                builder.programID = "program-id-1"
                builder.kind = .cheerMeter
                builder.subscribeChannel = "sub-channel-1"
                return builder.build()
            }(),
            rewardsUrl: nil,
            question: "question",
            options: [
                .init(
                    id: "option-id-1",
                    description: "option 1",
                    imageUrl: MockObjects.mockURL,
                    voteUrl: MockObjects.mockURL,
                    voteCount: 0
                ),
                .init(
                    id: "option-id-2",
                    description: "option 2",
                    imageUrl: MockObjects.mockURL,
                    voteUrl: MockObjects.mockURL,
                    voteCount: 0
                )
            ],
            widgetInteractionsUrlTemplate: "",
            sponsors: [MockObjects.sponsorResource]
        )

        let session = ContentSessionBuilder()
            .config(SessionConfiguration(programID: "program-id-1"))
            .build()
        do {
            let encoder = LLJSONEncoder()
            let json = try encoder.encode(cheerMeterCreated)
            let data = try JSONSerialization.jsonObject(with: json, options: [])

            session.createWidgetModel(fromJSON: data) { result in
                switch result {
                case .success(let widgetModel):
                    switch widgetModel {
                    case .cheerMeter:
                        e.fulfill()
                    default:
                        XCTFail("wrong widgetModel")
                    }
                case .failure(let error):
                    XCTFail(error.localizedDescription)
                }
            }
        } catch {
            XCTFail(error.localizedDescription)
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_createWidgetModel_fromJSON_for_cheerMeter_fails_when_programID_mismatched() {
        let e = expectation(description: "Failed because program id mismatch")

        let cheerMeterCreated = CheerMeterCreated(
            baseData: {
                let builder = WidgetMetadataResourceBuilder()
                builder.id = "cheer-meter-1"
                builder.programID = "program-id-1"
                builder.kind = .cheerMeter
                return builder.build()
            }(),
            rewardsUrl: nil,
            question: "question",
            options: [
                .init(
                    id: "option-id-1",
                    description: "option 1",
                    imageUrl: MockObjects.mockURL,
                    voteUrl: MockObjects.mockURL,
                    voteCount: 0
                ),
                .init(
                    id: "option-id-2",
                    description: "option 2",
                    imageUrl: MockObjects.mockURL,
                    voteUrl: MockObjects.mockURL,
                    voteCount: 0
                )
            ],
            widgetInteractionsUrlTemplate: "",
            sponsors: [MockObjects.sponsorResource]
        )

        let session = ContentSessionBuilder()
            .config(SessionConfiguration(programID: "program-id-2"))
            .build()

        do {
            let encoder = LLJSONEncoder()
            let json = try encoder.encode(cheerMeterCreated)
            let data = try JSONSerialization.jsonObject(with: json, options: [])

            session.createWidgetModel(fromJSON: data) { result in
                switch result {
                case .success:
                    XCTFail("should fail")
                case .failure(let error):
                    if case let ContentSessionError.failedToCreateWidgetModelMismatchedProgramID(widgetProgramID, sessionProgramID) = error {
                        XCTAssert(widgetProgramID == "program-id-1")
                        XCTAssert(sessionProgramID == "program-id-2")
                        e.fulfill()
                    } else {
                        XCTFail("Expected failedToCreateWidgetModelMismatchedProgramID error")
                    }
                }
            }
        } catch {
            XCTFail(error.localizedDescription)
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_getPostedWidgetModels_for_cheerMeter_isSuccessful() {
        let e = expectation(description: "Successfully retrieved cheer meter model")

        let cheerMeterCreated = CheerMeterCreated(
            baseData: {
                let builder = WidgetMetadataResourceBuilder()
                builder.id = "cheer-meter-1"
                builder.programID = "program-id-1"
                builder.kind = .cheerMeter
                return builder.build()
            }(),
            rewardsUrl: nil,
            question: "question",
            options: [
                .init(
                    id: "option-id-1",
                    description: "option 1",
                    imageUrl: MockObjects.mockURL,
                    voteUrl: MockObjects.mockURL,
                    voteCount: 0
                ),
                .init(
                    id: "option-id-2",
                    description: "option 2",
                    imageUrl: MockObjects.mockURL,
                    voteUrl: MockObjects.mockURL,
                    voteCount: 0
                )
            ],
            widgetInteractionsUrlTemplate: "",
            sponsors: [MockObjects.sponsorResource]
        )

        let livelikeAPI = MockLiveLikeRestAPIServices()
        livelikeAPI.getTimelineCompletion = { _, _ in
            let resource = WidgetTimelineResource(
                previous: nil,
                count: 0,
                next: nil,
                results: [
                    .cheerMeterCreated(cheerMeterCreated),
                    .cheerMeterCreated(cheerMeterCreated)
                ],
                widgetInteractionsURLTemplate: "https://google.com/{profile_id}/"
            )
            return Promise(value: resource)
        }

        let session = ContentSessionBuilder()
            .livelikeRestAPIService(livelikeAPI)
            .build()

        session.getPostedWidgetModels(page: .first) { result in
            switch result {
            case .success(let widgets):
                XCTAssert(widgets != nil)
                XCTAssert(widgets?.count == 2)
                e.fulfill()
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_getPostedWidgetModels_first_then_next_page_succeeds() {
        let e = expectation(description: "getPostedWidgetModels successfully returns next page after calling .first then .next")

        let firstURL: URL = URL(string: "https://first.com")!
        let nextURL: URL = URL(string: "https://second.com")!

        let firstResults: WidgetTimelineResource = .init(
            previous: nil,
            count: 1,
            next: nextURL,
            results: [],
            widgetInteractionsURLTemplate: ""
        )

        let nextResults: WidgetTimelineResource = .init(
            previous: nil,
            count: 1,
            next: nil,
            results: [],
            widgetInteractionsURLTemplate: ""
        )

        var firstURLCalled: Bool = false

        let livelikeAPI = MockLiveLikeRestAPIServices()
        livelikeAPI.getTimelineCompletion = { url, accessToken in
            if firstURL == url {
                firstURLCalled = true
                return Promise(value: firstResults)
            } else if nextURL == url {
                XCTAssert(firstURLCalled)
                e.fulfill()
                return Promise(value: nextResults)
            } else {
                XCTFail("Expected firstURL or nextURL. got \(url)")
                return Promise(error: MockError())
            }
        }

        let session = ContentSessionBuilder()
            .programDetail(
                ProgramDetailBuilder()
                    .timelineURL(firstURL)
                    .build()
            )
            .livelikeRestAPIService(livelikeAPI)
            .build()

        session.getPostedWidgetModels(page: .first) { firstResult in
            switch firstResult {
            case .success:
                session.getPostedWidgetModels(page: .next) { _ in }
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_getPostedWidgets_first_then_next_page_succeeds() {
        let e = expectation(description: "getPostedWidgetModels successfully fails when calling with .next without calling .first")

        let firstURL: URL = URL(string: "https://first.com")!
        let nextURL: URL = URL(string: "https://second.com")!

        let firstResults: WidgetTimelineResource = .init(
            previous: nil,
            count: 1,
            next: nextURL,
            results: [],
            widgetInteractionsURLTemplate: ""
        )

        let nextResults: WidgetTimelineResource = .init(
            previous: nil,
            count: 1,
            next: nil,
            results: [],
            widgetInteractionsURLTemplate: ""
        )

        var firstURLCalled: Bool = false

        let livelikeAPI = MockLiveLikeRestAPIServices()
        livelikeAPI.getTimelineCompletion = { url, accessToken in
            if firstURL == url {
                firstURLCalled = true
                return Promise(value: firstResults)
            } else if nextURL == url {
                XCTAssert(firstURLCalled)
                e.fulfill()
                return Promise(value: nextResults)
            } else {
                XCTFail("Expected firstURL or nextURL. got \(url)")
                return Promise(error: MockError())
            }
        }

        let session = ContentSessionBuilder()
            .programDetail(
                ProgramDetailBuilder()
                    .timelineURL(firstURL)
                    .build()
            )
            .livelikeRestAPIService(livelikeAPI)
            .build()

        session.getPostedWidgets(page: .first) { firstResult in
            switch firstResult {
            case .success:
                session.getPostedWidgets(page: .next) { _ in }
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_getWidgetInteractionWithUnclaimedRewards_next_page_first_fails() {
        let e = expectation(description: "Expected to fail")
        let contentSession = ContentSessionBuilder().build()
        contentSession.getWidgetInteractionWithUnclaimedRewards(page: .next) { result in
            switch result {
            case .failure:
                e.fulfill()
            case .success:
                XCTFail("Expected to fail")
            }
        }
        waitForExpectations(timeout: 1.0, handler: nil)
    }

    func test_getWidgetInteractionWithUnclaimedRewards_with_first_succeeds() {
        let e = expectation(description: "Expected to succeed")
        let livelikeAPI = MockLiveLikeRestAPIServices()
        livelikeAPI.getUnclaimedWidgetInteractionsCompletion = { _, _ in
            let interactions = PaginatedResult<WidgetInteraction>(
                previous: nil,
                count: 0,
                next: nil,
                items: []
            )
            return .init(value: interactions)
        }
        let contentSession = ContentSessionBuilder()
            .livelikeRestAPIService(livelikeAPI)
            .build()
        contentSession.getWidgetInteractionWithUnclaimedRewards(page: .first) { result in
            switch result {
            case .failure(let error):
                XCTFail("Expected to succeed: \(error.localizedDescription)")
            case .success:
                e.fulfill()
            }
        }
        waitForExpectations(timeout: 1.0, handler: nil)
    }

    func test_getWidgetInteractionWithUnclaimedRewards_with_first_then_next_succeeds() {
        let e = expectation(description: "Expected to succeed")
        let livelikeAPI = MockLiveLikeRestAPIServices()
        var callCount = 0
        livelikeAPI.getUnclaimedWidgetInteractionsCompletion = { _, _ in
            if callCount == 0 {
                callCount += 1
                let interactions = PaginatedResult<WidgetInteraction>(
                    previous: nil,
                    count: 0,
                    next: MockObjects.mockURL,
                    items: []
                )
                return .init(value: interactions)
            } else {
                callCount += 1
                let interactions = PaginatedResult<WidgetInteraction>(
                    previous: nil,
                    count: 0,
                    next: nil,
                    items: []
                )
                return .init(value: interactions)
            }
        }
        let contentSession = ContentSessionBuilder()
            .livelikeRestAPIService(livelikeAPI)
            .build()
        contentSession.getWidgetInteractionWithUnclaimedRewards(page: .first) { result in
            switch result {
            case .failure(let error):
                XCTFail("Expected to succeed: \(error.localizedDescription)")
            case .success:
                contentSession.getWidgetInteractionWithUnclaimedRewards(page: .next) { result in
                    switch result {
                    case .failure(let error):
                        XCTFail("Expected to succeed: \(error.localizedDescription)")
                    case .success:
                        e.fulfill()
                    }
                }
            }
        }
        waitForExpectations(timeout: 1.0, handler: nil)
    }

    func test_getWidgetModels_isSuccessful() {
        let e = expectation(description: "Successfully retrieved widget models")

        let jsonData = validWidgetJson.data(using: .utf8)!
        guard let jsonObject = try? JSONSerialization.jsonObject(with: jsonData, options: []) else {
            XCTFail("Data failed to be converted to JSON")
            return
        }
        guard let widgetResource = try? WidgetPayloadParser.parse(jsonObject) else {
            XCTFail("WidgetPayloadParser failed to parse JSON")
            return
        }
        let networking = StubNetworking(
            result: .success(
                PaginatedResult<WidgetResource>(
                    previous: nil,
                    count: 0,
                    next: nil,
                    items: [widgetResource]
                )
            )
        )

        let session = ContentSessionBuilder()
            .networking(networking)
            .build()

        let options = GetWidgetModelsRequestOptions(
            widgetStatus: .published,
            widgetKind: [.textPrediction, .imagePrediction],
            widgetOrdering: .recent
        )

        session.getWidgetModels(
            page: .first,
            options: options
        ) { result in

            switch result {
            case .success(let widgets):
                if let firstWidget = widgets.first {
                    if firstWidget.kind.displayName == "Text Poll" {
                        e.fulfill()
                    }
                }
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_getWidgetModels_failure() {
        let e = expectation(description: "Successfully failed getting widget models")
        let networking = StubNetworking<PaginatedResult<WidgetResource>>(
            result: .failure(PaginationErrors.nextPageUnavailable)
        )

        let session = ContentSessionBuilder()
            .networking(networking)
            .build()

        let options = GetWidgetModelsRequestOptions(
            widgetStatus: .published,
            widgetKind: [.textPrediction, .imagePrediction],
            widgetOrdering: .recent
        )

        session.getWidgetModels(
            page: .first,
            options: options
        ) { result in

            switch result {
            case .success:
                XCTFail("Getting Widget Models failed at failing")
            case .failure:
                e.fulfill()
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }
}

// MARK: - Mocks

class MockWidgetClient: PubSubWidgetClient {
    func unsubscribe(fromChannel channel: String) {}

    var widgetListeners: ChannelListeners = ChannelListeners()
    func addListener(_ listener: WidgetProxyInput, toChannel channel: String) {
        widgetListeners.addListener(listener, forChannel: channel)
    }
    func removeListener(_ listener: WidgetProxyInput, fromChannel channel: String) {
        widgetListeners.removeListener(listener, forChannel: channel)
    }
    func removeAllListeners() {
        widgetListeners.removeAll()
    }
}

class MockWidgetInteractionRepository: WidgetInteractionRepository {
    func add(interactions: [WidgetInteraction]) { }

    func get(widgetID: String) -> [WidgetInteraction]? { return nil }

    func get(interactionsForTimeline timeline: WidgetTimelineResource, completion: @escaping (Result<[WidgetInteraction], Error>) -> Void) { }

    func promise(interactionsForTimeline timeline: WidgetTimelineResource) -> Promise<[WidgetInteraction]> { return Promise() }

    func get(widgetModel: WidgetModel, completion: @escaping (Result<[WidgetInteraction], Error>) -> Void) { }
}

class AlwaysSuccessProgramDetailVendor: ProgramDetailVendor {
    private let programDetails: ProgramDetailResource

    init(programDetails: ProgramDetailResource) {
        self.programDetails = programDetails
    }

    func getProgramDetails() -> Promise<ProgramDetailResource> {
        return Promise(value: self.programDetails)
    }
}

class AlwaysSuccessLiveLikeID: LiveLikeIDVendor {
    var whenLiveLikeID: Promise<LiveLikeID> = Promise(value: LiveLikeID(from: ""))
}

class AlwaysSuccessAccessTokenVendor: AccessTokenVendor {
    var whenAccessToken = Promise<AccessToken>(value: AccessToken(fromString: "test-access-token"))
}

class MockError: Error { }

class AlwaysSuccessNickname: UserNicknameService {
    var whenInitialNickname: Promise<Void> = Promise(value: ())

    var currentNickname: String?

    var nicknameDidChange: [(String) -> Void] = []

    @discardableResult
    func setNickname(nickname: String) -> Promise<String> {
        currentNickname = nickname
        return Promise(value: nickname)
    }
}

class SessionSpy: ContentSessionDelegate {
    func contentSession(_ session: ContentSession, didReceiveWidget widget: WidgetModel) {

    }

    func widget(_ session: ContentSession, didBecomeReady widget: Widget) {

    }

    func playheadTimeSource(_ session: ContentSession) -> Date? {
        return Date()
    }

    func chat(session: ContentSession, roomID: String, newMessage message: ChatMessage) {

    }

    func widget(_ session: ContentSession, didBecomeReady jsonObject: Any) {

    }

    var errorExpection: XCTestExpectation?
    var error: Error?

    func session(_ session: ContentSession, didChangeStatus status: SessionStatus) {}

    func session(_ session: ContentSession, didReceiveError error: Error) {
        guard let expectation = errorExpection else {
            XCTFail("Content Session was not setup correctly. Missing XCTExpectation reference")
            return
        }
        self.error = error
        expectation.fulfill()
    }
}
