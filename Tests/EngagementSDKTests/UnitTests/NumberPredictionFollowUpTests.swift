//
//  NumberPredictionFollowUpTests.swift
//  EngagementSDKTests
//
//  Created by Jelzon Monzon on 9/15/21.
//

@testable import EngagementSDK
@testable import LiveLikeSwift
import XCTest

class NumberPredictionFollowUpTests: XCTestCase {
    func test_model_data_matches_resource() {
        let resource = NumberPredictionFollowUpResourceBuilder().build()
        let model: NumberPredictionFollowUpWidgetModel = {
            let builder = NumberPredictionFollowUpModelBuilder()
            builder.resource = resource
            return builder.build()
        }()
        
        test_widget_metadata_copied_from_resource(
            metadata: resource,
            model: model
        )
        XCTAssert(resource.options == model.options)
        XCTAssert(resource.question == model.question)
    }
    
    func test_claimRewards_fails_when_no_interaction_found() {
        let e = expectation(description: "claim rewards failed")
        let model: NumberPredictionFollowUpWidgetModel = {
            let builder = NumberPredictionFollowUpModelBuilder()
            builder.widgetInteractionRepo = {
                let repo = ServerWidgetInteractionRepository(
                    livelikeAPI: MockLiveLikeRestAPIServices(),
                    userProfile: MockUserProfile(),
                    cache: {
                        let cache = WidgetInteractionCache()
                        cache.widgetInteractionsByWidgetID[builder.resource.id] = nil
                        return cache
                    }()
                )
                return repo
            }()
            return builder.build()
        }()
        
        model.claimRewards { result in
            switch result {
            case .failure:
                e.fulfill()
            case .success:
                XCTFail("Expected to fail")
            }
        }
        
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    func test_claimRewards_succeeds_when_interaction_found() {
        let e = expectation(description: "claim rewards succeeds")
        let model: NumberPredictionFollowUpWidgetModel = {
            let builder = NumberPredictionFollowUpModelBuilder()
            builder.widgetInteractionRepo = {
                let repo = ServerWidgetInteractionRepository(
                    livelikeAPI: MockLiveLikeRestAPIServices(),
                    userProfile: MockUserProfile(),
                    cache: {
                        let cache = WidgetInteractionCache()
                        cache.widgetInteractionsByWidgetID[builder.resource.predictionID] = [
                            WidgetInteraction.numberPredictionVote(
                                NumberPredictionVote(
                                    widgetID: builder.resource.predictionID,
                                    widgetKind: builder.resource.predictionKind,
                                    votes: [],
                                    claimToken: "claimToken"
                                )
                            )
                        ]
                        return cache
                    }()
                )
                return repo
            }()
            return builder.build()
        }()
        
        model.claimRewards { result in
            switch result {
            case .failure(let error):
                XCTFail(error.localizedDescription)
            case .success:
                e.fulfill()
            }
        }
        
        waitForExpectations(timeout: 1.0, handler: nil)
    }
}

class NumberPredictionFollowUpModelBuilder {
    
    var resource: NumberPredictionFollowUp
    var eventRecorder: EventRecorder
    var widgetAPI: LiveLikeWidgetAPIProtocol
    var leaderboardAPI: LiveLikeLeaderboardAPIProtocol
    var userProfile: UserProfileProtocol
    var widgetInteractionRepo: WidgetInteractionRepository
    var leaderboardsManager: LeaderboardsManager
    var rewardItems: [RewardItem]
    
    init() {
        self.resource = NumberPredictionFollowUpResourceBuilder().build()
        self.eventRecorder = MockEventRecorder()
        self.widgetAPI = MockLiveLikeRestAPIServices()
        self.leaderboardAPI = MockLiveLikeRestAPIServices()
        self.userProfile = MockUserProfile()
        self.widgetInteractionRepo = MockWidgetInteractionRepository()
        self.leaderboardsManager = LeaderboardsManager()
        self.rewardItems = []
    }
    
    func build() -> NumberPredictionFollowUpWidgetModel {
        return InternalNumberPredictionFollowUpWidgetModel(
            resource: resource,
            eventRecorder: eventRecorder,
            widgetAPI: widgetAPI,
            leaderboardAPI: leaderboardAPI,
            userProfile: userProfile,
            widgetInteractionRepo: widgetInteractionRepo,
            leaderboardsManager: leaderboardsManager,
            rewardItems: rewardItems
        )
    }
}

class NumberPredictionFollowUpResourceBuilder {
    
    var baseData: BaseWidgetResource
    var title: String
    var question: String
    var options: [NumberPredictionOption]
    var voteURL: URL
    var widgetInteractionsURLTemplate: String
    var predictionID: String
    var claimURL: URL
    var status: WidgetStatus
    var predictionKind: WidgetKind
    
    init() {
        baseData = WidgetMetadataResourceBuilder().build()
        title = "title"
        question = "question"
        options = [
            .init(id: "ex-1", imageURL: nil, text: "text 1", correctNumber: nil, url: MockObjects.mockURL),
            .init(id: "ex-2", imageURL: nil, text: "text 2", correctNumber: nil, url: MockObjects.mockURL)
        ]
        voteURL = MockObjects.mockURL
        self.widgetInteractionsURLTemplate = "widgetInteractionsURLTemplate"
        self.predictionID = "predictionID"
        self.claimURL = MockObjects.mockURL
        self.status = .pending
        predictionKind = .textNumberPrediction
    }
    
    func build() -> NumberPredictionFollowUp {
        return NumberPredictionFollowUp(
            baseData: baseData,
            question: question,
            options: options,
            widgetInteractionsURLTemplate: widgetInteractionsURLTemplate,
            predictionID: predictionID,
            predictionKind: predictionKind,
            claimURL: claimURL,
            status: .pending
        )
    }
    
}
