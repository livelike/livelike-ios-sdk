//
//  WidgetInteractionRepositoryTests.swift
//  EngagementSDKTests
//
//  Created by Jelzon Monzon on 6/4/21.
//

@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift
import XCTest
import Foundation

class WidgetInteractionRepositoryTests: XCTestCase {

    /// Tests that getPollInterations is not called when there are interactions in cache
    func test_get_poll_interactions_cache_hit() {
        let api = MockLiveLikeRestAPIServices()
        api.getTimelineInteractionsCompletion = { _, _ in
            XCTFail("API should not be called if cache hit")
            return Promise()
        }
        let userProfile = MockUserProfile()
        let cache = WidgetInteractionCache()
        let widgetID = "widget-id-1"
        cache.widgetInteractionsByWidgetID[widgetID] = [.pollVote(PollWidgetModel.Vote(id: "", widgetID: "", url: nil, optionID: "", rewards: [], createdAt: Date(), widgetKind: .textPoll))]
        let repo = ServerWidgetInteractionRepository(
            livelikeAPI: api,
            userProfile: userProfile,
            cache: cache
        )
        let textPollCreated: TextPollCreated = {
            let builder = TextPollCreatedBuilder()
            builder.baseData = {
                let builder = WidgetMetadataResourceBuilder()
                builder.id = widgetID
                return builder.build()
            }()
            return builder.build()
        }()
        let model = PollModelBuilder().build(from: textPollCreated)
        let e = expectation(description: "")
        repo.get(widgetModel: .poll(model)) { result in
            switch result {
            case .failure(let error):
                XCTFail(error.localizedDescription)
            case .success:
                e.fulfill()
            }
        }
        waitForExpectations(timeout: 1.0, handler: nil)
    }

    /// Tests that the getPollInteractions api is called if interations don't exist in cache
    func test_get_poll_interactions_cache_miss() {
        let apiExpectation = expectation(description: "")
        let successExpectation = expectation(description: "")
        let api = MockLiveLikeRestAPIServices()
        api.getTimelineInteractionsCompletion = { _, _ in
            apiExpectation.fulfill()
            let interactions: [WidgetInteraction] = [.pollVote(.init(id: "", widgetID: "", url: nil, optionID: "", rewards: [], createdAt: Date(), widgetKind: .textPoll))]
            return Promise(value: .init(interactionsByKind: [.textPoll: interactions]))
        }
        let userProfile = MockUserProfile()
        let cache = WidgetInteractionCache()
        let repo = ServerWidgetInteractionRepository(
            livelikeAPI: api,
            userProfile: userProfile,
            cache: cache
        )
        let textPollCreated: TextPollCreated = {
            let builder = TextPollCreatedBuilder()
            return builder.build()
        }()
        let model = PollModelBuilder().build(from: textPollCreated)
        repo.get(widgetModel: .poll(model)) { result in
            switch result {
            case .failure(let error):
                XCTFail(error.localizedDescription)
            case .success:
                successExpectation.fulfill()
            }
        }
        waitForExpectations(timeout: 3.0, handler: nil)
    }

    func test_repo_write_when_text_ask_reply_submitted() {
        let api = MockLiveLikeRestAPIServices()
        let userProfile = MockUserProfile()
        let cache = WidgetInteractionCache()
        let repo = ServerWidgetInteractionRepository(
            livelikeAPI: api,
            userProfile: userProfile,
            cache: cache
        )
        let model: TextAskWidgetModel = {
            let builder = TextAskWidgetModelBuilder()
            builder.widgetAPI = {
                let widgetAPI = MockLiveLikeRestAPIServices()
                widgetAPI.createTextAskReplyCompletion = { _, reply, _ in
                    let reply = TextAskWidgetModel.Reply(
                        id: "id-1",
                        widgetID: "widgetID-1",
                        widgetKind: .textAsk,
                        createdAt: Date(),
                        text: reply
                    )
                    return .init(value: reply)
                }
                return widgetAPI
            }()
            builder.widgetInteractionRepo = repo
            return builder.build()
        }()

        let e = expectation(description: "Submit reply completed")

        model.submitReply("reply-1") {
            switch $0 {
            case .failure(let error):
                XCTFail(error.localizedDescription)
            case .success(let reply):
                let interaction = cache.widgetInteractionsByWidgetID[reply.widgetID]?.first
                switch interaction {
                case .textAskReply(let replyInteraction):
                    XCTAssert(replyInteraction.id == reply.id)
                    XCTAssert(replyInteraction.text == reply.text)
                default:
                    XCTFail("Expected textAskReply for widgetID \(reply.widgetID)")
                }

                e.fulfill()
            }
        }

        waitForExpectations(timeout: 1.0, handler: nil)
    }
}
