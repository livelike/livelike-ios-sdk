//
//  QuizWidgetTests.swift
//  LiveLikeSDK
//
//  Created by jelzon on 3/21/19.
//

@testable import EngagementSDK
@testable import LiveLikeSwift
import Foundation
import XCTest

class QuizWidgetTests: XCTestCase {
    override func setUp() {}

    func test_text_quiz_created_json_decode() {
        do {
            let _: TextQuizCreated = try Helper.jsonDecode(filename: "TextQuizCreated")
        } catch {
            XCTFail("\(error)")
        }
    }

    func test_image_quiz_created_json_decode() {
        do {
            let _: ImageQuizCreated = try Helper.jsonDecode(filename: "ImageQuizCreated_Four")
        } catch {
            XCTFail("\(error)")
        }
    }
}
