//
//  SocialGraphClientTests.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 2/23/23.
//

import XCTest
@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift

class SocialGraphClientTests: XCTestCase {

    enum socialGraphClientError: LocalizedError {
        case failure
    }

    let profileRelationshipJSON: String =
        """
        {
            "id": "134154a9-b23e-42a0-a6ff-50543a81a238",
            "url": "https://cf-blast-qa.livelikecdn.com/api/v1/profile-relationships/134154a9-b23e-42a0-a6ff-50543a81a238/",
            "created_at": "2023-02-16T15:56:19.905091Z",
            "relationship_type": {
                "id": "90771f95-d866-4271-aa2f-36f1bc1503f8",
                "url": "https://cf-blast-qa.livelikecdn.com/api/v1/profile-relationship-types/90771f95-d866-4271-aa2f-36f1bc1503f8/",
                "created_at": "2023-02-16T15:50:26Z",
                "client_id": "YB83JIrWB1ysPLWRa9K3U5zU3DCZoZaEpvO7mjVa",
                "name": "Follow",
                "key": "follow"
            },
            "from_profile": {
                "id": "5778683f-b391-4872-93fc-988463920174",
                "url": "https://cf-blast-qa.livelikecdn.com/api/v1/profiles/5778683f-b391-4872-93fc-988463920174/",
                "created_at": "2022-11-04T17:49:46Z",
                "nickname": "Giant Rider",
                "custom_id": "1",
                "custom_data": "",
                "points": 0,
                "badges": [],
                "badges_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profiles/5778683f-b391-4872-93fc-988463920174/badges/",
                "badge_progress_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profiles/5778683f-b391-4872-93fc-988463920174/badge-progress/",
                "subscribe_channel": "profile.5778683f-b391-4872-93fc-988463920174",
                "reported_count": 0,
                "chat_room_memberships_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profiles/5778683f-b391-4872-93fc-988463920174/chat-room-memberships/",
                "reward_item_credit_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profiles/5778683f-b391-4872-93fc-988463920174/reward-item-credits/",
                "reward_item_debit_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profiles/5778683f-b391-4872-93fc-988463920174/reward-item-debits/",
                "reward_item_balances_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profiles/5778683f-b391-4872-93fc-988463920174/reward-item-balances/",
                "reward_item_transfer_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profiles/5778683f-b391-4872-93fc-988463920174/reward-item-transfers/",
                "block_profile_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profile-blocks/",
                "blocked_profiles_template_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profile-blocks/?blocked_profile_id={blocked_profile_id}",
                "blocked_profile_ids_url": "https://cf-blast-qa.livelikecdn.com/api/v1/blocked-profile-ids/",
                "leaderboards_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profiles/5778683f-b391-4872-93fc-988463920174/leaderboards/",
                "leaderboard_views_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profiles/5778683f-b391-4872-93fc-988463920174/leaderboard-views/",
                "earned_badges_url": "https://cf-blast-qa.livelikecdn.com/api/v1/earned-badges/?profile_id=5778683f-b391-4872-93fc-988463920174"
            },
            "to_profile": {
                "id": "0a5fe42b-3028-4f59-810e-f3905c97f345",
                "url": "https://cf-blast-qa.livelikecdn.com/api/v1/profiles/0a5fe42b-3028-4f59-810e-f3905c97f345/",
                "created_at": "2021-06-10T11:11:51Z",
                "nickname": "Nimble Hawk",
                "custom_id": null,
                "custom_data": "",
                "points": 0,
                "badges": [],
                "badges_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profiles/0a5fe42b-3028-4f59-810e-f3905c97f345/badges/",
                "badge_progress_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profiles/0a5fe42b-3028-4f59-810e-f3905c97f345/badge-progress/",
                "subscribe_channel": "profile.0a5fe42b-3028-4f59-810e-f3905c97f345",
                "reported_count": 0,
                "chat_room_memberships_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profiles/0a5fe42b-3028-4f59-810e-f3905c97f345/chat-room-memberships/",
                "reward_item_credit_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profiles/0a5fe42b-3028-4f59-810e-f3905c97f345/reward-item-credits/",
                "reward_item_debit_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profiles/0a5fe42b-3028-4f59-810e-f3905c97f345/reward-item-debits/",
                "reward_item_balances_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profiles/0a5fe42b-3028-4f59-810e-f3905c97f345/reward-item-balances/",
                "reward_item_transfer_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profiles/0a5fe42b-3028-4f59-810e-f3905c97f345/reward-item-transfers/",
                "block_profile_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profile-blocks/",
                "blocked_profiles_template_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profile-blocks/?blocked_profile_id={blocked_profile_id}",
                "blocked_profile_ids_url": "https://cf-blast-qa.livelikecdn.com/api/v1/blocked-profile-ids/",
                "leaderboards_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profiles/0a5fe42b-3028-4f59-810e-f3905c97f345/leaderboards/",
                "leaderboard_views_url": "https://cf-blast-qa.livelikecdn.com/api/v1/profiles/0a5fe42b-3028-4f59-810e-f3905c97f345/leaderboard-views/",
                "earned_badges_url": "https://cf-blast-qa.livelikecdn.com/api/v1/earned-badges/?profile_id=0a5fe42b-3028-4f59-810e-f3905c97f345"
            }
        }
        """

    let profileRelationshipTypeJSON: String =
        """
        {
            "id": "90771f95-d866-4271-aa2f-36f1bc1503f8",
            "url": "https://cf-blast-qa.livelikecdn.com/api/v1/profile-relationship-types/90771f95-d866-4271-aa2f-36f1bc1503f8/",
            "created_at": "2023-02-16T15:50:26Z",
            "client_id": "YB83JIrWB1ysPLWRa9K3U5zU3DCZoZaEpvO7mjVa",
            "name": "Follow",
            "key": "follow"
        }
        """

    let profileRelationshipsResources = [
        MockObjects.mockProfileRelationship,
        MockObjects.mockProfileRelationship,
        MockObjects.mockProfileRelationship
    ]

    let profileRelationshipTypeResources = [
        MockObjects.mockProfileRelationshipType,
        MockObjects.mockProfileRelationshipType
    ]


    let decoder = LLJSONDecoder()

    // MARK: - JSON -> Resource conversion tests
    func test_profile_relationship_json_to_resource_conversion() {
        let jsonData = profileRelationshipJSON.data(using: .utf8)!
        do {
            _ = try decoder.decode(ProfileRelationship.self, from: jsonData)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    func test_profile_relationship_type_json_to_resource_conversion() {
        let jsonData = profileRelationshipTypeJSON.data(using: .utf8)!
        do {
            _ = try decoder.decode(ProfileRelationshipType.self, from: jsonData)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    // MARK: - getProfileRelationships() Tests
    func test_get_profile_relationships_success() {
        let e = expectation(description: "Successfully retrieved a all profile relationships")
        let networking = StubNetworking(result: .success(
            PaginatedResult<ProfileRelationship>(
                previous: nil,
                count: profileRelationshipsResources.count,
                next: nil,
                items: profileRelationshipsResources
            )
        ))

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockSocialGraphClient = InternalSocialGraphClient(
                coreAPI: builder.mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                userProfileVendor: builder.userProfileVendor,
                networking: networking
            )

            return builder.build()
        }()

        sdk.socialGraphClient.getProfileRelationships(
            page: .first,
            options: nil
        ) { result in
            switch result {
            case .success(let result):
                if result.count == 3 {
                    e.fulfill()
                } else {
                    XCTFail("Failed because the amount of profile relationships do not match")
                }
            case .failure(let error):
                XCTFail("Failed retrieving all rewards from the Rewards Client - \(error)")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_get_profile_relationships_fail() {
        let e = expectation(description: "Successfully failed at retrieving all profile relationships")
        let networking = StubNetworking<PaginatedResult<ProfileRelationship>>(result: .failure(socialGraphClientError.failure))

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockSocialGraphClient = InternalSocialGraphClient(
                coreAPI: builder.mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                userProfileVendor: builder.userProfileVendor,
                networking: networking
            )

            return builder.build()
        }()

        sdk.socialGraphClient.getProfileRelationships(
            page: .first,
            options: nil
        ) { result in
            switch result {
            case .success(_):
                XCTFail("Failed at failing when there is an error")
            case .failure(_):
                e.fulfill()
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    // MARK: - getProfileRelationshipTypes() Tests
    func test_get_profile_relationship_types_success() {
        let e = expectation(description: "Successfully retrieved all profile relationship types")
        let networking = StubNetworking(result: .success(
            PaginatedResult<ProfileRelationshipType>(
                previous: nil,
                count: profileRelationshipTypeResources.count,
                next: nil,
                items: profileRelationshipTypeResources
            )
        ))

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockSocialGraphClient = InternalSocialGraphClient(
                coreAPI: builder.mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                userProfileVendor: builder.userProfileVendor,
                networking: networking
            )

            return builder.build()
        }()

        sdk.socialGraphClient.getProfileRelationshipTypes(
            page: .first,
            options: nil
        ) { result in
            switch result {
            case .success(let result):
                if result.count == 2 {
                    e.fulfill()
                } else {
                    XCTFail("Failed because the amount of profile relationships do not match")
                }
            case .failure(let error):
                XCTFail("Failed retrieving all rewards from the Rewards Client - \(error)")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_get_profile_relationship_types_fail() {
        let e = expectation(description: "Successfully failed at retrieving all profile relationship types")
        let networking = StubNetworking<PaginatedResult<ProfileRelationshipType>>(result: .failure(socialGraphClientError.failure))

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockSocialGraphClient = InternalSocialGraphClient(
                coreAPI: builder.mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                userProfileVendor: builder.userProfileVendor,
                networking: networking
            )

            return builder.build()
        }()

        sdk.socialGraphClient.getProfileRelationshipTypes(
            page: .first,
            options: nil
        ) { result in
            switch result {
            case .success(_):
                XCTFail("Failed at failing when there is an error")
            case .failure(_):
                e.fulfill()
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }
}
