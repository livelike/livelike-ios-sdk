//
//  ImagePollTests.swift
//  LikeLiveTests
//
//  Created by jelzon on 3/14/19.
//

@testable import EngagementSDK
@testable import LiveLikeSwift
import Foundation
import XCTest

class ImagePollTests: XCTestCase {
    override func setUp() {}

    func test_image_poll_created_json_decode() {
        do {
            let _: ImagePollCreated = try Helper.jsonDecode(filename: "ImagePollCreated")
        } catch {
            XCTFail("\(error)")
        }
    }

    func test_text_poll_created_json_decode() {
        do {
            let _: TextPollCreated = try Helper.jsonDecode(filename: "TextPollCreated")
        } catch {
            XCTFail("\(error)")
        }
    }
}
