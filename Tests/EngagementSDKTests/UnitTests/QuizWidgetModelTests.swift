//
//  QuizWidgetModelTests.swift
//  EngagementSDKTests
//
//  Created by Jelzon Monzon on 7/2/20.
//

@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift
import XCTest

class QuizWidgetModelTests: XCTestCase {

    func test_model_data_matches_text_quiz_resource() {
        let textQuizResource: TextQuizCreated = {
            let builder = TextQuizBuilder()
            return builder.build()
        }()

        let model: QuizWidgetModel = {
            let builder = QuizModelBuilder()
            return builder.build(from: textQuizResource)
        }()

        XCTAssert(model.id == textQuizResource.id)
        XCTAssert(model.question == textQuizResource.question)
        XCTAssert(model.choices.count == textQuizResource.choices.count)
        model.choices.enumerated().forEach { index, choice in
            XCTAssert(choice.id == textQuizResource.choices[index].id)
            XCTAssert(choice.text == textQuizResource.choices[index].description)
            XCTAssert(choice.imageURL == nil)
            XCTAssert(choice.isCorrect == textQuizResource.choices[index].isCorrect)
            XCTAssert(choice.answerCount == textQuizResource.choices[index].answerCount)
            XCTAssert(choice.answerURL == textQuizResource.choices[index].answerUrl)
        }
        XCTAssert(model.customData == textQuizResource.customData)
        XCTAssert(model.containsImages == false)
        XCTAssert(model.kind == textQuizResource.kind)
        XCTAssert(model.publishedAt == textQuizResource.publishedAt)
        XCTAssert(model.createdAt == textQuizResource.createdAt)

        model.sponsors.enumerated().forEach { index, sponsor in
            XCTAssert(sponsor.id == textQuizResource.sponsors[index].id)
            XCTAssert(sponsor.clientID == textQuizResource.sponsors[index].clientID)
            XCTAssert(sponsor.name == textQuizResource.sponsors[index].name)
            XCTAssert(sponsor.logoURL == textQuizResource.sponsors[index].logoURL)
        }
    }

    func test_model_data_matches_image_quiz_resource() {
        let textQuizResource: ImageQuizCreated = {
            let builder = ImageQuizBuilder()
            return builder.build()
        }()

        let model: QuizWidgetModel = {
            let builder = QuizModelBuilder()
            return builder.build(from: textQuizResource)
        }()

        XCTAssert(model.id == textQuizResource.id)
        XCTAssert(model.question == textQuizResource.question)
        XCTAssert(model.choices.count == textQuizResource.choices.count)
        model.choices.enumerated().forEach { index, choice in
            XCTAssert(choice.id == textQuizResource.choices[index].id)
            XCTAssert(choice.text == textQuizResource.choices[index].description)
            XCTAssert(choice.imageURL == textQuizResource.choices[index].imageUrl)
            XCTAssert(choice.isCorrect == textQuizResource.choices[index].isCorrect)
            XCTAssert(choice.answerCount == textQuizResource.choices[index].answerCount)
            XCTAssert(choice.answerURL == textQuizResource.choices[index].answerUrl)
        }
        XCTAssert(model.customData == textQuizResource.customData)
        XCTAssert(model.containsImages == true)
        XCTAssert(model.kind == textQuizResource.kind)
        XCTAssert(model.publishedAt == textQuizResource.publishedAt)
        XCTAssert(model.createdAt == textQuizResource.createdAt)

        model.sponsors.enumerated().forEach { index, sponsor in
            XCTAssert(sponsor.id == textQuizResource.sponsors[index].id)
            XCTAssert(sponsor.clientID == textQuizResource.sponsors[index].clientID)
            XCTAssert(sponsor.name == textQuizResource.sponsors[index].name)
            XCTAssert(sponsor.logoURL == textQuizResource.sponsors[index].logoURL)
        }
    }

    func test_registerImpression_makes_livelikeAPI_request() {
        let e = expectation(description: "createImpression called")

        let livelikeAPI = MockLiveLikeRestAPIServices()
        livelikeAPI.createImpressionCompletion = { _, _, _ in
            e.fulfill()
            return Promise()
        }

        let resource = TextQuizBuilder().build()
        let model: QuizWidgetModel = {
            let builder = QuizModelBuilder()
            builder.livelikeAPI = livelikeAPI
            return builder.build(from: resource)
        }()

        model.registerImpression()

        waitForExpectations(timeout: 1.0, handler: nil)
    }

    func test_concurent_lockInAnswer_calls_raises_error() {
        let e = expectation(description: "Error raised")

        let resource = TextQuizBuilder().build()

        let model = QuizModelBuilder().build(from: resource)

        model.lockInAnswer(choiceID: resource.choices[0].id, completion: { _ in })
        model.lockInAnswer(choiceID: resource.choices[0].id) { result in
            switch result {
            case .success:
                XCTFail("Expected failure")
            case .failure(let error):
                switch error {
                case QuizWidgetModel.QuizWidgetModelError.concurrentLockInAnswer:
                    e.fulfill()
                default:
                    XCTFail("unexpected error")
                }
            }
        }

        waitForExpectations(timeout: 1.0, handler: nil)
    }

}

class QuizModelBuilder {

    var eventRecorder: EventRecorder = MockEventRecorder()
    var widgetClient: PubSubWidgetClient = MockWidgetClient()
    var userProfile: UserProfileProtocol = MockUserProfile()
    var rewardItems: [RewardItem] = []
    var leaderboardsManager: LeaderboardsManager = LeaderboardsManager()
    var livelikeAPI: MockLiveLikeRestAPIServices = MockLiveLikeRestAPIServices()
    var widgetInteractionRepo: WidgetInteractionRepository = MockWidgetInteractionRepository()

    func build(from resource: TextQuizCreated) -> QuizWidgetModel {
        let model = QuizWidgetModel(
            data: resource,
            eventRecorder: eventRecorder,
            userProfile: userProfile,
            rewardItems: rewardItems,
            leaderboardsManager: leaderboardsManager,
            widgetClient: widgetClient,
            livelikeAPI: livelikeAPI,
            widgetInteractionRepo: widgetInteractionRepo
        )
        return model
    }

    func build(from resource: ImageQuizCreated) -> QuizWidgetModel {
        let model = QuizWidgetModel(
            data: resource,
            eventRecorder: eventRecorder,
            userProfile: userProfile,
            rewardItems: rewardItems,
            leaderboardsManager: leaderboardsManager,
            widgetClient: widgetClient,
            livelikeAPI: livelikeAPI,
            widgetInteractionRepo: widgetInteractionRepo
        )
        return model
    }
}

class QuizBuilder {
    var baseData: BaseWidgetResource = WidgetMetadataResourceBuilder().build()
    var question: String = UUID().uuidString
    var rewardsUrl: URL? = MockObjects.mockURL
}

class ImageQuizBuilder: QuizBuilder {
    var choices: [ImageQuizChoice] = [
        .init(id: UUID().uuidString, description: UUID().uuidString, imageUrl: MockObjects.mockURL, isCorrect: true, answerCount: 0, answerUrl: MockObjects.mockURL),
        .init(id: UUID().uuidString, description: UUID().uuidString, imageUrl: MockObjects.mockURL, isCorrect: false, answerCount: 0, answerUrl: MockObjects.mockURL)
    ]

    func build() -> ImageQuizCreated {
        return ImageQuizCreated(
            baseData: WidgetMetadataResourceBuilder().build(),
            question: question,
            choices: choices,
            rewardsUrl: rewardsUrl,
            widgetInteractionsUrlTemplate: "",
            sponsors: [MockObjects.sponsorResource]
        )
    }
}

class TextQuizBuilder: QuizBuilder {
    var choices: [TextQuizChoice] = [
        .init(id: UUID().uuidString, description: UUID().uuidString, isCorrect: true, answerCount: 0, answerUrl: MockObjects.mockURL),
        .init(id: UUID().uuidString, description: UUID().uuidString, isCorrect: false, answerCount: 0, answerUrl: MockObjects.mockURL)
    ]

    func build() -> TextQuizCreated {
        return TextQuizCreated(
            baseData: baseData,
            question: question,
            choices: choices,
            rewardsUrl: rewardsUrl,
            widgetInteractionsUrlTemplate: "",
            sponsors: [MockObjects.sponsorResource]
        )
    }
}
