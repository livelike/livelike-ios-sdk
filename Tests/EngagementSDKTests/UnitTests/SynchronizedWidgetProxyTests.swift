//
//  SynchronizedWidgettProxyTests.swift
//  LiveLikeTests
//
//  Created by Cory Sullivan on 2019-02-22.
//

@testable import EngagementSDK
@testable import LiveLikeSwift
import Foundation
import XCTest

// swiftlint:disable force_try

class SynchronizedWidgetProxyTests: XCTestCase {
    var proxyInputSpy: downStreamWidgetProxySpy!

    override func setUp() {
        proxyInputSpy = downStreamWidgetProxySpy()
    }

    func test_playhead_position_is_behind_widget_timestamp() {
        // Widget time is 1549627071
        let playerTimeSource = PlayerTimeSourceMock(playheadPosition: 1_549_627_068.0)
        playerTimeSource.start()
        let syncProxy = SynchronizedWidgetProxy(playerTimeSource: playerTimeSource.now)
        syncProxy.downStreamProxyInput = proxyInputSpy
        let messageExpectation = expectation(description: "downStreamChatProxySpy calls this when a message is ready to display")
        proxyInputSpy.messageReadyExpectation = messageExpectation
        syncProxy.publish(event: .init(clientEvent: .textPrediction()))

        waitForExpectations(timeout: 10.0) { error in
            if let error = error {
                XCTFail("Wait for expectation timeout error: \(error)")
                return
            }

            guard let result = self.proxyInputSpy.messageResult else {
                XCTFail("Expected clientSpy to set this value")
                return
            }
            XCTAssertEqual(result.minimumScheduledTime, playerTimeSource.playheadPosition)
        }
    }

    func test_playhead_position_is_a_head_of_widget_timestamp() {
        // Widget time is 1_549_627_070
        let playerTimeSource = PlayerTimeSourceMock(playheadPosition: 1_549_627_072.0)
        playerTimeSource.start()
        let syncProxy = SynchronizedWidgetProxy(playerTimeSource: playerTimeSource.now)
        syncProxy.downStreamProxyInput = proxyInputSpy
        let messageExpectation = expectation(description: "downStreamChatProxySpy calls this when a message is ready to display")
        proxyInputSpy.messageReadyExpectation = messageExpectation
        syncProxy.publish(event: .init(clientEvent: .textPrediction()))

        waitForExpectations(timeout: 10.0) { error in
            if let error = error {
                XCTFail("Wait for expectation timeout error: \(error)")
                return
            }

            guard self.proxyInputSpy.messageResult != nil else {
                XCTFail("Expected clientSpy to set this value")
                return
            }
            XCTAssertEqual(playerTimeSource.playheadPosition, 1_549_627_072.0)
        }
    }

    /**
     Test is disabled because threshold is currently set to max value and therefore
     impossible to exceed
     */
    func disabled_test_playhead_position_exceeds_delay_threshold() {
        // Widget time is 1_549_627_070
        let delayThreshold: TimeInterval = 10
        let extraTime: TimeInterval = 2
        let playHeadPosition: TimeInterval = 1_549_627_070
        let playerTimeSource = PlayerTimeSourceMock(playheadPosition: playHeadPosition + delayThreshold + extraTime)
        playerTimeSource.start()
        let syncProxy = SynchronizedWidgetProxy(playerTimeSource: playerTimeSource.now)
        syncProxy.downStreamProxyInput = proxyInputSpy
        let messageDiscardedExpection = expectation(description: "downStreamChatProxySpy calls this when a message is discarded")
        proxyInputSpy.messageDiscardedExpection = messageDiscardedExpection
        syncProxy.publish(event: .init(clientEvent: .textPrediction()))

        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("Wait for expectation timeout error: \(error)")
                return
            }
            XCTAssertEqual(self.proxyInputSpy.discardedReason, DiscardedReason.invalidPublishDate)
        }
    }

    func test_no_widget_time_stamp() {
        let playerTimeSource = PlayerTimeSourceMock(playheadPosition: 1_549_627_068.0)
        playerTimeSource.start()
        let syncProxy = SynchronizedWidgetProxy(playerTimeSource: playerTimeSource.now)
        syncProxy.downStreamProxyInput = proxyInputSpy
        let messageExpectation = expectation(description: "downStreamChatProxySpy calls this when a message is ready to display")
        proxyInputSpy.messageReadyExpectation = messageExpectation
        syncProxy.publish(event: .init(clientEvent: .textPredictionNoPDT()))

        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("Wait for expectation timeout error: \(error)")
                return
            }

            guard self.proxyInputSpy.messageResult != nil else {
                XCTFail("Expected clientSpy to set this value")
                return
            }
            XCTAssert((1_549_627_068.0 ... 1_549_627_069.0).contains(playerTimeSource.playheadPosition))
        }
    }

    func test_immediately_publish_when_no_time_source() {
        let syncProxy = SynchronizedWidgetProxy(playerTimeSource: nil)
        syncProxy.downStreamProxyInput = proxyInputSpy
        let messageExpectation = expectation(description: "downStreamChatProxySpy calls this when a message is ready to display")
        proxyInputSpy.messageReadyExpectation = messageExpectation
        syncProxy.publish(event: .init(clientEvent: .textPrediction()))
        waitForExpectations(timeout: 3) { error in
            if let error = error {
                XCTFail("Wait for expectation timeout error: \(error)")
                return
            }
        }
    }
}

extension ClientEvent {
    static func textPrediction() -> ClientEvent {
        let widgetDataInitial: TextPredictionCreated = try! Helper.jsonDecode(filename: "TextPredictionCreated")
        return ClientEvent.widget(.textPredictionCreated(widgetDataInitial))
    }

    static func textPredictionNoPDT() -> ClientEvent {
        let widgetDataInitial: TextPredictionCreated = try! Helper.jsonDecode(filename: "TextPredictionCreated-No-PDT")
        return ClientEvent.widget(.textPredictionCreated(widgetDataInitial))
    }
}
