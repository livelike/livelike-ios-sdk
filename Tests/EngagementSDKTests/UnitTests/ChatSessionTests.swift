//
//  ChatSessionTests.swift
//  EngagementSDKTests
//
//  Created by Jelzon Monzon on 5/13/20.
//

@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift
import XCTest

class MockChatSessionDelegate: ChatSessionDelegate {
    
    var didRecieveNewMessageCompletion: (_ chatSession: ChatSession, _ message: ChatMessage) -> Void = { _, _ in }
    
    var didRecieveRoomUpdateCompletion: (_ ChatSession: ChatSession, _ chatRoom: ChatRoomInfo) -> Void = { _, _ in }
    
    var didDeleteMessageCompletion: (_ chatSession: ChatSession, _ messageID: ChatMessageID) -> Void = { _, _ in }
    
    var didPinMessageCompletion: (_ ChatSession: ChatSession, _ message: PinMessageInfo) -> Void = { _, _ in }
    
    var didUnpinMessageCompletion: (_ ChatSession: ChatSession, _ pinMessageInfoID: String) -> Void = { _, _ in }
    
    func chatSession(_ chatSession: ChatSession, didRecieveMessageUpdate message: ChatMessage) { }
    
    func chatSession(
        _ chatSession: ChatSession,
        didRecieveNewMessage message: ChatMessage
    ) {
        didRecieveNewMessageCompletion(chatSession, message)
    }
    
    func chatSession(_ chatSession: ChatSession, didRecieveRoomUpdate chatRoom: ChatRoomInfo) {
        didRecieveRoomUpdateCompletion(chatSession, chatRoom)
    }
    
    func chatSession(_ chatSession: ChatSession, didPinMessage message: PinMessageInfo) {
        didPinMessageCompletion(chatSession, message)
    }
    
    func chatSession(_ chatSession: ChatSession, didUnpinMessage pinMessageInfoID: String) {
        didUnpinMessageCompletion(chatSession, pinMessageInfoID)
    }
    
    func chatSession(_ chatSession: ChatSession, didDeleteMessage messageID: ChatMessageID) {
        didDeleteMessageCompletion(chatSession, messageID)
    }
}

class ChatSessionTests: XCTestCase {
    lazy var currentUserID = "current_user_identifier"
    
    lazy var cleanMSG = ChatMessageResponse(
        id: "clean_message",
        clientMessageID: "11",
        roomID: "9012",
        senderID: currentUserID,
        senderNickname: "user",
        message: "normal message",
        videoTimestamp: nil,
        reactions: ReactionVotes(allVotes: []),
        timestamp: Date(),
        profileImageURL: nil,
        createdAt: TimeToken(date: Date()),
        bodyImageURL: nil,
        bodyImageHeight: nil,
        bodyImageWidth: nil,
        filteredMessage: nil,
        filteredReasons: Set<ChatFilter>(),
        customData: nil,
        quoteMessage: nil,
        messageEvent: .messageCreated,
        pubnubTimetoken: TimeToken(pubnubTimetoken: 0),
        senderProfileURL: nil,
        messageMetadata: nil,
        repliesCount: nil,
        repliesUrl: nil,
        parentMessageId: nil
    )
    
    lazy var nonCurrentUserShadowMutedMSG = ChatMessageResponse(
        id: "nonCurrentUserShadowMutedMSG",
        clientMessageID: "22",
        roomID: "9012",
        senderID: "101112",
        senderNickname: "user",
        message: "shadow muted message",
        videoTimestamp: nil,
        reactions: ReactionVotes(allVotes: []),
        timestamp: Date(),
        profileImageURL: nil,
        createdAt: TimeToken(date: Date()),
        bodyImageURL: nil,
        bodyImageHeight: nil,
        bodyImageWidth: nil,
        filteredMessage: nil,
        filteredReasons: [ChatFilter.filtered, ChatFilter.shadowMuted],
        customData: nil,
        quoteMessage: nil,
        messageEvent: .messageCreated,
        pubnubTimetoken: TimeToken(pubnubTimetoken: 0),
        senderProfileURL: nil,
        messageMetadata: nil,
        repliesCount: nil,
        repliesUrl: nil,
        parentMessageId: nil
    )
    
    lazy var currentUserShadowMutedMSG = ChatMessageResponse(
        id: "currentUserShadowMutedMSG",
        clientMessageID: "22",
        roomID: "9012",
        senderID: currentUserID,
        senderNickname: "user",
        message: "shadow muted message",
        videoTimestamp: nil,
        reactions: ReactionVotes(allVotes: []),
        timestamp: Date(),
        profileImageURL: nil,
        createdAt: TimeToken(date: Date()),
        bodyImageURL: nil,
        bodyImageHeight: nil,
        bodyImageWidth: nil,
        filteredMessage: nil,
        filteredReasons: [ChatFilter.filtered, ChatFilter.shadowMuted],
        customData: nil,
        quoteMessage: nil,
        messageEvent: .messageCreated,
        pubnubTimetoken: TimeToken(pubnubTimetoken: 0),
        senderProfileURL: nil,
        messageMetadata: nil,
        repliesCount: nil,
        repliesUrl: nil,
        parentMessageId: nil
    )
    
    lazy var currentUserContentFilteredMSG = ChatMessageResponse(
        id: "currentUserContentFilteredMSG",
        clientMessageID: "22",
        roomID: "9012",
        senderID: currentUserID,
        senderNickname: "user",
        message: "weed is legal",
        videoTimestamp: nil,
        reactions: ReactionVotes(allVotes: []),
        timestamp: Date(),
        profileImageURL: nil,
        createdAt: TimeToken(date: Date()),
        bodyImageURL: nil,
        bodyImageHeight: nil,
        bodyImageWidth: nil,
        filteredMessage: "*** is legal",
        filteredReasons: [ChatFilter.profanity, ChatFilter.filtered],
        customData: nil,
        quoteMessage: nil,
        messageEvent: .messageCreated,
        pubnubTimetoken: TimeToken(pubnubTimetoken: 0),
        senderProfileURL: nil,
        messageMetadata: nil,
        repliesCount: nil,
        repliesUrl: nil,
        parentMessageId: nil
    )
    
    lazy var nonUserContentFilteredMSG = ChatMessageResponse(
        id: "nonUserContentFilteredMSG",
        clientMessageID: "22",
        roomID: "9012",
        senderID: "23",
        senderNickname: "user",
        message: "weed is legal",
        videoTimestamp: nil,
        reactions: ReactionVotes(allVotes: []),
        timestamp: Date(),
        profileImageURL: nil,
        createdAt: TimeToken(date: Date()),
        bodyImageURL: nil,
        bodyImageHeight: nil,
        bodyImageWidth: nil,
        filteredMessage: "*** is legal",
        filteredReasons: [ChatFilter.profanity, ChatFilter.filtered],
        customData: nil,
        quoteMessage: nil,
        messageEvent: .messageCreated,
        pubnubTimetoken: TimeToken(pubnubTimetoken: 0),
        senderProfileURL: nil,
        messageMetadata: nil,
        repliesCount: nil,
        repliesUrl: nil,
        parentMessageId: nil
    )
    
    lazy var chatMessages = [
        cleanMSG,
        currentUserContentFilteredMSG,
        nonUserContentFilteredMSG,
        nonCurrentUserShadowMutedMSG,
        currentUserShadowMutedMSG
    ]
    lazy var chatMessagesPromise = Promise(
        value: GetMessagesResult(
            results: chatMessages,
            previous: MockObjects.mockURL
        )
    )
    
    func test_message_count() {
        let e = expectation(description: "message count request finished.")
        
        let chatSession: ChatSession = {
            let builder = PubSubChatSessionBuilder()
            return builder.build()
        }()
        
        chatSession.getMessageCount(since: TimeToken(date: .distantPast)) { result in
            switch result {
            case .success(let count):
                print(count)
                XCTAssert(count == 20)
                e.fulfill()
            case .failure(let error):
                print(error)
                XCTFail(error.localizedDescription)
            }
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_filtered_message_not_included_in_getMessageCount() {
        let e = expectation(description: "message count request finished.")
        
        let chatSession: ChatSession = {
            let builder = PubSubChatSessionBuilder()
            return builder.build()
        }()
        
        chatSession.getMessageCount(since: TimeToken(date: .distantPast)) { result in
            switch result {
            case .success(let count):
                print(count)
                XCTAssert(count == 20)
                e.fulfill()
            case .failure(let error):
                print(error)
                XCTFail(error.localizedDescription)
            }
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    // MARK: - getMessages() tests
    func test_filtered_message_not_included_in_getMessages() {
        let e = expectation(description: "message history request finished.")
        
        let chatSession: ChatSession = {
            let builder = PubSubChatSessionBuilder()
            builder.networking = StubNetworking(
                result: .success(
                    GetMessagesResult(
                        results: chatMessages,
                        previous: MockObjects.mockURL
                    )
                )
            )
            return builder.build()
        }()
        
        chatSession.getMessages(since: TimeToken(date: .distantPast)) { result in
            switch result {
            case .success(let messages):
                XCTAssertTrue(messages.contains(ChatMessage(chatMessageResponse: self.cleanMSG)))
                XCTAssertTrue(!messages.contains(ChatMessage(chatMessageResponse: self.currentUserContentFilteredMSG)))
                XCTAssertTrue(!messages.contains(ChatMessage(chatMessageResponse: self.nonUserContentFilteredMSG)))
                XCTAssertTrue(chatSession.containsFilteredMessages == false)
                e.fulfill()

            case .failure(let error):
                print(error)
                XCTFail(error.localizedDescription)
            }
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_filtered_message_not_included_in_getMessages_with_include_filtered_messages_true() {
        let e = expectation(description: "message history request finished.")
        
        let chatSession: ChatSession = {
            let builder = PubSubChatSessionBuilder()
            builder.networking = StubNetworking(
                result: .success(
                    GetMessagesResult(
                        results: chatMessages,
                        previous: MockObjects.mockURL
                    )
                )
            )
            builder.includeFilteredMessages = true
            return builder.build()
        }()
        
        chatSession.getMessages(since: TimeToken(date: .distantPast)) { result in
            switch result {
            case .success(let messages):
                XCTAssertTrue(messages.contains(ChatMessage(chatMessageResponse: self.cleanMSG)))
                XCTAssertTrue(messages.contains(ChatMessage(chatMessageResponse: self.currentUserContentFilteredMSG)))
                XCTAssertTrue(messages.contains(ChatMessage(chatMessageResponse: self.nonUserContentFilteredMSG)))
                XCTAssertTrue(chatSession.containsFilteredMessages == true)
                e.fulfill()
            case .failure(let error):
                print(error)
                XCTFail(error.localizedDescription)
            }
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_current_user_filtered_message_included_in_getMessages() {
        let e = expectation(description: "message history request finished.")
        
        let chatSession: ChatSession = {
            
            let builder = PubSubChatSessionBuilder()
            builder.networking = StubNetworking(
                result: .success(
                    GetMessagesResult(
                        results: chatMessages,
                        previous: MockObjects.mockURL
                    )
                )
            )
            builder.currentUserID = currentUserID
            
            return builder.build()
        }()
        
        chatSession.getMessages(since: TimeToken(date: .distantPast)) { result in
            switch result {
            case .success(let messages):
                XCTAssertTrue(messages.contains(ChatMessage(chatMessageResponse: self.cleanMSG)))
                XCTAssertTrue(messages.contains(ChatMessage(chatMessageResponse: self.currentUserContentFilteredMSG)))
                XCTAssertTrue(!messages.contains(ChatMessage(chatMessageResponse: self.nonUserContentFilteredMSG)))
                XCTAssertTrue(chatSession.containsFilteredMessages == false)
                e.fulfill()
                
            case .failure(let error):
                print(error)
                XCTFail(error.localizedDescription)
            }
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    // MARK: - loadInitialHistory() tests
    func test_filtered_message_not_included_in_loadInitialHistory() {
        let e = expectation(description: "load initial history worked")
        
        let chatSession: ChatSession = {
            
            let chatAPI = MockLiveLikeRestAPIServices()
            chatAPI.getMessageCompletionPromise = chatMessagesPromise
            
            let builder = PubSubChatSessionBuilder()
            builder.chatApi = chatAPI
//            builder.networking = StubNetworking(result: <#T##Result<Decodable, any Error>#>)
            return builder.build()
        }()
        
        chatSession.loadInitialHistory { result in
            switch result {
            case .success:
                XCTAssertTrue(chatSession.messages.contains(ChatMessage(chatMessageResponse: self.cleanMSG)))
                XCTAssertTrue(!chatSession.messages.contains(ChatMessage(chatMessageResponse: self.currentUserContentFilteredMSG)))
                XCTAssertTrue(!chatSession.messages.contains(ChatMessage(chatMessageResponse: self.nonUserContentFilteredMSG)))
                XCTAssertTrue(chatSession.containsFilteredMessages == false)
                e.fulfill()

            case .failure(let error):
                print(error)
                XCTFail(error.localizedDescription)
            }
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_filtered_message_included_in_loadInitialHistory_with_include_filtered_messages_true() {
        let e = expectation(description: "load initial history worked")
        
        let chatSession: ChatSession = {
            
            let chatAPI = MockLiveLikeRestAPIServices()
            chatAPI.getMessageCompletionPromise = chatMessagesPromise
            
            let builder = PubSubChatSessionBuilder()
            builder.chatApi = chatAPI
            builder.includeFilteredMessages = true
            return builder.build()
        }()
        
        chatSession.loadInitialHistory { result in
            switch result {
            case .success:
                XCTAssertTrue(chatSession.messages.contains(ChatMessage(chatMessageResponse: self.cleanMSG)))
                XCTAssertTrue(chatSession.messages.contains(ChatMessage(chatMessageResponse: self.currentUserContentFilteredMSG)))
                XCTAssertTrue(chatSession.messages.contains(ChatMessage(chatMessageResponse: self.nonUserContentFilteredMSG)))
                XCTAssertTrue(chatSession.containsFilteredMessages == true)
                e.fulfill()
            case .failure(let error):
                print(error)
                XCTFail(error.localizedDescription)
            }
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    // Tests to make sure that we include current user message, regardless of its filter status
    func test_current_user_filtered_message_included_in_loadInitialHistory() {
        let e = expectation(description: "load initial history worked")
        
        let chatSession: ChatSession = {
            
            let chatAPI = MockLiveLikeRestAPIServices()
            chatAPI.getMessageCompletionPromise = chatMessagesPromise
            
            let builder = PubSubChatSessionBuilder()
            builder.currentUserID = currentUserID
            builder.chatApi = chatAPI
            return builder.build()
        }()
        
        chatSession.loadInitialHistory { result in
            switch result {
            case .success:
                XCTAssertTrue(chatSession.messages.contains(ChatMessage(chatMessageResponse: self.cleanMSG)))
                XCTAssertTrue(chatSession.messages.contains(ChatMessage(chatMessageResponse: self.currentUserContentFilteredMSG)))
                XCTAssertTrue(!chatSession.messages.contains(ChatMessage(chatMessageResponse: self.nonUserContentFilteredMSG)))
                XCTAssertTrue(chatSession.containsFilteredMessages == false)
                
                e.fulfill()
            case .failure(let error):
                print(error)
                XCTFail(error.localizedDescription)
            }
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    // MARK: - Shadow Muting
    func test_shadow_muted_message_in_loadInitialHistory() {
        
        /*
         * Shadow muted messages from other users should be omited
         * Shadow muted messages from current user should be displayed
         */
        
        let e = expectation(description: "load initial history worked")
        
        let chatSession: ChatSession = {
            
            let chatAPI = MockLiveLikeRestAPIServices()
            chatAPI.getMessageCompletionPromise = chatMessagesPromise
            
            let builder = PubSubChatSessionBuilder()
            builder.currentUserID = self.currentUserID
            builder.chatApi = chatAPI
            return builder.build()
        }()
        
        chatSession.loadInitialHistory { result in
            switch result {
            case .success:
                XCTAssertTrue(!chatSession.messages.contains(ChatMessage(chatMessageResponse: self.nonCurrentUserShadowMutedMSG)))
                XCTAssertTrue(chatSession.messages.contains(ChatMessage(chatMessageResponse: self.currentUserShadowMutedMSG)))
                e.fulfill()
            case .failure(let error):
                print(error)
                XCTFail(error.localizedDescription)
            }
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    // MARK: - Chat Avatar
    
    /// Checks that the setter `shouldDisplayAvatar` is correctly represented by `isAvatarDisplayed`
    func test_chat_avatar_setter_getter() {
        let chatSession: ChatSession = {
            let builder = PubSubChatSessionBuilder()
            builder.shouldDisplayAvatar = true
            return builder.build()
        }()
        XCTAssert(chatSession.isAvatarDisplayed == true, "`shouldDisplayAvatar` and `isAvatarDisplayed` are not in parity")
    }
    
    /// Checks that the setter `shouldDisplayAvatar` is correctly represented by `isAvatarDisplayed`
    func test_chat_avatar_deprecation() {
        let chatSession: ChatSession = {
            let builder = PubSubChatSessionBuilder()
            builder.shouldDisplayAvatar = true
            return builder.build()
        }()
        
        let avatarURL = URL(string: "https://ca.slack-edge.com/T04G26FHU-U012MNVGMRQ-8d7434ded7ff-512")!
        chatSession.avatarURL = avatarURL
        XCTAssert(chatSession.avatarURL == avatarURL)
    }
    
    func test_loadMoreHistory_updates_messages() {
        let e = expectation(description: "load history completed")
        let chatSession: ChatSession = {
            let builder = PubSubChatSessionBuilder()
            let chatMessageBuilder = ChatMessageBuilder()
            builder.messages = [
                chatMessageBuilder.build(withID: "id-1")
            ]
            return builder.build()
        }()
        
        chatSession.loadNextHistory { result in
            switch result {
            case .success(let messages):
                XCTAssert(messages.count == 1)
                XCTAssertEqual(chatSession.messages[0].id.asString, "1234")
                XCTAssert(chatSession.messages.count == 2)
                e.fulfill()
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_loadMoreHistory_consecutive_calls_throws_error() {
        let e = expectation(description: "load history completed")
        let chatSession: ChatSession = {
            let builder = PubSubChatSessionBuilder()
            return builder.build()
        }()
        
        chatSession.loadNextHistory { _ in }
        chatSession.loadNextHistory { result in
            switch result {
            case .success:
                XCTFail("Previous load history call has not finished.")
            case .failure(let error):
                guard let error = error as? ChatSessionError else {
                    XCTFail("Expected error type ChatSessionError")
                    return
                }
                XCTAssert(error == ChatSessionError.concurrentLoadHistoryCalls)
                e.fulfill()
            }
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_loadMoreHistory_nested_consecutive_calls_success() {
        let e = expectation(description: "load history completed")
        let chatSession: ChatSession = {
            let builder = PubSubChatSessionBuilder()
            return builder.build()
        }()
        
        chatSession.loadNextHistory { _ in
            chatSession.loadNextHistory { result in
                switch result {
                case .success:
                    e.fulfill()
                case .failure(let error):
                    XCTFail("Expected success: \(error)")
                }
            }
        }
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    /// Tests the `send` function to return the same value in sync and async return
    func test_sendResultIsTheSame() {
        let chatSession: ChatSession = {
            let builder = PubSubChatSessionBuilder()
            let mockChannel = MockPubSubChannel()
            mockChannel.sendCompletionResult = .success("123")
            builder.nickNameVendor.setNickname(nickname: "Mike")
            builder.chatChannel = mockChannel
            return builder.build()
        }()
        
        let newMSG = NewChatMessage(text: "test")
        var newMessageAsync: ChatMessage?
        let newMessageSync = chatSession.sendMessage(newMSG) { result in
            switch result {
            case .success(let chatMessageID):
                newMessageAsync = chatMessageID
            case .failure(let error):
                XCTFail("error: \(error)")
            }
        }
        
        sleep(2)
        guard let newMsgIDAsync = newMessageAsync else { return }
        
        XCTAssertTrue(newMessageSync == newMsgIDAsync)
    }
    
    /// Tests the sticker shortcode validation functionality
    func test_stickercodeValidation() {
        let stickerPackBuilder = StickerPackBuilder()
        stickerPackBuilder.stickers = [
            Sticker(
                id: "1",
                url: MockObjects.mockURL,
                packId: "1",
                packUrl: MockObjects.mockURL,
                packName: "",
                shortcode: "shortcode1",
                file: MockObjects.mockURL
            ),
            Sticker(
                id: "2",
                url: MockObjects.mockURL,
                packId: "2",
                packUrl: MockObjects.mockURL,
                packName: "",
                shortcode: "shortcode2",
                file: MockObjects.mockURL
            ),
        ]
        let stickerPack = stickerPackBuilder.build()
        let message: String = ":shortcode1:"
        let validStickers = message.getStickerShortcodes(stickerPacks: [stickerPack])
        if validStickers.count != 1 {
            XCTFail("There should have been one valid sticker found")
        }
    }
    
    /// Check to make sure that a string representing a time is not cosnidered a sticker shortcode
    func test_stickercodeTimeValidation() {
        let stickerPackBuilder = StickerPackBuilder()
        stickerPackBuilder.stickers = [
            Sticker(
                id: "1",
                url: MockObjects.mockURL,
                packId: "1",
                packUrl: MockObjects.mockURL,
                packName: "",
                shortcode: "shortcode1",
                file: MockObjects.mockURL
            ),
            Sticker(
                id: "2",
                url: MockObjects.mockURL,
                packId: "2",
                packUrl: MockObjects.mockURL,
                packName: "",
                shortcode: "shortcode2",
                file: MockObjects.mockURL
            ),
        ]
        let stickerPack = stickerPackBuilder.build()
        let message: String = "11:54:23"
        let validStickers = message.getStickerShortcodes(stickerPacks: [stickerPack])
        if validStickers.count != 0 {
            XCTFail("There should be no valid stickers")
        }
    }
    
    func test_custom_chat_message_delegate_raised() {
        let channel = MockPubSubChannel()
        let customChatMessage = PubSubChannelMessage(
            pubsubID: "",
            message: PubSubChatMessageBuilder()
                .customData("test-data")
                .senderID("some-other-user")
                .buildCustomMessage(),
            createdAt: TimeToken(date: .distantPast).pubnubTimetoken,
            messageActions: []
        )
        let chatSession: PubSubChatRoom = {
            let builder = PubSubChatSessionBuilder()
            builder.currentUserID = "some-user"
            return builder.build()
        }()
        
        
        let didRecieveNewMessageExpectation = expectation(description: "done")
        let delegate = MockChatSessionDelegate()
        delegate.didRecieveNewMessageCompletion = { _, message in
            XCTAssert(message.customData == "test-data")
            didRecieveNewMessageExpectation.fulfill()
        }
        
        chatSession.addDelegate(delegate)
        chatSession.channel(channel, messageCreated: customChatMessage)
        
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    func test_send_custom_chat_message() {
        let e = expectation(description: "Sending Custom Message Succeeded")
        let chatSession: ChatSession = {
            let builder = PubSubChatSessionBuilder()
            let mockChannel = MockPubSubChannel()
            mockChannel.sendCompletionResult = .success("123")
            builder.chatChannel = mockChannel
            builder.nickNameVendor.setNickname(nickname: "Mike")
            return builder.build()
        }()
        
        let customMessage: String = "Custom Message Text"
        chatSession.sendCustomMessage(customMessage) { result in
            switch result {
            case .success:
                e.fulfill()
            case .failure(let error):
                XCTFail("Failed Sending Message: \(error)")
            }
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_pin_message() {
        let e = expectation(description: "Pinning Message Succeeded")
        let chatSession: ChatSession = {
            let builder = PubSubChatSessionBuilder()
            let mockChannel = MockPubSubChannel()
            mockChannel.sendCompletionResult = .success("123")
            builder.chatChannel = mockChannel
            builder.nickNameVendor.setNickname(nickname: "Mike")
            return builder.build()
        }()
        
        let pinMessage = ChatMessageBuilder().build(withID: "1234")
        
        chatSession.pinMessage(pinMessage) { result in
            switch result {
            case .success(let message):
                XCTAssert(message.messagePayload.id == "1234")
                e.fulfill()
            case .failure(let error):
                XCTFail("Failed Pinning Message: \(error)")
            }
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_unpin_chat_message() {
        let e = expectation(description: "Unpinning Message Succeeded")
        let chatSession: ChatSession = {
            let builder = PubSubChatSessionBuilder()
            let mockChannel = MockPubSubChannel()
            mockChannel.sendCompletionResult = .success("123")
            builder.chatChannel = mockChannel
            builder.nickNameVendor.setNickname(nickname: "Mike")
            return builder.build()
        }()
        
        chatSession.unpinMessage(pinMessageInfoID: "1234") { result in
            switch result {
            case .success(let result):
                XCTAssert(result == true)
                e.fulfill()
            case .failure(let error):
                XCTFail("Failed Pinning Message: \(error)")
            }
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }

}
