//
//  downStreamWidgetProxySpy.swift
//  LiveLikeTests
//
//  Created by Cory Sullivan on 2019-02-22.
//

@testable import EngagementSDK
@testable import LiveLikeSwift
import Foundation
import XCTest

class downStreamWidgetProxySpy: WidgetProxyInput {
    var messageReadyExpectation: XCTestExpectation?
    var messageDiscardedExpection: XCTestExpectation?
    var messageResult: ClientEvent?
    var discardedReason: DiscardedReason?

    func publish(event: WidgetProxyPublishData) {
        guard let expectation = messageReadyExpectation else {
            XCTFail("downStreamWidgetProxySpy was not setup correctly. Missing XCTExpectation reference")
            return
        }
        messageResult = event.clientEvent
        expectation.fulfill()
    }

    func discard(event: WidgetProxyPublishData, reason: DiscardedReason) {
        guard let expectation = messageDiscardedExpection else {
            XCTFail("downStreamWidgetProxySpy was not setup correctly. Missing XCTExpectation reference")
            return
        }
        discardedReason = reason
        expectation.fulfill()
    }

    func connectionStatusDidChange(_ status: ConnectionStatus) {}

    func error(_ error: Error) {
        print("Received error: \(error)")
    }
}
