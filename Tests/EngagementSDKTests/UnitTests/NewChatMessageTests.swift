//
//  NewChatMessageTests.swift
//  EngagementSDKTests
//
//  Created by Mike Moloksher on 3/24/21.
//

@testable import EngagementSDK
import XCTest

class NewChatMessageTests: XCTestCase {
    
    func test_NewChatMessageTextChatMessage() {
        let msgText = "This is a test"
        let newMSG = NewChatMessage(text: "This is a test")
        
        XCTAssertTrue(newMSG.text == msgText && newMSG.imageSize == nil && newMSG.imageURL == nil)
    }
    
    func test_NewChatMessageImageURL() {
        guard let imgURL = URL(string: "http://www.google.com") else { return }
        let imgSize = CGSize(width: 50, height: 50)
        let newMSG = NewChatMessage(imageURL: imgURL, imageSize: imgSize)
        
        XCTAssertTrue(newMSG.text == nil && newMSG.imageSize == imgSize && newMSG.imageURL == imgURL)
    }
    
    func test_NewChatMessageImageData() {
        guard let imageData = UIImage(named: "logo_small")?.pngData() else { return }
        let imgSize = CGSize(width: 50, height: 50)
        let newMSG = NewChatMessage(imageData: imageData, imageSize: imgSize)
        
        XCTAssertTrue(newMSG.text == nil && newMSG.imageSize == imgSize && newMSG.imageURL != nil)
    }
}
