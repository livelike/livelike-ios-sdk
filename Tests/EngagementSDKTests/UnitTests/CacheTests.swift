//
//  CacheTests.swift
//  LikeLiveTests
//
//  Created by Heinrich Dahms on 2019-02-19.
//

@testable import EngagementSDK
@testable import LiveLikeSwift
@testable import LiveLikeCore
import XCTest

class CacheTests: XCTestCase {
    let testImage1 = Helper.getImage(name: "LiveLikeLogo1", ofType: "png")!
    let testImage2 = Helper.getImage(name: "LiveLikeLogo2", ofType: "jpg")!

    var testImage1Data: Data { return testImage1.pngData()! }
    var testImage2Data: Data { return testImage2.pngData()! }

    let testImageURL1 = "https://example.com/testImage1.png"
    let testImageURL2 = "https://example.com/testImage2.jpg"

    var cache: Cache?

    override func setUp() {
        cache = Cache()
    }

    override func tearDown() {
        cache?.clear()
        cache = nil
    }

    func testCacheImage() {
        let testImage1Data = testImage1.pngData()!
        cache?.set(object: testImage1Data, key: testImageURL1) {
            self.cache?.get(key: self.testImageURL1, completion: { (data: Data?) in
                XCTAssert(data == self.testImage1Data, "Cached image and reference image do not match")
                XCTAssert(data != self.testImage2.pngData(), "Cached image and incorrect reference image match")
            })
        }
    }

    // Clean up this nested test
    func testCacheClear() {
        let testImage1Data = testImage1.pngData()!
        let testImage2Data = testImage2.pngData()!
        cache?.set(object: testImage1Data, key: testImageURL1, completion: {
            self.cache?.set(object: self.testImage2Data, key: self.testImageURL2, completion: {
                self.cache?.get(key: self.testImageURL1, completion: { (data: Data?) in
                    XCTAssert(data == testImage1Data, "Cached image and reference image do not match")
                    self.cache?.get(key: self.testImageURL2, completion: { (data: Data?) in
                        XCTAssert(data == testImage2Data, "Cached image and reference image do not match")
                        self.cache?.clear()
                        self.cache?.get(key: self.testImageURL1, completion: { (data: Data?) in
                            XCTAssert(data == nil, "Cached image and reference image do not match")
                            self.cache?.get(key: self.testImageURL2, completion: { (data: Data?) in
                                XCTAssert(data == nil, "Cached image and reference image do not match")
                            })
                        })
                    })
                })
            })
        })
    }
}
