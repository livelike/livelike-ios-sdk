//
//  SocialEmbedWidgetModelTests.swift
//  EngagementSDKTests
//
//  Created by Mike Moloksher on 8/5/21.
//

@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift
import XCTest

class SocialEmbedWidgetModelTests: XCTestCase {

    var socialEmebedWidgetResource: SocialEmbedWidgetResource?

    override func setUpWithError() throws {
        do {
            let socialEmebedWidgetResource: SocialEmbedWidgetResource = try Helper.jsonDecode(filename: "SocialEmbedResourceSample")
            self.socialEmebedWidgetResource = socialEmebedWidgetResource
        } catch {
            XCTFail("\(error)")
        }
    }

    /// Tests conversion of a resource into a model
    func test_model_data_matches_social_embed_resource() {
        guard let socialEmebedWidgetResource = socialEmebedWidgetResource else {
            XCTFail("Resource Missing")
            return
        }

        let model: SocialEmbedWidgetModel = {
            let builder = SocialEmbedWidgetBuilder()
            return builder.build(from: socialEmebedWidgetResource)
        }()

        // meta data
        XCTAssert(model.id == socialEmebedWidgetResource.id)
        XCTAssert(model.customData == socialEmebedWidgetResource.customData)
        XCTAssert(model.kind == socialEmebedWidgetResource.kind)
        XCTAssert(model.publishedAt == socialEmebedWidgetResource.publishedAt)
        XCTAssert(model.createdAt == socialEmebedWidgetResource.createdAt)
        XCTAssert(model.interactionTimeInterval == socialEmebedWidgetResource.timeout)

        // data
        XCTAssert(model.comment == socialEmebedWidgetResource.comment)
        model.items.enumerated().forEach { index, choice in
            XCTAssert(choice.id == socialEmebedWidgetResource.items[index].id)
            XCTAssert(choice.url == socialEmebedWidgetResource.items[index].url)
            XCTAssert(choice.oembed.html == socialEmebedWidgetResource.items[index].oembed.html)
            XCTAssert(choice.oembed.providerName == socialEmebedWidgetResource.items[index].oembed.providerName)
        }

        model.sponsors.enumerated().forEach { index, sponsor in
            XCTAssert(sponsor.id == socialEmebedWidgetResource.sponsors[index].id)
            XCTAssert(sponsor.clientID == socialEmebedWidgetResource.sponsors[index].clientID)
            XCTAssert(sponsor.name == socialEmebedWidgetResource.sponsors[index].name)
            XCTAssert(sponsor.logoURL == socialEmebedWidgetResource.sponsors[index].logoURL)
        }
    }

    func test_socialEmbed_registerImpression_makes_livelikeAPI_request() {
        guard let socialEmebedWidgetResource = socialEmebedWidgetResource else {
            XCTFail("Resource Missing")
            return
        }

        let e = expectation(description: "createImpression called")

        let livelikeAPI = MockLiveLikeRestAPIServices()
        livelikeAPI.createImpressionCompletion = { _, _, _ in
            e.fulfill()
            return Promise()
        }

        let model: SocialEmbedWidgetModel = {
            let builder = SocialEmbedWidgetBuilder()
            builder.livelikeAPI = livelikeAPI
            return builder.build(from: socialEmebedWidgetResource)
        }()

        model.registerImpression()

        waitForExpectations(timeout: 1.0, handler: nil)
    }

}

class SocialEmbedWidgetBuilder {

    var eventRecorder: EventRecorder = MockEventRecorder()
    var widgetClient: PubSubWidgetClient = MockWidgetClient()
    var userProfile: UserProfileProtocol = MockUserProfile()
    var leaderboardsManager: LeaderboardsManager = LeaderboardsManager()
    var livelikeAPI: MockLiveLikeRestAPIServices = MockLiveLikeRestAPIServices()
    var widgetInteractionRepo: WidgetInteractionRepository = MockWidgetInteractionRepository()

    func build(from resource: SocialEmbedWidgetResource) -> SocialEmbedWidgetModel {
        let model = SocialEmbedWidgetModel(
            resource: resource,
            eventRecorder: eventRecorder,
            livelikeAPI: livelikeAPI,
            userProfile: userProfile
        )
        return model
    }
}
