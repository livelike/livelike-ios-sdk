//
//  QuestsClientTests.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 6/9/22.
//

import XCTest
@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift

class QuestsClientTests: XCTestCase {

    let questTaskJSON: String =
        """
        {
            "id": "2ca6bbf8-b214-4e34-a19d-cad55d2ed398",
            "name": "Buy iPhone",
            "description": "",
            "quest_id": "0bd85b3c-7fb0-4e49-b75d-b80d2d069963",
            "target_value": 1,
            "default_progress_increment": 1,
            "created_at": "2022-06-30T15:59:49.544276Z"
        }
        """
    
    let questJSON: String =
        """
        {
            "id": "0bd85b3c-7fb0-4e49-b75d-b80d2d069963",
            "client_id": "GaEBcpVrCxiJOSNu4bvX6krEaguxHR9Hlp63tK6L",
            "name": "Mike Quest 1",
            "description": "",
            "quest_tasks": [
                {
                    "id": "2ca6bbf8-b214-4e34-a19d-cad55d2ed398",
                    "name": "Buy iPhone",
                    "description": "",
                    "quest_id": "0bd85b3c-7fb0-4e49-b75d-b80d2d069963",
                    "target_value": 1,
                    "default_progress_increment": 1,
                    "created_at": "2022-06-30T15:59:49.544276Z"
                },
                {
                    "id": "581d7f80-1538-4733-a301-d62db6a68868",
                    "name": "Buy shoes",
                    "description": "",
                    "quest_id": "0bd85b3c-7fb0-4e49-b75d-b80d2d069963",
                    "target_value": 1,
                    "default_progress_increment": 1,
                    "created_at": "2022-06-30T15:59:49.537791Z"
                }
            ],
            "created_at": "2022-06-30T15:59:49.518176Z",
            "quest_rewards_url": "https://google.com"
        }
        """
    
    let userQuestTaskJSON: String =
        """
        {
            "id": "d85bc607-61e8-4c4e-8c8f-a19c9bc6095a",
            "status": "completed",
            "created_at": "2022-06-30T19:57:23.921935Z",
            "completed_at": "2022-07-06T19:14:50.635843Z",
            "progress": 0,
            "user_quest_id": "9786776c-7b14-4cd5-bebd-8e16b906848d",
            "quest_task": {
                "id": "2ca6bbf8-b214-4e34-a19d-cad55d2ed398",
                "name": "Buy iPhone",
                "description": "",
                "quest_id": "0bd85b3c-7fb0-4e49-b75d-b80d2d069963",
                "created_at": "2022-06-30T15:59:49.544276Z",
                "default_progress_increment": 1,
                "target_value": 1
            }
        }
        """
    
    let userQuestJSON: String =
        """
        {
            "id": "9786776c-7b14-4cd5-bebd-8e16b906848d",
            "profile_id": "cf264acb-503c-465a-bb2d-2936db2197b0",
            "created_at": "2022-06-30T19:57:23.911664Z",
            "completed_at": "2022-07-06T19:15:31.192944Z",
            "user_quest_tasks": [
                {
                    "id": "d85bc607-61e8-4c4e-8c8f-a19c9bc6095a",
                    "status": "completed",
                    "created_at": "2022-06-30T19:57:23.921935Z",
                    "completed_at": "2022-07-06T19:14:50.635843Z",
                    "progress": 0,
                    "user_quest_id": "9786776c-7b14-4cd5-bebd-8e16b906848d",
                    "quest_task": {
                        "id": "2ca6bbf8-b214-4e34-a19d-cad55d2ed398",
                        "name": "Buy iPhone",
                        "description": "",
                        "quest_id": "0bd85b3c-7fb0-4e49-b75d-b80d2d069963",
                        "created_at": "2022-06-30T15:59:49.544276Z",
                        "default_progress_increment": 1,
                        "target_value": 1
                    }
                },
                {
                    "id": "1e25c069-06eb-4006-bc92-ec168889248f",
                    "status": "completed",
                    "created_at": "2022-06-30T19:57:23.934880Z",
                    "completed_at": "2022-07-06T19:15:31.186075Z",
                    "progress": 0,
                    "user_quest_id": "9786776c-7b14-4cd5-bebd-8e16b906848d",
                    "quest_task": {
                        "id": "581d7f80-1538-4733-a301-d62db6a68868",
                        "name": "Buy shoes",
                        "description": "",
                        "quest_id": "0bd85b3c-7fb0-4e49-b75d-b80d2d069963",
                        "created_at": "2022-06-30T15:59:49.537791Z",
                        "default_progress_increment": 1,
                        "target_value": 1
                    }
                }
            ],
            "status": "completed",
            "quest": {
                "id": "0bd85b3c-7fb0-4e49-b75d-b80d2d069963",
                "client_id": "GaEBcpVrCxiJOSNu4bvX6krEaguxHR9Hlp63tK6L",
                "name": "Mike Quest 1",
                "description": "",
                "quest_tasks": [
                    {
                        "id": "2ca6bbf8-b214-4e34-a19d-cad55d2ed398",
                        "name": "Buy iPhone",
                        "description": "",
                        "quest_id": "0bd85b3c-7fb0-4e49-b75d-b80d2d069963",
                        "target_value": 1,
                        "default_progress_increment": 1,
                        "created_at": "2022-06-30T15:59:49.544276Z"
                    },
                    {
                        "id": "581d7f80-1538-4733-a301-d62db6a68868",
                        "name": "Buy shoes",
                        "description": "",
                        "quest_id": "0bd85b3c-7fb0-4e49-b75d-b80d2d069963",
                        "target_value": 1,
                        "default_progress_increment": 1,
                        "created_at": "2022-06-30T15:59:49.537791Z"
                    }
                ],
                "created_at": "2022-06-30T15:59:49.518176Z",
                "quest_rewards_url": "https://google.com"
            },
            "rewards_status": "unclaimed",
            "rewards_claimed_at": "2022-06-30T15:59:49.537791Z",
            "user_quest_rewards_url": "https://google.com"
        }
        """
    
    let decoder = LLJSONDecoder()

    // MARK: - JSON -> Resource conversion tests
    func test_quest_json_to_resource_conversion() {
        let jsonData = questJSON.data(using: .utf8)!
        do {
            _ = try decoder.decode(Quest.self, from: jsonData)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
    func test_quest_task_json_to_resource_conversion() {
        let jsonData = questTaskJSON.data(using: .utf8)!
        do {
            _ = try decoder.decode(QuestTask.self, from: jsonData)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
    func test_user_quest_task_json_to_resource_conversion() {
        let jsonData = userQuestTaskJSON.data(using: .utf8)!
        do {
            _ = try decoder.decode(UserQuestTask.self, from: jsonData)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
    func test_user_quest_json_to_resource_conversion() {
        let jsonData = userQuestJSON.data(using: .utf8)!
        do {
            _ = try decoder.decode(UserQuest.self, from: jsonData)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
    // MARK: - `QuestClient` SDK Interface Tests
    
    func test_get_quests_success() {
        let e = expectation(description: "Successfully retrieved all Quests")
        
        guard let jsonData = questJSON.data(using: .utf8),
              let decodedQuest = try? decoder.decode(Quest.self, from: jsonData)
        else {
            
            XCTFail("Failed to decode Quest")
            return
        }
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getQuestsPromise = Promise(
            value: PaginatedResult<Quest>(
                previous: nil,
                count: 1,
                next: nil,
                items: [decodedQuest]
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockQuestsClient = InternalQuestsClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                userProfileVendor: builder.userProfileVendor,
                networking: builder.networking,
                questsAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()
        
        sdk.quests.getQuests(
            page: .first,
            options: GetQuestsRequestOptions()
        ) { result in
            switch result {
            case .success(let quests):
                if quests.first?.id == decodedQuest.id {
                    e.fulfill()
                } else {
                    XCTFail("Returned Quests do not match")
                }
            case .failure(let error):
                XCTFail("Failed retrieving quests from QuestsClient - \(error)")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_get_quests_fail() {
        let e = expectation(description: "Successfully handled getting quests failure")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getQuestsPromise = Promise(
            error:
            QuestsClientError.failedGettingQuests(
                error: "data corrupted"
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockQuestsClient = InternalQuestsClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                userProfileVendor: builder.userProfileVendor,
                networking: builder.networking,
                questsAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()
        
        sdk.quests.getQuests(
            page: .first,
            options: GetQuestsRequestOptions()
        ) { result in
            switch result {
            case .success(_):
                XCTFail("Failed failing on Error")
            case .failure(_):
                e.fulfill()
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_get_user_quests_success() {
        let e = expectation(description: "Successfully retrieved all User Quests")
        
        guard let jsonData = userQuestJSON.data(using: .utf8),
              let decodedUserQuest = try? decoder.decode(UserQuest.self, from: jsonData)
        else {
            
            XCTFail("Failed to decode UserQuest")
            return
        }
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getUserQuestsPromise = Promise(
            value: PaginatedResult<UserQuest>(
                previous: nil,
                count: 1,
                next: nil,
                items: [decodedUserQuest]
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockQuestsClient = InternalQuestsClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                userProfileVendor: builder.userProfileVendor,
                networking: builder.networking,
                questsAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()
        
        sdk.quests.getUserQuests(
            page: .first,
            options: GetUserQuestsRequestOptions()
        ) { result in
            switch result {
            case .success(let userQuests):
                if userQuests.first?.id == decodedUserQuest.id {
                    e.fulfill()
                } else {
                    XCTFail("Returned User Quests do not match")
                }
            case .failure(let error):
                XCTFail("Failed retrieving user quests from QuestsClient - \(error)")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_get_user_quests_fail() {
        let e = expectation(description: "Successfully handled getting user quests failure")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getUserQuestsPromise = Promise(
            error:
            QuestsClientError.failedGettingQuests(
                error: "data corrupted"
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockQuestsClient = InternalQuestsClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                userProfileVendor: builder.userProfileVendor,
                networking: builder.networking,
                questsAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()
        
        sdk.quests.getUserQuests(
            page: .first,
            options: GetUserQuestsRequestOptions()
        ) { result in
            switch result {
            case .success(_):
                XCTFail("Failed failing on Error")
            case .failure(_):
                e.fulfill()
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_start_user_quests_success() {
        let e = expectation(description: "Successfully started a User Quest")
        
        guard let jsonData = userQuestJSON.data(using: .utf8),
              let decodedUserQuest = try? decoder.decode(UserQuest.self, from: jsonData)
        else {
            
            XCTFail("Failed to decode UserQuest")
            return
        }
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.startUserQuestPromise = Promise(value: decodedUserQuest)

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockQuestsClient = InternalQuestsClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                userProfileVendor: builder.userProfileVendor,
                networking: builder.networking,
                questsAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()
        
        sdk.quests.startUserQuest(questID: "") { result in
                
            switch result {
            case .success(let userQuest):
                if userQuest.id == decodedUserQuest.id {
                    e.fulfill()
                } else {
                    XCTFail("Returned User Quest does not match the one that was passed")
                }
            case .failure(let error):
                XCTFail("Failed retrieving user quests from QuestsClient - \(error)")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_start_user_quests_fail() {
        let e = expectation(description: "Successfully failed starting a User Quest")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.startUserQuestPromise = Promise(
            error: QuestsClientError.failedStartingUserQuest(
                error: "corrupt data"
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockQuestsClient = InternalQuestsClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                userProfileVendor: builder.userProfileVendor,
                networking: builder.networking,
                questsAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()
        
        sdk.quests.startUserQuest(questID: MockObjects.mockString) { result in
                
            switch result {
            case .success(_):
                XCTFail("Failed failing when error occured")
            case .failure(_):
                e.fulfill()
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_update_user_quest_tasks_success() {
        let e = expectation(description: "Successfully update user quest tasks")
        
        guard let jsonData = userQuestJSON.data(using: .utf8),
              let decodedUserQuest = try? decoder.decode(UserQuest.self, from: jsonData)
        else {
            
            XCTFail("Failed to decode UserQuest")
            return
        }
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.updateUserQuestTasksPromise = Promise(value: decodedUserQuest)

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockQuestsClient = InternalQuestsClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                userProfileVendor: builder.userProfileVendor,
                networking: builder.networking,
                questsAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()
        
        sdk.quests.updateUserQuestTasks(
            userQuestID: MockObjects.mockString,
            userQuestTaskIDs: [MockObjects.mockString],
            status: .completed
        ) { result in
                
            switch result {
            case .success(let userQuest):
                if userQuest.id == decodedUserQuest.id {
                    e.fulfill()
                } else {
                    XCTFail("Returned User Quest does not match the one that was passed")
                }
            case .failure(let error):
                XCTFail("Failed updating user quest tasks from QuestsClient - \(error)")
            }
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_update_user_quest_tasks_fail() {
        let e = expectation(description: "Successfully failed updating user quest tasks")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.updateUserQuestTasksPromise = Promise(
            error: QuestsClientError.failedStartingUserQuest(
                error: "corrupt data"
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockQuestsClient = InternalQuestsClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                userProfileVendor: builder.userProfileVendor,
                networking: builder.networking,
                questsAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()
        
        sdk.quests.updateUserQuestTasks(
            userQuestID: MockObjects.mockString,
            userQuestTaskIDs: [MockObjects.mockString],
            status: .completed
        ) { result in
                
            switch result {
            case .success(_):
                XCTFail("Failed failing when error occured")
            case .failure(_):
                e.fulfill()
            }
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_incerement_user_quest_task_progress_success() {
        let e = expectation(description: "Successfully incremented a progress for a user quest task")
        
        guard let jsonData = userQuestJSON.data(using: .utf8),
              let decodedUserQuest = try? decoder.decode(UserQuest.self, from: jsonData)
        else {
            
            XCTFail("Failed to decode UserQuestTask")
            return
        }
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.updateUserQuestTaskProgressPromise = Promise(
            value: UserQuestTaskProgress(
                id: MockObjects.mockString,
                customIncrement: nil,
                customProgress: nil,
                userQuest: decodedUserQuest
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockQuestsClient = InternalQuestsClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                userProfileVendor: builder.userProfileVendor,
                networking: builder.networking,
                questsAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()
        
        sdk.quests.incrementUserQuestTaskProgress(
            userQuestTaskID: MockObjects.mockString,
            customIncrement: nil
        ) { result in
            
            switch result {
            case .success(let userQuest):
                if userQuest.id == decodedUserQuest.id {
                    e.fulfill()
                } else {
                    XCTFail("Returned User Quest Task does not match the one that was passed")
                }
            case .failure(let error):
                XCTFail("Failed incrementing user quest task from QuestsClient - \(error)")
            }
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_incerement_user_quest_task_progress_fail() {
        let e = expectation(description: "Successfully failed incrementing a progress for a user quest task")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.updateUserQuestTaskProgressPromise = Promise(
            error: QuestsClientError.failedStartingUserQuest(
                error: "corrupt data"
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockQuestsClient = InternalQuestsClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                userProfileVendor: builder.userProfileVendor,
                networking: builder.networking,
                questsAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()
        
        sdk.quests.incrementUserQuestTaskProgress(
            userQuestTaskID: MockObjects.mockString,
            customIncrement: nil
        ) { result in
            
            switch result {
            case .success(_):
                XCTFail("Failed failing when error occured")
            case .failure(_):
                e.fulfill()
                
            }
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_update_progress_for_user_quest_task_progress_success() {
        let e = expectation(description: "Successfully set progress for a user quest task")
        
        guard let jsonData = userQuestJSON.data(using: .utf8),
              let decodedUserQuest = try? decoder.decode(UserQuest.self, from: jsonData)
        else {
            
            XCTFail("Failed to decode UserQuestTask")
            return
        }
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.updateUserQuestTaskProgressPromise = Promise(
            value: UserQuestTaskProgress(
                id: MockObjects.mockString,
                customIncrement: nil,
                customProgress: nil,
                userQuest: decodedUserQuest
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockQuestsClient = InternalQuestsClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                userProfileVendor: builder.userProfileVendor,
                networking: builder.networking,
                questsAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()
        
        sdk.quests.setUserQuestTaskProgress(
            userQuestTaskID: MockObjects.mockString,
            progress: 1.0
        ) { result in
                
            switch result {
            case .success(let userQuest):
                if userQuest.id == decodedUserQuest.id {
                    e.fulfill()
                } else {
                    XCTFail("Returned User Quest Task does not match the one that was passed")
                }
            case .failure(let error):
                XCTFail("Failed updating progress for user quest task from QuestsClient - \(error)")
            }
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_update_progress_for_user_quest_task_progress_fail() {
        let e = expectation(description: "Successfully failed updating progress for a user quest task")
        
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.updateUserQuestTaskProgressPromise = Promise(
            error: QuestsClientError.failedStartingUserQuest(
                error: "corrupt data"
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            builder.mockQuestsClient = InternalQuestsClient(
                coreAPI: mockLLRestAPIServices,
                accessTokenVendor: builder.accessTokenVendor,
                userProfileVendor: builder.userProfileVendor,
                networking: builder.networking,
                questsAPI: mockLLRestAPIServices
            )
            return builder.build()
        }()
        
        sdk.quests.setUserQuestTaskProgress(
            userQuestTaskID: MockObjects.mockString,
            progress: 1.0
        ) { result in
                
            switch result {
            case .success(_):
                XCTFail("Failed failing when error occured")
            case .failure(_):
                e.fulfill()
                
            }
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
}
