//
//  ImageSliderTests.swift
//  EngagementSDKTests
//
//  Created by Jelzon Monzon on 5/13/19.
//

@testable import EngagementSDK
@testable import LiveLikeSwift
import Foundation
import XCTest

class ImageSliderTests: XCTestCase {
    func test_image_slider_one_option_decode() {
        do {
            let _: ImageSliderCreated = try Helper.jsonDecode(filename: "ImageSliderOneOption")
        } catch {
            XCTFail("\(error)")
        }
    }

    func test_image_slider_five_option_decode() {
        do {
            let _: ImageSliderCreated = try Helper.jsonDecode(filename: "ImageSliderFiveOptions")
        } catch {
            XCTFail("\(error)")
        }
    }
}
