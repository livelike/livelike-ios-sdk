//
//  String+SecureDecoderTests.swift
//  EngagementSDK
//
//  Created by Keval Shah on 20/05/22.
//

@testable import EngagementSDK
@testable import LiveLikeSwift
import XCTest

class String_SecureDecoderTests: XCTestCase {

    let encodedMessages: [String] = [
        "LiveLike &amp; EngagementSDK",
        "&lt;Swift Package Manager&gt; &amp; &lt;CocoaPods&gt;",
        "https:&#47;&#47;www.google.com",
        "Test&#47;String&#47;Decoded"
    ]

    let decodedMessages: [String] = [
        "LiveLike & EngagementSDK",
        "<Swift Package Manager> & <CocoaPods>",
        "https://www.google.com",
        "Test/String/Decoded"
    ]

    func test_DecodedMessagesSuccess() {
        var messagesToDecode: [String] = []
        for message in encodedMessages {
            messagesToDecode.append(message.decodedTextMessage)
        }
        XCTAssertEqual(decodedMessages, messagesToDecode)
    }

}
