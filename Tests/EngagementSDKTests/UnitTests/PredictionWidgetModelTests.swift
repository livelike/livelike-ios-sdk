//
//  PredictionWidgetViewModelTests.swift
//  EngagementSDKTests
//
//  Created by Jelzon Monzon on 7/2/20.
//

@testable import EngagementSDK
@testable import LiveLikeSwift
import XCTest

class PredictionWidgetModelTests: XCTestCase {

    func test_lockInVote() {
        let livelikeAPIExpectation = expectation(description: "livelikeapi invoked")
        let lockInVoteCompletionExpectation = expectation(description: "lockInVote completion invoked")
        let predictionRepoWriteExpectation = expectation(description: "predictionRepo add invoked")

        let resource = TextPredictionResourceBuilder().build()
        let predictionVote = PredictionVoteResource(
            id: UUID().uuidString,
            widgetID: UUID().uuidString,
            url: MockObjects.mockURL,
            optionId: UUID().uuidString,
            rewards: [],
            createdAt: Date(),
            claimToken: UUID().uuidString,
            widgetKind: .textPrediction
        )
        let model: PredictionWidgetModel = {
            let builder = PredictionModelBuilder()
            let livelikeAPI = MockLiveLikeRestAPIServices()
            livelikeAPI.createPredictionVoteCompletion = { _, _, completion in
                livelikeAPIExpectation.fulfill()
                completion(.success(predictionVote))
                return URLSessionDataTask()
            }
            let predictionRepo = MockPredictionVoteRepo()
            predictionRepo.addCompletion = { vote, _ in
                XCTAssert(predictionVote.optionId == vote.optionID)
                predictionRepoWriteExpectation.fulfill()
            }
            builder.livelikeAPI = livelikeAPI
            builder.voteRepo = predictionRepo
            return builder.build(from: resource)
        }()

        model.lockInVote(optionID: resource.options[0].id) { result in
            switch result {
            case .success:
                lockInVoteCompletionExpectation.fulfill()
            case .failure(let error):
                XCTFail("Expected success \(error)")
            }
        }

        waitForExpectations(timeout: 1.0, handler: nil)
    }
}

class MockPredictionVoteRepo: PredictionVoteRepository {
    var getCompletion: (String, (PredictionVote?) -> Void) -> Void = { _, _ in }
    var addCompletion: (PredictionVote, (Bool) -> Void) -> Void = { _, _ in }

    func get(by widgetID: String, completion: @escaping (PredictionVote?) -> Void) {
        getCompletion(widgetID, completion)
    }

    func add(vote: PredictionVote, completion: @escaping (Bool) -> Void) {
        addCompletion(vote, completion)
    }
}

class TextPredictionResourceBuilder {

    var confirmationMessage: String = UUID().uuidString
    var createdAt: Date = Date()
    var publishedAt: Date? = Date()
    var followUpUrl: URL = MockObjects.mockURL
    var id: String = UUID().uuidString
    var kind: WidgetKind = .textPrediction
    var options: [TextPredictionCreatedOption] = [
        .init(
            description: UUID().uuidString,
            id: UUID().uuidString,
            voteUrl: MockObjects.mockURL,
            voteCount: 0,
            earnableRewards: [],
            url: MockObjects.mockURL
        ),
        .init(
            description: UUID().uuidString,
            id: UUID().uuidString,
            voteUrl: MockObjects.mockURL,
            voteCount: 0,
            earnableRewards: [],
            url: MockObjects.mockURL
        )
    ]
    var programId: String = UUID().uuidString
    var question: String = UUID().uuidString
    var subscribeChannel: String = UUID().uuidString
    var timeout: TimeInterval = MockObjects.timeout.timeInterval
    var url: URL = MockObjects.mockURL
    var programDateTime: Date? = Date()
    var impressionUrl: URL? = MockObjects.mockURL
    var rewardsUrl: URL? = MockObjects.mockURL
    var customData: String? = UUID().uuidString
    var baseData: BaseWidgetResource = WidgetMetadataResourceBuilder().build()

    func build() -> TextPredictionCreated {
        return TextPredictionCreated(
            baseData: baseData,
            confirmationMessage: self.confirmationMessage,
            followUpUrl: self.followUpUrl,
            options: self.options,
            question: self.question,
            url: self.url,
            rewardsUrl: self.rewardsUrl,
            widgetInteractionsUrlTemplate: "",
            followUps: [],
            sponsors: [MockObjects.sponsorResource],
            earnableRewards: []
        )
    }

}

class PredictionModelBuilder {

    var eventRecorder: EventRecorder = MockEventRecorder()
    var userProfile: UserProfileProtocol = MockUserProfile()
    var rewardItems: [RewardItem] = []
    var voteRepo: PredictionVoteRepository = MockPredictionVoteRepo()
    var leaderboardsManager: LeaderboardsManager = LeaderboardsManager()
    var livelikeAPI: MockLiveLikeRestAPIServices = MockLiveLikeRestAPIServices()
    var widgetClient: PubSubWidgetClient = MockWidgetClient()
    var widgetInteractionRepo: WidgetInteractionRepository = MockWidgetInteractionRepository()
    var programSubscribeChannel: String = ""
    var predictionFollowUpModelFactory: PredictionWidgetModelFactoryProtocol = MockPredictionWidgetModelFactory()

    func build(from resource: TextPredictionCreated) -> PredictionWidgetModel {
        let model = PredictionWidgetModel(
            resource: resource,
            eventRecorder: self.eventRecorder,
            userProfile: self.userProfile,
            rewardItems: self.rewardItems,
            voteRepo: self.voteRepo,
            leaderboardManager: self.leaderboardsManager,
            livelikeAPI: self.livelikeAPI,
            widgetClient: self.widgetClient,
            widgetInteractionRepo: self.widgetInteractionRepo,
            programSubscribeChannel: self.programSubscribeChannel,
            followUpModelFactory: self.predictionFollowUpModelFactory
        )
        return model
    }

    func build(from resource: ImagePredictionCreated) -> PredictionWidgetModel {
        let model = PredictionWidgetModel(
            resource: resource,
            eventRecorder: self.eventRecorder,
            userProfile: self.userProfile,
            rewardItems: self.rewardItems,
            voteRepo: self.voteRepo,
            leaderboardManager: self.leaderboardsManager,
            livelikeAPI: self.livelikeAPI,
            widgetClient: self.widgetClient,
            widgetInteractionRepo: self.widgetInteractionRepo,
            programSubscribeChannel: self.programSubscribeChannel,
            followUpModelFactory: self.predictionFollowUpModelFactory
        )
        return model
    }

}

class PredictionFollowUpModelBuilder {

    var eventRecorder: EventRecorder = MockEventRecorder()
    var userProfile: UserProfileProtocol = MockUserProfile()
    var rewardItems: [RewardItem] = []
    var voteRepo: PredictionVoteRepository = MockPredictionVoteRepo()
    var leaderboardsManager: LeaderboardsManager = LeaderboardsManager()
    var livelikeAPI: MockLiveLikeRestAPIServices = MockLiveLikeRestAPIServices()
    var widgetClient: PubSubWidgetClient = MockWidgetClient()
    var widgetInteractionRepo: WidgetInteractionRepository = MockWidgetInteractionRepository()
    var programSubscribeChannel: String = ""
    var predictionFollowUpModelFactory: PredictionWidgetModelFactoryProtocol = MockPredictionWidgetModelFactory()

    func build(from resource: TextPredictionFollowUp) -> PredictionFollowUpWidgetModel {
        let model = PredictionFollowUpWidgetModel(
            resource: resource,
            eventRecorder: self.eventRecorder,
            livelikeAPI: self.livelikeAPI,
            leaderboardAPI: self.livelikeAPI,
            rewardItems: self.rewardItems,
            leaderboardsManager: self.leaderboardsManager,
            userProfile: self.userProfile,
            voteRepo: self.voteRepo,
            widgetInteractionRepo: self.widgetInteractionRepo
        )
        return model
    }

    func build(from resource: ImagePredictionFollowUp) -> PredictionFollowUpWidgetModel {
        let model = PredictionFollowUpWidgetModel(
            resource: resource,
            eventRecorder: self.eventRecorder,
            livelikeAPI: self.livelikeAPI,
            leaderboardAPI: self.livelikeAPI,
            rewardItems: self.rewardItems,
            leaderboardsManager: self.leaderboardsManager,
            userProfile: self.userProfile,
            voteRepo: self.voteRepo,
            widgetInteractionRepo: self.widgetInteractionRepo
        )
        return model
    }

}

class TextPredictionFollowUpResourceBuilder {

    var confirmationMessage: String = UUID().uuidString
    var createdAt: Date = Date()
    var publishedAt: Date? = Date()
    var followUpUrl: URL = MockObjects.mockURL
    var id: String = UUID().uuidString
    var kind: WidgetKind = .textPrediction
    var options: [TextPredictionFollowUpOption] = [
        .init(voteCount: 0, voteUrl: MockObjects.mockURL, description: "", id: UUID().uuidString, isCorrect: true, url: MockObjects.mockURL),
        .init(voteCount: 0, voteUrl: MockObjects.mockURL, description: "", id: UUID().uuidString, isCorrect: false, url: MockObjects.mockURL),
    ]
    var programId: String = UUID().uuidString
    var question: String = UUID().uuidString
    var subscribeChannel: String = UUID().uuidString
    var timeout: TimeInterval = MockObjects.timeout.timeInterval
    var url: URL = MockObjects.mockURL
    var programDateTime: Date? = Date()
    var impressionUrl: URL? = MockObjects.mockURL
    var rewardsUrl: URL? = MockObjects.mockURL
    var customData: String? = UUID().uuidString
    var claimURL: URL = MockObjects.mockURL
    var textPredictionUrl = MockObjects.mockURL
    var predictionID: String = UUID().uuidString
    var widgetInteractionsUrlTemplate: String = ""
    var status: WidgetStatus = .published
    var baseData: BaseWidgetResource = WidgetMetadataResourceBuilder().build()

    func build() -> TextPredictionFollowUp {
        return TextPredictionFollowUp(
            baseData: baseData,
            options: options,
            question: question,
            textPredictionUrl: textPredictionUrl,
            url: url,
            rewardsUrl: rewardsUrl,
            claimUrl: claimURL,
            textPredictionId: predictionID,
            widgetInteractionsUrlTemplate: widgetInteractionsUrlTemplate,
            status: status,
            sponsors: [MockObjects.sponsorResource]
        )
    }
}

class ImagePredictionFollowUpResourceBuilder {

    var confirmationMessage: String = UUID().uuidString
    var followUpUrl: URL = MockObjects.mockURL
    var options: [ImagePredictionFollowUpOption] = [
        .init(voteCount: 0, voteUrl: MockObjects.mockURL, imageUrl: MockObjects.mockURL, description: "", id: UUID().uuidString, isCorrect: true, url: MockObjects.mockURL),
        .init(voteCount: 0, voteUrl: MockObjects.mockURL, imageUrl: MockObjects.mockURL, description: "", id: UUID().uuidString, isCorrect: false, url: MockObjects.mockURL)
    ]
    var question: String = UUID().uuidString
    var url: URL = MockObjects.mockURL
    var rewardsUrl: URL? = MockObjects.mockURL
    var claimURL: URL = MockObjects.mockURL
    var textPredictionUrl = MockObjects.mockURL
    var predictionID: String = UUID().uuidString
    var interactionURL: URL = MockObjects.mockURL
    var status: WidgetStatus = .published
    var baseData: BaseWidgetResource = WidgetMetadataResourceBuilder().build()

    func build() -> ImagePredictionFollowUp {
        return ImagePredictionFollowUp(
            baseData: baseData,
            options: options,
            question: question,
            imagePredictionUrl: textPredictionUrl,
            url: url,
            claimUrl: claimURL,
            imagePredictionId: predictionID,
            interactionURL: interactionURL,
            status: status,
            sponsors: [MockObjects.sponsorResource]
        )
    }
}

class MockPredictionWidgetModelFactory: PredictionWidgetModelFactoryProtocol {
    func makePredictionFollowUpModel(resource: TextPredictionFollowUp) -> PredictionFollowUpWidgetModel {
        return PredictionFollowUpModelBuilder().build(from: resource)
    }

    func makePredictionFollowUpModel(resource: ImagePredictionFollowUp) -> PredictionFollowUpWidgetModel {
        return PredictionFollowUpModelBuilder().build(from: resource)
    }

    func makePredictionFollowUpModel(resource: NumberPredictionFollowUp) -> NumberPredictionFollowUpWidgetModel {
        let builder = NumberPredictionFollowUpModelBuilder()
        builder.resource = resource
        return builder.build()
    }
}
