//
//  PollWidgetViewModelTests.swift
//  EngagementSDKTests
//
//  Created by Jelzon Monzon on 7/2/20.
//

@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift
import XCTest

class PollWidgetModelTests: XCTestCase {

    func test_model_data_matches_text_poll_resource() {
        let pollWidgetResource: TextPollCreated = {
            let builder = TextPollCreatedBuilder()
            return builder.build()
        }()

        let model: PollWidgetModel = {
            let builder = PollModelBuilder()
            return builder.build(from: pollWidgetResource)
        }()

        XCTAssert(model.id == pollWidgetResource.id)
        XCTAssert(model.question == pollWidgetResource.question)
        XCTAssert(model.options.count == pollWidgetResource.options.count)
        model.options.enumerated().forEach { index, choice in
            XCTAssert(choice.id == pollWidgetResource.options[index].id)
            XCTAssert(choice.text == pollWidgetResource.options[index].description)
            XCTAssert(choice.imageURL == nil)
            XCTAssert(choice.voteCount == pollWidgetResource.options[index].voteCount)
            XCTAssert(choice.voteURL == pollWidgetResource.options[index].voteUrl)
        }
        XCTAssert(model.customData == pollWidgetResource.customData)
        XCTAssert(model.containsImages == false)
        XCTAssert(model.kind == pollWidgetResource.kind)
        XCTAssert(model.publishedAt == pollWidgetResource.publishedAt)
        XCTAssert(model.createdAt == pollWidgetResource.createdAt)

        model.sponsors.enumerated().forEach { index, sponsor in
            XCTAssert(sponsor.id == pollWidgetResource.sponsors[index].id)
            XCTAssert(sponsor.clientID == pollWidgetResource.sponsors[index].clientID)
            XCTAssert(sponsor.name == pollWidgetResource.sponsors[index].name)
            XCTAssert(sponsor.logoURL == pollWidgetResource.sponsors[index].logoURL)
        }
    }

    func test_model_data_matches_image_poll_resource() {
        let pollWidgetResource: ImagePollCreated = {
            let builder = ImagePollCreatedBuilder()
            return builder.build()
        }()

        let model: PollWidgetModel = {
            let builder = PollModelBuilder()
            return builder.build(from: pollWidgetResource)
        }()

        XCTAssert(model.id == pollWidgetResource.id)
        XCTAssert(model.question == pollWidgetResource.question)
        XCTAssert(model.options.count == pollWidgetResource.options.count)
        model.options.enumerated().forEach { index, choice in
            XCTAssert(choice.id == pollWidgetResource.options[index].id)
            XCTAssert(choice.text == pollWidgetResource.options[index].description)
            XCTAssert(choice.imageURL == pollWidgetResource.options[index].imageUrl)
            XCTAssert(choice.voteCount == pollWidgetResource.options[index].voteCount)
            XCTAssert(choice.voteURL == pollWidgetResource.options[index].voteUrl)
        }
        XCTAssert(model.customData == pollWidgetResource.customData)
        XCTAssert(model.containsImages == true)
        XCTAssert(model.kind == pollWidgetResource.kind)
        XCTAssert(model.publishedAt == pollWidgetResource.publishedAt)
        XCTAssert(model.createdAt == pollWidgetResource.createdAt)

        model.sponsors.enumerated().forEach { index, sponsor in
            XCTAssert(sponsor.id == pollWidgetResource.sponsors[index].id)
            XCTAssert(sponsor.clientID == pollWidgetResource.sponsors[index].clientID)
            XCTAssert(sponsor.name == pollWidgetResource.sponsors[index].name)
            XCTAssert(sponsor.logoURL == pollWidgetResource.sponsors[index].logoURL)
        }

    }

    func test_textPollModel_registerImpression_request() {
        let e = expectation(description: "registerImpression called on TextPoll")

        let livelikeAPI = MockLiveLikeRestAPIServices()
        livelikeAPI.createImpressionCompletion = { _, _, _ in
            e.fulfill()
            return Promise()
        }

        let resource = TextPollCreatedBuilder().build()
        let model: PollWidgetModel = {
            let builder = PollModelBuilder()
            builder.livelikeAPI = livelikeAPI
            return builder.build(from: resource)
        }()

        model.registerImpression()

        waitForExpectations(timeout: 1.0, handler: nil)
    }

    func test_ImagePollModel_registerImpression_request() {
        let e = expectation(description: "registerImpression called on ImagePoll")

        let livelikeAPI = MockLiveLikeRestAPIServices()
        livelikeAPI.createImpressionCompletion = { _, _, _ in
            e.fulfill()
            return Promise()
        }

        let resource = ImagePollCreatedBuilder().build()
        let model: PollWidgetModel = {
            let builder = PollModelBuilder()
            builder.livelikeAPI = livelikeAPI
            return builder.build(from: resource)
        }()

        model.registerImpression()

        waitForExpectations(timeout: 1.0, handler: nil)
    }

    func test_textPollModel_makes_submitVote_request() {
        let e = expectation(description: "submitVote called on TextPoll")

        let livelikeAPI = MockLiveLikeRestAPIServices()
        livelikeAPI.getVoteCompletion = { Promise(value: PollWidgetModel.Vote(
            id: UUID().uuidString,
            widgetID: UUID().uuidString,
            url: MockObjects.mockURL,
            optionID: TextPollCreatedBuilder.optionID,
            rewards: [],
            createdAt: Date(),
            widgetKind: .textPoll
        )
        )}
        let resource = TextPollCreatedBuilder().build()
        let model: PollWidgetModel = {
            let builder = PollModelBuilder()
            builder.livelikeAPI = livelikeAPI
            return builder.build(from: resource)
        }()

        model.submitVote(optionID: TextPollCreatedBuilder.optionID) { result in
            switch result {
            case .success(let vote):
                if vote.optionID == TextPollCreatedBuilder.optionID {
                    e.fulfill()
                }
            case .failure(let error):
                print(error)
            }
        }

        waitForExpectations(timeout: 1.0, handler: nil)
    }

    func test_imagePollModel_makes_submitVote_request() {
        let e = expectation(description: "submitVote called on ImagePoll")

        let livelikeAPI = MockLiveLikeRestAPIServices()
        livelikeAPI.getVoteCompletion = { Promise(value: PollWidgetModel.Vote(
            id: UUID().uuidString,
            widgetID: UUID().uuidString,
            url: MockObjects.mockURL,
            optionID: ImagePollCreatedBuilder.optionID,
            rewards: [],
            createdAt: Date(),
            widgetKind: .imagePoll
        )
        )}
        let resource = ImagePollCreatedBuilder().build()
        let model: PollWidgetModel = {
            let builder = PollModelBuilder()
            builder.livelikeAPI = livelikeAPI
            return builder.build(from: resource)
        }()

        model.submitVote(optionID: ImagePollCreatedBuilder.optionID) { result in
            switch result {
            case .success(let vote):
                if vote.optionID == ImagePollCreatedBuilder.optionID {
                    e.fulfill()
                }
            case .failure(let error):
                print(error)
            }
        }

        waitForExpectations(timeout: 1.0, handler: nil)
    }
}

class PollModelBuilder {

    var eventRecorder: EventRecorder = MockEventRecorder()
    var widgetClient: PubSubWidgetClient = MockWidgetClient()
    var userProfile: UserProfileProtocol = MockUserProfile()
    var rewardItems: [RewardItem] = []
    var leaderboardsManager: LeaderboardsManager = LeaderboardsManager()
    var livelikeAPI: MockLiveLikeRestAPIServices = MockLiveLikeRestAPIServices()
    var widgetInteractionRepo: WidgetInteractionRepository = MockWidgetInteractionRepository()

    func build(from resource: TextPollCreated) -> PollWidgetModel {
        let model = PollWidgetModel(
            data: resource,
            userProfile: userProfile,
            rewardItems: rewardItems,
            leaderboardManager: leaderboardsManager,
            livelikeAPI: livelikeAPI,
            widgetClient: widgetClient,
            eventRecorder: eventRecorder,
            widgetInteractionRepo: widgetInteractionRepo
        )
        return model
    }

    func build(from resource: ImagePollCreated) -> PollWidgetModel {
        let model = PollWidgetModel(
            data: resource,
            userProfile: userProfile,
            rewardItems: rewardItems,
            leaderboardManager: leaderboardsManager,
            livelikeAPI: livelikeAPI,
            widgetClient: widgetClient,
            eventRecorder: eventRecorder,
            widgetInteractionRepo: widgetInteractionRepo
        )
        return model
    }
}

class PollBuilder {
    var question: String = UUID().uuidString
    var rewardsUrl: URL? = MockObjects.mockURL
    var baseData: BaseWidgetResource = WidgetMetadataResourceBuilder().build()
}

class TextPollCreatedBuilder: PollBuilder {
    static var optionID = "999"
    var options: [TextPollOption] = [
        .init(id: TextPollCreatedBuilder.optionID, description: UUID().uuidString, voteCount: 1, voteUrl: MockObjects.mockURL),
        .init(id: UUID().uuidString, description: UUID().uuidString, voteCount: 2, voteUrl: MockObjects.mockURL)
    ]

    func build() -> TextPollCreated {
        return TextPollCreated(
            baseData: baseData,
            question: question,
            options: options,
            rewardsUrl: rewardsUrl,
            widgetInteractionsUrlTemplate: "https://google.com/{profile_id}",
            sponsors: [MockObjects.sponsorResource]
        )
    }
}

class ImagePollCreatedBuilder: PollBuilder {
    static var optionID = "998"
    var options: [ImagePollOption] = [
        .init(id: ImagePollCreatedBuilder.optionID, description: UUID().uuidString, imageUrl: MockObjects.mockURL, voteCount: 1, voteUrl: MockObjects.mockURL),
        .init(id: UUID().uuidString, description: UUID().uuidString, imageUrl: MockObjects.mockURL, voteCount: 2, voteUrl: MockObjects.mockURL)
    ]

    func build() -> ImagePollCreated {
        return ImagePollCreated(
            baseData: baseData,
            question: question,
            options: options,
            rewardsUrl: rewardsUrl,
            widgetInteractionsUrlTemplate: "",
            sponsors: [MockObjects.sponsorResource]
        )
    }
}
