//
//  TextAskWidgetModelTests.swift
//  EngagementSDKTests
//
//  Created by Jelzon Monzon on 8/10/21.
//

import XCTest
@testable import EngagementSDK
@testable import LiveLikeSwift

func test_widget_metadata_copied_from_resource(
    metadata: BaseWidgetResource,
    model: WidgetModelable
) {
    XCTAssertEqual(metadata.id, model.id)
    XCTAssertEqual(metadata.kind, model.kind)
    XCTAssertEqual(metadata.createdAt, model.createdAt)
    XCTAssertEqual(metadata.publishedAt, model.publishedAt)
    XCTAssertEqual(metadata.timeout, model.interactionTimeInterval)
    XCTAssertEqual(metadata.customData, model.customData)
    XCTAssertEqual(metadata.programID, model.programID)
    XCTAssertEqual(metadata.interactiveUntil, model.interactiveUntil)
}

class TextAskWidgetModelTests: XCTestCase {
    
    func test_data_copied_from_resource() {
        let resource = TextAskWidgetResourceBuilder().build()
        let model: TextAskWidgetModel = {
            let builder = TextAskWidgetModelBuilder()
            builder.resource = resource
            return builder.build()
        }()
        
        XCTAssertEqual(resource.title, model.title)
        XCTAssertEqual(resource.prompt, model.prompt)
        XCTAssertEqual(resource.confirmationMessage, model.confirmationMessage)
        test_widget_metadata_copied_from_resource(
            metadata: resource,
            model: model
        )
    }
    
    func test_submitReply_success() {
        let model: TextAskWidgetModel = {
            let builder = TextAskWidgetModelBuilder()
            builder.widgetAPI = {
                let widgetAPI = MockLiveLikeRestAPIServices()
                widgetAPI.createTextAskReplyCompletion = { _, reply, _ in
                    let reply = TextAskWidgetModel.Reply(
                        id: "id-1",
                        widgetID: "widgetID-1",
                        widgetKind: .textAsk,
                        createdAt: Date(),
                        text: reply
                    )
                    return .init(value: reply)
                }
                return widgetAPI
            }()
            return builder.build()
        }()
        
        let e = expectation(description: "Submit reply completed")
        
        model.submitReply("reply-1") {
            switch $0 {
            case .failure(let error):
                XCTFail(error.localizedDescription)
            case .success(let reply):
                XCTAssert(reply.id == "id-1")
                e.fulfill()
            }
        }
        
        waitForExpectations(timeout: 1.0, handler: nil)
    }
}

class TextAskWidgetModelBuilder {
    
    var resource: TextAskWidgetResource = TextAskWidgetResourceBuilder().build()
    var eventRecorder: EventRecorder = MockEventRecorder()
    var widgetAPI: LiveLikeWidgetAPIProtocol = MockLiveLikeRestAPIServices()
    var userProfile: UserProfileProtocol = MockUserProfile()
    var widgetInteractionRepo: WidgetInteractionRepository = MockWidgetInteractionRepository()
    
    func build() -> TextAskWidgetModel {
        return TextAskWidgetModel(
            resource: self.resource,
            eventRecorder: self.eventRecorder,
            widgetAPI: self.widgetAPI,
            userProfile: self.userProfile,
            widgetInteractionRepo: self.widgetInteractionRepo
        )
    }
}

class TextAskWidgetResourceBuilder {
    
    var baseData: BaseWidgetResource = WidgetMetadataResourceBuilder().build()
    var title: String = "title"
    var prompt: String = "prompt"
    var confirmationMessage: String = "confirmationMessage"
    var replyURL: URL = URL(string: "https://replyURL.com")!
    var widgetInteractionsUrlTemplate: String = ""
    
    func build() -> TextAskWidgetResource {
        return .init(
            baseData: self.baseData,
            title: self.title,
            prompt: self.prompt,
            confirmationMessage: self.confirmationMessage,
            replyURL: self.replyURL,
            widgetInteractionsURLTemplate: self.widgetInteractionsUrlTemplate
        )
    }
}

class WidgetMetadataResourceBuilder {
    
    var id: String = "id"
    var programID: String = "programID"
    var clientID: String = "clientID"
    var publishedAt: Date? = Date()
    var createdAt: Date = Date()
    var timeout: TimeInterval = 7
    var kind: WidgetKind = .alert
    var subscribeChannel: String = "subscribeChannel"
    var programDateTime: Date? = Date()
    var impressionURL: URL = URL(string: "https://impressionURL.com")!
    var widgetInteractionsUrlTemplate: String = ""
    var customData: String? = "customData"
    var interactiveUntil: Date? = Date()
    var attributes: [Attribute] = []
    var isPubnubEnabled: Bool = true
    var playbackTime: Int? = 0
    
    func build() -> BaseWidgetResource {
        return .init(
            id: self.id,
            programID: self.programID,
            clientID: self.clientID,
            publishedAt: self.publishedAt,
            createdAt: self.createdAt,
            timeout: self.timeout,
            kind: self.kind,
            subscribeChannel: self.subscribeChannel,
            programDateTime: self.programDateTime,
            impressionURL: self.impressionURL,
            customData: self.customData,
            interactiveUntil: self.interactiveUntil,
            attributes: [],
            isPubnubEnabled: self.isPubnubEnabled,
            playbackTime: self.playbackTime,
            deleteURL: MockObjects.mockURL,
            scheduleURL: MockObjects.mockURL
        )
    }
}
