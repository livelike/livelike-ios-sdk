//
//  CheerMeterWidgetViewModelTests.swift
//  EngagementSDKTests
//
//  Created by Jelzon Monzon on 7/6/20.
//

@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift
import XCTest

class CheerMeterWidgetViewModelTests: XCTestCase {

    let data = CheerMeterCreated(
        baseData: {
            let builder = WidgetMetadataResourceBuilder()
            builder.id = "widget-id-1"
            builder.programID = "program-id-1"
            builder.kind = .cheerMeter
            return builder.build()
        }(),
        rewardsUrl: nil,
        question: "question?",
        options: [
            .init(
                id: "option-id-1",
                description: "option 1",
                imageUrl: MockObjects.mockURL,
                voteUrl: MockObjects.mockURL,
                voteCount: 0
            ),
            .init(
                id: "option-id-2",
                description: "option 2",
                imageUrl: MockObjects.mockURL,
                voteUrl: MockObjects.mockURL,
                voteCount: 0
            ),
        ],
        widgetInteractionsUrlTemplate: "",
        sponsors: [MockObjects.sponsorResource]
    )

    func test_model_data_matches_cheer_meter_resource() {
        let model: CheerMeterWidgetModel = {
            let builder = CheerMeterBuilder()
            return builder.build(from: data)
        }()

        XCTAssert(model.id == data.id)
        XCTAssert(model.title == data.question)
        XCTAssert(model.customData == data.customData)
        XCTAssert(model.kind == data.kind)
        XCTAssert(model.publishedAt == data.publishedAt)
        XCTAssert(model.createdAt == data.createdAt)

        model.options.enumerated().forEach { index, choice in
            XCTAssert(choice.id == data.options[index].id)
            XCTAssert(choice.imageURL == data.options[index].imageUrl)
            XCTAssert(choice.text == data.options[index].description)
        }

        model.sponsors.enumerated().forEach { index, sponsor in
            XCTAssert(sponsor.id == data.sponsors[index].id)
            XCTAssert(sponsor.clientID == data.sponsors[index].clientID)
            XCTAssert(sponsor.name == data.sponsors[index].name)
            XCTAssert(sponsor.logoURL == data.sponsors[index].logoURL)
        }
    }

    func test_option_vote_count_updated_after_pubnub_message() {
        let widgetClient = MockWidgetClient()
        let cheerMeter = CheerMeterWidgetModel(
            data: data,
            userProfile: MockUserProfile(),
            rewardItems: [],
            leaderboardManager: LeaderboardsManager(),
            livelikeAPI: MockLiveLikeRestAPIServices(),
            widgetClient: widgetClient,
            eventRecorder: MockAnalytics(),
            widgetInteractionRepo: MockWidgetInteractionRepository()
        )

        do {
            let channel = try data.getResultsChannel()
            widgetClient.widgetListeners.publish(channel: channel) {
                $0.publish(event: WidgetProxyPublishData(
                    clientEvent: .cheerMeterResults(
                        CheerMeterResults(id: "widget-id-1", options: [
                            .init(id: "option-id-1", voteCount: 10)
                        ])
                    ))
                )
            }

            XCTAssert(cheerMeter.options.first(where: { $0.id == "option-id-1" })?.voteCount == 10)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    func test_submitVote_increments_vote_count_by_1() {
        let e = expectation(description: "vote count is accurate")
        let livelikeAPI = MockLiveLikeRestAPIServices()

        livelikeAPI.createCheerMeterVoteCompletion = { voteCount, _, _ in
            // we'll call 3 times
            XCTAssert(voteCount == 1)
            e.fulfill()
            return Promise(value: .init(
                rewards: [],
                widgetID: UUID().uuidString,
                voteCount: voteCount,
                patchURL: MockObjects.mockURL,
                optionId: "option-id-1",
                createdAt: Date(),
                widgetKind: .cheerMeter
            )
            )
        }

        let mockWidgetInteractionRepo = ServerWidgetInteractionRepository(
            livelikeAPI: livelikeAPI,
            userProfile: MockUserProfile(),
            cache: WidgetInteractionCache()
        )
        let cheerMeter = CheerMeterWidgetModel(
            data: data,
            userProfile: MockUserProfile(),
            rewardItems: [],
            leaderboardManager: LeaderboardsManager(),
            livelikeAPI: livelikeAPI,
            widgetClient: MockWidgetClient(),
            eventRecorder: MockAnalytics(),
            widgetInteractionRepo: mockWidgetInteractionRepo
        )

        DispatchQueue.global(qos: .userInitiated).async {
            cheerMeter.submitVote(optionID: "option-id-1")
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_cheerMeterDelegate_called_after_pubnub_message() {
        let e = expectation(description: "voteCountDidChange called")
        let delegate = MockCheerMeterDelegate()
        delegate.voteCountDidChangeCompletion = { voteCount, optionID in
            XCTAssert(voteCount == 10)
            XCTAssert(optionID == "option-id-1")
            e.fulfill()
        }
        let widgetClient = MockWidgetClient()
        let cheerMeter = CheerMeterWidgetModel(
            data: data,
            userProfile: MockUserProfile(),
            rewardItems: [],
            leaderboardManager: LeaderboardsManager(),
            livelikeAPI: MockLiveLikeRestAPIServices(),
            widgetClient: widgetClient,
            eventRecorder: MockAnalytics(),
            widgetInteractionRepo: MockWidgetInteractionRepository()
        )
        cheerMeter.delegate = delegate

        do {
            let channel = try data.getResultsChannel()
            widgetClient.widgetListeners.publish(channel: channel) {
                $0.publish(event: WidgetProxyPublishData(
                    clientEvent: .cheerMeterResults(
                        CheerMeterResults(id: "widget-id-1", options: [
                            .init(id: "option-id-1", voteCount: 10)
                        ])
                    ))
                )
            }
        } catch {
            XCTFail(error.localizedDescription)
        }


        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_registerImpression_make_livelikeAPI_request() {
        let e = expectation(description: "Successfully made call to livelikeAPI service")

        let livelikeAPI = MockLiveLikeRestAPIServices()
        livelikeAPI.createImpressionCompletion = { url, string, accessToken in
            e.fulfill()
            return Promise(value: .init(id: ""))
        }
        let model = CheerMeterWidgetModel(
            data: data,
            userProfile: MockUserProfile(),
            rewardItems: [],
            leaderboardManager: LeaderboardsManager(),
            livelikeAPI: livelikeAPI,
            widgetClient: MockWidgetClient(),
            eventRecorder: MockAnalytics(),
            widgetInteractionRepo: MockWidgetInteractionRepository()
        )

        model.registerImpression()

        waitForExpectations(timeout: 5.0, handler: nil)
    }
}

class MockCheerMeterDelegate: CheerMeterWidgetModelDelegate {

    var voteCountDidChangeCompletion: (Int, String) -> Void = { _, _ in }

    func cheerMeterWidgetModel(_ cheerMeter: CheerMeterWidgetModel, voteCountDidChange voteCount: Int, forOption optionID: String) {
        voteCountDidChangeCompletion(voteCount, optionID)
    }

    func cheerMeterWidgetModel(_ cheerMeter: CheerMeterWidgetModel, voteRequest: CheerMeterWidgetModel.VoteRequest, didComplete result: Result<CheerMeterWidgetModel.Vote, Error>) {

    }
}

class CheerMeterBuilder {

    var eventRecorder: EventRecorder = MockEventRecorder()
    var widgetClient: PubSubWidgetClient = MockWidgetClient()
    var userProfile: UserProfileProtocol = MockUserProfile()
    var rewardItems: [RewardItem] = []
    var leaderboardsManager: LeaderboardsManager = LeaderboardsManager()
    var livelikeAPI: MockLiveLikeRestAPIServices = MockLiveLikeRestAPIServices()
    var widgetInteractionRepo: WidgetInteractionRepository = MockWidgetInteractionRepository()

    func build(from resource: CheerMeterCreated) -> CheerMeterWidgetModel {
        let model = CheerMeterWidgetModel(
            data: resource,
            userProfile: userProfile,
            rewardItems: rewardItems,
            leaderboardManager: leaderboardsManager,
            livelikeAPI: livelikeAPI,
            widgetClient: widgetClient,
            eventRecorder: eventRecorder,
            widgetInteractionRepo: widgetInteractionRepo
        )
        return model
    }
}
