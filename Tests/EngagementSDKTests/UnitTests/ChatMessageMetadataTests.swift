//
//  ChatMessageMetadataTests.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 10/11/22.
//

import XCTest
@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift

class MessageMetadataTests: XCTestCase {
    
    /// Tests  that the encoding string does not include the non-encodable type safely
    func test_toMessageMetadataCodable() {
        
        struct NonCodableType {
            let someProperty: String
        }
        
        let sut: MessageMetadata = [
            "key1": "test-string",
            "key2": NonCodableType(someProperty: "some-value")
        ]
        
        let messageMetadataCodable = sut.asMessageMetadataCodable()
        
        do {
            let jsonData = try LLJSONEncoder().encode(messageMetadataCodable)
            let jsonString = String(data: jsonData, encoding: .utf8)!
            XCTAssertEqual(jsonString, "{\"key1\":\"test-string\"}")
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
}
