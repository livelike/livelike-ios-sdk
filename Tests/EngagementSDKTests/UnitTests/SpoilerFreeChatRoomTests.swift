//
//  SpoilerFreeChatRoomTests.swift
//  EngagementSDKTests
//
//  Created by Jelzon Monzon on 4/16/21.
//

@testable import EngagementSDK
@testable import LiveLikeSwift
import XCTest
import Foundation

class SpoilerFreeChatRoomTests: XCTestCase {
    
    /// Tests that new messages are sorted
    func test_new_messages_are_sorted_before_synchronized() {
        let chatSession = PubSubChatSessionBuilder().build()
        let spoilerFreeChatRoom = SpoilerFreeChatSession(realChatRoom: chatSession, startTimer: false, playerTimeSource: {
            return Date.distantFuture.timeIntervalSince1970
        })
        
        // Recieve messages where videoTimestamp is out of order [10, 15, 5]
        spoilerFreeChatRoom.chatSession(
            chatSession,
            didRecieveNewMessage: ChatMessageBuilder().videoTimestamp(10).build(withID: "1")
        )
        spoilerFreeChatRoom.chatSession(
            chatSession,
            didRecieveNewMessage: ChatMessageBuilder().videoTimestamp(15).build(withID: "2")
        )
        spoilerFreeChatRoom.chatSession(
            chatSession,
            didRecieveNewMessage: ChatMessageBuilder().videoTimestamp(5).build(withID: "3")
        )
        
        // Test that when we synchronize next message the earliest timestamp is shown first
        // Expecting [5, 10, 15]
        XCTAssert(spoilerFreeChatRoom.messages.count == 0)
        spoilerFreeChatRoom.synchronizeNextMessage()
        XCTAssert(spoilerFreeChatRoom.messages[0].id.asString == "3")
        spoilerFreeChatRoom.synchronizeNextMessage()
        XCTAssert(spoilerFreeChatRoom.messages[1].id.asString == "1")
        spoilerFreeChatRoom.synchronizeNextMessage()
        XCTAssert(spoilerFreeChatRoom.messages[2].id.asString == "2")
        
    }
    
    /// Tests that new messages are sorted with messages that have nil pdt
    func test_new_messages_are_sorted_before_synchronized_with_nil_pdt() {
        let chatSession = PubSubChatSessionBuilder().build()
        let spoilerFreeChatRoom = SpoilerFreeChatSession(realChatRoom: chatSession, startTimer: false, playerTimeSource: {
            return Date.distantFuture.timeIntervalSince1970
        })
        
        // Recieve messages where videoTimestamp is out of order [10, 15, nil, 5, nil]
        spoilerFreeChatRoom.chatSession(
            chatSession,
            didRecieveNewMessage: ChatMessageBuilder().videoTimestamp(10).build(withID: "1")
        )
        spoilerFreeChatRoom.chatSession(
            chatSession,
            didRecieveNewMessage: ChatMessageBuilder().videoTimestamp(15).build(withID: "2")
        )
        spoilerFreeChatRoom.chatSession(
            chatSession,
            didRecieveNewMessage: ChatMessageBuilder().build(withID: "4")
        )
        spoilerFreeChatRoom.chatSession(
            chatSession,
            didRecieveNewMessage: ChatMessageBuilder().videoTimestamp(5).build(withID: "3")
        )
        spoilerFreeChatRoom.chatSession(
            chatSession,
            didRecieveNewMessage: ChatMessageBuilder().build(withID: "5")
        )
        
        // Test that when we synchronize next message the earliest timestamp is shown first
        // Expecting [nil, nil, 5, 10, 15]
        XCTAssert(spoilerFreeChatRoom.messages.count == 2)
        XCTAssert(spoilerFreeChatRoom.messages[0].id.asString == "4")
        XCTAssert(spoilerFreeChatRoom.messages[1].id.asString == "5")
        spoilerFreeChatRoom.synchronizeNextMessage()
        XCTAssert(spoilerFreeChatRoom.messages[2].id.asString == "3")
        spoilerFreeChatRoom.synchronizeNextMessage()
        XCTAssert(spoilerFreeChatRoom.messages[3].id.asString == "1")
        spoilerFreeChatRoom.synchronizeNextMessage()
        XCTAssert(spoilerFreeChatRoom.messages[4].id.asString == "2")
        
    }
    
    /// Test loading history when your pdt is in the middle of the range of pdts in history
    /// Tests that history before now time is inserted at front of message list
    func test_history_with_middle_pdt() {
        let now = Date()
        let chatSession = PubSubChatSessionBuilder().build()
        
        let spoilerFreeChatRoom = SpoilerFreeChatSession(realChatRoom: chatSession, startTimer: false, playerTimeSource: {
            return now.timeIntervalSince1970
        })
        
        let loadNextFinished = expectation(description: "load next finished")
        
        spoilerFreeChatRoom.loadNextHistory { result in
            switch result {
            case .success(let messages):
                XCTAssert(spoilerFreeChatRoom.messages[0].id.asString == "1234")
                XCTAssert(messages[0].id.asString == "1234")
                
                loadNextFinished.fulfill()
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }
        
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    /// Tests that the initial history is loaded and is sorted by pdt
    func test_initial_history_is_sorted_by_pdt() {
        let now = Date()
        let chatSession = PubSubChatSessionBuilder()
            .build()
        
        let spoilerFreeChatRoom = SpoilerFreeChatSession(realChatRoom: chatSession, startTimer: false, playerTimeSource: {
            return now.timeIntervalSince1970
        })
        
        let initialLoadExpectation = expectation(description: "initial history load finished")
        
        spoilerFreeChatRoom.loadInitialHistory { result in
            switch result {
            case .success:
                XCTAssert(spoilerFreeChatRoom.messages[0].id.asString == "1234")
                
                initialLoadExpectation.fulfill()
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    /// Tests that the initial history is loaded and is sorted by pdt, with message not having pdt
    func test_initial_history_is_sorted_by_pdt_with_nil_pdt() {
        let now = Date()
        
        let chatSession = PubSubChatSessionBuilder()
            .build()
        
        let spoilerFreeChatRoom = SpoilerFreeChatSession(realChatRoom: chatSession, startTimer: false, playerTimeSource: {
            return now.timeIntervalSince1970
        })
        
        let initialLoadExpectation = expectation(description: "initial history load finished")
        
        spoilerFreeChatRoom.loadInitialHistory { result in
            switch result {
            case .success:
                
                // Ordered messages without PDT
                XCTAssert(spoilerFreeChatRoom.messages[0].id.asString == "1234")
                
                initialLoadExpectation.fulfill()
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    /// Tests that the initial history is loaded with messages that do not have PDT
    func test_initial_history_with_no_pdt() {
        let now = Date()
        
        let chatSession = PubSubChatSessionBuilder()
            .build()
        
        let spoilerFreeChatRoom = SpoilerFreeChatSession(realChatRoom: chatSession, startTimer: false, playerTimeSource: {
            return now.timeIntervalSince1970
        })
        
        let initialLoadExpectation = expectation(description: "initial history load finished")
        
        spoilerFreeChatRoom.loadInitialHistory { result in
            switch result {
            case .success:
                
                // Ordered messages without PDT
                XCTAssert(spoilerFreeChatRoom.messages.count == 1)
                XCTAssert(spoilerFreeChatRoom.messages[0].id.asString == "1234")
                
                // Unsycned should be empty
                XCTAssert(spoilerFreeChatRoom.sortedUnsynchronizedMessages.count == 0)
                
                initialLoadExpectation.fulfill()
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    /// Tests that the next history is loaded and is sorted by pdt, with message not having pdt
    func test_next_history_is_sorted_by_pdt_with_nil_pdt() {
        let now = Date()
        let chatSession = PubSubChatSessionBuilder()
            .build()
        
        let spoilerFreeChatRoom = SpoilerFreeChatSession(realChatRoom: chatSession, startTimer: false, playerTimeSource: {
            return now.timeIntervalSince1970
        })
        
        let initialLoadExpectation = expectation(description: "initial history load finished")
        
        spoilerFreeChatRoom.loadNextHistory { result in
            switch result {
            case .success:
                // Ordered messages without PDT
                XCTAssert(spoilerFreeChatRoom.messages[0].id.asString == "1234")
                
                initialLoadExpectation.fulfill()
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    /// Tests that the next history is with messages not having PDT
    func test_next_history_with_no_pdt_messages() {
        let now = Date()
        
        let chatSession = PubSubChatSessionBuilder().build()
        
        let spoilerFreeChatRoom = SpoilerFreeChatSession(realChatRoom: chatSession, startTimer: false, playerTimeSource: {
            return now.timeIntervalSince1970
        })
        
        let initialLoadExpectation = expectation(description: "initial history load finished")
        
        spoilerFreeChatRoom.loadNextHistory { result in
            switch result {
            case .success:
                
                // Ordered messages without PDT
                XCTAssert(spoilerFreeChatRoom.messages.count == 1)
                XCTAssert(spoilerFreeChatRoom.messages[0].id.asString == "1234")
                
                // Unsycned should be empty
                XCTAssert(spoilerFreeChatRoom.sortedUnsynchronizedMessages.count == 0)
                
                initialLoadExpectation.fulfill()
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    /// tests that new messages will be appended to end after synchronized regardless of timestamp
    func test_new_messages_appended_to_message_list() {
        let now = Date()
        
        let chatSession = PubSubChatSessionBuilder().build()
        
        let spoilerFreeChatRoom = SpoilerFreeChatSession(
            realChatRoom: chatSession,
            startTimer: false,
            playerTimeSource: {
                return now.addingTimeInterval(20).timeIntervalSince1970
            }
        )
        
        let loadInitialHistoryExpectation = expectation(description: "load initial history finished")
        
        spoilerFreeChatRoom.loadInitialHistory { result in
            switch result {
            case .success:
                XCTAssert(spoilerFreeChatRoom.messages[0].id.asString == "1234")
                
                spoilerFreeChatRoom.chatSession(
                    chatSession,
                    didRecieveNewMessage: ChatMessageBuilder()
                        .id("2")
                        .videoTimestamp(now.addingTimeInterval(5).timeIntervalSince1970)
                        .build()
                )
                XCTAssert(spoilerFreeChatRoom.sortedUnsynchronizedMessages[0].id.asString == "2")
                spoilerFreeChatRoom.synchronizeNextMessage()
                XCTAssert(spoilerFreeChatRoom.sortedUnsynchronizedMessages.isEmpty)
                XCTAssert(spoilerFreeChatRoom.messages[1].id.asString == "2")
                
                loadInitialHistoryExpectation.fulfill()
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
            
        }
        waitForExpectations(timeout: 1.0, handler: nil)
        
    }
    
}
