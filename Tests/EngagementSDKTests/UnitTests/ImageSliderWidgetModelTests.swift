//
//  ImageSliderWidgetModelTests.swift
//  EngagementSDKTests
//
//  Created by Jelzon Monzon on 7/2/20.
//

@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift
import XCTest

class ImageSliderWidgetModelTests: XCTestCase {

    let imageSliderResource = ImageSliderCreated(
        baseData: {
            let builder = WidgetMetadataResourceBuilder()
            builder.id = "widget-id-1"
            builder.subscribeChannel = "sub-channel-1"
            builder.programID = "program-id-1"
            return builder.build()
        }(),
        initialMagnitude: "0.5",
        voteUrl: URL(string: "https://google.com")!,
        question: "question?",
        url: URL(string: "https://google.com")!,
        options: [],
        widgetInteractionsUrlTemplate: "",
        sponsors: [MockObjects.sponsorResource],
        engagementCount: 0
    )

    func test_model_data_matches_image_slider_resource() {
        let model: ImageSliderWidgetModel = {
            let builder = ImageSliderBuilder()
            return builder.build(from: imageSliderResource)
        }()

        XCTAssert(model.id == imageSliderResource.id)
        XCTAssert(model.question == imageSliderResource.question)
        XCTAssert(model.customData == imageSliderResource.customData)
        XCTAssert(model.kind == imageSliderResource.kind)
        XCTAssert(model.publishedAt == imageSliderResource.publishedAt)
        XCTAssert(model.createdAt == imageSliderResource.createdAt)
        XCTAssert(model.initialMagnitude == Double(imageSliderResource.initialMagnitude))
        if let averageMagnitude = imageSliderResource.averageMagnitude {
            XCTAssert(model.averageMagnitude == Double(averageMagnitude))
        }

        model.options.enumerated().forEach { index, choice in
            XCTAssert(choice.id == imageSliderResource.options[index].id)
            XCTAssert(choice.imageURL == imageSliderResource.options[index].imageUrl)
        }

        model.sponsors.enumerated().forEach { index, sponsor in
            XCTAssert(sponsor.id == imageSliderResource.sponsors[index].id)
            XCTAssert(sponsor.clientID == imageSliderResource.sponsors[index].clientID)
            XCTAssert(sponsor.name == imageSliderResource.sponsors[index].name)
            XCTAssert(sponsor.logoURL == imageSliderResource.sponsors[index].logoURL)
        }
    }

    func test_imageSlider_registerImpression_makes_livelikeAPI_request() {
        let e = expectation(description: "createImpression called")

        let livelikeAPI = MockLiveLikeRestAPIServices()
        livelikeAPI.createImpressionCompletion = { _, _, _ in
            e.fulfill()
            return Promise()
        }

        let model: ImageSliderWidgetModel = {
            let builder = ImageSliderBuilder()
            builder.livelikeAPI = livelikeAPI
            return builder.build(from: imageSliderResource)
        }()

        model.registerImpression()

        waitForExpectations(timeout: 1.0, handler: nil)
    }

    func test_averageMagnitudeDidChange_called_when_update_recieved() {
        let e = expectation(description: "averageMagnitudeDidChange raised")

        let expectedAverageMagnitude = 0.9
        let widgetClient = MockWidgetClient()
        let delegate = MockImageSliderWidgetModelDelegate()
        delegate.averageMagnitudeDidChangeCompletion = { _, averageMagnitude in
            XCTAssert(averageMagnitude == expectedAverageMagnitude)
            e.fulfill()
        }
        let model = ImageSliderWidgetModel(
            data: imageSliderResource,
            eventRecorder: MockEventRecorder(),
            userProfile: MockUserProfile(),
            rewardItems: [],
            leaderboardManager: LeaderboardsManager(),
            livelikeAPI: MockLiveLikeRestAPIServices(),
            widgetClient: widgetClient,
            widgetInteractionRepo: MockWidgetInteractionRepository()
        )
        model.delegate = delegate

        do {
            let channel = try imageSliderResource.getResultsChannel()
            widgetClient.widgetListeners.publish(channel: channel) {
                $0.publish(
                    event: WidgetProxyPublishData(
                        clientEvent: .imageSliderResults(
                            ImageSliderResults(
                                id: "widget-id-1",
                                averageMagnitude: expectedAverageMagnitude.description
                            )
                        )
                    )
                )
            }
        } catch {
            XCTFail(error.localizedDescription)
        }

        waitForExpectations(timeout: 1.0, handler: nil)
    }
}

class ImageSliderBuilder {

    var eventRecorder: EventRecorder = MockEventRecorder()
    var widgetClient: PubSubWidgetClient = MockWidgetClient()
    var userProfile: UserProfileProtocol = MockUserProfile()
    var rewardItems: [RewardItem] = []
    var leaderboardsManager: LeaderboardsManager = LeaderboardsManager()
    var livelikeAPI: MockLiveLikeRestAPIServices = MockLiveLikeRestAPIServices()
    var widgetInteractionRepo: WidgetInteractionRepository = MockWidgetInteractionRepository()

    func build(from resource: ImageSliderCreated) -> ImageSliderWidgetModel {
        let model = ImageSliderWidgetModel(
            data: resource,
            eventRecorder: eventRecorder,
            userProfile: userProfile,
            rewardItems: rewardItems,
            leaderboardManager: leaderboardsManager,
            livelikeAPI: livelikeAPI,
            widgetClient: widgetClient,
            widgetInteractionRepo: widgetInteractionRepo
        )
        return model
    }
}

class MockImageSliderWidgetModelDelegate: ImageSliderWidgetModelDelegate {

    var averageMagnitudeDidChangeCompletion: (ImageSliderWidgetModel, Double) -> Void = { _, _ in }

    func imageSliderWidgetModel(_ model: ImageSliderWidgetModel, averageMagnitudeDidChange averageMagnitude: Double) {
        averageMagnitudeDidChangeCompletion(model, averageMagnitude)
    }
}
