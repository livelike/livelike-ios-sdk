//
//  WidgetPauseProxyTests.swift
//  LiveLikeTests
//
//  Created by Cory Sullivan on 2019-02-24.
//

@testable import EngagementSDK
@testable import LiveLikeSwift
import Foundation
import XCTest

class WidgetPauseProxyTests: XCTestCase {
    var proxyInputSpy: downStreamWidgetProxySpy!

    override func setUp() {
        proxyInputSpy = downStreamWidgetProxySpy()
    }

    func test_widgets_are_published_immediately_if_not_paused() {
        // Widget time is 1549627071
        let playerTimeSource = PlayerTimeSourceMock(playheadPosition: 1_549_627_068.0)
        playerTimeSource.start()
        let pauseProxy = PauseWidgetProxy(playerTimeSource: playerTimeSource.now, initialPauseStatus: .unpaused)
        pauseProxy.downStreamProxyInput = proxyInputSpy
        let messageExpectation = expectation(description: "downStreamWidgetProxySpy calls this when a message is ready to display")
        proxyInputSpy.messageReadyExpectation = messageExpectation
        pauseProxy.publish(event: .init(clientEvent: .textPrediction()))

        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("Wait for expectation timeout error: \(error)")
                return
            }

            guard self.proxyInputSpy.messageResult != nil else {
                XCTFail("Expected clientSpy to set this value")
                return
            }
            let message =
                """
                Widgets should be sent immediately, therefore
                the starting playhead position should
                equal the current playhead position
                """

            XCTAssertEqual(playerTimeSource.playheadPosition, 1_549_627_068.0, message)
        }
    }

    func test_discarding_non_scheduled_message_during_a_pause() {
        // Widget time is 1549627071
        let playerTimeSource = PlayerTimeSourceMock(playheadPosition: 1_549_627_068.0)
        playerTimeSource.start()
        let pauseProxy = PauseWidgetProxy(playerTimeSource: playerTimeSource.now, initialPauseStatus: .paused)
        pauseProxy.downStreamProxyInput = proxyInputSpy
        let messageExpectation = expectation(description: "downStreamWidgetProxySpy calls this when a message is ready to display")
        proxyInputSpy.messageReadyExpectation = messageExpectation

        let discardExpection = expectation(description: "downStreamWidgetProxySpy calls this when a will be discarded")
        proxyInputSpy.messageDiscardedExpection = discardExpection

        // Send Widget
        pauseProxy.publish(event: .init(clientEvent: .textPredictionNoPDT()))

        // Invert expectation (we should not receive a message)
        messageExpectation.isInverted = true

        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("Wait for expectation timeout error: \(error)")
                return
            }
            XCTAssertNil(self.proxyInputSpy.messageResult, "Message should be nil")
            XCTAssertEqual(self.proxyInputSpy.discardedReason, DiscardedReason.paused)
        }
    }

    func test_discarding_a_scheduled_widget_that_was_published_during_a_pause() {
        // Widget time is 1549627071
        let playerTimeSource = PlayerTimeSourceMock(playheadPosition: 1_549_627_068.0)
        playerTimeSource.start()
        let pauseProxy = PauseWidgetProxy(playerTimeSource: playerTimeSource.now, initialPauseStatus: .paused)

        pauseProxy.downStreamProxyInput = proxyInputSpy
        let messageExpectation = expectation(description: "downStreamWidgetProxySpy calls this when a message is ready to display")
        proxyInputSpy.messageReadyExpectation = messageExpectation

        let discardExpection = expectation(description: "downStreamWidgetProxySpy calls this when a will be discarded")
        proxyInputSpy.messageDiscardedExpection = discardExpection

        // Advance playhead position by seconds
        playerTimeSource.playheadPosition += 5

        // Send Widget
        pauseProxy.publish(event: .init(clientEvent: .textPrediction()))

        // Invert expectation (we should not receive a message)
        messageExpectation.isInverted = true

        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("Wait for expectation timeout error: \(error)")
                return
            }

            XCTAssertNil(self.proxyInputSpy.messageResult)
            XCTAssertEqual(self.proxyInputSpy.discardedReason, DiscardedReason.paused)
        }
    }

    func test_discarding_a_scheduled_widget_that_was_published_during_a_past_pause() {
        // Widget time is 1549627071
        let playerTimeSource = PlayerTimeSourceMock(playheadPosition: 1_549_627_068.0)
        playerTimeSource.start()
        let pauseProxy = PauseWidgetProxy(playerTimeSource: playerTimeSource.now, initialPauseStatus: .unpaused)
        pauseProxy.downStreamProxyInput = proxyInputSpy
        let messageExpectation = expectation(description: "downStreamWidgetProxySpy calls this when a message is ready to display")
        proxyInputSpy.messageReadyExpectation = messageExpectation

        let discardExpection = expectation(description: "downStreamWidgetProxySpy calls this when a will be discarded")
        proxyInputSpy.messageDiscardedExpection = discardExpection

        // Pause Widgets
        pauseProxy.pauseStatusDidChange(status: .paused)

        // Advance playhead position by seconds
        playerTimeSource.playheadPosition += 5

        // Resume Widgets
        pauseProxy.pauseStatusDidChange(status: .unpaused)

        // Send Widget
        pauseProxy.publish(event: .init(clientEvent: .textPrediction()))

        // Invert expectation (we should not receive a message)
        messageExpectation.isInverted = true

        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("Wait for expectation timeout error: \(error)")
                return
            }

            XCTAssertNil(self.proxyInputSpy.messageResult)
            XCTAssertEqual(self.proxyInputSpy.discardedReason, DiscardedReason.paused)
        }
    }
}
