//
//  ChatClientTests.swift
//  EngagementSDKTests
//
//  Created by Keval Shah on 08/11/21.
//

import XCTest
@testable import EngagementSDK
@testable import LiveLikeSwift
@testable import LiveLikeCore

class ChatClientTests: XCTestCase {

    enum chatClientError: LocalizedError {
        case failure
    }

    let blockProfileResources: [BlockInfo] = [
        BlockInfo(id: MockObjects.mockString, url: MockObjects.mockURL, blockedProfileID: MockObjects.mockString, blockedByProfileID: MockObjects.mockString, blockedProfile: MockLiveLikeRestAPIServices.profileResource, blockedByProfile: MockLiveLikeRestAPIServices.profileResource),
        MockLiveLikeRestAPIServices.blockedProfileResource,
        MockLiveLikeRestAPIServices.blockedProfileResource
    ]

    let chatRoomInvitationResources: [ChatRoomInvitation] = [
        MockLiveLikeRestAPIServices.chatRoomInvitation,
        MockLiveLikeRestAPIServices.chatRoomInvitation,
        MockLiveLikeRestAPIServices.chatRoomInvitation,
        MockLiveLikeRestAPIServices.chatRoomInvitation
    ]

    var successfulAppConfig = ApplicationConfiguration(
        name: "test",
        clientId: "client-id",
        pubnubPublishKey: nil,
        pubnubSubscribeKey: "",
        sessionsUrl: URL(string: "https://livelike.com/")!,
        profileUrl: URL(string: "https://livelike.com/")!,
        profileDetailUrlTemplate: "Hello bad url",
        stickerPacksUrl: URL(string: "https://livelike.com/")!,
        programsUrl: URL(string: "https://livelike.com/")!,
        programDetailUrlTemplate: "",
        chatRoomDetailUrlTemplate: "",
        mixpanelToken: nil,
        analyticsProperties: [:],
        pubnubOrigin: nil,
        organizationId: "",
        organizationName: "",
        createChatRoomUrl: "",
        widgetDetailUrlTemplate: "",
        leaderboardDetailUrlTemplate: "",
        pubnubHeartbeatInterval: 0,
        pubnubPresenceTimeout: 0,
        badgesUrl: MockObjects.mockURL,
        rewardItemsUrl: MockObjects.mockURL,
        chatRoomsInvitationsUrl: URL(string: "https://livelike.com/")!,
        chatRoomInvitationDetailUrlTemplate: "Hello Bad Url",
        createChatRoomInvitationUrl: URL(string: "https://livelike.com/")!,
        profileChatRoomReceivedInvitationsUrlTemplate: "Hello Bad Url",
        profileChatRoomSentInvitationsUrlTemplate: "Hello Bad Url",
        profileSearchUrl: "Hello Bad Url",
        rewardTransactionsUrl: MockObjects.mockURL,
        pinnedMessagesUrl: URL(string: "https://livelike.com/")!,
        sponsorsUrl: MockObjects.mockURL,
        redemptionKeysUrl: MockObjects.mockURL,
        redemptionKeyDetailByCodeUrlTemplate: MockObjects.mockString,
        redemptionKeyDetailUrlTemplate: MockObjects.mockString,
        questsUrl: MockObjects.mockURL,
        userQuestsUrlTemplate: MockObjects.mockString,
        userQuestDetailUrlTemplate: MockObjects.mockString,
        userQuestsUrl: MockObjects.mockURL,
        userQuestTaskProgressUrl: MockObjects.mockURL,
        userQuestRewardsUrl: MockObjects.mockURL,
        reactionSpaceDetailUrlTemplate: MockObjects.mockString,
        reactionSpacesUrl: MockObjects.mockURL,
        reactionPackDetailUrlTemplate: MockObjects.mockString,
        reactionPacksUrl: MockObjects.mockURL,
        invokedActionsUrl: MockObjects.mockURL,
        rewardActionsUrl: MockObjects.mockURL,
        profileLeaderboardsUrlTemplate: MockObjects.mockString,
        profileLeaderboardViewsUrlTemplate: MockObjects.mockString,
        chatRoomEventsUrl: MockObjects.mockURL,
        apiPollingInterval: 10,
        programCustomIdUrlTemplate: MockObjects.mockString,
        badgeProfilesUrl: MockObjects.mockURL,
        commentBoardsUrl: MockObjects.mockURL,
        commentBoardDetailUrlTemplate: MockObjects.mockString,
        profileRelationshipsUrl: MockObjects.mockURL,
        profileRelationshipTypesUrl: MockObjects.mockURL,
        profileRelationshipDetailUrlTemplate: MockObjects.mockString,
        commentBoardBanUrl: MockObjects.mockURL,
        commentBoardBanDetailUrlTemplate: MockObjects.mockString,
        commentReportUrl: MockObjects.mockURL,
        commentReportDetailUrlTemplate: MockObjects.mockString,
        autoclaimInteractionRewards: false,
        chatRoomMembershipsUrl: MockObjects.mockURL,
        savedContractAddressesUrl: MockObjects.mockURL,
        textPollsUrl: MockObjects.mockURL,
        imagePollsUrl: MockObjects.mockURL,
        textPredictionsUrl: MockObjects.mockURL,
        imagePredictionsUrl: MockObjects.mockURL,
        alertsUrl: MockObjects.mockURL,
        textAsksUrl: MockObjects.mockURL,
        textQuizzesUrl: MockObjects.mockURL,
        imageQuizzesUrl: MockObjects.mockURL,
        richPostsUrl: MockObjects.mockURL,
        imageNumberPredictionsUrl: MockObjects.mockURL
    )

    // MARK: - addNewMemberToChatRoom tests
    func test_add_new_member_to_chatroom_success() {
        let e = expectation(description: "Successfully added a member to a chatroom")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getChatRoomMemberPromise = Promise(value: MockLiveLikeRestAPIServices.chatRoomMember)

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()

        sdk.chat.addNewMemberToChatRoom(roomID: MockObjects.mockString, profileID: MockObjects.mockString) { result in
            switch result {
            case .success:
                e.fulfill()
            case .failure(let error):
                XCTFail("Failed to add the user to the chatroom - \(error)")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_add_new_member_to_chatroom_fail() {
        let e = expectation(description: "Failed to add a member to a chatroom")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getChatRoomMemberPromise = Promise(error: chatClientError.failure)

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()

        sdk.chat.addNewMemberToChatRoom(roomID: MockObjects.mockString, profileID: MockObjects.mockString) { result in
            switch result {
            case .success:
                XCTFail("Failed facing an error adding the user")
            case .failure:
                e.fulfill()
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    // MARK: - Chat Room Invitation tests
    func test_update_chatroom_invitation_status_success() {
        let e = expectation(description: "Successfully updated the invitation status")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.sendChatRoomInvitePromise = Promise(value: MockLiveLikeRestAPIServices.chatRoomInvitation)

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()

        sdk.chat.updateChatRoomInviteStatus(chatRoomInvitation: MockLiveLikeRestAPIServices.chatRoomInvitation, invitationStatus: .accepted) { result in
            switch result {
            case .success:
                e.fulfill()
            case .failure(let error):
                XCTFail("Failed updating the status - \(error)")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_update_chatroom_invitation_status_fail() {
        let e = expectation(description: "Failed to update the invitation status")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.sendChatRoomInvitePromise = Promise(error: chatClientError.failure)

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()

        sdk.chat.updateChatRoomInviteStatus(chatRoomInvitation: MockLiveLikeRestAPIServices.chatRoomInvitation, invitationStatus: .accepted) { result in
            switch result {
            case .success:
                XCTFail("Failed facing an error inviting the user")
            case .failure:
                e.fulfill()
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    // MARK: - Block User Profile tests
    func test_block_profile_success() {
        let e = expectation(description: "Successfully blocked user profile")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.blockInfoPromise = Promise(value: MockLiveLikeRestAPIServices.blockedProfileResource)

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()

        sdk.chat.blockProfile(profileID: MockObjects.mockString) { result in
            switch result {
            case .success:
                e.fulfill()
            case .failure(let error):
                XCTFail("Failed updating the status - \(error)")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_block_profile_fail() {
        let e = expectation(description: "Successfully updated the invitation status")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.sendChatRoomInvitePromise = Promise(error: ChatClientError.invalidBlockProfileURLTemplate)

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()

        sdk.chat.blockProfile(profileID: MockObjects.mockString) { result in
            switch result {
            case .success:
                e.fulfill()
            case .failure(let error):
                XCTFail("Failed updating the status - \(error)")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_unblock_profile_success() {
        let e = expectation(description: "Successfully updated the invitation status")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.unblockInfoPromise = Promise(value: true)

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()

        sdk.chat.unblockProfile(blockRequestID: MockObjects.mockString) { result in
            switch result {
            case .success:
                e.fulfill()
            case .failure(let error):
                XCTFail("Failed updating the status - \(error)")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_unblock_profile_fail() {
        let e = expectation(description: "Successfully updated the invitation status")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.sendChatRoomInvitePromise = Promise(error: chatClientError.failure)

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()

        sdk.chat.unblockProfile(blockRequestID: MockObjects.mockString) { result in
            switch result {
            case .success:
                e.fulfill()
            case .failure(let error):
                XCTFail("Failed updating the status - \(error)")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_get_blocked_profile_list_success() {
        let e = expectation(description: "Successfully retrieved all profiles blocked by the user")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getBlockedProfilesListPromise = Promise(
            value: PaginatedResult<BlockInfo>(
                previous: nil,
                count: blockProfileResources.count,
                next: nil,
                items: blockProfileResources
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()

        sdk.chat.getBlockedProfileList(blockedProfileID: MockObjects.mockString, page: .first) { result in
            switch result {
            case .success(let result):
                let secondProfile = result[1]
                if secondProfile.id == self.blockProfileResources[1].id, secondProfile.blockedProfileID == self.blockProfileResources[1].blockedProfileID {
                    e.fulfill()
                } else {
                    XCTFail("Failed retrieving blocked profiles list due to wrong result")
                }
            case .failure(let error):
                XCTFail("Failed retrieving blocked profiles - \(error)")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_get_blocked_profile_list_fail() {
        let e = expectation(description: "Successfully retrieved all profiles blocked by the user")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getBlockedProfilesListPromise = Promise(error: chatClientError.failure)

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()

        sdk.chat.getBlockedProfileList(blockedProfileID: MockObjects.mockString, page: .first) { result in
            switch result {
            case .success:
                XCTFail("Failed causing error to recieve block profiles")
            case .failure:
                e.fulfill()
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_get_block_profile_info_success() {
        let e = expectation(description: "Successfully received blocked user profile")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getBlockedProfilesListPromise = Promise(
            value: PaginatedResult<BlockInfo>(
                previous: nil,
                count: blockProfileResources.count,
                next: nil,
                items: blockProfileResources
            )
        )

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()

        sdk.chat.getProfileBlockInfo(profileID: MockObjects.mockString) { result in
            switch result {
            case .success:
                e.fulfill()
            case .failure(let error):
                XCTFail("Failed updating the status - \(error)")
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_get_block_profile_info_fail() {
        let e = expectation(description: "Failed to receive block profile info")
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.getBlockedProfilesListPromise = Promise(error: chatClientError.failure)

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()

        sdk.chat.getProfileBlockInfo(profileID: MockObjects.mockString) { result in
            switch result {
            case .success:
                XCTFail("Failed to induce failure of getting blockInfo")
            case .failure:
                e.fulfill()
            }
        }

        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_get_token_gated_chat_access_details_success() {
        let e = expectation(description: "Successfully received Token gated chat access details")
        let roomID = "roomid_test"
        let walletAddress = "wallestaddress_test"
        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.checkTokenGatedChatAccessPromise = Promise(value: TokenGatedChat(
            chatRoomID: roomID,
            access: TokenGatedChatAccessType.allowed,
            walletDetails: WalletDetails(walletAddress: "123", contractBalances: [ContractBalance(contractAddress: "123", gatePassed: true, failedAttributes: [], tokenType: BlockChainTokenType.nonfungible, networkType: .ethereum, balance: 0, metadata: NFTMetadata(attributes: [NFTAttribute(traitType: "trait1", value: "value1")], nftWithAttributesCount: 1))]),
            details: "test message"
        ))

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()

        sdk.chat.getTokenGatedChatAccessDetails(roomID: roomID, walletAddress: walletAddress) { result in
            switch result {
            case .success(let chatAccess):
                if chatAccess.access == TokenGatedChatAccessType.allowed {
                    e.fulfill()
                }
            case .failure:
                XCTFail("Getting Token gated chat access Info Failed")
            }
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_get_token_gated_chat_access_details_fail() {
        let e = expectation(description: "Successfully failed to receive Token gated chat access details")

        let mockLLRestAPIServices = MockLiveLikeRestAPIServices()
        mockLLRestAPIServices.checkTokenGatedChatAccessPromise = Promise(error: NetworkClientError.badRequest())

        let sdk: LiveLike = {
            let builder = EngagementSDKBuilder()
            builder.mockLLRestAPIServices = mockLLRestAPIServices
            return builder.build()
        }()

        sdk.chat.getTokenGatedChatAccessDetails(roomID: "123", walletAddress: "1234") { result in
            switch result {
            case .success:
                XCTFail("Testing Token gated chat access info failure... has failed ")
            case .failure:
                e.fulfill()
            }
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
}
