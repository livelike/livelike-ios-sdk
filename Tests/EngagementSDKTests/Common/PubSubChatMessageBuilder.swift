//
//  PubSubChatMessageBuilder.swift
//  EngagementSDKTests
//
//  Created by Jelzon Monzon on 5/13/20.
//

@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift
import Foundation

class PubSubChatMessageBuilder {
    
    var contentFilter: [ChatFilter] = []
    
    var customData: String = ""
    func customData(_ customData: String) -> PubSubChatMessageBuilder {
        self.customData = customData
        return self
    }
    
    var id: String = UUID().uuidString
    func id(_ id: String) -> PubSubChatMessageBuilder {
        self.id = id
        return self
    }
    
    var programDateTime: Date?
    func programDateTime(_ programDateTime: Date) -> PubSubChatMessageBuilder {
        self.programDateTime = programDateTime
        return self
    }
    
    var createdAt: Date = Date()
    func createdAt(_ createdAt: Date) -> PubSubChatMessageBuilder {
        self.createdAt = createdAt
        return self
    }
    
    var senderID: String = UUID().uuidString
    func senderID(_ senderID: String) -> PubSubChatMessageBuilder {
        self.senderID = senderID
        return self
    }
    
    func build() -> PubSubChatMessage {
        let pubsubchatMessage = PubSubChatMessage(
            event: .messageCreated,
            payload: .init(
                id: id,
                clientMessageId: id,
                message: "",
                imageUrl: nil,
                imageHeight: nil,
                imageWidth: nil,
                senderId: self.senderID,
                senderNickname: "",
                senderImageUrl: nil,
                programDateTime: programDateTime,
                filteredMessage: nil,
                contentFilter: contentFilter,
                messageEvent: .messageCreated,
                quoteMessage: nil,
                chatRoomId: "",
                messageMetadata: nil,
                parentMessageId: nil,
                repliesCount: nil,
                repliesUrl: nil,
                customData: nil
                
                
            )
        )
        return pubsubchatMessage
    }
    
    func buildCustomMessage() -> [String: Any] {
        let message = PubSubChatMessage(
            event: .customMessageCreated,
            payload: .init(
                id: id,
                clientMessageId: id,
                message: nil,
                imageUrl: nil,
                imageHeight: nil,
                imageWidth: nil,
                senderId: "",
                senderNickname: "",
                senderImageUrl: nil,
                programDateTime: programDateTime,
                filteredMessage: nil,
                contentFilter: nil,
                messageEvent: .customMessageCreated,
                quoteMessage: nil,
                chatRoomId: nil,
                messageMetadata: nil,
                parentMessageId: nil,
                repliesCount: nil,
                repliesUrl: nil,
                customData: customData
            )
        )
        let encoder = LLJSONEncoder()
        let data = try! encoder.encode(message) // swiftlint:disable:this force_try
        return try! JSONSerialization.jsonObject(with: data, options: []) as! [String: Any] // swiftlint:disable:this force_try force_cast
    }
    
    func buildAsJSONDict() -> [String: Any] {
        let pubsubChatMessage = build()
        
        let encoder = LLJSONEncoder()
        let data = try! encoder.encode(pubsubChatMessage) // swiftlint:disable:this force_try
        return try! JSONSerialization.jsonObject(with: data, options: []) as! [String: Any] // swiftlint:disable:this force_try force_cast
    }
    
    func buildQuoteTextMessage() -> ChatMessagePayload {
        let quoteMessage = ChatMessagePayload(
            id: "1234",
            message: MockObjects.mockString,
            senderID: MockObjects.mockString,
            senderNickname: MockObjects.mockString,
            senderImageURL: MockObjects.mockURL,
            programDateTime: programDateTime,
            filteredMessage: nil,
            contentFilter: nil,
            imageURL: nil,
            imageHeight: nil,
            imageWidth: nil,
            customData: nil,
            parentMessageId: nil,
            repliesCount: nil,
            repliesUrl: nil
        )
        return quoteMessage
    }
    
    func buildQuoteImageMessage() -> ChatMessagePayload {
        let quoteMessage = ChatMessagePayload(
            id: "1234",
            message: nil,
            senderID: MockObjects.mockString,
            senderNickname: MockObjects.mockString,
            senderImageURL: MockObjects.mockURL,
            programDateTime: programDateTime,
            filteredMessage: nil,
            contentFilter: nil,
            imageURL: MockObjects.mockURL,
            imageHeight: 50,
            imageWidth: 50,
            customData: nil,
            parentMessageId: nil,
            repliesCount: nil,
            repliesUrl: nil
        )
        return quoteMessage
    }
}
