//
//  AnalyticsMocks.swift
//  EngagementSDKTests
//
//  Created by Jelzon Monzon on 7/2/20.
//

@testable import EngagementSDK
@testable import LiveLikeSwift

class MockEventRecorder: EventRecorder {

    var recordCompletion: ((AnalyticsEvent) -> Void)?

    func record(_ event: AnalyticsEvent) {
        recordCompletion?(event)
    }

}
