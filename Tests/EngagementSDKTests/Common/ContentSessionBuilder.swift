//
//  ContentSessionBuilder.swift
//  EngagementSDKTests
//
//  Created by Jelzon Monzon on 4/10/20.
//

import Foundation
@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift

class ContentSessionBuilder {
    
    private var sdk = EngagementSDKBuilder().build()
    private var sessionConfig = SessionConfiguration(programID: "")
    private var analytics: EventRecorder = MockAnalytics()
    private var reactionVendor: ReactionVendor = LocalReactionsVendor()
    private var accessTokenVendor: AccessTokenVendor = AlwaysSuccessAccessTokenVendor()
    private var stickerRepo = StickerRepository(stickerPacksURL: URL(string: "https://livelike.com")!)
    private var programDetailVendor: ProgramDetailVendor =
        AlwaysSuccessProgramDetailVendor(programDetails: ProgramDetailBuilder().build())
    private var nicknameVendor: UserNicknameVendor = AlwaysSuccessNickname()
    private var livelikeIDVendor: LiveLikeIDVendor = AlwaysSuccessLiveLikeID()
    private var widgetVotes = WidgetVotes()
    private var widgetClient: PubSubWidgetClient = MockWidgetClient()
    private var networking: LLNetworking = MockNetworking()
    
    func analytics(
        _ analytics: EventRecorder
    ) -> ContentSessionBuilder {
        self.analytics = analytics
        return self
    }
    
    func config(
        _ config: SessionConfiguration
    ) -> ContentSessionBuilder {
        self.sessionConfig = config
        return self
    }
    
    func reactionVendor(
        _ reactionVendor: ReactionVendor
    ) -> ContentSessionBuilder {
        self.reactionVendor = reactionVendor
        return self
    }

    func livelikeRestAPIService(
        _ livelikeRestAPIService: MockLiveLikeRestAPIServices
    ) -> ContentSessionBuilder {
        let builder = EngagementSDKBuilder()
        builder.mockLLRestAPIServices = livelikeRestAPIService
        self.sdk = builder.build()
        return self
    }
    
    func networking(
        _ networking: LLNetworking
    ) -> ContentSessionBuilder {
        self.networking = networking
        return self
    }
    
    func accessTokenVendor(
        _ accessTokenVendor: AccessTokenVendor
    ) -> ContentSessionBuilder {
        self.accessTokenVendor = accessTokenVendor
        return self
    }
    
    func widgetClient(
        _ widgetClient: PubSubWidgetClient
    ) -> ContentSessionBuilder {
        self.widgetClient = widgetClient
        return self
    }
    
    func programDetail(
        _ programDetail: ProgramDetailResource
    ) -> ContentSessionBuilder {
        self.programDetailVendor = AlwaysSuccessProgramDetailVendor(programDetails: programDetail)
        return self
    }
    
    func programDetailVendor(
        _ programDetailVendor: ProgramDetailVendor
    ) -> ContentSessionBuilder {
        self.programDetailVendor = programDetailVendor
        return self
    }
    
    func build() -> ContentSession {
        return InternalContentSession(
            sdkInstance: sdk,
            config: sessionConfig,
            whenWidgetClient: Promise(value: widgetClient),
            nicknameVendor: nicknameVendor,
            programDetailVendor: programDetailVendor,
            eventRecorder: analytics,
            widgetVotes: widgetVotes,
            leaderboardsManager: LeaderboardsManager(),
            networking: networking
        )
    }
    
}
