//
//  MockReactionVendor.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 12/1/22.
//

import Foundation
@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift

class MockReactionVendor: ReactionVendor {

    init() {}

    func getReactions() -> Promise<[Reaction]> {
        return Promise(value: [])
    }

}
