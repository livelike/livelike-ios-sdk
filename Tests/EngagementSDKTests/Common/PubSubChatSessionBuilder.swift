//
//  PubSubChatSessionBuilder.swift
//  EngagementSDKTests
//
//  Created by Jelzon Monzon on 5/13/20.
//

@testable import EngagementSDK
@testable import LiveLikeSwift
@testable import LiveLikeCore
import Foundation

class PubSubChatSessionBuilder {
    
    var chatRoomResource: ChatRoomResource = ChatRoomResource(
        id: "",
        channels: ChatRoomResource.Channels(
            chat: ChatRoomResource.Channel(pubnub: ""),
            reactions: nil,
            control: nil
        ),
        uploadUrl: URL(string: "https://google.com")!,
        reportMessageUrl: URL(string: "https://google.com")!,
        stickerPacksUrl: URL(string: "https://google.com")!,
        reactionPacksUrl: URL(string: "https://google.com")!,
        membershipsUrl: URL(string: "https://google.com")!,
        mutedStatusUrlTemplate: "https://google.com",
        visibility: .everyone,
        customMessagesUrl: URL(string: "https://google.com")!,
        sponsorsUrl: MockObjects.mockURL,
        chatroomMessagesUrl: MockObjects.mockURL,
        chatroomMessagesCountUrl: MockObjects.mockURL,
        deleteMessageUrl: MockObjects.mockURL,
        tokenGates: [],
        reactionSpaceId: MockObjects.mockString,
        chatroomTokenGateAccessUrlTemplate: "https://google.com",
        chatroomAccessRuleUrl: MockObjects.mockURL
    )
    
    var chatChannel: PubSubChannel = MockPubSubChannel()
    func chatChannel(_ chatChannel: PubSubChannel) -> PubSubChatSessionBuilder {
        self.chatChannel = chatChannel
        return self
    }
    var controlChannel: PubSubChannel = MockPubSubChannel()
    
    var chatApi: LiveLikeChatAPIProtocol = MockLiveLikeRestAPIServices()
    
    var imageUploader: ImageUploader = ImageUploader(
        uploadUrl: URL(string: "https://google.com")!,
        networking: MockNetworking(),
        accessToken: AccessToken(fromString: "")
    )
    var chatFilters: Set<ChatFilter> = Set()
    var mediaRepository: MediaRepository = MediaRepository(cache: Cache.shared)
    var shouldDisplayAvatar: Bool = false
    var enableChatMessageURLs: Bool = true
    var chatMessageUrlPatterns: String?
    var chatClient: ChatClient = MockChatClient()
    var userClient: UserClient = MockUserClient()
    var reactionClient: ReactionClient = MockReactionClient()
    var previousHistoryPageURL: URL? = MockObjects.mockURL
    var includeFilteredMessages = false
    var currentUserID = ""
    var networking: LLNetworking = MockNetworking()
    
    var messages: [ChatMessage] = []
    func messages(_ messages: [ChatMessage]) -> PubSubChatSessionBuilder {
        self.messages = messages
        return self
    }
    
    var nickNameVendor = AlwaysSuccessNickname()
    
    func build() -> PubSubChatRoom {
        let chatRoom = PubSubChatRoom(
            roomID: "",
            chatChannel: chatChannel,
            controlChannel: controlChannel,
            userID: currentUserID,
            userNickname: nickNameVendor.currentNickname ?? "No Nickname",
            nicknameVendor: nickNameVendor,
            imageUploader: self.imageUploader,
            analytics: MockAnalytics(),
            reactionsVendor: MockReactionVendor(),
            messageReporter: nil,
            title: "",
            accessToken: AccessToken(fromString: ""),
            chatFilters: chatFilters,
            stickerRepository: StickerRepository(stickerPacksURL: chatRoomResource.stickerPacksUrl),
            shouldDisplayAvatar: shouldDisplayAvatar,
            enableChatMessageURLs: enableChatMessageURLs,
            chatMessageUrlPatterns: chatMessageUrlPatterns,
            chatAPI: chatApi,
            customMessagesUrl: chatRoomResource.customMessagesUrl,
            pinnedMessagesURL: URL(string: "https://google.com")!,
            chatroomMessageURL: MockObjects.mockURL,
            chatroomMessagesCountURL: MockObjects.mockURL,
            deleteMessageURL: MockObjects.mockURL,
            chatClient: chatClient,
            userClient: userClient,
            reactionClient: reactionClient,
            includeFilteredMessages: includeFilteredMessages,
            chatRoomEventsURL: MockObjects.mockURL,
            apiPollingInterval: 10,
            reactionSpaceID: MockObjects.mockString,
            networking: networking,
            previousHistoryPageURL: previousHistoryPageURL
        )
        chatRoom.messages = messages
        return chatRoom
    }
}
