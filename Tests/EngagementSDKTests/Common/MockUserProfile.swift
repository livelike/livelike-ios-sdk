//
//  MockUserProfile.swift
//  EngagementSDKTests
//
//  Created by Jelzon Monzon on 9/2/20.
//

@testable import EngagementSDK
@testable import LiveLikeSwift
import Foundation

class MockUserProfile: UserProfileProtocol {
    var userID: LiveLikeID = LiveLikeID(from: "user-id-1")
    var accessToken: AccessToken = AccessToken(fromString: "access-token-1")
    var nickname: String = "nickname"
    var customData: String? = nil

    func notifyRewardItemsEarned(rewards: [Reward]) { }

    static func mockUserProfileResource(id: String = "1", nickname: String = "J Man", customData: String? = nil) -> ProfileResource {
        return ProfileResource(
            id: id,
            nickname: nickname,
            chatRoomMembershipsUrl: MockObjects.mockURL,
            customData: customData,
            reportedCount: 0,
            rewardItemTransferUrl: MockObjects.mockURL,
            rewardItemBalancesUrl: MockObjects.mockURL,
            badgesUrl: MockObjects.mockURL,
            badgeProgressUrl: MockObjects.mockURL,
            subscribeChannel: "",
            blockProfileUrl: MockObjects.mockURL,
            blockedProfilesTemplateUrl: "https://www.google.com",
            blockedProfileIdsUrl: MockObjects.mockURL,
            leaderboardsUrl: MockObjects.mockURL,
            leaderboardViewsUrl: MockObjects.mockURL
        )
    }
}
