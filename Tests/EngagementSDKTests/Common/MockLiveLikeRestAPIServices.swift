//
//  EngagementSDKTestServices.swift
//  EngagementSDKTests
//
//  Created by Mike Moloksher on 5/21/20.
//

@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift
import Foundation

class MockLiveLikeRestAPIServices: LiveLikeCoreAPIProtocol, LiveLikeChatAPIProtocol, LiveLikeWidgetAPIProtocol, LiveLikeLeaderboardAPIProtocol, LiveLikeReactionAPIProtocol {
    
    func createReactionSpace(reactionSpaceURL: URL, name: String?, targetGroupID: String, reactionPackIDs: [String], accessToken: AccessToken) -> Promise<ReactionSpace> {
        return createReactionSpacePromise
    }
    
    func getReactionSpaces(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<ReactionSpace>> {
        return getReactionSpacesPromise
    }
    
    func updateReactionSpace(reactionSpaceURL: URL, reactionPackIDs: [String], accessToken: AccessToken) -> Promise<UpdateReactionSpaceResult> {
        return updateReactionSpacePromise
    }
    
    func deleteReactionSpace(reactionSpaceURL: URL, accessToken: AccessToken) -> Promise<Bool> {
        return Promise(value: true)
    }
    
    func getReactionSpaceInfo(reactionSpaceURL: URL, accessToken: AccessToken) -> Promise<ReactionSpace> {
        return getReactionSpaceInfoPromise
    }
    
    func getReactionPacks(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<ReactionPack>> {
        return getReactionPacksPromise
    }
    
    func getReactionPackInfo(reactionPackURL: URL, accessToken: AccessToken) -> Promise<ReactionPack> {
        return getReactionPackInfoPromise
    }
    
    func addUserReactions(userReactionURL: URL, reactionSpaceID: String, targetID: String, reactionID: String, customData: String?, accessToken: AccessToken) -> Promise<UserReaction> {
        return addUserReactionPromise
    }
    
    func removeUserReactions(userReactionURL: URL, accessToken: AccessToken) -> Promise<Bool> {
        return Promise(value: true)
    }
    
    func getUserReactions(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<UserReaction>> {
        return getUserReactionListPromise
    }
    
    func getUserReactionsCount(userReactionsCountURL: URL, accessToken: AccessToken) -> Promise<PaginatedResult<UserReactionsCountResult>> {
        return getUserReactionCountPromise
    }
    
    static let sampleChatRoomResource: Promise<ChatRoomResource> = {
        return Promise(value: ChatRoomResource(
            id: "123",
            channels: ChatRoomResource.Channels(
                chat: ChatRoomResource.Channel(pubnub: ""),
                reactions: nil,
                control: nil
            ),
            uploadUrl: URL(string: "https://google.com")!,
            title: "Room Test",
            reportMessageUrl: URL(string: "https://google.com")!,
            stickerPacksUrl: URL(string: "https://google.com")!,
            reactionPacksUrl: URL(string: "https://google.com")!,
            membershipsUrl: URL(string: "https://google.com")!,
            mutedStatusUrlTemplate: "https://google.com",
            visibility: .everyone,
            customMessagesUrl: URL(string: "https://google.com")!,
            sponsorsUrl: MockObjects.mockURL,
            chatroomMessagesUrl: MockObjects.mockURL,
            chatroomMessagesCountUrl: MockObjects.mockURL,
            deleteMessageUrl: MockObjects.mockURL,
            tokenGates: [],
            reactionSpaceId: MockObjects.mockString,
            chatroomTokenGateAccessUrlTemplate: "https://google.com",
            chatroomAccessRuleUrl: URL(string: "https://google.com")!
        ))
    }()
    
    static let sampleTokenGatedChat: Promise<TokenGatedChat> = {
        return Promise(value: TokenGatedChat(
            chatRoomID: "123",
            access: TokenGatedChatAccessType.allowed,
            walletDetails: WalletDetails(walletAddress: "123", contractBalances: [ContractBalance(contractAddress: "123", gatePassed: true, failedAttributes: [], tokenType: BlockChainTokenType.nonfungible, networkType: BlockChainNetworkType.ethereum, balance: 0, metadata: NFTMetadata(attributes: [NFTAttribute(traitType: "trait1", value: "value1")], nftWithAttributesCount: 1))]),
            details: "test message"
        ))
    }()
    
    static let successFullAppConfig: Promise<ApplicationConfiguration> = {
        let configuration = ApplicationConfiguration(
            name: "test",
            clientId: "client-id",
            pubnubPublishKey: nil,
            pubnubSubscribeKey: "",
            sessionsUrl: URL(string: "https://livelike.com/")!,
            profileUrl: URL(string: "https://livelike.com/")!,
            profileDetailUrlTemplate: "https://cf-blast-qa.livelikecdn.com/api/v1/profiles/{profile_id}/",
            stickerPacksUrl: URL(string: "https://livelike.com/")!,
            programsUrl: URL(string: "https://livelike.com/")!,
            programDetailUrlTemplate: "",
            chatRoomDetailUrlTemplate: "",
            mixpanelToken: nil,
            analyticsProperties: [:],
            pubnubOrigin: nil,
            organizationId: "",
            organizationName: "",
            createChatRoomUrl: "",
            widgetDetailUrlTemplate: "",
            leaderboardDetailUrlTemplate: "",
            pubnubHeartbeatInterval: 0,
            pubnubPresenceTimeout: 0,
            badgesUrl: MockObjects.mockURL,
            rewardItemsUrl: MockObjects.mockURL,
            chatRoomsInvitationsUrl: URL(string: "https://livelike.com/")!,
            chatRoomInvitationDetailUrlTemplate: "Hello Bad Url",
            createChatRoomInvitationUrl: URL(string: "https://livelike.com/")!,
            profileChatRoomReceivedInvitationsUrlTemplate: "Hello Bad Url",
            profileChatRoomSentInvitationsUrlTemplate: "Hello Bad Url",
            profileSearchUrl: "Hello Bad Url",
            rewardTransactionsUrl: MockObjects.mockURL,
            pinnedMessagesUrl: URL(string: "https://livelike.com/")!,
            sponsorsUrl: MockObjects.mockURL,
            redemptionKeysUrl: MockObjects.mockURL,
            redemptionKeyDetailByCodeUrlTemplate: MockObjects.mockString,
            redemptionKeyDetailUrlTemplate: MockObjects.mockString,
            questsUrl: MockObjects.mockURL,
            userQuestsUrlTemplate: MockObjects.mockString,
            userQuestDetailUrlTemplate: MockObjects.mockString,
            userQuestsUrl: MockObjects.mockURL,
            userQuestTaskProgressUrl: MockObjects.mockURL,
            userQuestRewardsUrl: MockObjects.mockURL,
            reactionSpaceDetailUrlTemplate: MockObjects.mockString,
            reactionSpacesUrl: MockObjects.mockURL,
            reactionPackDetailUrlTemplate: MockObjects.mockString,
            reactionPacksUrl: MockObjects.mockURL,
            invokedActionsUrl: MockObjects.mockURL,
            rewardActionsUrl: MockObjects.mockURL,
            profileLeaderboardsUrlTemplate: MockObjects.mockString,
            profileLeaderboardViewsUrlTemplate: MockObjects.mockString,
            chatRoomEventsUrl: MockObjects.mockURL,
            apiPollingInterval: 10,
            programCustomIdUrlTemplate: MockObjects.mockString,
            badgeProfilesUrl: MockObjects.mockURL,
            commentBoardsUrl: MockObjects.mockURL,
            commentBoardDetailUrlTemplate: MockObjects.mockString,
            profileRelationshipsUrl: MockObjects.mockURL,
            profileRelationshipTypesUrl: MockObjects.mockURL, profileRelationshipDetailUrlTemplate: MockObjects.mockString,
            commentBoardBanUrl: MockObjects.mockURL,
            commentBoardBanDetailUrlTemplate: MockObjects.mockString,
            commentReportUrl: MockObjects.mockURL,
            commentReportDetailUrlTemplate: MockObjects.mockString,
            autoclaimInteractionRewards: false,
            chatRoomMembershipsUrl: MockObjects.mockURL,
            savedContractAddressesUrl: MockObjects.mockURL,
            textPollsUrl: MockObjects.mockURL,
            imagePollsUrl: MockObjects.mockURL,
            textPredictionsUrl: MockObjects.mockURL,
            imagePredictionsUrl: MockObjects.mockURL,
            alertsUrl: MockObjects.mockURL,
            textAsksUrl: MockObjects.mockURL,
            textQuizzesUrl: MockObjects.mockURL,
            imageQuizzesUrl: MockObjects.mockURL,
            richPostsUrl: MockObjects.mockURL,
            imageNumberPredictionsUrl: MockObjects.mockURL
        )
        let promise = Promise<ApplicationConfiguration>(value: configuration)
        return promise
    }()
    
    static let chatRoomMember = ChatRoomMember(
        id: "",
        url: URL(string: "http://www.google.com")!,
        profile: MockUserProfile.mockUserProfileResource()
    )
    static let chatRoomInfo: ChatRoomInfo = ChatRoomInfo(id: "123", title: "", visibility: .everyone, contentFilter: nil, customData: "", tokenGates: [], reactionSpaceID: MockObjects.mockString)
    static let profileResource: ProfileResource = MockUserProfile.mockUserProfileResource()
    static let voteResource: PollWidgetModel.Vote = PollWidgetModel.Vote(
        id: "1234",
        widgetID: UUID().uuidString,
        url: URL(string: "https://livelike.com/")!,
        optionID: "124",
        rewards: [],
        createdAt: Date(),
        widgetKind: .textPrediction
    )
    
    static let chatRoomInvitation = ChatRoomInvitation(
        id: "1234",
        chatRoomID: "5678",
        url: URL(string: "https://www.livelike.com")!,
        createdAt: Date(),
        status: .pending,
        invitedProfile: profileResource,
        chatRoom: chatRoomInfo,
        invitedBy: profileResource,
        invitedProfileID: profileResource.id
    )
    
    static let blockedProfileResource = BlockInfo(
        id: "1234",
        url: URL(string: "https://www.livelike.com")!,
        blockedProfileID: "5678",
        blockedByProfileID: "91011",
        blockedProfile: profileResource,
        blockedByProfile: profileResource
    )
    
    static let messagePayloadResource = ChatMessagePayload(
        id: "1234",
        message: "5678",
        senderID: "91011",
        senderNickname: "test",
        senderImageURL: nil,
        programDateTime: Date(),
        filteredMessage: nil,
        contentFilter: nil,
        imageURL: nil,
        imageHeight: nil,
        imageWidth: nil,
        customData: nil,
        parentMessageId: nil,
        repliesCount: nil,
        repliesUrl: nil
    )
    
    var getChatRoomResourcePromise: Promise<ChatRoomResource> = MockLiveLikeRestAPIServices.sampleChatRoomResource
    var checkTokenGatedChatAccessPromise: Promise<TokenGatedChat> = MockLiveLikeRestAPIServices.sampleTokenGatedChat
    var createChatRoomResourcePromise: Promise<ChatRoomResource> = MockLiveLikeRestAPIServices.sampleChatRoomResource
    var whenApplicationConfig: Promise<ApplicationConfiguration> = MockLiveLikeRestAPIServices.successFullAppConfig
    var getChatRoomMemberPromise: Promise<ChatRoomMember> = Promise(value: MockLiveLikeRestAPIServices.chatRoomMember)
    var sendChatRoomInvitePromise: Promise<ChatRoomInvitation> = Promise(value: MockLiveLikeRestAPIServices.chatRoomInvitation)
    var getChatRoomsInfoPromise: Promise<UserChatRoomMembershipsResult> = Promise(value: UserChatRoomMembershipsResult(chatRooms: [MockLiveLikeRestAPIServices.chatRoomInfo], next: nil, previous: nil))
    var blockInfoPromise: Promise<BlockInfo> = Promise(value: MockLiveLikeRestAPIServices.blockedProfileResource)
    var unblockInfoPromise: Promise<Bool> = Promise(value: true)
    var deleteChatRoomMembershipPromise: Promise<Bool> = Promise(value: true)
    var getWidgetCompletion: (String, WidgetKind) -> Promise<WidgetResource> = { _, _ in Promise() }
    var getProgramDetailPromise: Promise<ProgramDetailResource> = Promise(value: ProgramDetailBuilder().build())
    var getLeaderboardsPromise: Promise<[Leaderboard]> = Promise(value: [])
    var getLeaderboardPromise: Promise<Leaderboard> = Promise(value: MockGamification.mockLeaderboardResource(id: nil))
    var getLeaderboardEntriesPromise: Promise<PaginatedResult<LeaderboardEntryResource>> = Promise(value: MockGamification.mockLeaderboardPaginatedEntryResource())
    var getProfileLeaderboardsPromise: Promise<PaginatedResult<ProfileLeaderboard>> = Promise(value: MockGamification.mockProfileLeaderboardPaginatedResource())
    var getProfileLeaderboardViewsPromise: Promise<PaginatedResult<ProfileLeaderboardView>> = Promise(value: MockGamification.mockProfileLeaderboardPaginatedViewResource())
    var getLeaderboardProfilePromise: Promise<LeaderboardEntryResource> = Promise(value: MockGamification.mockLeaderboardEntryResource(id: nil))
    var createProfileCompletion: () -> Promise<AccessToken> = { Promise(value: AccessToken(fromString: "")) }
    var setNicknameCompletion: () -> Promise<ProfileResource> = { Promise(value: MockLiveLikeRestAPIServices.profileResource) }
    var getProfileCompletion: () -> Promise<ProfileResource> = { Promise(value: MockLiveLikeRestAPIServices.profileResource) }
    var createCheerMeterVoteCompletion: (Int, URL, AccessToken) -> Promise<CheerMeterWidgetModel.Vote> = { _, _, _ in return Promise() }
    var updateCheerMeterVoteCompletion: (Int, URL, AccessToken) -> Promise<CheerMeterWidgetModel.Vote> = {
        _, _, _ in return Promise()
    }
    var getTimelineCompletion: (URL, AccessToken) -> Promise<WidgetTimelineResource> = { _, _ in return Promise() }
    var getTimelineInteractionsCompletion: (URL, AccessToken) -> Promise<WidgetTimelineInteractionsResource> = { _, _ in
        return Promise(value: .init(interactionsByKind: [:]))
    }
    var createImpressionCompletion: (URL, String, AccessToken) -> Promise<ImpressionResponse> = { _, _, _ in return Promise() }
    var createQuizAnswerCompletion: (URL, AccessToken) -> Promise<QuizWidgetModel.Answer> = { _, _ in return Promise() }
    var createPredictionVoteCompletion: (URL, AccessToken, (Result<PredictionVoteResource, Error>) -> Void) -> URLSessionDataTask = { _, _, _ in return URLSessionDataTask() }
    var getVoteCompletion: () -> Promise<PollWidgetModel.Vote> = { Promise(value: MockLiveLikeRestAPIServices.voteResource) }
    var createImageSliderVoteCompletion: (URL, Double, AccessToken) -> Promise<ImageSliderWidgetModel.Vote> = { _, _, _ in return Promise() }
    var getChatUserMutedStatusPromise: Promise<ChatUserMuteStatusResource> = Promise(value: ChatUserMuteStatusResource(isMuted: false))
    var setUserCustomDataPromise: Promise<ProfileResource> = Promise(value: MockUserProfile.mockUserProfileResource())
    var deleteMessagePromise: Promise<Bool> = Promise(value: true)
    var pinMessagePromise: Promise<PinMessageInfo> = Promise(
        value: PinMessageInfo(
            id: "1234",
            url: URL(string: "https://livelike.com/")!,
            messageID: "5678",
            messagePayload: messagePayloadResource,
            chatRoomID: "91011",
            pinnedByID: "1213",
            pinnedBy: profileResource
        )
    )
    var unpinMessagePromise: Promise<Bool> = Promise(value: true)
    
    var getPinMessageInfoListPromise: Promise<PaginatedResult<PinMessageInfo>> = Promise(
        value: PaginatedResult<PinMessageInfo>(
            previous: nil,
            count: 0,
            next: nil,
            items: []
        )
    )
    
    var sendMessageCompletionPromise: Promise<ChatMessage> = Promise(value: ChatMessage(
        id: "1234",
        clientMessageID: "5678",
        roomID: "9012",
        message: "test",
        senderID: "101112",
        senderNickname: "user",
        videoTimestamp: nil,
        reactions: ReactionVotes(allVotes: []),
        timestamp: Date(),
        profileImageUrl: nil,
        createdAt: TimeToken(date: Date()),
        bodyImageUrl: nil,
        bodyImageHeight: nil,
        bodyImageWidth: nil,
        filteredMessage: nil,
        filteredReasons: Set<ChatFilter>(),
        customData: nil,
        quoteMessage: PubSubChatMessageBuilder().buildQuoteTextMessage(),
        messageEvent: .messageCreated,
        pubnubTimetoken: TimeToken(pubnubTimetoken: 0),
        messageMetadata: nil,
        parentMessageID: nil,
        repliesCount: nil,
        repliesURL: nil
    ))
    
    var getMessageCompletionPromise: Promise<GetMessagesResult> = Promise(
        value: GetMessagesResult(
            results: [
                ChatMessageResponse(
                    id: "1234",
                    clientMessageID: "5678",
                    roomID: "9012",
                    senderID: "101112",
                    senderNickname: "user",
                    message: "test",
                    videoTimestamp: nil,
                    reactions: ReactionVotes(allVotes: []),
                    timestamp: Date(),
                    profileImageURL: nil,
                    createdAt: TimeToken(date: Date()),
                    bodyImageURL: nil,
                    bodyImageHeight: nil,
                    bodyImageWidth: nil,
                    filteredMessage: nil,
                    filteredReasons: Set<ChatFilter>(),
                    customData: nil,
                    quoteMessage: PubSubChatMessageBuilder().buildQuoteTextMessage(),
                    messageEvent: .messageCreated,
                    pubnubTimetoken: TimeToken(pubnubTimetoken: 0),
                    senderProfileURL: nil,
                    messageMetadata: nil,
                    repliesCount: nil,
                    repliesUrl: nil,
                    parentMessageId: nil
                )
            ],
            previous: MockObjects.mockURL
        )
    )
    
    var getMessageCountCompletionPromise: Promise<GetMessagesCountResult> = Promise(
        value: GetMessagesCountResult(
            count: 20
        )
    )
    
    var getMessageEventsCompletionPromise: Promise<PaginatedResult<ChatRoomEvent>> = Promise(value: PaginatedResult<ChatRoomEvent>(
        previous: nil,
        count: 0,
        next: nil,
        items: []
    ))
    
    var getSponsorshipCompletion: Result<[Sponsor], Error> = .success([Sponsor(id: "1", clientID: "123", name: "Sponsor Name", logoURL: MockObjects.mockURL, brandColorHex: "ffffff")])
    
    var getSponsorsListCompletion: Result<PaginatedResult<Sponsor>, Error> = .success(PaginatedResult<Sponsor>(
        previous: nil,
        count: 0,
        next: nil,
        items: []
    ))
    
    var getProfileBadgesPromise: Promise<PaginatedResult<ProfileBadge>> = Promise(
        value: PaginatedResult<ProfileBadge>(
            previous: nil,
            count: 0,
            next: nil,
            items: []
        )
    )
    var getApplicationBadgesPromise: Promise<PaginatedResult<Badge>> = Promise(
        value: PaginatedResult<Badge>(
            previous: nil,
            count: 0,
            next: nil,
            items: []
        )
    )
    
    var getBadgeProfilesPromise: Promise<PaginatedResult<BadgeProfile>> = Promise(
        value: PaginatedResult<BadgeProfile>(
            previous: nil,
            count: 0,
            next: nil,
            items: []
        )
    )
    
    var getProfileBadgesProgressPromise: Promise<[BadgeProgress]> = Promise(
        value: [BadgeProgress(
            badge: Badge(
                id: "1",
                name: "badge name",
                description: nil,
                badgeIconURL: MockObjects.mockURL,
                registeredLinks: [MockObjects.mockRegisteredLink]
            ),
            badgeProgression: [BadgeProgression(
                rewardItemID: "itemID",
                rewardItemName: "Reward Item Name",
                rewardItemThreshold: 1,
                currentRewardAmount: 1
            )]
        )]
    )
    
    var updatePredictionVoteCompletion: (String, URL, AccessToken, (Result<PredictionVoteResource, Error>) -> Void) -> URLSessionDataTask = { _, _, _, _ in return URLSessionDataTask() }
    var getUnclaimedWidgetInteractionsCompletion: (URL, AccessToken) -> Promise<PaginatedResult<WidgetInteraction>> = { _, _ in return Promise() }
    
    var createNumberPredictionVoteCompletion: (URL, [NumberPredictionVoteSubmission], AccessToken) -> Promise<NumberPredictionVote> = { _, _, _ in return Promise() }
    func createNumberPredictionVote(
        voteURL: URL,
        votes: [NumberPredictionVoteSubmission],
        accessToken: AccessToken
    ) -> Promise<NumberPredictionVote> {
        return createNumberPredictionVoteCompletion(voteURL, votes, accessToken)
    }
    
    var getTextAskInteractionsCompletion: (_ interactionsURL: URL, _ accessToken: AccessToken) -> Promise<[TextAskWidgetModel.Reply]> = { _, _ in return Promise() }
    
    var createTextAskReplyCompletion: (URL, String, AccessToken) -> Promise<TextAskWidgetModel.Reply> = { _, _, _ in return Promise() }
    
    var sendCustomMessageCompletionPromise: Promise<ChatMessage> = Promise(value: ChatMessageBuilder().build())
    
    var getAllRewardsPromise: Promise<PaginatedResult<RewardItem>> = Promise(
        value: PaginatedResult<RewardItem>(
            previous: nil,
            count: 0,
            next: nil,
            items: []
        )
    )
    
    var getInvitationsForProfilePromise: Promise<PaginatedResult<ChatRoomInvitation>> = Promise(value: PaginatedResult<ChatRoomInvitation>(
        previous: nil,
        count: 0,
        next: nil,
        items: []
    )
    )
    
    var getBlockedProfilesListPromise: Promise<PaginatedResult<BlockInfo>> = Promise(value: PaginatedResult<BlockInfo>(
        previous: nil,
        count: 0,
        next: nil,
        items: []
    )
    )
    
    var getBlockedProfileIDsListPromise: Promise<PaginatedResult<String>> = Promise(value: PaginatedResult<String>(
        previous: nil,
        count: 0,
        next: nil,
        items: []
    )
    )
    
    var transferRewardItemAmountPromise: Promise<RewardItemTransfer> = Promise(value: MockObjects.mockRewardItemTransfer)
    
    var getRewardItemBalancesPromise: Promise<PaginatedResult<RewardItemBalance>> = Promise(
        value: PaginatedResult<RewardItemBalance>(
            previous: nil,
            count: 0,
            next: nil,
            items: [RewardItemBalance(rewardItemID: "123", rewardItemName: "name", rewardItemBalance: 0)]
        )
    )
    
    var getRewardItemTransfersPromise: Promise<PaginatedResult<RewardItemTransfer>> = Promise(
        value: PaginatedResult<RewardItemTransfer>(
            previous: nil,
            count: 0,
            next: nil,
            items: []
        )
    )
    
    var getRewardTransactionsPromise: Promise<PaginatedResult<RewardTransaction>> = Promise(
        value: PaginatedResult<RewardTransaction>(
            previous: nil,
            count: 0,
            next: nil,
            items: []
        )
    )
    
    var getWidgetResourcesCompletion: (URL, AccessToken) -> Promise<PaginatedResult<WidgetResource>> = { _, _ in
        return Promise(
            value: PaginatedResult<WidgetResource>(
                previous: nil,
                count: 0,
                next: nil,
                items: []
            )
        )
    }
    
    var getRedemptionKeysPromise: Promise<PaginatedResult<RedemptionKey>> = Promise(
        value: PaginatedResult<RedemptionKey>(
            previous: nil,
            count: 0,
            next: nil,
            items: []
        )
    )
    
    var getRedeemKeyPromise: Promise<RedemptionKey> = Promise(value: MockObjects.mockRedemptionCode)

    var getQuestsPromise: Promise<PaginatedResult<Quest>> = Promise(
        value: PaginatedResult<Quest>(
            previous: nil,
            count: 0,
            next: nil,
            items: []
        )
    )

    var getUserQuestsPromise: Promise<PaginatedResult<UserQuest>> = Promise(
        value: PaginatedResult<UserQuest>(
            previous: nil,
            count: 0,
            next: nil,
            items: []
        )
    )

    var startUserQuestPromise: Promise<UserQuest> = Promise(value: MockObjects.mockUserQuest)

    var updateUserQuestTasksPromise: Promise<UserQuest> = Promise(value: MockObjects.mockUserQuest)

    var updateUserQuestTaskProgressPromise: Promise<UserQuestTaskProgress> = Promise(
        value: UserQuestTaskProgress(
            id: "",
            customIncrement: nil,
            customProgress: nil,
            userQuest: MockObjects.mockUserQuest
        )
    )
    
    var getQuestRewardsPromise: Promise<PaginatedResult<QuestReward>>!
    var getUserQuestRewardsPromise: Promise<PaginatedResult<UserQuestReward>>!
    var claimUserQuestRewardsPromise: Promise<UserQuest>!
    
    var createReactionSpacePromise: Promise<ReactionSpace> = Promise(
        value: ReactionSpace(
            id: MockObjects.mockString,
            url: MockObjects.mockURL,
            name: MockObjects.mockString,
            targetGroupID: MockObjects.mockString,
            reactionPackIDs: [MockObjects.mockString],
            clientID: MockObjects.mockString,
            reactionSpaceChannel: MockObjects.mockString,
            userReactionsURL: MockObjects.mockURL,
            userReactionDetailURLTemplate: MockObjects.mockString,
            userReactionsCountURL: MockObjects.mockURL
        )
    )
    
    var updateReactionSpacePromise: Promise<UpdateReactionSpaceResult> = Promise(value: UpdateReactionSpaceResult(reactionPackIDs: []))

    var getReactionSpaceInfoPromise: Promise<ReactionSpace> = Promise(
        value: ReactionSpace(
            id: MockObjects.mockString,
            url: MockObjects.mockURL,
            name: MockObjects.mockString,
            targetGroupID: MockObjects.mockString,
            reactionPackIDs: [MockObjects.mockString],
            clientID: MockObjects.mockString,
            reactionSpaceChannel: MockObjects.mockString,
            userReactionsURL: MockObjects.mockURL,
            userReactionDetailURLTemplate: MockObjects.mockString,
            userReactionsCountURL: MockObjects.mockURL
        )
    )

    var getReactionSpacesPromise: Promise<PaginatedResult<ReactionSpace>> = Promise(value: PaginatedResult(previous: nil, count: 0, next: nil, items: []))
    
    var getReactionPackInfoPromise: Promise<ReactionPack> = Promise(value: ReactionPack(id: MockObjects.mockString, emojis: []))
    
    var getReactionPacksPromise: Promise<PaginatedResult<ReactionPack>> = Promise(value: PaginatedResult(previous: nil, count: 0, next: nil, items: []))

    var addUserReactionPromise: Promise<UserReaction> = Promise(value: UserReaction(id: MockObjects.mockString, targetID: MockObjects.mockString, reactedByID: MockObjects.mockString, createdAt: Date(), reactionID: MockObjects.mockString, reactionSpaceID: MockObjects.mockString, customData: MockObjects.mockString, clientID: MockObjects.mockString))

    var getUserReactionListPromise: Promise<PaginatedResult<UserReaction>> = Promise(value: PaginatedResult(previous: nil, count: 0, next: nil, items: []))
    
    var getUserReactionCountPromise: Promise<PaginatedResult<UserReactionsCountResult>> = Promise(value: PaginatedResult(previous: nil, count: 0, next: nil, items: []))
    
    var createCommentBoardPromise: Promise<CommentBoard> = Promise(
        value: CommentBoard(
            id: MockObjects.mockString,
            url: MockObjects.mockURL,
            title: MockObjects.mockString,
            customID: MockObjects.mockString,
            clientID: MockObjects.mockString,
            allowComments: true,
            repliesDepth: 100,
            createdBy: MockObjects.mockString,
            createdAt: Date(),
            commentsURL: MockObjects.mockURL,
            commentDetailUrlTemplate: MockObjects.mockString,
            customData: nil,
            commentReportURL: MockObjects.mockURL,
            commentReportDetailUrlTemplate: MockObjects.mockString,
            dismissReportedCommentUrlTemplate: MockObjects.mockString
        )
    )
    
    var createCommentPromise: Promise<Comment> = Promise(
        value: Comment(
            id: MockObjects.mockString,
            url: MockObjects.mockURL,
            parentCommentID: MockObjects.mockString,
            threadCommentID: MockObjects.mockString,
            text: MockObjects.mockString,
            commentDepth: 1,
            customData: nil,
            authorID: MockObjects.mockString,
            author: profileResource,
            authorImageURL: MockObjects.mockURL,
            createdAt: Date(),
            repliesURL: MockObjects.mockURL,
            commentRepliesCount: 0,
            isDeleted: false,
            isReported: false,
            commentReportsCount: 0,
            dismissReportedCommentURL: MockObjects.mockURL,
            filteredText: nil,
            contentFilter: Set<CommentFilter>()
        )
    )

    
    var getCommentBoardsPromise: Promise<PaginatedResult<CommentBoard>> = Promise(value: PaginatedResult(previous: nil, count: 0, next: nil, items: []))
    
    var getCommentsPromise: Promise<PaginatedResult<Comment>> = Promise(value: PaginatedResult(previous: nil, count: 0, next: nil, items: []))
    
    var createCommentBoardBanPromise: Promise<CommentBoardBanDetails> = Promise(
        value: CommentBoardBanDetails(
            id: MockObjects.mockString,
            url: MockObjects.mockURL,
            profileID: MockObjects.mockString,
            commentBoardID: MockObjects.mockString,
            clientID: MockObjects.mockString,
            bannedByID: MockObjects.mockString,
            createdAt: Date(),
            description: nil
        )
    )
    
    var getCommentBoardBansPromise: Promise<PaginatedResult<CommentBoardBanDetails>> = Promise(
        value: PaginatedResult(
            previous: nil,
            count: 0,
            next: nil,
            items: []
        )
    )

    func getUnclaimedWidgetInteractions(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<WidgetInteraction>> {
        return getUnclaimedWidgetInteractionsCompletion(url, accessToken)
    }
    
    func getChatRoomResource(roomID: String, accessToken: AccessToken) -> Promise<ChatRoomResource> {
        return getChatRoomResourcePromise
    }
    
    func checkTokenGatedChatAccess(roomID: String, walletAddress: String, accessToken: AccessToken) -> Promise<TokenGatedChat> {
        return checkTokenGatedChatAccessPromise
    }
    
    func createChatRoomResource(title: String?, visibility: ChatRoomVisibilty, accessToken: AccessToken, appConfig: ApplicationConfiguration, tokenGates: [TokenGate]?) -> Promise<ChatRoomResource> {
        return createChatRoomResourcePromise
    }
    
    func getUserChatRoomMemberships(url: URL, accessToken: AccessToken, page: ChatRoomMembershipPagination) -> Promise<UserChatRoomMembershipsResult> {
        return getChatRoomsInfoPromise
    }
    
    func createChatRoomMembership(roomID: String, accessToken: AccessToken) -> Promise<ChatRoomMember> {
        return getChatRoomMemberPromise
    }
    
    func addNewMemberToChatRoom(membershipUrl: URL, profileID: String, accessToken: AccessToken) -> Promise<ChatRoomMember> {
        return getChatRoomMemberPromise
    }
    
    func sendChatRoomInviteToUser(chatRoomInviteUrl: URL, chatRoomID: String, profileID: String, accessToken: AccessToken) -> Promise<ChatRoomInvitation> {
        return sendChatRoomInvitePromise
    }
    
    func updateChatRoomInvitationStatus(chatRoomInvitationUrl: URL, invitationStatus: ChatRoomInvitationStatus, accessToken: AccessToken) -> Promise<ChatRoomInvitation> {
        return sendChatRoomInvitePromise
    }
    
    func getInvitationsForUserWithInvitationStatus(invitationStatus: ChatRoomInvitationStatus, chatRoomInvitationsUrlTemplate: String, profileID: String, accessToken: AccessToken) -> Promise<PaginatedResult<ChatRoomInvitation>> {
        return getInvitationsForProfilePromise
    }
    
    func getInvitationsByUserWithInvitationStatus(invitationStatus: ChatRoomInvitationStatus, chatRoomInvitationsUrlTemplate: String, profileID: String, accessToken: AccessToken) -> Promise<PaginatedResult<ChatRoomInvitation>> {
        return getInvitationsForProfilePromise
    }
    
    func blockProfile(blockProfileURL: URL, profileID: String, accessToken: AccessToken) -> Promise<BlockInfo> {
        return blockInfoPromise
    }
    
    func unblockProfile(unblockProfileURL: URL, accessToken: AccessToken) -> Promise<Bool> {
        return unblockInfoPromise
    }
    
    func getBlockedProfileList(blockProfileURL: URL, blockedProfileID: String?, accessToken: AccessToken) -> Promise<PaginatedResult<BlockInfo>> {
        return getBlockedProfilesListPromise
    }
    
    func getBlockProfileInfo(blockProfileInfoURL: URL, accessToken: AccessToken) -> Promise<PaginatedResult<BlockInfo>> {
        return getBlockedProfilesListPromise
    }
    
    func getBlockedProfileIDList(blockedIDListURL: URL, accessToken: AccessToken) -> Promise<PaginatedResult<String>> {
        return getBlockedProfileIDsListPromise
    }
    
    func deleteChatRoomMembership(roomID: String, accessToken: AccessToken) -> Promise<Bool> {
        return deleteChatRoomMembershipPromise
    }
    
    func getWidget(id: String, kind: WidgetKind) -> Promise<WidgetResource> {
        return getWidgetCompletion(id, kind)
    }
    
    func getProgramDetail(programID: String) -> Promise<ProgramDetailResource> {
        return getProgramDetailPromise
    }
    
    func getLeaderboards(programID: String) -> Promise<[Leaderboard]> {
        return getLeaderboardsPromise
    }
    
    func getLeaderboard(leaderboardID: String) -> Promise<Leaderboard> {
        return getLeaderboardPromise
    }
    
    func getLeaderboardEntries(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<LeaderboardEntryResource>> {
        return getLeaderboardEntriesPromise
    }
    
    func getLeaderboardProfile(url: URL, accessToken: AccessToken) -> Promise<LeaderboardEntryResource> {
        return getLeaderboardProfilePromise
    }
    
    func getProfileLeaderboards(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<ProfileLeaderboard>> {
        return getProfileLeaderboardsPromise
    }
    
    func getProfileLeaderboardViews(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<ProfileLeaderboardView>> {
        return getProfileLeaderboardViewsPromise
    }
    
    func getRewardItems(url: URL, accessToken: AccessToken) -> Promise<[RewardItem]> {
        return Promise(value: [])
    }
    
    func claimRewards(claimURL: URL, claimToken: String, accessToken: AccessToken) -> Promise<ClaimRewardResource> {
        return Promise(value: .init(rewards: []))
    }
    
    func getLeaderboardEntry(url: URL, accessToken: AccessToken) -> Promise<LeaderboardEntryResource> {
        return Promise(value: MockGamification.mockLeaderboardEntryResource(id: nil))
    }
    
    func createProfile(profileURL: URL) -> Promise<AccessToken> {
        return createProfileCompletion()
    }
    
    func setNickname(profileURL: URL, nickname: String, accessToken: AccessToken) -> Promise<ProfileResource> {
        return setNicknameCompletion()
    }
    
    func getProfile(profileURL: URL, accessToken: AccessToken) -> Promise<ProfileResource> {
        return getProfileCompletion()
    }
    
    func createCheerMeterVote(voteCount: Int, voteURL: URL, accessToken: AccessToken) -> Promise<CheerMeterWidgetModel.Vote> {
        return createCheerMeterVoteCompletion(voteCount, voteURL, accessToken)
    }
    
    func updateCheerMeterVote(voteCount: Int, voteURL: URL, accessToken: AccessToken) -> Promise<CheerMeterWidgetModel.Vote> {
        return updateCheerMeterVoteCompletion(voteCount, voteURL, accessToken)
    }
    func getTimeline(timelineURL: URL, accessToken: AccessToken) -> Promise<WidgetTimelineResource> {
        return getTimelineCompletion(timelineURL, accessToken)
    }
    
    func getTimelineInteractions(timelineInteractionURL: URL, accessToken: AccessToken) -> Promise<WidgetTimelineInteractionsResource> {
        return getTimelineInteractionsCompletion(timelineInteractionURL, accessToken)
    }
    
    func getWidgetInteractions(url: URL, accessToken: AccessToken) -> Promise<WidgetTimelineInteractionsResource> {
        return getTimelineInteractionsCompletion(url, accessToken)
    }
    
    func createImpression(impressionURL: URL, userSessionID: String, accessToken: AccessToken) -> Promise<ImpressionResponse> {
        return createImpressionCompletion(impressionURL, userSessionID, accessToken)
    }
    
    func createQuizAnswer(answerURL: URL, accessToken: AccessToken) -> Promise<QuizWidgetModel.Answer> {
        return createQuizAnswerCompletion(answerURL, accessToken)
    }
    
    func createPredictionVote(voteURL: URL, accessToken: AccessToken, completion: @escaping (Result<PredictionVoteResource, Error>) -> Void) -> URLSessionDataTask {
        return createPredictionVoteCompletion(voteURL, accessToken, completion)
    }
    
    func updatePredictionVote(optionID: String, optionURL: URL, accessToken: AccessToken, completion: @escaping (Result<PredictionVoteResource, Error>) -> Void) -> URLSessionDataTask {
        return updatePredictionVoteCompletion(optionID, optionURL, accessToken, completion)
    }
    
    func createVoteOnPoll(for optionURL: URL, accessToken: AccessToken) -> Promise<PollWidgetModel.Vote> {
        return getVoteCompletion()
    }
    
    func updateVoteOnPoll(for optionID: String, optionURL: URL, accessToken: AccessToken) -> Promise<PollWidgetModel.Vote> {
        return getVoteCompletion()
    }
    
    func createImageSliderVote(voteURL: URL, magnitude: Double, accessToken: AccessToken) -> Promise<ImageSliderWidgetModel.Vote> {
        return createImageSliderVoteCompletion(voteURL, magnitude, accessToken)
    }
    
    func getChatUserMutedStatus(profileID: String, roomID: String, accessToken: AccessToken) -> Promise<ChatUserMuteStatusResource> {
        return getChatUserMutedStatusPromise
    }
    
    func setCustomData(profileURL: URL, data: String, accessToken: AccessToken) -> Promise<ProfileResource> {
        return setUserCustomDataPromise
    }
    
    func createTextAskReply(url: URL, reply: String, accessToken: AccessToken) -> Promise<TextAskWidgetModel.Reply> {
        return createTextAskReplyCompletion(url, reply, accessToken)
    }
    
    func sendCustomMessage(chatRoomID: String, customMessagesUrl: URL, accessToken: AccessToken, customData: String) -> Promise<ChatMessage> {
        return sendCustomMessageCompletionPromise
    }
    
    func sendMessage<T>(chatroomMessagesURL: URL, accessToken: AccessToken, messagePayload: T) -> Promise<ChatMessage> where T: Encodable {
        return sendMessageCompletionPromise
    }
    
    func deleteMessage(deleteMessagesURL: URL, messageID: String, timetoken: UInt64?, accessToken: AccessToken) -> Promise<Bool> {
        return deleteMessagePromise
    }
    
    func getWidgetResources(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<WidgetResource>> {
        return getWidgetResourcesCompletion(url, accessToken)
    }
    
    func getMessages(chatroomMessagesURL: URL, accessToken: AccessToken) -> Promise<GetMessagesResult> {
        return getMessageCompletionPromise
    }
    
    func getMessagesCount(chatroomMessagesCountURL: URL, accessToken: AccessToken) -> Promise<GetMessagesCountResult> {
        return getMessageCountCompletionPromise
    }
    
    func getMessageEvents(chatroomEventsURL: URL) -> Promise<PaginatedResult<ChatRoomEvent>> {
        return getMessageEventsCompletionPromise
    }
    
    func pinMessage(message: ChatMessagePayload, chatRoomID: String, pinnedMessagesURL: URL, accessToken: AccessToken) -> Promise<PinMessageInfo> {
        return pinMessagePromise
    }
    
    func unpinMessage(pinnedMessagesURL: URL, accessToken: AccessToken) -> Promise<Bool> {
        return unpinMessagePromise
    }
    
    func getPinMessageInfoList(pinMessageInfoListURL: URL, accessToken: AccessToken) -> Promise<PaginatedResult<PinMessageInfo>> {
        return getPinMessageInfoListPromise
    }
    
    func getRedemptionKeys(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<RedemptionKey>> {
        return getRedemptionKeysPromise
    }
    
    func redeemKey(url: URL, accessToken: AccessToken) -> Promise<RedemptionKey> {
        return getRedeemKeyPromise
    }
    
    func getProgramID(programURL: URL, accessToken: AccessToken) -> Promise<ProgramDetailResource> {
        return getProgramDetailPromise
    }
    
}

extension MockLiveLikeRestAPIServices: LiveLikeSponsorshipAPIProtocol {
    func getByApplication(applicationSponsorsURL: URL, accessToken: AccessToken, completion: @escaping (Result<PaginatedResult<Sponsor>, Error>) -> Void) {
        completion(getSponsorsListCompletion)
    }
    
    func getBy(chatRoomSponsorsURL: URL, accessToken: AccessToken, completion: @escaping (Result<PaginatedResult<Sponsor>, Error>) -> Void) {
        completion(getSponsorsListCompletion)
    }
    
    func getBy(programID: String, completion: @escaping (Result<[Sponsor], Error>) -> Void) {
        completion(getSponsorshipCompletion)
    }
}

extension MockLiveLikeRestAPIServices: LiveLikeBadgesAPIProtocol {
    func getProfileBadgeProgress(url: URL) -> Promise<[BadgeProgress]> {
        return getProfileBadgesProgressPromise
    }
    
    func getProfileBadges(url: URL) -> Promise<PaginatedResult<ProfileBadge>> {
        return getProfileBadgesPromise
    }
    
    func getApplicationBadges(url: URL) -> Promise<PaginatedResult<Badge>> {
        return getApplicationBadgesPromise
    }
    
    func getBadgeProfiles(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<BadgeProfile>> {
        return getBadgeProfilesPromise
    }
}

extension MockLiveLikeRestAPIServices: LiveLikeRewardsAPIProtocol {
    
    func getApplicationRewardItems(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<RewardItem>> {
        return getAllRewardsPromise
    }
    
    func transferRewardItemAmount(
        url: URL,
        accessToken: AccessToken,
        amount: Int,
        recipientProfileID: String,
        rewardItemID: String
    ) -> Promise<RewardItemTransfer> {
        return transferRewardItemAmountPromise
    }
    
    func getRewardItemTransfers(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<RewardItemTransfer>> {
        return getRewardItemTransfersPromise
    }
    
    func getRewardItemBalances(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<RewardItemBalance>> {
        return getRewardItemBalancesPromise
    }
    
    func getRewardTransactions(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<RewardTransaction>> {
        return getRewardTransactionsPromise
    }
}

extension MockLiveLikeRestAPIServices: LiveLikeQuestsAPIProtocol {
    
    func getQuests(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<Quest>> {
        return getQuestsPromise
    }

    func getUserQuests(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<UserQuest>> {
        return getUserQuestsPromise
    }

    func startUserQuest(
        url: URL,
        postBody: StartUserQuestBody,
        accessToken: AccessToken
    ) -> Promise<UserQuest> {
        return startUserQuestPromise
    }

    func updateUserQuestTasks(
        url: URL,
        patchBody: CompleteUserQuestTaskBody,
        accessToken: AccessToken
    ) -> Promise<UserQuest> {
        return updateUserQuestTasksPromise
    }

    func updateUserQuestTaskProgress(
        url: URL,
        postBody: UpdateUserQuestTaskProgressBody,
        accessToken: AccessToken
    ) -> Promise<UserQuestTaskProgress> {
        return updateUserQuestTaskProgressPromise
    }
    
    func getQuestRewards(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<QuestReward>> {
        return getQuestRewardsPromise
    }
    
    func getUserQuestRewards(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<UserQuestReward>> {
        return getUserQuestRewardsPromise
    }
    
    func claimUserQuestRewards(url: URL, accessToken: AccessToken) -> Promise<UserQuest> {
        return claimUserQuestRewardsPromise
    }
}

extension MockLiveLikeRestAPIServices: LiveLikeCommentAPIProtocol {
    func createCommentBoard(commentBoardURL: URL, options: CreateCommentBoardRequestOptions, accessToken: AccessToken) -> Promise<CommentBoard> {
        return createCommentBoardPromise
    }
    
    func updateCommentBoard(commentBoardsURL: URL, options: UpdateCommentBoardRequestOptions, accessToken: AccessToken) -> Promise<CommentBoard> {
        return createCommentBoardPromise
    }
    
    func getCommentBoards(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<CommentBoard>> {
        return getCommentBoardsPromise
    }
    
    func deleteCommentBoard(commentBoardURL: URL, accessToken: AccessToken) -> Promise<Bool> {
        return Promise(value: true)
    }
    
    func getCommentBoardInfo(commentBoardURL: URL, accessToken: AccessToken) -> Promise<CommentBoard> {
        return createCommentBoardPromise
    }
    
    func addComment(commentsURL: URL, commentBoardID: String, text: String, parentCommentID: String?, accessToken: AccessToken) -> Promise<Comment> {
        return createCommentPromise
    }
    
    func deleteComment(commentURL: URL, accessToken: AccessToken) -> Promise<Bool> {
        return Promise(value: true)
    }
    
    func editComment(commentURL: URL, text: String, accessToken: AccessToken) -> Promise<Comment> {
        return createCommentPromise
    }
    
    func getComments(commentsURL: URL, accessToken: AccessToken) -> Promise<PaginatedResult<Comment>> {
        return getCommentsPromise
    }
    
    func createCommentBoardBan(commentBoardBanURL: URL, profileID: String, options: CommentBoardBanRequestOptions?, accessToken: AccessToken) -> Promise<CommentBoardBanDetails> {
        return createCommentBoardBanPromise
    }
    
    func deleteCommentBoardBan(commentBoardBanURL: URL, accessToken: AccessToken) -> Promise<Bool> {
        return Promise(value: true)
    }
    
    func getCommentBoardBans(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<CommentBoardBanDetails>> {
        return getCommentBoardBansPromise
    }
    
    func getCommentBoardBanDetail(url: URL, accessToken: AccessToken) -> Promise<CommentBoardBanDetails> {
        return createCommentBoardBanPromise
    }
}
