//
//  PlayerTimeSourceMock.swift
//  LikeLiveTests
//
//  Created by Cory Sullivan on 2019-03-07.
//

import AVFoundation
@testable import EngagementSDK
@testable import LiveLikeSwift
import UIKit

class PlayerTimeSourceMock {
    var playheadPosition: EpochTime
    var playerSync: Timer?

    init(playheadPosition: EpochTime) {
        self.playheadPosition = playheadPosition
    }

    func start() {
        playerSync = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { _ in
            self.playheadPosition += 1
        })
    }

    func now() -> TimeInterval {
        return playheadPosition
    }
}
