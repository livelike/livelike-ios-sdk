//
//  ChatMessageBuilder.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 5/7/20.
//

@testable import EngagementSDK
@testable import LiveLikeSwift
import Foundation

class ChatMessageBuilder {
    
    var id: String = UUID().uuidString
    var roomID: String = ""
    var message: String = ""
    var senderID: String = ""
    var nickname: String = ""
    
    var filteredMessage: String?
    var filteredReasons: Set<ChatFilter> = Set()
    
    var videoTimestamp: EpochTime?
    func videoTimestamp(_ videoTimestamp: EpochTime) -> ChatMessageBuilder {
        self.videoTimestamp = videoTimestamp
        return self
    }
    
    func id(_ id: String) -> ChatMessageBuilder {
        self.id = id
        return self
    }
    
    func build(withID id: String) -> ChatMessage {
        self.id = id
        return build()
    }
    
    func build() -> ChatMessage {
        return ChatMessage(
            id: id,
            clientMessageID: id,
            roomID: roomID,
            message: message,
            senderID: senderID,
            senderNickname: nickname,
            videoTimestamp: self.videoTimestamp,
            reactions: ReactionVotes(allVotes: []),
            timestamp: Date(),
            profileImageUrl: nil,
            createdAt: TimeToken(date: Date()),
            bodyImageUrl: nil,
            bodyImageHeight: nil,
            bodyImageWidth: nil,
            filteredMessage: filteredMessage,
            filteredReasons: filteredReasons,
            customData: nil,
            quoteMessage: nil,
            messageEvent: .messageCreated,
            pubnubTimetoken: TimeToken(date: Date()),
            messageMetadata: nil,
            parentMessageID: nil,
            repliesCount: nil,
            repliesURL: nil
        )
    }
    
}
