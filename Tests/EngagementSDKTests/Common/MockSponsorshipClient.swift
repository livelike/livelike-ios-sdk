//
//  MockSponsorshipClient.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 6/10/21.
//

@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift
import Foundation

class MockSponsorshipClient: SponsorshipClient {
    var sponsorshipAPI: MockLiveLikeRestAPIServices
    var chatAPI: MockLiveLikeRestAPIServices
    var coreAPI: MockLiveLikeRestAPIServices

    init(coreAPI: MockLiveLikeRestAPIServices) {
        self.sponsorshipAPI = coreAPI
        self.coreAPI = coreAPI
        self.chatAPI = coreAPI
    }

    func getBy(programID: String, completion: @escaping (Result<[Sponsor], Error>) -> Void) {
        self.sponsorshipAPI.getBy(programID: programID, completion: completion)
    }

    func getByApplication(page: Pagination, completion: @escaping (Result<PaginatedResult<Sponsor>, Error>) -> Void) {
        firstly {
            self.coreAPI.whenApplicationConfig
        }.then { applicationConfig in
            self.sponsorshipAPI.getByApplication(
                applicationSponsorsURL: applicationConfig.sponsorsUrl,
                accessToken: AccessToken(fromString: ""),
                completion: completion
            )
        }
    }

    func getBy(chatRoomID: String, page: Pagination, completion: @escaping (Result<PaginatedResult<Sponsor>, Error>) -> Void) {
        firstly {
            self.chatAPI.getChatRoomResource(
                roomID: chatRoomID,
                accessToken: AccessToken(fromString: "")
            )
        }.then { chatRoomResource in
            self.sponsorshipAPI.getBy(
                chatRoomSponsorsURL: chatRoomResource.sponsorsUrl,
                accessToken: AccessToken(fromString: ""),
                completion: completion
            )
        }
    }
}
