//
//  MockGamification.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 7/30/20.
//

@testable import EngagementSDK
@testable import LiveLikeSwift
@testable import LiveLikeCore
import Foundation

class MockGamification {
    static func mockLeaderboardResource(id: String?) -> Leaderboard {
        return Leaderboard(
            id: id ?? "123",
            url: MockObjects.mockURL,
            clientID: "",
            name: "",
            rewardItem: MockObjects.mockRewardItem,
            isLocked: false,
            entriesURL: MockObjects.mockURL,
            entryDetailUrlTemplate: ""
        )
    }

    static func mockLeaderboardPaginatedEntryResource() -> PaginatedResult<LeaderboardEntryResource> {
        return PaginatedResult<LeaderboardEntryResource>(previous: MockObjects.mockURL, count: 1, next: nil, items: [])
    }

    static func mockProfileLeaderboardPaginatedResource() -> PaginatedResult<ProfileLeaderboard> {
        return PaginatedResult<ProfileLeaderboard>(previous: MockObjects.mockURL, count: 1, next: nil, items: [])
    }

    static func mockProfileLeaderboardPaginatedViewResource() -> PaginatedResult<ProfileLeaderboardView> {
        return PaginatedResult<ProfileLeaderboardView>(previous: MockObjects.mockURL, count: 1, next: nil, items: [])
    }

    static func mockLeaderboardEntryResource(id: String?) -> LeaderboardEntryResource {
        return LeaderboardEntryResource(percentileRank: "", profileId: id ?? "123", rank: 1, score: 1.0, profileNickname: "", profile: MockUserProfile.mockUserProfileResource())
    }
}
