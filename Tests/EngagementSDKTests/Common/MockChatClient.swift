//
//  MockChatClient.swift
//  
//
//  Created by Jelzon Monzon on 12/19/22.
//

import Foundation
@testable import EngagementSDK
@testable import LiveLikeSwift
@testable import LiveLikeCore

class MockChatClient: ChatClient {
    func getChatMessages(page: LiveLikeSwift.Pagination, options: LiveLikeSwift.GetChatMessageRequestOptions, completion: @escaping (Result<[LiveLikeSwift.ChatMessage], any Error>) -> Void) {
    }
    
    
    var delegate: ChatClientDelegate?
    
    func addNewMemberToChatRoom(roomID: String, profileID: String, completion: @escaping (Result<ChatRoomMember, Error>) -> Void) {

    }
    
    func sendChatRoomInviteToUser(roomID: String, profileID: String, completion: @escaping (Result<ChatRoomInvitation, Error>) -> Void) {
    
    }
    
    func updateChatRoomInviteStatus(chatRoomInvitation: ChatRoomInvitation, invitationStatus: ChatRoomInvitationStatus, completion: @escaping (Result<ChatRoomInvitation, Error>) -> Void) {
    
    }
    
    func getInvitationsForUserWithInvitationStatus(invitationStatus: ChatRoomInvitationStatus, page: Pagination, completion: @escaping (Result<[ChatRoomInvitation], Error>) -> Void) {
        
    }
    
    func getInvitationsByUserWithInvitationStatus(invitationStatus: ChatRoomInvitationStatus, page: Pagination, completion: @escaping (Result<[ChatRoomInvitation], Error>) -> Void) {
        
    }
    
    func blockProfile(profileID: String, completion: @escaping (Result<BlockInfo, Error>) -> Void) {
        
    }
    
    func unblockProfile(blockRequestID: String, completion: @escaping (Result<Bool, Error>) -> Void) {
        
    }
    
    func getBlockedProfileList(blockedProfileID: String?, page: Pagination, completion: @escaping (Result<[BlockInfo], Error>) -> Void) {
        
    }
    
    func getProfileBlockInfo(profileID: String, completion: @escaping (Result<BlockInfo, Error>) -> Void) {
        
    }
    
    func getBlockedProfileIDList(profileID: String, page: Pagination, completion: @escaping (Result<PaginatedResult<String>, Error>) -> Void) {
        
    }
    
    func getBlockedProfileIDListComplete(completion: @escaping (Result<[String], Error>) -> Void) {
        
    }
    
    func getTokenGatedChatAccessDetails(roomID: String, walletAddress: String, completion: @escaping (Result<TokenGatedChatAccessInfo, Error>) -> Void) {
        
    }
}
