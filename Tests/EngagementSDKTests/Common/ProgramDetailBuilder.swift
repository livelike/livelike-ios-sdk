//
//  ProgramDetailBuilder.swift
//  EngagementSDKTests
//
//  Created by Jelzon Monzon on 4/10/20.
//

import Foundation
@testable import EngagementSDK
@testable import LiveLikeSwift

class ProgramDetailBuilder {
    
    private var id: String = "id"
    private var title: String = "title"
    private var subscribeChannel: String = "subscribeChannel"
    private var timelineURL: URL = MockObjects.mockURL
    private var unclaimedWidgetInteractionsUrlTemplate: String = "https://google.com/{profile_id}"
    
    func id(_ id: String) -> ProgramDetailBuilder {
        self.id = id
        return self
    }
    
    func title(_ title: String) -> ProgramDetailBuilder {
        self.title = title
        return self
    }
    
    func subscribeChannel(
        _ subscribeChannel: String
    ) -> ProgramDetailBuilder {
        self.subscribeChannel = subscribeChannel
        return self
    }

    func timelineURL(_ timelineURL: URL) -> ProgramDetailBuilder {
        self.timelineURL = timelineURL
        return self
    }
    
    func build() -> ProgramDetailResource {
        ProgramDetailResource(
            id: id,
            title: title,
            widgetsEnabled: true,
            chatEnabled: true,
            subscribeChannel: subscribeChannel,
            pubnubEnabled: true,
            syncSessionsUrl: URL(string: "https://livelike.com")!,
            rankUrl: URL(string: "https://livelike.com")!,
            reactionPacksUrl: URL(string: "https://livelike.com")!,
            defaultChatRoom: nil,
            timelineUrl: timelineURL,
            widgetsUrl: MockObjects.mockURL,
            leaderboards: [],
            rewardItems: [],
            sponsors: [],
            streamUrl: nil,
            unclaimedWidgetInteractionsUrlTemplate: self.unclaimedWidgetInteractionsUrlTemplate,
            widgetInteractionsUrlTemplate: self.unclaimedWidgetInteractionsUrlTemplate
        )
    }
    
}
