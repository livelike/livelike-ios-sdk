//
//  MockNetworking.swift
//  
//
//  Created by Jelzon Monzon on 1/27/23.
//

import Foundation
import LiveLikeCore

class MockNetworking: LLNetworking {
    func task(request: URLRequest, completion: @escaping (Result<Data, Error>) -> Void) { }
    
    var userAgent: String = ""
    
    func task<A>(
        _ resource: Resource<A>,
        completion: @escaping (Result<A, Error>) -> Void
    ) -> URLSessionDataTask {
        return URLSessionDataTask()
    }
}

/// A LLNetworking double that will always return the specified result
class StubNetworking<B: Decodable>: LLNetworking {
    func task(request: URLRequest, completion: @escaping (Result<Data, Error>) -> Void) { }
    
    var userAgent: String = ""

    var result: Result<B, Error>
    
    init(result: Result<B, Error>) {
        self.result = result
    }
    
    func task<A>(
        _ resource: Resource<A>,
        completion: @escaping (Result<A, Error>) -> Void
    ) -> URLSessionDataTask {
        
        completion(self.result.map { $0 as! A })
        return URLSessionDataTask()
    }
}
