//
//  MockUserClient.swift
//  
//
//  Created by Keval Shah on 13/04/23.
//

import UIKit

import Foundation
@testable import EngagementSDK
@testable import LiveLikeSwift
@testable import LiveLikeCore

class MockUserClient: UserClient {

    var delegate: UserClientDelegate?

    func blockProfile(profileID: String, completion: @escaping (Result<BlockInfo, Error>) -> Void) {

    }

    func unblockProfile(blockRequestID: String, completion: @escaping (Result<Bool, Error>) -> Void) {

    }

    func getBlockedProfileList(blockedProfileID: String?, page: Pagination, completion: @escaping (Result<[BlockInfo], Error>) -> Void) {

    }

    func getProfileBlockInfo(profileID: String, completion: @escaping (Result<BlockInfo, Error>) -> Void) {

    }

    func getBlockedProfileIDList(page: Pagination, completion: @escaping (Result<PaginatedResult<String>, Error>) -> Void) {

    }

    func getBlockedProfileIDListComplete(completion: @escaping (Result<[String], Error>) -> Void) {

    }

}
