//
//  Helpers.swift
//  LikeLiveTests
//
//  Created by Heinrich Dahms on 2019-02-14.
//

@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift
import UIKit

final class EngagementTestsResources {
    public static let resourceBundle: Bundle = {
        let candidates = [
            // Bundle should be present here when the package is linked into an App.
            Bundle.main.resourceURL,

            // Bundle should be present here when the package is linked into a framework.
            Bundle(for: EngagementTestsResources.self).resourceURL,
        ]

        let bundleName = "EngagementSDK_EngagementSDKTests"

        for candidate in candidates {
            let bundlePath = candidate?.appendingPathComponent(bundleName + ".bundle")
            if let bundle = bundlePath.flatMap(Bundle.init(url:)) {
                return bundle
            }
        }

        // Return whatever bundle this code is in as a last resort.
        return Bundle(for: EngagementTestsResources.self)
    }()
}

class Helper {
    static let shouldRecord = false

    static func jsonDecode<T: Decodable>(filename: String) throws -> T {
        let jsonUrl = EngagementTestsResources.resourceBundle.url(forResource: filename, withExtension: "json")!
        let jsonData = try Data(contentsOf: jsonUrl, options: .dataReadingMapped)
        let decoder = LLJSONDecoder()

        return try decoder.decode(T.self, from: jsonData)
    }

    static func getImage(name: String, ofType: String) -> UIImage? {
        guard let imagePath = EngagementTestsResources.resourceBundle.path(forResource: name, ofType: ofType) else {
            return nil
        }
        return UIImage(contentsOfFile: imagePath)
    }
}

class InMemoryAccessTokenStorage: AccessTokenStorage {
    var accessToken: String?
    func fetchAccessToken() -> String? {
        return accessToken
    }
    func storeAccessToken(accessToken: String) {
        self.accessToken = accessToken
    }
}

class MockObjects {
    static let mockURL = URL(string: "http://www.google.com")!
    static let timeout: Timeout = "0"
    static let sponsorResource = Sponsor(
        id: "08e0ace4-859d-4e8a-ba87-944111df95b4",
        clientID: "IOWGz4LAfkVNfdLUS6c0B1iFewR2BFrm6HgEOchI",
        name: "Pepsi",
        logoURL: MockObjects.mockURL,
        brandColorHex: "ffffff"
    )

    static var mockString: String {
        return String(Date().timeIntervalSince1970)
    }

    static let mockRewardItemTransfer = RewardItemTransfer(
        id: MockObjects.mockString,
        rewardItemID: MockObjects.mockString,
        rewardItemName: MockObjects.mockString,
        rewardItemAmount: 1,
        recipientProfileID: MockObjects.mockString,
        recipientProfileName: MockObjects.mockString,
        senderProfileID: MockObjects.mockString,
        senderProfileName: MockObjects.mockString,
        createdAt: Date()
    )

    static let mockRewardItem = RewardItem(
        id: MockObjects.mockString,
        name: MockObjects.mockString,
        images: [],
        attributes: [],
        url: MockObjects.mockURL,
        clientID: MockObjects.mockString
    )

    static let mockRedemptionCode = RedemptionKey(
        id: mockString,
        clientId: mockString,
        name: mockString,
        description: nil,
        code: mockString,
        createdAt: Date(),
        redeemedAt: nil,
        redeemedBy: nil,
        status: .active,
        assignedTo: nil
    )

    static let mockRegisteredLink = RegisteredLink(
        id: mockString,
        name: mockString,
        description: mockString,
        linkURL: mockURL,
        clientID: mockString
    )

    static let mockQuestTask = QuestTask(
        id: mockString,
        name: mockString,
        description: mockString,
        createdAt: Date(),
        questID: mockString,
        targetValue: 1.0,
        defaultProgressIncrement: 1.0
    )

    static let mockQuest = Quest(
        id: mockString,
        clientID: mockString,
        name: mockString,
        description: mockString,
        createdAt: Date(),
        questTasks: [mockQuestTask],
        questRewardsURL: MockObjects.mockURL
    )

    static let mockUserQuestTask = UserQuestTask(
        id: mockString,
        status: .incomplete,
        createdAt: Date(),
        completedAt: nil,
        userQuestID: mockString,
        questTask: mockQuestTask,
        progress: 0.0
    )

    static let mockUserQuest = UserQuest(
        id: mockString,
        profileID: mockString,
        userQuestTasks: [mockUserQuestTask],
        status: .incomplete,
        createdAt: Date(),
        completedAt: nil,
        quest: mockQuest,
        rewardsStatus: nil,
        rewardsClaimedAt: nil
    )

    static let mockProfileRelationship = ProfileRelationship(
        id: mockString,
        url: mockURL,
        createdAt: Date(),
        fromProfile: MockUserProfile.mockUserProfileResource(),
        toProfile: MockUserProfile.mockUserProfileResource(),
        relationshipType: mockProfileRelationshipType
    )

    static let mockProfileRelationshipType = ProfileRelationshipType(
        id: mockString,
        url: mockURL,
        createdAt: Date(),
        clientID: mockString,
        name: mockString,
        key: mockString
    )
}
