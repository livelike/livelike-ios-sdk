//
//  StickerPackBuilder.swift
//  EngagementSDKTests
//
//  Created by Mike Moloksher on 4/16/21.
//

import Foundation
@testable import EngagementSDK
@testable import LiveLikeSwift

class StickerPackBuilder {
    var id: String = UUID().uuidString
    var url: URL = MockObjects.mockURL
    var name: String = ""
    var file: URL = MockObjects.mockURL
    var stickers = [Sticker]()

    func build() -> StickerPack {
        return StickerPack(
            id: id,
            name: name,
            file: file,
            stickers: stickers,
            url: url
        )
    }
}
