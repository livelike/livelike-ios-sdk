//
//  MockReactionClient.swift
//  
//
//  Created by Keval Shah on 26/04/23.
//

import UIKit
import Foundation
@testable import EngagementSDK
@testable import LiveLikeSwift

class MockReactionClient: ReactionClient {
    var delegate: ReactionClientDelegate?
    
    func createReactionSpace(name: String?, targetGroupID: String, reactionPackIDs: [String], completion: @escaping (Result<ReactionSpace, Error>) -> Void) {
    }
    
    func updateReactionSpace(reactionSpaceID: String, reactionPackIDs: [String], completion: @escaping (Result<[String], Error>) -> Void) {
        
    }
    
    func deleteReactionSpace(reactionSpaceID: String, completion: @escaping (Result<Bool, Error>) -> Void) {
        
    }
    
    func getReactionSpaceInfo(reactionSpaceID: String, completion: @escaping (Result<ReactionSpace, Error>) -> Void) {
        
    }
    
    func getReactionSpaces(reactionSpaceID: String?, targetGroupID: String?, page: Pagination, completion: @escaping (Result<[ReactionSpace], Error>) -> Void) {
        
    }
    
    func getReactionPacks(page: Pagination, completion: @escaping (Result<[ReactionPack], Error>) -> Void) {
        
    }
    
    func getReactionPackInfo(reactionPackID: String, completion: @escaping (Result<ReactionPack, Error>) -> Void) {
        
    }
    
    func createReactionSession(reactionSpace: ReactionSpace) -> ReactionSession {
        return MockReactionSession()
    }
}


class MockReactionSession: ReactionSession {
    var name: String? = nil
    
    var reactionSpaceID: String = MockObjects.mockString
    
    var targetGroupID: String = MockObjects.mockString
    
    var reactionPackIDs: [String] = []
    
    var sessionDelegate: ReactionSessionDelegate?
    
    func addUserReaction(targetID: String, reactionID: String, customData: String?, completion: @escaping (Result<UserReaction, Error>) -> Void) {
        
    }
    
    func removeUserReaction(userReactionID: String, completion: @escaping (Result<Bool, Error>) -> Void) {
        
    }
    
    func getUserReactions(page: Pagination, reactionSpaceID: String, options: GetUserReactionsRequestOptions?, completion: @escaping (Result<[UserReaction], Error>) -> Void) {
        
    }
    
    func getUserReactionsCount(reactionSpaceID: String, targetID: [String], page: Pagination, completion: @escaping (Result<[UserReactionsCountResult], Error>) -> Void) {
        
    }
}
