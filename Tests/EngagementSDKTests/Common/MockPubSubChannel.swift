//
//  MockPubSubChannel.swift
//  EngagementSDKTests
//
//  Created by Jelzon Monzon on 5/13/20.
//

@testable import EngagementSDK
@testable import LiveLikeSwift
import Foundation

class MockPubSubService: PubSubService {
    func subscribe(_ channel: String) -> PubSubChannel {
        return MockPubSubChannel()
    }
}

class MockPubSubChannel: PubSubChannel {

    var fetchMessagesCompletion: ((TimeToken, UInt, (Result<PubSubHistoryResult, Error>) -> Void) -> Void)?

    func fetchMessages(since timestamp: TimeToken, limit: UInt, completion: @escaping (Result<PubSubHistoryResult, Error>) -> Void) {
        fetchMessagesCompletion?(timestamp, limit, completion)
    }

    weak var delegate: PubSubChannelDelegate?
    
    var name: String = ""
    
    var fetchHistoryCompletion: (TimeToken?, TimeToken?, UInt, (Result<PubSubHistoryResult, Error>) -> Void) -> Void = { _, _, _, _ in }
    
    var sendCompletionResult: Result<String, Error>?
    
    func send(_ message: [String: AnyObject], completion: @escaping (Result<String, Error>) -> Void) {
        guard let sendCompletionResult = sendCompletionResult else { return }
        completion(sendCompletionResult)
    }
    
    func sendMessageAction(type: String, value: String, messageID: String, completion: @escaping (Result<Bool, Error>) -> Void) { }
    
    func removeMessageAction(messageID: String, messageActionID: String, completion: @escaping (Result<Bool, Error>) -> Void) { }
    
    func fetchHistory(
        oldestMessageDate: TimeToken?,
        newestMessageDate: TimeToken?,
        limit: UInt,
        completion: @escaping (Result<PubSubHistoryResult, Error>) -> Void
    ) {
        fetchHistoryCompletion(oldestMessageDate, newestMessageDate, limit, completion)
    }
    
    func messageCount(since timestamp: TimeToken, completion: @escaping (Result<Int, Error>) -> Void) { }
    
    var pauseStatus: PauseStatus = .unpaused
    
    func pause() { }
    
    func resume() { }
    
    func disconnect() { }
    
}
