//
//  ContainerViewController.swift
//  LikeLiveTests
//
//  Created by Heinrich Dahms on 2019-02-14.
//

@testable import EngagementSDK
import UIKit

enum ContainerSize {
    case lessThanRecommended
    case recommendedWidgetSize
    case recommendedChatSize
    case greaterThanRecommended

    var size: CGRect {
        switch self {
        case .lessThanRecommended:
            return CGRect(x: 0, y: 0, width: 100, height: 100)
        case .recommendedWidgetSize:
            return CGRect(x: 0, y: 0, width: 260, height: 250)
        case .recommendedChatSize:
            return CGRect(x: 0, y: 0, width: 292, height: 250)
        case .greaterThanRecommended:
            return CGRect(x: 0, y: 0, width: 300, height: 250)
        }
    }
}

class ContainerViewController: UIViewController {
    lazy var containerView: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = UIColor(red: 35.0 / 255.0, green: 40.0 / 255.0, blue: 45.0 / 255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private let containerSize: ContainerSize

    init(containerSize: ContainerSize = .recommendedWidgetSize) {
        self.containerSize = containerSize
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        containerSize = .recommendedWidgetSize
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func loadView() {
        super.loadView()

        view.addSubview(containerView)

        let constraints = [
            containerView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            containerView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            containerView.heightAnchor.constraint(equalToConstant: containerSize.size.height),
            containerView.widthAnchor.constraint(equalToConstant: containerSize.size.width)
        ]
        NSLayoutConstraint.activate(constraints)
    }

    func addWidget(viewController: UIViewController) {
        addChild(viewController: viewController, into: containerView)
    }
}
