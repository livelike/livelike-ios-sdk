//
//  MockAnalytics.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 7/18/19.
//

@testable import EngagementSDK
@testable import LiveLikeSwift
import Foundation

struct MockAnalytics: EventRecorder {
    func record(_ event: AnalyticsEvent) {}
    func record(fromStringDict stringDict: [String: String]) {}
    func register(fromStringDict stringDict: [String: String]) {}
}
