//
//  EngagementSDKBuilder.swift
//  EngagementSDKTests
//
//  Created by Jelzon Monzon on 5/4/20.
//

@testable import EngagementSDK
@testable import LiveLikeCore
@testable import LiveLikeSwift
import Foundation

class EngagementSDKBuilder {

    var config: EngagementSDKConfig = EngagementSDKConfig(clientID: "")
    var mockLLRestAPIServices: MockLiveLikeRestAPIServices = MockLiveLikeRestAPIServices()
    var sdkErrorReporter = InternalErrorReporter()
    var tokenStorage: AccessTokenStorage = {
        let storage = InMemoryAccessTokenStorage()
        storage.accessToken = ""
        return storage
    }()

    var mockSponsorshipClient = MockSponsorshipClient(coreAPI: MockLiveLikeRestAPIServices())


    var accessTokenVendor: AccessTokenVendor = MockAccessTokenVendor()
    var livelikeIDVendor: LiveLikeIDVendor = MockLiveLikeIDVendor()
    var userNicknameService: UserNicknameService = MockNicknameService()
    var userCustomDataService: UserCustomDataService = MockUserCustomDataService()
    var userProfileVendor: UserProfileVendor = MockProfileVendor()
    var pubsubServiceFactory: PubSubServiceFactory = MockPubSubServiceFactory()
    var networking: LLNetworking = MockNetworking()

    var mockBadgeClient: BadgesClient?
    var mockRewardsClient: RewardsClient?
    var mockChatClient: ChatClient?
    var mockReactionClient: ReactionClient?
    var mockQuestsClient: QuestsClient?
    var mockSocialGraphClient: SocialGraphClient?

    func build() -> LiveLike {
        let sdk = LiveLike(
            config: config,
            coreAPI: mockLLRestAPIServices,
            widgetAPI: mockLLRestAPIServices,
            chatAPI: mockLLRestAPIServices,
            reactionAPI: mockLLRestAPIServices,
            commentAPI: mockLLRestAPIServices,
            leaderboardAPI: mockLLRestAPIServices,
            sponsorshipClient: mockSponsorshipClient,
            badgesClient: mockBadgeClient ?? InternalBadgesClient(coreAPI: mockLLRestAPIServices, accessTokenVendor: accessTokenVendor),
            accessTokenVendor: accessTokenVendor,
            livelikeIDVendor: livelikeIDVendor,
            userNicknameService: userNicknameService,
            customDataService: userCustomDataService,
            userProfileVendor: userProfileVendor,
            sdkErrorReporter: sdkErrorReporter,
            pubsubServiceFactory: pubsubServiceFactory,
            networking: networking
        )

        if let mockRewardsClient = mockRewardsClient {
            sdk.rewards = mockRewardsClient
        }

        if let mockQuestsClient = mockQuestsClient {
            sdk.quests = mockQuestsClient
        }

        if let mockSocialGraphClient = mockSocialGraphClient {
            sdk.socialGraphClient = mockSocialGraphClient
        }

        return sdk
    }
}

class MockPubSubServiceFactory: PubSubServiceFactory {
    func make() -> Promise<PubSubService> {
        return Promise(value: MockPubSubService())
    }
}

class MockAccessTokenVendor: AccessTokenVendor {
    var whenAccessToken: Promise<AccessToken> = Promise(value: AccessToken(fromString: "token"))
}

class MockLiveLikeIDVendor: LiveLikeIDVendor {
    var whenLiveLikeID: Promise<LiveLikeID> = Promise(value: LiveLikeID(from: "id"))
}

class MockNicknameService: UserNicknameService {
    func setNickname(nickname: String) -> Promise<String> {
        return Promise(value: nickname)
    }

    var whenInitialNickname: Promise<Void> = Promise(value: ())

    var currentNickname: String? = "initial name"

    var nicknameDidChange: [(String) -> Void] = []
}

class MockUserCustomDataService: UserCustomDataService {
    func setCustomData(customData: String) -> LiveLikeCore.Promise<String> {
        return Promise(value: customData)
    }

    var whenInitialCustomData: LiveLikeCore.Promise<Void> = Promise(value: ())

    var currentCustomData: String? = "Initial custom data"

    var customDataDidChange: [(String) -> Void] = []

}

class MockProfileVendor: UserProfileVendor {
    var whenProfileResource: Promise<ProfileResource> = Promise(value: MockLiveLikeRestAPIServices.profileResource)
}
