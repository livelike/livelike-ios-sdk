//
//  TextAskWidgetE2E.swift
//  
//
//  Created by Jelzon Monzon on 5/17/23.
//

import Foundation
import XCTest
import LiveLikeWidgetPublish
import LiveLikeSwift

@available(iOS 13.0.0, *)
class TextAskWidgetE2E: BaseE2E {
    
    override var accessToken: String? {
        get { return config.producerToken }
        set { super.accessToken = newValue }
    }
    
    /// Validates widget data
    func testData() async throws {
        let textAskModel = try await withCheckedThrowingContinuation { c in
            livelike.widgetClient.createTextAskWidget(
                options: CreateTextAskWidgetOptions(
                    common: CommonCreateWidgetOptions(
                        programID: config.programID
                    ),
                    title: "test text ask",
                    prompt: "test prompt",
                    confirmationMessage: "test confirmation"
                )
            ) { result in
                c.resume(with: result)
            }
        }
        
        XCTAssertEqual(textAskModel.title, "test text ask")
        XCTAssertEqual(textAskModel.prompt, "test prompt")
        XCTAssertEqual(textAskModel.programID, config.programID)
        XCTAssertEqual(textAskModel.confirmationMessage, "test confirmation")
        
        try await deleteWidget(model: textAskModel)
    }
    
    /// Tests submitting replies
    func testReplies() async throws {
        let textAsk = try await withCheckedThrowingContinuation { c in
            WidgetPublishAPI.createTextAsk(
                baseURL: livelike.currentConfiguration.apiOrigin,
                accessToken: config.producerToken,
                payload: CreateTextAsk(
                    program_id: config.programID,
                    title: "test text ask",
                    prompt: "test prompt",
                    confirmation_message: "test confirmation"
                )
            ) { result in
                c.resume(with: result)
            }
        }
        
        let textAskModel: TextAskWidgetModel = try await withCheckedThrowingContinuation { c in
            livelike.getWidgetModel(
                TextAskWidgetModel.self,
                id: textAsk.id,
                kind: WidgetKind(stringValue: textAsk.kind)!
            ) {
                c.resume(with: $0)
            }
        }
        
        let reply: TextAskWidgetModel.Reply = try await withCheckedThrowingContinuation { c in
            textAskModel.submitReply("my reply") {
                c.resume(with: $0)
            }
        }
        
        XCTAssertEqual(reply.text, "my reply")
        
        let anotherReply = try await withCheckedThrowingContinuation { c in
            textAskModel.submitReply("another reply") {
                c.resume(with: $0)
            }
        }
        
        XCTAssertEqual(anotherReply.text, "another reply")
        
        XCTAssertEqual(textAskModel.userReplies.count, 2)
        XCTAssertEqual(textAskModel.mostRecentReply?.id, anotherReply.id)
        
        try await deleteWidget(model: textAskModel)
    }
    
}
