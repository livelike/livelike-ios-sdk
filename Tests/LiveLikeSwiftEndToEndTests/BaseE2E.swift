//
//  BaseE2E.swift
//  
//
//  Created by Jelzon Monzon on 5/16/23.
//

import Foundation
import XCTest
import LiveLikeSwift

@available(iOS 13.0.0, *)
class BaseE2E: XCTestCase {

    private(set) var livelike: LiveLike!
    private(set) var config: Config!

    /// default to nil for new user every test run
    /// override if necessary to test specific user (ie. specific permissions)
    var accessToken: String? = nil

    override func setUp() async throws {
        self.config = try Config.load()
        var llConfig = LiveLikeConfig(clientID: self.config.clientID)
        llConfig.apiOrigin = config.apiOrigin
        llConfig.accessTokenStorage = self
        self.livelike = LiveLike(config: llConfig)
    }

    func deleteWidget(model: BaseWidgetModel) async throws {
        return try await withCheckedThrowingContinuation { c in
            livelike.widgetClient.deleteWidget(
                options: DeleteWidgetRequestOptions(
                    id: model.id,
                    kind: model.kind
                )
            ) { c.resume(with: $0.map { _ in }) }
        }
    }
}

@available(iOS 13.0.0, *)
extension BaseE2E: AccessTokenStorage {
    func fetchAccessToken() -> String? {
        return accessToken
    }

    func storeAccessToken(accessToken: String) { }
}
