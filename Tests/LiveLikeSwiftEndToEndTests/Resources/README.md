Setup guide for LiveLikeSwiftEndToEndTests
# Run Tests
Before running the tests in Xcode make sure you have a valid config.json file located at LiveLikeSwiftEndToEndTests/Resources.

Sample:
```
{
    "clientID" : "client-id",
    "programID" : "program-id",
    "producerToken" : "producer-token"
}
```
