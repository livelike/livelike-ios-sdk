//
//  AssertResultSuccess.swift
//  
//
//  Created by Jelzon Monzon on 6/15/23.
//

import XCTest

func AssertResultSuccess<T, E>(
    _ result: Result<T, E>,
    message: (E) -> String = { "Expected to be a success but got a failure with \($0) "},
    file: StaticString = #filePath,
    line: UInt = #line
) where E: Error {
    switch result {
    case .failure(let error):
        XCTFail(message(error), file: file, line: line)
    case .success:
        XCTAssertTrue(true)
    }
}
