//
//  Config.swift
//  
//
//  Created by Jelzon Monzon on 4/26/23.
//

import Foundation

struct Config: Decodable {
    let clientID: String
    let programID: String
    let producerToken: String
    let apiOrigin: URL

    static func load() throws -> Config {
        guard
            let filePath = Bundle.module.path(forResource: "config", ofType: "json")
        else {
            assertionFailure("Failed to load the test configuration. Add the config.json file to Tests/LiveLikeSwiftEndToEndTests/Resources.")
            throw BasicError()
        }

        do {
            let fileUrl = URL(fileURLWithPath: filePath)
            let data = try Data(contentsOf: fileUrl)
            return try JSONDecoder().decode(Config.self, from: data)
        } catch {
            assertionFailure(error.localizedDescription)
        }
        throw BasicError()
    }
}


