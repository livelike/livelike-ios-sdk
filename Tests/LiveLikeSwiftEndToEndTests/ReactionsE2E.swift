//
//  ReactionsE2E.swift
//  
//
//  Created by Jelzon Monzon on 6/7/23.
//

import Foundation
import XCTest
import LiveLikeSwift

@available(iOS 13.0.0, *)
class ReactionsE2E: BaseE2E {
    
    func test() async throws {
        let name = "name"
        let targetGroupID = UUID().uuidString
        let reactionPackIDs = [String]()
        
        let reactionSpace = try await withCheckedThrowingContinuation { c in
            livelike.reaction.createReactionSpace(
                name: name,
                targetGroupID: targetGroupID,
                reactionPackIDs: reactionPackIDs
            ) {
                c.resume(with: $0)
            }
        }
        
        XCTAssertEqual(reactionSpace.name, name)
        XCTAssertEqual(reactionSpace.targetGroupID, targetGroupID)
        XCTAssertEqual(reactionSpace.reactionPackIDs, reactionPackIDs)
        
        let getReactionSpace = try await withCheckedThrowingContinuation { c in
            livelike.reaction.getReactionSpaceInfo(reactionSpaceID: reactionSpace.id) {
                c.resume(with: $0)
            }
        }

        XCTAssertEqual(getReactionSpace.name, name)
        XCTAssertEqual(getReactionSpace.targetGroupID, targetGroupID)
        XCTAssertEqual(getReactionSpace.reactionPackIDs, reactionPackIDs)

        _ = try await withCheckedThrowingContinuation { c in
            livelike.reaction.getReactionSpaces(reactionSpaceID: nil, targetGroupID: nil, page: .first) {
                AssertResultSuccess($0)
                c.resume(with: $0)
            }
        }
        
        _ = try await withCheckedThrowingContinuation { c in
            livelike.reaction.deleteReactionSpace(reactionSpaceID: reactionSpace.id) {
                AssertResultSuccess($0)
                c.resume(with: $0)
            }
        }
    }
    
}
