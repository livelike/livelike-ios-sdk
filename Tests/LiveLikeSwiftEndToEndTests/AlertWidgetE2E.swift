//
//  AlertWidgetE2E.swift
//  
//
//  Created by Jelzon Monzon on 8/11/23.
//

import Foundation
import LiveLikeSwift
import XCTest

@available(iOS 13.0.0, *)
class AlertWidgetE2E: BaseE2E {
    
    override var accessToken: String? {
        get { return config.producerToken }
        set { super.accessToken = newValue }
    }
    
    func test_createAlert() async throws {
        
        let url = URL(string: "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")!
        
        let sponsors = try await withCheckedThrowingContinuation { c in
            livelike.sponsorship.getBy(programID: self.config.programID) {
                c.resume(with: $0)
            }
        }
        
        let alert = try await withCheckedThrowingContinuation { c in
            livelike.widgetClient.createAlertWidget(
                options: CreateAlertRequestOptions(
                    common: CommonCreateWidgetOptions(
                        programID: self.config.programID,
                        timeoutSeconds: 1000,
                        customData: "custom",
                        interactiveUntil: .distantFuture, playbackTimeMilliseconds: 1000,
                        widgetAttributes: [
                            .init(key: "key-a", value: "value-a"),
                            .init(key: "key-b", value: "value-b")
                        ],
                        sponsorIDs: sponsors.map { $0.id }
                    ),
                    title: "title",
                    text: "text",
                    imageURL: url,
                    linkLabel: "link label",
                    linkURL: url
                ),
                completion: { c.resume(with: $0) }
            )
        }
        
        XCTAssertEqual(alert.title, "title")
        XCTAssertEqual(alert.text, "text")
        XCTAssertEqual(alert.imageURL, url)
        XCTAssertEqual(alert.linkLabel, "link label")
        XCTAssertEqual(alert.customData, "custom")
        XCTAssertEqual(alert.linkURL, url)
        XCTAssertEqual(alert.playbackTimeMilliseconds, 1000)
        XCTAssertEqual(alert.interactiveUntil, .distantFuture)
        XCTAssertTrue(alert.widgetAttributes.contains(Attribute(key: "key-a", value: "value-a")))
        XCTAssertTrue(alert.widgetAttributes.contains(Attribute(key: "key-b", value: "value-b")))
        XCTAssertEqual(Set(sponsors.map { $0.id }), Set(alert.sponsors.map { $0.id }))
        
        /// Cleanup
        _ = try await deleteWidget(model: alert)
    }
    
    func test_createAndPublish() async throws {
        /// STEP 1 - Tests programDateTime param
        
        try await testCreateAndPublish(
            programDateTime: nil,
            assertions: { publishInfo in
                XCTAssertEqual(publishInfo.programDateTime, nil)
            }
        )

        try await testCreateAndPublish(
            programDateTime: .distantPast,
            assertions: { publishInfo in
                XCTAssertEqual(publishInfo.programDateTime, .distantPast)
            }
        )

        try await testCreateAndPublish(
            programDateTime: .distantFuture,
            assertions: { publishInfo in
                XCTAssertEqual(publishInfo.programDateTime, .distantFuture)
            }
        )

        /// STEP 3 - Tests publishDelay param

        // publishDelay is set to "00:00:00" when nil
        try await testCreateAndPublish(
            publishDelaySeconds: nil,
            assertions: { publishInfo in
                XCTAssertEqual(publishInfo.publishDelayISO8601, "00:00:00")
            }
        )

        try await testCreateAndPublish(
            publishDelaySeconds: 0,
            assertions: { publishInfo in
                XCTAssertEqual(publishInfo.publishDelayISO8601, "00:00:00")
            }
        )
        
        try await testCreateAndPublish(
            publishDelaySeconds: 9000,
            assertions: { publishInfo in
                XCTAssertEqual(publishInfo.publishDelayISO8601, "02:30:00")
            }
        )
        
        func testCreateAndPublish(
            programDateTime: Date? = nil,
            publishDelaySeconds: UInt? = nil,
            assertions: ((PublishedWidgetInfo) -> Void)? = nil
        ) async throws {
            /// STEP 1 - Create alert
            
            let alertModel = try await withCheckedThrowingContinuation { c in
                livelike.widgetClient.createAlertWidget(
                    options: CreateAlertRequestOptions(
                        common: CommonCreateWidgetOptions(
                            programID: self.config.programID,
                            timeoutSeconds: 10
                        ),
                        text: "test"
                    ),
                    completion: { c.resume(with: $0) }
                )
            }
            
            /// STEP 2 - Publish Poll
            let publishedPoll = try await withCheckedThrowingContinuation { c in
                livelike.widgetClient.publishWidget(
                    options: PublishWidgetRequestOptions(
                        kind: alertModel.kind,
                        id: alertModel.id,
                        publishDelaySeconds: publishDelaySeconds,
                        programDateTime: programDateTime
                    )
                ) { c.resume(with: $0) }
            }
            
            assertions?(publishedPoll)
            
            /// STEP 3 - Cleanup
            _ = try await withCheckedThrowingContinuation { c in
                livelike.widgetClient.deleteWidget(options: DeleteWidgetRequestOptions(
                    id: alertModel.id,
                    kind: alertModel.kind
                )
                ) {
                    c.resume(with: $0)
                }
            }
        }
    }
}
