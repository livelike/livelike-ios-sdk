//
//  CheerMeterWidgetE2E.swift
//  
//
//  Created by Jelzon Monzon on 5/15/23.
//

import Foundation
import XCTest
import LiveLikeSwift
import LiveLikeWidgetPublish

@available(iOS 13.0.0, *)
class CheerMeterWidgetE2E: BaseE2E {
    
    override var accessToken: String? {
        get { return config.producerToken }
        set { super.accessToken = newValue }
    }
    
    
    /// Validates data
    func testData() async throws {
        let cheerMeter = try await withCheckedThrowingContinuation { c in
            WidgetPublishAPI.createCheerMeter(
                baseURL: self.livelike.currentConfiguration.apiOrigin,
                accessToken: config.producerToken,
                payload: CreateCheerMeter(
                    question: "my question",
                    cheer_type: "tap",
                    program_id: config.programID,
                    timeout: "P0DT00H00M10S",
                    options: [
                        .init(description: "Mike", image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!),
                        .init(description: "Jelzon", image_url: URL(string: "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")!)
                    ],
                    custom_data: "my custom data"
                ),
                completion: {
                    c.resume(with: $0)
                }
            )
        }
        
        let cheerMeterModel: CheerMeterWidgetModel = try await withCheckedThrowingContinuation { c in
            livelike.getWidgetModel(
                CheerMeterWidgetModel.self,
                id: cheerMeter.id,
                kind: WidgetKind(stringValue: cheerMeter.kind)!
            ) {
                c.resume(with: $0)
            }
        }
        
        XCTAssertEqual(cheerMeterModel.customData, "my custom data")
        XCTAssertEqual(cheerMeterModel.userVotes.count, 0)
        XCTAssertEqual(cheerMeterModel.options.count, 2)
        XCTAssertEqual(cheerMeterModel.programID, config.programID)
        XCTAssertEqual(cheerMeterModel.title, "my question")
        
        let interactionHistory = try await withCheckedThrowingContinuation { c in
            cheerMeterModel.loadInteractionHistory {
                c.resume(with: $0)
            }
        }
        
        XCTAssertEqual(interactionHistory.count, 0)
        
        try await deleteWidget(model: cheerMeterModel)
    }
    
    /// Tests voting on multiple options in succession
    func testVoting() async throws {
        
        let cheerMeter = try await withCheckedThrowingContinuation { c in
            WidgetPublishAPI.createCheerMeter(
                baseURL: self.livelike.currentConfiguration.apiOrigin,
                accessToken: config.producerToken,
                payload: CreateCheerMeter(
                    question: "my question",
                    cheer_type: "tap",
                    program_id: config.programID,
                    timeout: "P0DT00H00M10S",
                    options: [
                        .init(description: "Mike", image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!),
                        .init(description: "Jelzon", image_url: URL(string: "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")!)
                    ],
                    custom_data: "my custom data"
                ),
                completion: {
                    c.resume(with: $0)
                }
            )
        }
        
        let cheerMeterModel: CheerMeterWidgetModel = try await withCheckedThrowingContinuation { c in
            livelike.getWidgetModel(
                CheerMeterWidgetModel.self,
                id: cheerMeter.id,
                kind: WidgetKind(stringValue: cheerMeter.kind)!
            ) {
                c.resume(with: $0)
            }
        }
        
        cheerMeterModel.submitVote(optionID: cheerMeterModel.options[0].id)
        cheerMeterModel.submitVote(optionID: cheerMeterModel.options[0].id)
        cheerMeterModel.submitVote(optionID: cheerMeterModel.options[0].id)
        
        cheerMeterModel.submitVote(optionID: cheerMeterModel.options[1].id)
        cheerMeterModel.submitVote(optionID: cheerMeterModel.options[1].id)
        cheerMeterModel.submitVote(optionID: cheerMeterModel.options[1].id)
        
        repeat {
            try await Task.sleep(nanoseconds: 1_000_000_000)
        } while
            cheerMeterModel.options[0].voteCount != 3 &&
            cheerMeterModel.options[1].voteCount != 3
        
        XCTAssertEqual(cheerMeterModel.options[0].voteCount, 3)
        XCTAssertEqual(cheerMeterModel.options[1].voteCount, 3)
       
        let interactionHistory = try await withCheckedThrowingContinuation { c in
            cheerMeterModel.loadInteractionHistory {
                c.resume(with: $0)
            }
        }
                
        XCTAssertEqual(interactionHistory.count, 6)
        
        try await deleteWidget(model: cheerMeterModel)
    }
}
