//
//  PredictionWidgetE2ETests.swift
//  
//
//  Created by Jelzon Monzon on 8/8/23.
//

import Foundation
import LiveLikeSwift
import XCTest

@available(iOS 13.0.0, *)
class PredictionWidgetE2ETests: BaseE2E {
    
    override var accessToken: String? {
        get { return config.producerToken }
        set { super.accessToken = newValue }
    }
    
    func test_createTextPrediction_updateCorrectOptions_andFollowUp() async throws {
        
        /// STEP 1 - Create a text prediction widget
        /// A follow up is automatically created
        let prediction = try await withCheckedThrowingContinuation { c in
            livelike.widgetClient.createTextPredictionWidget(
                options: CreateTextPredictionRequestOptions(
                    common: CommonCreateWidgetOptions(
                        programID: self.config.programID,
                        timeoutSeconds: 1000
                    ),
                    question: "e2e",
                    choices: [
                        .init(text: "a"),
                        .init(text: "b"),
                        .init(text: "c")
                    ]

                ),
                completion: { c.resume(with: $0) }
            )
        }

        /// STEP 2 - Update the first option of the follow up to be correct
        let updateCorrect = try await withCheckedThrowingContinuation { c in
            livelike.widgetClient.updateTextPredictionFollowUpWidgetOption(
                options: UpdateTextPredictionFollowUpRequestOptions(
                    predictionID: prediction.id,
                    optionID: prediction.options[0].id,
                    isCorrect: true
                )
            ) {
                c.resume(with: $0)
            }
        }
        
        /// STEP 3 - Update the second option of the follow up to be correct
        /// Doing this twice because predictions support multiple correct answers
        let updateCorrect2 = try await withCheckedThrowingContinuation { c in
            livelike.widgetClient.updateTextPredictionFollowUpWidgetOption(
                options: UpdateTextPredictionFollowUpRequestOptions(
                    predictionID: prediction.id,
                    optionID: prediction.options[1].id,
                    isCorrect: true
                )
            ) {
                c.resume(with: $0)
            }
        }
        
        /// STEP 4 - Get the follow up widget
        let followUp: PredictionFollowUpWidgetModel = try await withCheckedThrowingContinuation { c in
            livelike.getWidgetModel(
                id: prediction.followUpWidgetModels.first!.id,
                kind: prediction.followUpWidgetModels.first!.kind
            ) {
                switch $0 {
                case .failure(let error):
                    c.resume(throwing: error)
                case .success(let widgetModel):
                    switch widgetModel {
                    case .predictionFollowUp(let model):
                        c.resume(returning: model)
                    default:
                        c.resume(throwing: BasicError())
                    }
                }
            }
        }
        
        XCTAssertEqual(followUp.options.first(where: { $0.id == updateCorrect.id})?.isCorrect, true)
        XCTAssertEqual(followUp.options.first(where: { $0.id == updateCorrect2.id})?.isCorrect, true)
        
    }
    
    func test_createImagePrediction_updateCorrectOptions_andFollowUp() async throws {
        
        let imageURL = URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!
        
        /// STEP 1 - Create a text prediction widget
        /// A follow up is automatically created
        let prediction = try await withCheckedThrowingContinuation { c in
            livelike.widgetClient.createImagePredictionWidget(
                options: CreateImagePredictionRequestOptions(
                    common: CommonCreateWidgetOptions(
                        programID: self.config.programID,
                        timeoutSeconds: 1000
                    ),
                    question: "e2e",
                    choices: [
                        .init(text: "a", imageURL: imageURL),
                        .init(text: "b", imageURL: imageURL),
                        .init(text: "c", imageURL: imageURL)
                    ]
                ),
                completion: { c.resume(with: $0) }
            )
        }
        
        /// STEP 2 - Update the first option of the follow up to be correct
        let updateCorrect = try await withCheckedThrowingContinuation { c in
            livelike.widgetClient.updateImagePredictionFollowUpWidgetOption(
                options: UpdateImagePredictionFollowUpRequestOptions(
                    predictionID: prediction.id,
                    optionID: prediction.options[0].id,
                    isCorrect: true
                )
            ) {
                c.resume(with: $0)
            }
        }

        /// STEP 3 - Update the second option of the follow up to be correct
        /// Doing this twice because predictions support multiple correct answers
        let updateCorrect2 = try await withCheckedThrowingContinuation { c in
            livelike.widgetClient.updateImagePredictionFollowUpWidgetOption(
                options: UpdateImagePredictionFollowUpRequestOptions(
                    predictionID: prediction.id,
                    optionID: prediction.options[1].id,
                    isCorrect: true
                )
            ) {
                c.resume(with: $0)
            }
        }
        
        /// STEP 4 - Get the follow up widget
        let followUp: PredictionFollowUpWidgetModel = try await withCheckedThrowingContinuation { c in
            livelike.getWidgetModel(
                id: prediction.followUpWidgetModels.first!.id,
                kind: prediction.followUpWidgetModels.first!.kind
            ) {
                switch $0 {
                case .failure(let error):
                    c.resume(throwing: error)
                case .success(let widgetModel):
                    switch widgetModel {
                    case .predictionFollowUp(let model):
                        c.resume(returning: model)
                    default:
                        c.resume(throwing: BasicError())
                    }
                }
            }
        }

        XCTAssertEqual(followUp.options.first(where: { $0.id == updateCorrect.id})?.isCorrect, true)
        XCTAssertEqual(followUp.options.first(where: { $0.id == updateCorrect2.id})?.isCorrect, true)
        
        /// STEP 5 - Cleanup
        _ = try await withCheckedThrowingContinuation { c in
            livelike.widgetClient.deleteWidget(
                options: DeleteWidgetRequestOptions(
                    id: followUp.id, kind: followUp.kind
                )
            ) { c.resume(with: $0) }
        }

        _ = try await withCheckedThrowingContinuation { c in
            livelike.widgetClient.deleteWidget(
                options: DeleteWidgetRequestOptions(
                    id: prediction.id, kind: prediction.kind
                )
            ) { c.resume(with: $0) }
        }
        
    }
    
}
