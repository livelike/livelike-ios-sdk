//
//  WidgetE2E.swift
//  
//
//  Created by Jelzon Monzon on 6/23/23.
//

import Foundation
import XCTest
import LiveLikeSwift

@available(iOS 13.0.0, *)
class WidgetE2E: BaseE2E {
    
    private var contentSession: ContentSession!
    
    override func setUp() {
        self.contentSession = livelike.contentSession(
            config: SessionConfiguration(programID: config.programID)
        )
    }
    
    func test_getWidgetModels() async throws {
        try await getWidgetModels(
            options: GetWidgetModelsRequestOptions(
                widgetStatus: .pending
            )
        )
        try await getWidgetModels(
            options: GetWidgetModelsRequestOptions(
                widgetStatus: .published
            )
        )
        try await getWidgetModels(
            options: GetWidgetModelsRequestOptions(
                widgetStatus: .scheduled
            )
        )
        try await getWidgetModels(
            options: GetWidgetModelsRequestOptions(
                widgetKind: Set([.textQuiz, .textPoll])
            )
        )
        try await getWidgetModels(
            options: GetWidgetModelsRequestOptions(
                widgetKind: Set([.cheerMeter, .textAsk])
            )
        )
        try await getWidgetModels(
            options: GetWidgetModelsRequestOptions(
                widgetKind: Set([.richPost])
            )
        )
        try await getWidgetModels(
            options: GetWidgetModelsRequestOptions(
                widgetOrdering: .oldest
            )
        )
        try await getWidgetModels(
            options: GetWidgetModelsRequestOptions(
                widgetOrdering: .recent
            )
        )
        try await getWidgetModels(
            options: GetWidgetModelsRequestOptions(
                interactive: true
            )
        )
        try await getWidgetModels(
            options: GetWidgetModelsRequestOptions(
                interactive: false
            )
        )
        try await getWidgetModels(
            options: GetWidgetModelsRequestOptions(
                since: .distantPast
            )
        )
        try await getWidgetModels(
            options: GetWidgetModelsRequestOptions(
                since: .distantFuture
            )
        )
        try await getWidgetModels(
            options: GetWidgetModelsRequestOptions(
                sincePlaybackTimeMilliseconds: 0,
                untilPlaybackTimeMilliseconds: 10000
            )
        )
        try await getWidgetModels(
            options: GetWidgetModelsRequestOptions(
                sincePlaybackTimeMilliseconds: 10000,
                untilPlaybackTimeMilliseconds: 20000
            )
        )
        try await getWidgetModels(
            options: GetWidgetModelsRequestOptions(
                widgetAttributes: [
                    Attribute(key: "team", value: "red"),
                    Attribute(key: "mood", value: "happy")
                ]
            )
        )
    }
    
    @discardableResult
    private func getWidgetModels(
        options: GetWidgetModelsRequestOptions,
        file: StaticString = #filePath,
        line: UInt = #line
    ) async throws -> [WidgetModel] {
        return try await withCheckedThrowingContinuation { c in
            contentSession.getWidgetModels(
                page: .first,
                options: options
            ) { result in
                AssertResultSuccess(result, file: file, line: line)
                c.resume(with: result)
            }
        }
    }
    
}
