//
//  ChatE2E.swift
//  
//
//  Created by Jelzon Monzon on 5/5/23.
//

import Foundation
import XCTest
import LiveLikeSwift
import LiveLikeCore

@available(iOS 13.0.0, *)
class ChatE2E: XCTestCase {
    
    private var livelike: LiveLike!
    private var config: Config!
    
    class TokenStorage: AccessTokenStorage {
        init(token: String) {
            self.token = token
        }
        
        let token: String
        
        func fetchAccessToken() -> String? {
            return token
        }
        
        func storeAccessToken(accessToken: String) { }
    }
    
    override func setUp() async throws {
        self.config = try Config.load()
        var llConfig = LiveLikeConfig(clientID: self.config.clientID)
        llConfig.accessTokenStorage = TokenStorage(token: config.producerToken)
        llConfig.apiOrigin = config.apiOrigin
        self.livelike = LiveLike(config: llConfig)
    }
    
    func test_getMessages_default() async throws {
        
        // Create a chat room
        let chatRoomID: String = try await withCheckedThrowingContinuation { continuation in
            self.livelike.createChatRoom(
                title: "my title",
                visibility: .everyone
            ) {
                continuation.resume(with: $0)
            }
        }
        
        let messages: [ChatMessage] = try await withCheckedThrowingContinuation { c in
            self.livelike.chat.getChatMessages(
                page: .first,
                options: GetChatMessageRequestOptions(chatRoomID: chatRoomID)
            ) {
                AssertResultSuccess($0)
                c.resume(with: $0)
            }
        }
        
    }
    
    func test_threading_send() async throws {
        
        // Create a chat room
        let chatRoomID: String = try await withCheckedThrowingContinuation { continuation in
            self.livelike.createChatRoom(
                title: "my title",
                visibility: .everyone
            ) {
                continuation.resume(with: $0)
            }
        }
        
        let chatSession: ChatSession = try await withCheckedThrowingContinuation { continuation in
            self.livelike.connectChatRoom(config: ChatSessionConfig(roomID: chatRoomID)) {
                continuation.resume(with: $0)
            }
        }
        
        let rootMessage: ChatMessage = try await withCheckedThrowingContinuation { c in
            chatSession.sendMessage(
                NewChatMessage(
                    text: "root message"
                ) 
            ) {
                c.resume(with: $0)
            }
        }
        
        sleep(2)
        
        let firstReply: ChatMessage = try await withCheckedThrowingContinuation { c in
            chatSession.sendMessage(
                NewChatMessage(
                    text: "first reply",
                    parentMessageID: rootMessage.id.asString
                )
            ) {
                c.resume(with: $0)
            }
        }
        
        sleep(2)
        
        let messages: [ChatMessage] = try await withCheckedThrowingContinuation { c in
            self.livelike.chat.getChatMessages(
                page: .first,
                options: GetChatMessageRequestOptions(
                    chatRoomID: chatRoomID,
                    parentMessageID: rootMessage.id.asString
                )
            ) {
                AssertResultSuccess($0)
                c.resume(with: $0)
            }
        }
        
        XCTAssertEqual(messages.first!.id.asString, firstReply.id.asString)
        XCTAssertEqual(firstReply.parentMessageID, rootMessage.id.id)
        
    }
    
    func test_threading_reactions() async throws {
        // Create a chat room
        let chatRoomID: String = try await withCheckedThrowingContinuation { continuation in
            self.livelike.createChatRoom(
                title: "my title",
                visibility: .everyone
            ) {
                continuation.resume(with: $0)
            }
        }
        
        let chatRoom = try await withCheckedThrowingContinuation { c in
            self.livelike.getChatRoomInfo(roomID: chatRoomID
            ) {
                c.resume(with: $0)
            }
        }
        
        let chatSession: ChatSession = try await withCheckedThrowingContinuation { continuation in
            self.livelike.connectChatRoom(config: ChatSessionConfig(roomID: chatRoomID)) {
                continuation.resume(with: $0)
            }
        }
        
        let rootMessage: ChatMessage = try await withCheckedThrowingContinuation { c in
            chatSession.sendMessage(
                NewChatMessage(
                    text: "root message"
                )
            ) {
                c.resume(with: $0)
            }
        }
        
        sleep(2)
        
        let firstReply: ChatMessage = try await withCheckedThrowingContinuation { c in
            chatSession.sendMessage(
                NewChatMessage(
                    text: "first reply",
                    parentMessageID: rootMessage.id.asString
                )
            ) {
                c.resume(with: $0)
            }
        }
        
        sleep(2)

        
        let reactionSpace = try await withCheckedThrowingContinuation { c in
            livelike.reaction.getReactionSpaceInfo(
                reactionSpaceID: chatRoom.reactionSpaceID!
            ) {
                c.resume(with: $0)
            }
        }
        
        let reactionPacks = try await withCheckedThrowingContinuation { c in
            livelike.reaction.getReactionPacks(
                page: .first
            ) {
                c.resume(with: $0)
            }
        }
        
        // set reaction pack
        _ = try await withCheckedThrowingContinuation { c in
            UpdateReactionSpaceRequest(
                options: UpdateReactionSpaceRequestOptions(
                    reactionSpaceID: reactionSpace.id,
                    reactionPackIDs: [reactionPacks.first!.id]
                ),
                networking: LLCore.networking,
                accessToken: config.producerToken
            ).execute(
                url: config.apiOrigin
                    .appendingPathComponent("reaction-spaces/\(reactionSpace.id)/")
            ) {
                c.resume(with: $0)
            }
        }
        
        let reactionPack: ReactionPack = try await withCheckedThrowingContinuation { c in
            livelike.reaction.getReactionPackInfo(
                reactionPackID: reactionPacks.first!.id
            ) {
                c.resume(with: $0)
            }
        }
        
        let reactionSession = livelike.reaction.createReactionSession(reactionSpace: reactionSpace)
        
        _ = try await withCheckedThrowingContinuation { c in
            reactionSession.addUserReaction(
                targetID: firstReply.id.asString,
                reactionID: reactionPack.emojis[0].id,
                customData: nil
            ) {
                c.resume(with: $0)
            }
        }
        
        let targetID = firstReply.id.asString
        let reactionID = reactionPack.emojis[1].id
        
        _ = try await withCheckedThrowingContinuation { c in
            reactionSession.addUserReaction(
                targetID: targetID,
                reactionID: reactionID,
                customData: nil
            ) {
                c.resume(with: $0)
            }
        }
        
        let reactionCounts = try await withCheckedThrowingContinuation { c in
            reactionSession.getUserReactionsCount(
                reactionSpaceID: reactionSpace.id,
                targetID: [targetID],
                page: .first
            ) {
                c.resume(with: $0)
            }
        }
        
        XCTAssertEqual(
            reactionCounts
                .first(where: { $0.targetID == targetID})!.reactions
                .first(where: { $0.reactionID == reactionID})!
                .count,
            1
        )
    }
    
    /// Tests that you can reply to a quoted message
    func test_quote_reply() async throws {
        // Create a chat room
        let chatRoomID: String = try await withCheckedThrowingContinuation { continuation in
            self.livelike.createChatRoom(
                title: "my title",
                visibility: .everyone
            ) {
                continuation.resume(with: $0)
            }
        }
        
        let chatSession: ChatSession = try await withCheckedThrowingContinuation { continuation in
            self.livelike.connectChatRoom(config: ChatSessionConfig(roomID: chatRoomID)) {
                continuation.resume(with: $0)
            }
        }
        
        let messageToQuote: ChatMessage = try await withCheckedThrowingContinuation { c in
            chatSession.sendMessage(
                NewChatMessage(
                    text: "root message"
                )
            ) {
                c.resume(with: $0)
            }
        }
        
        sleep(2)
        
        let quotedMessage: ChatMessage = try await withCheckedThrowingContinuation { c in
            chatSession.quoteMessage(
                NewChatMessage(text: "quoted message to reply to"),
                quoteMessage: ChatMessagePayload(chatMessage: messageToQuote)
            ) {
                c.resume(with: $0)
            }
        }
        
        sleep(2)
        
        let replyToQuotedMessage: ChatMessage = try await withCheckedThrowingContinuation { c in
            chatSession.sendMessage(
                NewChatMessage(
                    text: "reply to quoted message",
                    parentMessageID: quotedMessage.id.id
                )
            ) {
                c.resume(with: $0)
            }
        }
        
        sleep(2)
        
        XCTAssertEqual(replyToQuotedMessage.parentMessageID, quotedMessage.id.id)
    }
    
    /// Tests that you can reply to a message with a quoted message
    func test_reply_with_quote() async throws {
        // Create a chat room
        let chatRoomID: String = try await withCheckedThrowingContinuation { continuation in
            self.livelike.createChatRoom(
                title: "my title",
                visibility: .everyone
            ) {
                continuation.resume(with: $0)
            }
        }
        
        let chatSession: ChatSession = try await withCheckedThrowingContinuation { continuation in
            self.livelike.connectChatRoom(config: ChatSessionConfig(roomID: chatRoomID)) {
                continuation.resume(with: $0)
            }
        }
        
        let rootMessage: ChatMessage = try await withCheckedThrowingContinuation { c in
            chatSession.sendMessage(
                NewChatMessage(
                    text: "root message"
                )
            ) {
                c.resume(with: $0)
            }
        }
        
        sleep(2)
        
        let messageToQuote: ChatMessage = try await withCheckedThrowingContinuation { c in
            chatSession.sendMessage(
                NewChatMessage(
                    text: "message to quote"
                )
            ) {
                c.resume(with: $0)
            }
        }
        
        sleep(2)
        
        let replyWithQuotedMessage: ChatMessage = try await withCheckedThrowingContinuation { c in
            chatSession.quoteMessage(
                NewChatMessage(
                    text: "first reply",
                    parentMessageID: rootMessage.id.asString
                ),
                quoteMessage: ChatMessagePayload(chatMessage: messageToQuote)
            ) {
                c.resume(with: $0)
            }
        }
        
        sleep(2)
        
        XCTAssertEqual(replyWithQuotedMessage.parentMessageID, rootMessage.id.id)
        XCTAssertEqual(replyWithQuotedMessage.quoteMessage?.id, messageToQuote.id.id)
    }
    
    /// 1. Create a chat room
    /// 2. Join a chat session
    /// 3. Send various messages
    /// 4. Delete messages
    func test_sendAndDeleteMessages() async throws {
        
        // Create a chat room
        
        let chatRoomID: String = try await withCheckedThrowingContinuation { continuation in
            self.livelike.createChatRoom(
                title: "my title",
                visibility: .everyone
            ) {
                continuation.resume(with: $0)
            }
        }
        
        // Get chat room info
        
        let chatRoom: ChatRoomInfo = try await withCheckedThrowingContinuation { continuation in
            self.livelike.getChatRoomInfo(roomID: chatRoomID) {
                continuation.resume(with: $0)
            }
        }
        XCTAssertEqual(chatRoom.id, chatRoomID)
        XCTAssertEqual(chatRoom.title, "my title")
        XCTAssertEqual(chatRoom.visibility, .everyone)
        
        // Create a chat session
        
        let chatSession: ChatSession = try await withCheckedThrowingContinuation { continuation in
            self.livelike.connectChatRoom(config: ChatSessionConfig(roomID: chatRoomID)) {
                continuation.resume(with: $0)
            }
        }
        
        XCTAssertEqual(chatSession.roomID, chatRoomID)
        XCTAssertEqual(chatSession.title, "my title")
        
        // Send a text message
        
        let textChatMessage: ChatMessage = try await withCheckedThrowingContinuation { continuation in
            chatSession.sendMessage(NewChatMessage(text: "my text message")) {
                continuation.resume(with: $0)
            }
        }
        
        XCTAssertEqual(textChatMessage.text, "my text message")
        
        // Send an image message
        
        let imageChatMessage: ChatMessage = try await withCheckedThrowingContinuation { continuation in
            chatSession.sendMessage(
                NewChatMessage(
                    imageURL: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg"),
                    imageHeight: 100,
                    imageWidth: 100
                )
            ) {
                continuation.resume(with: $0)
            }
        }
        
        // Send a custom message (a)
        
        let customChatMessageA: ChatMessage = try await withCheckedThrowingContinuation { continuation in
            chatSession.sendCustomMessage("my custom message a") {
                continuation.resume(with: $0)
            }
        }

        XCTAssertEqual(customChatMessageA.customData, "my custom message a")
        
        // Send a custom message (b)
        
        let customChatMessageB: ChatMessage = try await withCheckedThrowingContinuation { continuation in
            chatSession.sendMessage(NewChatMessage(customData: "my custom message b")) {
                continuation.resume(with: $0)
            }
        }

        XCTAssertEqual(customChatMessageB.customData, "my custom message b")
        
        // Delete messages
        
        let textMessageDeleted: Bool = try await withCheckedThrowingContinuation { continuation in
            chatSession.deleteMessage(message: textChatMessage) {
                continuation.resume(with: $0)
            }
        }

        XCTAssertEqual(textMessageDeleted, true)

        let imageMessageDeleted: Bool = try await withCheckedThrowingContinuation { continuation in
            chatSession.deleteMessage(
                options: DeleteMessageRequestOptions(
                    messageID: imageChatMessage.id.id!,
                    pubnubTimetoken: imageChatMessage.createdAt.pubnubTimetoken
                )
            ) {
                continuation.resume(with: $0)
            }
        }

        XCTAssertEqual(imageMessageDeleted, true)

        let customMessageADeleted: Bool = try await withCheckedThrowingContinuation { continuation in
            chatSession.deleteMessage(message: customChatMessageA) {
                continuation.resume(with: $0)
            }
        }

        XCTAssertEqual(customMessageADeleted, true)
        
        let customMessageBDeleted: Bool = try await withCheckedThrowingContinuation { continuation in
            chatSession.deleteMessage(message: customChatMessageB) {
                continuation.resume(with: $0)
            }
        }

        XCTAssertEqual(customMessageBDeleted, true)
        
    }
    
    /// 1. Creates a members only chat room
    /// 2. Expected fail to join room
    /// 2. Adds self as chat room member
    /// 3. Joins room success
    /// 4. Deletes self as chat room member
    func test_chatMemberships() async throws {
        
        // Create a chat room
        
        let chatRoomID: String = try await withCheckedThrowingContinuation { continuation in
            self.livelike.createChatRoom(
                title: "my title",
                visibility: .members
            ) {
                continuation.resume(with: $0)
            }
        }
        
        let myProfileID: String = try await withCheckedThrowingContinuation { c in
            self.livelike.getCurrentUserProfileID {
                c.resume(with: $0)
            }
        }
        
        // Check that connectChatRoom fails when I am not a member
        do {
            let _ = try await withCheckedThrowingContinuation { c in
                self.livelike.connectChatRoom(config: ChatSessionConfig(roomID: chatRoomID)) {
                    c.resume(with: $0)
                }
            }
        } catch {
            XCTAssertEqual(error.localizedDescription, "forbidden")
        }
        
        let members: [ChatRoomMember] = try await withCheckedThrowingContinuation { c in
            self.livelike.getChatRoomMemberships(roomID: chatRoomID, page: .first) {
                c.resume(with: $0)
            }
        }
        
        XCTAssertTrue(members.isEmpty)
        
        let newMember: ChatRoomMember = try await withCheckedThrowingContinuation { c in
            self.livelike.chat.addNewMemberToChatRoom(roomID: chatRoomID, profileID: myProfileID) {
                c.resume(with: $0)
            }
        }
        
        XCTAssertEqual(newMember.profile.id, myProfileID)
        
        // Test that I can join the chat room as a membe
        let _ = try await withCheckedThrowingContinuation { c in
            self.livelike.connectChatRoom(config: ChatSessionConfig(roomID: chatRoomID)) {
                c.resume(with: $0)
            }
        }
        
        // delete the membership
        
        let memberDeleted: Bool = try await withCheckedThrowingContinuation { c in
            self.livelike.deleteUserChatRoomMembership(roomID: chatRoomID) {
                c.resume(with: $0)
            }
        }
        
        XCTAssertEqual(memberDeleted, true)
        
    }
    
    func test_getChatMembershipOptions() async throws {
        
        let chatRoomID = try await withCheckedThrowingContinuation { c in
            livelike.createChatRoom(
                title: "my room"
            ) { c.resume(with: $0) }
        }
        
        
        _ = try await withCheckedThrowingContinuation { c in
            livelike.getChatRoomMemberships(
                options: GetChatRoomMembershipsRequestOptions(
                    roomID: chatRoomID
                ),
                page: .first
            ) {
                AssertResultSuccess($0)
                c.resume(with: $0)
            }
        }
        
        let myProfileID = try await withCheckedThrowingContinuation { c in
            livelike.getCurrentUserProfileID { c.resume(with: $0) }
        }
        
        _ = try await withCheckedThrowingContinuation { c in
            livelike.getChatRoomMemberships(
                options: GetChatRoomMembershipsRequestOptions(
                    profileIDs: [myProfileID]
                ),
                page: .first
            ) {
                AssertResultSuccess($0)
                c.resume(with: $0)
            }
        }
        
    }
    
}
