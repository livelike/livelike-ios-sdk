//
//  SocialGraphE2E.swift
//  
//
//  Created by Mike Moloksher on 7/20/23.
//

import XCTest
import Foundation
import LiveLikeCore
import LiveLikeSwift

@available(iOS 13.0.0, *)
class SocialGraphE2E: BaseE2E {
    
    /*
     * The following test flow tests the social graph client where we have two users between which
     * we create a relationship.
     */
    func test_social_graph_client_flow() async throws {
        
        /*
         * STEP 1A  - Rerieve current user profile ID for later use
         */
        let currentUserProfileID: String = try await withCheckedThrowingContinuation { c in
            
            livelike.getCurrentUserProfileID { result in
                switch result {
                case .success(let profileID):
                    c.resume(with: .success(profileID))
                case .failure(let error):
                    XCTFail("Failed retrieving current user profile ID - \(error.localizedDescription)")
                    c.resume(throwing: error)
                }
            }
        }
        
        /*
         * STEP 1B - Create another user profile with which I can create a relationship with ❤️
         * https://docs.livelike.com/reference/create-user-profile
         */
        let nonCurrentUserProfileID: String = try await withCheckedThrowingContinuation { c in
            
            let url = URL(string: "\(config.apiOrigin)/applications/\(config.clientID)/profile/")!
    
            struct Payload: Encodable {
                var clientId: String
            }
            
            let resource = Resource<ProfileResource>(
                url: url,
                method: .post(Payload(clientId: config.clientID))
            )
            
            LLCore.networking.task(resource) { result in
                switch result {
                case .success(let userProfile):
                    c.resume(with: .success(userProfile.id))
                case .failure(let error):
                    XCTFail("Failed to create a Quest - \(error.localizedDescription)")
                    c.resume(with: .failure(error))
                }
            }
        }
        
        /*
         * STEP 2A - Create relationship type to use later
         * https://docs.livelike.com/reference/create-a-relationship-type-1
         */
        let newRelationshipType = try await withCheckedThrowingContinuation { c in
            
            let url = URL(string: "\(config.apiOrigin)/profile-relationship-types/")!
    
            struct Payload: Encodable {
                var clientId: String
                var name: String
                var key: String
            }
            
            let rightNow = String(Date().timeIntervalSince1970)
            
            let resource = Resource<ProfileRelationshipType>(
                url: url,
                method: .post(Payload(clientId: config.clientID, name: "E2E Follow", key: "e2e_follow_\(rightNow)")),
                accessToken: config.producerToken
            )
            
            LLCore.networking.task(resource) { result in
                switch result {
                case .success(let relationshipType):
                    c.resume(with: .success(relationshipType))
                case .failure(let error):
                    XCTFail("Failed to create a relationship type - \(error.localizedDescription)")
                    c.resume(with: .failure(error))
                }
            }
        }
        
        /*
         * STEP 3A - Creates a relationship between the current user and non current user
         */
        let profileRelationshipID = try await withCheckedThrowingContinuation { c in
            
            livelike.socialGraphClient.createProfileRelationship(
                options: CreateProfileRelationshipOptions(
                    fromProfileID: currentUserProfileID,
                    toProfileID: nonCurrentUserProfileID,
                    relationshipTypeKey: newRelationshipType.key
                )
            ) { result in
                
                switch result {
                case .success(let profileRelationship):
                    c.resume(with: .success(profileRelationship.id))
                case .failure(let error):
                    XCTFail("Failed created a relationship between two users - \(error.localizedDescription)")
                    c.resume(throwing: error)
                }
            }
        }
        
        /*
         * STEP 3B - Verify the relationship on the backend that was created in the previous step
         */
        let _ = try await withCheckedThrowingContinuation { c in
            
            livelike.socialGraphClient.getProfileRealtionshipDetails(
                options: GetProfileRealtionshipDetailsOptions(profileRelationshipID: profileRelationshipID)
            ) { result in
                
                switch result {
                case .success(_):
                    c.resume()
                case .failure(let error):
                    XCTFail("Failed created a relationship between two users - \(error.localizedDescription)")
                    c.resume(throwing: error)
                }
            }
        }
        
        /*
         * STEP  - Delete created relationship
         */
        let _ = try await withCheckedThrowingContinuation { c in
            
            livelike.socialGraphClient.deleteProfileRelationship(
                options: DeleteProfileRelationshipOptions(profileRelationshipID: profileRelationshipID)
            ) { result in
                
                switch result {
                case .success(_):
                    c.resume()
                case .failure(let error):
                    XCTFail("Failed to delete profile relationship)")
                    c.resume(throwing: error)
                }
            }
        }
        
        /*
         * STEP Cleanup - Delete newly created user profile to run this test
         */
        let _ = try await withCheckedThrowingContinuation { c in
            
            let url = URL(string: "\(config.apiOrigin)/profiles/\(nonCurrentUserProfileID)/")!
    
            struct Payload: Encodable {
                var id: String
            }
            
            let resource = Resource<Bool>(
                url: url,
                method: .delete(Payload(id: nonCurrentUserProfileID)),
                accessToken: config.producerToken
            )
            
            LLCore.networking.task(resource) { result in
                switch result {
                case .success(_):
                    c.resume()
                case .failure(let error):
                    XCTFail("Failed to delete a new user profile - \(error.localizedDescription)")
                    c.resume(with: .failure(error))
                }
            }
        }
    }
    
    
}
