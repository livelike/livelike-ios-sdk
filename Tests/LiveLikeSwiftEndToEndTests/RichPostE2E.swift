//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//
	

import Foundation
import XCTest
@testable import LiveLikeSwift

@available(iOS 13.0, *)
class RichPostE2E: BaseE2E {
    
    override var accessToken: String? {
        get { return config.producerToken }
        set { super.accessToken = newValue }
    }
    
    func test_create() async throws {
        
        let richPost = try await withCheckedThrowingContinuation { c in
            livelike.widgetClient.createRichPost(
                options: CreateRichPostWidgetOptions(
                    common: CommonCreateWidgetOptions(programID: config.programID),
                    content: "html content",
                    title: "title"
                )
            ) { result in
                c.resume(with: result)
            }
        }
        
        XCTAssertEqual(richPost.title, "title")
        XCTAssertEqual(richPost.content, "html content")
        
        
    }
    
}
