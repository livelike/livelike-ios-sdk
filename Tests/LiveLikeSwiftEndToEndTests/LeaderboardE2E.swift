//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//
	

import Foundation
import LiveLikeSwift
import XCTest

@available(iOS 13.0.0, *)
class LeaderboardE2E: BaseE2E {
    
    func test_getLeaderboardEntries() async throws {
        let leaderboard = try await withCheckedThrowingContinuation { c in
            livelike.getLeaderboards(
                programID: config.programID
            ) {
                c.resume(with: $0.map({ $0.first!}))
            }
        }
        
        let entries = try await withCheckedThrowingContinuation { c in
            livelike.getLeaderboardEntries(
                page: .first,
                options: GetLeaderboardEntriesRequestOptions(
                    leaderboardID: leaderboard.id,
                    pageSize: 100
                )
            ) {
                c.resume(with: $0)
            }
        }
        
    }
    
}
