//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//
	
import Foundation
import LiveLikeCore
import LiveLikeSwift

struct UpdateReactionSpaceResponse: Decodable {
}

struct UpdateReactionSpaceRequestOptions {
    let reactionSpaceID: String
    let reactionPackIDs: [String]?
    
    init(
        reactionSpaceID: String,
        reactionPackIDs: [String]? = nil
    ) {
        self.reactionSpaceID = reactionSpaceID
        self.reactionPackIDs = reactionPackIDs
    }
}

struct UpdateReactionSpaceRequest: LLRequest {
    
    let options: UpdateReactionSpaceRequestOptions
    let networking: LLNetworking
    let accessToken: String
    
    init(
        options: UpdateReactionSpaceRequestOptions,
        networking: LLNetworking,
        accessToken: String
    ) {
        self.options = options
        self.networking = networking
        self.accessToken = accessToken
    }
    
    func execute(
        url: URL,
        completion: @escaping (Result<UpdateReactionSpaceResponse, Error>) -> Void
    ) {
        struct Payload: Encodable {
            let reactionPackIds: [String]?
        }
        let resource = Resource<UpdateReactionSpaceResponse>(
            url: url,
            method: .patch(
                Payload(
                    reactionPackIds: self.options.reactionPackIDs
                )
            ), 
            accessToken: self.accessToken
        )
        networking.task(resource, completion: completion)
    }
    
}
