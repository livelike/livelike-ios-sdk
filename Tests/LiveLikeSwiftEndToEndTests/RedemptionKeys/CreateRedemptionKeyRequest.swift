//
//  CreateRedemptionKey.swift
//  
//
//  Created by Jelzon Monzon on 7/27/23.
//

import Foundation
import LiveLikeSwift
import LiveLikeCore

struct CreateRedemptionKeyRequest: LLRequest {

    private let networking: LLNetworking
    private let accessToken: String
    private let keyName: String
    private let keyCode: String?
    private let keyDescription: String
    private let clientID: String
    private let assignedTo: String?

    init(
        networking: LLNetworking,
        accessToken: String,
        keyName: String,
        keyCode: String?,
        keyDescription: String,
        clientID: String,
        assignedTo: String?
    ) {
        self.networking = networking
        self.accessToken = accessToken
        self.keyName = keyName
        self.keyCode = keyCode
        self.keyDescription = keyDescription
        self.clientID = clientID
        self.assignedTo = assignedTo
    }

    func execute(
        url: URL,
        completion: @escaping (Result<RedemptionKey, Error>) -> Void
    ) {
        struct Body: Encodable {
            let name: String
            let code: String?
            let description: String
            let clientId: String
            let assignedTo: String?
        }
        let resource = Resource<RedemptionKey>(
            url: url,
            method: .post(
                Body(
                    name: self.keyName,
                    code: self.keyCode,
                    description: self.keyDescription,
                    clientId: self.clientID,
                    assignedTo: self.assignedTo

                )
            ),
            accessToken: self.accessToken
        )
        networking.task(resource, completion: completion)
    }
}
