//
//  RedemptionKeyE2E.swift
//  
//
//  Created by Jelzon Monzon on 7/27/23.
//

import Foundation
import XCTest
import LiveLikeSwift
import LiveLikeCore

@available(iOS 13.0.0, *)
class RedemptionKeyE2E: BaseE2E {
    
    func test_redeemByCode() async throws {
        
        // Create a redemption key (producer)
        let keyCode = UUID().uuidString.padding(toLength: 35, withPad: "", startingAt: 0)
        let redemptionKey = try await withCheckedThrowingContinuation { c in
            CreateRedemptionKeyRequest(
                networking: LLCore.networking,
                accessToken: config.producerToken,
                keyName: "key name",
                keyCode: keyCode,
                keyDescription: "key description",
                clientID: config.clientID,
                assignedTo: nil
            ).execute(
                url: config.apiOrigin.appendingPathComponent("redemption-keys/")
            ) {
                c.resume(with: $0)
            }
        }
        
        // Redeem the key using code
        let redeemedKey = try await withCheckedThrowingContinuation { c in
            self.livelike.redeemKeyBy(redemptionCode: redemptionKey.code) {
                c.resume(with: $0)
            }
        }
        
        XCTAssertEqual(redeemedKey.status, .redeemed)
        
        // Cleanup
        let _ = try await withCheckedThrowingContinuation { c in
            DeleteRedemptionKeyRequest(
                networking: LLCore.networking,
                accessToken: config.producerToken
            ).execute(
                url: config.apiOrigin.appendingPathComponent("redemption-keys/\(redemptionKey.id)/")
            ) {
                c.resume(with: $0)
            }

        }
    }
    
    func test_redeemByKeyID() async throws {
        
        // Create a redemption key (producer)
        let keyCode = UUID().uuidString.padding(
            toLength: 35,
            withPad: "",
            startingAt: 0
        )
        let redemptionKey = try await withCheckedThrowingContinuation { c in
            CreateRedemptionKeyRequest(
                networking: LLCore.networking,
                accessToken: config.producerToken,
                keyName: "key name",
                keyCode: keyCode,
                keyDescription: "key description",
                clientID: config.clientID,
                assignedTo: nil
            ).execute(
                url: config.apiOrigin.appendingPathComponent("redemption-keys/")
            ) {
                c.resume(with: $0)
            }
        }
        
        // Redeem the key using code
        let redeemedKey = try await withCheckedThrowingContinuation { c in
            self.livelike.redeemKeyBy(redemptionKeyID: redemptionKey.id) {
                c.resume(with: $0)
            }
        }
        
        XCTAssertEqual(redeemedKey.status, .redeemed)
        
        // Cleanup
        let _ = try await withCheckedThrowingContinuation { c in
            DeleteRedemptionKeyRequest(
                networking: LLCore.networking,
                accessToken: config.producerToken
            ).execute(
                url: config.apiOrigin.appendingPathComponent("redemption-keys/\(redemptionKey.id)/")
            ) {
                c.resume(with: $0)
            }

        }
    }
    
    func test_getRedemptionKeys() async throws {
        
        @discardableResult
        func testGetRedemptionKeys(
            options: GetRedemptionKeysRequestOptions?
        ) async throws -> [RedemptionKey] {
            return try await withCheckedThrowingContinuation { c in
                livelike.getRedemptionKeys(
                    page: .first,
                    options: options
                ) {
                    AssertResultSuccess($0)
                    c.resume(with: $0)
                }
            }
        }
        
        try await testGetRedemptionKeys(options: nil)
        try await testGetRedemptionKeys(options: .init(status: .active))
        try await testGetRedemptionKeys(options: .init(status: .inactive))
        try await testGetRedemptionKeys(options: .init(status: .redeemed))
        try await testGetRedemptionKeys(options: .init(isAssigned: true))
        try await testGetRedemptionKeys(options: .init(isAssigned: false))
    }
    
    func test_releaseRedemptionKey() async throws {
        
        // Create a redemption key (producer)
        let keyCode = UUID().uuidString.padding(toLength: 35, withPad: "", startingAt: 0)
        let profileID = try await withCheckedThrowingContinuation { c in
            livelike.getCurrentUserProfileID {
                c.resume(with: $0)
            }
        }
        
        let redemptionKeyClient = livelike.redemptionKeyClient
        
        let redemptionKey = try await withCheckedThrowingContinuation { c in
            CreateRedemptionKeyRequest(
                networking: LLCore.networking,
                accessToken: config.producerToken,
                keyName: "key name",
                keyCode: keyCode,
                keyDescription: "key description",
                clientID: config.clientID,
                assignedTo: profileID
            ).execute(
                url: config.apiOrigin.appendingPathComponent("redemption-keys/")
            ) {
                c.resume(with: $0)
            }
        }
        
        XCTAssertEqual(profileID, redemptionKey.assignedTo)
        
        // Release the key
        let releasedKey = try await withCheckedThrowingContinuation { c in
            redemptionKeyClient.unassignRedemptionKey(
                options: .init(redemptionKeyID: redemptionKey.id)
            ) {
                c.resume(with: $0)
            }
        }
        
        XCTAssertEqual(releasedKey.assignedTo, nil)
        
        // Cleanup
        let _ = try await withCheckedThrowingContinuation { c in
            DeleteRedemptionKeyRequest(
                networking: LLCore.networking,
                accessToken: config.producerToken
            ).execute(
                url: config.apiOrigin.appendingPathComponent("redemption-keys/\(redemptionKey.id)/")
            ) {
                c.resume(with: $0)
            }

        }
    }
    
}
