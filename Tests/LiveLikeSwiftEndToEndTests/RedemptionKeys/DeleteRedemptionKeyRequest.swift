//
//  DeleteRedemptionKeyRequest.swift
//  
//
//  Created by Jelzon Monzon on 7/27/23.
//

import Foundation
import LiveLikeCore
import LiveLikeSwift

struct DeleteRedemptionKeyRequest: LLRequest {
    
    private let networking: LLNetworking
    private let accessToken: String
    
    init(
        networking: LLNetworking,
        accessToken: String
    ) {
        self.networking = networking
        self.accessToken = accessToken
    }
    
    func execute(
        url: URL,
        completion: @escaping (Result<Bool, Error>) -> Void
    ) {
        struct Body: Encodable { }
        let resource = Resource<Bool>(
            url: url,
            method: .delete(Body()),
            accessToken: self.accessToken
        )
        networking.task(resource, completion: completion)
    }
    
}
