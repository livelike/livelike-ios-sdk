//
//  QuizWidgetE2E.swift
//  
//
//  Created by Jelzon Monzon on 4/26/23.
//

import Foundation
import XCTest
import LiveLikeSwift

@available(iOS 13.0.0, *)
class QuizWidgetE2E: BaseE2E {
    
    override var accessToken: String? {
        get { return config.producerToken }
        set { super.accessToken = newValue }
    }
    
    /// Validates data
    func testData() async throws {
        let playbackTime: UInt = 100
        let quizModel: QuizWidgetModel = try await withCheckedThrowingContinuation { c in
            livelike.widgetClient.createTextQuizWidget(
                options: CreateTextQuizWidgetOptions(
                    common: CommonCreateWidgetOptions(
                        programID: self.config.programID,
                        timeoutSeconds: 10,
                        playbackTimeMilliseconds: playbackTime
                    ),
                    question: "e2e",
                    choices: [
                        .init(text: "option 1", isCorrect: true),
                        .init(text: "option 2", isCorrect: false)
                    ]
                )
            ) {
                c.resume(with: $0)
            }
        }
        
        XCTAssertEqual(quizModel.programID, config.programID)
        XCTAssertEqual(quizModel.containsImages, false)
        XCTAssertEqual(quizModel.question, "e2e")
        XCTAssertEqual(quizModel.totalAnswerCount, 0)
        XCTAssertEqual(UInt(quizModel.playbackTimeMilliseconds!), playbackTime)
        
        XCTAssertEqual(quizModel.choices[0].isCorrect, true)
        XCTAssertEqual(quizModel.choices[0].text, "option 1")
        XCTAssertEqual(quizModel.choices[0].answerCount, 0)
        XCTAssertEqual(quizModel.choices[0].imageURL, nil)
        
        XCTAssertEqual(quizModel.choices[1].isCorrect, false)
        XCTAssertEqual(quizModel.choices[1].text, "option 2")
        XCTAssertEqual(quizModel.choices[1].answerCount, 0)
        XCTAssertEqual(quizModel.choices[1].imageURL, nil)
        
        try await deleteWidget(model: quizModel)
    }
    
    func testImageData() async throws {
        let playbackTime: UInt = 100
        let imageURL: URL = URL(string: "https://livelike.com")!
        let quizModel: QuizWidgetModel = try await withCheckedThrowingContinuation { c in
            livelike.widgetClient.createImageQuizWidget(
                options: CreateImageQuizWidgetOptions(
                    common: CommonCreateWidgetOptions(
                        programID: self.config.programID,
                        timeoutSeconds: 10,
                        playbackTimeMilliseconds: playbackTime
                    ),
                    question: "e2e",
                    choices: [
                        .init(text: "option 1", imageURL: imageURL, isCorrect: true),
                        .init(text: "option 2", imageURL: imageURL, isCorrect: false)
                    ]
                )
            ) {
                c.resume(with: $0)
            }
        }
        
        XCTAssertEqual(quizModel.programID, config.programID)
        XCTAssertEqual(quizModel.containsImages, true)
        XCTAssertEqual(quizModel.question, "e2e")
        XCTAssertEqual(quizModel.totalAnswerCount, 0)
        XCTAssertEqual(UInt(quizModel.playbackTimeMilliseconds!), playbackTime)
        
        XCTAssertEqual(quizModel.choices[0].isCorrect, true)
        XCTAssertEqual(quizModel.choices[0].text, "option 1")
        XCTAssertEqual(quizModel.choices[0].answerCount, 0)
        XCTAssertEqual(quizModel.choices[0].imageURL, imageURL)
        
        XCTAssertEqual(quizModel.choices[1].isCorrect, false)
        XCTAssertEqual(quizModel.choices[1].text, "option 2")
        XCTAssertEqual(quizModel.choices[1].answerCount, 0)
        XCTAssertEqual(quizModel.choices[1].imageURL, imageURL)
        
        try await deleteWidget(model: quizModel)
    }
    
    /// Tests voting correctly
    func testVoteCorrect() async throws {
        let quizModel = try await getModel(
            options: CreateTextQuizWidgetOptions(
                common: CommonCreateWidgetOptions(
                    programID: self.config.programID,
                    timeoutSeconds: 10
                ),
                question: "e2e",
                choices: [
                    .init(text: "option 1", isCorrect: true),
                    .init(text: "option 2", isCorrect: false),
                    .init(text: "option 3", isCorrect: false),
                    .init(text: "option 4", isCorrect: false)
                ]
            )
        )
        
        let correctChoice = quizModel.choices.first(where: {$0.isCorrect})!
        
        let answer = try await withCheckedThrowingContinuation { continuation in
            quizModel.lockInAnswer(choiceID: correctChoice.id) {
                continuation.resume(with: $0)
            }
        }
        
        let interactionHistory = try await withCheckedThrowingContinuation { continuation in
            quizModel.loadInteractionHistory {
                continuation.resume(with: $0)
            }
        }
        
        XCTAssertEqual(answer.id, interactionHistory.first?.id)
        
        try await deleteWidget(model: quizModel)
    }
    
    /// Test voting incorrectly
    func testVoteIncorrect() async throws {
        
        let quizModel = try await getModel(
            options: CreateTextQuizWidgetOptions(
                common: CommonCreateWidgetOptions(
                    programID: self.config.programID,
                    timeoutSeconds: 10
                ),
                question: "e2e",
                choices: [
                    .init(text: "option 1", isCorrect: true),
                    .init(text: "option 2", isCorrect: false),
                    .init(text: "option 3", isCorrect: false),
                    .init(text: "option 4", isCorrect: false)
                ]
            )
        )
        
        let incorrectChoice = quizModel.choices.first(where: { !$0.isCorrect})!
        
        _ = try await withCheckedThrowingContinuation { continuation in
            quizModel.lockInAnswer(choiceID: incorrectChoice.id) {
                continuation.resume(with: $0)
            }
        }
        
        try await deleteWidget(model: quizModel)
    }
    
    /// Test voting multiple times
    func testVoteMultiple() async throws {
        let quizModel = try await getModel(
            options: CreateTextQuizWidgetOptions(
                common: CommonCreateWidgetOptions(
                    programID: self.config.programID,
                    timeoutSeconds: 10
                ),
                question: "e2e",
                choices: [
                    .init(text: "option 1", isCorrect: true),
                    .init(text: "option 2", isCorrect: false),
                    .init(text: "option 3", isCorrect: false),
                    .init(text: "option 4", isCorrect: false)
                ]
            )
        )
        
        let firstChoice = quizModel.choices.first(where: {$0.isCorrect})!
        let secondChoice = quizModel.choices.first(where: { !$0.isCorrect})!
        
        _ = try await withCheckedThrowingContinuation { continuation in
            quizModel.lockInAnswer(choiceID: firstChoice.id) {
                continuation.resume(with: $0)
            }
        }
        
        do {
            _ = try await withCheckedThrowingContinuation { c in
                // Try to answer again (expected fail)
                quizModel.lockInAnswer(choiceID: secondChoice.id) { result in
                    c.resume(with: result)
                }
            }
            XCTFail("expected to fail")
        } catch {
            XCTAssert(true)
        }
        
        try await deleteWidget(model: quizModel)
    }
    
    /// Tests voting on an expired widget should fail
    func testVoteOnExpiredWidget() async throws {

        let quizModel = try await getModel(
            options: CreateTextQuizWidgetOptions(
                common: CommonCreateWidgetOptions(
                    programID: self.config.programID,
                    timeoutSeconds: 10,
                    interactiveUntil: .distantPast
                ),
                question: "e2e",
                choices: [
                    .init(text: "option 1", isCorrect: true),
                    .init(text: "option 2", isCorrect: false),
                    .init(text: "option 3", isCorrect: false),
                    .init(text: "option 4", isCorrect: false)
                ]
            )
        )
        
        // Seconds until interactiity expiration should be negative
        XCTAssertTrue(quizModel.getSecondsUntilInteractivityExpiration()! < 0)
        
        do {
            let _ = try await withCheckedThrowingContinuation { c in
                // Try to answer again (expected fail)
                quizModel.lockInAnswer(choiceID: quizModel.choices.first!.id) { result in
                    c.resume(with: result)
                }
            }
            XCTFail("expected to fail")
        } catch {
            XCTAssert(true)
        }
        
        try await deleteWidget(model: quizModel)
    }

    private func getModel(options: CreateTextQuizWidgetOptions) async throws -> QuizWidgetModel {
        return try await withCheckedThrowingContinuation { c in
            livelike.widgetClient.createTextQuizWidget(
                options: options
            ) {
                c.resume(with: $0)
            }
        }
    }
}
