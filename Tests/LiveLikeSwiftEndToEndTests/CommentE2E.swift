//
//  CommentE2E.swift
//  
//
//  Created by Jelzon Monzon on 5/9/23.
//

import Foundation
import XCTest
import LiveLikeSwift

@available(iOS 13.0.0, *)
class CommentE2E: XCTestCase {
    
    private var livelike: LiveLike!
    private var config: Config!
    
    class TokenStorage: AccessTokenStorage {
        init(token: String) {
            self.token = token
        }
        
        let token: String
        
        func fetchAccessToken() -> String? {
            return token
        }
        
        func storeAccessToken(accessToken: String) { }
    }
    
    override func setUp() async throws {
        self.config = try Config.load()
        var llConfig = LiveLikeConfig(clientID: self.config.clientID)
        llConfig.accessTokenStorage = TokenStorage(token: config.producerToken)
        llConfig.apiOrigin = config.apiOrigin
        self.livelike = LiveLike(config: llConfig)
    }
    
    func testCommentBoardClient() async throws {
        
        let _ = try await withCheckedThrowingContinuation { c in
            livelike.commentBoards.getCommentBoards(page: .first) {
                AssertResultSuccess($0)
                c.resume(with: $0)
            }
        }
        
        let commentBoard: CommentBoard = try await withCheckedThrowingContinuation { c in
            livelike.commentBoards.createCommentBoard(
                options: CreateCommentBoardRequestOptions(
                    allowComments: true,
                    repliesDepth: 2,
                    title: "my test commentboard",
                    customData: nil
                )
            ) { c.resume(with: $0) }
        }
        
        XCTAssertEqual(commentBoard.repliesDepth, 2)
        XCTAssertEqual(commentBoard.allowComments, true)
        XCTAssertEqual(commentBoard.title, "my test commentboard")
        
        let commentBoardDetails: CommentBoard = try await withCheckedThrowingContinuation { c in
            livelike.commentBoards.getCommentBoardDetails(
                commentBoardID: commentBoard.id
            ) { c.resume(with: $0) }
        }
        
        XCTAssertEqual(commentBoardDetails.repliesDepth, 2)
        XCTAssertEqual(commentBoardDetails.allowComments, true)
        XCTAssertEqual(commentBoardDetails.title, "my test commentboard")
        
        let updatedCommentBoard: CommentBoard = try await withCheckedThrowingContinuation { c in
            livelike.commentBoards.updateCommentBoard(
                commentBoardID: commentBoard.id,
                options: UpdateCommentBoardRequestOptions(
                    title: "title 2",
                    allowComments: false,
                    repliesDepth: 3,
                    customData: "custom data 2"
                )
            ) { c.resume(with: $0) }
        }
        
        XCTAssertEqual(updatedCommentBoard.repliesDepth, 3)
        XCTAssertEqual(updatedCommentBoard.allowComments, false)
        XCTAssertEqual(updatedCommentBoard.title, "title 2")
        
        let myProfileID: String = try await withCheckedThrowingContinuation { c in
            livelike.getCurrentUserProfileID { c.resume(with: $0) }
        }
        
        let commentBoardBan: CommentBoardBanDetails = try await withCheckedThrowingContinuation { c in
            livelike.commentBoards.createCommentBoardBan(
                profileID: myProfileID,
                options: CommentBoardBanRequestOptions(commentBoardID: commentBoard.id, description: "ban description")
            ) { c.resume(with: $0) }
        }
        
        XCTAssertEqual(commentBoardBan.profileID, myProfileID)
        XCTAssertEqual(commentBoardBan.commentBoardID, commentBoard.id)
        XCTAssertEqual(commentBoardBan.description, "ban description")
        
        let _ = try await withCheckedThrowingContinuation { c in
            livelike.commentBoards.getCommentBoardBanDetails(commentBoardBanID: commentBoardBan.id) {
                AssertResultSuccess($0)
                c.resume(with: $0)
            }
        }
        
        let _ = try await withCheckedThrowingContinuation { c in
            livelike.commentBoards.getCommentBoardBans(
                page: .first,
                options: GetCommentBoardBansListRequestOptions(
                    profileID: myProfileID,
                    commentBoardID: commentBoard.id
                )
            ) {
                AssertResultSuccess($0)
                c.resume(with: $0)
            }
        }
        
        let isCommentBoardBanDeleted = try await withCheckedThrowingContinuation { c in
            livelike.commentBoards.deleteCommentBoardBan(
                commentBoardBanID: commentBoardBan.id
            ) { c.resume(with: $0) }
        }
        
        XCTAssertEqual(isCommentBoardBanDeleted, true)
        
        let isDeleted = try await withCheckedThrowingContinuation { c in
            livelike.commentBoards.deleteCommentBoard(
                commentBoardID: commentBoard.id
            ) { c.resume(with: $0) }
        }
        
        XCTAssertEqual(isDeleted, true)
    }
    
    func testCommentClient() async throws {
        let commentBoard: CommentBoard = try await withCheckedThrowingContinuation { c in
            livelike.commentBoards.createCommentBoard(
                options: CreateCommentBoardRequestOptions(
                    title: "my test commentboard",
                    customID: UUID().uuidString,
                    allowComments: true,
                    repliesDepth: 2,
                    customData: nil
                )
            ) { c.resume(with: $0) }
        }
        
        let commentSession: CommentClient = try await withCheckedThrowingContinuation { c in
            livelike.createCommentClient(
                for: commentBoard.id
            ) { c.resume(with: $0) }
        }
        
        let comment: Comment = try await withCheckedThrowingContinuation { c in
            commentSession.addComment(text: "my comment") { c.resume(with: $0) }
        }
        
        XCTAssertEqual(comment.text, "my comment")
        
        let commentReply: Comment = try await withCheckedThrowingContinuation { c in
            commentSession.addCommentReply(
                parentCommentID: comment.id, text: "my comment reply"
            ) { c.resume(with: $0) }
        }
        
        XCTAssertEqual(commentReply.parentCommentID, comment.id)
        
        let edittedComment: Comment = try await withCheckedThrowingContinuation { c in
            commentSession.editComment(
                commentID: comment.id,
                text: "editted comment"
            ) { c.resume(with: $0) }
        }
        
        XCTAssertEqual(edittedComment.text, "editted comment")
        
        let _ = try await withCheckedThrowingContinuation { c in
            commentSession.getComment(commentID: comment.id) {
                AssertResultSuccess($0)
                c.resume(with: $0)
            }
        }
        
        let _ = try await withCheckedThrowingContinuation { c in
            commentSession.getComments(
                page: .first,
                options: nil
            ) {
                AssertResultSuccess($0)
                c.resume(with: $0)
            }
        }
        
        let _ = try await withCheckedThrowingContinuation { c in
            commentSession.getComments(
                page: .first,
                options: GetCommentsRequestOptions(ordering: .newest, topLevel: nil)
            ) {
                AssertResultSuccess($0)
                c.resume(with: $0)
            }
        }
        
        let _ = try await withCheckedThrowingContinuation { c in
            commentSession.getCommentsPage(
                page: .first,
                options: nil
            ) {
                switch $0 {
                case .success(let page):
                    XCTAssertEqual(page.count, 2)
                case .failure:
                    XCTFail()
                }
                AssertResultSuccess($0)
                c.resume(with: $0)
            }
        }
        
        let _ = try await withCheckedThrowingContinuation { c in
            commentSession.getCommentReplies(
                page: .first,
                options: GetCommentRepliesRequestOptions(
                    commentBoardID: commentBoard.id,
                    parentCommentID: comment.id
                )
            ) {
                AssertResultSuccess($0)
                c.resume(with: $0)
            }
        }
        
        let commentReport: CommentReport = try await withCheckedThrowingContinuation { c in
            commentSession.createCommentReport(
                commentID: comment.id,
                options: CreateCommentReportRequestOptions(description: "report description")
            ) { c.resume(with: $0) }
        }
        
        XCTAssertEqual(commentReport.description, "report description")
        XCTAssertEqual(commentReport.commentID, comment.id)
        
        let commentReportDetails: CommentReport = try await withCheckedThrowingContinuation { c in
            commentSession.getCommentReportDetails(commentReportID: commentReport.id) {
                c.resume(with: $0)
            }
        }
        
        XCTAssertEqual(commentReportDetails.id, commentReport.id)
        
        let _: [CommentReport] = try await withCheckedThrowingContinuation { c in
            commentSession.getCommentReports(
                page: .first,
                options: GetCommentReportsListRequestOptions(
                    commentBoardID: commentBoard.id,
                    commentID: comment.id,
                    reportStatus: nil
                )
            ) { c.resume(with: $0) }
        }
        
        let _: CommentReportStatusDetail = try await withCheckedThrowingContinuation { c in
            commentSession.dismissCommentReport(
                commentReportID: commentReport.id
            ) { c.resume(with: $0) }
        }
        
        let _ = try await withCheckedThrowingContinuation { c in
            commentSession.dismissAllCommentReports(commentID: comment.id) {
                c.resume(with: $0)
            }
        }

        let _ = try await withCheckedThrowingContinuation { c in
            commentSession.deleteCommentReport(
                commentReportID: commentReport.id
            ) { c.resume(with: $0) }
        }

        let _ = try await withCheckedThrowingContinuation { c in
            commentSession.deleteComment(commentID: comment.id) {
                c.resume(with: $0)
            }
        }
    }
    
    func test_getCommentReplies_ordering() async throws {
        let commentBoard: CommentBoard = try await withCheckedThrowingContinuation { c in
            livelike.commentBoards.createCommentBoard(
                options: CreateCommentBoardRequestOptions(
                    title: "my test commentboard",
                    customID: UUID().uuidString,
                    allowComments: true,
                    repliesDepth: 2,
                    customData: nil
                )
            ) { c.resume(with: $0) }
        }
        
        let commentSession: CommentClient = try await withCheckedThrowingContinuation { c in
            livelike.createCommentClient(
                for: commentBoard.id
            ) { c.resume(with: $0) }
        }
        
        let comment: Comment = try await withCheckedThrowingContinuation { c in
            commentSession.addComment(text: "my comment") { c.resume(with: $0) }
        }
        
        XCTAssertEqual(comment.text, "my comment")
        
        let commentReply: Comment = try await withCheckedThrowingContinuation { c in
            commentSession.addCommentReply(
                parentCommentID: comment.id, text: "my comment reply"
            ) { c.resume(with: $0) }
        }
        
        XCTAssertEqual(commentReply.parentCommentID, comment.id)
        
        let commentReplyB: Comment = try await withCheckedThrowingContinuation { c in
            commentSession.addCommentReply(
                parentCommentID: comment.id, text: "my comment reply b"
            ) { c.resume(with: $0) }
        }
        
        XCTAssertEqual(commentReplyB.parentCommentID, comment.id)
        
        let commentRepliesNewest: [Comment] = try await withCheckedThrowingContinuation { c in
            commentSession.getCommentReplies(
                page: .first,
                options: GetCommentRepliesRequestOptions(
                    commentBoardID: commentBoard.id,
                    parentCommentID: comment.id,
                    ordering: .newest
                )
            ) { c.resume(with: $0) }
        }
        
        XCTAssertEqual(commentRepliesNewest[0].id, commentReplyB.id)
        
        let commentRepliesDefault: [Comment] = try await withCheckedThrowingContinuation { c in
            commentSession.getCommentReplies(
                page: .first,
                options: GetCommentRepliesRequestOptions(
                    commentBoardID: commentBoard.id,
                    parentCommentID: comment.id
                )
            ) { c.resume(with: $0) }
        }
        
        XCTAssertEqual(commentRepliesDefault[0].id, commentReply.id)
        
        let commentRepliesOldest: [Comment] = try await withCheckedThrowingContinuation { c in
            commentSession.getCommentReplies(
                page: .first,
                options: GetCommentRepliesRequestOptions(
                    commentBoardID: commentBoard.id,
                    parentCommentID: comment.id,
                    ordering: .oldest
                )
            ) { c.resume(with: $0) }
        }
        
        XCTAssertEqual(commentRepliesOldest[0].id, commentReply.id)
        
        let _ = try await withCheckedThrowingContinuation { c in
            commentSession.deleteComment(commentID: comment.id) {
                c.resume(with: $0)
            }
        }
    }
}
