//
//  File.swift
//  
//
//  Created by Jelzon Monzon on 5/15/23.
//

import Foundation
import XCTest
import LiveLikeSwift
import LiveLikeWidgetPublish

@available(iOS 13.0.0, *)
class PollWidgetE2E: BaseE2E {
    
    override var accessToken: String? {
        get { return config.producerToken }
        set { super.accessToken = newValue }
    }
    
    /// Validates widget data
    func test_textPollCreate() async throws {
        let pollModel = try await withCheckedThrowingContinuation { c in
            livelike.widgetClient.createTextPollWidget(
                options: CreateTextPollRequestOptions(
                    common: CommonCreateWidgetOptions(
                        programID: self.config.programID,
                        timeoutSeconds: 10
                    ),
                    question: "my poll question",
                    options: [
                        .init(text: "option a"),
                        .init(text: "option b"),
                        .init(text: "option c"),
                        .init(text: "option d"),
                    ]
                )
            ) { c.resume(with: $0) }
        }
        
        XCTAssertEqual(pollModel.userVotes.count, 0)
        XCTAssertEqual(pollModel.options.count, 4)
        XCTAssertEqual(pollModel.containsImages, false)
        XCTAssertEqual(pollModel.totalVoteCount, 0)
        
        try await deleteWidget(model: pollModel)
    }
    
    func test_imagePollCreate() async throws {
        let url = URL(string: "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")!
        let pollModel = try await withCheckedThrowingContinuation { c in
            livelike.widgetClient.createImagePollWidget(
                options: CreateImagePollRequestOptions(
                    common: CommonCreateWidgetOptions(
                        programID: config.programID,
                        timeoutSeconds: 10
                    ),
                    question: "my poll question",
                    options: [
                        .init(text: "option a", imageURL: url),
                        .init(text: "option b", imageURL: url),
                        .init(text: "option c", imageURL: url),
                        .init(text: "option d", imageURL: url),
                    ]
                )
            ) { c.resume(with: $0) }
        }
        
        XCTAssertEqual(pollModel.userVotes.count, 0)
        XCTAssertEqual(pollModel.options.count, 4)
        XCTAssertEqual(pollModel.containsImages, true)
        XCTAssertEqual(pollModel.totalVoteCount, 0)
        
        try await deleteWidget(model: pollModel)
    }
    
    /// Tests voting on a poll then updating that vote
    func testVoting() async throws {
        let pollModel = try await withCheckedThrowingContinuation { c in
            livelike.widgetClient.createTextPollWidget(
                options: CreateTextPollRequestOptions(
                    common: CommonCreateWidgetOptions(
                        programID: config.programID,
                        timeoutSeconds: 10
                    ),
                    question: "my poll question",
                    options: [
                        .init(text: "option a"),
                        .init(text: "option b"),
                        .init(text: "option c"),
                        .init(text: "option d"),
                    ]
                )
            ) { c.resume(with: $0) }
        }
        
        let firstVote: PollWidgetModel.Vote = try await withCheckedThrowingContinuation { c in
            pollModel.submitVote(optionID: pollModel.options[0].id) {
                c.resume(with: $0)
            }
        }
        
        XCTAssertTrue(firstVote.canBeUpdated)
        XCTAssertEqual(firstVote.optionID, pollModel.options[0].id)
        
        let secondVote: PollWidgetModel.Vote = try await withCheckedThrowingContinuation { c in
            pollModel.submitVote(optionID: pollModel.options[1].id) {
                c.resume(with: $0)
            }
        }
        
        XCTAssertTrue(secondVote.canBeUpdated)
        XCTAssertEqual(secondVote.optionID, pollModel.options[1].id)
        
        XCTAssertEqual(pollModel.userVotes.count, 2)
        
        try await deleteWidget(model: pollModel)
    }
    
    func test_createAndSchedulePoll() async throws {
        /// STEP 1 - Tests programDateTime param
    
        try await testCreateAndSchedulePoll(
            programDateTime: nil,
            assertions: { publishInfo in
                XCTAssertEqual(publishInfo.programDateTime, nil)
            }
        )

        try await testCreateAndSchedulePoll(
            programDateTime: .distantPast,
            assertions: { publishInfo in
                XCTAssertEqual(publishInfo.programDateTime, .distantPast)
            }
        )
        
        try await testCreateAndSchedulePoll(
            programDateTime: .distantFuture,
            assertions: { publishInfo in
                XCTAssertEqual(publishInfo.programDateTime, .distantFuture)
            }
        )
        
        /// STEP 3 - Tests publishDelay param
        
        // publishDelay is set to "00:00:00" when nil
        try await testCreateAndSchedulePoll(
            publishDelay: nil,
            assertions: { publishInfo in
                XCTAssertEqual(publishInfo.publishDelayISO8601, "00:00:00")
            }
        )
        
        try await testCreateAndSchedulePoll(
            publishDelay: 0,
            assertions: { publishInfo in
                XCTAssertEqual(publishInfo.publishDelayISO8601, "00:00:00")
            }
        )
        
        try await testCreateAndSchedulePoll(
            publishDelay: 9000,
            assertions: { publishInfo in
                XCTAssertEqual(publishInfo.publishDelayISO8601, "02:30:00")
            }
        )
        
        func testCreateAndSchedulePoll(
            programDateTime: Date? = nil,
            publishDelay: UInt? = nil,
            assertions: ((PublishedWidgetInfo) -> Void)? = nil
        ) async throws {
            /// STEP 1 - Create Poll
            let pollModel = try await withCheckedThrowingContinuation { c in
                livelike.widgetClient.createTextPollWidget(
                    options: CreateTextPollRequestOptions(
                        common: CommonCreateWidgetOptions(
                            programID: config.programID,
                            timeoutSeconds: 10
                        ),
                        question: "my poll question",
                        options: [
                            .init(text: "option a"),
                            .init(text: "option b"),
                            .init(text: "option c"),
                            .init(text: "option d"),
                        ]
                    )
                ) { c.resume(with: $0) }
            }
            
            /// STEP 2 - Publish Poll
            let publishedPoll = try await withCheckedThrowingContinuation { c in
                livelike.widgetClient.publishWidget(
                    options: PublishWidgetRequestOptions(
                        kind: pollModel.kind,
                        id: pollModel.id,
                        publishDelaySeconds: publishDelay,
                        programDateTime: programDateTime
                    )
                ) { c.resume(with: $0) }
            }
            
            assertions?(publishedPoll)
            
            /// STEP 3 - Cleanup
            _ = try await withCheckedThrowingContinuation { c in
                livelike.widgetClient.deleteWidget(options: DeleteWidgetRequestOptions(
                    id: pollModel.id,
                    kind: pollModel.kind
                )
                ) {
                    c.resume(with: $0)
                }
            }
        }
        
    }
}
