//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//
	

import Foundation
import XCTest
import LiveLikeSwift
import LiveLikeRBAC
import LiveLikeCore

struct Permission {
    let hello: String
}

@available(iOS 13.0.0, *)
class RBACE2E: BaseE2E {
    
    override var accessToken: String? {
        get { return config.producerToken }
        set { super.accessToken = newValue }
    }
    
    var rbacClient: RBACClient!
    
    override func setUp() async throws {
        try await super.setUp()
        rbacClient = try await withCheckedThrowingContinuation { c in
            LiveLikeRBACClient.make(
                config: RBACClientConfig(
                    clientID: config.clientID,
                    accessToken: config.producerToken
                )
            ) {
                c.resume(with: $0)
            }
        }
    }
    
    func test_listRoles() async throws {
        _ = try await withCheckedThrowingContinuation { c in
            rbacClient.getRoles(
                page: .first,
                options: ListRolesOptions()
            ) {
                AssertResultSuccess($0)
                c.resume(with: $0)
            }
        }
    }
    
    func test_listResources() async throws {
        _ = try await withCheckedThrowingContinuation { c in
            rbacClient.getResources(
                page: .first,
                options: GetResourceOptions()
            ) {
                AssertResultSuccess($0)
                c.resume(with: $0)
            }
        }
    }
    
    func test_listAssignedRoles() async throws {
        let profileID = try await withCheckedThrowingContinuation { c in
            livelike.getCurrentUserProfileID {
                c.resume(with: $0)
            }
        }
        
        _ = try await withCheckedThrowingContinuation { c in
            rbacClient.getAssignedRoles(
                page: .first,
                options: GetAssignedRolesOptions(
                    profileID: profileID
                )
            ) {
                AssertResultSuccess($0)
                c.resume(with: $0)
            }
        }
    }
    
    func test_listAssignedRolesForScope() async throws {
        let profileID = try await withCheckedThrowingContinuation { c in
            livelike.getCurrentUserProfileID {
                c.resume(with: $0)
            }
        }
        
        let resource = try await withCheckedThrowingContinuation { c in
            rbacClient.getResources(
                page: .first,
                options: GetResourceOptions()
            ) {
                c.resume(with: $0.map { $0.items.first! })
            }
        }
        
        let scope = try await withCheckedThrowingContinuation { c in
            rbacClient.getScopes(
                page: .first,
                options: GetScopesOptions(resourceID: resource.id)
            ) {
                c.resume(with: $0.map { $0.items.first! })
            }
        }
        
        _ = try await withCheckedThrowingContinuation { c in
            rbacClient.getAssignedRoles(
                page: .first,
                options: GetAssignedRolesOptions(
                    profileID: profileID, 
                    scopeKey: scope.resourceKey
                )
            ) {
                AssertResultSuccess($0)
                c.resume(with: $0)
            }
        }
    }
    
    func test_listPermissions() async throws {
        let profileID = try await withCheckedThrowingContinuation { c in
            livelike.getCurrentUserProfileID {
                c.resume(with: $0)
            }
        }
        
        _ = try await withCheckedThrowingContinuation { c in
            rbacClient.getPermissions(
                page: .first,
                options: GetPermissionsOptions()
            ) {
                AssertResultSuccess($0)
                c.resume(with: $0)
            }
        }
        
        let x: PaginatedResult<LiveLikeRBAC.RBACPermission> = try await withCheckedThrowingContinuation { c in
            rbacClient.getPermissions(
                page: .first,
                options: GetPermissionsOptions(profileID: profileID)
            ) {
                AssertResultSuccess($0)
                c.resume(with: $0)
            }
        }
    }
    
    func test_createRoleAssignment() async throws {
        let profileID = try await withCheckedThrowingContinuation { c in
            livelike.getCurrentUserProfileID {
                c.resume(with: $0)
            }
        }
        
        let assignedRole: RBACRoleAssignment? = try await withCheckedThrowingContinuation { c in
            rbacClient.getAssignedRoles(
                page: .first,
                options: GetAssignedRolesOptions(
                    profileID: profileID
                )
            ) {
                AssertResultSuccess($0)
                c.resume(with: $0.map { $0.items.first })
            }
        }
        
        guard let assignedRole = assignedRole else {
            XCTFail("Expected at least 1 role")
            return
        }
        
        _ = try await withCheckedThrowingContinuation { c in
            rbacClient.deleteRoleAssignment(
                options: DeleteRoleAssignmentOptions(
                    roleAssignmentID: assignedRole.id
                )
            ) {
                c.resume(with: $0)
            }
        }
        
        // Reassign the deleted role assignment
        _ = try await withCheckedThrowingContinuation { c in
            rbacClient.createRoleAssignment(
                options: CreateRoleAssignmentOptions(
                    roleKey: assignedRole.role.key,
                    profileID: assignedRole.profile.id,
                    resourceKind: assignedRole.scope.resource.kind,
                    resourceKey: assignedRole.scope.resourceKey
                )
            ) {
                AssertResultSuccess($0)
                c.resume(with: $0)
            }
        }
            
    }
}
