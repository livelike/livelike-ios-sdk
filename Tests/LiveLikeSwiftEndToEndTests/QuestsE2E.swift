//
//  QuestsE2E.swift
//
//
//  Created by Mike Moloksher on 3/29/23.
//

import XCTest
import Foundation
import LiveLikeCore
import LiveLikeSwift

struct EmptyBody: Encodable {}

@available(iOS 13.0.0, *)
class QuestsE2E: BaseE2E {
    
    var questsURL: URL!
    var questTasksURL: URL!
    var userQuestsURL: URL!
    
    override func setUp() {
        self.questsURL = config.apiOrigin.appendingPathComponent("quests", isDirectory: true)
        self.questTasksURL = config.apiOrigin.appendingPathComponent("quest-tasks", isDirectory: true)
        self.userQuestsURL = config.apiOrigin.appendingPathComponent("user-quests", isDirectory: true)
    }
    
    /*
     * This test goes through a basic flow of Creatig a Quest, assigning QuestTasks,
     * starting a Quest, updating the UserQuest status, deleting UserQuest and Quest
     */
    func test_quest_flow() async throws {
        
        // MARK: - STEP 1 - Testing creation of a Quest
        /*
         * STEP 1A - Create a Quest on the backend
         */
        let newQuest: Quest = try await withCheckedThrowingContinuation { c in
            struct Payload: Encodable {
                var clientId: String
                var name: String
            }
            
            let resource = Resource<Quest>(
                url: questsURL,
                method: .post(Payload(clientId: config.clientID, name: "E2EQuest\(UUID().uuidString)")),
                accessToken: config.producerToken
            )
            
            LLCore.networking.task(resource) { result in
                switch result {
                case .success(let quest):
                    log.dev("Quest Created: \(quest.id)")
                    c.resume(with: .success(quest))
                case .failure(let error):
                    XCTFail("Failed to create a Quest - \(error.localizedDescription)")
                    c.resume(with: .failure(error))
                }
            }
        }
        
        /*
         * STEP 1B - Verify creation of a Quest on the backend
         */
        let _ = try await withCheckedThrowingContinuation { c in
            
            livelike.quests.getQuests(
                page: .first,
                options: GetQuestsRequestOptions(questIDs: [newQuest.id])
            ) { result in

                switch result {
                case .success(let quests):
                    if quests.count == 0 {
                        XCTFail("Failed to find newly created quest on backend")
                    }
                    c.resume()
                case .failure(let error):
                    XCTFail("Failed to verify that the newly created Quest exists on backend - \(error.localizedDescription)")
                    c.resume(throwing: error)
                }
            }
        }
        
        // MARK: - STEP 2 - Create Quests Tasks and Verify their existance
        /*
         * STEP 2A - Create Quest Tasks
         */
        
        struct Payload: Encodable {
            var questId: String
            var name: String
        }
        
        // Creating Quest Task 1
        let _: QuestTask = try await withCheckedThrowingContinuation { c in
            
            let resource = Resource<QuestTask>(
                url: questTasksURL,
                method: .post(Payload(questId: newQuest.id, name: "e2eQuestTask1")),
                accessToken: config.producerToken
            )
            
            LLCore.networking.task(resource) { result in
                switch result {
                case .success(let questTask):
                    log.dev("Quest Task 1 Created: \(questTask.id)")
                    c.resume(with: .success(questTask))
                case .failure(let error):
                    XCTFail(error.localizedDescription)
                    c.resume(with: .failure(error))
                }
                
            }
        }
        
        // Creating Quest Task 2
        let _: QuestTask = try await withCheckedThrowingContinuation { c in
            
            let resource = Resource<QuestTask>(
                url: questTasksURL,
                method: .post(Payload(questId: newQuest.id, name: "e2eQuestTask2")),
                accessToken: config.producerToken
            )
            
            LLCore.networking.task(resource) { result in
                switch result {
                case .success(let questTask):
                    log.dev("Quest Task 2 Created: \(questTask.id)")
                    c.resume(with: .success(questTask))
                case .failure(let error):
                    XCTFail(error.localizedDescription)
                    c.resume(with: .failure(error))
                }
                
            }
        }
        
        // MARK: - STEP 3 - Test starting a Quest (creation of UserQuest)
        /*
         * STEP 3A - Start a Quest (Create `UserQuest`)
         */
        let userQuest: UserQuest = try await withCheckedThrowingContinuation { c in
            
            livelike.quests.startUserQuest(
                questID: newQuest.id
            ) { result in
                switch result {
                case .success(let userQuest):
                    log.dev("UserQuest Created: \(userQuest.id)")
                    c.resume(with: .success(userQuest))
                case .failure(let error):
                    XCTFail(error.localizedDescription)
                    c.resume(with: .failure(error))
                }
            }
        }
        
        /*
         * STEP 3B - Verify that a `UserQuest` has been created and assigned to current profile
         */
        let _ = try await withCheckedThrowingContinuation { c in
            
            livelike.quests.getUserQuests(
                page: .first,
                options: GetUserQuestsRequestOptions(userQuestIDs: [userQuest.id])
            ) { result in
                switch result {
                case .success(let userQuests):
                    if let userQuest = userQuests.first {
                        log.dev("UserQuest verified on backened: \(userQuest.id)")
                    } else {
                        XCTFail("UserQuest was not found on backend, verification failed")
                    }
                    c.resume()
                case .failure(let error):
                    XCTFail(error.localizedDescription)
                    c.resume(throwing: error)
                }
            }
        }
        
        guard let firstUserQuestTask = userQuest.userQuestTasks.first else {
            XCTFail("No UserQuestTask found in the UserQuest")
            return
        }
        
        // MARK: - STEP 4 - Test UserQuestTask.status change
        /*
         * STEP 4A - Change state of UsetQuestTak to `.completed`
         */
        let _ = try await withCheckedThrowingContinuation { c in
            
            livelike.quests.updateUserQuestTasks(
                userQuestID: userQuest.id,
                userQuestTaskIDs: [firstUserQuestTask.id],
                status: .completed
            ) { result in
                switch result {
                case .success(_):
                    log.dev("UserQuestID: \(userQuest.id) UserTask ID:\(firstUserQuestTask.id) has been set to `completed` status")
                    c.resume()
                case .failure(let error):
                    XCTFail(error.localizedDescription)
                    c.resume(throwing: error)
                }
            }
        }
        
        /*
         * STEP 4B - Verify that `UserQuestTask` status has been changed to `.completed`
         */
        let _ = try await withCheckedThrowingContinuation { c in
            
            livelike.quests.getUserQuests(
                page: .first,
                options: GetUserQuestsRequestOptions(userQuestIDs: [userQuest.id])
            ) { result in
                
                switch result {
                case .success(let userQuests):
                    if let userQuestTask = userQuests.first?.userQuestTasks.first(where: { $0.id == firstUserQuestTask.id }) {
                        if userQuestTask.status == .incomplete {
                            XCTFail("UserQuestTask status has been changed to `.completed` but could not be verified on backend")
                        }
                        
                    } else {
                        XCTFail("Cannot find UserQuestTask to verify status")
                    }
                    c.resume()
                case .failure(let error):
                    XCTFail(error.localizedDescription)
                    c.resume(throwing: error)
                }
            }
        }
        
        // MARK: - STEP 5 - Testing deletion of a UserQuest
        /*
         * STEP 5A - Delete a UserQuest
         */
        let _ = try await withCheckedThrowingContinuation { c in
            
            let resource = Resource<Bool>(
                url: userQuestsURL.appendingPathComponent(userQuest.id, isDirectory: true),
                method: .delete(EmptyBody()),
                accessToken: config.producerToken
            )
            
            LLCore.networking.task(resource) { result in
                switch result {
                case .success(_):
                    log.dev("UserQuest Deleted: \(userQuest.id)")
                    c.resume()
                case .failure(let error):
                    XCTFail("Failed to DELETE a UserQuest - \(error.localizedDescription)")
                    c.resume(throwing: error)
                }
            }
        }
        
            
        /*
         * STEP 5B - Verify deletion of a UserQuest from backend
         */
        let _ = try await withCheckedThrowingContinuation { c in
            livelike.quests.getUserQuests(
                page: .first,
                options: GetUserQuestsRequestOptions(userQuestIDs: [userQuest.id])
            ) { result in
                
                switch result {
                case .success(let userQuests):
                    if userQuests.count > 0 {
                        XCTFail("UserQuest has not been deleted on the backend")
                    }
                    c.resume()
                case .failure(let error):
                    XCTFail("Failed to DELETE a UserQuest - \(error.localizedDescription)")
                    c.resume(throwing: error)
                }
            }
        }
            
            
        // MARK: - STEP 6 - Test deleting Quest
        /*
         * STEP 6A - Delete a Quest on the backend
         */
        let _ = try await withCheckedThrowingContinuation { c in
        
            let resource = Resource<Bool>(
                url: questsURL.appendingPathComponent(newQuest.id, isDirectory: true),
                method: .delete(EmptyBody()),
                accessToken: config.producerToken
            )
            
            LLCore.networking.task(resource) { result in
                
                switch result {
                case .success(_):
                    log.dev("Quest Deleted: \(newQuest.id)")
                    c.resume()
                case .failure(let error):
                    XCTFail("Failed to DELETE a Quest - \(error.localizedDescription)")
                    c.resume(throwing: error)
                }
            }
        }
    }
}
