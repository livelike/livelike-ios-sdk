//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//
	

import Foundation
import LiveLikeSwift
import XCTest

@available(iOS 13.0.0, *)
class NumberPredictionWidgetE2E: BaseE2E {
    
    override var accessToken: String? {
        get { return config.producerToken }
        set { super.accessToken = newValue }
    }
    
    
    func test_number_prediction_create_update() async throws {
        let imageURL = URL(string: "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")!
        let numberPrediction = try await withCheckedThrowingContinuation { c in
            self.livelike.widgetClient.createNumberPredictionWidget(
                options: CreateNumberPredictionWidgetOptions(
                    common: CommonCreateWidgetOptions(programID: config.programID),
                    question: "question",
                    options: [
                        .init(text: "text a", imageURL: imageURL),
                        .init(text: "text b", imageURL: imageURL)
                    ]
                )
            ) {
                c.resume(with: $0)
            }
        }
        
        XCTAssertEqual(numberPrediction.question, "question")
        
        let updateOption = try await withCheckedThrowingContinuation { c in
            self.livelike.widgetClient.updateNumberPredictionFollowUpOption(
                options: UpdateNumberPredictionFollowUpOption(
                    predictionFollowUpID: numberPrediction.followUpWidgetModels.first!.id,
                    optionID: numberPrediction.options.first!.id,
                    correctNumber: 10,
                    text: "new text"
                )
            ) {
                c.resume(with: $0)
            }
        }
        
        XCTAssertEqual(updateOption.correctNumber, 10)
        XCTAssertEqual(updateOption.text, "new text")
    }
}
