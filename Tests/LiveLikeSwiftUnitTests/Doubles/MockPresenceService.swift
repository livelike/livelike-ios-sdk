//
//  MockPresenceService.swift
//  
//
//  Created by Jelzon Monzon on 3/7/23.
//

import Foundation
@testable import LiveLikeSwift

class MockPresenceService: PresenceService {
    
    var subscribeCompletion: (([String]) -> Void)?
    var unsubscribeCompletion: (([String]) -> Void)?
    var joinChannelsCompletion: (([String]) -> Void)?
    var leaveChannelsCompletion: (([String]) -> Void)?
    var updateAttributesCompletion: (([String], PresenceClientAttributes) -> Void)?
    
    func setDelegate(_ delegate: LiveLikeSwift.PresenceServiceDelegate?) {
        
    }
    
    func subscribe(to channels: [String]) {
        self.subscribeCompletion?(channels)
    }
    
    func unsubscribe(from channels: [String]) {
        self.unsubscribeCompletion?(channels)
    }
    
    func joinChannels(_ channels: [String]) {
        self.joinChannelsCompletion?(channels)
    }
    
    func leaveChannels(_ channels: [String]) {
        self.leaveChannelsCompletion?(channels)
    }
    
    func setAttributes(for channels: [String], with attributes: LiveLikeSwift.PresenceClientAttributes, completion: @escaping (Result<LiveLikeSwift.PresenceClientAttributes, Error>) -> Void) {
        self.updateAttributesCompletion?(channels, attributes)
    }
    
    func getAttributes(for profileID: String, on channels: [String], completion: @escaping (Result<[String: LiveLikeSwift.PresenceClientAttributes], Error>) -> Void) {
        
    }
    
    func whereNow(for profileID: String, completion: @escaping (Result<[String], Error>) -> Void) {
        
    }
    
    func hereNow(
        in channels: [String],
        completion: @escaping (Result<[String: [String]], Error>) -> Void
    ) {
        
    }
}
