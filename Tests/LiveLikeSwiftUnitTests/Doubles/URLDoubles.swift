//
//  URLDoubles.swift
//  
//
//  Created by Jelzon Monzon on 1/30/23.
//

import Foundation

extension URL {
    static let fakeURL = URL(string: "https://example.com/")!
}
