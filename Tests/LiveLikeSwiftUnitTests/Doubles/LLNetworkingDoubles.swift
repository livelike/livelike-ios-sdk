//
//  LLNetworkingDoubles.swift
//  
//
//  Created by Jelzon Monzon on 1/30/23.
//

import Foundation
@testable import LiveLikeCore

class MockNetworking: LLNetworking {
    func task(request: URLRequest, completion: @escaping (Result<Data, Error>) -> Void) {
        
    }
    
    var userAgent: String = ""
    
    var validateResourceURLCompletion: ((URL) -> Void)?
    
    func task<A>(
        _ resource: Resource<A>,
        completion: @escaping (Result<A, Error>) -> Void
    ) -> URLSessionDataTask {
        validateResourceURLCompletion?(resource.urlRequest.url!)
        return URLSessionDataTask()
    }
}

/// A LLNetworking double that will always return the specified result
class StubNetworking<B: Decodable>: LLNetworking {
    func task(request: URLRequest, completion: @escaping (Result<Data, Error>) -> Void) { }
    
    var userAgent: String = ""

    var result: Result<B, Error>
    
    init(result: Result<B, Error>) {
        self.result = result
    }
    
    func task<A>(
        _ resource: Resource<A>,
        completion: @escaping (Result<A, Error>) -> Void
    ) -> URLSessionDataTask {
        
        completion(self.result.map { $0 as! A })
        return URLSessionDataTask()
    }
}
