//
//  InternalPresenceClientTests.swift
//  
//
//  Created by Jelzon Monzon on 3/7/23.
//

import Foundation
import XCTest
@testable import LiveLikeSwift
import LiveLikeCore

class InternalPresenceClientTests: XCTestCase {
    
    func test_subscribe_appendsClientIDToChannel() {
        let e = expectation(description: "")
        let clientID = "ex-client-id-1"
        let presenceService = MockPresenceService()
        presenceService.subscribeCompletion = { channels in
            XCTAssertEqual(channels[0], "\(clientID)_channelA")
            XCTAssertEqual(channels[1], "\(clientID)_channelB")
            e.fulfill()
        }
        let sut = InternalPresenceClient(
            presenceService: Promise(value: presenceService),
            clientID: clientID
        )
        sut.subscribe(to: [
            "channelA", "channelB"
        ])
        waitForExpectations(timeout: 1.0)
    }
    
    func test_unsubscribe_appendsClientIDToChannel() {
        let e = expectation(description: "")
        let clientID = "ex-client-id-1"
        let presenceService = MockPresenceService()
        presenceService.unsubscribeCompletion = { channels in
            XCTAssertEqual(channels[0], "\(clientID)_channelA")
            XCTAssertEqual(channels[1], "\(clientID)_channelB")
            e.fulfill()
        }
        let sut = InternalPresenceClient(
            presenceService: Promise(value: presenceService),
            clientID: clientID
        )
        sut.unsubscribe(from: [
            "channelA", "channelB"
        ])
        waitForExpectations(timeout: 1.0)
    }
    
    func test_joinChannels_appendsClientIDToChannel() {
        let e = expectation(description: "")
        let clientID = "ex-client-id-1"
        let presenceService = MockPresenceService()
        presenceService.joinChannelsCompletion = { channels in
            XCTAssertEqual(channels[0], "\(clientID)_channelA")
            XCTAssertEqual(channels[1], "\(clientID)_channelB")
            e.fulfill()
        }
        let sut = InternalPresenceClient(
            presenceService: Promise(value: presenceService),
            clientID: clientID
        )
        sut.joinChannels([
            "channelA", "channelB"
        ])
        waitForExpectations(timeout: 1.0)
    }
    
    func test_leaveChannels_appendsClientIDToChannel() {
        let e = expectation(description: "")
        let clientID = "ex-client-id-1"
        let presenceService = MockPresenceService()
        presenceService.leaveChannelsCompletion = { channels in
            XCTAssertEqual(channels[0], "\(clientID)_channelA")
            XCTAssertEqual(channels[1], "\(clientID)_channelB")
            e.fulfill()
        }
        let sut = InternalPresenceClient(
            presenceService: Promise(value: presenceService),
            clientID: clientID
        )
        sut.leaveChannels([
            "channelA", "channelB"
        ])
        waitForExpectations(timeout: 1.0)
    }
    
    func test_updateAttributes_appendsClientIDToChannel() {
        let e = expectation(description: "")
        let clientID = "ex-client-id-1"
        let presenceService = MockPresenceService()
        presenceService.updateAttributesCompletion = { channels, _ in
            XCTAssertEqual(channels[0], "\(clientID)_channelA")
            XCTAssertEqual(channels[1], "\(clientID)_channelB")
            e.fulfill()
        }
        let sut = InternalPresenceClient(
            presenceService: Promise(value: presenceService),
            clientID: clientID
        )
        sut.setAttributes(
            for: [
                "channelA", "channelB"
            ],
            with: PresenceClientAttributes()
        ) { _ in }
        waitForExpectations(timeout: 1.0)
    }
}
