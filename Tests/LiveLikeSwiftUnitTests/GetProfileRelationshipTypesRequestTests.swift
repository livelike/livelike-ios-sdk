//
//  GetProfileRelationshipTypesRequestTests.swift
//  
//
//  Created by Mike Moloksher on 2/17/23.
//

import Foundation
import XCTest
@testable import LiveLikeCore
@testable import LiveLikeSwift

class GetProfileRelationshipTypesRequestTests: XCTestCase {

    func test_url_nilOptionsContainsClientIDParam() {
        let networking = MockNetworking()

        let request = GetProfileRelationshipTypesRequest(
            clientID: "ex-client-id",
            options: nil,
            accessToken: "ex-access-token",
            networking: networking
        )

        AssertRequestURLContainsQueryItems(
            request: request,
            baseURL: URL(string: "https://example.com")!,
            networking: networking,
            expectedQueryItems: [
                URLQueryItem(name: "client_id", value: "ex-client-id"),
            ]
        )
    }
}
