//
//  GetApplicationRewardItemsRequestTests.swift
//  
//
//  Created by Jelzon Monzon on 1/30/23.
//

import Foundation
@testable import LiveLikeSwift
import LiveLikeCore
import XCTest

class GetApplicationRewardItemsRequestTests: XCTestCase {
    
    func test_url_containsAttributesQueryParam() {
        let networking = MockNetworking()
        let request = GetApplicationRewardItemsRequest(
            options: GetApplicationRewardItemsRequestOptions(
                attributes: [
                    .init(key: "key1", value: "val1"),
                    .init(key: "key2", value: "val2")
                ]
            ),
            clientID: "ex-client-id",
            accessToken: "ex-access-token",
            networking: networking
        )
        
        AssertRequestURLContainsQueryItems(
            request: request,
            baseURL: URL(string: "https://example.com")!,
            networking: networking,
            expectedQueryItems: [
                URLQueryItem(name: "client_id", value: "ex-client-id"),
                URLQueryItem(name: "attributes", value: "key1:val1"),
                URLQueryItem(name: "attributes", value: "key2:val2")
            ]
        )
    }
    
    func test_url_defaultOptionsContainsClientIDParam() {
        let networking = MockNetworking()
        let request = GetApplicationRewardItemsRequest(
            options: GetApplicationRewardItemsRequestOptions(
            ),
            clientID: "ex-client-id",
            accessToken: "ex-access-token",
            networking: networking
        )
        
        AssertRequestURLContainsQueryItems(
            request: request,
            baseURL: URL(string: "https://example.com")!,
            networking: networking,
            expectedQueryItems: [
                URLQueryItem(name: "client_id", value: "ex-client-id"),
            ]
        )
    }
    
    func test_execute_completesWithSuccessOnNetworkingSuccess() {
        let e = expectation(description: "")
        let baseURL = URL(string: "https://example.com")!
        let accessToken = "ex-access-token"
        let networking = StubNetworking<PaginatedResult<RewardItem>>(
            result: .success(.init(
                previous: nil,
                count: 0,
                next: nil,
                items: [
                    RewardItem(
                        id: "id-1",
                        name: "name-1",
                        images: [],
                        attributes: [],
                        url: .fakeURL,
                        clientID: "client-id-1"
                    )
                ]
            ))
        )
        let sut = GetApplicationRewardItemsRequest(
            options: nil,
            clientID: "ex-client-id",
            accessToken: accessToken,
            networking: networking
        )
        sut.execute(url: baseURL) { result in
            switch result {
            case .failure:
                XCTFail()
            case .success(let page):
                XCTAssertTrue(page.items.contains(where: { $0.id == "id-1" }))
                e.fulfill()
            }
        }
        waitForExpectations(timeout: 1.0)
    }
    
    func test_execute_completesWithFailureOnNetworkingError() {
        let e = expectation(description: "")
        let baseURL = URL(string: "https://example.com")!
        let accessToken = "ex-access-token"
        let networking = StubNetworking<AnyDecodable>(
            result: .failure(NetworkClientError.notFound404)
        )
        let sut = GetApplicationRewardItemsRequest(
            options: nil,
            clientID: "ex-client-id",
            accessToken: accessToken,
            networking: networking
        )
        sut.execute(url: baseURL) { result in
            switch result {
            case .success:
                XCTFail()
            case .failure:
                e.fulfill()
            }
        }
        waitForExpectations(timeout: 1.0)
    }
    
}
