//
//  AssertRequestURLContainsQueryItems.swift
//  
//
//  Created by Jelzon Monzon on 1/31/23.
//

import Foundation
import XCTest
@testable import LiveLikeSwift
import LiveLikeCore

extension XCTestCase {
    func AssertRequestURLContainsQueryItems(
        request: some LLRequest,
        baseURL: URL = URL(string: "https://example.com")!,
        networking: MockNetworking,
        expectedQueryItems: Set<URLQueryItem>
    ) {
        let e = expectation(description: "")
        networking.validateResourceURLCompletion = { url in
            print(url)
            if
                let queryItems = URLComponents(
                    url: url,
                    resolvingAgainstBaseURL: false
                )?.queryItems
            {
                let queryItemsSet = Set(queryItems)
                XCTAssertEqual(expectedQueryItems, queryItemsSet)
            }


            e.fulfill()
        }

        let _ = request.execute(url: baseURL)
        waitForExpectations(timeout: 1)
    }
}
