//
//  GetInvokedRewardActionsOptionsTests.swift
//  
//
//  Created by Jelzon Monzon on 1/27/23.
//

import Foundation
import XCTest
@testable import LiveLikeSwift

class GetInvokedRewardActionsOptionsTests: XCTestCase {

    func test_init_defaults() {
        let sut = GetInvokedRewardActionsOptions()
        XCTAssertNil(sut.programID)
        XCTAssertNil(sut.rewardActionKeys)
    }

    func test_init_allParameters() {
        let programID = "ex-program-id"
        let rewardActionKeys = [
            "ex-reward-action-key-1",
            "ex-reward-action-key-2",
        ]
        let sut = GetInvokedRewardActionsOptions(
            programID: programID,
            rewardActionKeys: rewardActionKeys
        )
        XCTAssertEqual(sut.programID, programID)
        XCTAssertEqual(sut.rewardActionKeys, rewardActionKeys)
    }

}
