//
//  GetWidgetsRequestTests.swift
//  
//
//  Created by Jelzon Monzon on 6/14/23.
//

import Foundation
import XCTest
@testable import LiveLikeSwift

class GetWidgetsRequestTests: XCTestCase {
    
    var networking: MockNetworking!
    
    override func setUp() {
        super.setUp()
        
        networking = MockNetworking()
    }
    
    func test_options_widgetStatus() {
        testWidgetStatus(status: .pending)
        testWidgetStatus(status: .published)
        testWidgetStatus(status: .scheduled)
        
        func testWidgetStatus(status: WidgetStatus) {
            let sut = GetWidgetsRequest(
                options: GetWidgetModelsRequestOptions(
                    widgetStatus: status
                ),
                networking: networking,
                accessToken: "access"
            )
            
            AssertRequestURLContainsQueryItems(
                request: sut,
                networking: networking,
                expectedQueryItems: [
                    URLQueryItem(
                        name: "status",
                        value: status.rawValue
                    )
                ]
            )
        }
    }
    
    func test_options_widgetKind() {
        let sut = GetWidgetsRequest(
            options: GetWidgetModelsRequestOptions(
                widgetKind: [.textQuiz, .textPoll]
            ),
            networking: networking,
            accessToken: "access"
        )
        
        AssertRequestURLContainsQueryItems(
            request: sut,
            networking: networking,
            expectedQueryItems: [
                URLQueryItem(name: "kind", value: "text-quiz"),
                URLQueryItem(name: "kind", value: "text-poll")
            ]
        )
    }
    
    func test_options_widgetOrderingRecent() {
        let sut = GetWidgetsRequest(
            options: GetWidgetModelsRequestOptions(
                widgetOrdering: .recent
            ),
            networking: networking,
            accessToken: "access"
        )
        
        AssertRequestURLContainsQueryItems(
            request: sut,
            networking: networking,
            expectedQueryItems: [
                URLQueryItem(name: "ordering", value: "recent")
            ]
        )
    }
    
    func test_options_widgetOrderingOldest() {
        let sut = GetWidgetsRequest(
            options: GetWidgetModelsRequestOptions(
                widgetOrdering: .oldest
            ),
            networking: networking,
            accessToken: "access"
        )
        AssertRequestURLContainsQueryItems(
            request: sut,
            networking: networking,
            expectedQueryItems: [] // .oldest should not append a query item
        )
    }
    
    func test_options_interactive() {
        testInteractive(interactive: true)
        testInteractive(interactive: false)
        
        func testInteractive(interactive: Bool) {
            let sut = GetWidgetsRequest(
                options: GetWidgetModelsRequestOptions(
                    interactive: interactive
                ),
                networking: networking,
                accessToken: "access"
            )
            
            AssertRequestURLContainsQueryItems(
                request: sut,
                networking: networking,
                expectedQueryItems: [
                    URLQueryItem(name: "interactive", value: String(interactive))
                ]
            )
        }
    }
    
    func test_options_since() {
        let since = Date.distantPast
        let sut = GetWidgetsRequest(
            options: GetWidgetModelsRequestOptions(
                since: since
                
            ),
            networking: networking,
            accessToken: "access"
        )
        
        AssertRequestURLContainsQueryItems(
            request: sut,
            networking: networking,
            expectedQueryItems: [
                URLQueryItem(name: "since", value: "0001-01-01T00:00:00.000Z")
            ]
        )
    }
    
    func test_sincePlaybackTimestamp() {
        let sut = GetWidgetsRequest(
            options: GetWidgetModelsRequestOptions(
                sincePlaybackTimeMilliseconds: 100
            ),
            networking: networking,
            accessToken: "access"
        )
        
        AssertRequestURLContainsQueryItems(
            request: sut,
            networking: networking,
            expectedQueryItems: [
                URLQueryItem(name: "since_playback_time_ms", value: "100")
            ]
        )
    }
    
    func test_untilPlaybackTimestamp() {
        let sut = GetWidgetsRequest(
            options: GetWidgetModelsRequestOptions(
                untilPlaybackTimeMilliseconds: 100
            ),
            networking: networking,
            accessToken: "access"
        )
        
        AssertRequestURLContainsQueryItems(
            request: sut,
            networking: networking,
            expectedQueryItems: [
                URLQueryItem(name: "until_playback_time_ms", value: "100")
            ]
        )
    }
    
    func test_widgetAttributes() {
        let sut = GetWidgetsRequest(
            options: GetWidgetModelsRequestOptions(
                widgetAttributes: [
                    Attribute(key: "team", value: "red"),
                    Attribute(key: "mood", value: "happy")
                ]
            ),
            networking: networking,
            accessToken: "access"
        )
        
        AssertRequestURLContainsQueryItems(
            request: sut,
            networking: networking,
            expectedQueryItems: [
                URLQueryItem(name: "widget_attribute", value: "team:red"),
                URLQueryItem(name: "widget_attribute", value: "mood:happy")
            ]
        )
    }
}
