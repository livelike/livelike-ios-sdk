//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//
	

import Foundation
import LiveLikeCore
import XCTest

class APIHelperTests: XCTestCase {
    
    func test_url_construction() {
        let url = LLAPIHelper.defaultAPIOrigin.appendingPathComponent("quests", isDirectory: true)
        XCTAssertEqual(
            url.absoluteString,
            "https://cf-blast.livelikecdn.com/api/v1/quests/"
        )
    }
    
    func test_attachAccessToken() {
        let url = URL.fakeURL
        var urlRequest = URLRequest(url: url)
        let accessToken = "some-access-token"
        LLAPIHelper.attach(accessToken: accessToken, to: &urlRequest)
        XCTAssertEqual(
            urlRequest.value(forHTTPHeaderField: "Authorization"),
            "Bearer \(accessToken)"
        )
        
    }
    
    func test_attachPersonalAPIToken() {
        let url = URL.fakeURL
        var urlRequest = URLRequest(url: url)
        let personalAPIToken = "some-access-token"
        LLAPIHelper.attach(personalAPIToken: personalAPIToken, to: &urlRequest)
        XCTAssertEqual(
            urlRequest.value(forHTTPHeaderField: "Authorization"),
            "Token \(personalAPIToken)"
        )
    }
    
    func sampleUsage() {
        struct Payload: Encodable {
            let clientId: String
            let name: String
            let date: Date
        }
        
        struct Quest: Decodable {
            let id: String
        }
        
        var urlComponents = URLComponents(
            url: LLAPIHelper.defaultAPIOrigin.appendingPathComponent("quests", isDirectory: true),
            resolvingAgainstBaseURL: false
        )!
        urlComponents.queryItems = [
            URLQueryItem(name: "user-quest-id", value: "some-user-quest-id")
        ]
        
        var urlRequest = URLRequest(url: urlComponents.url!)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = try? LLAPIHelper.jsonEncoder.encode(
            Payload(clientId: "client-id", name: "name", date: Date())
        )
        
        LLAPIHelper.attach(accessToken: "some-access-token", to: &urlRequest)
        // LLAPIHelper.attach(personalAPIToken: "some-personal-api-token", to: &urlRequest)
        
        URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            if let data = data {
                let quest = try? LLAPIHelper.jsonDecoder.decode(Quest.self, from: data)
            }
        }
    }
}
