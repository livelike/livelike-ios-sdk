//
//  LLJSONDecoderTests.swift
//  EngagementSDKTests
//
//  Created by Mike Moloksher on 4/15/21.
//

@testable import LiveLikeCore
import XCTest

class LLJSONDecoderTests: XCTestCase {
    struct ISOTime: Codable {
        var timeIso: Date
    }
    
    struct badSnakeCaseISOTime: Codable {
        var time_iso: Date
    }

    let validISODates: [String] = [
        "{ \"time_iso\" : \"2020-09-04T16:40:53.000Z\"}",
        "{ \"time_iso\" : \"2020-09-04T16:40:53Z\"}",
        "{ \"time_iso\" : \"2020-09-04T16:40:53.000000+00:00\"}",
        "{ \"time_iso\" : \"2020-09-04T16:40:53+05:30\"}",
        "{ \"time_iso\" : \"2020-09-14T22:00:41.528971Z\"}",
        "{ \"time_iso\" : \"2020-09-04T16:40:53,000000+00:00\"}",
        "{ \"time_iso\" : \"2020-09-04T16:40:53.000000+00:00\"}",
        "{ \"time_iso\" : \"2020-09-04T16:40:53,000Z\"}"
    ]
    
    let invalidISODate: String = "{ \"time_iso\" : \"2020-09-04T16:40:53\"}"
    let invalidSnakeCaseDate: String = "{ \"time_iso\" : \"2020-09-14T22:00:41.528971Z\"}"
    
    let decoder = LLJSONDecoder()
    
    /// Test to make sure valid ISO date formats decode
    func test_LLJSONDecoderValidISODatesSuccess() {
        for date in validISODates {
            let jsonData = date.data(using: .utf8)!
            do {
                _ = try decoder.decode(ISOTime.self, from: jsonData)
            } catch {
                XCTFail(error.localizedDescription)
            }
        }
    }
    
    func test_LLJSONDecoderSkipDateDecodingPlatformCheck() {
        let decoder = LLJSONDecoder(skipPlatformCheck: true)
        
        for date in validISODates {
            let jsonData = date.data(using: .utf8)!
            do {
                _ = try decoder.decode(ISOTime.self, from: jsonData)
            } catch {
                XCTFail(error.localizedDescription)
            }
        }
    }
    
    /// Tests failure in decoding a non ISO format
    func test_LLJSONDecoderInValidISODatesSuccess() {
        let jsonData = invalidISODate.data(using: .utf8)!
        do {
            _ = try decoder.decode(ISOTime.self, from: jsonData)
            XCTFail("Invalid ISO Date should not decode")
        } catch {}
    }
    
    /// Test to make sure the `keyDecodingStrategy = .convertFromSnakeCase` is being followed
    func test_LLJSONDecoderBadSnakeCase() {
        let jsonData = invalidSnakeCaseDate.data(using: .utf8)!
        do {
            _ = try decoder.decode(badSnakeCaseISOTime.self, from: jsonData)
            XCTFail("bad snackcase should not decode")
        } catch {}
    }

}
