//
//  GetChatRoomMembershipsRequestTests.swift
//  
//
//  Created by Jelzon Monzon on 7/3/23.
//

import Foundation
import XCTest
@testable import LiveLikeSwift
import LiveLikeCore

class GetChatRoomMembershipsRequestTests: XCTestCase {
    
    private var networking: MockNetworking!
    private var accessToken: String!
    
    override func setUp() {
        networking = MockNetworking()
        accessToken = "access"
    }
    
    func test_options() {
        test(
            options: .init(),
            expectedQueryItems: []
        )
        test(
            options: .init(roomID: "1234"),
            expectedQueryItems: [
                URLQueryItem(name: "room_id", value: "1234")
            ]
        )
        test(
            options: .init(profileIDs: ["profile123", "profile456"]),
            expectedQueryItems: [
                URLQueryItem(name: "profile_id", value: "profile123"),
                URLQueryItem(name: "profile_id", value: "profile456")
            ]
        )
    }
    
    private func test(
        options: GetChatRoomMembershipsRequestOptions,
        expectedQueryItems: Set<URLQueryItem>
    ) {
        let sut = GetChatRoomMembershipsRequest(
            options: options,
            networking: networking,
            accessToken: accessToken
        )
        
        AssertRequestURLContainsQueryItems(
            request: sut,
            networking: self.networking,
            expectedQueryItems: expectedQueryItems
        )
    }
    
}
