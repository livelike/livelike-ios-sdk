//
//  GetProfileRelationshipsRequestTests.swift
//  
//
//  Created by Mike Moloksher on 2/16/23.
//

import Foundation
import XCTest
@testable import LiveLikeCore
@testable import LiveLikeSwift

class GetProfileRelationshipsRequestTests: XCTestCase {
    
    func test_url_nilOptionsContainsClientIDParam() {
        let networking = MockNetworking()
        
        let request = GetProfileRelationshipsRequest(
            clientID: "ex-client-id",
            options: nil,
            accessToken: "ex-access-token",
            networking: networking
        )
        
        AssertRequestURLContainsQueryItems(
            request: request,
            baseURL: URL(string: "https://example.com")!,
            networking: networking,
            expectedQueryItems: [
                URLQueryItem(name: "client_id", value: "ex-client-id"),
            ]
        )
    }
    
    func test_url_with_full_options() {
        let networking = MockNetworking()
        
        let request = GetProfileRelationshipsRequest(
            clientID: "ex-client-id",
            options: GetProfileRelationshipsOptions(
                relationshipTypeKey: "follow",
                fromProfileID: "from_123",
                toProfileID: "to_123"
            ),
            accessToken: "ex-access-token",
            networking: networking
        )
        
        AssertRequestURLContainsQueryItems(
            request: request,
            baseURL: URL(string: "https://example.com")!,
            networking: networking,
            expectedQueryItems: [
                URLQueryItem(name: "client_id", value: "ex-client-id"),
                URLQueryItem(name: "to_profile_id", value: "to_123"),
                URLQueryItem(name: "from_profile_id", value: "from_123"),
                URLQueryItem(name: "relationship_type_key", value: "follow"),
            ]
        )
    }
}
