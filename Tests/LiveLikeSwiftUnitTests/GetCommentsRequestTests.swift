//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation
import XCTest
@testable import LiveLikeSwift

class GetCommentsRequestTests: XCTestCase {

    func test_url_default() {
        let networking = MockNetworking()
        let request = GetCommentsRequest(
            options: GetCommentsRequestOptions(ordering: .newest),
            commentBoardID: "test",
            accessToken: "",
            networking: networking
        )

        AssertRequestURLContainsQueryItems(
            request: request,
            baseURL: URL(string: "https://example.com/?cursor=xyz")!,
            networking: networking,
            expectedQueryItems: [
                URLQueryItem(name: "cursor", value: "xyz"), // check that query items are being appended to existing query items
                URLQueryItem(name: "ordering", value: "-created_at"),
                URLQueryItem(name: "comment_board_id", value: "test"),
            ]
        )
    }

    func test_url_containsTopLevel() {
        let networking = MockNetworking()
        let request = GetCommentsRequest(
            options: GetCommentsRequestOptions(ordering: .newest, topLevel: false),
            commentBoardID: "test",
            accessToken: "",
            networking: networking
        )

        AssertRequestURLContainsQueryItems(
            request: request,
            baseURL: URL(string: "https://example.com")!,
            networking: networking,
            expectedQueryItems: [
                URLQueryItem(name: "ordering", value: "-created_at"),
                URLQueryItem(name: "comment_board_id", value: "test"),
                URLQueryItem(name: "top_level", value: "false")
            ]
        )
    }

    func test_url_defaultOrderBy() {
        let networking = MockNetworking()
        let request = GetCommentsRequest(
            options: GetCommentsRequestOptions(orderBy: .newest),
            commentBoardID: "test",
            accessToken: "",
            networking: networking
        )

        AssertRequestURLContainsQueryItems(
            request: request,
            baseURL: URL(string: "https://example.com")!,
            networking: networking,
            expectedQueryItems: [
                URLQueryItem(name: "ordering", value: "-created_at"),
                URLQueryItem(name: "comment_board_id", value: "test"),
                URLQueryItem(name: "top_level", value: "false")
            ]
        )
    }
}
