//
//  GetInvokedRewardActionsRequestTests.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 8/25/22.
//

import Foundation
import XCTest
@testable import LiveLikeCore
@testable import LiveLikeSwift

class GetInvokedRewardActionsRequestTests: XCTestCase {
    
    let profileID1 = "112233"
    let profileID2 = "445566"
    let rewardActionKey1 = "ex-reward-action-1"
    let rewardActionKey2 = "ex-reward-action-2"
    let clientID = "123"
    let programID = "pg1"
    
    func test_options_areAddedToQuery() {
        let baseURL = URL(string: "https://example.com")!
        let options = GetInvokedRewardActionsOptions(
            programID: programID,
            rewardActionKeys: [
                rewardActionKey1,
                rewardActionKey2,
            ],
            profileIDs: [
                profileID1,
                profileID2
            ]
        )
        let networking = MockNetworking()
        networking.validateResourceURLCompletion = { [weak self] url in
            guard let self = self else { return }
            XCTAssertEqual(
                url.absoluteString,
                "https://example.com?client_id=\(self.clientID)&program_id=\(self.programID)&reward_action_key=\(self.rewardActionKey1)&reward_action_key=\(self.rewardActionKey2)&profile_id=\(self.profileID1)&profile_id=\(self.profileID2)"
            )
        }
        
        let _ = GetInvokedRewardActionsRequest(
            clientID: clientID,
            options: options,
            accessToken: AccessToken(fromString: ""),
            networking: networking
        ).execute(url: baseURL)
    }
    
    func test_nilOptions_haveEmptyQuery() {
        let baseURL = URL(string: "https://example.com")!
        let networking = MockNetworking()
        networking.validateResourceURLCompletion = { url in
            XCTAssertEqual(
                url.absoluteString,
                "https://example.com?client_id=\(self.clientID)"
            )
        }
        
        let _ = GetInvokedRewardActionsRequest(
            clientID: clientID,
            options: nil,
            accessToken: AccessToken(fromString: ""),
            networking: networking
        ).execute(url: baseURL)
    }
    
    func test_url_containsProgramIDQueryItem() {
        let networking = MockNetworking()
        let request = GetInvokedRewardActionsRequest(
            clientID: "client-id",
            options: GetInvokedRewardActionsOptions(
                programID: "ex-program-id"
            ),
            accessToken: AccessToken(fromString: ""),
            networking: networking
        )
        
        AssertRequestURLContainsQueryItems(
            request: request,
            baseURL: URL(string: "https://example.com")!,
            networking: networking,
            expectedQueryItems: [
                URLQueryItem(name: "client_id", value: "client-id"),
                URLQueryItem(name: "program_id", value: "ex-program-id")
            ]
        )
    }
    
    func test_url_containsRewardActionKeyQueryItem() {
        let networking = MockNetworking()
        let request = GetInvokedRewardActionsRequest(
            clientID: "client-id",
            options: GetInvokedRewardActionsOptions(
                rewardActionKeys: [
                    "ex-reward-action-1",
                    "ex-reward-action-2",
                ]
            ),
            accessToken: AccessToken(fromString: ""),
            networking: networking
        )
        
        AssertRequestURLContainsQueryItems(
            request: request,
            baseURL: URL(string: "https://example.com")!,
            networking: networking,
            expectedQueryItems: [
                URLQueryItem(name: "client_id", value: "client-id"),
                URLQueryItem(name: "reward_action_key", value: "ex-reward-action-1"),
                URLQueryItem(name: "reward_action_key", value: "ex-reward-action-2")
            ]
        )
    }
    
    func test_url_containsAttributesQueryParam() {
        let networking = MockNetworking()
        let request = GetInvokedRewardActionsRequest(
            clientID: "client-id",
            options: GetInvokedRewardActionsOptions(
                attributes: [
                    .init(key: "key1", value: "val1"),
                    .init(key: "key2", value: "val2")
                ]
            ),
            accessToken: AccessToken(fromString: ""),
            networking: networking
        )
        
        AssertRequestURLContainsQueryItems(
            request: request,
            baseURL: URL(string: "https://example.com")!,
            networking: networking,
            expectedQueryItems: [
                URLQueryItem(name: "client_id", value: "client-id"),
                URLQueryItem(name: "attributes", value: "key1:val1"),
                URLQueryItem(name: "attributes", value: "key2:val2")
            ]
        )
    }
    
    
    func test_url_defaultContainsClientID() {
        let networking = MockNetworking()
        let request = GetInvokedRewardActionsRequest(
            clientID: "client-id",
            options: nil,
            accessToken: AccessToken(fromString: ""),
            networking: networking
        )
        
        AssertRequestURLContainsQueryItems(
            request: request,
            baseURL: URL(string: "https://example.com")!,
            networking: networking,
            expectedQueryItems: [
                URLQueryItem(name: "client_id", value: "client-id")
            ]
        )
    }
    
    func test_execute_completesWithSuccessOnNetworkingSuccess() {
        let e = expectation(description: "")
        let baseURL = URL(string: "https://example.com")!
        let accessToken = AccessToken(fromString: "ex-access-token")
        let networking = StubNetworking<PaginatedResult<InvokedRewardAction>>(
            result: .success(.init(
                previous: nil,
                count: 0,
                next: nil,
                items: [
                    InvokedRewardAction(
                        id: "id-1",
                        createdAt: Date(),
                        programID: "program-id-1",
                        profileID: "profile-id-1",
                        rewardActionKey: "reward-action-key-1",
                        code: "code-1",
                        rewards: [],
                        profileNickname: "nickname",
                        attributes: [
                            .init(key: "key-1", value: "value-1")
                        ]
                    )
                ]
            ))
        )
        let sut = GetInvokedRewardActionsRequest(
            clientID: "client-id",
            options: nil,
            accessToken: accessToken,
            networking: networking
        )
        sut.execute(url: baseURL).then { page in
            XCTAssertTrue(
                page.items.contains(where: {
                    $0.id == "id-1" &&
                        $0.programID == "program-id-1" &&
                        $0.profileID == "profile-id-1" &&
                        $0.code == "code-1" &&
                        $0.rewardActionKey == "reward-action-key-1" &&
                        $0.attributes.contains(where: {
                            $0.key == "key-1" &&
                                $0.value == "value-1"
                        })
                })
            )
            e.fulfill()
        }.catch { error in
            XCTFail(error.localizedDescription)
        }
        waitForExpectations(timeout: 1.0)
    }
    
    func test_execute_completesWithFailureOnNetworkingError() {
        let e = expectation(description: "")
        let baseURL = URL(string: "https://example.com")!
        let accessToken = AccessToken(fromString: "ex-access-token")
        let networking = StubNetworking<AnyDecodable>(
            result: .failure(NetworkClientError.notFound404)
        )
        let sut = GetInvokedRewardActionsRequest(
            clientID: "client-id",
            options: nil,
            accessToken: accessToken,
            networking: networking
        )
        sut.execute(url: baseURL).then { _ in
            XCTFail()
        }.catch { error in
            e.fulfill()
            
        }
        waitForExpectations(timeout: 1.0)
    }

}
