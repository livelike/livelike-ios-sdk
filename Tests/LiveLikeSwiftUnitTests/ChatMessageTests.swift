//
//  ChatMessageTests.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 4/4/23.
//

@testable import LiveLikeCore
@testable import LiveLikeSwift
import XCTest

class ChatMessageTests: XCTestCase {

    let chatMessageJSON: String =
        """
        {
          "image_url": null,
          "created_at": "2022-03-15T11:15:07+0000",
          "quote_message_id": "ff1c3c83-ec02-4728-b97a-89f52da169f0",
          "program_date_time": null,
          "sender_id": "6f0fd81e-85ca-4df1-9c93-a78258143226",
          "filtered_message": null,
          "sender_nickname": "Stylish Keeper",
          "sender_profile_url": "https://cf-blast.livelikecdn.com/api/v1/profiles/6f0fd81e-85ca-4df1-9c93-a78258143226/",
          "sender_image_url": null,
          "badge_image_url": null,
          "chat_room_id": "b40a2946-2a89-4b08-9d6d-ac4d04994f89",
          "content_filter": [],
          "updated_at": "2022-03-15T11:15:07+0000",
          "message_event": "message-created",
          "message": "http:&#47;&#47;www.livelike.com",
          "filtered_sender_nickname": null,
          "quote_message": {
            "badge_image_url": null,
            "created_at": "2022-03-08T12:03:24.559Z",
            "from_pubnub": true,
            "content_filter": [],
            "filtered_sender_nickname": null,
            "updated_at": "2022-03-08T12:03:24+0000",
            "image_width": null,
            "id": "9e012999-a770-4d5c-9bc7-6bf943396488",
            "program_date_time": null,
            "sender_id": "c74b6935-6688-4a7f-a0bc-d62801e88e82",
            "image_height": null,
            "message": "hjbh",
            "filtered_message": null,
            "sender_nickname": "Royal Joker",
            "sender_profile_url": "https://cf-blast.livelikecdn.com/api/v1/profiles/c74b6935-6688-4a7f-a0bc-d62801e88e82/",
            "sender_image_url": null
          },
          "id": "8bcd6181-7560-467f-a7e4-8ca38e31c404"
        }
        """

    let decoder = LLJSONDecoder()

    // MARK: - JSON -> Resource conversion tests
    func test_chatMessage_json_to_resource_conversion() {
        let jsonData = chatMessageJSON.data(using: .utf8)!
        do {
            _ = try decoder.decode(ChatMessage.self, from: jsonData)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    // Checks whether a url property decoded in the `.text` property
    func test_chatMessage_decodes_message_text_successfully() {
        let jsonData = chatMessageJSON.data(using: .utf8)!
        do {
            let chatMessage = try decoder.decode(ChatMessage.self, from: jsonData)
            XCTAssertTrue(chatMessage.text == "http://www.livelike.com")
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

}
