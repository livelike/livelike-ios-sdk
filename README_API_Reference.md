# EngagementSDK - iOS
LiveLike’s Audience Engagement Suite gives broadcasters’ teams full control to activate their mobile audience through social features.

## Documentation
Check out our official [integration documentation](https://docs.livelike.com/ios/index.html).

## Requirements
* iOS 10.0+
* Xcode 10.2+
* Swift 4.2+