Pod::Spec.new do |spec|

  spec.name         = "LiveLikeSwift"
  spec.version      = "2.98.1"
  spec.summary      = "Audience Engagement Platform for Broadcasters"
  spec.description  = "LiveLike’s Audience Engagement Suite gives broadcasters’ teams full control to activate their mobile audience through social features."
  spec.homepage     = "https://www.livelike.com"
  spec.license      = { :type => "MIT", :file => "LICENSE.md" }
  spec.author             = { "LiveLike Inc." => "contact@livelike.com" }
  spec.social_media_url   = "https://www.linkedin.com/company/livelike/"
  spec.platform     = :ios, "13.0"
  spec.platform     = :tvos, "12.0"
  spec.ios.deployment_target = '13.0'
  spec.tvos.deployment_target = '12.0'
  spec.swift_version = '5.0.1'
  spec.source       = { :git => 'https://bitbucket.org/livelike/livelike-ios-sdk.git', :tag => spec.version }

  spec.source_files = 'Sources/LiveLikeSwift/**/*.swift'
  spec.resource_bundles = { 'LiveLikeSwiftPrivacyInfo' => ['Sources/LiveLikeSwift/PrivacyInfo.xcprivacy']}

  spec.dependency 'LiveLikeCore', '~> 2.98.1'
  spec.dependency 'PubNubSwift', '~> 7.2.0'
  
end
