//
//  SponsorshipClient.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 6/10/21.
//

import Foundation
import LiveLikeCore

// MARK: - Codable Resources

public struct Sponsor: Codable {

    /// ID of the sponsor
    public let id: String

    /// Application client ID that the sponsor is associated with
    public let clientID: String

    /// Sponsor name
    public let name: String

    /// A sponsor image logo `URL`
    public let logoURL: URL

    /// Sponsor's brand color
    public let brandColorHex: String?

    /// A URL that represents a Sponsor's call to action
    public let clickthroughURL: URL?

    enum CodingKeys: CodingKey {
        case id
        case logoUrl
        case clientId
        case name
        case brandColor
        case clickthroughUrl
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.clientID = try container.decode(String.self, forKey: .clientId)
        self.name = try container.decode(String.self, forKey: .name)
        self.logoURL = try container.decode(URL.self, forKey: .logoUrl)
        self.brandColorHex = try container.decodeIfPresent(String.self, forKey: .brandColor)
        self.clickthroughURL = try? container.decodeIfPresent(URL.self, forKey: .clickthroughUrl)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(clientID, forKey: .clientId)
        try container.encode(name, forKey: .name)
        try container.encode(logoURL, forKey: .logoUrl)
        try container.encode(brandColorHex, forKey: .brandColor)
        try container.encode(clickthroughURL, forKey: .clickthroughUrl)
    }

    init(
        id: String,
        clientID: String,
        name: String,
        logoURL: URL,
        brandColorHex: String? = nil,
        clickthroughURL: URL? = nil
    ) {
        self.id = id
        self.clientID = clientID
        self.name = name
        self.logoURL = logoURL
        self.brandColorHex = brandColorHex
        self.clickthroughURL = clickthroughURL
    }
}

// MARK: - Protocols

public protocol SponsorshipClient {
    func getBy(programID: String, completion: @escaping (Result<[Sponsor], Error>) -> Void)
    func getByApplication(page: Pagination, completion: @escaping (Result<PaginatedResult<Sponsor>, Error>) -> Void)
    func getBy(chatRoomID: String, page: Pagination, completion: @escaping (Result<PaginatedResult<Sponsor>, Error>) -> Void)
}

// MARK: - Internal

final class InternalSponsorshipClient: SponsorshipClient {

    let sponsorshipAPI: LiveLikeSponsorshipAPIProtocol
    let accessTokenVendor: AccessTokenVendor
    let chatAPI: LiveLikeChatAPI
    let coreAPI: LiveLikeCoreAPIProtocol

    private var sponsorsPageURLRepo = [String: PaginatedResultURL]()

    init(coreAPI: LiveLikeCoreAPIProtocol, accessTokenVendor: AccessTokenVendor) {
        self.sponsorshipAPI = LiveLikeSponsorshipAPI(coreAPI: coreAPI)
        self.chatAPI = LiveLikeChatAPI(coreAPI: coreAPI)
        self.coreAPI = coreAPI
        self.accessTokenVendor = accessTokenVendor
    }

    func getBy(programID: String, completion: @escaping (Result<[Sponsor], Error>) -> Void) {
        sponsorshipAPI.getBy(programID: programID, completion: completion)
    }

    func getByApplication(page: Pagination, completion: @escaping (Result<PaginatedResult<Sponsor>, Error>) -> Void) {

        let pageResultURLID = "getSponsorsForApplication"
        let sponsorsForApplicationURL: Promise<URL> = Promise { fulfill, reject in
            switch page {
            case .first:

                self.sponsorsPageURLRepo[pageResultURLID] = nil

                firstly {
                    self.coreAPI.whenApplicationConfig
                }.then { applicationConfig in
                    fulfill(applicationConfig.sponsorsUrl)
                }.catch {
                    reject($0)
                }

            case .next:
                if let url = self.sponsorsPageURLRepo[pageResultURLID]?.nextURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.nextPageUnavailable)
                }
            case .previous:
                if let url = self.sponsorsPageURLRepo[pageResultURLID]?.previousURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.previousPageUnavailable)
                }
            }
        }

        firstly {
            Promises.zip(
                sponsorsForApplicationURL,
                accessTokenVendor.whenAccessToken
            )
        }.then { sponsorsURL, accessToken in
            self.sponsorshipAPI.getByApplication(applicationSponsorsURL: sponsorsURL, accessToken: accessToken, completion: completion)
        }
    }

    func getBy(chatRoomID: String, page: Pagination, completion: @escaping (Result<PaginatedResult<Sponsor>, Error>) -> Void) {

        let pageResultURLID = "getSponsorsForChatRoom_\(chatRoomID)"
        let sponsorsForChatRoomURL: Promise<URL> = Promise { fulfill, reject in
            switch page {
            case .first:

                self.sponsorsPageURLRepo[pageResultURLID] = nil

                firstly {
                    self.accessTokenVendor.whenAccessToken
                }.then { accessToken in
                    self.chatAPI.getChatRoomResource(roomID: chatRoomID, accessToken: accessToken)
                }.then { chatRoomResource in
                    fulfill(chatRoomResource.sponsorsUrl)
                }.catch {
                    reject($0)
                }
            case .next:
                if let url = self.sponsorsPageURLRepo[pageResultURLID]?.nextURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.nextPageUnavailable)
                }
            case .previous:
                if let url = self.sponsorsPageURLRepo[pageResultURLID]?.previousURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.previousPageUnavailable)
                }
            }
        }

        firstly {
            Promises.zip(sponsorsForChatRoomURL, accessTokenVendor.whenAccessToken)
        }.then { sponsorsURL, accessToken in
            self.sponsorshipAPI.getBy(chatRoomSponsorsURL: sponsorsURL, accessToken: accessToken, completion: completion)
        }
    }
}
