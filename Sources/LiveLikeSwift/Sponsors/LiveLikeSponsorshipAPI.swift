//
//  LiveLikeSponsorshipAPI.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 6/7/21.
//

import Foundation
import LiveLikeCore

protocol LiveLikeSponsorshipAPIProtocol {
    func getBy(programID: String, completion: @escaping (Result<[Sponsor], Error>) -> Void)
    func getByApplication(applicationSponsorsURL: URL, accessToken: AccessToken, completion: @escaping (Result<PaginatedResult<Sponsor>, Error>) -> Void)
    func getBy(chatRoomSponsorsURL: URL, accessToken: AccessToken, completion: @escaping (Result<PaginatedResult<Sponsor>, Error>) -> Void)
}

class LiveLikeSponsorshipAPI: LiveLikeSponsorshipAPIProtocol {
    var coreAPI: LiveLikeCoreAPIProtocol

    init(coreAPI: LiveLikeCoreAPIProtocol) {
        self.coreAPI = coreAPI
    }

    func getBy(programID: String, completion: @escaping (Result<[Sponsor], Error>) -> Void) {
        firstly {
            coreAPI.getProgramDetail(programID: programID)
        }.then { program in
            let sponsors = program.sponsors
            completion(.success(sponsors))
        }.catch { error in
            log.error("Sponsor retrieval by program ID Failed")
            completion(.failure(SponsorError.getSponsorFailed(error: error.localizedDescription)))
        }
    }

    func getByApplication(applicationSponsorsURL: URL, accessToken: AccessToken, completion: @escaping (Result<PaginatedResult<Sponsor>, Error>) -> Void) {
        firstly {
            coreAPI.whenApplicationConfig
        }.then { applicationResource in
            let resource = Resource<PaginatedResult<Sponsor>>(
                get: applicationResource.sponsorsUrl,
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { sponsors in
            completion(.success(sponsors))
        }.catch { error in
            log.error("Sponsor retrieval for Application Failed")
            completion(.failure(SponsorError.getSponsorFailed(error: error.localizedDescription)))
        }
    }

    func getBy(chatRoomSponsorsURL: URL, accessToken: AccessToken, completion: @escaping (Result<PaginatedResult<Sponsor>, Error>) -> Void) {
        firstly {
            let resource = Resource<PaginatedResult<Sponsor>>(
                get: chatRoomSponsorsURL,
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { sponsors in
            completion(.success(sponsors))
        }.catch { error in
            log.error("Sponsor retrieval by chatRoom ID Failed")
            completion(.failure(SponsorError.getSponsorFailed(error: error.localizedDescription)))
        }
    }
}
