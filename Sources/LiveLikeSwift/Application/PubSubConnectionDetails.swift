//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

/// The pub sub connection details
public struct PubSubConnectionDetails {
    /// The subscriber key for the connection
    public let subscribeKey: String
    /// The origin for the connection
    public let origin: String?
    /// The ID of the user that is connecting
    public let userID: String
    /// The heartbeat timeout
    public let heartbeatTimeout: Int
    /// The heartbeat interval
    public let heartbeatInterval: Int
}
