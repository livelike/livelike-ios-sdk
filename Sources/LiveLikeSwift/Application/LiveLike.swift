//
//  ViewController.swift
//  Test
//
//  Created by Cory Sullivan on 2019-01-11.
//  Copyright © 2019 Cory Sullivan. All rights reserved.
//

import Foundation
import LiveLikeCore

/**
  The entry point for all interaction with the EngagementSDK.

 - Important: Concurrent instances of the EngagementSDK is not supported; Only one instance should exist at any time.
 */
open class LiveLike: NSObject {
    // MARK: - Static Properties

    // MARK: Internal

    static let networking: LLNetworking = LLCore.networking
    static let prodAPIEndpoint: URL = URL(string: "https://cf-blast.livelikecdn.com/api/v1")!
    static let mediaRepository: MediaRepository = MediaRepository.shared
    static let widgetInteractionCache = WidgetInteractionCache()

    // MARK: - Stored Properties

    // MARK: Public

    /// The sdk's delegate, currently only used to report setup errors
    public weak var delegate: EngagementSDKDelegate?

    private let config: LiveLikeConfig

    /// The EngagementSDKConfig used to initialize this instance of the EngagementSDK
    public var currentConfiguration: LiveLikeConfig {
        return config
    }

    /// A sponsorship client used for sponsorship related queries
    public let sponsorship: SponsorshipClient

    /// A badge client used for badge related queries 
    public let badges: BadgesClient

    /// A chat client used for chat related queries and updates
    public lazy var chat: ChatClient = InternalChatClient(
        whenAccessToken: self.accessTokenVendor.whenAccessToken,
        whenUserProfile: self.whenUserProfile,
        whenProfileResource: self.userProfileVendor.whenProfileResource,
        whenPubSubService: self.whenPubSubService,
        coreAPI: self.coreAPI,
        chatAPI: self.chatAPI
    )

    public lazy var user: UserClient = InternalUserClient(
        whenAccessToken: self.accessTokenVendor.whenAccessToken,
        whenUserProfile: self.whenUserProfile,
        whenProfileResource: self.userProfileVendor.whenProfileResource,
        whenPubSubService: self.whenPubSubService,
        coreAPI: self.coreAPI,
        networking: LiveLike.networking
    )

    /// A reaction client used for `Reaction Space`'s related queries
    public lazy var reaction: ReactionClient = InternalReactionClient(
        sdk: self, 
        clientID: self.clientID
    )

    /// A comment client used for `Comment Boards`'s related queries
    public lazy var commentBoards: CommentBoardClient = InternalCommentBoardClient(
        whenApplicationConfig: self.coreAPI.whenApplicationConfig,
        whenAccessToken: self.accessTokenVendor.whenAccessToken,
        commentsAPI: self.commentAPI,
        networking: LiveLike.networking
    )

    /// A client used to for `Quest`s related queries
    public lazy var quests: QuestsClient = InternalQuestsClient(
        coreAPI: coreAPI,
        accessTokenVendor: self.accessTokenVendor,
        userProfileVendor: userProfileVendor,
        networking: LiveLike.networking
    )

    /// A rewards client used for rewards related queries
    public lazy var rewards: RewardsClient = InternalRewardsClient(
        coreAPI: self.coreAPI,
        accessTokenVendor: self.accessTokenVendor,
        rewardsAPI: LiveLikeRewardsAPI(),
        pubNubService: whenPubSubService,
        userProfileVendor: userProfileVendor,
        networking: LiveLike.networking
    )

    /// A social graph client that can be utilized to create
    /// and query relationships between user profiles
    public lazy var socialGraphClient: SocialGraphClient = InternalSocialGraphClient(
        coreAPI: self.coreAPI,
        accessTokenVendor: self.accessTokenVendor,
        userProfileVendor: userProfileVendor,
        networking: LiveLike.networking
    )

    /// A client for managing Presence
    public lazy var presenceClient: PresenceClient = InternalPresenceClient(
        presenceService: whenPresenceService,
        clientID: self.clientID
    )

    /// A client for managing Redemption Keys
    public lazy var redemptionKeyClient: RedemptionKeyClient = {
        return InternalRedemptionKeyClient(
            coreAPI: self.coreAPI,
            whenUserProfile: self.whenUserProfile,
            networking: self.networking,
            clientID: self.clientID
        )
    }()

    /// A client for managing Widgets
    public lazy var widgetClient: WidgetClient = {
        return InternalWidgetClientAsyncWrapper(
            whenAccessToken: self.whenUserProfile.then {
                $0.accessToken.asString
            },
            whenWidgetModelFactory: self.whenWidgetModelFactory,
            networking: self.networking,
            whenApplication: self.coreAPI.whenApplicationConfig,
            coreAPI: self.coreAPI
        )
    }()

    // MARK: Internal

    private var clientID: String {
        config.clientID
    }
    private(set) var whenInitializedAndReady: Promise<Void> = Promise()

    var widgetPauseStatus: PauseStatus {
        didSet {
            UserDefaults.standard.set(areWidgetsPausedForAllSessions, forKey: LiveLike.permanentPauseUserDefaultsKey)
            widgetPauseDelegates.publish { $0.pauseStatusDidChange(status: self.widgetPauseStatus) }
        }
    }

    // MARK: Private

    private lazy var whenWidgetClient: Promise<PubSubWidgetClient?> = {
        firstly {
            self.coreAPI.whenApplicationConfig

        }.then { configuration -> Promise<(ApplicationConfiguration, UserProfile)> in
            log.info("Successfully initialized the Engagement SDK!")
            self.whenProgramURLTemplate.fulfill(configuration.programDetailUrlTemplate)
            return Promises.zip(
                .init(value: configuration),
                self.whenUserProfile
            )

        }.then { values -> PubSubWidgetClient? in
            let (configuration, userProfile) = values

            let userID = userProfile.userID.asString

            var widgetClient: PubSubWidgetClient?
            if let subscribeKey = configuration.pubnubSubscribeKey {
                widgetClient = InternalPubSubWidgetClient(
                    subscribeKey: subscribeKey,
                    origin: configuration.pubnubOrigin,
                    userID: userID,
                    heartbeatTimeout: configuration.pubnubPresenceTimeout,
                    heartbeatInterval: configuration.pubnubHeartbeatInterval
                )
            }

            self.whenInitializedAndReady.fulfill(())
            return widgetClient
        }
    }()

    private var whenProgramURLTemplate: Promise<String>
    private let livelikeIDVendor: LiveLikeIDVendor
    private let userNicknameService: UserNicknameService
    private let customDataServices: UserCustomDataService
    private let sdkErrorReporter: InternalErrorReporter
    private let predictionVoteRepo: PredictionVoteRepository
    private let leaderboardsManager: LeaderboardsManager = LeaderboardsManager()
    private let widgetPauseDelegates: Listener<PauseDelegate> = Listener<PauseDelegate>()
    private let analytics: Analytics
    private let pubsubServiceFactory: PubSubServiceFactory
    private let paginatedRequestController = PaginatedRequestController()

    let coreAPI: LiveLikeCoreAPIProtocol
    let widgetAPI: LiveLikeWidgetAPIProtocol
    let chatAPI: LiveLikeChatAPIProtocol
    let reactionAPI: LiveLikeReactionAPIProtocol
    let commentAPI: LiveLikeCommentAPIProtocol
    let leaderboardAPI: LiveLikeLeaderboardAPIProtocol
    let accessTokenVendor: AccessTokenVendor
    let userProfileVendor: UserProfileVendor

    struct PaginationProgress {
        var next: URL?
        var previous: URL?
        var total: Int = 0
    }
    let paginationController = PaginatedRequestController()
    var userChatRoomMembershipPagination: PaginationProgress
    var leaderboardEntriesPagination: PaginationProgress
    private let networking: LLNetworking

    lazy var whenWidgetInteractionRepo: Promise<WidgetInteractionRepository> = {
        firstly {
            self.whenUserProfile
        }.then { userProfile in
            ServerWidgetInteractionRepository(
                livelikeAPI: self.widgetAPI,
                userProfile: userProfile,
                cache: LiveLike.widgetInteractionCache
            )
        }
    }()
    private let leaderboardEnriesPromiseQueue = PromiseQueue(
        name: "com.livelike.leaderboardEntries",
        maxConcurrentPromises: 1
    )

    /// Repo to keep track of all the `next` and `previous` urls
    private var pageResultURLRepo = [String: PaginatedResultURL]()

    private(set) lazy var whenPresenceService: Promise<PresenceService> = {
        return firstly {
            Promises.zip(
                self.coreAPI.whenApplicationConfig,
                self.userProfileVendor.whenProfileResource
            )
        }.then { appconfig, profile in
            guard let subscribeKey = appconfig.pubnubSubscribeKey else {
                return Promise(error: PresenceServiceError.pubnubNotAvailable)
            }
            var presenceService = PubNubPresenceService(
                configuration: PresenceServiceConfig(
                    subscribeKey: subscribeKey,
                    userID: profile.id,
                    origin: appconfig.pubnubOrigin,
                    pubnubHeartbeatInterval: appconfig.pubnubHeartbeatInterval,
                    pubnubPresenceTimeout: appconfig.pubnubPresenceTimeout
                )
            )

            return Promise(value: presenceService)
        }.catch { error in
            log.error(error.localizedDescription)
        }
    }()

    private(set) lazy var whenPubSubService: Promise<PubSubService?> = {
        return firstly {
            self.pubsubServiceFactory.make()
        }.then { pubSubService -> Promise<PubSubService?> in
            return Promise(value: pubSubService)
        }.recover { _ in
            return Promise(value: nil)
        }
    }()

    private(set) lazy var whenUserProfile: Promise<UserProfile> = {
        return firstly {
            Promises.zip(
                self.accessTokenVendor.whenAccessToken,
                self.livelikeIDVendor.whenLiveLikeID,
                self.userNicknameService.whenInitialNickname,
                self.userProfileVendor.whenProfileResource
            )
        }.then { accessToken, livelikeID, _, profileResource -> UserProfile in
            guard let nickname = self.userNicknameService.currentNickname else {
                throw UserProfileError.failedToFindNickname
            }
            return UserProfile(
                userID: livelikeID,
                accessToken: accessToken,
                nickname: nickname,
                customData: profileResource.customData
            )
        }
    }()

    private lazy var whenWidgetModelFactory: Promise<WidgetModelFactory> = {
        return firstly {
            Promises.zip(
                self.whenWidgetClient,
                self.accessTokenVendor.whenAccessToken,
                self.whenUserProfile,
                self.whenWidgetInteractionRepo
            )
        }.then { widgetClient, accessToken, userProfile, widgetInteractionRepo in
            let widgetModelFactory = WidgetModelFactory(
                eventRecorder: self.eventRecorder,
                userProfile: userProfile,
                rewardItems: [],
                leaderboardsManager: self.leaderboardsManager,
                widgetClient: widgetClient,
                widgetAPI: self.widgetAPI,
                leaderboardAPI: self.leaderboardAPI,
                predictionVoteRepo: self.predictionVoteRepo,
                widgetInteractionRepo: widgetInteractionRepo
            )
            return Promise(value: widgetModelFactory)
        }
    }()

    // MARK: - Initialization

    /// Initializes an instance of the EngagementSDK
    /// - Parameter config: An LiveLikeConfig object
    public init(config: LiveLikeConfig) {
        let coreAPI = LiveLikeCoreAPI(
            apiBaseURL: config.apiOrigin,
            clientID: config.clientID
        )
        let chatAPI = LiveLikeChatAPI(coreAPI: coreAPI)
        let reactionAPI = LiveLikeReactionAPI(coreAPI: coreAPI)
        let widgetAPI = LiveLikeWidgetAPI(coreAPI: coreAPI)
        let leaderboardAPI = LiveLikeLeaderboardAPI(coreAPI: coreAPI)
        let sdkErrorReporter = InternalErrorReporter()
        let userResolver = UserResolver(
            accessTokenStorage: config.accessTokenStorage,
            clientID: config.clientID,
            origin: config.apiOrigin,
            sdkDelegate: sdkErrorReporter
        )
        let profileVendor = ProfileVendor(
            accessTokenVendor: userResolver,
            coreAPI: coreAPI
        )
        let sponsorshipClient = InternalSponsorshipClient(coreAPI: coreAPI, accessTokenVendor: userResolver)

        let badgesClient = InternalBadgesClient(
            coreAPI: coreAPI,
            accessTokenVendor: userResolver
        )

        self.config = config
        self.predictionVoteRepo = config.widget.predictionVoteRepository
        self.accessTokenVendor = userResolver
        self.coreAPI = coreAPI
        self.widgetAPI = widgetAPI
        self.chatAPI = chatAPI
        self.reactionAPI = reactionAPI
        self.leaderboardAPI = leaderboardAPI
        self.sponsorship = sponsorshipClient
        self.badges = badgesClient
        self.livelikeIDVendor = profileVendor
        self.userNicknameService = profileVendor
        self.customDataServices = profileVendor
        self.userProfileVendor = profileVendor
        self.sdkErrorReporter = sdkErrorReporter
        self.commentAPI = LiveLikeCommentAPI(
            coreAPI: coreAPI,
            networking: LiveLike.networking
        )
        self.networking = LiveLike.networking
        analytics = Analytics()
        whenProgramURLTemplate = Promise<String>()

        widgetPauseStatus = UserDefaults.standard.bool(forKey: LiveLike.permanentPauseUserDefaultsKey) == true ? .paused : .unpaused
        self.userChatRoomMembershipPagination = PaginationProgress()
        self.leaderboardEntriesPagination = PaginationProgress()
        self.pubsubServiceFactory = PubNubServiceFactory(
            whenApplication: coreAPI.whenApplicationConfig,
            whenAccessToken: userResolver.whenAccessToken,
            whenProfile: profileVendor.whenProfileResource
        )

        super.init()
        setup()
    }

    internal init(
        config: LiveLikeConfig,
        coreAPI: LiveLikeCoreAPIProtocol,
        widgetAPI: LiveLikeWidgetAPIProtocol,
        chatAPI: LiveLikeChatAPIProtocol,
        reactionAPI: LiveLikeReactionAPIProtocol,
        commentAPI: LiveLikeCommentAPIProtocol,
        leaderboardAPI: LiveLikeLeaderboardAPIProtocol,
        sponsorshipClient: SponsorshipClient,
        badgesClient: BadgesClient,
        accessTokenVendor: AccessTokenVendor,
        livelikeIDVendor: LiveLikeIDVendor,
        userNicknameService: UserNicknameService,
        customDataService: UserCustomDataService,
        userProfileVendor: UserProfileVendor,
        sdkErrorReporter: InternalErrorReporter,
        pubsubServiceFactory: PubSubServiceFactory,
        networking: LLNetworking
    ) {
        self.config = config
        self.predictionVoteRepo = config.widget.predictionVoteRepository
        self.accessTokenVendor = accessTokenVendor
        self.coreAPI = coreAPI
        self.widgetAPI = widgetAPI
        self.chatAPI = chatAPI
        self.reactionAPI = reactionAPI
        self.commentAPI = commentAPI
        self.leaderboardAPI = leaderboardAPI
        self.sponsorship = sponsorshipClient
        self.badges = badgesClient
        self.livelikeIDVendor = livelikeIDVendor
        self.userNicknameService = userNicknameService
        self.customDataServices = customDataService
        self.userProfileVendor = userProfileVendor
        self.sdkErrorReporter = sdkErrorReporter
        analytics = Analytics()
        whenProgramURLTemplate = Promise<String>()
        log.info("Initializing EngagementSDK using client id: '\(config.clientID)'")
        widgetPauseStatus = UserDefaults.standard.bool(forKey: LiveLike.permanentPauseUserDefaultsKey) == true ? .paused : .unpaused
        self.userChatRoomMembershipPagination = PaginationProgress()
        self.leaderboardEntriesPagination = PaginationProgress()
        self.pubsubServiceFactory = pubsubServiceFactory
        self.networking = networking

        super.init()
        setup()
    }

    private func setup() {
        LLCore.networking.userAgent += "[LiveLikeSwift]"
        log.info("Initializing EngagementSDK using client id: '\(config.clientID)'")
        sdkErrorReporter.delegate = self

        /// 1. Load application resource
        /// 2. Load profile resource
        firstly {
            Promises.zip(
                self.coreAPI.whenApplicationConfig,
                self.whenUserProfile
            )
        }.then { _, _ in
            self.delegate?.sdk?(setupCompleted: self)
        }.catch { error in
            switch error {
            case NetworkClientError.badRequest, NetworkClientError.notFound404:
                if self.config.apiOrigin != LiveLikeConfig.defaultAPIOrigin {
                    self.delegate?.sdk?(self, setupFailedWithError: SetupError.invalidAPIOrigin)
                } else {
                    self.delegate?.sdk?(self, setupFailedWithError: SetupError.invalidClientID(self.config.clientID))
                }
            default:
                self.delegate?.sdk?(self, setupFailedWithError: error)
            }
        }
    }

    struct LeaderboardAndCurrentEntry {
        let leaderboard: Leaderboard
        let currentEntry: LeaderboardEntryResource?
    }

    func getLeaderboardAndCurrentEntry(leaderboardID: String, profileID: String) -> Promise<LeaderboardAndCurrentEntry> {
        firstly {
            self.leaderboardAPI.getLeaderboard(leaderboardID: leaderboardID)
        }.then { leaderboard in
            return self.getLeaderboardAndCurrentEntry(leaderboard: leaderboard, profileID: profileID)
        }
    }

    func getLeaderboardAndCurrentEntry(leaderboard: Leaderboard, profileID: String) -> Promise<LeaderboardAndCurrentEntry> {
        firstly {
            self.accessTokenVendor.whenAccessToken
        }.then { accessToken -> Promise<LeaderboardEntryResource> in
            guard let entryURL = leaderboard.getEntryURL(profileID: profileID) else {
                return Promise(error: LLGamificationError.leaderboardUrlCreationFailure)
            }
            return self.leaderboardAPI.getLeaderboardEntry(url: entryURL, accessToken: accessToken)
        }.then { leaderboardEntry in
            let leaderboardAndCurrentEntry = LeaderboardAndCurrentEntry(
                leaderboard: leaderboard,
                currentEntry: leaderboardEntry
            )
            return Promise(value: leaderboardAndCurrentEntry)
        }.recover { error in
            // If the user's entry is not found then recover with nil
            // A user will not have an entry until they earn their first points
            if case NetworkClientError.notFound404 = error {
                return Promise(value: LeaderboardAndCurrentEntry(
                    leaderboard: leaderboard,
                    currentEntry: nil
                )
                )
            } else {
                return Promise(error: error)
            }
        }
    }

    /// Gets the current user's profile
    public func getCurrentUserProfile(completion: @escaping (Result<UserProfile, Error>) -> Void) {
        firstly {
            Promises.zip(
                self.accessTokenVendor.whenAccessToken,
                self.livelikeIDVendor.whenLiveLikeID,
                self.userNicknameService.whenInitialNickname,
                self.customDataServices.whenInitialCustomData,
                self.userProfileVendor.whenProfileResource
            )
        }.then { accessToken, livelikeID, _, _, profileResource in

            guard let nickname = self.userNicknameService.currentNickname else {
                throw UserProfileError.failedToFindNickname
            }
            let customData = self.customDataServices.currentCustomData

            let profile = UserProfile(
                userID: livelikeID,
                accessToken: accessToken,
                nickname: nickname,
                customData: customData
            )

            completion(.success(profile))
        }.catch {
            completion(.failure($0))
        }
    }
}

// MARK: - Static Public APIs

public extension LiveLike {
    /// A property to control the level of logging from the `EngagementSDK`.

    static var logLevel: LogLevel {
        get { return Logger.LoggingLevel }
        set { Logger.LoggingLevel = newValue }
    }
}

// MARK: - Public APIs

public extension LiveLike {

    /// A delegate that returns analytics events.
    var analyticsDelegate: EngagementAnalyticsDelegate? {
        get { return analytics.delegate }
        set { analytics.delegate = newValue }
    }

    /// Returns whether widgets are paused for all sessions
    var areWidgetsPausedForAllSessions: Bool {
        return widgetPauseStatus == .paused
    }

    /**
     Creates a new `ContentSession` instance using a SessionConfiguration object.

     - Parameter config: A configuration object that defines the properties for a `ContentSession`
     - Parameter delegate: an object that will act as the delegate of the content session.
     - Returns: returns the `ContentSession`
     */
    func contentSession(config: SessionConfiguration, delegate: ContentSessionDelegate) -> ContentSession {
        return contentSessionInternal(config: config, delegate: delegate)
    }

    /**
     Creates a new `ContentSession` instance using a SessionConfiguration object.

     - Parameter config: A configuration object that defines the properties for a `ContentSession`
     - Returns: returns the `ContentSession`
     */
    func contentSession(config: SessionConfiguration) -> ContentSession {
        return contentSessionInternal(config: config, delegate: nil)
    }

    /// Creates a new `ContentSession` instance using a SessionConfiguration object.
    /// - Parameters:
    ///   - config: A configuration object that defines the properties for a `ContentSession`
    ///   - completion: A completion that returns the `ContentSession` if successful
    func createContentSession(config: SessionConfiguration, completion: @escaping (Result<ContentSession, Error>) -> Void) {
        let contentSession = self.contentSessionInternal(config: config, delegate: nil)
        firstly {
            contentSession.whenInitializedAndReady
        }.then {
            completion(.success(contentSession))
        }.catch { error in
            completion(.failure(error))
        }
    }

    // MARK: User Profile

    /// Sets a user's display name and calls the completion block
    func setUserDisplayName(_ newDisplayName: String, enforceNameLenghtLimit: Bool = true, completion: @escaping (Result<Void, Error>) -> Void) {

        if enforceNameLenghtLimit, !(1 ... 20).contains(newDisplayName.count) {
            completion(.failure(SetUserDisplayNameError.invalidNameLength))
            return
        }

        firstly {
            userNicknameService.setNickname(nickname: newDisplayName)
        }.then { _ in
            completion(.success(()))
        }.catch {
            completion(.failure($0))
        }
    }

    /**
     Sets a user's display name and calls the completion block
     - Note: This version of the method is for Objective-C, if using swift we encourage the variant which returns a `Result<Void, Error>`.
     */

    func setUserDisplayName(_ newDisplayName: String, completion: @escaping (Bool, Error?) -> Void) {
        setUserDisplayName(newDisplayName) {
            switch $0 {
            case .success:
                completion(true, nil)
            case let .failure(error):
                completion(false, error)
            }
        }
    }

    func getUserDisplayName(completion: @escaping (Result<String, Error>) -> Void) {
        firstly {
            whenUserProfile
        }.then { userProfile in
            completion(.success(userProfile.nickname))
        }.catch {
            completion(.failure($0))
        }
    }

    /// Retrieves current user profile ID
    func getCurrentUserProfileID(completion: @escaping (Result<String, Error>) -> Void) {
        firstly {
            whenUserProfile
        }.then {
            completion(.success($0.userID.asString))
        }.catch {
            completion(.failure($0))
        }
    }

    /// Sets custom data on the current user's profile
    func setUserCustomData(data: String, completion: @escaping (Result<Void, Error>) -> Void) {
        firstly {
            customDataServices.setCustomData(customData: data)
        }.then { _ in
            completion(.success(()))
        }.catch {
            completion(.failure($0))
        }
    }

    // MARK: Chat

    /// Creates a connection to a chat room.
    func connectChatRoom(
        config: ChatSessionConfig,
        completion: @escaping (Result<ChatSession, Error>) -> Void
    ) {
        log.info("Connecting to chat room with id \(config.roomID).")
        self.loadChatRoom(
            config: config
        ) { result in
            switch result {
            case .success(let chatRoom):
                let chatSession: ChatSession = {
                    if let syncTimeSource = config.syncTimeSource {
                        log.info("Found syncTimeSource - Enabling Spoiler Free Sync for Chat Session with id \(config.roomID)")
                        let spoilerFreeChatRoom = SpoilerFreeChatSession(
                            realChatRoom: chatRoom,
                            startTimer: true,
                            playerTimeSource: syncTimeSource
                        )
                        return spoilerFreeChatRoom
                    } else {
                        return chatRoom
                    }
                }()

                log.info("Loading initial history for Chat Room with id: \(config.roomID)")
                chatSession.loadInitialHistory {
                    switch $0 {
                    case .success:
                        completion(.success(chatSession))
                    case .failure(let error):
                        completion(.failure(error))
                    }
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    /// Creates a public chat room with an optional title
    func createChatRoom(title: String?, completion: @escaping (Result<String, Error>) -> Void) {
        createChatRoom(title: title, visibility: .everyone, completion: completion)
    }

    /// Creates a chat room with an optional title and an ability to set the room's visibility and token gating
    func createChatRoom(
        title: String?,
        visibility: ChatRoomVisibilty,
        tokenGates: [TokenGate]? = nil,
        completion: @escaping (Result<String, Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                self.accessTokenVendor.whenAccessToken,
                self.coreAPI.whenApplicationConfig
            )
        }.then { accessToken, appConfig -> Promise<ChatRoomResource> in
            self.chatAPI.createChatRoomResource(
                title: title,
                visibility: visibility,
                accessToken: accessToken,
                appConfig: appConfig,
                tokenGates: tokenGates
            )
        }.then { chatRoomResource in
            completion(.success(chatRoomResource.id))
        }.catch { error in
            log.error("Error creating room: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }

    func createCommentClient(
        for commentBoardID: String,
        completion: @escaping (Result<CommentClient, Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                self.accessTokenVendor.whenAccessToken,
                self.coreAPI.whenApplicationConfig
            )
        }.then { accessToken, appConfig -> Promise<CommentBoard> in
            self.commentAPI.getCommentBoardInfo(
                commentBoardURL: try appConfig.getCommentBoardDetailURL(
                    commentBoardID: commentBoardID
                ),
                accessToken: accessToken
            )
        }.then { commentBoard in
            completion(.success(
                InternalCommentClient(
                    whenAccessToken: self.accessTokenVendor.whenAccessToken,
                    commentsAPI: self.commentAPI,
                    commentBoardClient: self.commentBoards,
                    commentBoard: commentBoard,
                    networking: LiveLike.networking
                )
            )
            )
        }.catch { error in
            log.error("Error creating Comment Client: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }

    /// Retrieve information about a chat room
    func getChatRoomInfo(roomID: String, completion: @escaping (Result<ChatRoomInfo, Error>) -> Void) {
        firstly {
            self.accessTokenVendor.whenAccessToken
        }.then { accessToken -> Promise<ChatRoomResource> in
            self.chatAPI.getChatRoomResource(roomID: roomID, accessToken: accessToken)
        }.then { chatRoomResource in
            completion(.success(ChatRoomInfo(chatroomResource: chatRoomResource)))
        }.catch { error in
            log.error("Error getting chat room info: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }

    /// Retrieve all the users who are members of a chat room
    func getChatRoomMemberships(
        roomID: String,
        page: ChatRoomMembershipPagination,
        completion: @escaping (Result<[ChatRoomMember], Error>) -> Void
    ) {
        firstly {
            self.accessTokenVendor.whenAccessToken
        }.then { accessToken in
            return Promises.zip(
                Promise(value: accessToken),
                self.chatAPI.getChatRoomResource(
                    roomID: roomID,
                    accessToken: accessToken
                )
            )
        }.then { accessToken, chatRoomResource in
            let request = GetChatRoomMembershipsRequest(
                options: GetChatRoomMembershipsRequestOptions(roomID: roomID),
                networking: self.networking,
                accessToken: accessToken.asString
            )

            self.paginationController.performExecute(
                paginatedRequest: request,
                rootURL: chatRoomResource.membershipsUrl,
                page: page.asPagination,
                completion: completion
            )
        }
    }

    func getChatRoomMemberships(
        options: GetChatRoomMembershipsRequestOptions?,
        page: Pagination,
        completion: @escaping (Result<[ChatRoomMember], Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                coreAPI.whenApplicationConfig,
                self.accessTokenVendor.whenAccessToken
            )
        }.then { appConfig, accessToken in
            let request = GetChatRoomMembershipsRequest(
                options: options,
                networking: LiveLike.networking,
                accessToken: accessToken.asString
            )

            self.paginationController.performExecute(
                paginatedRequest: request,
                rootURL: appConfig.chatRoomMembershipsUrl,
                page: page,
                completion: completion
            )
        }
    }

    /// Retrieve all Chat Rooms the current user is a member of
    func getUserChatRoomMemberships(
        page: ChatRoomMembershipPagination,
        completion: @escaping (Result<[ChatRoomInfo], Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                self.accessTokenVendor.whenAccessToken,
                self.userProfileVendor.whenProfileResource
            )
        }.then { accessToken, profileResource -> Promise<UserChatRoomMembershipsResult> in

            // Handle `.next`, `.previous` cases and their availibility
            var notFirstMembershipUrl: URL?
            switch page {
            case .first:
                // reset next/prev urls stored from previous calls to a different room
                self.userChatRoomMembershipPagination = PaginationProgress()
            case .next:
                guard let nextPageUrl = self.userChatRoomMembershipPagination.next else {
                    log.info("Next chat room membership page is unavailable")
                    return Promise(value: UserChatRoomMembershipsResult(chatRooms: [], next: nil, previous: nil))
                }
                notFirstMembershipUrl = nextPageUrl
            case .previous:
                guard let previousPageUrl = self.userChatRoomMembershipPagination.previous else {
                    log.info("Previous chat room membership page is unavailable")
                    return Promise(value: UserChatRoomMembershipsResult(chatRooms: [], next: nil, previous: nil))
                }
                notFirstMembershipUrl = previousPageUrl
            }

            return self.chatAPI.getUserChatRoomMemberships(
                url: notFirstMembershipUrl ?? profileResource.chatRoomMembershipsUrl,
                accessToken: accessToken,
                page: page
            )
        }.then { userChatRoomMembershipsResult in
            self.userChatRoomMembershipPagination.next = userChatRoomMembershipsResult.next
            self.userChatRoomMembershipPagination.previous = userChatRoomMembershipsResult.previous
            completion(.success(userChatRoomMembershipsResult.chatRooms))
        }.catch { error in
            log.error("Error getting user chat room memberships: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }

    /// Retrieves all saved smart contract address aliases
    func getSmartContracts(
        page: Pagination,
        completion: @escaping (Result<[SmartContractInfo], Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                self.accessTokenVendor.whenAccessToken,
                self.coreAPI.whenApplicationConfig
            )
        }.then(on: DispatchQueue.global()) { accessToken, appConfig in
            self.paginatedRequestController.performExecute(
                paginatedRequest: GetSmartContractsRequest(
                    accessToken: accessToken.asString,
                    networking: LiveLike.networking
                ),
                rootURL: appConfig.savedContractAddressesUrl,
                page: page,
                completion: completion
            )
        }.catch { error in
            log.error("Error getting user smart contract names: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }

    /// Create a membership between the current user and a Chat Room
    func createUserChatRoomMembership(
        roomID: String,
        completion: @escaping (Result<ChatRoomMember, Error>) -> Void
    ) {
        firstly {
            self.accessTokenVendor.whenAccessToken
        }.then { accessToken -> Promise<ChatRoomMember> in
            self.chatAPI.createChatRoomMembership(roomID: roomID, accessToken: accessToken)
        }.then { chatRoomMember in
            completion(.success(chatRoomMember))
        }.catch { error in
            log.error("Error creating room membership: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }

    /// Deletes a membership between the current user and a Chat Room
    func deleteUserChatRoomMembership(
        roomID: String,
        completion: @escaping (Result<Bool, Error>) -> Void
    ) {
        firstly {
            self.accessTokenVendor.whenAccessToken
        }.then { accessToken -> Promise<Bool> in
            self.chatAPI.deleteChatRoomMembership(roomID: roomID, accessToken: accessToken)
        }.then { chatRoomMember in
            completion(.success(chatRoomMember))
        }.catch { error in
            log.error("Error deleting room membership: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }

    /// Find out whether the current user is muted for a `roomID`
    func getChatUserMutedStatus(
        roomID: String,
        completion: @escaping (Result<ChatUserMuteStatus, Error>) -> Void
    ) {
        firstly {
            self.whenUserProfile
        }.then { userProfile -> Promise<ChatUserMuteStatusResource> in
            self.chatAPI.getChatUserMutedStatus(
                profileID: userProfile.userID.asString,
                roomID: roomID,
                accessToken: userProfile.accessToken
            )
        }.then { chatUserMuteStatusResource in
            completion(.success(ChatUserMuteStatus(isMuted: chatUserMuteStatusResource.isMuted)))
        }.catch { error in
            log.error("Error getting chat user muted status: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }

    // MARK: Widgets

    func getWidgetModel<T: WidgetModelable>(
        _ type: T.Type,
        id: String,
        kind: WidgetKind,
        completion: @escaping (Result<T, Error>) -> Void
    ) {
        self.getWidgetModel(id: id, kind: kind) { result in
            completion(
                result.flatMap {
                    guard let model = $0.baseWidgetModel as? T else {
                        return .failure(GetWidgetError.widgetDoesNotExist)
                    }
                    return .success(model)
                }
            )
        }
    }

    /// Retrieve widget details of a widget by `id` and `kind`
    func getWidgetModel(
        id: String,
        kind: WidgetKind,
        completion: @escaping (Result<WidgetModel, Error>) -> Void
    ) {
        firstly {
            widgetAPI.getWidget(id: id, kind: kind)
        }.then { widgetResource in
            Promises.zip(
                .init(value: widgetResource),
                self.whenWidgetModelFactory,
                self.coreAPI.getProgramDetail(programID: widgetResource.programID)
            )
        }.then { widgetResource, widgetModelFactory, programResource in
            let widgetModel = try widgetModelFactory.make(
                from: widgetResource,
                programSubscribeChannel: programResource.getWidgetPublishChannel()
            )
            completion(.success(widgetModel))
        }.catch { error in
            log.error("Error retrieving widget: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }

    func createWidgetModel(
        withJSONObject jsonObject: Any,
        completion: @escaping (Result<WidgetModel, Error>) -> Void
    ) {
        do {
            let widgetResource = try WidgetPayloadParser.parse(jsonObject)
            firstly {
                Promises.zip(
                    whenWidgetModelFactory,
                    coreAPI.getProgramDetail(programID: widgetResource.programID)
                )
            }.then { widgetModelFactory, programResource in
                let widgetModel = try widgetModelFactory.make(
                    from: widgetResource,
                    programSubscribeChannel: programResource.getWidgetPublishChannel()
                )
                completion(.success(widgetModel))
            }.catch {
                completion(.failure($0))
            }
        } catch {
            completion(.failure(error))
        }
    }

    // MARK: Gamification

    func getLeaderboardClients(leaderboardIDs: [String], completion: @escaping (Result<[LeaderboardClient], Error>) -> Void) {
        firstly {
            whenUserProfile
        }.then { userProfile in
            Promises.all(leaderboardIDs.map {
                self.getLeaderboardAndCurrentEntry(leaderboardID: $0, profileID: userProfile.userID.asString)
            })
        }.then { leaderboardEntriesResponse in
            let leaderboards = leaderboardEntriesResponse.map {
                LeaderboardClient(
                    leaderboardResource: $0.leaderboard,
                    currentLeaderboardEntry: $0.currentEntry,
                    leaderboardsManager: self.leaderboardsManager
                )
            }
            completion(.success(leaderboards))
        }.catch { error in
            log.error("Error retrieving Leaderboards: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }

    /// Retrieves leaderboards for a given program
    func getLeaderboards(programID: String, completion: @escaping (Result<[Leaderboard], Error>) -> Void) {
        firstly {
            leaderboardAPI.getLeaderboards(programID: programID)
        }.then { leaderboards in
            completion(.success(leaderboards))
        }.catch { error in
            log.error("Error retrieving Leaderboards: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }

    /// Retrieves leaderboard details
    func getLeaderboard(leaderboardID: String, completion: @escaping (Result<Leaderboard, Error>) -> Void) {
        firstly {
            leaderboardAPI.getLeaderboard(leaderboardID: leaderboardID)
        }.then { leaderboard in
            completion(.success(leaderboard))
        }.catch { error in
            log.error("Error retrieving Leaderboard: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }

    /// Retrieves a paginated list of leaderboard entries defined by `GetLeaderboardEntriesRequestOptions`
    func getLeaderboardEntries(
        page: Pagination,
        options: GetLeaderboardEntriesRequestOptions,
        completion: @escaping (Result<LeaderboardEntriesResult, Error>) -> Void
    ) {

        var pageResultURLID = "getLeaderboardEntriesWithOptions"

        // add params to the ID to uniqely identify a paginated call that has params
        pageResultURLID += "_\(options.id)"
        let whenLeaderboardEntriesURL: Promise<URL> = Promise { fulfill, reject in
            switch page {
            case .first:

                // Reset result repo cache if the user is calling `.first` page
                self.pageResultURLRepo[pageResultURLID] = nil

                firstly {
                    self.leaderboardAPI.getLeaderboard(leaderboardID: options.leaderboardID)
                }.then { leaderboardResource in

                    guard var leaderboardEntriesURLComponents = URLComponents(
                        url: leaderboardResource.entriesURL,
                        resolvingAgainstBaseURL: false
                    ) else {
                        completion(
                            .failure(
                                LeaderboardErrors.failedBuildingLeaderboardEntriesURL(
                                    error: "Failed to build Leaderboard Entries URL Components"
                                )
                            )
                        )
                        return
                    }

                    leaderboardEntriesURLComponents.queryItems = options.urlQueries

                    guard let leaderboardEntriesURL = leaderboardEntriesURLComponents.url else {
                        completion(
                            .failure(
                                LeaderboardErrors.failedBuildingLeaderboardEntriesURL(
                                    error: "Failed to build Leaderboard Entries URL"
                                )
                            )
                        )
                        return
                    }

                    fulfill(leaderboardEntriesURL)

                }.catch {
                    reject($0)
                }

            case .next:
                if let url = self.pageResultURLRepo[pageResultURLID]?.nextURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.nextPageUnavailable)
                }
            case .previous:
                if let url = self.pageResultURLRepo[pageResultURLID]?.previousURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.previousPageUnavailable)
                }
            }
        }

        firstly {
            Promises.zip(
                whenLeaderboardEntriesURL,
                self.accessTokenVendor.whenAccessToken
            )

        }.then { leaderboardEntriesURL, accessToken in
            self.leaderboardAPI.getLeaderboardEntries(url: leaderboardEntriesURL, accessToken: accessToken)
        }.then { paginatedLeaderboardEntries in

            let leaderboardEntries = paginatedLeaderboardEntries.items.map { LeaderboardEntry($0)}

            let entriesResult = LeaderboardEntriesResult(
                entries: leaderboardEntries,
                total: paginatedLeaderboardEntries.count,
                hasPrevious: paginatedLeaderboardEntries.hasPrevious,
                hasNext: paginatedLeaderboardEntries.hasNext
            )
            completion(.success(entriesResult))

        }.catch { error in
            log.error("Failed retrieving leaderboard entries")
            completion(.failure(LeaderboardErrors.failedRetrievingLeaderboardEntries(
                error: error.localizedDescription)))
        }
    }

    /// Retrieves a paginated list of leaderboard entries for a leaderboard
    func getLeaderboardEntries(
        leaderboardID: String,
        page: Pagination,
        completion: @escaping (Result<LeaderboardEntriesResult, Error>) -> Void
    ) {

        let leaderboardEntriesPromise = PromiseTask { () -> Promise<Void> in
            return Promise<Void> { [weak self] fulfill, reject in
                guard let self = self else { return }

                firstly {
                    Promises.zip(
                        self.leaderboardAPI.getLeaderboard(leaderboardID: leaderboardID),
                        self.accessTokenVendor.whenAccessToken
                    )
                }.then { leaderboard, accessToken -> Promise<PaginatedResult<LeaderboardEntryResource>> in

                    // Handle `.next`, `.previous` cases and their availibility
                    var notFirstPageUrl: URL?
                    switch page {
                    case .first:
                        // reset next/prev urls stored from previous calls to a different room
                        self.leaderboardEntriesPagination = PaginationProgress()
                    case .next:
                        guard let nextPageUrl = self.leaderboardEntriesPagination.next else {
                            log.info("Next leaderboard entries page is unavailable")
                            return Promise(value: PaginatedResult(
                                previous: nil,
                                count: self.leaderboardEntriesPagination.total,
                                next: nil,
                                items: []
                            ))
                        }
                        notFirstPageUrl = nextPageUrl
                    case .previous:
                        guard let previousPageUrl = self.leaderboardEntriesPagination.previous else {
                            log.info("Previous leaderboard entries page is unavailable")
                            return Promise(value: PaginatedResult(
                                previous: nil,
                                count: self.leaderboardEntriesPagination.total,
                                next: nil,
                                items: []
                            ))
                        }
                        notFirstPageUrl = previousPageUrl
                    }

                    return self.leaderboardAPI.getLeaderboardEntries(
                        url: notFirstPageUrl ?? leaderboard.entriesURL,
                        accessToken: accessToken
                    )
                }.then { leaderboardEntriesResult in
                    self.leaderboardEntriesPagination.total = leaderboardEntriesResult.count
                    self.leaderboardEntriesPagination.next = leaderboardEntriesResult.next
                    self.leaderboardEntriesPagination.previous = leaderboardEntriesResult.previous
                    let hasPrevious: Bool = leaderboardEntriesResult.previous != nil
                    let hasNext: Bool = leaderboardEntriesResult.next != nil
                    let leaderboardEntries = leaderboardEntriesResult.items.map { LeaderboardEntry($0)}

                    completion(.success(LeaderboardEntriesResult(
                        entries: leaderboardEntries,
                        total: leaderboardEntriesResult.count,
                        hasPrevious: hasPrevious,
                        hasNext: hasNext
                    )))
                    fulfill(())

                }.catch { error in
                    log.error("Error retrieving Leaderboard Entries: \(error.localizedDescription)")
                    completion(.failure(error))
                    reject(error)
                }

            }
        }

        leaderboardEnriesPromiseQueue.enque(promiseTask: leaderboardEntriesPromise)

    }

    /// Get a leaderboard entry profile
    func getLeaderboardEntry(
        profileID: String,
        leaderboardID: String,
        completion: @escaping (Result<LeaderboardEntry, Error>) -> Void
    ) {
        firstly {
            self.getLeaderboardAndCurrentEntry(leaderboardID: leaderboardID, profileID: profileID)
        }.then { leaderboardAndEntry in
            guard let entryResource = leaderboardAndEntry.currentEntry else {
                completion(.failure(LeaderboardErrors.failedToFindCurrentEntry))
                return
            }
            completion(.success(LeaderboardEntry(entryResource)))
        }.catch { error in
            log.error("Error retrieving Leaderboard Entries: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }

    /// Get a leaderboard entry profile
    func getLeaderboardEntryForCurrentProfile(
        leaderboardID: String,
        completion: @escaping (Result<LeaderboardEntry, Error>) -> Void
    ) {
        firstly {
            userProfileVendor.whenProfileResource
        }.then { userProfile in
            self.getLeaderboardEntry(
                profileID: userProfile.id,
                leaderboardID: leaderboardID,
                completion: completion
            )
        }.catch { error in
            log.error("Error retrieving Leaderboard Entries: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }

    /// Get Leaderboards on which the profile is ranked
    func getProfileLeaderboards(
        profileID: String,
        page: Pagination,
        completion: @escaping (Result<ProfileLeaderboardsResult, Error>) -> Void
    ) {
        let leaderboardEntriesPromise = PromiseTask { () -> Promise<Void> in
            return Promise<Void> { [weak self] fulfill, reject in
                guard let self = self else { return }

                firstly {
                    Promises.zip(
                        self.accessTokenVendor.whenAccessToken,
                        self.coreAPI.whenApplicationConfig
                    )
                }.then { accessToken, applicationConfig -> Promise<PaginatedResult<ProfileLeaderboard>> in

                    // Handle `.next`, `.previous` cases and their availibility
                    var notFirstPageUrl: URL?
                    switch page {
                    case .first:
                        // reset next/prev urls stored from previous calls to a different room
                        self.leaderboardEntriesPagination = PaginationProgress()
                    case .next:
                        guard let nextPageUrl = self.leaderboardEntriesPagination.next else {
                            log.info("Next leaderboard entries page is unavailable")
                            return Promise(value: PaginatedResult(
                                previous: nil,
                                count: self.leaderboardEntriesPagination.total,
                                next: nil,
                                items: []
                            ))
                        }
                        notFirstPageUrl = nextPageUrl
                    case .previous:
                        guard let previousPageUrl = self.leaderboardEntriesPagination.previous else {
                            log.info("Previous leaderboard entries page is unavailable")
                            return Promise(value: PaginatedResult(
                                previous: nil,
                                count: self.leaderboardEntriesPagination.total,
                                next: nil,
                                items: []
                            ))
                        }
                        notFirstPageUrl = previousPageUrl
                    }

                    if let url = notFirstPageUrl {
                        return self.leaderboardAPI.getProfileLeaderboards(url: url, accessToken: accessToken)
                    } else {
                        return self.leaderboardAPI.getProfileLeaderboards(
                            url: try applicationConfig.getProfileLeaderboardsURL(profileID: profileID),
                            accessToken: accessToken
                        )
                    }
                }.then { leaderboardEntriesResult in
                    self.leaderboardEntriesPagination.total = leaderboardEntriesResult.count
                    self.leaderboardEntriesPagination.next = leaderboardEntriesResult.next
                    self.leaderboardEntriesPagination.previous = leaderboardEntriesResult.previous
                    let hasPrevious: Bool = leaderboardEntriesResult.previous != nil
                    let hasNext: Bool = leaderboardEntriesResult.next != nil
                    let leaderboardEntries = leaderboardEntriesResult.items

                    completion(.success(ProfileLeaderboardsResult(
                        entries: leaderboardEntries,
                        total: leaderboardEntriesResult.count,
                        hasPrevious: hasPrevious,
                        hasNext: hasNext
                    )))
                    fulfill(())

                }.catch { error in
                    log.error("Error retrieving Leaderboard Entries: \(error.localizedDescription)")
                    completion(.failure(error))
                    reject(error)
                }

            }
        }
        leaderboardEnriesPromiseQueue.enque(promiseTask: leaderboardEntriesPromise)
    }

    /// Get Leaderboards views on which the profile is ranked
    func getProfileLeaderboardViews(
        profileID: String,
        page: Pagination,
        completion: @escaping (Result<ProfileLeaderboardViewsResult, Error>) -> Void
    ) {
        let leaderboardEntriesPromise = PromiseTask { () -> Promise<Void> in
            return Promise<Void> { [weak self] fulfill, reject in
                guard let self = self else { return }

                firstly {
                    Promises.zip(
                        self.coreAPI.whenApplicationConfig,
                        self.accessTokenVendor.whenAccessToken
                    )
                }.then { applicationConfig, accessToken -> Promise<PaginatedResult<ProfileLeaderboardView>> in

                    // Handle `.next`, `.previous` cases and their availibility
                    var notFirstPageUrl: URL?
                    switch page {
                    case .first:
                        // reset next/prev urls stored from previous calls to a different room
                        self.leaderboardEntriesPagination = PaginationProgress()
                    case .next:
                        guard let nextPageUrl = self.leaderboardEntriesPagination.next else {
                            log.info("Next leaderboard entries page is unavailable")
                            return Promise(value: PaginatedResult(
                                previous: nil,
                                count: self.leaderboardEntriesPagination.total,
                                next: nil,
                                items: []
                            ))
                        }
                        notFirstPageUrl = nextPageUrl
                    case .previous:
                        guard let previousPageUrl = self.leaderboardEntriesPagination.previous else {
                            log.info("Previous leaderboard entries page is unavailable")
                            return Promise(value: PaginatedResult(
                                previous: nil,
                                count: self.leaderboardEntriesPagination.total,
                                next: nil,
                                items: []
                            ))
                        }
                        notFirstPageUrl = previousPageUrl
                    }

                    if let url = notFirstPageUrl {
                        return self.leaderboardAPI.getProfileLeaderboardViews(url: url, accessToken: accessToken)
                    } else {
                        return self.leaderboardAPI.getProfileLeaderboardViews(
                            url: try applicationConfig.getProfileLeaderboardViewsURL(profileID: profileID),
                            accessToken: accessToken
                        )
                    }
                }.then { leaderboardEntriesResult in
                    self.leaderboardEntriesPagination.total = leaderboardEntriesResult.count
                    self.leaderboardEntriesPagination.next = leaderboardEntriesResult.next
                    self.leaderboardEntriesPagination.previous = leaderboardEntriesResult.previous
                    let hasPrevious: Bool = leaderboardEntriesResult.previous != nil
                    let hasNext: Bool = leaderboardEntriesResult.next != nil
                    let leaderboardEntries = leaderboardEntriesResult.items

                    completion(.success(ProfileLeaderboardViewsResult(
                        entries: leaderboardEntries,
                        total: leaderboardEntriesResult.count,
                        hasPrevious: hasPrevious,
                        hasNext: hasNext
                    )))
                    fulfill(())

                }.catch { error in
                    log.error("Error retrieving Leaderboard Entries: \(error.localizedDescription)")
                    completion(.failure(error))
                    reject(error)
                }

            }
        }
        leaderboardEnriesPromiseQueue.enque(promiseTask: leaderboardEntriesPromise)
    }

    // MARK: Redemption Keys

    /// Retrieve all redemption keys associated
    /// with the current user profile
    func getRedemptionKeys(
        page: Pagination,
        options: GetRedemptionKeysRequestOptions? = nil,
        completion: @escaping (Result<[RedemptionKey], Error>) -> Void
    ) {
        redemptionKeyClient.getRedemptionKeys(
            page: page,
            options: options,
            completion: completion
        )
    }

    /// Redeem a redemption key by code
    func redeemKeyBy(
        redemptionCode: String,
        completion: @escaping (Result<RedemptionKey, Error>) -> Void
    ) {
        redemptionKeyClient.redeemKeyBy(
            redemptionCode: redemptionCode,
            completion: completion
        )
    }

    /// Redeem Key by the redemption key ID
    func redeemKeyBy(
        redemptionKeyID: String,
        completion: @escaping (Result<RedemptionKey, Error>) -> Void
    ) {
        redemptionKeyClient.redeemKeyBy(
            redemptionKeyID: redemptionKeyID,
            completion: completion
        )
    }

    /// Get program id by using that programs custom id
    func getProgramID(
        customID: String,
        completion: @escaping (Result<String, Error>) -> Void
    ) {

        firstly {
            return Promises.zip(
                self.coreAPI.whenApplicationConfig,
                self.whenUserProfile
            )
        }.then { applicationConfig, userProfile in
            return self.coreAPI.getProgramID(programURL: try applicationConfig.getProgramCustomIDURL(customID: customID), accessToken: userProfile.accessToken)
        }.then { program in
            completion(.success(program.id))
        }.catch { error in
            log.error("Failed to get program id by using client id")
            completion(.failure(ProgramDetailsError.failedGettingProgram(error: error.localizedDescription)))
        }
    }

    // Get status of the flag for autoclaim of Interaction Rewards
    func getAutoclaimInteractionRewardsStatus(
        completion: @escaping (Result<Bool, Error>) -> Void
    ) {
        firstly {
            self.coreAPI.whenApplicationConfig
        }.then { application in
            completion(.success(application.autoclaimInteractionRewards))
        }.catch { error in
            completion(.failure(error))
        }
    }

    /// Returns pub/sub connection details
    func getPubSubConnectionDetails(completion: @escaping (Result<PubSubConnectionDetails, Error>) -> Void) {
        firstly {
            return Promises.zip(
                self.coreAPI.whenApplicationConfig,
                self.whenUserProfile
            )
        }.then { configuration, userProfile in

            let userID = userProfile.userID.asString
            var connectionDetails: PubSubConnectionDetails?

            if let subscribeKey = configuration.pubnubSubscribeKey {
                connectionDetails = PubSubConnectionDetails(
                    subscribeKey: subscribeKey,
                    origin: configuration.pubnubOrigin,
                    userID: userID,
                    heartbeatTimeout: configuration.pubnubPresenceTimeout,
                    heartbeatInterval: configuration.pubnubHeartbeatInterval
                )
            }
            if let details = connectionDetails {
                completion(.success(details))
            } else {
                completion(.failure(SocketConnectionError.missingSubscriberKey))
            }
        }.catch { error in
            completion(.failure(error))
        }
    }
}

// MARK: - Private APIs

private extension LiveLike {
    var eventRecorder: EventRecorder { return analytics }

    func loadChatRoom(
        config: ChatSessionConfig,
        completion: @escaping (Result<ChatSession, Error>) -> Void
    ) {
        firstly {
            return Promises.zip(
                self.coreAPI.whenApplicationConfig,
                self.whenUserProfile
            )
        }.then { application, userProfile in
            return Promises.zip(
                self.whenPubSubService,
                .init(value: userProfile),
                self.chatAPI.getChatRoomResource(roomID: config.roomID, accessToken: userProfile.accessToken),
                .init(value: application)
            )
        }.then { pubsubService, userProfile, chatRoomResource, application in
            let chatPubnubChannel = chatRoomResource.channels.chat.pubnub
            let controlPubnubChannel = chatRoomResource.channels.control?.pubnub

            let chatId = userProfile.userID.asString
            let imageUploader = ImageUploader(
                uploadUrl: chatRoomResource.uploadUrl,
                networking: LLCore.networking,
                accessToken: userProfile.accessToken
            )

            let reactionVendor = ChatRoomReactionVendor(
                reactionPacksUrl: chatRoomResource.reactionPacksUrl,
                isPubnubEnabled: application.pubnubPublishKey != nil
            )

            let messageReporter = APIMessageReporter(
                reportURL: chatRoomResource.reportMessageUrl,
                accessToken: userProfile.accessToken
            )

            let stickerRepository = StickerRepository(stickerPacksURL: chatRoomResource.stickerPacksUrl)

            let chatRoom: ChatSession = PubSubChatRoom(
                roomID: chatRoomResource.id,
                chatChannel: chatPubnubChannel != nil ? pubsubService?.subscribe(chatPubnubChannel!) : nil,
                controlChannel: controlPubnubChannel != nil ? pubsubService?.subscribe(controlPubnubChannel!) : nil,
                userID: chatId,
                userNickname: self.userNicknameService.currentNickname ?? userProfile.nickname,
                nicknameVendor: self.userNicknameService,
                imageUploader: imageUploader,
                analytics: self.analytics,
                reactionsVendor: reactionVendor,
                messageReporter: messageReporter,
                title: chatRoomResource.title,
                accessToken: userProfile.accessToken,
                chatFilters: Set([.filtered]),
                stickerRepository: stickerRepository,
                shouldDisplayAvatar: config.shouldDisplayAvatar,
                enableChatMessageURLs: config.enableChatMessageURLs,
                chatMessageUrlPatterns: config.chatMessageUrlPatterns,
                chatAPI: self.chatAPI,
                customMessagesUrl: chatRoomResource.customMessagesUrl,
                pinnedMessagesURL: application.pinnedMessagesUrl,
                chatroomMessageURL: chatRoomResource.chatroomMessagesUrl,
                chatroomMessagesCountURL: chatRoomResource.chatroomMessagesCountUrl,
                deleteMessageURL: chatRoomResource.deleteMessageUrl,
                chatClient: self.chat,
                userClient: self.user,
                reactionClient: self.reaction,
                includeFilteredMessages: config.includeFilteredMessages,
                chatRoomEventsURL: application.chatRoomEventsUrl,
                apiPollingInterval: application.apiPollingInterval,
                reactionSpaceID: chatRoomResource.reactionSpaceId,
                networking: LiveLike.networking
            )
            completion(.success(chatRoom))
        }.catch { error in
            completion(.failure(error))
        }
    }

    func contentSessionInternal(config: SessionConfiguration, delegate: ContentSessionDelegate?) -> InternalContentSession {
        let programDetailVendor = ProgramDetailClient(programID: config.programID, applicationVendor: self.coreAPI)

        return InternalContentSession(
            sdkInstance: self,
            config: config,
            whenWidgetClient: whenWidgetClient,
            nicknameVendor: userNicknameService,
            programDetailVendor: programDetailVendor,
            eventRecorder: eventRecorder,
            widgetVotes: predictionVoteRepo,
            leaderboardsManager: self.leaderboardsManager,
            networking: LiveLike.networking,
            delegate: delegate
        )
    }

}

// MARK: - WidgetPauser

extension LiveLike: WidgetPauser {
    private static let permanentPauseUserDefaultsKey = "EngagementSDK.widgetsPausedForAllSessions"

    func setDelegate(_ delegate: PauseDelegate) {
        widgetPauseDelegates.addListener(delegate)
    }

    func removeDelegate(_ delegate: PauseDelegate) {
        widgetPauseDelegates.removeListener(delegate)
    }

    func pauseWidgets() {
        widgetPauseStatus = .paused
    }

    func resumeWidgets() {
        widgetPauseStatus = .unpaused
    }
}

// MARK: - WidgetCrossSessionPauser

extension LiveLike: WidgetCrossSessionPauser {
    /**
     Pauses widgets for all ContentSessions
     This is stored in UserDefaults and will persist on future app launches
     */
    public func pauseWidgetsForAllContentSessions() {
        pauseWidgets()
    }

    /**
     Resumes widgets for all ContentSessions
     This is stored in UserDefaults and will persist on future app launches
     */
    public func resumeWidgetsForAllContentSessions() {
        resumeWidgets()
    }
}

extension LiveLike: InternalErrorDelegate {
    // Repeat errors to the intergrator delegate
    func setupError(_ error: LiveLike.SetupError) {
        delegate?.sdk?(self, setupFailedWithError: error)
    }
}
