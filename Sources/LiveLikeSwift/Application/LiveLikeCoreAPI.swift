//
//  LiveLikeCoreAPI.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 5/20/21.
//

import Foundation
import LiveLikeCore

/// Represents a link registered in the CMS
public struct RegisteredLink: Codable {
    /// Registered Link ID
    public let id: String
    /// The name of the Registered Link
    public let name: String
    /// Description of the Registered Link
    public let description: String?
    /// The URL associated with the Registered Link
    public let linkURL: URL
    /// The client id associated with the Registered Link
    public let clientID: String

    enum CodingKeys: String, CodingKey {
        case id
        case clientID = "clientId"
        case description
        case name
        case linkURL = "linkUrl"
    }
}

protocol LiveLikeCoreAPIProtocol {
    var whenApplicationConfig: Promise<ApplicationConfiguration> { get }
    func getProgramDetail(programID: String) -> Promise<ProgramDetailResource>
    /// Create a new user profile
    func createProfile(profileURL: URL) -> Promise<AccessToken>

    /// Update the nickname of a user profile
    func setNickname(profileURL: URL, nickname: String, accessToken: AccessToken) -> Promise<ProfileResource>

    /// Get a user profile
    func getProfile(profileURL: URL, accessToken: AccessToken) -> Promise<ProfileResource>

    func setCustomData(profileURL: URL, data: String, accessToken: AccessToken) -> Promise<ProfileResource>

    /// Gets the programID
    func getProgramID(programURL: URL, accessToken: AccessToken) -> Promise<ProgramDetailResource>

    func getRedemptionKeys(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<RedemptionKey>>

    func redeemKey(url: URL, accessToken: AccessToken) -> Promise<RedemptionKey>
}

class LiveLikeCoreAPI: LiveLikeCoreAPIProtocol {

    func getProgramDetail(programID: String) -> Promise<ProgramDetailResource> {
        let programDetailVendor = ProgramDetailClient(
            programID: programID,
            applicationVendor: self
        )
        return programDetailVendor.getProgramDetails()
    }

    var whenApplicationConfig: Promise<ApplicationConfiguration>

    private let apiBaseURL: URL
    private let clientID: String

    init(apiBaseURL: URL, clientID: String) {
        self.clientID = clientID
        self.apiBaseURL = apiBaseURL

        self.whenApplicationConfig = {
            let url = apiBaseURL.appendingPathComponent("applications").appendingPathComponent(clientID)
            let resource = Resource<ApplicationConfiguration>(get: url)
            return LiveLike.networking.load(resource)
        }()
    }

    func createProfile(profileURL: URL) -> Promise<AccessToken> {
        struct AccessTokenResource: Decodable {
            let accessToken: String
        }
        let resource = Resource<AccessTokenResource>(
            url: profileURL,
            method: .post(EmptyBody())
        )
        return firstly {
            LiveLike.networking.load(resource)
        }.then {
            AccessToken(fromString: $0.accessToken)
        }
    }

    func setNickname(profileURL: URL, nickname: String, accessToken: AccessToken) -> Promise<ProfileResource> {
        struct NicknamePatchBody: Encodable {
            let nickname: String
        }

        let body = NicknamePatchBody(nickname: nickname)
        let resource = Resource<ProfileResource>(
            url: profileURL,
            method: .patch(body),
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func getProfile(profileURL: URL, accessToken: AccessToken) -> Promise<ProfileResource> {
        let resource = Resource<ProfileResource>(
            get: profileURL,
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func setCustomData(profileURL: URL, data: String, accessToken: AccessToken) -> Promise<ProfileResource> {
        struct CustomDataPatchBody: Encodable {
            let customData: String
        }
        let body = CustomDataPatchBody(customData: data)
        let resource = Resource<ProfileResource>(
            url: profileURL,
            method: .patch(body),
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func getProgramID(programURL: URL, accessToken: AccessToken) -> Promise<ProgramDetailResource> {
        let resource = Resource<ProgramDetailResource>(
            get: programURL,
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func getRedemptionKeys(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<RedemptionKey>> {
        let resource = Resource<PaginatedResult<RedemptionKey>>(
            get: url,
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func redeemKey(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<RedemptionKey> {

        struct RedeemCodePatchBody: Encodable {
            let status: RedemptionKeyStatus
        }

        let body = RedeemCodePatchBody(status: .redeemed)
        let resource = Resource<RedemptionKey>(
            url: url,
            method: .patch(body),
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }
}
