//
//  LiveLikeErrors.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 7/29/20.
//

import Foundation

public enum SendMessageError: LocalizedError {
    case accessDenied
    case userMuted

    public var errorDescription: String? {
        switch self {
        case .accessDenied:
            return "Sending Message Failed - Access Denied"
        case .userMuted:
            return "forbidden"
        }
    }
}

// MARK: - CHAT
enum PubNubChannelError: LocalizedError {
    case failedToSerializeHistoryToJsonData
    case pubnubStatusError(errorStatus: Error)
    case foundNoResultsFromHistoryRequest
    case foundNoResultsForChannel(channel: String)
    case messageActionAlreadyAdded
    case messageActionDoesntExist
    case failedToSendMessageAction
    case failedToRemoveMessageAction
    case expectedPubSubIDInternalToBeNSNumber
    case foundNoAction
    case noMessageCountResult
    case noMessageCountForChannel(channel: String)

    var errorDescription: String? {
        switch self {
        case .failedToSerializeHistoryToJsonData:
            return "Failed to serialize the PubNub history result as json data."
        case .pubnubStatusError(let errorStatus):
            return errorStatus.localizedDescription
        case .foundNoResultsFromHistoryRequest:
            return "Failed to find results for history request."
        case .foundNoResultsForChannel(let channel):
            return "Didn't find any results for channel \(channel)"
        case .messageActionAlreadyAdded:
            return "Failed to send the message action because it already exists."
        case .messageActionDoesntExist:
            return "Failed to remove the message action because it doesn't exist."
        case .failedToSendMessageAction:
            return "Failed to send the message action."
        case .failedToRemoveMessageAction:
            return "Failed to remove the message action."
        case .expectedPubSubIDInternalToBeNSNumber:
            return "Expected the pub sub id internal to be type NSNumber."
        case .foundNoAction:
            return "Failed to receive an action"
        case .noMessageCountResult:
            return "Message count request returned nil result."
        case .noMessageCountForChannel(let channel):
            return "Message count for channel \(channel) not found in dictionary."
        }
    }
}

enum PubNubServiceError: LocalizedError {
    case failedToParseHistoryResultAsJsonData
    case pubnubStatusError(errorStatus: Error)

    var errorDescription: String? {
        switch self {
        case .failedToParseHistoryResultAsJsonData:
            return "Failed to parse the history result as json data."
        case .pubnubStatusError(let errorStatus):
            return errorStatus.localizedDescription
        }
    }
}

enum ProgramChatReactionVendorError: LocalizedError {
    case invalidReactionPacksURL

    var errorDescription: String? {
        switch self {
        case .invalidReactionPacksURL:
            return "Invalid Reaction Packs URL"
        }
    }
}

enum PubSubChatRoomError: LocalizedError {
    case invalidUserChatRoomImageUrl
    case failedToEncodeChatMessage
    case sendMessageFailedNoNickname
    case failedToFindPubSubID(messageID: ChatMessageID)
    case reactionIdInternalNotPubSubID
    case failedToFindChatMessageForPubSubID(pubsubID: String)
    case failedToFindReportedChatMessage(messageId: String)
    case promiseRejectedDueToNilSelf
    case failedToSendImageDueToMissingData
    case failedDueToMissingMessageReporter
    case reactionSessionMissing

    var errorDescription: String? {
        switch self {
        case .invalidUserChatRoomImageUrl:
            return "The user chat image url provided is not valid"
        case .failedToEncodeChatMessage:
            return "The SDK failed to decode the chat message to json."
        case .sendMessageFailedNoNickname:
            return "The SDK failed to send the message because there is no user nickname set."
        case .failedToFindPubSubID(let messageID):
            return "Failed to find PubSubID for message with id: \(messageID)"
        case .reactionIdInternalNotPubSubID:
            return "Failed because internal id of reaction is not of type PubSubID"
        case .failedToFindChatMessageForPubSubID(let pubsubID):
            return "Failed to find the ChatMessageType for pubsub message with id: \(pubsubID)"
        case .failedToFindReportedChatMessage(let messageId):
            return "Failed to find message that is being reported with id: \(messageId)"
        case .promiseRejectedDueToNilSelf:
            return "Promise rejected due to self being nil"
        case .failedToSendImageDueToMissingData:
            return "Failed to send image message because expected imageData cannot be retrieved from cache."
        case .failedDueToMissingMessageReporter:
            return "Failed to report a message because the message reporter has not been found"
        case .reactionSessionMissing:
            return "Reaction Session attached to the chatroom hasn't been initialized"
        }
    }
}

enum PubSubChatRoomDecodeError: LocalizedError {
    case failedToDecodePayload(decodingError: String)
    case missingPayload

    var errorDescription: String? {
        switch self {
        case .failedToDecodePayload(let decodingError):
            return "\(decodingError)"
        case .missingPayload:
            return "'payload' is missing from PubSubChatEventWithPayload"
        }
    }

}

enum PubSubUserProfileDecodeError: LocalizedError {
    case failedToDecodePayload(decodingError: String)
    case missingPayload

    var errorDescription: String? {
        switch self {
        case .failedToDecodePayload(let decodingError):
            return "\(decodingError)"
        case .missingPayload:
            return "'payload' is missing from PubSubUserProfileEventWithPayload"
        }
    }

}

enum CreateChatRoomResourceError: Swift.Error, LocalizedError {
    case failedCreatingChatRoomUrl
    var errorDescription: String? { return "A url for creating chat rooms is corrupt" }
}

// MARK: - WIDGET
enum GetWidgetError: Swift.Error, LocalizedError {
    case widgetDoesNotExist
    case failedBuildingGetWidgetsURL(error: String)

    var errorDescription: String? {
        switch self {
        case .failedBuildingGetWidgetsURL(let error):
            return "Failed building getWidgets URL: \(error)"
        case .widgetDoesNotExist:
            return "Widget does not exist"
        }
    }
}

enum GetWidgetAPIServicesError: Swift.Error, LocalizedError {
    case widgetUrlIsCorrupt
    var errorDescription: String? { return "Widget API url is corrupt" }
}

enum MessagingClientError: Swift.Error, LocalizedError {
    case invalidEvent(event: String)
    var errorDescription: String? {
        switch self {
        case let .invalidEvent(event):
            return event
        }
    }
}

enum PredictionFollowUpModelErrors: LocalizedError {
    case couldNotFindVote
    case concurrentGetVote
    case claimTokenNotFound
}

enum PollWidgetModelError: Swift.Error, LocalizedError {
    case voteAlreadySubmitted
    case failedDueToInvalidOptionID
    case cannotUpdateVote
    var errorDescription: String? {
        switch self {
        case .voteAlreadySubmitted:
            return "Poll vote already submitted"
        case .failedDueToInvalidOptionID:
            return "Vote failed due to invalid Option ID"
        case .cannotUpdateVote:
            return "Cannot update a vote from previous app session"
        }
    }
}

enum WidgetInteractionErrors: LocalizedError {
    case invalidUnclaimedInteractionURLTemplate
    case invalidInteractionsURLTemplate
    case invalidUnclaimedInteractionURL
    case missingInteractionsURL(id: String, kind: WidgetKind)
    case unexpectedInteractionForWidgetKind(kind: WidgetKind)

    var errorDescription: String? {
        switch self {
        case .invalidUnclaimedInteractionURLTemplate:
            return "Failed to find string to replace in widgetInteractionsURLTemplate"
        case .invalidInteractionsURLTemplate:
            return "Failed to find string to replace in widgetInteractionsURLTemplate"
        case .invalidUnclaimedInteractionURL:
            return "Failed to create UnclaimedWidgetInteractionsURL"
        case let .missingInteractionsURL(id, kind):
            return "Failed to find user interactions for widget with id(\(id)) and kind(\(kind)) because the interactionsURL is nil"
        case let .unexpectedInteractionForWidgetKind(kind):
            return "Found unexpected interaction for a widget with kind \(kind)"
        }
    }
}

// MARK: - GAMIFICATION
enum LLGamificationError: LocalizedError {
    case noNextLeaderboardEntries
    case noPreviousLeaderboardEntries
    case leaderboardUrlCreationFailure
    case leaderboardDetailUrlCorrupt

    var errorDescription: String? {
        switch self {
        case .noNextLeaderboardEntries:
            return "Next leaderboard entries page is unavailable"
        case .noPreviousLeaderboardEntries:
            return "Previous leaderboard entries page is unavailable"
        case .leaderboardUrlCreationFailure:
            return "Leaderboard URL failed to be created"
        case .leaderboardDetailUrlCorrupt:
            return "Leaderboard Detail URL is corrupt"
        }
    }
}

enum BadgeError: LocalizedError {
    case getBadgesByProfileFailure(error: String)
    case invalidUserProfileDetailURL
    case getApplicationBadgesFailure(error: String)
    case getBadgesProgressFailure(error: String)
    case getBadgeProfilesFailure(error: String)

    var errorDescription: String? {
        switch self {
        case .getBadgesByProfileFailure(let error):
            return "Profile Badges retrieval failed - \(error)"
        case .invalidUserProfileDetailURL:
            return "Profile Detail URL is not valid, badges cannot be retrieved"
        case .getApplicationBadgesFailure(error: let error):
            return "Application Badges retrieval failed - \(error)"
        case .getBadgesProgressFailure(error: let error):
            return "Badge Progress retrieval failed - \(error)"
        case .getBadgeProfilesFailure(error: let error):
            return "Profiles for Badge retrieval failed - \(error)"
        }
    }
}

enum RedemptionError: LocalizedError {
    case invalidRedemptionKeyDetailURL
    case failedRedeemingKey(error: String)
    case failedGettingRedemptionKeys(error: String)
    case failedBuilgindGetRedemptionKeysURL(error: String)

    var errorDescription: String? {
        switch self {
        case .invalidRedemptionKeyDetailURL:
            return "Redemption Keys URL is not valid"
        case .failedRedeemingKey(error: let error):
            return "Failed Redeeming Key due to error - \(error)"
        case .failedGettingRedemptionKeys(error: let error):
            return "Failed getting redemption keys due to error - \(error)"
        case .failedBuilgindGetRedemptionKeysURL(error: let error):
            return "Failed getting redemption keys url due to error - \(error)"
        }
    }
}

enum LeaderboardErrors: LocalizedError {
    case failedToFindCurrentEntry
    case failedBuildingLeaderboardEntriesURL(error: String)
    case failedRetrievingLeaderboardEntries(error: String)

}

enum RewardsClientError: LocalizedError {
    case failedGettingApplicationRewardItems(error: String)
    case failedTransferingRewardItemAmount(error: String)
    case failedBuildingRewardItemBalancesURL(error: String)
    case failedGettingRewardItemBalances(error: String)
    case failedGettingRewardItemTransfers(error: String)
    case failedBuildingRewardItemTransfersURL(error: String)
    case failedBuildingRewardTransactionsURL(error: String)

    var errorDescription: String? {
        switch self {
        case .failedGettingApplicationRewardItems(let error):
            return "Failed getting all rewards due to the following error: \(error)"
        case .failedTransferingRewardItemAmount(let error):
            return "Failed transfering reward item amount due to the following error: \(error)"
        case .failedBuildingRewardItemBalancesURL(let error):
            return "Failed building reward item balances URL due to the following error: \(error)"
        case .failedGettingRewardItemBalances(let error):
            return "Failed getting reward item balances due to the following error: \(error)"
        case .failedGettingRewardItemTransfers(let error):
            return "Failed getting reward item transfers due to the following error: \(error)"
        case .failedBuildingRewardItemTransfersURL(let error):
            return "Failed building reward item transfers URL due to the following error: \(error)"
        case .failedBuildingRewardTransactionsURL(let error):
            return "Failed building reward transactions URL due to the following error: \(error)"
        }
    }
}

let questsClientErrorPrefix: String = "Quests Client Error: "
enum QuestsClientError: LocalizedError {

    case questsClientError(error: String)
    case failedGettingQuests(error: String)
    case failedGettingUserQuests(error: String)
    case failedStartingUserQuest(error: String)
    case failedUpdatingUserQuestTasks(error: String)

    var errorDescription: String? {
        switch self {
        case .questsClientError(error: let error):
            return "\(error)"
        case .failedGettingQuests(error: let error):
            return "\(questsClientErrorPrefix) Failed getting Quests due to \(error)"
        case .failedGettingUserQuests(error: let error):
            return "\(questsClientErrorPrefix) Failed getting User Quests due to \(error)"
        case .failedStartingUserQuest(error: let error):
            return "\(questsClientErrorPrefix) Starting a User Quest failed due to \(error)"
        case .failedUpdatingUserQuestTasks(error: let error):
            return "\(questsClientErrorPrefix) Updating user quest tasks failed due to \(error)"
        }
    }
}

// MARK: - NETWORK
enum ImageUploaderError: LocalizedError {
    case failedToGetJPEG
    case noDataInResponse
    case failedToDecodeDataAsImageResource
}

enum ProgramDetailsError: Swift.Error, LocalizedError {
    case invalidURL(String)
    case failedGettingProgram(error: String)

    var errorDescription: String? {
        switch self {
        case .invalidURL(let urlString):
            return "Failed to construct the program url \(urlString). Check that the template and program id are valid."
        case .failedGettingProgram(error: let error):
            return "Failed to get program due to \(error)."
        }
    }
}

enum SetUserDisplayNameError: Swift.Error, LocalizedError {
    case invalidNameLength
    var errorDescription: String? { return "Display names must be between 1 and 20 characters" }
}

enum UIImageDownloadError: Error {
    case error(description: String)
}

enum StickerRepositoryError: Error {
    case invalidURL
    case unknownStickerID
}

enum SocketConnectionError: Error {
    case missingSubscriberKey
}

// MARK: - SPONSOR
enum SponsorError: LocalizedError {
    case getSponsorFailed(error: String)

    var errorDescription: String? {
        switch self {
        case .getSponsorFailed(let error):
            return "Sponsor retrieval failed - \(error)"
        }
    }
}

// MARK: - MISC

enum AnalyticsError: LocalizedError {
    case noTokenProvidedForMixpanel

    var errorDescription: String? {
        switch self {
        case .noTokenProvidedForMixpanel:
            return "Will not initialize Mixpanel because no token available."
        }
    }
}

enum ContentSessionError: LocalizedError {
    case invalidChatRoomURLTemplate
    case invalidChatroomTokenGateAccessURLTemplate
    case invalidChatroomTokenGateAccessURL
    case invalidChatRoomInvitationURLTemplate
    case invalidChatRoomURL
    case invalidUserMutedStatusURLTemplate
    case invalidUserMutedStatusURL
    case missingChatService
    case missingChatRoomResourceFields
    case failedLoadingInitialChat
    case missingWidgetClient
    case missingSubscribeChannel
    case failedToFindUserNickname
    case missingChatRoom(placeOfError: String)
    case missingPubNubSubscribeKeyForChat
    case failedToCreateWidgetModelMismatchedProgramID(widgetProgramID: String, sessionProgramID: String)
    case defaultChatRoomNotFound

    var errorDescription: String? {
        switch self {
        case .invalidChatRoomURLTemplate:
            return "The template provided to build a chat room url is invalid or incompatible. Expected replaceable string of '{chat_room_id}'."
        case .invalidChatroomTokenGateAccessURLTemplate:
            return "The template provided to build a chat room token gate access url is invalid or incompatible. Expected replaceable string of '{wallet_address}'."
        case .invalidChatroomTokenGateAccessURL:
            return "The chat room token gate access url is not a valid URL."
        case .invalidChatRoomInvitationURLTemplate:
            return "The template provided to build a chat room url is invalid or incompatible. Expected replaceable string of '{profile_id}'."
        case .invalidChatRoomURL:
            return "The chat room resource url is not a valid URL."
        case .missingChatRoomResourceFields:
            return "Failed to initalize Chat because of missing required fields on the chat room resource."
        case .missingChatService:
            return "Failed to initialize Chat because the service is missing."
        case .missingWidgetClient:
            return "Failed creating Widget Queue due to a missing Widget Client"
        case .missingSubscribeChannel:
            return "Failed creating Widget Queue due to a missing Subscribe Channel"
        case .missingChatRoom(let placeOfError):
            return "Failed \(placeOfError) due to a missing Chat Room"
        case .failedLoadingInitialChat:
            return "Failed loading initial history for chat"
        case .failedToCreateWidgetModelMismatchedProgramID(let widgetProgramID, let sessionProgramID):
            return "Failed to create the Widget Model because the program id of the widget (\(widgetProgramID)) doesn't match the Content Session (\(sessionProgramID))"
        case .invalidUserMutedStatusURLTemplate:
            return "The template provided to build user muted status url is invalid or incompatible. Expected replaceable string of '{profile_id}'."
        case .invalidUserMutedStatusURL:
            return "The user muted status resource url is not a valid URL."
        case .failedToFindUserNickname:
            return "Failed to initialize Content Session because user nickname could not be found."
        case .missingPubNubSubscribeKeyForChat:
            return "Chat Service failed to initialize due to missing subscribe key"
        case .defaultChatRoomNotFound:
            return "This program does not have an associated defaultChatRoom."
        }
    }
}

enum ChatClientError: LocalizedError {
    case invalidBlockProfileURLTemplate
    case decodingFailedForBlockInfo

    var errorDescription: String? {
        switch self {
        case .invalidBlockProfileURLTemplate:
            return "The template provided to build a block profile url is invalid or incompatible. Expected replaceable string of '{}'."
        case .decodingFailedForBlockInfo:
            return "Couldn't decode the JSON Object received for BlockInfo. Missing parameter key to decode."
        }
    }
}

enum UserProfileError: Error {
    case failedToFindNickname
}

enum SessionError: Error, LocalizedError {
    case messagingClientsNotConfigured
    case invalidSessionStatus(SessionStatus)

    var errorDescription: String? {
        switch self {
        case .messagingClientsNotConfigured:
            return "No messaging clients have been configured"
        case let .invalidSessionStatus(status):
            return "Could not complete request. Invalid session status \(status)"
        }
    }
}

enum ReactionClientError: Error {
    case invalidURLTemplate
    case invalidReactionSpaceURL
    case invalidUserReactionURL

    var errorDescription: String? {
        switch self {
        case .invalidURLTemplate:
            return "The template provided to build the url is invalid or incompatible. Expected replaceable string of '{}'."
        case .invalidReactionSpaceURL:
            return "The reaction space url is not a valid URL."
        case .invalidUserReactionURL:
            return "The user reaction url is not a valid URL."
        }
    }
}

enum CommentBoardClientError: Error {
    case invalidURLTemplate
    case invalidCommentBoardURL

    var errorDescription: String? {
        switch self {
        case .invalidURLTemplate:
            return "The template provided to build the url is invalid or incompatible. Expected replaceable string of '{}'."
        case .invalidCommentBoardURL:
            return "The comment board url is not a valid URL."
        }
    }
}

enum CommentClientError: Error {
    case invalidCommentURL

    var errorDescription: String? {
        switch self {
        case .invalidCommentURL:
            return "The comment board url is not a valid URL."
        }
    }
}

// MARK: - SOCIAL GRAPH
enum SocialGraphClientError: Error {
    case profileRelationshipIDRequiredForDetailsRequest
    case profileRelationshipIDRequiredForDeletionRequest

    var errorDescription: String? {
        switch self {
        case .profileRelationshipIDRequiredForDetailsRequest:
            return "A profile relationship ID is required to get profile relationship details"
        case .profileRelationshipIDRequiredForDeletionRequest:
            return "A profile relationship ID is required to delete a relationship"
        }
    }
}
