//
//  AccessToken.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

struct AccessToken {
    private let internalToken: String

    init(fromString token: String) {
        internalToken = token
    }

    var asString: String {
        return internalToken
    }
}
