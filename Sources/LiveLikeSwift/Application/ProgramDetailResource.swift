//
//  ProgramDetail.swift
//  LiveLikeSDK
//
//  Created by Cory Sullivan on 2019-02-03.
//

import Foundation
import LiveLikeCore

struct ProgramsResource: Decodable {
    let results: [ProgramDetailResource]
}

struct ProgramDetailResource: Decodable {
    init(
        id: String,
        title: String,
        widgetsEnabled: Bool,
        chatEnabled: Bool,
        subscribeChannel: String,
        pubnubEnabled: Bool,
        syncSessionsUrl: URL,
        rankUrl: URL,
        reactionPacksUrl: URL?,
        defaultChatRoom: ChatRoomResource?,
        timelineUrl: URL,
        widgetsUrl: URL,
        leaderboards: [Leaderboard],
        rewardItems: [RewardItem],
        sponsors: [Sponsor],
        streamUrl: String?,
        unclaimedWidgetInteractionsUrlTemplate: String,
        widgetInteractionsUrlTemplate: String
    ) {
        self.id = id
        self.title = title
        self.widgetsEnabled = widgetsEnabled
        self.chatEnabled = chatEnabled
        self.subscribeChannel = subscribeChannel
        self.pubnubEnabled = pubnubEnabled
        self.syncSessionsUrl = syncSessionsUrl
        self.rankUrl = rankUrl
        self.reactionPacksUrl = reactionPacksUrl
        self.defaultChatRoom = defaultChatRoom
        self.timelineUrl = timelineUrl
        self.widgetsUrl = widgetsUrl
        self.leaderboards = leaderboards
        self.rewardItems = rewardItems
        self.sponsors = sponsors
        self.streamUrl = streamUrl
        self.unclaimedWidgetInteractionsUrlTemplate = unclaimedWidgetInteractionsUrlTemplate
        self.widgetInteractionsUrlTemplate = widgetInteractionsUrlTemplate
    }

    enum Errors: LocalizedError {
        case pubnubNotEnabled
    }

    let id: String
    let title: String
    let widgetsEnabled: Bool
    let chatEnabled: Bool
    private let subscribeChannel: String
    private let pubnubEnabled: Bool
    let syncSessionsUrl: URL
    let rankUrl: URL
    let reactionPacksUrl: URL?
    let defaultChatRoom: ChatRoomResource?
    let timelineUrl: URL
    let widgetsUrl: URL
    let leaderboards: [Leaderboard]
    let rewardItems: [RewardItem]
    let sponsors: [Sponsor]

    /// Exclusively for CMS and Demo use
    let streamUrl: String?

    let unclaimedWidgetInteractionsUrlTemplate: String
    func getUnclaimedWidgetInteractionsURL(profileID: String) throws -> URL {
        let stringToReplace = "{profile_id}"
        guard unclaimedWidgetInteractionsUrlTemplate.contains(stringToReplace) else {
            log.error("Failed to find string to replace in widgetInteractionsURLTemplate")
            throw WidgetInteractionErrors.invalidUnclaimedInteractionURLTemplate
        }
        let urlTemplateFilled = unclaimedWidgetInteractionsUrlTemplate.replacingOccurrences(
            of: stringToReplace,
            with: profileID
        )
        guard let url = URL(string: urlTemplateFilled) else {
            throw WidgetInteractionErrors.invalidUnclaimedInteractionURL
        }
        return url
    }

    let widgetInteractionsUrlTemplate: String
    func getWidgetInteractionsURL(profileID: String, options: GetWidgetInteractionsRequestOptions) throws -> URL {
        let stringToReplace = "{profile_id}"
        guard widgetInteractionsUrlTemplate.contains(stringToReplace) else {
            log.error("Failed to find string to replace in widgetInteractionsURLTemplate")
            throw WidgetInteractionErrors.invalidInteractionsURLTemplate
        }
        let urlTemplateFilled = widgetInteractionsUrlTemplate.replacingOccurrences(
            of: stringToReplace,
            with: profileID
        )
        guard var getInteractionsURl = URL(string: urlTemplateFilled) else {
            throw WidgetInteractionErrors.invalidUnclaimedInteractionURL
        }

        if var urlComponents = URLComponents(
            url: getInteractionsURl,
            resolvingAgainstBaseURL: false
        ) {
            urlComponents.queryItems = options.urlQueries
            if let url = urlComponents.url {
                getInteractionsURl = url
            }
        }
        return getInteractionsURl
    }

    /// Return the subscribe channel for
    func getWidgetPublishChannel() -> String? {
        if pubnubEnabled {
            return subscribeChannel
        } else {
            log.verbose("No widget publishing channel because PubNub is not enabled")
            return nil
        }
    }
}
