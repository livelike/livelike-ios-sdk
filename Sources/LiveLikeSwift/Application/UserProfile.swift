//
//  UserProfile.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 9/2/20.
//

import Foundation

/// Methods for managing changes to a User's profile.
protocol UserProfileDelegate: AnyObject {
}

protocol UserProfileProtocol {
    var userID: LiveLikeID { get }
    var accessToken: AccessToken { get }
    var nickname: String { get }
    var customData: String? { get }
}

/// Methods for managing a User's profile.
public class UserProfile: UserProfileProtocol {

    weak var delegate: UserProfileDelegate?

    let userID: LiveLikeID
    let accessToken: AccessToken
    public let nickname: String
    public let customData: String?

    init(userID: LiveLikeID, accessToken: AccessToken, nickname: String, customData: String?) {
        self.userID = userID
        self.accessToken = accessToken
        self.nickname = nickname
        self.customData = customData
    }
}
