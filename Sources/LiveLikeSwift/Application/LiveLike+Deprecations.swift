//
//  EngagementSDK+Deprecations.swift
//  EngagementSDK
//
//  Created by Xavi Matos on 9/5/19.
//

import Foundation

public extension LiveLike {
    /**
     - Parameter clientID: Find more information regarding the Client ID in our [Basic Integration documentation](https://docs.livelike.com/ios/index.html#initialization)
     - Parameter livelikeAccessToken: A token generated by LiveLike that lets the EngagementSDK identify the user
     */
    @objc
    @available(*, deprecated, message: "Replaced by EngagementSDK(clientID:accessTokenStorage:), using this method will cause the SDK to use UserDefaults for storing the user access token, and override any existing value with the given `livelikeAccessToken` parameter.")
    convenience init(clientID: String, livelikeAccessToken: String) {
        let storage = UserDefaultsAccessTokenStorage()
        storage.storeAccessToken(accessToken: livelikeAccessToken)
        
        var config = LiveLikeConfig(clientID: clientID)
        config.accessTokenStorage = storage
        self.init(config: config)
    }
    
    /**
     - Parameter clientID: Find more information regarding the Client ID in our [Basic Integration documentation](https://docs.livelike.com/ios/index.html#initialization)
     - Note: This initializer will cause the SDK to use UserDefaults for storing the user access token.
     */
    @objc
    @available(*, deprecated, message: "This method is replaced by init(config:).")
    convenience init(clientID: String) {
        let config = LiveLikeConfig(clientID: clientID)
        self.init(config: config)
    }
    
    /**
     - Parameter clientID: Find more information regarding the Client ID in our [Basic Integration documentation](https://docs.livelike.com/ios/index.html#initialization)
     - Parameter accessTokenStorage: An object which the EngagementSDK uses to check for a stored access token between sessions, as well as to inform the storage of a newly generated token.
     */
    @objc
    @available(*, deprecated, message: "This method is replaced by init(config:).")
    convenience init(clientID: String, accessTokenStorage: AccessTokenStorage) {
        var config = LiveLikeConfig(clientID: clientID)
        config.accessTokenStorage = accessTokenStorage
        self.init(config: config)
    }
    
}
