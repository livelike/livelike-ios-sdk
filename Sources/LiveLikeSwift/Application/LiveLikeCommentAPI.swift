//
//  LiveLikeCommentAPI.swift
//  EngagementSDK
//
//  Created by Keval Shah on 04/12/22.
//

import Foundation
import LiveLikeCore

protocol LiveLikeCommentAPIProtocol {

    func createCommentBoard(
        commentBoardURL: URL,
        options: CreateCommentBoardRequestOptions,
        accessToken: AccessToken
    ) -> Promise<CommentBoard>

    func getCommentBoards(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<CommentBoard>>

    func updateCommentBoard(
        commentBoardsURL: URL,
        options: UpdateCommentBoardRequestOptions,
        accessToken: AccessToken
    ) -> Promise<CommentBoard>

    func deleteCommentBoard(
        commentBoardURL: URL,
        accessToken: AccessToken
    ) -> Promise<Bool>

    func getCommentBoardInfo(
        commentBoardURL: URL,
        accessToken: AccessToken
    ) -> Promise<CommentBoard>

    func addComment(
        commentsURL: URL,
        commentBoardID: String,
        text: String,
        parentCommentID: String?,
        accessToken: AccessToken
    ) -> Promise<Comment>

    func deleteComment(
        commentURL: URL,
        accessToken: AccessToken
    ) -> Promise<Bool>

    func editComment(
        commentURL: URL,
        text: String,
        accessToken: AccessToken
    ) -> Promise<Comment>

    func getComments(
        commentsURL: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<Comment>>
}

class LiveLikeCommentAPI: LiveLikeCommentAPIProtocol {

    private let coreAPI: LiveLikeCoreAPIProtocol
    private let networking: LLNetworking

    init(
        coreAPI: LiveLikeCoreAPIProtocol,
        networking: LLNetworking
    ) {
        self.coreAPI = coreAPI
        self.networking = networking
    }

    func createCommentBoard(
        commentBoardURL: URL,
        options: CreateCommentBoardRequestOptions,
        accessToken: AccessToken
    ) -> Promise<CommentBoard> {
        struct Payload: Encodable {
            let title: String?
            let customId: String?
            let allowComments: Bool
            let repliesDepth: Int
        }
        let payload = Payload(
            title: options.title,
            customId: options.customIdentifier,
            allowComments: options.allowComments,
            repliesDepth: options.repliesDepth
        )
        let resource = Resource<CommentBoard>.init(
            url: commentBoardURL,
            method: .post(payload),
            accessToken: accessToken.asString
        )
        return networking.load(resource)
    }


    func updateCommentBoard(
        commentBoardsURL: URL,
        options: UpdateCommentBoardRequestOptions,
        accessToken: AccessToken
    ) -> Promise<CommentBoard> {
        return firstly {
            struct Payload: Encodable {
                let title: String?
                let customId: String?
                let allowComments: Bool?
                let repliesDepth: Int?
            }
            let payload = Payload(
                title: options.title,
                customId: options.customID,
                allowComments: options.allowComments,
                repliesDepth: options.repliesDepth
            )

            let resource = Resource<CommentBoard>(
                url: commentBoardsURL,
                method: .patch(payload),
                accessToken: accessToken.asString
            )
            return networking.load(resource)
        }.then { updatedCommentBoard in
            return Promise(value: updatedCommentBoard)
        }
    }

    func deleteCommentBoard(
        commentBoardURL: URL,
        accessToken: AccessToken
    ) -> Promise<Bool> {
        return firstly {
            let resource = Resource<Bool>(
                url: commentBoardURL,
                method: .delete(EmptyBody()),
                accessToken: accessToken.asString
            )
            return networking.load(resource)
        }.then { result in
            return Promise(value: result)
        }
    }

    func getCommentBoards(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<CommentBoard>> {
        return firstly { () -> Promise<PaginatedResult<CommentBoard>> in
            let resource = Resource<PaginatedResult<CommentBoard>>.init(
                get: url,
                accessToken: accessToken.asString
            )
            return networking.load(resource)
        }.then { commentBoardsList in
            return Promise(value: commentBoardsList)
        }
    }

    func getCommentBoardInfo(
        commentBoardURL: URL,
        accessToken: AccessToken
    ) -> Promise<CommentBoard> {
        let resource = Resource<CommentBoard>(
            get: commentBoardURL,
            accessToken: accessToken.asString
        )
        return networking.load(resource)
    }

    func addComment(
        commentsURL: URL,
        commentBoardID: String,
        text: String,
        parentCommentID: String?,
        accessToken: AccessToken
    ) -> Promise<Comment> {
        struct Payload: Encodable {
            let commentBoardId: String
            let text: String
            let parentCommentId: String?
        }

        let payload = Payload(
            commentBoardId: commentBoardID,
            text: text,
            parentCommentId: parentCommentID
        )
        let resource = Resource<Comment>(
            url: commentsURL,
            method: .post(payload),
            accessToken: accessToken.asString
        )
        return networking.load(resource)
    }

    func deleteComment(
        commentURL: URL,
        accessToken: AccessToken
    ) -> Promise<Bool> {
        return firstly {
            let resource = Resource<Bool>(
                url: commentURL,
                method: .delete(EmptyBody()),
                accessToken: accessToken.asString
            )
            return networking.load(resource)
        }.then { result in
            return Promise(value: result)
        }
    }

    func editComment(
        commentURL: URL,
        text: String,
        accessToken: AccessToken
    ) -> Promise<Comment> {
        return firstly {
            struct Payload: Encodable {
                let text: String
            }
            let payload = Payload(
                text: text
            )

            let resource = Resource<Comment>(
                url: commentURL,
                method: .patch(payload),
                accessToken: accessToken.asString
            )
            return networking.load(resource)
        }.then { updatedComment in
            return Promise(value: updatedComment)
        }
    }

    func getComments(
        commentsURL: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<Comment>> {
        return firstly { () -> Promise<PaginatedResult<Comment>> in
            let resource = Resource<PaginatedResult<Comment>>.init(
                get: commentsURL,
                accessToken: accessToken.asString
            )
            return networking.load(resource)
        }.then { commentsList in
            return Promise(value: commentsList)
        }
    }
}

// MARK: - Models
enum CommentBoardQueryKeys: String {
    case clientID = "client_id"
    case profileID = "profile_id"
    case commentBoardID = "comment_board_id"
}


// MARK: - Models
enum CommentQueryKeys: String {
    case commentBoardID = "comment_board_id"
    case parentCommentID = "parent_comment_id"
}
