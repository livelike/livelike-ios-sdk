//
//  ProfileVendor.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 4/5/22.
//

import Foundation
import LiveLikeCore

/// Vends cached versions of ProfileResource, LiveLikeID, and Nickname
/// Also handles and raises nicknameDidChange event
class ProfileVendor: UserProfileVendor, LiveLikeIDVendor, UserNicknameService, UserCustomDataService {

    lazy var whenInitialNickname: Promise<Void> = {
        firstly {
            whenProfileResource
        }.then { [weak self] profileResource in
            self?.currentNickname = profileResource.nickname
        }.asVoid()
    }()

    lazy var whenInitialCustomData: Promise<Void> = {
        return firstly {
            whenProfileResource
        }.then { [weak self] profileResource in
            self?.currentCustomData = profileResource.customData
        }.asVoid()
    }()

    var whenLiveLikeID: Promise<LiveLikeID> {
        firstly {
            whenProfileResource
        }.then {
            return LiveLikeID(from: $0.id)
        }
    }

    lazy var whenProfileResource: Promise<ProfileResource> = {
        firstly {
            Promises.zip(
                accessTokenVendor.whenAccessToken,
                coreAPI.whenApplicationConfig
            )
        }.then { accessToken, appConfig in
            self.coreAPI.getProfile(
                profileURL: appConfig.profileUrl,
                accessToken: accessToken
            )
        }
    }()

    var nicknameDidChange: [(String) -> Void] = []
    private(set) var currentNickname: String? {
        didSet {
            guard let currentNickname = currentNickname else { return }
            nicknameDidChange.forEach { $0(currentNickname) }
        }
    }

    var customDataDidChange: [(String) -> Void] = []
    private(set) var currentCustomData: String? {
        didSet {
            guard let currentCustomData = currentCustomData else { return }
            customDataDidChange.forEach { $0(currentCustomData) }
        }
    }

    private let coreAPI: LiveLikeCoreAPIProtocol
    private let accessTokenVendor: AccessTokenVendor

    init(
        accessTokenVendor: AccessTokenVendor,
        coreAPI: LiveLikeCoreAPIProtocol
    ) {
        self.coreAPI = coreAPI
        self.accessTokenVendor = accessTokenVendor
    }

    func setNickname(nickname: String) -> Promise<String> {
        return firstly {
            Promises.zip(
                accessTokenVendor.whenAccessToken,
                coreAPI.whenApplicationConfig
            )
        }.then { accessToken, appResource in
            self.coreAPI.setNickname(
                profileURL: appResource.profileUrl,
                nickname: nickname,
                accessToken: accessToken
            )
        }.then(on: DispatchQueue.global()) { profile -> String in
            let newNickname = profile.nickname
            self.currentNickname = nickname
            return newNickname
        }
    }

    func setCustomData(customData: String) -> Promise<String> {
        return firstly {
            Promises.zip(
                accessTokenVendor.whenAccessToken,
                coreAPI.whenApplicationConfig
            )
        }.then { accessToken, appResource in
            self.coreAPI.setCustomData(
                profileURL: appResource.profileUrl,
                data: customData,
                accessToken: accessToken
            )
        }.then(on: DispatchQueue.global()) { profile -> String in
            let newCustomData = profile.customData
            self.currentCustomData = newCustomData
            return newCustomData ?? ""
        }
    }

}

protocol UserProfileVendor {
    var whenProfileResource: Promise<ProfileResource> { get }
}

protocol LiveLikeIDVendor {
    var whenLiveLikeID: Promise<LiveLikeID> { get }
}

struct LiveLikeID {
    private let internalID: String

    init(from string: String) {
        internalID = string
    }

    var asString: String {
        return internalID
    }
}

protocol UserNicknameVendor: AnyObject {
    var whenInitialNickname: Promise<Void> { get }
    var currentNickname: String? { get }
    var nicknameDidChange: [(String) -> Void] { get set }
}

protocol UserNicknameService: UserNicknameVendor {
    func setNickname(nickname: String) -> Promise<String>
}

protocol UserCustomDataVendor: AnyObject {
    var whenInitialCustomData: Promise<Void> { get }
    var currentCustomData: String? { get }
    var customDataDidChange: [(String) -> Void] { get set }
}

protocol UserCustomDataService: UserCustomDataVendor {
    func setCustomData(customData: String) -> Promise<String>
}
