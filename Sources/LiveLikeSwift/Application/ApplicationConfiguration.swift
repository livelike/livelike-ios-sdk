//
//  ApplicationConfiguration.swift
//  LiveLikeSDK
//
//  Created by Cory Sullivan on 2019-02-03.
//

import Foundation

struct Applications: Decodable {
    let results: [ApplicationConfiguration]
}

struct ApplicationConfiguration: Decodable {
    let name: String
    let clientId: String
    let pubnubPublishKey: String?
    let pubnubSubscribeKey: String?
    let sessionsUrl: URL
    let profileUrl: URL
    let profileDetailUrlTemplate: String
    let stickerPacksUrl: URL
    let programsUrl: URL
    let programDetailUrlTemplate: String
    let chatRoomDetailUrlTemplate: String
    let mixpanelToken: String?
    let analyticsProperties: [String: String]
    let pubnubOrigin: String?
    let organizationId: String
    let organizationName: String
    let createChatRoomUrl: String
    let widgetDetailUrlTemplate: String
    let leaderboardDetailUrlTemplate: String
    let pubnubHeartbeatInterval: Int
    let pubnubPresenceTimeout: Int
    let badgesUrl: URL
    let rewardItemsUrl: URL
    let chatRoomsInvitationsUrl: URL
    let chatRoomInvitationDetailUrlTemplate: String
    let createChatRoomInvitationUrl: URL
    let profileChatRoomReceivedInvitationsUrlTemplate: String
    let profileChatRoomSentInvitationsUrlTemplate: String
    let profileSearchUrl: String
    let rewardTransactionsUrl: URL
    let pinnedMessagesUrl: URL
    let sponsorsUrl: URL
    let redemptionKeysUrl: URL
    let redemptionKeyDetailByCodeUrlTemplate: String
    let redemptionKeyDetailUrlTemplate: String
    let questsUrl: URL
    let userQuestsUrlTemplate: String
    let userQuestDetailUrlTemplate: String
    let userQuestsUrl: URL
    let userQuestTaskProgressUrl: URL
    let userQuestRewardsUrl: URL
    let reactionSpaceDetailUrlTemplate: String
    let reactionSpacesUrl: URL
    let reactionPackDetailUrlTemplate: String
    let reactionPacksUrl: URL
    let invokedActionsUrl: URL
    let rewardActionsUrl: URL
    let profileLeaderboardsUrlTemplate: String
    let profileLeaderboardViewsUrlTemplate: String
    let chatRoomEventsUrl: URL
    let apiPollingInterval: Int
    let programCustomIdUrlTemplate: String
    let badgeProfilesUrl: URL
    let commentBoardsUrl: URL
    let commentBoardDetailUrlTemplate: String
    let profileRelationshipsUrl: URL
    let profileRelationshipTypesUrl: URL
    let profileRelationshipDetailUrlTemplate: String
    let commentBoardBanUrl: URL
    let commentBoardBanDetailUrlTemplate: String
    let commentReportUrl: URL
    let commentReportDetailUrlTemplate: String
    let autoclaimInteractionRewards: Bool
    let chatRoomMembershipsUrl: URL
    let savedContractAddressesUrl: URL

    let textPollsUrl: URL
    let imagePollsUrl: URL
    let textPredictionsUrl: URL
    let imagePredictionsUrl: URL
    let alertsUrl: URL
    let textAsksUrl: URL
    let textQuizzesUrl: URL
    let imageQuizzesUrl: URL
    let richPostsUrl: URL
    let imageNumberPredictionsUrl: URL

    func getUserQuestsURL(profileID: String) throws -> URL {
        let filledTemplate = userQuestsUrlTemplate.replacingOccurrences(
            of: "{profile_id}",
            with: profileID
        )
        guard let url = URL(string: filledTemplate) else {
            throw QuestsClientError.questsClientError(
                error: "failing to build a URL from `userQuestsUrlTemplate`"
            )
        }
        return url
    }

    func getUserQuestDetailURL(userQuestID: String) throws -> URL {
        let filledTemplate = userQuestDetailUrlTemplate.replacingOccurrences(
            of: "{user_quest_id}",
            with: userQuestID
        )
        guard let url = URL(string: filledTemplate) else {
            throw QuestsClientError.questsClientError(
                error: "not being able to build URL from `userQuestDetailUrlTemplate`"
            )
        }
        return url
    }

    func getProfileLeaderboardsURL(profileID: String) throws -> URL {
        let filledTemplate = profileLeaderboardsUrlTemplate.replacingOccurrences(
            of: "{profile_id}",
            with: profileID
        )
        guard let url = URL(string: filledTemplate) else {
            throw LLGamificationError.leaderboardUrlCreationFailure
        }
        return url
    }

    func getProfileLeaderboardViewsURL(profileID: String) throws -> URL {
        let filledTemplate = profileLeaderboardViewsUrlTemplate.replacingOccurrences(
            of: "{profile_id}",
            with: profileID
        )
        guard let url = URL(string: filledTemplate) else {
            throw LLGamificationError.leaderboardUrlCreationFailure
        }
        return url
    }

    func getProgramCustomIDURL(customID: String) throws -> URL {
        let filledTemplate = programCustomIdUrlTemplate.replacingOccurrences(
            of: "{custom_id}",
            with: customID
        )
        guard let url = URL(string: filledTemplate) else {
            throw ProgramDetailsError.failedGettingProgram(
                error: "failing to build a URL from `programCustomIdUrlTemplate`"
            )
        }
        return url
    }

    func getReactionSpaceDetailURL(reactionSpaceID: String) throws -> URL {
        return try TemplatedURLHelper.buildURL(
            templatedURLString: self.reactionSpaceDetailUrlTemplate,
            templateKey: "{reaction_space_id}",
            replacementString: reactionSpaceID
        )
    }

    func getReactionPackURL(reactionPackID: String) throws -> URL {
        return try TemplatedURLHelper.buildURL(
            templatedURLString: self.reactionPackDetailUrlTemplate,
            templateKey: "{reaction_pack_id}",
            replacementString: reactionPackID
        )
    }

    func getCommentBoardDetailURL(commentBoardID: String) throws -> URL {
        return try TemplatedURLHelper.buildURL(
            templatedURLString: self.commentBoardDetailUrlTemplate,
            templateKey: "{comment_board_id}",
            replacementString: commentBoardID
        )
    }

    func getProfileRelationshipsDetailURL(profileRelationshipID: String) throws -> URL {
        return try TemplatedURLHelper.buildURL(
            templatedURLString: self.profileRelationshipDetailUrlTemplate,
            templateKey: "{profile_relationship_id}",
            replacementString: profileRelationshipID
        )
    }

    func getCommentBoardBanDetailURL(commentBoardBanID: String) throws -> URL {
        return try TemplatedURLHelper.buildURL(
            templatedURLString: self.commentBoardBanDetailUrlTemplate,
            templateKey: "{comment_board_ban_id}",
            replacementString: commentBoardBanID
        )
    }

    func getCommentReportDetailURL(commentReportID: String) throws -> URL {
        return try TemplatedURLHelper.buildURL(
            templatedURLString: self.commentReportDetailUrlTemplate,
            templateKey: "{comment_report_id}",
            replacementString: commentReportID
        )
    }

    func getWidgetDetailURL(id: String, kind: WidgetKind) throws -> URL {
        let widgetDetailURLString = self.widgetDetailUrlTemplate
            .replacingOccurrences(
                of: "{kind}",
                with: kind.stringValue
            ).replacingOccurrences(
                of: "{id}",
                with: id
            )

        guard
            let widgetDetailURL = URL(string: widgetDetailURLString)
        else {
            throw GetWidgetAPIServicesError.widgetUrlIsCorrupt
        }

        return widgetDetailURL

    }
}
