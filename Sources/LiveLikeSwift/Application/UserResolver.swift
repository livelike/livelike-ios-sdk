//
//  UserSessionRestorer.swift
//  EngagementSDK
//
//  Created by Cory Sullivan on 2019-04-14.
//

import Foundation
import LiveLikeCore

/// Vends a valid access token
/// UserResolver will check the validity of the accessToken passed by the `AccessTokenStorage`
/// If accessToken is missing or invalid a new token will be generated
class UserResolver: AccessTokenVendor {

    lazy var whenAccessToken: Promise<AccessToken> = {
        if let accessTokenString = self.accessTokenStorage.fetchAccessToken() {
            return validateAccessToken(AccessToken(fromString: accessTokenString))
        } else {
            return generateNewAccessToken()
        }
    }()

    private func generateNewAccessToken() -> Promise<AccessToken> {
        return firstly {
            whenApplicationConfig
        }.then {
            log.warning("""
            The EngagementSDK is creating a new User Profile because it was initialized without an existing Access Token.
            The created User Profile will be counted towards the Monthly Active Users (MAU) calculation.
            For more information: https://docs.livelike.com/docs/user-profiles
            """)
            return self.createProfile(profileURL: $0.profileUrl)
        }.ensure { [weak self] in
            self?.accessTokenStorage.storeAccessToken(accessToken: $0.asString)
            return true
        }
    }

    // MARK: Private Properties

    private let accessTokenStorage: AccessTokenStorage
    private let clientID: String
    private let origin: URL
    private weak var sdkDelegate: InternalErrorReporter?

    /**
     - Parameter integratorAccessToken: The access token given by the integrator to attempt to retreive a user's profile
     */
    init(
        accessTokenStorage: AccessTokenStorage,
        clientID: String,
        origin: URL,
        sdkDelegate: InternalErrorReporter
    ) {
        self.accessTokenStorage = accessTokenStorage
        self.clientID = clientID
        self.origin = origin
        self.sdkDelegate = sdkDelegate
    }

    /**
     Test access token is valid by requesting the profile
     If token is invalid (403 Invalid Authorization) returns anonymous profile
     */
    private func validateAccessToken(_ accessToken: AccessToken) -> Promise<AccessToken> {
        return firstly {
            self.whenApplicationConfig
        }.then {
            self.getProfile(profileURL: $0.profileUrl, accessToken: accessToken)
        }.then { _ in
            // successfully loaded profile - access token is good
            Promise(value: accessToken)
        }.recover { (error) -> Promise<AccessToken> in
            switch error {
            case NetworkClientError.forbidden,
                 NetworkClientError.unauthorized:
                self.sdkDelegate?.report(setupError: .invalidUserAccessToken(accessToken.asString))
            default:
                self.sdkDelegate?.report(setupError: .unknownError(error))
            }
            return self.generateNewAccessToken()
        }
    }

    private lazy var whenApplicationConfig: Promise<ApplicationConfiguration> = {
        let url = self.origin.appendingPathComponent("applications").appendingPathComponent(self.clientID)
        let resource = Resource<ApplicationConfiguration>(get: url)
        return LiveLike.networking.load(resource)
    }()

    private func createProfile(profileURL: URL) -> Promise<AccessToken> {
        struct AccessTokenResource: Decodable {
            let accessToken: String
        }
        let resource = Resource<AccessTokenResource>(
            url: profileURL,
            method: .post(EmptyBody())
        )
        return firstly {
            LiveLike.networking.load(resource)
        }.then {
            AccessToken(fromString: $0.accessToken)
        }
    }

    private func getProfile(profileURL: URL, accessToken: AccessToken) -> Promise<ProfileResource> {
        let resource = Resource<ProfileResource>(
            get: profileURL,
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }
}

protocol AccessTokenVendor {
    var whenAccessToken: Promise<AccessToken> { get }
}
