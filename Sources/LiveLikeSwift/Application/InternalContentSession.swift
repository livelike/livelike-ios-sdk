//
//  InternalContentSession.swift
//  LiveLikeSDK
//
//  Created by Heinrich Dams on 2019-01-18.
//

import AVFoundation
import Foundation
import LiveLikeCore

/// Concrete implementation of `ContentSession`
class InternalContentSession: ContentSession {

    var widgetClient: PubSubWidgetClient?
    var widgetChannel: String?

    var status: SessionStatus = .uninitialized {
        didSet {
            delegate?.session(self, didChangeStatus: status)
        }
    }

    let whenInitializedAndReady: Promise<Void> = Promise()

    var widgetPauseListeners = Listener<PauseDelegate>()
    private(set) var widgetPauseStatus: PauseStatus {
        didSet {
            timeWidgetPauseStatusChanged = Date()
            widgetPauseListeners.publish { $0.pauseStatusDidChange(status: self.widgetPauseStatus) }
        }
    }

    var config: SessionConfiguration

    var programID: String

    var playerTimeSource: PlayerTimeSource?

    /// Unique identifier to represent the session instance
    private var hashValue: String {
        return String(UInt(bitPattern: ObjectIdentifier(self)))
    }

    private let chatHistoryLimitRange = 0 ... 200
    private let whenWidgetClient: Promise<PubSubWidgetClient?>
    let sdkInstance: LiveLike
    let nicknameVendor: UserNicknameVendor

    var nicknameDidChange: ((String) -> Void)?

    private let predictionVoteRepo: PredictionVoteRepository
    var sessionDidEnd: (() -> Void)?
    weak var delegate: ContentSessionDelegate? {
        didSet {
            if delegate != nil {
                initializeDelegatePlayheadTimeSource()
            }
        }
    }

    private lazy var whenWidgetModelFactory: Promise<WidgetModelFactory> = {
        return firstly {
            Promises.zip(
                sdkInstance.whenUserProfile,
                self.whenProgramDetail,
                self.whenWidgetClient,
                self.sdkInstance.whenWidgetInteractionRepo
            )
        }.then { (userProfile, programResource, widgetClient, widgetInteractionRepo) in
            let factory = WidgetModelFactory(
                eventRecorder: self.eventRecorder,
                userProfile: userProfile,
                rewardItems: programResource.rewardItems,
                leaderboardsManager: self.leaderboardsManager,
                widgetClient: widgetClient,
                widgetAPI: self.sdkInstance.widgetAPI,
                leaderboardAPI: self.sdkInstance.leaderboardAPI,
                predictionVoteRepo: self.predictionVoteRepo,
                widgetInteractionRepo: widgetInteractionRepo
            )
            return Promise(value: factory)
        }
    }()

    private lazy var whenChatSession: Promise<ChatSession> = {
        return firstly {
            self.whenProgramDetail
        }.then { programDetail -> Promise<ChatSession> in
            guard let chatRoomResource = programDetail.defaultChatRoom else {
                return Promise(error: ContentSessionError.defaultChatRoomNotFound)
            }

            return Promise { fulfill, reject in
                var chatConfig = ChatSessionConfig(roomID: chatRoomResource.id)
                chatConfig.shouldDisplayAvatar = self.config.chatShouldDisplayAvatar
                chatConfig.enableChatMessageURLs = self.config.enableChatMessageURLs
                chatConfig.chatMessageUrlPatterns = self.config.chatMessageUrlPatterns
                chatConfig.syncTimeSource = self.playerTimeSource
                chatConfig.includeFilteredMessages = self.config.includeFilteredChatMessages

                self.sdkInstance.connectChatRoom(
                    config: chatConfig
                ) { result in
                    switch result {
                    case .success(let currentChatRoom):
                        self.currentChatRoom = currentChatRoom
                        fulfill(currentChatRoom)
                    case .failure(let error):
                        reject(error)
                    }
                }
            }
        }
    }()

    func getChatSession(completion: @escaping (Result<ChatSession, Error>) -> Void) {
        firstly {
            self.whenChatSession
        }.then { chatSession in
            completion(.success(chatSession))
        }.catch { error in
            completion(.failure(error))
        }
    }

    weak var player: AVPlayer?
    var periodicTimebaseObserver: Any?

    private var nextWidgetTimelineUrl: URL?
    private var nextWidgetModelTimelineURL: URL?
    private var nextUnclaimedWidgetInteractionsURL: URL?
    private let leaderboardsManager: LeaderboardsManager

    // Analytics properties

    var eventRecorder: EventRecorder
    private var timeWidgetPauseStatusChanged: Date = Date()

    private var sessionIsValid: Bool {
        if status == .ready {
            return true
        }
        delegate?.session(self, didReceiveError: SessionError.invalidSessionStatus(status))
        return false
    }

    private let whenProgramDetail: Promise<ProgramDetailResource>

    /// Maintains a reference to the base widget proxy
    ///
    /// This allows us to remove it as a listener from the `WidgetMessagingClient`
    private var baseWidgetProxy: WidgetProxy?

    /// Repo to keep track of all the `next` and `previous` urls
    private var pageResultURLRepo = [String: PaginatedResultURL]()
    private let paginationController = PaginatedRequestController()

    var currentChatRoom: ChatSession?

    private let networking: LLNetworking

    // MARK: -
    required init(
        sdkInstance: LiveLike,
        config: SessionConfiguration,
        whenWidgetClient: Promise<PubSubWidgetClient?>,
        nicknameVendor: UserNicknameVendor,
        programDetailVendor: ProgramDetailVendor,
        eventRecorder: EventRecorder,
        widgetVotes: PredictionVoteRepository,
        leaderboardsManager: LeaderboardsManager,
        networking: LLNetworking,
        delegate: ContentSessionDelegate? = nil
    ) {
        self.config = config
        self.whenWidgetClient = whenWidgetClient
        self.nicknameVendor = nicknameVendor
        self.delegate = delegate
        programID = config.programID
        self.sdkInstance = sdkInstance
        widgetPauseStatus = sdkInstance.widgetPauseStatus
        self.whenProgramDetail = programDetailVendor.getProgramDetails()
        self.eventRecorder = eventRecorder
        self.predictionVoteRepo = widgetVotes
        self.leaderboardsManager = leaderboardsManager
        self.networking = networking
        sdkInstance.setDelegate(self)

        initializeWidgetProxy()
        initializeDelegatePlayheadTimeSource()
        startSession()

        self.nicknameVendor.nicknameDidChange.append { [weak self] nickname in
            self?.nicknameDidChange?(nickname)
        }

        whenWidgetClient.then { [weak self] in
            guard let self = self else { return }
            self.widgetClient = $0
        }.catch {
            log.error($0.localizedDescription)
        }
    }

    private func initializeDelegatePlayheadTimeSource() {
        // If syncTimeSource given in config use that
        // Otherwise use the delegate until playheadTimeSource removed from delegate IOSSDK-1228
        if config.syncTimeSource != nil {
            playerTimeSource = { [weak self] in
                self?.config.syncTimeSource?()
            }
        } else if let delegateTimeSource = self.delegate?.playheadTimeSource(self) {
            playerTimeSource = {
                return delegateTimeSource.timeIntervalSince1970
            }
        } else {
            playerTimeSource = nil
        }
    }

    deinit {
        teardownSession()
        log.info("Content Session closed for program \(programID)")
    }

    private func startSession() {
        status = .initializing

        firstly {
            whenProgramDetail
        }.then { program in
            log.info("Content Session started for program \(self.programID)")
            self.status = .ready
            self.whenInitializedAndReady.fulfill(())
        }.catch { error in
            self.status = .error
            self.whenInitializedAndReady.reject(error)
            self.delegate?.session(self, didReceiveError: error)

            switch error {
            case NetworkClientError.badRequest:
                log.error("Content Session failed to connect due to a bad request. Please check that the program is ready on the CMS and try again.")
            case NetworkClientError.internalServerError:
                log.error("Content Session failed to connect due to an internal server error. Attempting to retry connection.")
            case let NetworkClientError.invalidResponse(description):
                log.error(description)
            default:
                log.error(error.localizedDescription)
            }
        }
    }

    private func initializeWidgetProxy() {
        firstly {
            Promises.zip(
                self.whenWidgetClient,
                self.whenProgramDetail
            )
        }.then { widgetClient, programDetail -> Promise<WidgetProxy> in
            guard let widgetClient = widgetClient else {
                return Promise(error: ContentSessionError.missingWidgetClient)
            }

            let subscribeChannel = programDetail.getWidgetPublishChannel()

            self.widgetChannel = subscribeChannel

            let syncWidgetProxy = SynchronizedWidgetProxy(playerTimeSource: self.playerTimeSource)
            self.baseWidgetProxy = syncWidgetProxy

            if let subscribeChannel = subscribeChannel {
                widgetClient.addListener(syncWidgetProxy, toChannel: subscribeChannel)
            }

            let widgetQueue = syncWidgetProxy
                .addProxy {
                    let pauseProxy = PauseWidgetProxy(
                        playerTimeSource: self.playerTimeSource,
                        initialPauseStatus: self.widgetPauseStatus
                    )
                    self.widgetPauseListeners.addListener(pauseProxy)
                    return pauseProxy
                }
                .addProxy { ImageDownloadProxy() }
                .addProxy { WidgetLoggerProxy(playerTimeSource: self.playerTimeSource) }
                .addProxy {
                    OnPublishProxy { [weak self] publishData in
                        guard let self = self else { return }
                        guard case let ClientEvent.widget(widgetResource) = publishData.clientEvent else { return }

                        // Create WidgetDataObject
                        firstly {
                            self.whenWidgetModelFactory
                        }.then(on: DispatchQueue.main) { widgetModelFactory in
                            let widgetModel = try widgetModelFactory.make(
                                from: widgetResource,
                                programSubscribeChannel: programDetail.getWidgetPublishChannel()
                            )
                            self.delegate?.contentSession(self, didReceiveWidget: widgetModel)
                        }.catch { error in
                            log.error(error)
                        }
                    }
                }

            return Promise(value: widgetQueue)
        }.catch {
            log.error($0)
        }
    }

    private func teardownSession() {
        sessionDidEnd?()
        if let widgetChannel = self.widgetChannel, let baseProxy = baseWidgetProxy {
            widgetClient?.removeListener(baseProxy, fromChannel: widgetChannel)
            baseWidgetProxy = nil
        }
        currentChatRoom?.disconnect()
        currentChatRoom = nil
    }

    @available(*, deprecated, message: "Toggle `shouldShowIncomingMessages` and `isChatInputVisible` to achieve this functionality")
    func pause() {
        pauseWidgets()
    }

    @available(*, deprecated, message: "Toggle `shouldShowIncomingMessages` and `isChatInputVisible` to achieve this functionality")
    func resume() {
        resumeWidgets()
    }

    func close() {
        guard sessionIsValid else { return }
        teardownSession()
    }

    func getRewardItems(completion: @escaping (Result<[RewardItem], Error>) -> Void) {
        firstly {
            whenProgramDetail
        }.then {
            completion(.success($0.rewardItems))
        }.catch {
            completion(.failure($0))
        }
    }

    func getLeaderboardClients(completion: @escaping (Result<[LeaderboardClient], Error>) -> Void) {
        firstly {
            Promises.zip(whenProgramDetail, sdkInstance.whenUserProfile)
        }.then { programDetail, userProfile in
            Promises.all(programDetail.leaderboards.map { leaderboard in
                self.sdkInstance.getLeaderboardAndCurrentEntry(leaderboard: leaderboard, profileID: userProfile.userID.asString)
            })
        }.then { leaderboardsAndEntries in
            let clients = leaderboardsAndEntries.map {
                LeaderboardClient(
                    leaderboardResource: $0.leaderboard,
                    currentLeaderboardEntry: $0.currentEntry,
                    leaderboardsManager: self.leaderboardsManager
                )
            }
            completion(.success(clients))
        }.catch {
            completion(.failure($0))
        }
    }

    func getPostedWidgetModels(
        page: WidgetPagination,
        completion: @escaping (Result<[WidgetModel]?, Error>) -> Void
    ) {
        struct TimelineURLNotFound: Error { }

        firstly {
            Promises.zip(whenProgramDetail, self.sdkInstance.whenUserProfile)
        }.then { (program, userProfile) -> Promise<(UserProfile, WidgetModelFactory, WidgetTimelineResource, WidgetInteractionRepository, ProgramDetailResource)> in
            guard let timelineURL: URL = {
                switch page {
                case .first:
                    return program.timelineUrl
                case .next:
                    return self.nextWidgetModelTimelineURL
                }
            }() else {
                log.debug("No more posted widgets available")
                throw TimelineURLNotFound()
            }

            return Promises.zip(
                .init(value: userProfile),
                self.whenWidgetModelFactory,
                self.sdkInstance.widgetAPI.getTimeline(
                    timelineURL: timelineURL,
                    accessToken: userProfile.accessToken
                ),
                self.sdkInstance.whenWidgetInteractionRepo,
                .init(value: program)
            )
        }.then { userProfile, widgetModelFactory, timelineResource, widgetInteractionRepo, programResource -> Promise<(WidgetModelFactory, WidgetTimelineResource, [WidgetInteraction], ProgramDetailResource)> in
            self.nextWidgetModelTimelineURL = timelineResource.next
            return Promises.zip(
                .init(value: widgetModelFactory),
                .init(value: timelineResource),
                widgetInteractionRepo.promise(interactionsForTimeline: timelineResource),
                .init(value: programResource)
            )
        }.then { widgetModelFactory, timelineResource, _, programResource in
            let widgetModels = try timelineResource.items.map { widgetResource in
                return try widgetModelFactory.make(
                    from: widgetResource,
                    programSubscribeChannel: programResource.getWidgetPublishChannel()
                )
            }
            completion(.success(widgetModels))
        }.catch { error in
            if error is TimelineURLNotFound {
                completion(.success(nil))
            } else {
                log.debug("Error occured on getting posted widgets: \(error.localizedDescription)")
                completion(.failure(error))
            }
        }
    }

    func getWidgetModel(byID id: String, kind: WidgetKind, completion: @escaping (Result<WidgetModel, Error>) -> Void) {
        firstly {
            Promises.zip(
                self.sdkInstance.widgetAPI.getWidget(id: id, kind: kind),
                self.whenWidgetModelFactory,
                self.whenProgramDetail
            )
        }.then { (clientEvent, widgetModelFactory, programResource) in
            guard clientEvent.programID == self.programID else {
                throw ContentSessionError.failedToCreateWidgetModelMismatchedProgramID(widgetProgramID: clientEvent.programID, sessionProgramID: self.programID)
            }
            let widgetModel = try widgetModelFactory.make(
                from: clientEvent,
                programSubscribeChannel: programResource.getWidgetPublishChannel()
            )
            completion(.success(widgetModel))
        }.catch { error in
            completion(.failure(error))
        }
    }

    func createWidgetModel(fromJSON jsonObject: Any, completion: @escaping (Result<WidgetModel, Error>) -> Void) {
        firstly {
            Promises.zip(
                self.whenWidgetModelFactory,
                self.whenProgramDetail
            )
        }.then { widgetModelFactory, programResource in
            let widgetResource = try WidgetPayloadParser.parse(jsonObject)
            guard widgetResource.programID == self.programID else {
                throw ContentSessionError.failedToCreateWidgetModelMismatchedProgramID(widgetProgramID: widgetResource.programID, sessionProgramID: self.programID)
            }
            let widgetModel = try widgetModelFactory.make(
                from: widgetResource,
                programSubscribeChannel: programResource.getWidgetPublishChannel()
            )
            completion(.success(widgetModel))
        }.catch { error in
            completion(.failure(error))
        }
    }

    func getWidgetInteractionWithUnclaimedRewards(
        page: WidgetPagination,
        completion: @escaping (Result<[WidgetInteraction], Error>) -> Void
    ) {
        let whenUnclaimedInteractionsURL: Promise<URL> = Promise { fulfill, reject in
            switch page {
            case .first:
                firstly {
                    Promises.zip(
                        self.whenProgramDetail,
                        self.sdkInstance.whenUserProfile
                    )
                }.then { programResource, userProfile in
                    fulfill(try programResource.getUnclaimedWidgetInteractionsURL(profileID: userProfile.userID.asString))
                }.catch {
                    reject($0)
                }
            case .next:
                if let url = self.nextUnclaimedWidgetInteractionsURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.nextPageUnavailable)
                }
            }
        }

        firstly {
            Promises.zip(whenUnclaimedInteractionsURL, self.sdkInstance.whenUserProfile)
        }.then { url, userProfile in
            self.sdkInstance.widgetAPI.getUnclaimedWidgetInteractions(url: url, accessToken: userProfile.accessToken)
        }.then { unclaimedWidgetInteractions in
            self.nextUnclaimedWidgetInteractionsURL = unclaimedWidgetInteractions.next
            completion(.success(unclaimedWidgetInteractions.items))
        }.catch { error in
            log.error("getWidgetInteractionWithUnclaimedRewards failed with error: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }

    func getWidgetModels(
        page: WidgetPagination,
        options: GetWidgetModelsRequestOptions?,
        completion: @escaping (Result<[WidgetModel], Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                sdkInstance.whenUserProfile.then { $0.accessToken.asString },
                whenProgramDetail.then { $0.widgetsUrl },
                whenWidgetModelFactory,
                whenProgramDetail.then { $0.getWidgetPublishChannel() }
            )
        }.then { accessToken, rootURL, widgetModelFactory, publishChannel in

            self.paginationController.performExecute(
                paginatedRequest: GetWidgetsRequest(
                    options: options,
                    networking: self.networking,
                    accessToken: accessToken
                ),
                rootURL: rootURL,
                page: page.asPagination
            ) { result in
                switch result {
                case .failure(let error):
                    log.error("getWidgetModels failed with error: \(error.localizedDescription)")
                    completion(.failure(error))
                case .success(let paginatedWidgetResources):
                    do {
                        let widgetModels = try paginatedWidgetResources.map { widgetResource in
                            return try widgetModelFactory.make(
                                from: widgetResource,
                                programSubscribeChannel: publishChannel
                            )
                        }
                        completion(.success(widgetModels))
                    } catch {
                        completion(.failure(error))
                    }
                }

            }
        }
    }

    func getWidgetInteractions(options: GetWidgetInteractionsRequestOptions, completion: @escaping (Result<[WidgetInteraction], Error>) -> Void) {
        let widgetInteractionsURL: Promise<URL> = Promise { fulfill, reject in
            firstly {
                Promises.zip(
                    self.whenProgramDetail,
                    self.sdkInstance.whenUserProfile
                )
            }.then { programResource, userProfile in
                fulfill(try programResource.getWidgetInteractionsURL(
                    profileID: userProfile.userID.asString,
                    options: options
                )
                )
            }.catch {
                reject($0)
            }
        }

        firstly {
            Promises.zip(widgetInteractionsURL, self.sdkInstance.whenUserProfile)
        }.then { url, userProfile in
            self.sdkInstance.widgetAPI.getWidgetInteractions(url: url, accessToken: userProfile.accessToken)
        }.then { widgetInteractionsResponse in
            let interactions = widgetInteractionsResponse.interactionsByKind.reduce(into: [WidgetInteraction]()) { interactions, kvp in
                interactions.append(contentsOf: kvp.value)
            }
            completion(.success(interactions))
        }.catch { error in
            log.error("Get Widget Interactions failed with error: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
}

// MARK: - Chat
extension InternalContentSession {

    /// Updates the image that will represent the user in chat
    func updateUserChatRoomImage(
        url: URL,
        completion: @escaping () -> Void,
        failure: @escaping (Error) -> Void
    ) {

        firstly {
            whenChatSession
        }.then { chatSession in
            chatSession.avatarURL = url
            completion()
        }.catch { error in
            failure(error)
        }
    }
}

// MARK: - Widgets
extension InternalContentSession: WidgetPauser {
    func setDelegate(_ delegate: PauseDelegate) {
        widgetPauseListeners.addListener(delegate)
    }

    func removeDelegate(_ delegate: PauseDelegate) {
        widgetPauseListeners.removeListener(delegate)
    }

    func pauseWidgets() {
        guard widgetPauseStatus == .unpaused else {
            log.verbose("Widgets are already paused.")
            return
        }
        eventRecorder.record(.widgetPauseStatusChanged(previousStatus: widgetPauseStatus, newStatus: .paused, secondsInPreviousStatus: Date().timeIntervalSince(timeWidgetPauseStatusChanged)))

        widgetPauseStatus = .paused
        if let widgetChannel = self.widgetChannel, let baseProxy = baseWidgetProxy {
            widgetClient?.removeListener(baseProxy, fromChannel: widgetChannel)
        }
        log.info("Widgets were paused.")
    }

    func resumeWidgets() {
        guard widgetPauseStatus == .paused else {
            log.verbose("Widgets are already unpaused.")
            return
        }
        eventRecorder.record(.widgetPauseStatusChanged(previousStatus: widgetPauseStatus, newStatus: .unpaused, secondsInPreviousStatus: Date().timeIntervalSince(timeWidgetPauseStatusChanged)))

        widgetPauseStatus = .unpaused
        if let widgetChannel = self.widgetChannel, let baseProxy = baseWidgetProxy {
            widgetClient?.addListener(baseProxy, toChannel: widgetChannel)
        }
        log.info("Widgets have resumed from pause.")
    }
}

// MARK: - PauseDelegate
extension InternalContentSession: PauseDelegate {
    func pauseStatusDidChange(status: PauseStatus) {
        switch status {
        case .paused:
            pauseWidgets()
        case .unpaused:
            resumeWidgets()
        }
    }
}
