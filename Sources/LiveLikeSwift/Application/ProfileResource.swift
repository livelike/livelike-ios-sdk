//
//  ProfileResource.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

/// Represents a user's profile
public struct ProfileResource: Codable {
    public let id: String
    public let nickname: String
    public let chatRoomMembershipsUrl: URL
    public let customData: String?
    public let reportedCount: Int

    let rewardItemTransferUrl: URL
    let rewardItemBalancesUrl: URL
    let badgesUrl: URL
    let badgeProgressUrl: URL
    let subscribeChannel: String
    let blockProfileUrl: URL
    let blockedProfilesTemplateUrl: String
    let blockedProfileIdsUrl: URL
    let leaderboardsUrl: URL
    let leaderboardViewsUrl: URL
}
