//
//  UpdateRedemptionKeyRequest.swift
//  
//
//  Created by Jelzon Monzon on 7/27/23.
//

import Foundation
import LiveLikeCore

class UpdateRedemptionKeyRequest: LLRequest {

    private let assignedTo: String?
    private let networking: LLNetworking
    private let accessToken: String

    init(
        assignedTo: String? = nil,
        networking: LLNetworking,
        accessToken: String
    ) {
        self.assignedTo = assignedTo
        self.networking = networking
        self.accessToken = accessToken
    }

    func execute(
        url: URL,
        completion: @escaping (Result<RedemptionKey, Error>) -> Void
    ) {
        struct Body: Encodable {
            let assignedTo: String?

            enum CodingKeys: CodingKey {
                case assignedTo
            }

            func encode(to encoder: Encoder) throws {
                var container = encoder.container(keyedBy: CodingKeys.self)
                if let assignedTo = self.assignedTo {
                    try container.encode(assignedTo, forKey: .assignedTo)
                } else {
                    try container.encodeNil(forKey: .assignedTo)
                }
            }
        }
        let resource = Resource<RedemptionKey>(
            url: url,
            method: .patch(Body(assignedTo: self.assignedTo)),
            accessToken: accessToken
        )
        networking.task(resource, completion: completion)
    }
}
