//
//  RedemptionKeyStatus.swift
//  
//
//  Created by Jelzon Monzon on 7/27/23.
//

import Foundation

/// A status that a `RedemptionCode` can be in
public enum RedemptionKeyStatus: String, Codable {
    case active
    case redeemed
    case inactive
}
