//
//  RedemptionKeyClient.swift
//  
//
//  Created by Jelzon Monzon on 7/27/23.
//

import Foundation
import LiveLikeCore

/// Methods for managing Redemption Keys
public protocol RedemptionKeyClient {
    /// Get a paginated list of Redemption Keys
    /// - Parameters:
    ///   - page: The page to get
    ///   - options: Filtering options
    ///   - completion:
    ///      - success: A list of Redemption Keys
    ///      - failure: An error describing the failure
    func getRedemptionKeys(
        page: Pagination,
        options: GetRedemptionKeysRequestOptions?,
        completion: @escaping (Result<[RedemptionKey], Error>) -> Void
    )

    /// Redeem a Redemption Key by code
    /// - Parameters:
    ///   - redemptionCode: The code to redeem a Redemption Key
    ///   - completion:
    ///     - success: The redeemed Redemption Key
    ///     - failure: An error describing the failure
    func redeemKeyBy(
        redemptionCode: String,
        completion: @escaping (Result<RedemptionKey, Error>) -> Void
    )

    /// Redeem Key by the redemption key ID
    /// - Parameters:
    ///   - redemptionKeyID: The id of the Redemption Key to redeem
    ///   - completion:
    ///     - success: The redeemed Redemption Key
    ///     - failure: An error describing the failure
    func redeemKeyBy(
        redemptionKeyID: String,
        completion: @escaping (Result<RedemptionKey, Error>) -> Void
    )

    /// Unassign your profile from a Redemption Key
    /// - Parameters:
    ///   - options: Filtering options
    ///   - completion:
    ///     - success: The relinquished Redemption Key
    ///     - failure: An error describing the failure
    func unassignRedemptionKey(
        options: UnassignRedemptionKeyOptions,
        completion: @escaping (Result<RedemptionKey, Error>) -> Void
    )
}
