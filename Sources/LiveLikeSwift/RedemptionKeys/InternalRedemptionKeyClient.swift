//
//  InternalRedemptionKeyClient.swift
//  
//
//  Created by Jelzon Monzon on 7/27/23.
//

import Foundation
import LiveLikeCore

class InternalRedemptionKeyClient: RedemptionKeyClient {
    private let coreAPI: LiveLikeCoreAPIProtocol
    private let whenUserProfile: Promise<UserProfile>
    private let networking: LLNetworking
    private let clientID: String

    private var pageResultURLRepo = [String: PaginatedResultURL]()
    private let paginationController = PaginatedRequestController()

    init(
        coreAPI: LiveLikeCoreAPIProtocol,
        whenUserProfile: Promise<UserProfile>,
        networking: LLNetworking,
        clientID: String
    ) {
        self.coreAPI = coreAPI
        self.whenUserProfile = whenUserProfile
        self.networking = networking
        self.clientID = clientID
    }

    /// Retrieve all redemption keys associated
    /// with the current user profile
    func getRedemptionKeys(
        page: Pagination,
        options: GetRedemptionKeysRequestOptions? = nil,
        completion: @escaping (Result<[RedemptionKey], Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                coreAPI.whenApplicationConfig,
                whenUserProfile.then { $0.accessToken.asString }
            )
        }.then { app, accessToken in
            self.paginationController.performExecute(
                paginatedRequest: GetRedemptionKeysRequest(
                    networking: self.networking,
                    accessToken: accessToken,
                    options: options,
                    clientID: self.clientID
                ),
                rootURL: app.redemptionKeysUrl,
                page: page,
                completion: completion
            )
        }.catch {
            completion(.failure($0))
        }
    }

    /// Redeem a redemption key by code
    func redeemKeyBy(
        redemptionCode: String,
        completion: @escaping (Result<RedemptionKey, Error>) -> Void
    ) {

        firstly {
            return Promises.zip(
                self.coreAPI.whenApplicationConfig,
                self.whenUserProfile
            )
        }.then { applicationConfig, userProfile in

            let redemptionCodeDetailURL = applicationConfig.redemptionKeyDetailByCodeUrlTemplate
            let filledTemplate = redemptionCodeDetailURL.replacingOccurrences(of: "{code}", with: redemptionCode)

            guard let url = URL(string: filledTemplate) else {
                let error = RedemptionError.invalidRedemptionKeyDetailURL
                log.error(error.localizedDescription)
                throw error
            }
            return self.coreAPI.redeemKey(url: url, accessToken: userProfile.accessToken)
        }.then { redemptionCode in
            completion(.success(redemptionCode))
        }.catch { error in
            log.error("Failed redeeming key by redemption code")
            completion(.failure(RedemptionError.failedRedeemingKey(error: error.localizedDescription)))
        }

    }

    /// Redeem Key by the redemption key ID
    func redeemKeyBy(
        redemptionKeyID: String,
        completion: @escaping (Result<RedemptionKey, Error>) -> Void
    ) {

        firstly {
            return Promises.zip(
                self.coreAPI.whenApplicationConfig,
                self.whenUserProfile
            )
        }.then { applicationConfig, userProfile in
            return self.coreAPI.redeemKey(
                url: try applicationConfig.getRedemptionKeyURL(
                    redemptionKeyID: redemptionKeyID
                ),
                accessToken: userProfile.accessToken
            )
        }.then { redemptionCode in
            completion(.success(redemptionCode))
        }.catch { error in
            log.error("Failed redeeming key by redemption key ID")
            completion(.failure(RedemptionError.failedRedeemingKey(error: error.localizedDescription)))
        }

    }

    func unassignRedemptionKey(
        options: UnassignRedemptionKeyOptions,
        completion: @escaping (Result<RedemptionKey, Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                coreAPI.whenApplicationConfig,
                whenUserProfile.then { $0.accessToken.asString }
            )
        }.then { app, accessToken in
            UpdateRedemptionKeyRequest(
                assignedTo: nil,
                networking: self.networking,
                accessToken: accessToken
            ).execute(
                url: try app.getRedemptionKeyURL(
                    redemptionKeyID: options.redemptionKeyID
                )
            )
        }.then {
            completion(.success($0))
        }.catch {
            completion(.failure($0))
        }
    }
}

private extension ApplicationConfiguration {
    func getRedemptionKeyURL(redemptionKeyID: String) throws -> URL {
        return try TemplatedURLHelper.buildURL(
            templatedURLString: self.redemptionKeyDetailUrlTemplate,
            templateKey: "{redemption_key_id}",
            replacementString: redemptionKeyID
        )
    }
}
