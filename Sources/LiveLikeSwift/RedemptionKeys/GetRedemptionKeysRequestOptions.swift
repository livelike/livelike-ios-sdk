//
//  GetRedemptionKeysRequestOptions.swift
//  
//
//  Created by Jelzon Monzon on 7/27/23.
//

import Foundation

/// A configurable request object used to query for redemption codes
public struct GetRedemptionKeysRequestOptions {

    let status: RedemptionKeyStatus?
    let isAssigned: Bool?

    /// Initialize options to be used in the Redemption Code queries
    /// - Parameters:
    ///   - status: Filter by the status of the Redemption Key
    ///   - isAssigned: Filter by whether the Redemption Key is assigned to a profile
    public init(
        status: RedemptionKeyStatus? = nil,
        isAssigned: Bool? = nil
    ) {
        self.status = status
        self.isAssigned = isAssigned
    }
}
