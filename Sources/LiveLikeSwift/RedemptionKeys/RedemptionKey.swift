//
//  RedemptionKey.swift
//  
//
//  Created by Jelzon Monzon on 7/27/23.
//

import Foundation

/// Represent a redeemable code object
public struct RedemptionKey: Codable {
    /// The id of this Redemption Key
    public let id: String
    /// The client id of this Redmpetion Key
    public let clientId: String
    /// The name of the Redemption Key
    public let name: String
    /// An optional description of what the  Redemption Key is for
    public let description: String?
    /// The code that can be used to redeem this Redemption Key
    public let code: String
    /// The date that  this Redemption Key was created
    public let createdAt: Date
    /// The date that this Redemption Key was redeemend
    public let redeemedAt: Date?
    /// The id of the Profile that redeemed this Redemption Key
    public let redeemedBy: String?
    /// The  status of this Redemption Key
    public let status: RedemptionKeyStatus
    /// The id of the Profile which this Redemption Key is assigned to
    public let assignedTo: String?
}
