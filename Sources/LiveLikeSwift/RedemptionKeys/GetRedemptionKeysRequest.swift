//
//  GetRedemptionKeysRequest.swift
//  
//
//  Created by Jelzon Monzon on 7/27/23.
//

import Foundation
import LiveLikeCore

struct GetRedemptionKeysRequest: LLRequest {
    
    private let networking: LLNetworking
    private let accessToken: String
    private let options: GetRedemptionKeysRequestOptions?
    private let clientID: String
    
    init(
        networking: LLNetworking,
        accessToken: String,
        options: GetRedemptionKeysRequestOptions? = nil,
        clientID: String
    ) {
        self.networking = networking
        self.accessToken = accessToken
        self.options = options
        self.clientID = clientID
    }
    
    func execute(
        url: URL,
        completion: @escaping (Result<PaginatedResult<RedemptionKey>, Error>) -> Void
    ) {
        var urlQueries = [URLQueryItem]()
        if let options = options {
            if let status = options.status {
                urlQueries.append(
                    URLQueryItem(name: "status", value: status.rawValue)
                )
            }
                
            if let isAssigned = options.isAssigned {
                urlQueries.append(
                    URLQueryItem(name: "is_assigned", value: isAssigned.description)
                )
            }
        }
        
        urlQueries.append(
            URLQueryItem(name: "client_id", value: self.clientID)
        )
        
        do {
            let resource = Resource<PaginatedResult<RedemptionKey>>(
                get: try url.with(queryItems: urlQueries),
                accessToken: accessToken
            )
            networking.task(resource, completion: completion)
        } catch {
            completion(.failure(error))
        }
        
    }
}

extension GetRedemptionKeysRequest: LLPaginatedRequest {
    typealias ElementType = RedemptionKey
    
    var requestID: String {
        var pageResultURLID = "getRedemptionKeys"
        // add params to the ID to uniqely identify a paginated call that has params
        guard let options = self.options else {
            return pageResultURLID
        }
        
        if let status = options.status {
            pageResultURLID += status.rawValue
        }
        
        if let isAssigned = options.isAssigned {
            pageResultURLID += "_isAssigned\(isAssigned)"
        }
        
        return pageResultURLID
    }
}
