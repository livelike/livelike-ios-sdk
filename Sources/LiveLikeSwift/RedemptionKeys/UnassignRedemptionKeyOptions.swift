//
//  UnassignRedemptionKeyOptions.swift
//  
//
//  Created by Jelzon Monzon on 7/27/23.
//

import Foundation

/// Filtering options for unassignRedemptionKey
public struct UnassignRedemptionKeyOptions {
    let redemptionKeyID: String

    /// - Parameter redemptionKeyID: A Redemption Key id
    public init(redemptionKeyID: String) {
        self.redemptionKeyID = redemptionKeyID
    }
}
