//
//  LeaderboardReward.swift
//
//  Created by Mike Moloksher on 8/3/20.
//

import Foundation

public struct LeaderboardReward {
    public let id: String
    public let name: String
}
