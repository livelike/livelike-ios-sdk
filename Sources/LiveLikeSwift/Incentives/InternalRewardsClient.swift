//
//  InternalRewardsClient.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 9/15/21.
//

import Foundation
import LiveLikeCore

/// A resource representing Reward Item Attribute
public typealias RewardItemAttribute = Attribute

/// The details of a Reward Item than can be earned
public struct RewardItem: Codable {
    /// The reward item id
    public let id: String
    /// The reward item name
    public let name: String
    /// Images associated with the reward item
    public let images: [RewardItemImage]
    /// Attributes associated to the reward item
    public let attributes: [RewardItemAttribute]

    let url: URL
    let clientID: String

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case url
        case clientID = "clientId"
        case attributes
        case images
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.url = try container.decode(URL.self, forKey: .url)
        self.clientID = try container.decode(String.self, forKey: .clientID)
        self.images = try container.decodeIfPresent([RewardItemImage].self, forKey: .images) ?? []
        self.attributes = try container.decodeIfPresent([RewardItemAttribute].self, forKey: .attributes) ?? []
    }

    init(
        id: String,
        name: String,
        images: [RewardItemImage],
        attributes: [RewardItemAttribute],
        url: URL,
        clientID: String
    ) {
        self.id = id
        self.name = name
        self.images = images
        self.attributes = attributes
        self.url = url
        self.clientID = clientID
    }
}

/// An image object that can be associated with a `RewardItem`
public struct RewardItemImage: Codable {
    /// Reward item image id
    public let id: String
    /// Reward item image name
    public let name: String
    /// Reward item image url
    public let imageURL: URL
    /// Reward item image mime type
    public let mimetype: String

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case imageURL = "imageUrl"
        case mimetype
    }
}

/// A reward that can be earned
public struct EarnableReward: Codable {

    /// The name of the Reward Item which can be earned
    public let rewardItemName: String

    /// The ID of the Reward Item which can be earned
    public let rewardItemID: String

    /// Reward Item Amount which can be earned
    public let rewardItemAmount: Int

    /// The Reward Action Key for which a Reward Item can be earned
    public let rewardActionKey: String

    enum CodingKeys: String, CodingKey {
        case rewardItemName
        case rewardItemID = "rewardItemId"
        case rewardItemAmount
        case rewardActionKey
    }
}

/// A resource which represents a successful reward item transfer
public struct RewardItemTransfer: Codable {

    /// Trasnfer ID
    public let id: String

    /// Reward Item ID that was transfered
    public let rewardItemID: String

    /// The name of the Reward Item which was transfered
    public let rewardItemName: String

    /// Reward Item Amount that was transfered
    public let rewardItemAmount: Int

    /// Profile ID of a user who received the reward item transfer
    public let recipientProfileID: String

    /// The name of the user who received the reward item transfer
    public let recipientProfileName: String

    /// Profile ID of user who sent the reward item transfer
    public let senderProfileID: String

    /// The name of the user who sent the reward item transfer
    public let senderProfileName: String

    /// Date of the succesful reward item transfer
    public let createdAt: Date

    enum CodingKeys: String, CodingKey {
        case id
        case rewardItemID = "rewardItemId"
        case rewardItemAmount
        case recipientProfileID = "recipientProfileId"
        case recipientProfileName
        case senderProfileID = "senderProfileId"
        case senderProfileName
        case createdAt
        case rewardItemName
    }
}

/// A resource which represents a successful reward transaction
public struct RewardTransaction: Codable {

    /// Transaction ID
    public let id: String

    /// Reward Item ID that was transfered
    public let rewardItemID: String

    /// The name of the Reward Item which was transfered
    public let rewardItemName: String

    /// Reward Item Amount that was transfered
    public let rewardItemAmount: Int

    /// User profile ID associated with the Reward Transaction
    public let profileID: String

    /// User profile nickname associated with the Reward Transaction
    public let profileNickname: String

    /// The kind of the widget
    public let widgetKind: WidgetKind?

    /// The ID of the widget associated to the Reward Transaction
    public let widgetID: String?

    /// The Reward Action ID associated with the Rewards Transaction
    public let rewardActionID: String

    /// Reward Action key associated with the Reward Transaction
    public let rewardActionKey: String

    /// Reward Action name associated with the Reward Transaction
    public let rewardActionName: String

    /// Date of the succesful reward item transfer
    public let createdAt: Date

    enum CodingKeys: String, CodingKey {
        case id
        case rewardItemID = "rewardItemId"
        case rewardItemAmount
        case profileID = "profileId"
        case profileNickname
        case widgetKind
        case widgetID = "widgetId"
        case rewardActionID = "rewardActionId"
        case rewardActionKey
        case rewardActionName
        case createdAt
        case rewardItemName
    }
}

/// A resource representing a reward item balance
public struct RewardItemBalance: Codable {

    /// Reward Item ID
    public let rewardItemID: String

    /// Reward Item Name
    public let rewardItemName: String

    /// Reward Item Balance
    public let rewardItemBalance: Int

    enum CodingKeys: String, CodingKey {
        case rewardItemID = "rewardItemId"
        case rewardItemName
        case rewardItemBalance
    }
}

/// A configurable request object used to query for reward item transfers
public struct RewardItemTransferRequestOptions {

    // Is a unique identifier used for pagination functionality
    let id: String

    // Is an array of `URLQueryItem` used to compile URL parameters
    let urlQueries: [URLQueryItem]

    /// Use `transferType` to filter reward item transfers
    let transferType: RewardItemTransferType?

    /// Initialize options to be used in the Reward Item Balance queries
    public init(transferType: RewardItemTransferType? = nil) {
        self.transferType = transferType

        self.id = {
            var id: String = ""
            if let transferType = transferType {
                id += transferType.rawValue
            }
            return id
        }()

        self.urlQueries = {
            var urlQueries = [URLQueryItem]()
            if let transferType = transferType {
                urlQueries.append(URLQueryItem(name: "transfer_type", value: transferType.rawValue))
            }
            return urlQueries
        }()
    }
}

/// A configurable request object used to query for Reward Transactions
public struct RewardTransactionsRequestOptions {

    // Is a unique identifier used for pagination functionality
    let id: String

    // Is an array of `URLQueryItem` used to compile URL parameters
    let urlQueries: [URLQueryItem]

    // Use `widgetKind` to filter reward transaction by widget kind
    let widgetKind: Set<WidgetKind>?

    // Use `widgetID` to filter reward transaction by widget ID
    let widgetIDs: Set<String>?

    // Filter reward transaction by profile IDs
    let profileIDs: Set<String>?

    // Filter reward transaction by reward item IDs
    let rewardItemIDs: Set<String>?

    // Filter reward transaction by reward action keys
    let rewardActionKeys: Set<String>?

    // Filter reward transaction by a `since` date
    let createdSince: Date?

    // Filter reward transaction by an `until` date
    let createdUntil: Date?

    /// Initialize options to be used in the Reward Transaction queries
    public init(
        widgetIDs: Set<String>? = nil,
        widgetKind: Set<WidgetKind>? = nil,
        profileIDs: Set<String>? = nil,
        rewardItemIDs: Set<String>? = nil,
        rewardActionKeys: Set<String>? = nil,
        createdSince: Date? = nil,
        createdUntil: Date? = nil
    ) {
        self.widgetKind = widgetKind
        self.widgetIDs = widgetIDs
        self.profileIDs = profileIDs
        self.rewardItemIDs = rewardItemIDs
        self.rewardActionKeys = rewardActionKeys
        self.createdSince = createdSince
        self.createdUntil = createdUntil

        self.id = {
            var id: String = ""

            if let widgetKind = widgetKind {
                let sortedWidgetKind = widgetKind.sorted { $0.stringValue > $1.stringValue }
                sortedWidgetKind.forEach { id += $0.stringValue }
            }

            if let widgetIDs = widgetIDs {
                let sortedWidgetIDs = widgetIDs.sorted { $0 > $1 }
                sortedWidgetIDs.forEach { id += $0 }
            }

            if let profileIDs = profileIDs {
                let sortedprofileIDs = profileIDs.sorted { $0 > $1 }
                sortedprofileIDs.forEach { id += $0 }
            }

            if let rewardItemIDs = rewardItemIDs {
                let sortedRewardItemIDs = rewardItemIDs.sorted { $0 > $1 }
                sortedRewardItemIDs.forEach { id += $0 }
            }

            if let rewardActionKeys = rewardActionKeys {
                let sortedRewardActionKeys = rewardActionKeys.sorted { $0 > $1 }
                sortedRewardActionKeys.forEach { id += $0 }
            }

            if let createdSince = createdSince {
                id += DateFormatter.iso8601FullWithPeriod.string(from: createdSince)
            }

            if let createdUntil = createdUntil {
                id += DateFormatter.iso8601FullWithPeriod.string(from: createdUntil)
            }

            return id
        }()

        self.urlQueries = {
            var urlQueries = [URLQueryItem]()

            if let widgetKind = widgetKind {
                widgetKind.forEach { kind in
                    urlQueries.append(URLQueryItem(name: "widget_kind", value: kind.stringValue))
                }
            }

            if let widgetIDs = widgetIDs {
                widgetIDs.forEach { widgetID in
                    urlQueries.append(URLQueryItem(name: "widget_id", value: widgetID))
                }
            }

            if let profileIDs = profileIDs {
                profileIDs.forEach { profileID in
                    urlQueries.append(URLQueryItem(name: "profile_id", value: profileID))
                }
            }

            if let rewardItemIDs = rewardItemIDs {
                rewardItemIDs.forEach { rewardItemID in
                    urlQueries.append(URLQueryItem(name: "reward_item_id", value: rewardItemID))
                }
            }

            if let rewardActionKeys = rewardActionKeys {
                rewardActionKeys.forEach { rewardActionKey in
                    urlQueries.append(URLQueryItem(name: "reward_action_key", value: rewardActionKey))
                }
            }

            if let createdSince = createdSince {
                urlQueries.append(URLQueryItem(name: "created_since", value: DateFormatter.iso8601FullWithPeriod.string(from: createdSince)))
            }

            if let createdUntil = createdUntil {
                urlQueries.append(URLQueryItem(name: "created_until", value: DateFormatter.iso8601FullWithPeriod.string(from: createdUntil)))
            }

            return urlQueries
        }()
    }

    func buildURL(base: URL) throws -> URL {

        guard var rewardTransactionsURLComponents = URLComponents(
            url: base,
            resolvingAgainstBaseURL: false
        ) else {
            throw RewardsClientError.failedBuildingRewardTransactionsURL(
                error: "Failed to build Reward Transaction URL Components"
            )
        }

        rewardTransactionsURLComponents.queryItems = self.urlQueries

        guard let rewardTransactionsURL = rewardTransactionsURLComponents.url else {
            throw RewardsClientError.failedBuildingRewardTransactionsURL(
                error: "Failed to create Reward Transaction URL"
            )
        }

        return rewardTransactionsURL
    }
}

/// A configurable request object used to query for Application Reward Items
public struct GetApplicationRewardItemsRequestOptions {
    let attributes: [RewardItemAttribute]?

    /// Initialize options to be used in the Application Reward Item queries
    public init(attributes: [RewardItemAttribute]? = nil) {
        self.attributes = attributes
    }
}

/// The type of transfers available to filter Reward Item Transfers by
public enum RewardItemTransferType: String {
    case received
    case sent
}

/// A delegate through which Rewards Client events can be obsereved
public protocol RewardsClientDelegate: AnyObject {

    /// Called when the current user receives a reward item
    func rewardsClient(
        _ rewardsClient: RewardsClient,
        userDidReceiveRewardItemTransfer rewardItemTransfer: RewardItemTransfer
    )
}

/// A client used for Rewards queries
public protocol RewardsClient {

    /// Conform to the `RewardsClientDelegate` to observe rewards client events
    var delegate: RewardsClientDelegate? { get set }

    /// Retrieve all reward items that are linked to the currently used Application
    func getApplicationRewardItems(
        page: Pagination,
        completion: @escaping (Result<[RewardItem], Error>) -> Void
    )

    /// Retrieve all reward items that are linked to the currently used Application
    /// with query options for easier filtering
    func getApplicationRewardItems(
        page: Pagination,
        options: GetApplicationRewardItemsRequestOptions,
        completion: @escaping (Result<[RewardItem], Error>) -> Void
    )

    /// Transfer a reward item amount to another user
    /// - Parameters:
    ///   - amount: the amount of a reward item
    ///   - recipientProfileID: user profile ID of the recipient
    ///   - rewardItemID: reward item ID
    func transferRewardItemAmount(
        _ amount: Int,
        recipientProfileID: String,
        rewardItemID: String,
        completion: @escaping (Result<RewardItemTransfer, Error>) -> Void
    )

    /// Retrieve all reward item transfers associated with the current user profile
    func getRewardItemTransfers(
        page: Pagination,
        completion: @escaping (Result<[RewardItemTransfer], Error>) -> Void
    )

    /// Retrieve filtered reward item transfers associated with the current user profile
    func getRewardItemTransfers(
        page: Pagination,
        options: RewardItemTransferRequestOptions,
        completion: @escaping (Result<[RewardItemTransfer], Error>) -> Void
    )

    /// Retrieve reward item balances
    /// - Parameters:
    ///   - rewardItemIds: An array of reward item IDs
    func getRewardItemBalances(
        page: Pagination,
        rewardItemIDs: [String],
        completion: @escaping (Result<[RewardItemBalance], Error>) -> Void
    )

    /// Retrieve Reward Transactions based on the provided options parameter
    func getRewardTransactions(
        page: Pagination,
        options: RewardTransactionsRequestOptions?,
        completion: @escaping (Result<[RewardTransaction], Error>) -> Void
    )


    /// Retrieve a paginated list of invoked reward actions
    /// - Parameters:
    ///   - page: The page to request. Start with `.first`
    ///   - options: Filtering options (optional)
    func getInvokedRewardActions(
        page: Pagination,
        options: GetInvokedRewardActionsOptions?,
        completion: @escaping (Result<[InvokedRewardAction], Error>) -> Void
    )

    /// Retrieve a paginated list of reward actions
    /// - Parameters:
    ///   - page: The page to request. Start with `.first`
    func getRewardActions(
        page: Pagination,
        completion: @escaping (Result<[RewardActionInfo], Error>) -> Void
    )
}

class InternalRewardsClient: RewardsClient {

    private let coreAPI: LiveLikeCoreAPIProtocol
    private let rewardsAPI: LiveLikeRewardsAPIProtocol
    private let accessTokenVendor: AccessTokenVendor
    private let whenPubSubService: Promise<PubSubService?>
    private var userPubNubChannel: PubSubChannel?
    private let userProfileVendor: UserProfileVendor
    private let networking: LLNetworking

    /// Repo to keep track of all the `next` and `previous` urls
    private var pageResultURLRepo = [String: PaginatedResultURL]()
    private let paginatedRequestController = PaginatedRequestController()

    weak var delegate: RewardsClientDelegate?

    init(
        coreAPI: LiveLikeCoreAPIProtocol,
        accessTokenVendor: AccessTokenVendor,
        rewardsAPI: LiveLikeRewardsAPIProtocol,
        pubNubService: Promise<PubSubService?>,
        userProfileVendor: UserProfileVendor,
        networking: LLNetworking
    ) {
        self.rewardsAPI = rewardsAPI
        self.accessTokenVendor = accessTokenVendor
        self.coreAPI = coreAPI
        self.whenPubSubService = pubNubService
        self.userProfileVendor = userProfileVendor
        self.networking = networking

        firstly {
            Promises.zip(
                self.userProfileVendor.whenProfileResource,
                self.whenPubSubService
            )
        }.then { profileResource, pubNubService in
            self.userPubNubChannel = pubNubService?.subscribe(profileResource.subscribeChannel)
            self.userPubNubChannel?.delegate = self
        }
    }

    func getApplicationRewardItems(
        page: Pagination,
        completion: @escaping (Result<[RewardItem], Error>) -> Void
    ) {
        getApplicationRewardItems(page, options: nil, completion: completion)
    }

    func getApplicationRewardItems(
        page: Pagination,
        options: GetApplicationRewardItemsRequestOptions,
        completion: @escaping (Result<[RewardItem], Error>) -> Void
    ) {
        getApplicationRewardItems(page, options: options, completion: completion)
    }

    func transferRewardItemAmount(
        _ amount: Int,
        recipientProfileID: String,
        rewardItemID: String,
        completion: @escaping (Result<RewardItemTransfer, Error>) -> Void
    ) {

        firstly {
            Promises.zip(
                self.userProfileVendor.whenProfileResource,
                self.accessTokenVendor.whenAccessToken
            )
        }.then { profileResource, accessToken in
            self.rewardsAPI.transferRewardItemAmount(
                url: profileResource.rewardItemTransferUrl,
                accessToken: accessToken,
                amount: amount,
                recipientProfileID: recipientProfileID,
                rewardItemID: rewardItemID
            )
        }.then { rewardItemTransferResource in
            completion(.success(rewardItemTransferResource))
        }.catch { error in
            log.error("Failed transfering a reward item amount")
            completion(.failure(RewardsClientError.failedTransferingRewardItemAmount(error: error.localizedDescription)))
        }

    }

    func getRewardItemTransfers(
        page: Pagination,
        completion: @escaping (Result<[RewardItemTransfer], Error>) -> Void
    ) {
        getRewardItemTransfers(page, options: nil, completion: completion)
    }

    func getRewardItemTransfers(
        page: Pagination,
        options: RewardItemTransferRequestOptions,
        completion: @escaping (Result<[RewardItemTransfer], Error>) -> Void
    ) {
        getRewardItemTransfers(page, options: options, completion: completion)
    }

    func getRewardItemBalances(
        page: Pagination,
        rewardItemIDs: [String],
        completion: @escaping (Result<[RewardItemBalance], Error>) -> Void
    ) {

        let pageResultURLID = "getRewardItemBalances_\(rewardItemIDs.joined())"
        let whenRewardItemBalancesURL: Promise<URL> = Promise { fulfill, reject in
            switch page {
            case .first:

                // Reset result repo cache if the user is calling `.first` page
                self.pageResultURLRepo[pageResultURLID] = nil

                firstly {
                    self.userProfileVendor.whenProfileResource
                }.then { profileResource in

                    let queryItems: [URLQueryItem] = rewardItemIDs.map({ URLQueryItem(name: "reward_item_id", value: $0) })

                    guard var rewardItemBalancesURLComponents = URLComponents(
                        url: profileResource.rewardItemBalancesUrl,
                        resolvingAgainstBaseURL: false
                    ) else {
                        reject(RewardsClientError.failedBuildingRewardItemBalancesURL(error: "Failed building URLComponents object from `profileResource.rewardItemBalancesUrl`"))
                        return
                    }

                    rewardItemBalancesURLComponents.queryItems = queryItems

                    guard let rewardItemBalancesURL = rewardItemBalancesURLComponents.url else {
                        reject(RewardsClientError.failedBuildingRewardItemBalancesURL(error: "`URLComponents` failed to the creation of a `URL`"))
                        return 
                    }

                    fulfill(rewardItemBalancesURL)
                }.catch {
                    reject($0)
                }

            case .next:
                if let url = self.pageResultURLRepo[pageResultURLID]?.nextURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.nextPageUnavailable)
                }
            case .previous:
                if let url = self.pageResultURLRepo[pageResultURLID]?.previousURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.previousPageUnavailable)
                }
            }
        }

        firstly {
            Promises.zip(
                whenRewardItemBalancesURL,
                self.accessTokenVendor.whenAccessToken
            )
        }.then { rewardItemBalancesURL, accessToken in
            self.rewardsAPI.getRewardItemBalances(
                url: rewardItemBalancesURL,
                accessToken: accessToken
            )
        }.then { paginatedRewardItemBalances in

            self.pageResultURLRepo[pageResultURLID] = PaginatedResultURL(
                nextURL: paginatedRewardItemBalances.next,
                previousURL: paginatedRewardItemBalances.previous
            )

            completion(.success(paginatedRewardItemBalances.items))
        }.catch { error in
            log.error("Failed getting reward item balances")
            completion(.failure(RewardsClientError.failedGettingRewardItemBalances(error: error.localizedDescription)))
        }
    }

    private func getRewardItemTransfers(
        _ page: Pagination,
        options: RewardItemTransferRequestOptions?,
        completion: @escaping (Result<[RewardItemTransfer], Error>) -> Void
    ) {

        var pageResultURLID = "getRewardItemTransfers"

        // add params to the ID to uniqely identify a paginated call that has params
        if let options = options {
            pageResultURLID += "_\(options.id)"
        }

        let whenRewardItemTransfersURL: Promise<URL> = Promise { fulfill, reject in
            switch page {
            case .first:

                // Reset result repo cache if the user is calling `.first` page
                self.pageResultURLRepo[pageResultURLID] = nil

                firstly {
                    self.userProfileVendor.whenProfileResource
                }.then { profileResource in

                    if let options = options {
                        guard var rewardItemTransfersURLComponents = URLComponents(
                            url: profileResource.rewardItemTransferUrl,
                            resolvingAgainstBaseURL: false
                        ) else {
                            completion(
                                .failure(
                                    RewardsClientError.failedBuildingRewardItemTransfersURL(
                                        error: "Failed to build Reward Item Transfer URL Components"
                                    )
                                )
                            )
                            return
                        }

                        rewardItemTransfersURLComponents.queryItems = options.urlQueries

                        guard let rewardItemTransfersURL = rewardItemTransfersURLComponents.url else {
                            completion(
                                .failure(
                                    RewardsClientError.failedBuildingRewardItemTransfersURL(
                                        error: "Failed to create Reward Item Transfer URL"
                                    )
                                )
                            )
                            return
                        }

                        fulfill(rewardItemTransfersURL)
                    } else {
                        fulfill(profileResource.rewardItemTransferUrl)
                    }

                }.catch {
                    reject($0)
                }

            case .next:
                if let url = self.pageResultURLRepo[pageResultURLID]?.nextURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.nextPageUnavailable)
                }
            case .previous:
                if let url = self.pageResultURLRepo[pageResultURLID]?.previousURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.previousPageUnavailable)
                }
            }
        }

        firstly {
            Promises.zip(whenRewardItemTransfersURL, self.accessTokenVendor.whenAccessToken)
        }.then { url, accessToken in
            self.rewardsAPI.getRewardItemTransfers(url: url, accessToken: accessToken)
        }.then { paginatedRewardItemTransfers in

            self.pageResultURLRepo[pageResultURLID] = PaginatedResultURL(
                nextURL: paginatedRewardItemTransfers.next,
                previousURL: paginatedRewardItemTransfers.previous
            )

            completion(.success(paginatedRewardItemTransfers.items))
        }.catch { error in
            log.error("Failed getting reward item transfers")
            completion(.failure(RewardsClientError.failedGettingRewardItemTransfers(error: error.localizedDescription)))
        }

    }

    private func getApplicationRewardItems(
        _ page: Pagination,
        options: GetApplicationRewardItemsRequestOptions?,
        completion: @escaping (Result<[RewardItem], Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                self.coreAPI.whenApplicationConfig,
                self.accessTokenVendor.whenAccessToken
            )
        }.then(on: DispatchQueue.global()) { appConfig, accessToken in
            self.paginatedRequestController.performExecute(
                paginatedRequest: GetApplicationRewardItemsRequest(
                    options: options,
                    clientID: appConfig.clientId,
                    accessToken: accessToken.asString,
                    networking: self.networking
                ),
                rootURL: appConfig.rewardItemsUrl,
                page: page,
                completion: completion
            )
        }
    }

    func getRewardTransactions(
        page: Pagination,
        options: RewardTransactionsRequestOptions?,
        completion: @escaping (Result<[RewardTransaction], Error>) -> Void
    ) {

        var pageResultURLID = "getRewardTransactions"

        // add params to the ID to uniqely identify a paginated call that has params
        if let options = options {
            pageResultURLID += "_\(options.id)"
        }

        let whenRewardTransactionsURL: Promise<URL> = Promise { fulfill, reject in
            switch page {
            case .first:

                // Reset result repo cache if the user is calling `.first` page
                self.pageResultURLRepo[pageResultURLID] = nil

                firstly {
                    self.coreAPI.whenApplicationConfig
                }.then { applicationResource in

                    if let options = options {

                        do {
                            let rewardTransactionsURL = try options.buildURL(base: applicationResource.rewardTransactionsUrl)
                            fulfill(rewardTransactionsURL)
                        } catch {
                            completion(.failure(error))
                        }

                    } else {
                        fulfill(applicationResource.rewardTransactionsUrl)
                    }

                }.catch {
                    reject($0)
                }

            case .next:
                if let url = self.pageResultURLRepo[pageResultURLID]?.nextURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.nextPageUnavailable)
                }
            case .previous:
                if let url = self.pageResultURLRepo[pageResultURLID]?.previousURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.previousPageUnavailable)
                }
            }
        }

        firstly {
            Promises.zip(whenRewardTransactionsURL, self.accessTokenVendor.whenAccessToken)
        }.then { url, accessToken in
            self.rewardsAPI.getRewardTransactions(url: url, accessToken: accessToken)
        }.then { paginatedRewardItemTransfers in

            self.pageResultURLRepo[pageResultURLID] = PaginatedResultURL(
                nextURL: paginatedRewardItemTransfers.next,
                previousURL: paginatedRewardItemTransfers.previous
            )

            completion(.success(paginatedRewardItemTransfers.items))
        }.catch { error in
            log.error("Failed getting reward transactions")
            completion(.failure(RewardsClientError.failedGettingRewardItemTransfers(error: error.localizedDescription)))
        }
    }

    func getInvokedRewardActions(
        page: Pagination,
        options: GetInvokedRewardActionsOptions?,
        completion: @escaping (Result<[InvokedRewardAction], Error>) -> Void
    ) {

        firstly {
            Promises.zip(
                self.accessTokenVendor.whenAccessToken,
                self.coreAPI.whenApplicationConfig
            )
        }.then { accessToken, appConfig in
            self.paginatedRequestController.performExecute(
                paginatedRequest: GetInvokedRewardActionsRequest(
                    clientID: appConfig.clientId,
                    options: options,
                    accessToken: accessToken,
                    networking: LiveLike.networking
                ),
                rootURL: appConfig.invokedActionsUrl,
                page: page,
                completion: completion
            )
        }
    }

    func getRewardActions(
        page: Pagination,
        completion: @escaping (Result<[RewardActionInfo], Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                self.accessTokenVendor.whenAccessToken,
                self.coreAPI.whenApplicationConfig
            )
        }.then { accessToken, application in
            self.paginatedRequestController.performExecute(
                paginatedRequest: GetRewardActionsRequest(
                    clientID: application.clientId,
                    accessToken: accessToken,
                    networking: LiveLike.networking
                ),
                rootURL: application.rewardActionsUrl,
                page: page,
                completion: completion
            )
        }.catch { error in
            completion(.failure(error))
        }
    }
}

extension InternalRewardsClient: PubSubChannelDelegate {
    func channel(_ channel: PubSubChannel, messageCreated message: PubSubChannelMessage) {

        guard let payload = try? PubSubUserProfileMessageDecoder.shared.decode(dict: message.message) else {
            log.error("Failed to decode pub sub user profile message.")
            return
        }

        switch payload {
        case .rewardItemTransferReceived(let receivedRewardItemTransfer):
            delegate?.rewardsClient(self, userDidReceiveRewardItemTransfer: receivedRewardItemTransfer)
        }
    }

    func channel(_ channel: PubSubChannel, messageActionCreated messageAction: PubSubMessageAction) {}
    func channel(_ channel: PubSubChannel, messageActionDeleted messageActionID: String, messageID: String) {}
}

private enum PubSubUserProfileEvent: String, Codable {
    case rewardItemTransferReceived = "reward-item-transfer-received"
}

private class PubSubUserProfileMessageDecoder {
    enum PubSubUserProfileEventWithPayload {
        case rewardItemTransferReceived(RewardItemTransfer)
    }

    private let decoder: LLJSONDecoder = LLJSONDecoder()

    static let shared = PubSubUserProfileMessageDecoder()

    func decode(dict: [String: Any]) throws -> PubSubUserProfileEventWithPayload {
        guard let eventString = dict["event"] as? String else {
            throw PubSubUserProfileDecodeError.failedToDecodePayload(
                decodingError: "'event' is missing from PubSubUserProfileEventWithPayload"
            )
        }

        guard let event: PubSubUserProfileEvent = PubSubUserProfileEvent(rawValue: eventString) else {
            throw PubSubUserProfileDecodeError.failedToDecodePayload(
                decodingError: "Failed to create PubSubUserProfileEvent"
            )
        }

        switch event {
        case .rewardItemTransferReceived:
            guard let responsePayload = dict["payload"] else {
                throw PubSubUserProfileDecodeError.missingPayload
            }

            do {
                let payloadData = try JSONSerialization.data(
                    withJSONObject: responsePayload,
                    options: .prettyPrinted
                )

                let rewardItemTransferReceived = try self.decoder.decode(
                    RewardItemTransfer.self,
                    from: payloadData
                )
                return .rewardItemTransferReceived(rewardItemTransferReceived)
            } catch {
                throw PubSubUserProfileDecodeError.failedToDecodePayload(decodingError: "\(error)")
            }
        }
    }
}
