//
//  LeaderboardEntriesResult.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

public struct LeaderboardEntriesResult {
    public let entries: [LeaderboardEntry]
    public let total: Int
    public let hasPrevious: Bool
    public let hasNext: Bool
}
