//
//  InternalBadgesClient.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 7/6/21.
//

import Foundation
import LiveLikeCore

// MARK: - Codable Resources

/// Resource representing a Profile Badge
public struct ProfileBadge: Decodable {

    /// The date and time at which the badge was awarded
    public let awardedAt: Date

    /// The awarded badge
    public let badge: Badge
}

/// Resource representing badge progress
public struct BadgeProgress: Decodable {

    /// Badge Resource
    public let badge: Badge

    /// Badge Progression resource representing progress towards the badge
    public let badgeProgression: [BadgeProgression]
}

/// Resource representing badge progression
public struct BadgeProgression: Decodable {

    /// Reward Item ID
    public let rewardItemID: String

    /// Reward Item Name
    public let rewardItemName: String

    /// The amount of reward item points needed to earn a badge
    public let rewardItemThreshold: Int

    /// Current progress towards the reward item threshold
    public let currentRewardAmount: Int

    enum CodingKeys: String, CodingKey {
        case rewardItemID = "rewardItemId"
        case rewardItemName
        case rewardItemThreshold
        case currentRewardAmount
    }
}

/// Resource representing a Badge
public struct Badge: Decodable {

    /// ID of the badge
    public let id: String

    /// Name of the Badge
    public let name: String

    /// Badge description
    public let description: String?

    /// URL of the badge icon
    public let badgeIconURL: URL

    /// A list of registered links associated with a badge
    public let registeredLinks: [RegisteredLink]

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case badgeIconURL = "badgeIconUrl"
        case registeredLinks
    }
}

// MARK: - Protocols

public protocol BadgesClient {

    /// Retrieve badges that have been earned or awarded to a user profile
    func getProfileBadgesBy(
        profileID: String,
        page: Pagination,
        completion: @escaping (Result<PaginatedResult<ProfileBadge>, Error>) -> Void
    )

    /// Retrieves all badges linked to the application, excluding badges that have been archived
    func getApplicationBadges(
        page: Pagination,
        completion: @escaping (Result<PaginatedResult<Badge>, Error>) -> Void
    )

    /// Retrieve user's progress towards a badge
    func getProfileBadgeProgress(
        profileID: String,
        badgeIDs: [String],
        completion: @escaping (Result<[BadgeProgress], Error>) -> Void
    )

    func getBadgeProfiles(
        badgeID: String,
        page: Pagination,
        completion: @escaping (Result<PaginatedResult<BadgeProfile>, Error>) -> Void
    )
}

// MARK: - Internal

final class InternalBadgesClient: BadgesClient {

    private var coreAPI: LiveLikeCoreAPIProtocol
    private var accessTokenVendor: AccessTokenVendor
    private let badgesAPI: LiveLikeBadgesAPIProtocol

    /// Repo to keep track of all the `next` and `previous` urls
    private var pageResultURLRepo = [String: PaginatedResultURL]()

    private func getUserProfileResource(profileID: String) -> Promise<ProfileResource> {

        return firstly {
            Promises.zip(self.coreAPI.whenApplicationConfig, self.accessTokenVendor.whenAccessToken)
        }.then { applicationConfig, accessToken in
            let profileDetailURL = applicationConfig.profileDetailUrlTemplate
            let filledTemplate = profileDetailURL.replacingOccurrences(of: "{profile_id}", with: profileID)

            guard let url = URL(string: filledTemplate) else {
                let error = BadgeError.invalidUserProfileDetailURL
                log.error(error.localizedDescription)
                throw error
            }

            return Promises.zip(Promise(value: url), Promise(value: accessToken))
        }.then { profileURL, accessToken -> Promise<ProfileResource> in
            return self.coreAPI.getProfile(profileURL: profileURL, accessToken: accessToken)
        }.catch { error in
            log.error("Failed getting User Profile Resource for Badge Client")
        }

    }

    init(
        coreAPI: LiveLikeCoreAPIProtocol,
        accessTokenVendor: AccessTokenVendor,
        badgesAPI: LiveLikeBadgesAPIProtocol = LiveLikeBadgesAPI()
    ) {
        self.badgesAPI = badgesAPI
        self.accessTokenVendor = accessTokenVendor
        self.coreAPI = coreAPI
    }

    func getProfileBadgesBy(
        profileID: String,
        page: Pagination,
        completion: @escaping (Result<PaginatedResult<ProfileBadge>, Error>) -> Void
    ) {

        let pageResultURLID = "getByProfileID_\(profileID)"
        let whenBadgesPageURL: Promise<URL> = Promise { fulfill, reject in
            switch page {
            case .first:

                // Reset result repo cache if the user is calling `.first` page
                self.pageResultURLRepo[pageResultURLID] = nil

                firstly {
                    self.getUserProfileResource(profileID: profileID)
                }.then { profileResource in
                    fulfill(profileResource.badgesUrl)
                }.catch {
                    reject($0)
                }
            case .next:
                if let url = self.pageResultURLRepo[pageResultURLID]?.nextURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.nextPageUnavailable)
                }
            case .previous:
                if let url = self.pageResultURLRepo[pageResultURLID]?.previousURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.previousPageUnavailable)
                }
            }
        }

        firstly {
            whenBadgesPageURL
        }.then { url in
            self.badgesAPI.getProfileBadges(url: url)
        }.then { paginatedBadgeResource in

            self.pageResultURLRepo[pageResultURLID] = PaginatedResultURL(
                nextURL: paginatedBadgeResource.next,
                previousURL: paginatedBadgeResource.previous
            )

            completion(.success(paginatedBadgeResource))
        }.catch { error in
            log.error("Profile Badges retrieval by profile ID Failed")
            completion(.failure(BadgeError.getBadgesByProfileFailure(error: error.localizedDescription)))
        }
    }

    func getProfileBadgeProgress(
        profileID: String,
        badgeIDs: [String],
        completion: @escaping (Result<[BadgeProgress], Error>) -> Void
    ) {

        firstly {
            self.getUserProfileResource(profileID: profileID)
        }.then { profileResource in

            let queryItems: [URLQueryItem] = badgeIDs.map({ URLQueryItem(name: "badge_id", value: $0) })

            guard var badgeProgressURLComponents = URLComponents(
                url: profileResource.badgeProgressUrl,
                resolvingAgainstBaseURL: false
            ) else {
                return Promise(error: BadgeError.getBadgesProgressFailure(error: "Failed building badge id url"))
            }

            badgeProgressURLComponents.queryItems = queryItems

            guard let badgeProgressURL = badgeProgressURLComponents.url else {
                return Promise(error: BadgeError.getBadgesProgressFailure(error: "Badge progress url failed to be built"))
            }

            return Promise(value: badgeProgressURL)
        }.then { url in
            self.badgesAPI.getProfileBadgeProgress(url: url)
        }.then { badgeProgresses in
            completion(.success(badgeProgresses))
        }.catch { error in
            log.error("Badge progress retrieval Failed")
            completion(.failure(BadgeError.getBadgesProgressFailure(error: error.localizedDescription)))
        }

    }

    func getApplicationBadges(
        page: Pagination,
        completion: @escaping (Result<PaginatedResult<Badge>, Error>) -> Void
    ) {

        let pageResultURLID = "getApplicationBadges"
        let whenAppBadgesPageURL: Promise<URL> = Promise { fulfill, reject in
            switch page {
            case .first:

                // Reset result repo cache if the user is calling `.first` page
                self.pageResultURLRepo[pageResultURLID] = nil

                firstly {
                    self.coreAPI.whenApplicationConfig
                }.then { applicationConfig in
                    fulfill(applicationConfig.badgesUrl)
                }.catch {
                    reject($0)
                }
            case .next:
                if let url = self.pageResultURLRepo[pageResultURLID]?.nextURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.nextPageUnavailable)
                }
            case .previous:
                if let url = self.pageResultURLRepo[pageResultURLID]?.previousURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.previousPageUnavailable)
                }
            }
        }

        firstly {
            whenAppBadgesPageURL
        }.then { url in
            self.badgesAPI.getApplicationBadges(url: url)
        }.then { paginatedBadgeResource in

            self.pageResultURLRepo[pageResultURLID] = PaginatedResultURL(
                nextURL: paginatedBadgeResource.next,
                previousURL: paginatedBadgeResource.previous
            )

            completion(.success(paginatedBadgeResource))
        }.catch { error in
            log.error("Application Badges retrieval Failed")
            completion(.failure(BadgeError.getApplicationBadgesFailure(error: error.localizedDescription)))
        }

    }

    func getBadgeProfiles(badgeID: String, page: Pagination, completion: @escaping (Result<PaginatedResult<BadgeProfile>, Error>) -> Void) {
        let pageResultURLID = "getProfilesByBadgeID_\(badgeID)"
        let whenProfilesPageURL: Promise<URL> = Promise { fulfill, reject in
            switch page {
            case .first:

                // Reset result repo cache if the user is calling `.first` page
                self.pageResultURLRepo[pageResultURLID] = nil

                firstly {
                    self.coreAPI.whenApplicationConfig
                }.then { application in
                    self.buildBadgeProfilesURL(badgeID: badgeID, url: application.badgeProfilesUrl)
                }.then { url in
                    fulfill(url)
                }.catch {
                    reject($0)
                }
            case .next:
                if let url = self.pageResultURLRepo[pageResultURLID]?.nextURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.nextPageUnavailable)
                }
            case .previous:
                if let url = self.pageResultURLRepo[pageResultURLID]?.previousURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.previousPageUnavailable)
                }
            }
        }

        firstly {
            Promises.zip(
                whenProfilesPageURL,
                self.accessTokenVendor.whenAccessToken
            )
        }.then { url, accessToken in
            self.badgesAPI.getBadgeProfiles(url: url, accessToken: accessToken)
        }.then { paginatedProfileResource in

            self.pageResultURLRepo[pageResultURLID] = PaginatedResultURL(
                nextURL: paginatedProfileResource.next,
                previousURL: paginatedProfileResource.previous
            )

            completion(.success(paginatedProfileResource))
        }.catch { error in
            log.error("Profile retrieval by Badge ID Failed")
            completion(.failure(BadgeError.getBadgeProfilesFailure(error: error.localizedDescription)))
        }
    }

    private func buildBadgeProfilesURL(badgeID: String, url: URL) -> Promise<URL> {
        let queryItem: URLQueryItem = URLQueryItem(name: "badge_id", value: badgeID)

        guard var urlComponents = URLComponents(
            url: url,
            resolvingAgainstBaseURL: false
        ) else {
            return Promise(error: BadgeError.getBadgesProgressFailure(error: "Failed building profiles for badge id url"))
        }

        urlComponents.queryItems = [queryItem]

        guard let badgeProfilesURL = urlComponents.url else {
            return Promise(error: BadgeError.getBadgesProgressFailure(error: "Profiles for badge URL failed to be built"))
        }

        return Promise(value: badgeProfilesURL)
    }
}
