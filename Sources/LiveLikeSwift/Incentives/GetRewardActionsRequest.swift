//
//  GetRewardActionsRequest.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 8/25/22.
//

import Foundation
import LiveLikeCore

struct GetRewardActionsRequest {
    
    init(
        clientID: String,
        accessToken: AccessToken,
        networking: LLNetworking
    ) {
        self.clientID = clientID
        self.accessToken = accessToken
        self.networking = networking
    }
    
    private let clientID: String
    private let accessToken: AccessToken
    private let networking: LLNetworking
}

extension GetRewardActionsRequest: LLRequest {
    typealias ResponseType = PaginatedResult<RewardActionInfo>
    
    func execute(
        url: URL,
        completion: @escaping (Result<PaginatedResult<RewardActionInfo>, Error>) -> Void
    ) {
        do {
            guard var urlComponents = URLComponents(
                url: url,
                resolvingAgainstBaseURL: false
            ) else {
                throw URLComponentError.failedToInitURLComponents(fromURL: url)
            }
            
            urlComponents.queryItems = [
                URLQueryItem(name: "client_id", value: clientID)
            ]
            
            guard let url = urlComponents.url else {
                throw URLComponentError.urlIsNil
            }
            
            let resource = Resource<PaginatedResult<RewardActionInfo>>(
                get: url,
                accessToken: accessToken.asString
            )
            networking.task(resource, completion: completion)
        } catch {
            completion(.failure(error))
        }
    }
}

extension GetRewardActionsRequest: LLPaginatedRequest {
    typealias ElementType = RewardActionInfo
    
    var requestID: String {
        return "getRewardActions"
    }
}
