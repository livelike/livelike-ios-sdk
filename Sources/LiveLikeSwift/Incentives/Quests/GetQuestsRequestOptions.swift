//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

/// A configurable request object which can be used to customize a query for `Quest`(s)
public struct GetQuestsRequestOptions {

    // Is a unique identifier used for pagination functionality
    let id: String

    // Is an array of `URLQueryItem` used to compile URL parameters
    let urlQueries: [URLQueryItem]

    /// Use `questIDs` to filter quests by ID
    let questIDs: [String]

    public init(questIDs: [String] = []) {
        self.questIDs = questIDs

        self.id = {
            var id: String = ""
            questIDs.forEach { id += $0 }

            return id
        }()

        self.urlQueries = {
            var urlQueries = [URLQueryItem]()
            questIDs.forEach { urlQueries.append(URLQueryItem(name: "id", value: $0)) }
            return urlQueries
        }()
    }
}
