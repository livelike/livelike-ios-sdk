//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

/// A resource representing a `UserQuestTask` object
public struct UserQuestTask: Codable {
    public let id: String
    public let status: UserQuestTaskStatus
    public let createdAt: Date
    public let completedAt: Date?
    public let userQuestID: String
    public let questTask: QuestTask
    public let progress: Float

    enum CodingKeys: String, CodingKey {
        case id
        case status
        case createdAt
        case completedAt
        case userQuestID = "userQuestId"
        case questTask
        case progress
    }
}
