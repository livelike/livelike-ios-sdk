//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation
import LiveLikeCore

public struct GetUserQuestRewardRequestOptions {
    let userQuestIDs: [String]
    let rewardStatus: QuestRewardStatus?

    /// - Parameters:
    ///   - userQuestIDs: The ids of the User Quests.
    ///   - rewardStatus: An optional filter by `QuestRewardStatus`.
    public init(
        userQuestIDs: [String],
        rewardStatus: QuestRewardStatus? = nil
    ) {
        self.userQuestIDs = userQuestIDs
        self.rewardStatus = rewardStatus
    }
}

struct GetUserQuestRewardsRequest {
    private let networking: LLNetworking
    private let accessToken: String
    private let options: GetUserQuestRewardRequestOptions

    init(
        networking: LLNetworking,
        accessToken: String,
        options: GetUserQuestRewardRequestOptions
    ) {
        self.networking = networking
        self.accessToken = accessToken
        self.options = options
    }
}

extension GetUserQuestRewardsRequest: LLRequest {
    typealias ResponseType = PaginatedResult<UserQuestReward>

    func execute(
        url: URL,
        completion: @escaping (Result<PaginatedResult<UserQuestReward>, Error>) -> Void
    ) {
        do {
            var queryItems = [URLQueryItem]()

            queryItems.append(
                contentsOf: options.userQuestIDs.map { userQuestID in
                    URLQueryItem(name: "user_quest_id", value: userQuestID)
                }
            )

            if let rewardStatus = options.rewardStatus {
                queryItems.append(
                    URLQueryItem(
                        name: "reward_status",
                        value: rewardStatus.rawValue
                    )
                )
            }
            let url = try url.with(queryItems: queryItems)
            let resource = Resource<PaginatedResult<UserQuestReward>>(
                get: url,
                accessToken: accessToken
            )
            networking.task(resource, completion: completion)
        } catch {
            completion(.failure(error))
        }
    }

}

extension GetUserQuestRewardsRequest: LLPaginatedRequest {
    typealias ElementType = UserQuestReward

    var requestID: String {
        return "getUserQuestRewards_\(options.userQuestIDs.joined())_\(options.rewardStatus?.rawValue ?? "")"
    }
}
