//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

/// A resource representing a `Quest` object
public struct Quest {
    init(id: String, clientID: String, name: String, description: String?, createdAt: Date, questTasks: [QuestTask], questRewardsURL: URL) {
        self.id = id
        self.clientID = clientID
        self.name = name
        self.description = description
        self.createdAt = createdAt
        self.questTasks = questTasks
        self.questRewardsURL = questRewardsURL
    }

    public let id: String
    public let clientID: String
    public let name: String
    public let description: String?
    public let createdAt: Date
    public let questTasks: [QuestTask]

    private let questRewardsURL: URL

    func getQuestRewardsURL(questID: String) throws -> URL {
        guard var urlComponents = URLComponents(
            url: self.questRewardsURL,
            resolvingAgainstBaseURL: false
        ) else {
            throw QuestsClientError.questsClientError(
                error: "not being able to build URL from `userQuestDetailUrlTemplate`"
            )
        }

        var queryItems = [URLQueryItem]()

        queryItems.append(
            URLQueryItem(name: "quest_id", value: questID)
        )

        urlComponents.queryItems = queryItems
        switch urlComponents.asURL {
        case .success(let url):
            return url
        case .failure(let error):
            throw error
        }
    }
}

extension Quest: Codable {
    enum CodingKeys: String, CodingKey {
        case id
        case clientID = "clientId"
        case name
        case description
        case createdAt
        case questTasks
        case questRewardsURL = "questRewardsUrl"
    }
}
