//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

/// A resource representing a `UserQuest` object
public struct UserQuest {
    init(id: String, profileID: String, userQuestTasks: [UserQuestTask], status: UserQuestStatus, createdAt: Date, completedAt: Date?, quest: Quest, rewardsStatus: QuestRewardStatus?, rewardsClaimedAt: Date?) {
        self.id = id
        self.profileID = profileID
        self.userQuestTasks = userQuestTasks
        self.status = status
        self.createdAt = createdAt
        self.completedAt = completedAt
        self.quest = quest
        self.rewardsStatus = rewardsStatus
        self.rewardsClaimedAt = rewardsClaimedAt
    }

    public let id: String
    public let profileID: String
    public let userQuestTasks: [UserQuestTask]
    public let status: UserQuestStatus
    public let createdAt: Date
    public let completedAt: Date?
    public let quest: Quest
    public let rewardsStatus: QuestRewardStatus?
    public let rewardsClaimedAt: Date?
}

extension UserQuest: Codable {
    enum CodingKeys: String, CodingKey {
        case id
        case profileID = "profileId"
        case userQuestTasks
        case status
        case createdAt
        case completedAt
        case quest
        case rewardsStatus
        case rewardsClaimedAt
    }
}
