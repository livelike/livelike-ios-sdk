//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

/// A representation of the states a `UserQuestTaskStatus` can be in
public typealias UserQuestTaskStatus = UserQuestStatus

/// A representation of the states a `UserQuest` can be in
public enum UserQuestStatus: String, Codable {
    case incomplete
    case completed
}
