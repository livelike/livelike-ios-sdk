//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation
import LiveLikeCore

public protocol QuestsClient {

    /// Retrieve Quests available for a client
    /// - Parameters:
    ///   - options: A configurable request object used to customize the query
    func getQuests(
        page: Pagination,
        options: GetQuestsRequestOptions,
        completion: @escaping (Result<[Quest], Error>) -> Void
    )

    /// Retrieve User Quests associated to user profile ID
    /// - Parameters:
    ///   - options: A configurable request object used to customize the query
    func getUserQuests(
        page: Pagination,
        options: GetUserQuestsRequestOptions,
        completion: @escaping (Result<[UserQuest], Error>) -> Void
    )

    /// Engage a user in a Quest by creating a `UserQuest`
    func startUserQuest(
        questID: String,
        completion: @escaping (Result<UserQuest, Error>) -> Void
    )

    /// Update `UserQuestTask`s status for a `UserQuest`
    func updateUserQuestTasks(
        userQuestID: String,
        userQuestTaskIDs: [String],
        status: UserQuestTaskStatus,
        completion: @escaping (Result<UserQuest, Error>) -> Void
    )

    /// Increment a `UserQuestTask` progress by default increment which is defined in the CMS
    /// or custom increment when `customIncrement` is set
    func incrementUserQuestTaskProgress(
        userQuestTaskID: String,
        customIncrement: Float?,
        completion: @escaping (Result<UserQuest, Error>) -> Void
    )

    /// Set a custom `UserQuestTask` progress
    /// This will overwrite the current overall `UserQuestTask` progress
    func setUserQuestTaskProgress(
        userQuestTaskID: String,
        progress: Float,
        completion: @escaping (Result<UserQuest, Error>) -> Void
    )

    /// Gets the rewards that are available to earn upon quest completion.
    /// This is a paginated request.
    /// - Parameters:
    ///   - page: The page to request. Start with `first`.
    ///   - questID: The id of the Quest
    func getQuestRewards(
        page: Pagination,
        questID: String,
        completion: @escaping (Result<[QuestReward], Error>) -> Void
    )

    /// Claim the rewards of a User Quest.
    /// - Parameters:
    ///   - userQuestID: The id of the User Quest
    func claimUserQuestRewards(
        userQuestID: String,
        completion: @escaping (Result<UserQuest, Error>) -> Void
    )

    /// Gets the rewards that are associated to the User Quest with a `rewardStatus` property to check whether  the reward has been claimed.
    /// This is a paginated request.
    /// - Parameters:
    ///   - page: The page to request.  Start with `first`.
    ///   - userQuestID: The id of the User Quest.
    ///   - rewardStatus: An optional filter by `QuestRewardStatus`.
    @available(*, deprecated, message: "Use getUserQuestRewards(page:options:completion:) instead.")
    func getUserQuestRewards(
        page: Pagination,
        userQuestID: String,
        rewardStatus: QuestRewardStatus?,
        completion: @escaping (Result<[UserQuestReward], Error>) -> Void
    )

    /// Gets the rewards that are associated to the User Quest with a `rewardStatus` property to check whether  the reward has been claimed.
    /// This is a paginated request.
    /// - Parameters:
    ///   - page: The page to request.  Start with `first`.
    ///   - options: Filtering options for this request
    func getUserQuestRewards(
        page: Pagination,
        options: GetUserQuestRewardRequestOptions,
        completion: @escaping (Result<[UserQuestReward], Error>) -> Void
    )
}
