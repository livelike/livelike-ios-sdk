//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

/// A resource representing a `QuestTask` object
public struct QuestTask: Codable {
    public let id: String
    public let name: String
    public let description: String?
    public let createdAt: Date
    public let questID: String
    public let targetValue: Float
    public let defaultProgressIncrement: Float

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case createdAt
        case questID = "questId"
        case targetValue
        case defaultProgressIncrement
    }
}
