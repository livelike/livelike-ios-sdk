//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

/// A configurable request object which can be used to customize a query for `UserQuest`(s)
public struct GetUserQuestsRequestOptions {

    // Is a unique identifier used for pagination functionality
    let id: String

    // Is an array of `URLQueryItem` used to compile URL parameters
    let urlQueries: [URLQueryItem]

    /// Use `UserQuestIDs` to filter user quests by IDs
    let userQuestIDs: [String]

    /// Filter result set by the `UserQuest` status
    let status: UserQuestStatus?

    /// Filter result set by user profile ID
    /// When `profileID` is nil, current user profile will be used
    let profileID: String?

    let rewardStatus: QuestRewardStatus?

    public init(
        profileID: String? = nil,
        userQuestIDs: [String] = [],
        status: UserQuestStatus? = nil,
        rewardStatus: QuestRewardStatus? = nil
    ) {
        self.userQuestIDs = userQuestIDs
        self.profileID = profileID
        self.status = status
        self.rewardStatus = rewardStatus

        self.id = {
            var id: String = ""

            userQuestIDs.forEach { id += $0 }

            if let profileID = profileID {
                id += profileID
            }

            if let status = status?.rawValue {
                id += status
            }

            if let rewardStatus = rewardStatus {
                id += rewardStatus.rawValue
            }

            return id
        }()

        self.urlQueries = {

            /**
             Skip handing `profileID` parameter as a `URLQuery`.
             It is used by the `userQuestsUrlTemplate` template
             in the `whenGetUserQuestsURL` promise.
             **/

            var urlQueries = [URLQueryItem]()

            userQuestIDs.forEach { urlQueries.append(URLQueryItem(name: "id", value: $0)) }

            if let status = status {
                urlQueries.append(URLQueryItem(name: "status", value: status.rawValue))
            }

            if let rewardStatus = rewardStatus {
                urlQueries.append(URLQueryItem(name: "rewards_status", value: rewardStatus.rawValue))
            }

            return urlQueries
        }()
    }
}
