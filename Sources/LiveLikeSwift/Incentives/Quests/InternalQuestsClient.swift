//
//  InternalQuestsClient.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 6/9/22.
//

import Foundation
import LiveLikeCore

final class InternalQuestsClient {

    private var coreAPI: LiveLikeCoreAPIProtocol
    private var accessTokenVendor: AccessTokenVendor
    private let questsAPI: LiveLikeQuestsAPIProtocol
    private let userProfileVendor: UserProfileVendor
    private let paginatedController = PaginatedRequestController()
    private let networking: LLNetworking

    // Repo to keep track of all the `next` and `previous` urls
    private var pageResultURLRepo = [String: PaginatedResultURL]()

    init(
        coreAPI: LiveLikeCoreAPIProtocol,
        accessTokenVendor: AccessTokenVendor,
        userProfileVendor: UserProfileVendor,
        networking: LLNetworking,
        questsAPI: LiveLikeQuestsAPIProtocol = LiveLikeQuestsAPI()
    ) {
        self.questsAPI = questsAPI
        self.accessTokenVendor = accessTokenVendor
        self.coreAPI = coreAPI
        self.userProfileVendor = userProfileVendor
        self.networking = networking
    }

    private func updateUserQuestTaskProgress(
        postBody: UpdateUserQuestTaskProgressBody,
        completion: @escaping (Result<UserQuest, Error>) -> Void
    ) {

        firstly {
            Promises.zip(
                self.coreAPI.whenApplicationConfig,
                self.accessTokenVendor.whenAccessToken
            )
        }.then { applicationResource, accessToken in

            self.questsAPI.updateUserQuestTaskProgress(
                url: applicationResource.userQuestTaskProgressUrl,
                postBody: postBody,
                accessToken: accessToken
            )
        }.then { userQuestTaskProgress in
            completion(.success(userQuestTaskProgress.userQuest))
        }.catch { error in
            let failureError = QuestsClientError.failedUpdatingUserQuestTasks(error: error.localizedDescription)
            log.error(failureError.localizedDescription)
            completion(.failure(failureError))
        }
    }

    /// Helper to get first page url of Quests
    /// Does not modify `pageResultURLRepo`
    private func getQuestsFirstPageURL(
        options: GetQuestsRequestOptions
    ) -> Promise<URL> {
        return firstly {
            self.coreAPI.whenApplicationConfig
        }.then { applicationResource -> URL in
            guard var getQuestsURLComponents = URLComponents(
                url: applicationResource.questsUrl,
                resolvingAgainstBaseURL: false
            ) else {
                throw QuestsClientError.questsClientError(
                    error: "failing to build URLComponents from `questsUrl`"
                )
            }

            getQuestsURLComponents.queryItems?.append(contentsOf: options.urlQueries)

            guard let getQuestsURL = getQuestsURLComponents.url else {
                throw QuestsClientError.questsClientError(
                    error: "failing to create a `getQuestsURL`"
                )
            }

            return getQuestsURL
        }
    }

    /// Helper to get first page url of UserQuests
    /// Does not modify `pageResultURLRepo`
    private func getUserQuestsFirstPageURL(
        options: GetUserQuestsRequestOptions
    ) -> Promise<URL> {
        return firstly {
            Promises.zip(
                self.coreAPI.whenApplicationConfig,
                self.userProfileVendor.whenProfileResource
            )
        }.then { applicationResource, profileResource in
            try applicationResource.getUserQuestsURL(
                profileID: options.profileID ?? profileResource.id
            )
        }.then { getUserQuestURL -> URL in

            if options.urlQueries.count == 0 {
                return getUserQuestURL
            }

            guard var getUserQuestsURLComponents = URLComponents(
                url: getUserQuestURL,
                resolvingAgainstBaseURL: false
            ) else {
                throw QuestsClientError.questsClientError(
                    error: "failing to create `getUserQuestsURLComponents`"
                )
            }

            getUserQuestsURLComponents.queryItems?.append(contentsOf: options.urlQueries)

            guard let getUserQuestsURL = getUserQuestsURLComponents.url else {
                throw QuestsClientError.questsClientError(
                    error: "failing to create URL from `getUserQuestsURLComponents`"
                )
            }

            return getUserQuestsURL
        }
    }

    /// Helper to get a Quest by id
    /// Does not modify `pageResultURLRepo`
    private func getQuest(
        questID: String
    ) -> Promise<Quest> {
        return firstly {
            Promises.zip(
                getQuestsFirstPageURL(
                    options: GetQuestsRequestOptions(questIDs: [questID])
                ),
                accessTokenVendor.whenAccessToken
            )
        }.then { url, accessToken in
            self.questsAPI.getQuests(url: url, accessToken: accessToken)
        }.then { quests -> Quest in
            guard let quest = quests.items.first else {
                throw QuestsClientError.failedGettingQuests(
                    error: "Could not find quest with id \(questID)"
                )
            }
            return quest
        }
    }

    /// Helper to get a UserQuest by id
    /// Does not modify `pageResultURLRepo`
    private func getUserQuest(
        userQuestID: String
    ) -> Promise<UserQuest> {
        return firstly {
            Promises.zip(
                getUserQuestsFirstPageURL(
                    options: GetUserQuestsRequestOptions(
                        userQuestIDs: [userQuestID]
                    )
                ),
                accessTokenVendor.whenAccessToken
            )
        }.then { url, accessToken in
            self.questsAPI.getUserQuests(
                url: url,
                accessToken: accessToken
            )
        }.then { userQuests -> UserQuest in
            guard let userQuest = userQuests.items.first else {
                throw QuestsClientError.failedGettingQuests(
                    error: "Could not find user quest with id \(userQuestID)"
                )
            }
            return userQuest
        }
    }
}

extension InternalQuestsClient: QuestsClient {

    func getQuests(
        page: Pagination,
        options: GetQuestsRequestOptions,
        completion: @escaping (Result<[Quest], Error>) -> Void
    ) {

        let pageResultURLID = "getQuests_\(options.id)"
        firstly {
            Promises.zip(
                PaginationHelper.whenPaginationURL(
                    pageRepo: &self.pageResultURLRepo,
                    pageResultURLID: pageResultURLID,
                    page: page,
                    whenFirstPageURL: self.getQuestsFirstPageURL(options: options)
                ),
                self.accessTokenVendor.whenAccessToken
            )
        }.then { url, accessToken in
            self.questsAPI.getQuests(url: url, accessToken: accessToken)
        }.then { paginatedBadgeResource in

            self.pageResultURLRepo[pageResultURLID] = PaginatedResultURL(
                nextURL: paginatedBadgeResource.next,
                previousURL: paginatedBadgeResource.previous
            )

            completion(.success(paginatedBadgeResource.items))
        }.catch { error in
            let failureError = QuestsClientError.failedGettingQuests(error: error.localizedDescription)
            log.error(failureError.localizedDescription)
            completion(.failure(failureError))
        }

    }

    func getUserQuests(
        page: Pagination,
        options: GetUserQuestsRequestOptions,
        completion: @escaping (Result<[UserQuest], Error>) -> Void
    ) {
        let pageResultURLID = "getUserQuests_\(options.id)"
        firstly {
            Promises.zip(
                PaginationHelper.whenPaginationURL(
                    pageRepo: &self.pageResultURLRepo,
                    pageResultURLID: pageResultURLID,
                    page: page,
                    whenFirstPageURL: self.getUserQuestsFirstPageURL(
                        options: options
                    )
                ),
                self.accessTokenVendor.whenAccessToken
            )
        }.then { url, accessToken in
            self.questsAPI.getUserQuests(url: url, accessToken: accessToken)
        }.then { paginatedUserQuestResource in

            self.pageResultURLRepo[pageResultURLID] = PaginatedResultURL(
                nextURL: paginatedUserQuestResource.next,
                previousURL: paginatedUserQuestResource.previous
            )

            completion(.success(paginatedUserQuestResource.items))
        }.catch { error in
            let failureError = QuestsClientError.failedGettingUserQuests(error: error.localizedDescription)
            log.error(failureError.localizedDescription)
            completion(.failure(failureError))
        }
    }

    func startUserQuest(
        questID: String,
        completion: @escaping (Result<UserQuest, Error>) -> Void
    ) {

        firstly {
            Promises.zip(
                self.coreAPI.whenApplicationConfig,
                self.accessTokenVendor.whenAccessToken,
                self.userProfileVendor.whenProfileResource
            )
        }.then { applicationResource, accessToken, profileResource in

            return self.questsAPI.startUserQuest(
                url: applicationResource.userQuestsUrl,
                postBody: StartUserQuestBody(
                    profileId: profileResource.id,
                    questId: questID
                ),
                accessToken: accessToken
            )
        }.then { userQuestResource in
            completion(.success(userQuestResource))
        }.catch { error in
            let failureError = QuestsClientError.failedStartingUserQuest(error: error.localizedDescription)
            log.error(failureError.localizedDescription)
            completion(.failure(failureError))
        }
    }

    func updateUserQuestTasks(
        userQuestID: String,
        userQuestTaskIDs: [String],
        status: UserQuestTaskStatus,
        completion: @escaping (Result<UserQuest, Error>) -> Void
    ) {

        firstly {
            Promises.zip(
                self.coreAPI.whenApplicationConfig,
                self.accessTokenVendor.whenAccessToken
            )
        }.then { applicationResource, accessToken in

            var completedUserTasks = [UserQuestTaskStatusBody]()
            userQuestTaskIDs.forEach { userQuestTaskID in
                completedUserTasks.append(UserQuestTaskStatusBody(id: userQuestTaskID, status: status))
            }

            let patchBody = CompleteUserQuestTaskBody(userQuestTasks: completedUserTasks)

            let url = try applicationResource.getUserQuestDetailURL(
                userQuestID: userQuestID
            )

            return self.questsAPI.updateUserQuestTasks(
                url: url,
                patchBody: patchBody,
                accessToken: accessToken
            )
        }.then { userQuestResource in
            completion(.success(userQuestResource))
        }.catch { error in
            let failureError = QuestsClientError.failedUpdatingUserQuestTasks(error: error.localizedDescription)
            log.error(failureError.localizedDescription)
            completion(.failure(failureError))
        }
    }

    func incrementUserQuestTaskProgress(
        userQuestTaskID: String,
        customIncrement: Float? = nil,
        completion: @escaping (Result<UserQuest, Error>) -> Void
    ) {

        let postBody = UpdateUserQuestTaskProgressBody(
            userQuestTaskId: userQuestTaskID,
            customIncrement: customIncrement,
            customProgress: nil
        )

        updateUserQuestTaskProgress(postBody: postBody, completion: completion)
    }

    func setUserQuestTaskProgress(
        userQuestTaskID: String,
        progress: Float,
        completion: @escaping (Result<UserQuest, Error>) -> Void
    ) {

        let postBody = UpdateUserQuestTaskProgressBody(
            userQuestTaskId: userQuestTaskID,
            customIncrement: nil,
            customProgress: progress
        )

        updateUserQuestTaskProgress(postBody: postBody, completion: completion)
    }

    func getQuestRewards(
        page: Pagination,
        questID: String,
        completion: @escaping (Result<[QuestReward], Error>) -> Void
    ) {
        let pageResultURLID = "getQuestRewards_\(questID)"
        firstly {
            Promises.zip(
                PaginationHelper.whenPaginationURL(
                    pageRepo: &self.pageResultURLRepo,
                    pageResultURLID: pageResultURLID,
                    page: page,
                    whenFirstPageURL: self.getQuest(questID: questID).then {
                        return try $0.getQuestRewardsURL(questID: questID)
                    }
                ),
                self.accessTokenVendor.whenAccessToken
            )
        }.then { questRewardsURL, accessToken in
            self.questsAPI.getQuestRewards(
                url: questRewardsURL,
                accessToken: accessToken
            )
        }.then { questRewardsPage in
            self.pageResultURLRepo[pageResultURLID] = PaginatedResultURL(
                nextURL: questRewardsPage.next,
                previousURL: questRewardsPage.previous
            )
            completion(.success(questRewardsPage.items))
        }.catch { error in
            completion(.failure(error))
        }
    }

    func getUserQuestRewards(
        page: Pagination,
        userQuestID: String,
        rewardStatus: QuestRewardStatus?,
        completion: @escaping (Result<[UserQuestReward], Error>) -> Void
    ) {
        self.getUserQuestRewards(
            page: page,
            options: GetUserQuestRewardRequestOptions(
                userQuestIDs: [userQuestID],
                rewardStatus: rewardStatus
            ),
            completion: completion
        )
    }

    func getUserQuestRewards(
        page: Pagination,
        options: GetUserQuestRewardRequestOptions,
        completion: @escaping (Result<[UserQuestReward], Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                self.accessTokenVendor.whenAccessToken,
                self.coreAPI.whenApplicationConfig
            )
        }.then { accessToken, application in
            self.paginatedController.performExecute(
                paginatedRequest: GetUserQuestRewardsRequest(
                    networking: self.networking,
                    accessToken: accessToken.asString,
                    options: options
                ),
                rootURL: application.userQuestRewardsUrl,
                page: page,
                completion: completion
            )
        }.catch {
            completion(.failure($0))
        }
    }

    func claimUserQuestRewards(
        userQuestID: String,
        completion: @escaping (Result<UserQuest, Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                self.accessTokenVendor.whenAccessToken,
                self.coreAPI.whenApplicationConfig
            )
        }.then { accessToken, application in
            self.questsAPI.claimUserQuestRewards(
                url: try application.getUserQuestDetailURL(
                    userQuestID: userQuestID
                ),
                accessToken: accessToken
            )
        }.then { userQuest in
            return completion(.success(userQuest))
        }.catch { error in
            return completion(.failure(error))
        }
    }
}
