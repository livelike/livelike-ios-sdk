//
//  GetProfileRelationshipsOptions.swift
//  
//
//  Created by Mike Moloksher on 2/22/23.
//

/// Can be used to customize `getProfileRelationships()` query
public struct GetProfileRelationshipsOptions {

    /// A key that represents a relationship type. Interface`getProfileRelationshipTypes()`
    /// can be used to rerieve all relationship types
    public let relationshipTypeKey: String?

    /// The source profile ID from which a relationship originates
    public let fromProfileID: String?

    /// The target profile ID to which a relationship points
    public let toProfileID: String?

    public init(
        relationshipTypeKey: String? = nil,
        fromProfileID: String? = nil,
        toProfileID: String? = nil
    ) {
        self.relationshipTypeKey = relationshipTypeKey
        self.fromProfileID = fromProfileID
        self.toProfileID = toProfileID
    }
}
