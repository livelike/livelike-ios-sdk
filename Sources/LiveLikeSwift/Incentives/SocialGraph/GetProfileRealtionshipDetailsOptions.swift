//
//  GetProfileRealtionshipDetailsOptions.swift
//  
//
//  Created by Mike Moloksher on 2/22/23.
//

/// Can be used to customize `getProfileRelationshipDetails()` query
public struct GetProfileRealtionshipDetailsOptions {
    public let profileRelationshipID: String?

    public init(profileRelationshipID: String?) {
        self.profileRelationshipID = profileRelationshipID
    }
}
