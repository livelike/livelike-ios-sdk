//
//  DeleteProfileRelationshipRequest.swift
//  
//
//  Created by Mike Moloksher on 2/22/23.
//

import Foundation
import LiveLikeCore

struct DeleteProfileRelationshipRequest {
    
    private let accessToken: String
    private let networking: LLNetworking
    private let options: DeleteProfileRelationshipOptions
    
    init(
        accessToken: String,
        networking: LLNetworking,
        options: DeleteProfileRelationshipOptions
    ) {
        self.accessToken = accessToken
        self.networking = networking
        self.options = options
    }
    
}

extension DeleteProfileRelationshipRequest: LLRequest {
    typealias ResponseType = Bool
    
    func execute(
        url: URL,
        completion: @escaping (Result<Bool, Error>) -> Void
    ) {
        
        /*
         * Currently only supports deleting by profile relationship ID
         * When `DeleteProfileRelationshipOptions` will expand
         * this code will have to be refactored
         */
        
        guard let profileRelationshipID = self.options.profileRelationshipID else {
            return completion(.failure(SocialGraphClientError.profileRelationshipIDRequiredForDeletionRequest))
        }
        struct Payload: Encodable {
            let profile_relationship_id: String
        }
        let payload = Payload(
            profile_relationship_id: profileRelationshipID
        )
        
        let resource = Resource<Bool>.init(
            url: url,
            method: .delete(payload),
            accessToken: accessToken
        )
        networking.task(resource, completion: completion)
    }
    
}


