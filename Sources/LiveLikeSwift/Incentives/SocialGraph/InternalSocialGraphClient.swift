//
//  InternalSocialGraphClient.swift
//  
//
//  Created by Mike Moloksher on 2/16/23.
//

import Foundation
import LiveLikeCore

public protocol SocialGraphClient {
    
    /// Retrieve profile relationships of a user profile
    /// using the `options` parameter
    func getProfileRelationships(
        page: Pagination,
        options: GetProfileRelationshipsOptions?,
        completion: @escaping (Result<[ProfileRelationship], Error>) -> Void
    )
    
    /// Retrieve all available profile relationship types
    func getProfileRelationshipTypes(
        page: Pagination,
        options: GetProfileRelationshipTypesOptions?,
        completion: @escaping (Result<[ProfileRelationshipType], Error>) -> Void
    )
    
    /// Retrieve profile relationship details by a profile relationship ID
    func getProfileRealtionshipDetails(
        options: GetProfileRealtionshipDetailsOptions,
        completion: @escaping (Result<ProfileRelationship, Error>) -> Void
    )
    
    /// Create a relationship between two user profiles
    func createProfileRelationship(
        options: CreateProfileRelationshipOptions,
        completion: @escaping (Result<ProfileRelationship, Error>) -> Void
    )
    
    /// Delete a profile relationship
    func deleteProfileRelationship(
        options: DeleteProfileRelationshipOptions,
        completion: @escaping (Result<Void, Error>) -> Void
    )
}

final class InternalSocialGraphClient {

    private var coreAPI: LiveLikeCoreAPIProtocol
    private var accessTokenVendor: AccessTokenVendor
    private let userProfileVendor: UserProfileVendor
    private let networking: LLNetworking
    private let paginatedRequestController = PaginatedRequestController()

    init(
        coreAPI: LiveLikeCoreAPIProtocol,
        accessTokenVendor: AccessTokenVendor,
        userProfileVendor: UserProfileVendor,
        networking: LLNetworking
    ) {
        self.coreAPI = coreAPI
        self.accessTokenVendor = accessTokenVendor
        self.userProfileVendor = userProfileVendor
        self.networking = networking
    }
}

extension InternalSocialGraphClient: SocialGraphClient {
    
    func getProfileRelationships(
        page: Pagination,
        options: GetProfileRelationshipsOptions? = nil,
        completion: @escaping (Result<[ProfileRelationship], Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                self.coreAPI.whenApplicationConfig,
                self.accessTokenVendor.whenAccessToken
            )
        }.then(on: DispatchQueue.global()) { appConfig, accessToken in
            self.paginatedRequestController.performExecute(
                paginatedRequest: GetProfileRelationshipsRequest(
                    clientID: appConfig.clientId,
                    options: options,
                    accessToken: accessToken.asString,
                    networking: self.networking
                ),
                rootURL: appConfig.profileRelationshipsUrl,
                page: page,
                completion: completion
            )
        }
    }
    
    func getProfileRelationshipTypes(
        page: Pagination,
        options: GetProfileRelationshipTypesOptions? = nil,
        completion: @escaping (Result<[ProfileRelationshipType], Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                self.coreAPI.whenApplicationConfig,
                self.accessTokenVendor.whenAccessToken
            )
        }.then(on: DispatchQueue.global()) { appConfig, accessToken in
            self.paginatedRequestController.performExecute(
                paginatedRequest: GetProfileRelationshipTypesRequest(
                    clientID: appConfig.clientId,
                    options: options,
                    accessToken: accessToken.asString,
                    networking: self.networking
                ),
                rootURL: appConfig.profileRelationshipTypesUrl,
                page: page,
                completion: completion
            )
        }
    }
    
    func getProfileRealtionshipDetails(
        options: GetProfileRealtionshipDetailsOptions,
        completion: @escaping (Result<ProfileRelationship, Error>) -> Void
    ) {
        
        firstly {
            Promises.zip(
                self.coreAPI.whenApplicationConfig,
                self.accessTokenVendor.whenAccessToken
            )
        }.then(on: DispatchQueue.global()) { appConfig, accessToken in
            
            /*
             * `options.profileRelationshipID` is
             * intended to be an optional parameter
             * but the API currently requires it,
             * we can remove this check when the API is updated”
             */
            
            guard let profileRelationshipID = options.profileRelationshipID else {
                return Promise(error: SocialGraphClientError.profileRelationshipIDRequiredForDetailsRequest)
            }
            
            return try GetProfileRealtionshipDetailsRequest(
                accessToken: accessToken.asString,
                networking: self.networking
            ).execute(
                url: appConfig.getProfileRelationshipsDetailURL(
                    profileRelationshipID: profileRelationshipID
                )
            )
        }.then { profileReleationship in
            completion(.success(profileReleationship))
        }.catch { error in
            log.error(error.localizedDescription)
            completion(.failure(error))
        }
    }
    
    func createProfileRelationship(
        options: CreateProfileRelationshipOptions,
        completion: @escaping (Result<ProfileRelationship, Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                self.coreAPI.whenApplicationConfig,
                self.accessTokenVendor.whenAccessToken
            )
        }.then(on: DispatchQueue.global()) { appConfig, accessToken in
            return CreateProfileRelationshipRequest(
                clientID: appConfig.clientId,
                options: options,
                accessToken: accessToken.asString,
                networking: self.networking
            ).execute(url: appConfig.profileRelationshipsUrl)
        }.then { profileRelationship in
            completion(.success(profileRelationship))
        }.catch { error in
            log.error(error.localizedDescription)
            completion(.failure(error))
        }
    }
    
    func deleteProfileRelationship(
        options: DeleteProfileRelationshipOptions,
        completion: @escaping (Result<Void, Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                self.coreAPI.whenApplicationConfig,
                self.accessTokenVendor.whenAccessToken
            )
        }.then(on: DispatchQueue.global()) { appConfig, accessToken -> Promise<Bool> in
            
            /*
             * Currently only supports deleting by profile relationship ID
             * When `DeleteProfileRelationshipOptions` will expand
             * this code will have to be refactored
             */
            
            guard let profileRelationshipID = options.profileRelationshipID else {
                return Promise(error: SocialGraphClientError.profileRelationshipIDRequiredForDeletionRequest)
            }
            
            return try DeleteProfileRelationshipRequest(
                accessToken: accessToken.asString,
                networking: self.networking,
                options: options
            ).execute(
                url: appConfig.getProfileRelationshipsDetailURL(
                    profileRelationshipID: profileRelationshipID
                )
            )
        }.then { _ in
            completion(.success(()))
        }.catch { error in
            log.error(error.localizedDescription)
            completion(.failure(error))
        }
        
    }

}
