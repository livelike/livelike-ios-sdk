//
//  GetProfileRelationshipsRequest.swift
//  
//
//  Created by Mike Moloksher on 2/17/23.
//

import Foundation
import LiveLikeCore

struct GetProfileRelationshipsRequest {

    private let clientID: String
    private let options: GetProfileRelationshipsOptions?
    private let accessToken: String
    private let networking: LLNetworking

    init(
        clientID: String,
        options: GetProfileRelationshipsOptions?,
        accessToken: String,
        networking: LLNetworking
    ) {
        self.clientID = clientID
        self.options = options
        self.accessToken = accessToken
        self.networking = networking
    }

    private func buildURL(baseURL: URL) throws -> URL {
        guard var components = URLComponents(url: baseURL, resolvingAgainstBaseURL: false) else {
            throw URLComponentError.failedToInitURLComponents(fromURL: baseURL)
        }

        var urlQueries = [URLQueryItem]()

        urlQueries.append(URLQueryItem(name: "client_id", value: clientID))

        if let toProfileID = options?.toProfileID {
            urlQueries.append(URLQueryItem(name: "to_profile_id", value: toProfileID))
        }

        if let fromProfileID = options?.fromProfileID {
            urlQueries.append(URLQueryItem(name: "from_profile_id", value: fromProfileID))
        }

        if let relationshipTypeKey = options?.relationshipTypeKey {
            urlQueries.append(URLQueryItem(name: "relationship_type_key", value: relationshipTypeKey))
        }

        components.queryItems = urlQueries

        guard let url = components.url else {
            throw URLComponentError.urlIsNil
        }

        return url
    }
}

extension GetProfileRelationshipsRequest: LLRequest {
    typealias ResponseType = PaginatedResult<ProfileRelationship>

    func execute(
        url: URL,
        completion: @escaping (Result<PaginatedResult<ProfileRelationship>, Error>) -> Void
    ) {
        do {
            let resource = try Resource<PaginatedResult<ProfileRelationship>>(
                get: buildURL(baseURL: url),
                accessToken: accessToken
            )
            networking.task(resource, completion: completion)
        } catch {
            completion(.failure(error))
        }
    }
}

extension GetProfileRelationshipsRequest: LLPaginatedRequest {
    typealias ElementType = ProfileRelationship

    var requestID: String {
        var id = "GetProfileRelationshipsRequest"
        id += options?.fromProfileID ?? ""
        id += options?.toProfileID ?? ""
        id += options?.relationshipTypeKey ?? ""
        return id
    }
}
