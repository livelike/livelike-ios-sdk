//
//  ProfileRelationship.swift
//  
//
//  Created by Mike Moloksher on 2/22/23.
//
import Foundation

/// A resource representing a profile relationship
public struct ProfileRelationship: Decodable {
    
    /// A unqiue identifier of the profile relationship
    public let id: String
    
    /// The url which can be utilized to retrieve the profile relationship resource
    public let url: URL
    
    /// Date and time when the profile relationship was created
    public let createdAt: Date
    
    /// Represents the origin profile of the relationship
    public let fromProfile: ProfileResource
    
    /// Represents the target profile of the relationship
    public let toProfile: ProfileResource
    
    /// Represents the relationship type between the `fromProfile` and `toProfile`
    public let relationshipType: ProfileRelationshipType
}
