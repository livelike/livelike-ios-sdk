//
//  CreateProfileRelationshipOptions.swift
//  
//
//  Created by Mike Moloksher on 2/22/23.
//

/// Used in `createProfileRelationship()` interface to create a relationship
/// between two user profiles
public struct CreateProfileRelationshipOptions {

    /// a user profile that is requesting a relationship to be created
    public let fromProfileID: String

    /// a user profile towards which a relationship is requested
    public let toProfileID: String

    /// a key of a relationship type
    /// interface `getProfileRelationshipTypes()` can be utilized to retrieve all keys
    public let relationshipTypeKey: String

    public init(fromProfileID: String, toProfileID: String, relationshipTypeKey: String) {
        self.fromProfileID = fromProfileID
        self.toProfileID = toProfileID
        self.relationshipTypeKey = relationshipTypeKey
    }
}
