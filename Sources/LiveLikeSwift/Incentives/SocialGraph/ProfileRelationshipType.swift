//
//  ProfileRelationshipType.swift
//  
//
//  Created by Mike Moloksher on 2/22/23.
//

import Foundation

/// A resource representing a profile relationship type
public struct ProfileRelationshipType {
    
    /// A unqiue identifier of the profile relationship type
    public let id: String
    
    /// The url which can be utilized to retrieve the profile relationship type resource
    public let url: URL
    
    /// Date and time when the profile relationship type was created
    public let createdAt: Date
    
    /// Client ID the profile relationship type associated with
    public let clientID: String
    
    /// Name of the profile relationship type
    public let name: String
    
    /// Key of the profile relationship type that
    /// could be utilized in SDK interfaces to reference this profile relationship type
    public let key: String
}

extension ProfileRelationshipType: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case url
        case createdAt
        case clientID = "clientId"
        case name
        case key
    }
}
