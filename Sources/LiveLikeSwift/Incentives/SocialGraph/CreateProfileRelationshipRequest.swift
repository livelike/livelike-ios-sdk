//
//  CreateProfileRelationshipRequest.swift
//  
//
//  Created by Mike Moloksher on 2/21/23.
//

import Foundation
import LiveLikeCore

struct CreateProfileRelationshipRequest {
    
    private let clientID: String
    private let accessToken: String
    private let networking: LLNetworking
    private let options: CreateProfileRelationshipOptions
    
    init(
        clientID: String,
        options: CreateProfileRelationshipOptions,
        accessToken: String,
        networking: LLNetworking
    ) {
        self.clientID = clientID
        self.accessToken = accessToken
        self.networking = networking
        self.options = options
    }
    
}

extension CreateProfileRelationshipRequest: LLRequest {
    typealias ResponseType = ProfileRelationship
    
    func execute(
        url: URL,
        completion: @escaping (Result<ProfileRelationship, Error>) -> Void
    ) {
        struct Payload: Encodable {
            let clientId: String
            let relationshipTypeKey: String
            let fromProfileId: String
            let toProfileId: String
        }
        let payload = Payload(
            clientId: self.clientID,
            relationshipTypeKey: options.relationshipTypeKey,
            fromProfileId: options.fromProfileID,
            toProfileId: options.toProfileID
        )
        
        let resource = Resource<ProfileRelationship>.init(
            url: url,
            method: .post(payload),
            accessToken: accessToken
        )
        networking.task(resource, completion: completion)
    }
    
}
