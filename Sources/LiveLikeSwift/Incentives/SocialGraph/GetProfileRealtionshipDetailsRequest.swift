//
//  GetProfileRealtionshipDetailsRequest.swift
//
//
//  Created by Mike Moloksher on 2/21/23.
//

import Foundation
import LiveLikeCore

struct GetProfileRealtionshipDetailsRequest {

    private let accessToken: String
    private let networking: LLNetworking

    init(
        accessToken: String,
        networking: LLNetworking
    ) {
        self.accessToken = accessToken
        self.networking = networking
    }
}

extension GetProfileRealtionshipDetailsRequest: LLRequest {
    typealias ResponseType = ProfileRelationship

    func execute(
        url: URL,
        completion: @escaping (Result<ProfileRelationship, Error>) -> Void
    ) {
        let resource = Resource<ProfileRelationship>(
            get: url,
            accessToken: accessToken
        )
        networking.task(resource, completion: completion)
    }
}
