//
//  DeleteProfileRelationshipOptions.swift
//  
//
//  Created by Mike Moloksher on 2/22/23.
//

/// Used in `deleteProfileRelationship()` interface to delete a profile relationship
public struct DeleteProfileRelationshipOptions {
    public let profileRelationshipID: String?

    public init(
        profileRelationshipID: String?
    ) {
        self.profileRelationshipID = profileRelationshipID
    }
}
