//
//  GetProfileRelationshipTypesOptions.swift
//  
//
//  Created by Mike Moloksher on 2/22/23.
//

/// Can be used to customize `getProfileRelationshipTypes()` query
public struct GetProfileRelationshipTypesOptions {
    public init() {}
}
