//
//  GetProfileRelationshipTypesRequest.swift
//  
//
//  Created by Mike Moloksher on 2/17/23.
//

import Foundation
import LiveLikeCore

struct GetProfileRelationshipTypesRequest {

    private let clientID: String
    private let options: GetProfileRelationshipTypesOptions?
    private let accessToken: String
    private let networking: LLNetworking

    init(
        clientID: String,
        options: GetProfileRelationshipTypesOptions?,
        accessToken: String,
        networking: LLNetworking
    ) {
        self.clientID = clientID
        self.options = options
        self.accessToken = accessToken
        self.networking = networking
    }

    private func buildURL(baseURL: URL) throws -> URL {
        guard var components = URLComponents(url: baseURL, resolvingAgainstBaseURL: false) else {
            throw URLComponentError.failedToInitURLComponents(fromURL: baseURL)
        }

        var urlQueries = [URLQueryItem]()

        urlQueries.append(URLQueryItem(name: "client_id", value: clientID))

        components.queryItems = urlQueries

        guard let url = components.url else {
            throw URLComponentError.urlIsNil
        }

        return url
    }
}

extension GetProfileRelationshipTypesRequest: LLRequest {
    typealias ResponseType = PaginatedResult<ProfileRelationshipType>

    func execute(
        url: URL,
        completion: @escaping (Result<PaginatedResult<ProfileRelationshipType>, Error>) -> Void
    ) {
        do {
            let resource = try Resource<PaginatedResult<ProfileRelationshipType>>(
                get: buildURL(baseURL: url),
                accessToken: accessToken
            )
            networking.task(resource, completion: completion)
        } catch {
            completion(.failure(error))
        }
    }
}

extension GetProfileRelationshipTypesRequest: LLPaginatedRequest {
    typealias ElementType = ProfileRelationshipType

    var requestID: String {
        return "GetProfileRelationshipTypesRequest"
    }
}
