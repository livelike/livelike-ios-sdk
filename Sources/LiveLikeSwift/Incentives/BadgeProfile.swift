//
//  BadgeProfile.swift
//  EngagementSDK
//
//  Created by Keval Shah on 29/11/22.
//

import Foundation

public struct BadgeProfile {
    /// The date and time at which the badge was awarded
    public let awardedAt: Date

    /// ID of the badge
    public let badgeID: String

    /// The profile the badge has been awarded to
    public let profile: ProfileResource
}

extension BadgeProfile: Decodable {
    enum CodingKeys: String, CodingKey {
        case awardedAt
        case badgeID = "badgeId"
        case profile
    }
}
