//
//  ProfileLeaderboardsResult.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

public struct ProfileLeaderboardsResult {
    public let entries: [ProfileLeaderboard]
    public let total: Int
    public let hasPrevious: Bool
    public let hasNext: Bool
}
