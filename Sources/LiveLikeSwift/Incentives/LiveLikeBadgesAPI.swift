//
//  LiveLikeBadgesAPI.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 7/6/21.
//

import Foundation
import LiveLikeCore

protocol LiveLikeBadgesAPIProtocol {
    func getProfileBadges(url: URL) -> Promise<PaginatedResult<ProfileBadge>>
    func getProfileBadgeProgress(url: URL) -> Promise<[BadgeProgress]>
    func getApplicationBadges(url: URL) -> Promise<PaginatedResult<Badge>>
    func getBadgeProfiles(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<BadgeProfile>>
}

class LiveLikeBadgesAPI: LiveLikeBadgesAPIProtocol {
    func getProfileBadges(url: URL) -> Promise<PaginatedResult<ProfileBadge>> {
        let resource = Resource<PaginatedResult<ProfileBadge>>(get: url)
        return LiveLike.networking.load(resource)
    }

    func getApplicationBadges(url: URL) -> Promise<PaginatedResult<Badge>> {
        let resource = Resource<PaginatedResult<Badge>>(get: url)
        return LiveLike.networking.load(resource)
    }

    func getProfileBadgeProgress(url: URL) -> Promise<[BadgeProgress]> {
        let resource = Resource<[BadgeProgress]>(get: url)
        return LiveLike.networking.load(resource)
    }

    func getBadgeProfiles(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<BadgeProfile>> {
        let resource = Resource<PaginatedResult<BadgeProfile>>(get: url, accessToken: accessToken.asString)
        return LiveLike.networking.load(resource)
    }
}
