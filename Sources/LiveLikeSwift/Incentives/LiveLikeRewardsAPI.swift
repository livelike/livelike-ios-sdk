//
//  LiveLikeRewardsAPI.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 9/20/21.
//

import Foundation
import LiveLikeCore

protocol LiveLikeRewardsAPIProtocol {

    func transferRewardItemAmount(
        url: URL,
        accessToken: AccessToken,
        amount: Int,
        recipientProfileID: String,
        rewardItemID: String
    ) -> Promise<RewardItemTransfer>

    func getRewardItemBalances(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<RewardItemBalance>>

    func getRewardItemTransfers(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<RewardItemTransfer>>

    func getRewardTransactions(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<RewardTransaction>>
}

class LiveLikeRewardsAPI: LiveLikeRewardsAPIProtocol {
    func transferRewardItemAmount(
        url: URL,
        accessToken: AccessToken,
        amount: Int,
        recipientProfileID: String,
        rewardItemID: String
    ) -> Promise<RewardItemTransfer> {

        struct TransferRewardItemAmountBody: Encodable {
            var recipientProfileId: String
            var rewardItemId: String
            var rewardItemAmount: Int
        }

        let resource = Resource<RewardItemTransfer>(
            url: url,
            method: .post(
                TransferRewardItemAmountBody(
                    recipientProfileId: recipientProfileID,
                    rewardItemId: rewardItemID,
                    rewardItemAmount: amount
                )
            ),
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func getRewardItemTransfers(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<RewardItemTransfer>> {
        let resource = Resource<PaginatedResult<RewardItemTransfer>>(
            get: url,
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func getRewardItemBalances(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<RewardItemBalance>> {
        let resource = Resource<PaginatedResult<RewardItemBalance>>(
            get: url,
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func getRewardTransactions(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<RewardTransaction>> {
        let resource = Resource<PaginatedResult<RewardTransaction>>(
            get: url,
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }
}
