//
//  LiveLikeQuestsAPI.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 6/9/22.
//

import Foundation
import LiveLikeCore

struct StartUserQuestBody: Encodable {
    let profileId: String
    let questId: String
}

struct UserQuestTaskStatusBody: Encodable {
    let id: String
    let status: UserQuestTaskStatus
}

struct CompleteUserQuestTaskBody: Encodable {
    let userQuestTasks: [UserQuestTaskStatusBody]
}

struct UpdateUserQuestTaskProgressBody: Encodable {
    let userQuestTaskId: String
    let customIncrement: Float?
    let customProgress: Float?
}

struct UserQuestTaskProgress: Codable {
    let id: String
    let customIncrement: Float?
    let customProgress: Float?
    let userQuest: UserQuest
}

protocol LiveLikeQuestsAPIProtocol {
    func getQuests(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<Quest>>

    func getUserQuests(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<UserQuest>>

    func startUserQuest(
        url: URL,
        postBody: StartUserQuestBody,
        accessToken: AccessToken
    ) -> Promise<UserQuest>

    func updateUserQuestTasks(
        url: URL,
        patchBody: CompleteUserQuestTaskBody,
        accessToken: AccessToken
    ) -> Promise<UserQuest>

    func updateUserQuestTaskProgress(
        url: URL,
        postBody: UpdateUserQuestTaskProgressBody,
        accessToken: AccessToken
    ) -> Promise<UserQuestTaskProgress>

    func getQuestRewards(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<QuestReward>>

    func getUserQuestRewards(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<UserQuestReward>>

    func claimUserQuestRewards(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<UserQuest>
}

class LiveLikeQuestsAPI: LiveLikeQuestsAPIProtocol {

    func getQuests(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<Quest>> {

        let resource = Resource<PaginatedResult<Quest>>(get: url, accessToken: accessToken.asString)
        return LiveLike.networking.load(resource)
    }

    func getUserQuests(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<UserQuest>> {

        let resource = Resource<PaginatedResult<UserQuest>>(get: url, accessToken: accessToken.asString)
        return LiveLike.networking.load(resource)
    }

    func startUserQuest(
        url: URL,
        postBody: StartUserQuestBody,
        accessToken: AccessToken
    ) -> Promise<UserQuest> {

        let resource = Resource<UserQuest>.init(
            url: url,
            method: .post(postBody),
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func updateUserQuestTasks(
        url: URL,
        patchBody: CompleteUserQuestTaskBody,
        accessToken: AccessToken
    ) -> Promise<UserQuest> {

        let resource = Resource<UserQuest>.init(
            url: url,
            method: .patch(patchBody),
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func updateUserQuestTaskProgress(
        url: URL,
        postBody: UpdateUserQuestTaskProgressBody,
        accessToken: AccessToken
    ) -> Promise<UserQuestTaskProgress> {

        let resource = Resource<UserQuestTaskProgress>.init(
            url: url,
            method: .post(postBody),
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func getQuestRewards(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<QuestReward>> {
        let resource = Resource<PaginatedResult<QuestReward>>(
            get: url,
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func getUserQuestRewards(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<UserQuestReward>> {
        let resource = Resource<PaginatedResult<UserQuestReward>>(
            get: url,
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func claimUserQuestRewards(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<UserQuest> {
        struct PatchBody: Encodable {
            var rewardsStatus: QuestRewardStatus
        }
        let resource = Resource<UserQuest>(
            url: url,
            method: .patch(PatchBody(rewardsStatus: .claimed)),
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }
}
