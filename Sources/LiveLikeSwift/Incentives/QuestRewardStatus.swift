//
//  QuestRewardStatus.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 8/10/22.
//

/// An enum indicating the status of a User Quest reward
public enum QuestRewardStatus: String {
    case claimed
    case unclaimed
}

extension QuestRewardStatus: Codable { }
