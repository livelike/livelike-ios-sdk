//
//  UserQuestReward.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 8/10/22.
//

/// The rewards associated to a `UserQuest`
public struct UserQuestReward {
    public let id: String
    /// Determines whether the reward has been claimed
    public let rewardStatus: QuestRewardStatus
    /// The details of the reward
    public let questReward: QuestReward
}

extension UserQuestReward: Decodable { }
