//
//  GetApplicationRewardItemsRequest.swift
//  
//
//  Created by Jelzon Monzon on 1/30/23.
//

import Foundation
import LiveLikeCore

struct GetApplicationRewardItemsRequest {
    
    private let options: GetApplicationRewardItemsRequestOptions?
    private let clientID: String
    private let accessToken: String
    private let networking: LLNetworking
    
    init(
        options: GetApplicationRewardItemsRequestOptions?,
        clientID: String,
        accessToken: String,
        networking: LLNetworking
    ) {
        self.options = options
        self.clientID = clientID
        self.accessToken = accessToken
        self.networking = networking
    }
    
    private func buildURL(baseURL: URL) throws -> URL {
        guard var components = URLComponents(url: baseURL, resolvingAgainstBaseURL: false) else {
            throw URLComponentError.failedToInitURLComponents(fromURL: baseURL)
        }
        
        var urlQueries = [URLQueryItem]()
        
        urlQueries.append(URLQueryItem(name: "client_id", value: clientID))

        options?.attributes?.forEach { attribute in
            urlQueries.append(attribute.queryItem())
        }
        
        components.queryItems = urlQueries
        
        guard let url = components.url else {
            throw URLComponentError.urlIsNil
        }
        
        return url
    }
}

extension GetApplicationRewardItemsRequest: LLRequest {
    typealias ResponseType = PaginatedResult<RewardItem>
    
    func execute(
        url: URL,
        completion: @escaping (Result<PaginatedResult<RewardItem>, Error>) -> Void
    ) {
        do {
            let resource = try Resource<PaginatedResult<RewardItem>>(
                get: buildURL(baseURL: url),
                accessToken: accessToken
            )
            networking.task(resource, completion: completion)
        } catch {
            completion(.failure(error))
        }
    }
}

extension GetApplicationRewardItemsRequest: LLPaginatedRequest {
    typealias ElementType = RewardItem
    
    var requestID: String {
        var id: String = "getApplicationRewardItems"
        options?.attributes?.forEach({ attribute in
            id += attribute.value
        })
        return id
    }
}
