//
//  GetInvokedRewardActionsRequest.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 8/22/22.
//

import Foundation
import LiveLikeCore

struct GetInvokedRewardActionsRequest: LLRequest {
    
    typealias ResponseType = PaginatedResult<InvokedRewardAction>
    
    private let clientID: String
    private let options: GetInvokedRewardActionsOptions?
    private let accessToken: AccessToken
    private let networking: LLNetworking
    
    init(
        clientID: String,
        options: GetInvokedRewardActionsOptions?,
        accessToken: AccessToken,
        networking: LLNetworking
    ) {
        self.clientID = clientID
        self.options = options
        self.accessToken = accessToken
        self.networking = networking
    }
    
    func execute(
        url: URL,
        completion: @escaping (Result<ResponseType, Error>) -> Void
    ) {
        guard var urlComponents = URLComponents(
            url: url,
            resolvingAgainstBaseURL: false
        ) else {
            return completion(.failure(URLComponentError.failedToInitURLComponents(fromURL: url)))
        }
        
        urlComponents.queryItems = [URLQueryItem(name: "client_id", value: clientID)]
        
        if let options = self.options {
            urlComponents.queryItems?.append(contentsOf: buildQueryItems(options: options))
        }
        
        guard let url = urlComponents.url else {
            return completion(.failure(URLComponentError.urlIsNil))
        }
        
        let resource = Resource<PaginatedResult<InvokedRewardAction>>(
            get: url,
            accessToken: accessToken.asString
        )
        networking.task(resource, completion: completion)
    }
    
    private func buildQueryItems(
        options: GetInvokedRewardActionsOptions
    ) -> [URLQueryItem] {
        var queryItems = [URLQueryItem]()
        
        if let profileID = options.programID {
            queryItems.append(URLQueryItem(name: "program_id", value: profileID))
        }
        
        if let rewardActionKeys = options.rewardActionKeys {
            queryItems.append(contentsOf: rewardActionKeys.map {
                URLQueryItem(name: "reward_action_key", value: $0)
            })
        }
        
        options.profileIDs?.forEach({ userProfileID in
            queryItems.append(URLQueryItem(name: "profile_id", value: userProfileID))
        })
        
        if let attributes = options.attributes {
            queryItems.append(contentsOf: attributes.map { $0.queryItem() })
        }
        
        return queryItems
    }
    
}

extension GetInvokedRewardActionsRequest: LLPaginatedRequest {
    typealias ElementType = InvokedRewardAction
    
    var requestID: String {
        var id = "getInvokedRewardActions"
        id += options?.programID ?? ""
        id += options?.rewardActionKeys?.reduce("", +) ?? ""
        id += options?.profileIDs?.reduce("", +) ?? ""
        return id
    }
}
