//
//  RewardActionInfo.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 8/25/22.
//

/// An object representing a Reward Action
public struct RewardActionInfo {
    /// The id of the reward action
    public let id: String
    public let clientID: String
    /// The reward action key. Use this to invoke this reward action.
    public let key: String
    /// The  name of this reward action.
    public let name: String
    /// An optional description of this reward action.
    public let text: String?
}

extension RewardActionInfo: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case clientID = "clientId"
        case key
        case name
        case text = "description"
    }
}
