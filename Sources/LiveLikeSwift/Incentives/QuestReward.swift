//
//  QuestReward.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 8/10/22.
//

/// A reward associated to a Quest
public struct QuestReward {
    public let id: String
    /// The id of the reward item
    public let rewardItemID: String
    /// The amount of reward item
    public let rewardItemAmount: Int
}

extension QuestReward: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case rewardItemID = "rewardItemId"
        case rewardItemAmount
    }
}
