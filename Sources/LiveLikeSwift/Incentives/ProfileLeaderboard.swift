//
//  ProfileLeaderboard.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

public struct ProfileLeaderboard {
    public let entry: ProfileLeaderboardEntry
    public let leaderboard: Leaderboard
    public let profileNickname: String
    let clientID: String
    public let profileID: String
}

extension ProfileLeaderboard: Decodable {
    enum CodingKeys: String, CodingKey {
        case entry
        case leaderboard
        case profileNickname
        case clientID = "clientId"
        case profileID = "profileId"
    }
}
