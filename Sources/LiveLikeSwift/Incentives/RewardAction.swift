//
//  RewardAction.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

/// The source of a Reward
public enum RewardAction {

    /// Information to associate a Reward to a Widget
    public struct WidgetInfo {
        /// The widget id
        public let id: String
        /// The widget kind
        public let kind: WidgetKind
    }

    public typealias PredictionInfo = WidgetInfo
    public typealias QuizInfo = WidgetInfo
    public typealias PollInfo = WidgetInfo

    // The user has voted on a poll
    case pollVoted(PollInfo)

    // The user has answered a quiz
    case quizAnswered(QuizInfo)

    // The user has answered a quiz correctly
    case quizCorrect(QuizInfo)

    // The user has answered a prediction
    case predictionMade(PredictionInfo)

    // The user has answered a prediction correctly
    case predictionCorrect(PredictionInfo)

    // The user has earned rewards from an undefined action
    case undefined

    init(rewardActionResource: RewardActionResource, widgetInfo: WidgetInfo) {
        switch rewardActionResource {
        case .pollVoted:
            self = .pollVoted(widgetInfo)
        case .quizAnswered:
            self = .quizAnswered(widgetInfo)
        case .quizCorrect:
            self = .quizCorrect(widgetInfo)
        case .predictionMade:
            self = .predictionMade(widgetInfo)
        case .predictionCorrect:
            self = .predictionCorrect(widgetInfo)
        case .undefined:
            self = .undefined
        }
    }
}
