//
//  Reward.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

/// Describes an earned Reward
public struct Reward {
    /// The Reward Item earned
    public let item: RewardItem
    /// How many of the Reward Item was earned
    public let amount: Int
    /// How this reward was earned
    public let rewardAction: RewardAction

    init(item: RewardItem, amount: Int, rewardAction: RewardAction) {
        self.item = item
        self.amount = amount
        self.rewardAction = rewardAction
    }

    static func createRewards(
        availableRewardItems: [RewardItem],
        rewardResources: [RewardResource],
        widgetInfo: RewardAction.WidgetInfo
    ) -> [Reward] {
        return rewardResources.compactMap { rewardResource in
            guard let rewardItem = availableRewardItems.first(where: { $0.id == rewardResource.rewardItemId }) else {
                return nil
            }
            return Reward(
                item: rewardItem,
                amount: rewardResource.rewardItemAmount,
                rewardAction: .init(rewardActionResource: rewardResource.rewardAction, widgetInfo: widgetInfo)
            )
        }
    }
}
