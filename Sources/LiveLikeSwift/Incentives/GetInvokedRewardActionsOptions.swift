//
//  GetInvokedRewardActionsOptions.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 8/22/22.
//

/// Options for a  Get Invoked Reward Actions request
public struct GetInvokedRewardActionsOptions {

    public init(
        programID: String? = nil,
        rewardActionKeys: [String]? = nil,
        profileIDs: [String]? = nil,
        attributes: [Attribute]? = nil
    ) {
        self.programID = programID
        self.rewardActionKeys = rewardActionKeys
        self.profileIDs = profileIDs
        self.attributes = attributes
    }

    /// Optional filter by programID
    public let programID: String?
    /// Optional filter by reward action key(s)
    public let rewardActionKeys: [String]?
    /// Optional filter by profile IDs
    public let profileIDs: [String]?
    /// Optional filter by attributes(s)
    public let attributes: [Attribute]?
}
