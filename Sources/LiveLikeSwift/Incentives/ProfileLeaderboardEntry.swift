//
//  ProfileLeaderboardEntry.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

public struct ProfileLeaderboardEntry: Decodable {
    public let percentileRank: String
    public let rank: Int
    public let score: Double
}
