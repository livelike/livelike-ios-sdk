//
//  ProfileLeaderboardView.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

public struct ProfileLeaderboardView {
    public let id: String
    public let name: String?
    public let entry: ProfileLeaderboardEntry?
    public let leaderboard: Leaderboard
    public let profileNickname: String
    let clientID: String
    public let profileID: String
}

extension ProfileLeaderboardView: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case entry
        case leaderboard
        case profileNickname
        case clientID = "clientId"
        case profileID = "profileId"
    }
}
