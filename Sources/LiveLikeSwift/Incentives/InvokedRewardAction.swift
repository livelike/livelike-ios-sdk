//
//  InvokedRewardAction.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 8/22/22.
//

import Foundation

/// An object representing a Reward Action that has been Invoked by a profile
public struct InvokedRewardAction {

    public struct Reward {
        public let rewardItemID: String
        public let rewardItemName: String
        public let rewardItemAmount: Int
    }

    /// The id of the invoked reward action
    public let id: String
    /// When the action was invoked
    public let createdAt: Date
    /// The associated programID of this action
    public let programID: String
    /// The profileID that  invoked this action
    public let profileID: String
    /// The associated reward action key
    public let rewardActionKey: String
    /// `code` is an integrator-supplied arbitrary value that can be used to prevent duplicate rewards for the same activity. If livelike sees a duplicate code for the same action and profile, then the invocation will fail. It makes things like retries and concurrent operations safer.
    public let code: String?
    /// The rewards earned for invoking this action
    public let rewards: [Reward]
    /// The profile nickname that invoked this action
    public let profileNickname: String
    /// Additional attributes associated to this action
    public let attributes: [Attribute]
}

extension InvokedRewardAction: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case createdAt
        case programID = "programId"
        case profileID = "profileId"
        case rewardActionKey
        case code
        case rewards
        case profileNickname
        case attributes
    }
}

extension InvokedRewardAction.Reward: Decodable {
    enum CodingKeys: String, CodingKey {
        case rewardItemID = "rewardItemId"
        case rewardItemName
        case rewardItemAmount
    }
}
