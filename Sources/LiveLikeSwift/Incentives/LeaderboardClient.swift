//
//  LeaderboardClient.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 9/15/20.
//

import Foundation
import LiveLikeCore

public struct Leaderboard {
    public let id: String
    public let url: URL
    let clientID: String
    public let name: String
    public let rewardItem: RewardItem
    public let isLocked: Bool
    public let entriesURL: URL
    let entryDetailUrlTemplate: String

    func getEntryURL(profileID: String) -> URL? {
        let stringToReplace = "{profile_id}"
        guard entryDetailUrlTemplate.contains(stringToReplace) else {
            return nil
        }
        let urlTemplateFilled = entryDetailUrlTemplate.replacingOccurrences(
            of: stringToReplace,
            with: profileID
        )
        return URL(string: urlTemplateFilled)
    }
}

extension Leaderboard: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case url
        case clientID = "clientId"
        case name
        case rewardItem
        case isLocked
        case entriesURL = "entriesUrl"
        case entryDetailUrlTemplate
    }
}

struct LeaderboardEntryResource: Decodable {
    let percentileRank: String
    let profileId: String
    let rank: Int
    let score: Double
    let profileNickname: String
    let profile: ProfileResource
}

/// Used to query leaderboard entry interfaces
public struct GetLeaderboardEntriesRequestOptions {
    // Is a unique identifier used for pagination functionality
    let id: String

    // Is an array of `URLQueryItem` used to compile URL parameters
    let urlQueries: [URLQueryItem]

    let profileIDs: [String]
    let leaderboardID: String

    public init(
        leaderboardID: String,
        profileIDs: [String] = [],
        pageSize: Int? = nil
    ) {
        self.leaderboardID = leaderboardID
        self.profileIDs = profileIDs

        self.id = {
            var id: String = leaderboardID

            let sortedProfileIDs = profileIDs.sorted { $0 > $1 }
            sortedProfileIDs.forEach { id += $0 }

            if let pageSize = pageSize {
                id += String(pageSize)
            }

            return id
        }()

        self.urlQueries = {
            var urlQueries = [URLQueryItem]()
            profileIDs.forEach {
                urlQueries.append(
                    URLQueryItem(name: "profile_id", value: $0)
                )
            }

            if let pageSize = pageSize {
                urlQueries.append(
                    URLQueryItem(name: "page_size", value: String(pageSize))
                )
            }

            return urlQueries
        }()
    }
}

/// Describes a position in a leaderboard
public struct LeaderboardPosition {
    public let rank: Int
    public let score: Int
    public let percentileRank: String

    init(rewardResource: RewardResource) {
        self.rank = rewardResource.newRank
        self.score = rewardResource.newScore
        self.percentileRank = rewardResource.newPercentileRank
    }

    init(rank: Int, score: Int, percentileRank: String) {
        self.rank = rank
        self.score = score
        self.percentileRank = percentileRank
    }
}

/// Methods for managing changes to a Leaderboard
public protocol LeaderboardDelegate: AnyObject {
    /// Tells the delegate that the current user's LeaderboardPlacement has changed
    func leaderboard(_ leaderboardClient: LeaderboardClient, currentPositionDidChange position: LeaderboardPosition)
}

/// Methods for managing a Leaderboard
public class LeaderboardClient {

    public weak var delegate: LeaderboardDelegate?

    /// The id of the Leaderboard
    public let id: String

    /// The name of the Leaderboard
    public let name: String

    /// The Reward Item being tracked by this Leaderboard
    public let rewardItem: RewardItem

    /// The current user's position in this Leaderboard
    /// Their position is `nil` if the user hasn't earned any points on the Leaderboard
    public private(set) var currentPosition: LeaderboardPosition?

    private let leaderboardsManager: LeaderboardsManager

    convenience init(
        leaderboardResource: Leaderboard,
        currentLeaderboardEntry: LeaderboardEntryResource?,
        leaderboardsManager: LeaderboardsManager
    ) {
        self.init(
            id: leaderboardResource.id,
            name: leaderboardResource.name,
            rewardItem: leaderboardResource.rewardItem,
            currentPosition: {
                guard let leaderboardEntry = currentLeaderboardEntry else {
                    return nil
                }
                return LeaderboardPosition(
                    rank: leaderboardEntry.rank,
                    score: Int(leaderboardEntry.score),
                    percentileRank: leaderboardEntry.percentileRank
                )
            }(),
            leaderboardsManager: leaderboardsManager
        )
    }

    init(
        id: String,
        name: String,
        rewardItem: RewardItem,
        currentPosition: LeaderboardPosition?,
        leaderboardsManager: LeaderboardsManager
    ) {
        self.id = id
        self.name = name
        self.rewardItem = rewardItem
        self.currentPosition = currentPosition
        self.leaderboardsManager = leaderboardsManager
        leaderboardsManager.listeners.addListener(self)
    }
}

extension LeaderboardClient: LeaderboardsManagerDelegate {
    func leaderboardsManager(
        _ leaderboardsManager: LeaderboardsManager,
        currentPositionDidChange position: LeaderboardPosition,
        leaderboardID: String
    ) {
        guard id == leaderboardID else { return }
        self.currentPosition = position
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.delegate?.leaderboard(self, currentPositionDidChange: position)
        }
    }
}

protocol LeaderboardsManagerDelegate: AnyObject {
    func leaderboardsManager(_ leaderboardsManager: LeaderboardsManager, currentPositionDidChange placement: LeaderboardPosition, leaderboardID: String)
}

class LeaderboardsManager {
    let listeners: Listener<LeaderboardsManagerDelegate> = Listener(dispatchQueueLabel: "com.LiveLike.LeaderboardsManager")

    func notifyCurrentPositionChange(rewards: [RewardResource]) {
        rewards.forEach { reward in
            listeners.publish {
                $0.leaderboardsManager(
                    self,
                    currentPositionDidChange: LeaderboardPosition(rewardResource: reward),
                    leaderboardID: reward.leaderboardId
                )
            }
        }
    }
}
