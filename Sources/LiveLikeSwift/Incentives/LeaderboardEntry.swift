//
//  LeaderboardEntry.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

public struct LeaderboardEntry {
    public let percentileRank: String
    public let profileId: String
    public let rank: Int
    public let score: Double
    public let profileNickname: String
    public let profile: ProfileResource

    init(_ leaderboardEntryResource: LeaderboardEntryResource) {
        self.percentileRank = leaderboardEntryResource.percentileRank
        self.profileId = leaderboardEntryResource.profileId
        self.rank = leaderboardEntryResource.rank
        self.score = leaderboardEntryResource.score
        self.profileNickname = leaderboardEntryResource.profileNickname
        self.profile = leaderboardEntryResource.profile
    }
}
