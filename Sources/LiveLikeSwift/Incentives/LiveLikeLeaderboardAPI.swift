//
//  LiveLikeLeaderboardAPI.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 5/20/21.
//

import Foundation
import LiveLikeCore

protocol LiveLikeLeaderboardAPIProtocol {
    /// Get leaderboards for a program ID
    func getLeaderboards(programID: String) -> Promise<[Leaderboard]>

    /// Get leaderboard object by ID
    func getLeaderboard(leaderboardID: String) -> Promise<Leaderboard>

    /// Get a paginated list of leaderboard entries
    func getLeaderboardEntries(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<LeaderboardEntryResource>>

    func getLeaderboardEntry(url: URL, accessToken: AccessToken) -> Promise<LeaderboardEntryResource>

    /// Get a leaderboard entry profile
    func getLeaderboardProfile(url: URL, accessToken: AccessToken) -> Promise<LeaderboardEntryResource>

    func getProfileLeaderboards(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<ProfileLeaderboard>>

    func getProfileLeaderboardViews(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<ProfileLeaderboardView>>

    func getRewardItems(url: URL, accessToken: AccessToken) -> Promise<[RewardItem]>

    func claimRewards(claimURL: URL, claimToken: String, accessToken: AccessToken) -> Promise<ClaimRewardResource>
}

class LiveLikeLeaderboardAPI: LiveLikeLeaderboardAPIProtocol {

    var coreAPI: LiveLikeCoreAPIProtocol

    init(coreAPI: LiveLikeCoreAPIProtocol) {
        self.coreAPI = coreAPI
    }

    func getLeaderboards(programID: String) -> Promise<[Leaderboard]> {
        return firstly {
            coreAPI.getProgramDetail(programID: programID)
        }.then { program -> Promise<[Leaderboard]>in
            return Promise(value: program.leaderboards)
        }
    }

    func getLeaderboard(leaderboardID: String) -> Promise<Leaderboard> {
        return firstly {
            coreAPI.whenApplicationConfig
        }.then { (appConfig: ApplicationConfiguration) in
            let stringToReplace = "{leaderboard_id}"
            guard appConfig.leaderboardDetailUrlTemplate.contains(stringToReplace) else {
                return Promise(error: LLGamificationError.leaderboardDetailUrlCorrupt)
            }
            let urlTemplateFilled = appConfig.leaderboardDetailUrlTemplate.replacingOccurrences(
                of: stringToReplace,
                with: leaderboardID
            )
            guard let leaderboardURL = URL(string: urlTemplateFilled) else {
                return Promise(error: LLGamificationError.leaderboardUrlCreationFailure)
            }
            let resource = Resource<Leaderboard>(get: leaderboardURL)
            return LiveLike.networking.load(resource)
        }
    }

    func getLeaderboardEntries(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<LeaderboardEntryResource>> {
        return firstly { () -> Promise<PaginatedResult<LeaderboardEntryResource>> in
            let resource = Resource<PaginatedResult<LeaderboardEntryResource>>(
                get: url,
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }
    }

    func getLeaderboardEntry(url: URL, accessToken: AccessToken) -> Promise<LeaderboardEntryResource> {
        let resource = Resource<LeaderboardEntryResource>(get: url, accessToken: accessToken.asString)
        return LiveLike.networking.load(resource)
    }

    func getLeaderboardProfile(url: URL, accessToken: AccessToken) -> Promise<LeaderboardEntryResource> {
        return firstly { () -> Promise<LeaderboardEntryResource> in
            let resource = Resource<LeaderboardEntryResource>(
                get: url,
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }
    }

    func getProfileLeaderboards(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<ProfileLeaderboard>> {
        return firstly { () -> Promise<PaginatedResult<ProfileLeaderboard>> in
            let resource = Resource<PaginatedResult<ProfileLeaderboard>>(
                get: url,
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }
    }

    func getProfileLeaderboardViews(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<ProfileLeaderboardView>> {
        return firstly { () -> Promise<PaginatedResult<ProfileLeaderboardView>> in
            let resource = Resource<PaginatedResult<ProfileLeaderboardView>>(
                get: url,
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }
    }

    func getRewardItems(url: URL, accessToken: AccessToken) -> Promise<[RewardItem]> {
        struct RewardItems: Decodable {
            let results: [RewardItem]
        }

        let resource = Resource<RewardItems>(
            get: url,
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource).then { $0.results }
    }

    func claimRewards(claimURL: URL, claimToken: String, accessToken: AccessToken) -> Promise<ClaimRewardResource> {
        struct Payload: Encodable {
            let claimToken: String
        }
        let resource = Resource<ClaimRewardResource>(
            url: claimURL,
            method: .post(Payload(claimToken: claimToken)),
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }
}

// MARK: - Models
struct RewardResource: Decodable {
    let rewardItemId: String
    var newRank: Int
    var rewardItemAmount: Int
    var newScore: Int
    var newPercentileRank: String
    var leaderboardId: String
    var rewardAction: RewardActionResource

    enum CodingKeys: String, CodingKey {
        case rewardItemId
        case newRank
        case rewardItemAmount
        case newScore
        case newPercentileRank
        case leaderboardId
        case rewardAction
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        rewardItemId = try container.decode(String.self, forKey: .rewardItemId)
        newRank = try container.decode(Int.self, forKey: .newRank)
        rewardItemAmount = try container.decode(Int.self, forKey: .rewardItemAmount)
        newScore = try container.decode(Int.self, forKey: .newScore)
        newPercentileRank = try container.decode(String.self, forKey: .newPercentileRank)
        leaderboardId = try container.decode(String.self, forKey: .leaderboardId)
        if let rewardAction = try? container.decode(RewardActionResource.self, forKey: .rewardAction) {
            self.rewardAction = rewardAction
        } else {
            self.rewardAction = .undefined
        }
    }
}

enum RewardActionResource: String, Decodable {
    case pollVoted = "poll-voted"
    case predictionMade = "prediction-made"
    case predictionCorrect = "prediction-correct"
    case quizAnswered = "quiz-answered"
    case quizCorrect = "quiz-correct"
    case undefined
}

struct ClaimRewardResource: Decodable {
    let rewards: [RewardResource]
}
