//
//  ReactionClient.swift
//  EngagementSDK
//
//  Created by Keval Shah on 19/07/22.
//

import Foundation
import LiveLikeCore

// MARK: - Protocols
public protocol ReactionClientDelegate: AnyObject {
    /// Called when the reaction space is updated by the producer.
    /// - Parameters:
    /// - newReactionSpace: An object of type `ReactionSpace` containing updated details of the Reaction Space
    /// - reactionClient: The `ReactionClient` object informing the delegate of this event
    func reactionClient(_ reactionClient: ReactionClient, didUpdateReactionSpace newReactionSpace: ReactionSpace)
}

/// A client used for Reaction Space queries
public protocol ReactionClient {
    
    /// Conform to the `ReactionClientDelegate` to observe `ReactionSpace` events
    var delegate: ReactionClientDelegate? { get set }
    
    /// Create a Reaction Space
    /// - Parameters:
    ///   - name: the name of a `ReactionSpace`
    ///   - targetGroupID: the target group id corresponding to the `ReactionSpace`
    ///   - reactionPackIDs: List of IDs of `ReactionPack` linked to the `ReactionSpace`
    ///   - completion: A callback indicating whether the `ReactionSpace` was created or failed where `ReactionSpace` represents the created Reaction Space
    func createReactionSpace(
        name: String?,
        targetGroupID: String,
        reactionPackIDs: [String],
        completion: @escaping (Result<ReactionSpace, Error>) -> Void
    )
    
    /// Update a Reaction Space
    /// - Parameters:
    ///   - reactionSpaceID: the unique identifier of the `ReactionSpace` to be updated
    ///   - reactionPackIDs: List of IDs of `ReactionPack` linked to the `ReactionSpace` to be updated
    ///   - completion: A callback indicating whether the `ReactionSpace` was updated or failed where the object represents the updated list of IDs of `ReactionPack`
    func updateReactionSpace(
        reactionSpaceID: String,
        reactionPackIDs: [String],
        completion: @escaping (Result<[String], Error>) -> Void
    )
    
    /// Delete a Reaction Space
    /// - Parameters:
    ///   - reactionSpaceID: the unique identifier of the `ReactionSpace` to be deleted
    ///   - completion: A callback indicating whether the `ReactionSpace` was deleted or failed
    func deleteReactionSpace(
        reactionSpaceID: String,
        completion: @escaping (Result<Bool, Error>) -> Void
    )
    
    /// Get Details of a specific Reaction Space
    /// - Parameters:
    ///   - reactionSpaceID: the unique identifier of the `ReactionSpace`
    ///   - completion: A callback indicating  the `ReactionSpace` details were received or failed where the object represents the details  of `ReactionSpace`
    func getReactionSpaceInfo(
        reactionSpaceID: String,
        completion: @escaping (Result<ReactionSpace, Error>) -> Void
    )
    
    /// Get List of Reaction Spaces
    /// - Parameters:
    ///   - reactionSpaceID: Unique Identifier of the Reaction Space to be queried (Optional)
    ///   - targetGroupID: Unique Identifier of the Target Group to which the `ReactionSpace` belongs
    ///   - page: a `Pagination` typed parameter passed to allow paginated responses
    func getReactionSpaces(
        reactionSpaceID: String?,
        targetGroupID: String?,
        page: Pagination,
        completion: @escaping (Result<[ReactionSpace], Error>) -> Void
    )
    
    /// Get List of Reaction Packs
    /// - Parameters:
    ///   - page: a `Pagination` typed parameter passed to allow paginated responses
    ///   - completion: A callback indicating the list of `ReactionPack`
    func getReactionPacks(
        page: Pagination,
        completion: @escaping (Result<[ReactionPack], Error>) -> Void
    )
    
    /// Get Details of a specific Reaction Pack
    /// - Parameters:
    ///   - reactionPackID: the unique identifier of the `ReactionPack`
    ///   - completion: A callback indicating  the `ReactionPack` details were received or failed where the object represents the details  of `ReactionPack`
    func getReactionPackInfo(
        reactionPackID: String,
        completion: @escaping (Result<ReactionPack, Error>) -> Void
    )
    
    func createReactionSession(reactionSpace: ReactionSpace) -> ReactionSession
}

// MARK: - Internal
final class InternalReactionClient: ReactionClient {
    weak var delegate: ReactionClientDelegate?
    
    private var sdk: LiveLike
    private let whenReactionSpacesURL: Promise<URL>
    private let clientID: String
    
    struct ReactionClientPaginationProgress {
        var next: URL?
        var previous: URL?
        var total: Int = 0
    }
    
    var reactionClientPagination: ReactionClientPaginationProgress
    private var clientPageURLRepo = [String: PaginatedResultURL]()
    
    init(sdk: LiveLike, clientID: String) {
        
        self.sdk = sdk
        self.clientID = clientID
        self.reactionClientPagination = ReactionClientPaginationProgress()
        
        self.whenReactionSpacesURL = sdk.coreAPI.whenApplicationConfig.then { $0.reactionSpacesUrl }
    }
    
    deinit {
        log.info("Reaction Client has ended.")
    }
    
    func createReactionSpace(
        name: String?,
        targetGroupID: String,
        reactionPackIDs: [String],
        completion: @escaping (Result<ReactionSpace, Error>) -> Void
    ) {firstly {
        Promises.zip(
            sdk.accessTokenVendor.whenAccessToken,
            whenReactionSpacesURL
        )
    }.then { accessToken, reactionSpaceURL -> Promise<ReactionSpace> in
        self.sdk.reactionAPI.createReactionSpace(reactionSpaceURL: reactionSpaceURL, name: name, targetGroupID: targetGroupID, reactionPackIDs: reactionPackIDs, accessToken: accessToken)
    }.then { reactionSpace in
        completion(.success(reactionSpace))
    }.catch { error in
        completion(.failure(error))
    }
    }
    
    func updateReactionSpace(reactionSpaceID: String, reactionPackIDs: [String], completion: @escaping (Result<[String], Error>) -> Void) {
        firstly {
            Promises.zip(
                sdk.accessTokenVendor.whenAccessToken,
                whenReactionSpacesURL
            )
        }.then { accessToken, reactionSpaceURL -> Promise<UpdateReactionSpaceResult> in
            self.sdk.reactionAPI.updateReactionSpace(
                reactionSpaceURL: reactionSpaceURL.appendingPathComponent(
                    reactionSpaceID,
                    isDirectory: true
                ),
                reactionPackIDs: reactionPackIDs,
                accessToken: accessToken
            )
        }.then { reactionSpaceIDList in
            completion(.success(reactionSpaceIDList.reactionPackIDs))
        }.catch { error in
            log.error("Error Updating reaction space for: \(reactionSpaceID): \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func deleteReactionSpace(reactionSpaceID: String, completion: @escaping (Result<Bool, Error>) -> Void) {
        firstly {
            Promises.zip(
                sdk.accessTokenVendor.whenAccessToken,
                whenReactionSpacesURL
            )
        }.then { accessToken, reactionSpaceURL -> Promise<Bool> in
            self.sdk.reactionAPI.deleteReactionSpace(
                reactionSpaceURL: reactionSpaceURL.appendingPathComponent(
                    reactionSpaceID,
                    isDirectory: true
                ),
                accessToken: accessToken
            )
        }.then { result in
            completion(.success(result))
        }.catch { error in
            log.error("Error deleting Reaction Space: \(reactionSpaceID): \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func getReactionSpaceInfo(
        reactionSpaceID: String,
        completion: @escaping (Result<ReactionSpace, Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                self.sdk.coreAPI.whenApplicationConfig,
                self.sdk.accessTokenVendor.whenAccessToken
            )
        }.then { app, accessToken in
            self.sdk.reactionAPI.getReactionSpaceInfo(
                reactionSpaceURL: try app.getReactionSpaceDetailURL(
                    reactionSpaceID: reactionSpaceID
                ),
                accessToken: accessToken
            )
        }.then { reactionSpaceInfo in
            completion(.success(reactionSpaceInfo))
        }.catch { error in
            log.error("Failed getting information of reaction space: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func getReactionSpaces(
        reactionSpaceID: String?,
        targetGroupID: String?,
        page: Pagination,
        completion: @escaping (Result<[ReactionSpace], Error>) -> Void
    ) {
        
        let pageResultURLID = "getReactionSpaces"
        
        var queryItems: [URLQueryItem] = [
            URLQueryItem(
                name: ReactionSpaceQueryKeys.clientID.rawValue,
                value: self.clientID
            )
        ]
        
        if let spaceID = reactionSpaceID {
            queryItems.append(
                URLQueryItem(
                    name: ReactionSpaceQueryKeys.reactionSpaceID.rawValue,
                    value: spaceID
                )
            )
        }
        
        if let targetGroupID = targetGroupID {
            queryItems.append(
                URLQueryItem(
                    name: ReactionSpaceQueryKeys.targetGroupID.rawValue,
                    value: targetGroupID
                )
            )
        }
        
        let reactionSpacesListURL: Promise<URL> = Promise { fulfill, reject in
            switch page {
            case .first:
                
                // Reset result repo cache if the user is calling `.first` page
                self.clientPageURLRepo[pageResultURLID] = nil
                
                firstly {
                    self.whenReactionSpacesURL
                }.then { reactionSpacesURL in
                    if let url = reactionSpacesURL.appending(queryItems: queryItems) {
                        fulfill(url)
                    } else {
                        reject(ReactionClientError.invalidReactionSpaceURL)
                    }
                }.catch {
                    reject($0)
                }
            case .next:
                if let url = self.clientPageURLRepo[pageResultURLID]?.nextURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.nextPageUnavailable)
                }
            case .previous:
                if let url = self.clientPageURLRepo[pageResultURLID]?.previousURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.previousPageUnavailable)
                }
            }
        }
        
        firstly {
            Promises.zip(
                reactionSpacesListURL,
                self.sdk.accessTokenVendor.whenAccessToken
            )
        }.then { url, accessToken in
            self.sdk.reactionAPI.getReactionSpaces(
                url: url,
                accessToken: accessToken
            )
        }.then { paginatedReactionSpaces in
            self.clientPageURLRepo[pageResultURLID] = PaginatedResultURL(
                nextURL: paginatedReactionSpaces.next,
                previousURL: paginatedReactionSpaces.previous
            )
            completion(.success(paginatedReactionSpaces.items))
        }.catch { error in
            log.error("Failed getting list of reaction spaces: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func getReactionPackInfo(
        reactionPackID: String,
        completion: @escaping (Result<ReactionPack, Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                self.sdk.coreAPI.whenApplicationConfig,
                self.sdk.accessTokenVendor.whenAccessToken
            )
        }.then { application, accessToken in
            self.sdk.reactionAPI.getReactionPackInfo(
                reactionPackURL: try application.getReactionPackURL(
                    reactionPackID: reactionPackID
                ),
                accessToken: accessToken
            )
        }.then { reactionPackInfo in
            completion(.success(reactionPackInfo))
        }.catch { error in
            log.error("Failed getting information of reaction pack: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func getReactionPacks(
        page: Pagination,
        completion: @escaping (Result<[ReactionPack], Error>) -> Void
    ) {
        
        let pageResultURLID = "getReactionPacks"
        
        let reactionPacksListURL: Promise<URL> = Promise { fulfill, reject in
            switch page {
            case .first:
                
                // Reset result repo cache if the user is calling `.first` page
                self.clientPageURLRepo[pageResultURLID] = nil
                firstly {
                    self.sdk.coreAPI.whenApplicationConfig.then { $0.reactionPacksUrl }
                }.then {
                    fulfill($0)
                }.catch {
                    reject($0)
                }
            case .next:
                if let url = self.clientPageURLRepo[pageResultURLID]?.nextURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.nextPageUnavailable)
                }
            case .previous:
                if let url = self.clientPageURLRepo[pageResultURLID]?.previousURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.previousPageUnavailable)
                }
            }
        }
        
        firstly {
            Promises.zip(
                reactionPacksListURL,
                self.sdk.accessTokenVendor.whenAccessToken
            )
        }.then { url, accessToken in
            self.sdk.reactionAPI.getReactionPacks(
                url: url,
                accessToken: accessToken
            )
        }.then { paginatedReactionPacks in
            self.clientPageURLRepo[pageResultURLID] = PaginatedResultURL(
                nextURL: paginatedReactionPacks.next,
                previousURL: paginatedReactionPacks.previous
            )
            completion(.success(paginatedReactionPacks.items))
        }.catch { error in
            log.error("Failed getting list of reaction spaces: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func createReactionSession(reactionSpace: ReactionSpace) -> ReactionSession {
        return InternalReactionSession(sdk: sdk, reactionSpace: reactionSpace)
    }
}
