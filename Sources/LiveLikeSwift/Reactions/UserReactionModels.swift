//
//  UserReactionModels.swift
//  EngagementSDK
//
//  Created by Keval Shah on 19/07/22.
//

import Foundation

// MARK: - Models

/// The `ReactionSpace` struct represents the Reaction Space which contains details of different reaction packs for specific target group
public struct ReactionSpace: Decodable {
    // MARK: - Public Properties
    /// The unique identifer of the space
    public let id: String
    /// The url used to make API requests to the corresponding Reaction Space
    public let url: URL
    /// The name of the Reaction Space
    public let name: String?
    /// The target group ID for which the Reaction Space is to be used
    public let targetGroupID: String
    /// The list of IDs of the Reaction Packs which belong to the Reaction Space
    public let reactionPackIDs: [String]

    // MARK: - Internal Properties
    let clientID: String
    let reactionSpaceChannel: String
    let userReactionsURL: URL
    let userReactionDetailURLTemplate: String
    let userReactionsCountURL: URL

    enum CodingKeys: String, CodingKey {
        case id
        case url
        case name
        case targetGroupId
        case reactionPackIds
        case clientId
        case reactionSpaceChannel
        case userReactionsUrl
        case userReactionDetailUrlTemplate
        case userReactionsCountUrl
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.url = try container.decode(URL.self, forKey: .url)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.targetGroupID = try container.decode(String.self, forKey: .targetGroupId)
        self.reactionPackIDs = try container.decode([String].self, forKey: .reactionPackIds)
        self.clientID = try container.decode(String.self, forKey: .clientId)
        self.reactionSpaceChannel = try container.decode(String.self, forKey: .reactionSpaceChannel)
        self.userReactionsURL = try container.decode(URL.self, forKey: .userReactionsUrl)
        self.userReactionDetailURLTemplate = try container.decode(String.self, forKey: .userReactionDetailUrlTemplate)
        self.userReactionsCountURL = try container.decode(URL.self, forKey: .userReactionsCountUrl)
    }

    init(
        id: String,
        url: URL,
        name: String?,
        targetGroupID: String,
        reactionPackIDs: [String],
        clientID: String,
        reactionSpaceChannel: String,
        userReactionsURL: URL,
        userReactionDetailURLTemplate: String,
        userReactionsCountURL: URL
    ) {
        self.id = id
        self.url = url
        self.name = name
        self.targetGroupID = targetGroupID
        self.reactionPackIDs = reactionPackIDs
        self.clientID = clientID
        self.reactionSpaceChannel = reactionSpaceChannel
        self.userReactionsURL = userReactionsURL
        self.userReactionDetailURLTemplate = userReactionDetailURLTemplate
        self.userReactionsCountURL = userReactionsCountURL
    }
}

/// Represents a User reaction
public struct UserReaction: Decodable {
    // MARK: - Public Properties
    /// The unique identifer of the User Reaction
    public let id: String
    /// The target ID of the object for which the User Reaction was added
    public let targetID: String
    /// The ID of the user who added the User Reaction
    public let reactedByID: String
    /// Timestamp of when the User Reaction was created
    public let createdAt: Date
    /// The unique identifier of the reaction object
    public let reactionID: String
    /// The unique identifier of the Reaction Space associated with the User Reaction
    public let reactionSpaceID: String
    /// The String type object of custom data that can be attached to the User Reaction
    public let customData: String?

    // MARK: - Internal Properties
    internal let clientID: String

    enum CodingKeys: String, CodingKey {
        case id
        case targetId
        case reactedById
        case createdAt
        case reactionId
        case clientId
        case reactionSpaceId
        case customData
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.targetID = try container.decode(String.self, forKey: .targetId)
        self.reactedByID = try container.decode(String.self, forKey: .reactedById)
        self.createdAt = try container.decode(Date.self, forKey: .createdAt)
        self.reactionID = try container.decode(String.self, forKey: .reactionId)
        self.reactionSpaceID = try container.decode(String.self, forKey: .reactionSpaceId)
        self.customData = try container.decodeIfPresent(String.self, forKey: .customData)
        self.clientID = try container.decode(String.self, forKey: .clientId)
    }

    init(
        id: String,
        targetID: String,
        reactedByID: String,
        createdAt: Date,
        reactionID: String,
        reactionSpaceID: String,
        customData: String?,
        clientID: String
    ) {
        self.id = id
        self.targetID = targetID
        self.reactedByID = reactedByID
        self.createdAt = createdAt
        self.reactionID = reactionID
        self.reactionSpaceID = reactionSpaceID
        self.customData = customData
        self.clientID = clientID
    }
}
