//
//  LocalReactionsVendor.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 9/19/19.
//

import Foundation
import LiveLikeCore

class LocalReactionsVendor: ReactionVendor {
    func getReactions() -> Promise<[Reaction]> {
        let chatReactions = [
            angry,
            cry,
            laughter,
            love
        ]
        return Promise(value: chatReactions)
    }
}

// MARK: - Reaction Definitions

private extension LocalReactionsVendor {
    func imageUrl(forResource resource: String) -> URL {
        return Bundle(for: LocalReactionsVendor.self).url(forResource: resource, withExtension: "png")!
    }

    var angry: Reaction {
        return Reaction(
            id: "angry",
            file: imageUrl(forResource: "chatReactionAngry"),
            name: "chatReactionAngry"
        )
    }
    var cry: Reaction {
        return Reaction(
            id: "cry",
            file: imageUrl(forResource: "chatReactionCry"),
            name: "chatReactionCry"
        )
    }
    var laughter: Reaction {
        return Reaction(
            id: "laughter",
            file: imageUrl(forResource: "chatReactionLaughter"),
            name: "chatReactionLaughter"
        )
    }
    var love: Reaction {
        return Reaction(
            id: "love",
            file: imageUrl(forResource: "chatReactionLove"),
            name: "chatReactionLove"
        )
    }
}
