//
//  Reaction.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 12/1/22.
//

import Foundation

/// Represents the reaction resource from the backend
public struct Reaction {
    /// Unique identifier for the reaction resource
    public let id: String
    /// URL for the file/image associated with the reaction resource
    public let file: URL
    /// name of the reaction resource
    public let name: String
}

extension Reaction: Decodable { }
