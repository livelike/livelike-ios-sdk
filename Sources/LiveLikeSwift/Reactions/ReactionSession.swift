//
//  ReactionSession.swift
//  EngagementSDK
//
//  Created by Keval Shah on 20/07/22.
//

import Foundation
import LiveLikeCore

public protocol ReactionSessionDelegate: AnyObject {
    /// Notifies the delegate that a reaction was added on the `ReactionSession`
    /// - Parameters:
    ///   - reactionSession: The reaction session object informing the delegate of this event
    ///   - reaction: The `UserReaction` of the user reaction that was added
    func reactionSession(_ reactionSession: ReactionSession, didAddReaction reaction: UserReaction)
    /// Notifies the delegate that a reaction was removed on the `ReactionSession`
    /// - Parameters:
    ///   - reactionSession: The reaction session object informing the delegate of this event
    ///   - reaction: The `UserReaction` of the user reaction that was removed
    func reactionSession(_ reactionSession: ReactionSession, didRemoveReaction reaction: UserReaction)
}

public protocol ReactionSession: AnyObject {
    /// A String type which represents the name of the `Reaction Space` corresponding to the `Reaction Session` object
    var name: String? { get }
    /// A String type which represents the id of the `Reaction Space` corresponding to the `Reaction Session` object
    var reactionSpaceID: String { get }
    /// A String type which represents the id of the target group corresponding to the `Reaction Space` object
    var targetGroupID: String { get }
    /// An Array of String type which represents the ids of the reaction packs corresponding to the `Reaction Space` object
    var reactionPackIDs: [String] { get }
    
    /// Conform to the `ReactionSessionDelegate` to observe `ReactionSession` events
    var sessionDelegate: ReactionSessionDelegate? { get set }
    
    /// Adds a reaction to a particular target.
    ///
    /// - Parameters:
    ///   - targetID: A String typed parameter representing the object the reaction was applied to
    ///   - reactionID: A String typed parameter representing the unique id of the reaction
    ///   - customData: An optional String typed parameter representing any type of custom data for the reaction
    ///   - completion: A callback indicating whether the reaction was applied or failed where `UserReaction` represents the applied reaction
    /// - Returns: the user reaction to be applied
    func addUserReaction(
        targetID: String,
        reactionID: String,
        customData: String?,
        completion: @escaping (Result<UserReaction, Error>) -> Void
    )
    
    /// Allows users to remove a previously added user reaction
    ///
    /// - Parameters:
    ///   - userReactionID: A String typed parameter which can be used to pass the id of the `UserReaction` object for the reaction to be removed.
    ///
    func removeUserReaction(
        userReactionID: String,
        completion: @escaping (Result<Bool, Error>) -> Void
    )
    
    /// Returns a list of user reactions linked to the `Reaction Space`
    ///
    /// - Parameters:
    ///   - reactionSpaceID: A String typed parameter representing the id of the `Reaction Space` object corrsponding to the `Reaction Session`
    ///   - options: Query options for easier filtering of the request
    ///   - page: a `Pagination` typed parameter passed to allow paginated responses
    ///   - completion: A callback that receives the list of `User Reaction` objects for reactions that were added
    ///
    func getUserReactions(
        page: Pagination,
        reactionSpaceID: String,
        options: GetUserReactionsRequestOptions?,
        completion: @escaping (Result<[UserReaction], Error>) -> Void
    )
    
    /// Returns a list of counts of user reactions for a particular target
    ///
    /// - Parameters:
    ///   - reactionSpaceID: A String typed parameter representing the id of the `Reaction Space` object corrsponding to the `Reaction Session`
    ///   - targetID: A list of String type representing the id of the target objects for which the count is requested
    ///   - page: a `Pagination` typed parameter passed to allow paginated responses
    ///   - completion: A callback that receives the list of `UserReactionsCountResult` objects for count of reactions to corresponding targets.
    ///
    func getUserReactionsCount(
        reactionSpaceID: String,
        targetID: [String],
        page: Pagination,
        completion: @escaping (Result<[UserReactionsCountResult], Error>) -> Void
    )
}

final class InternalReactionSession: ReactionSession, PubSubChannelDelegate {
    
    var name: String?
    var reactionSpaceID: String
    var targetGroupID: String
    var reactionPackIDs: [String]
    private var userReactionChannel: PubSubChannel?
    private var reactionClient: ReactionClient
    private var sdk: LiveLike
    private var userReactionsUrl: URL
    private var userReactionDetailUrlTemplate: String
    private var userReactionsCountUrl: URL
    private var clientID: String
    
    weak var sessionDelegate: ReactionSessionDelegate?
    
    weak var clientDelegate: ReactionClientDelegate? {
        didSet {
            reactionClient.delegate = clientDelegate
        }
    }
    
    struct ReactionSessionPaginationProgress {
        var next: URL?
        var previous: URL?
        var total: Int = 0
    }
    
    var reactionSessionPagination: ReactionSessionPaginationProgress
    private var sessionPageURLRepo = [String: PaginatedResultURL]()
    
    init(sdk: LiveLike, reactionSpace: ReactionSpace) {
        
        self.sdk = sdk
        self.reactionSessionPagination = ReactionSessionPaginationProgress()
        self.reactionSpaceID = reactionSpace.id
        self.name = reactionSpace.name
        self.targetGroupID = reactionSpace.targetGroupID
        self.reactionPackIDs = reactionSpace.reactionPackIDs
        self.reactionClient = InternalReactionClient(sdk: sdk, clientID: reactionSpace.clientID)
        self.userReactionsUrl = reactionSpace.userReactionsURL
        self.userReactionDetailUrlTemplate = reactionSpace.userReactionDetailURLTemplate
        self.userReactionsCountUrl = reactionSpace.userReactionsCountURL
        self.clientID = reactionSpace.clientID
        
        firstly {
            sdk.whenPubSubService
        }.then { pubsubService in
            let userPubnubChannel = reactionSpace.reactionSpaceChannel
            let userReactionChannel = pubsubService?.subscribe(userPubnubChannel)
            self.userReactionChannel = userReactionChannel
            self.userReactionChannel?.delegate = self
        }
    }
    
    func addUserReaction(
        targetID: String,
        reactionID: String,
        customData: String?,
        completion: @escaping (Result<UserReaction, Error>) -> Void
    ) {
        firstly {
            sdk.accessTokenVendor.whenAccessToken
        }.then { accessToken in
            self.sdk.reactionAPI.addUserReactions(userReactionURL: self.userReactionsUrl, reactionSpaceID: self.reactionSpaceID, targetID: targetID, reactionID: reactionID, customData: customData, accessToken: accessToken)
        }.then { userReaction in
            completion(.success(userReaction))
        }.catch { error in
            completion(.failure(error))
        }
    }
    
    func removeUserReaction(userReactionID: String, completion: @escaping (Result<Bool, Error>) -> Void) {
        firstly {
            sdk.accessTokenVendor.whenAccessToken
        }.then { accessToken -> Promise<Bool> in
            self.sdk.reactionAPI.removeUserReactions(
                userReactionURL: self.userReactionsUrl.appendingPathComponent(
                    userReactionID,
                    isDirectory: true
                ),
                accessToken: accessToken
            )
        }.then { result in
            completion(.success(result))
        }.catch { error in
            log.error("Error deleting User Reaction: \(userReactionID): \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func getUserReactions(
        page: Pagination,
        reactionSpaceID: String,
        options: GetUserReactionsRequestOptions?,
        completion: @escaping (Result<[UserReaction], Error>) -> Void
    ) {
        
        var pageResultURLID = "getUserReactions"
        
        // add params to the ID to uniqely identify a paginated call that has params
        if let options = options {
            pageResultURLID += "_\(options.id)"
        }
        let whenReactionsListURL: Promise<URL> = Promise { fulfill, reject in
            switch page {
            case .first:
                
                // Reset result repo cache if the user is calling `.first` page
                self.sessionPageURLRepo[pageResultURLID] = nil
                
                var queryItems: [URLQueryItem] = [
                    URLQueryItem(
                        name: ReactionSpaceQueryKeys.clientID.rawValue,
                        value: self.clientID
                    ),
                    URLQueryItem(
                        name: ReactionSpaceQueryKeys.reactionSpaceID.rawValue,
                        value: reactionSpaceID
                    )
                ]
                
                guard var userReactionsURLComponents = URLComponents(
                    url: self.userReactionsUrl,
                    resolvingAgainstBaseURL: false
                ) else {
                    completion(
                        .failure(
                            ReactionClientError.invalidUserReactionURL
                        )
                    )
                    return
                }
                
                if let options = options {
                    queryItems.append(contentsOf: options.urlQueries)
                }

                userReactionsURLComponents.queryItems?.append(contentsOf: queryItems)

                guard let userReactionsListURL = userReactionsURLComponents.url else {
                    completion(
                        .failure(
                            ReactionClientError.invalidUserReactionURL
                        )
                    )
                    return
                }
                fulfill(userReactionsListURL)

            case .next:
                if let url = self.sessionPageURLRepo[pageResultURLID]?.nextURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.nextPageUnavailable)
                }
            case .previous:
                if let url = self.sessionPageURLRepo[pageResultURLID]?.previousURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.previousPageUnavailable)
                }
            }
        }
        
        firstly {
            Promises.zip(
                whenReactionsListURL,
                self.sdk.accessTokenVendor.whenAccessToken
            )
        }.then { url, accessToken in
            self.sdk.reactionAPI.getUserReactions(
                url: url,
                accessToken: accessToken
            )
        }.then { paginatedReactions in
            self.sessionPageURLRepo[pageResultURLID] = PaginatedResultURL(
                nextURL: paginatedReactions.next,
                previousURL: paginatedReactions.previous
            )
            completion(.success(paginatedReactions.items))
        }.catch { error in
            log.error("Failed getting list of reaction spaces")
            completion(.failure(error))
        }
    }
    
    func getUserReactionsCount(
        reactionSpaceID: String,
        targetID: [String],
        page: Pagination,
        completion: @escaping (Result<[UserReactionsCountResult], Error>) -> Void
    ) {
        let pageResultURLID = "getUserReactionsCount"
        
        let reactionCountURL: Promise<URL> = Promise { fulfill, reject in
            switch page {
            case .first:
                // Reset result repo cache if the user is calling `.first` page
                self.sessionPageURLRepo[pageResultURLID] = nil
                
                var queryItems: [URLQueryItem] = [
                    URLQueryItem(
                        name: ReactionSpaceQueryKeys.clientID.rawValue,
                        value: self.clientID
                    ),
                    URLQueryItem(
                        name: ReactionSpaceQueryKeys.reactionSpaceID.rawValue,
                        value: reactionSpaceID
                    )
                ]
                
                let targetIDQueryItems = targetID.map {
                    URLQueryItem(
                        name: ReactionSpaceQueryKeys.targetID.rawValue,
                        value: $0
                    )
                }
                for queryItem in targetIDQueryItems {
                    queryItems.append(queryItem)
                }
                
                if let url = self.userReactionsCountUrl.appending(queryItems: queryItems) {
                    fulfill(url)
                }
            case .next:
                if let url = self.sessionPageURLRepo[pageResultURLID]?.nextURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.nextPageUnavailable)
                }
            case .previous:
                if let url = self.sessionPageURLRepo[pageResultURLID]?.previousURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.previousPageUnavailable)
                }
            }
        }
        
        firstly {
            Promises.zip(
                reactionCountURL,
                self.sdk.accessTokenVendor.whenAccessToken
            )
        }.then { url, accessToken in
            self.sdk.reactionAPI.getUserReactionsCount(
                userReactionsCountURL: url,
                accessToken: accessToken
            )
        }.then { paginatedReactionSpaces in
            self.sessionPageURLRepo[pageResultURLID] = PaginatedResultURL(
                nextURL: paginatedReactionSpaces.next,
                previousURL: paginatedReactionSpaces.previous
            )
            completion(.success(paginatedReactionSpaces.items))
        }.catch { error in
            log.error("Failed getting list of reaction spaces")
            completion(.failure(error))
        }
    }
    
    func channel(_ channel: PubSubChannel, messageCreated message: PubSubChannelMessage) {
        guard let payload = try? PubSubChatMessageDecoder.shared.decode(dict: message.message) else {
            log.error("Failed to decode pub sub chat message.")
            return
        }
        
        switch payload {
        case .userReactionAdded(let payload):
            let userReaction = UserReaction(
                id: payload.id,
                targetID: payload.targetID,
                reactedByID: payload.reactedByID,
                createdAt: payload.createdAt,
                reactionID: payload.reactionID,
                reactionSpaceID: payload.reactionSpaceID,
                customData: payload.customData,
                clientID: payload.clientID
            )
            if let delegate = self.sessionDelegate {
                delegate.reactionSession(self, didAddReaction: userReaction)
            }
        case .userReactionRemoved(let payload):
            let userReaction = UserReaction(
                id: payload.id,
                targetID: payload.targetID,
                reactedByID: payload.reactedByID,
                createdAt: payload.createdAt,
                reactionID: payload.reactionID,
                reactionSpaceID: payload.reactionSpaceID,
                customData: payload.customData,
                clientID: payload.clientID
            )
            if let delegate = self.sessionDelegate {
                delegate.reactionSession(self, didRemoveReaction: userReaction)
            }
        case .reactionSpaceUpdated(let payload):
            let reactionSpace = ReactionSpace(
                id: payload.id,
                url: payload.url,
                name: payload.name,
                targetGroupID: payload.targetGroupID,
                reactionPackIDs: payload.reactionPackIDs,
                clientID: payload.clientID,
                reactionSpaceChannel: payload.reactionSpaceChannel,
                userReactionsURL: payload.userReactionsURL,
                userReactionDetailURLTemplate: payload.userReactionDetailURLTemplate,
                userReactionsCountURL: payload.userReactionsCountURL
            )
            if let delegate = clientDelegate {
                delegate.reactionClient(self.reactionClient, didUpdateReactionSpace: reactionSpace)
            }
        case .messageCreated, .messageDeleted, .imageCreated, .imageDeleted, .customMessageCreated, .chatroomUpdated, .addedToChatRoom, .invitedToChatRoom, .profileBlocked, .profileUnblocked, .messagePinned, .messageUnpinned:
            break
        }
    }
    
    func channel(_ channel: PubSubChannel, messageActionCreated messageAction: PubSubMessageAction) { }
    
    func channel(_ channel: PubSubChannel, messageActionDeleted messageActionID: String, messageID: String) { }
    
    deinit {
        log.info("Reaction Session has ended.")
    }
}

/// A configurable request object used to query for User Reactions List
public struct GetUserReactionsRequestOptions {

    // Is a unique identifier used for pagination functionality
    let id: String

    // Is an array of `URLQueryItem` used to compile URL parameters
    let urlQueries: [URLQueryItem]

    let reactionID: String?
    let targetID: String?
    let reactionByID: String?

    /// Initialize options to be used in the User Reaction List queries
    public init(reactionID: String?, targetID: String?, reactionByID: String?) {
        self.reactionID = reactionID
        self.targetID = targetID
        self.reactionByID = reactionByID

        self.id = {

            var id: String = ""
            if let reactionID = reactionID {
                id += reactionID
            }
            
            if let targetID = targetID {
                id += targetID
            }
            
            if let userID = reactionByID {
                id += userID
            }

            return id
        }()

        self.urlQueries = {
            var urlQueries = [URLQueryItem]()

            if let reactionID = reactionID {
                urlQueries.append(
                    URLQueryItem(
                        name: ReactionSpaceQueryKeys.reactionID.rawValue,
                        value: reactionID
                    )
                )
            }
            
            if let targetID = targetID {
                urlQueries.append(
                    URLQueryItem(
                        name: ReactionSpaceQueryKeys.targetID.rawValue,
                        value: targetID
                    )
                )
            }
            
            if let userID = reactionByID {
                urlQueries.append(
                    URLQueryItem(
                        name: ReactionSpaceQueryKeys.reactedByID.rawValue,
                        value: userID
                    )
                )
            }

            return urlQueries
        }()
    }
}
