//
//  LiveLikeReactionAPI.swift
//  EngagementSDK
//
//  Created by Keval Shah on 20/07/22.
//

import Foundation
import LiveLikeCore

protocol LiveLikeReactionAPIProtocol {

    func createReactionSpace(
        reactionSpaceURL: URL,
        name: String?,
        targetGroupID: String,
        reactionPackIDs: [String],
        accessToken: AccessToken
    ) -> Promise<ReactionSpace>

    func getReactionSpaces(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<ReactionSpace>>

    func updateReactionSpace(
        reactionSpaceURL: URL,
        reactionPackIDs: [String],
        accessToken: AccessToken
    ) -> Promise<UpdateReactionSpaceResult>

    func deleteReactionSpace(
        reactionSpaceURL: URL,
        accessToken: AccessToken
    ) -> Promise<Bool>

    func getReactionSpaceInfo(
        reactionSpaceURL: URL,
        accessToken: AccessToken
    ) -> Promise<ReactionSpace>

    func getReactionPacks(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<ReactionPack>>

    func getReactionPackInfo(
        reactionPackURL: URL,
        accessToken: AccessToken
    ) -> Promise<ReactionPack>

    func addUserReactions(
        userReactionURL: URL,
        reactionSpaceID: String,
        targetID: String,
        reactionID: String,
        customData: String?,
        accessToken: AccessToken
    ) -> Promise<UserReaction>

    func removeUserReactions(
        userReactionURL: URL,
        accessToken: AccessToken
    ) -> Promise<Bool>

    func getUserReactions(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<UserReaction>>

    func getUserReactionsCount(
        userReactionsCountURL: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<UserReactionsCountResult>>
}

class LiveLikeReactionAPI: LiveLikeReactionAPIProtocol {

    private let coreAPI: LiveLikeCoreAPIProtocol

    init(coreAPI: LiveLikeCoreAPIProtocol) {
        self.coreAPI = coreAPI
    }

    func createReactionSpace(
        reactionSpaceURL: URL,
        name: String?,
        targetGroupID: String,
        reactionPackIDs: [String],
        accessToken: AccessToken
    ) -> Promise<ReactionSpace> {
        let createReactionSpaceBody = CreateReactionSpaceBody(
            name: name,
            targetGroupId: targetGroupID,
            reactionPackIds: reactionPackIDs
        )
        let resource = Resource<ReactionSpace>.init(
            url: reactionSpaceURL,
            method: .post(createReactionSpaceBody),
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func updateReactionSpace(
        reactionSpaceURL: URL,
        reactionPackIDs: [String],
        accessToken: AccessToken
    ) -> Promise<UpdateReactionSpaceResult> {
        return firstly {
            struct Payload: Encodable {
                let reactionPackIds: [String]
            }
            let payload = Payload(reactionPackIds: reactionPackIDs)

            let resource = Resource<UpdateReactionSpaceResult>(
                url: reactionSpaceURL,
                method: .patch(payload),
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { reactionSpaceIDList in
            return Promise(value: reactionSpaceIDList)
        }
    }

    func deleteReactionSpace(
        reactionSpaceURL: URL,
        accessToken: AccessToken
    ) -> Promise<Bool> {
        return firstly {
            let resource = Resource<Bool>(
                url: reactionSpaceURL,
                method: .delete(EmptyBody()),
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { result in
            return Promise(value: result)
        }
    }

    func getReactionSpaces(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<ReactionSpace>> {
        return firstly { () -> Promise<PaginatedResult<ReactionSpace>> in
            let resource = Resource<PaginatedResult<ReactionSpace>>.init(
                get: url,
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { reactionSpaceList in
            return Promise(value: reactionSpaceList)
        }
    }

    func getReactionPacks(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<ReactionPack>> {
        return firstly { () -> Promise<PaginatedResult<ReactionPack>> in
            let resource = Resource<PaginatedResult<ReactionPack>>.init(
                get: url,
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { reactionSpaceList in
            return Promise(value: reactionSpaceList)
        }
    }

    func getReactionSpaceInfo(
        reactionSpaceURL: URL,
        accessToken: AccessToken
    ) -> Promise<ReactionSpace> {
        let resource = Resource<ReactionSpace>(
            get: reactionSpaceURL,
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func getReactionPackInfo(
        reactionPackURL: URL,
        accessToken: AccessToken
    ) -> Promise<ReactionPack> {
        let resource = Resource<ReactionPack>(
            get: reactionPackURL,
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func addUserReactions(
        userReactionURL: URL,
        reactionSpaceID: String,
        targetID: String,
        reactionID: String,
        customData: String?,
        accessToken: AccessToken
    ) -> Promise<UserReaction> {
        let addUserReactionBody = AddUserReactionBody(
            reactionSpaceId: reactionSpaceID,
            targetId: targetID,
            reactionId: reactionID,
            customData: customData
        )
        let resource = Resource<UserReaction>.init(
            url: userReactionURL,
            method: .post(addUserReactionBody),
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func removeUserReactions(
        userReactionURL: URL,
        accessToken: AccessToken
    ) -> Promise<Bool> {
        return firstly {
            let resource = Resource<Bool>(
                url: userReactionURL,
                method: .delete(EmptyBody()),
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { result in
            return Promise(value: result)
        }
    }

    func getUserReactions(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<UserReaction>> {
        return firstly { () -> Promise<PaginatedResult<UserReaction>> in
            let resource = Resource<PaginatedResult<UserReaction>>.init(
                get: url,
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { userReactionList in
            return Promise(value: userReactionList)
        }
    }

    func getUserReactionsCount(
        userReactionsCountURL: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<UserReactionsCountResult>> {
        return firstly { () -> Promise<PaginatedResult<UserReactionsCountResult>> in
            let resource = Resource<PaginatedResult<UserReactionsCountResult>>(
                get: userReactionsCountURL,
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { countList in
            return Promise(value: countList)
        }.catch { error in
            log.error(error)
        }
    }
}

// MARK: - Models
public struct UserReactionsCountResult: Decodable {
    public let targetID: String
    public let reactions: [UserReactionCount]

    enum CodingKeys: String, CodingKey {
        case targetId
        case reactions
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        targetID = try container.decode(String.self, forKey: .targetId)
        reactions = try container.decode([UserReactionCount].self, forKey: .reactions)
    }
}

public struct UserReactionCount: Decodable {
    public let reactionID: String
    public let count: Int
    @available(*, deprecated, renamed: "selfReactedUserReactionId", message: "This has been renamed and is now optional.")
    public let selfReactedUserReactionID: String
    public let selfReactedUserReactionId: String?

    enum CodingKeys: String, CodingKey {
        case reactionId
        case count
        case selfReactedUserReactionId
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        reactionID = try container.decode(String.self, forKey: .reactionId)
        count = try container.decode(Int.self, forKey: .count)
        let selfUserReactionID = try container.decode(String?.self, forKey: .selfReactedUserReactionId)
        self.selfReactedUserReactionId = selfUserReactionID
        self.selfReactedUserReactionID = selfUserReactionID ?? ""
    }
}

struct CreateReactionSpaceBody: Encodable {
    let name: String?
    let targetGroupId: String
    let reactionPackIds: [String]
}

struct AddUserReactionBody: Encodable {
    let reactionSpaceId: String
    let targetId: String
    let reactionId: String
    let customData: String?
}

struct UpdateReactionSpaceResult: Decodable {
    let reactionPackIDs: [String]

    enum CodingKeys: String, CodingKey {
        case reactionPackIds
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.reactionPackIDs = try container.decode([String].self, forKey: .reactionPackIds)
    }

    init(reactionPackIDs: [String]) {
        self.reactionPackIDs = reactionPackIDs
    }
}

enum ReactionSpaceQueryKeys: String {
    case clientID = "client_id"
    case reactionSpaceID = "reaction_space_id"
    case targetGroupID = "target_group_id"
    case targetID = "target_id"
    case reactionID = "reaction_id"
    case reactedByID = "reacted_by_id"
}
