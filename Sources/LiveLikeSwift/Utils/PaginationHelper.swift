//
//  PaginationHelper.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 8/28/22.
//

import Foundation
import LiveLikeCore

struct PaginationHelper {

    static func whenPaginationURL(
        pageRepo: inout [String: PaginatedResultURL],
        pageResultURLID: String,
        page: Pagination,
        whenFirstPageURL: Promise<URL>
    ) -> Promise<URL> {
        let whenPaginationURL: Promise<URL> = {
            switch page {
            case .first:
                // Reset result repo cache if the user is calling `.first` page
                pageRepo[pageResultURLID] = nil
                return whenFirstPageURL
            case .next:
                if let url = pageRepo[pageResultURLID]?.nextURL {
                    return Promise(value: url)
                } else {
                    return Promise(error: PaginationErrors.nextPageUnavailable)
                }
            case .previous:
                if let url = pageRepo[pageResultURLID]?.previousURL {
                    return Promise(value: url)
                } else {
                    return Promise(error: PaginationErrors.previousPageUnavailable)
                }
            }
        }()
        return whenPaginationURL
    }
}
