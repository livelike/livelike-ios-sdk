//
//  String+Decoder.swift
//  EngagementSDK
//
//  Created by Keval Shah on 11/05/22.
//

import Foundation

extension String {
    var decodedTextMessage: String {
        let decoderDictionary = [
            "&amp;": "&",
            "&lt;": "<",
            "&gt;": ">",
            "&quot;": "\"",
            "&#47;": "/"
        ]
        var finalString = self

        for decodeValue in decoderDictionary {
            finalString = finalString.replacingOccurrences(of: decodeValue.key, with: decodeValue.value)
        }

        return finalString
    }
}
