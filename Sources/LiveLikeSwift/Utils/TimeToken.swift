//
//  TimeToken.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 1/2/20.
//

import Foundation

enum TimeTokenError: LocalizedError {
    case encodingAsQueryParameterFailed

    public var errorDescription: String? {
        switch self {
        case .encodingAsQueryParameterFailed:
            return "Encoding Timetoken value as query parameter failed."
        }
    }
}

/// Represents a UTC time
///
/// Implements NSCoding for easy conversion to Data using NSKeyedArchiver/NSKeyedUnarchiver.
/// This is helpful if you need to store in persistent storage like UserDefaults.
@objc(LLTimeToken)
public class TimeToken: NSObject, Comparable, NSCoding {

    /// A 17-Digit precision utc time
    public let pubnubTimetoken: UInt64
    /// An approximate Date representation of the TimeToken
    public var approximateDate: Date

    init(pubnubTimetoken: UInt64) {
        self.pubnubTimetoken = pubnubTimetoken
        self.approximateDate = Date(timeIntervalSince1970: TimeInterval(pubnubTimetoken) / 10_000_000)
    }

    /// Creates an approximate Timetoken using a Date. Accuracy is not guaranteed.
    public init(date: Date) {
        self.approximateDate = date
        self.pubnubTimetoken = {
            var timeInterval = date.timeIntervalSince1970
            if timeInterval < 0 {
                timeInterval = 0
            }
            return UInt64(timeInterval * 10_000_000)
        }()
    }

    /// An approximate Timetoken of the 'now' Date. Accuracy is not guranteed.
    public static var now: TimeToken {
        return .init(date: Date())
    }

    public static func < (lhs: TimeToken, rhs: TimeToken) -> Bool {
        return lhs.pubnubTimetoken < rhs.pubnubTimetoken
    }

    public override func isEqual(_ object: Any?) -> Bool {
        guard let object = object as? TimeToken else { return false }
        return self.pubnubTimetoken == object.pubnubTimetoken
    }

    public func encode(with coder: NSCoder) {
        coder.encode(pubnubTimetoken, forKey: "pntimetoken")
    }

    public required init?(coder: NSCoder) {
        guard let pnTimeToken = coder.decodeObject(forKey: "pntimetoken") as? UInt64 else {
            return nil
        }
        self.pubnubTimetoken = pnTimeToken
        self.approximateDate = Date(timeIntervalSince1970: TimeInterval(pubnubTimetoken) / 10_000_000)
    }

    public override var description: String {
        return "\(approximateDate) | \(pubnubTimetoken)"
    }

}

extension TimeToken {
    func encode() throws -> String {
        var urlEncodeCharacterSet = CharacterSet.urlQueryAllowed
        urlEncodeCharacterSet.remove("+")

        guard let encodedDate = self.approximateDate.stringOptional?.addingPercentEncoding(withAllowedCharacters: urlEncodeCharacterSet) else {
            throw TimeTokenError.encodingAsQueryParameterFailed
        }

        return encodedDate
    }
}
