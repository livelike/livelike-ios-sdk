//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation
import LiveLikeCore

public typealias Pagination = LiveLikeCore.Pagination
public typealias TemplatedURLHelper = LiveLikeCore.TemplatedURLHelper
