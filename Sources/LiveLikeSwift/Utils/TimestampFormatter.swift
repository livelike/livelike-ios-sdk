//
//  TimestampFormatter.swift
//  
//
//  Created by Jelzon Monzon on 11/10/22.
//

import Foundation

public typealias TimestampFormatter = (Date) -> String
