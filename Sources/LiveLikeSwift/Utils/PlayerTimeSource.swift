//
//  PlayerTimeSource.swift
//  
//
//  Created by Jelzon Monzon on 11/9/22.
//

import Foundation

public typealias PlayerTimeSource = (() -> TimeInterval?)
