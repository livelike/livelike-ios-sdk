//
//  PubSubChatMessage.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

struct PubSubChatMessage: Codable {
    let event: PubSubChatEvent
    let payload: PubSubChatPayload

    init(event: PubSubChatEvent, payload: PubSubChatPayload) {
        self.event = event
        self.payload = payload
    }
}
