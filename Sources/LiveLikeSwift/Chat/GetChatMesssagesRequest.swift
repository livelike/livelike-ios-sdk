//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//
	

import Foundation
import LiveLikeCore

struct GetChatMesssagesRequest: LLRequest {
    
    
    private let accessToken: String
    private let networking: LLNetworking
    private let options: GetChatMessageRequestOptions
    
    init(
        accessToken: String,
        networking: LLNetworking,
        options: GetChatMessageRequestOptions
    ) {
        self.accessToken = accessToken
        self.networking = networking
        self.options = options
    }
    
    func execute(
        url: URL,
        completion: @escaping (Result<GetMessagesResult, Error>) -> Void
    ) {
        do {
            let chatroomMessagesHistoryURL = try url.with(queryItems: {
                var queryItems: [URLQueryItem] = [
                    URLQueryItem(
                        name: ChatMessageQueryKeys.chatRoomID.rawValue,
                        value: options.chatRoomID
                    )
                ]
                if let pageSize = options.pageSize?.description {
                    queryItems.append(
                        URLQueryItem(
                            name: ChatMessageQueryKeys.pageSize.rawValue,
                            value: pageSize
                        )
                    )
                }
                
                if let encodedTimestamp = try options.since?.encode() {
                    queryItems.append(
                        URLQueryItem(
                            name: ChatMessageQueryKeys.since.rawValue,
                            value: String(describing: encodedTimestamp)
                        )
                    )
                }
                
                if let parentMessageID = options.parentMessageID {
                    queryItems.append(
                        URLQueryItem(
                            name: ChatMessageQueryKeys.parentMessageID.rawValue,
                            value: parentMessageID
                        )
                    )
                }
                return queryItems
            }())
            let resource = Resource<GetMessagesResult>(
                get: chatroomMessagesHistoryURL,
                accessToken: accessToken
            )
            DispatchQueue.global().async {
                networking.task(resource, completion: completion)
            }
        } catch {
            completion(.failure(error))
        }
    }
}
