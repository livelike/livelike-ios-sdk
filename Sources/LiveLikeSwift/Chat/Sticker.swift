//
//  Sticker.swift
//  EngagementSDK
//
//  Created by Heinrich Dahms on 2019-05-21.
//

import Foundation

/// A Sticker that can be used in Chat messages
public struct Sticker: Equatable {
    init(id: String, url: URL, packId: String, packUrl: URL, packName: String, shortcode: String, file: URL) {
        self.id = id
        self.packId = packId
        self.packName = packName
        self.shortcode = shortcode
        self.file = file
        self.url = url
        self.packUrl = packUrl
    }

    /// The unique id of the Sticker
    public let id: String
    /// The unique id of the Sticker Pack this Sticker belongs to
    public let packId: String
    /// The name of the Sticker Pack this Sticker belongs to
    public let packName: String
    /// The  shortcode to be used in a Chat Message
    public let shortcode: String
    /// The url of the Sticker's image
    public let file: URL

    let url: URL
    let packUrl: URL
}

extension Sticker: Decodable {}
