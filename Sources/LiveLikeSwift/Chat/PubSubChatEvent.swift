//
//  PubSubChatEvent.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

enum PubSubChatEvent: String, Codable {
    case messageCreated = "message-created"
    case messageDeleted = "message-deleted"
    case imageCreated = "image-created"
    case imageDeleted = "image-deleted"
    case customMessageCreated = "custom-message-created"
    case chatroomUpdated = "chatroom-updated"
    case addedToChatroom = "chat-room-add"
    case invitedToChatroom = "chat-room-invite"
    case profileBlocked = "block-profile"
    case profileUnblocked = "unblock-profile"
    case messagePinned = "message-pinned"
    case messageUnpinned = "message-unpinned"
    case userReactionAdded = "user-reaction-added"
    case userReactionRemoved = "user-reaction-removed"
    case reactionSpaceUpdated = "reaction-space-updated"
}
