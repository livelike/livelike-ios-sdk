//
//  ChatSessionConfig.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 3/6/20.
//

import Foundation

/// Configuration to connect to a chat room
public struct ChatSessionConfig {
    /// The unique id of the Chat Room to connect
    public let roomID: String
    
    /// A timesource used for Spoiler Free Sync
    public var syncTimeSource: PlayerTimeSource?
    
    @available(*, deprecated, message: "This property no longer works. History limit is set at 25.")
    public var messageHistoryLimit: UInt = 50
    
    /// Show or hide user avatar next to a chat message
    public var shouldDisplayAvatar: Bool = false
    
    /// Enable / Disable URL links in chat messages”
    public var enableChatMessageURLs: Bool = true
    
    /// The RegEx to be set for customized URL validation
    public var chatMessageUrlPatterns: String?
    
    /// Toggle whether `ChatSession` should contain content filtered messages
    public var includeFilteredMessages: Bool = false
    
    /// Initialize a ChatSessionConfig
    /// - Parameter roomID: The unique id of the Chat Room to connect
    public init(roomID: String) {
        self.roomID = roomID
    }
}
