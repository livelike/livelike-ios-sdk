//
//  StickerPack.swift
//  EngagementSDK
//
//  Created by Heinrich Dahms on 2019-05-21.
//

import Foundation

/// A collection of Stickers
public struct StickerPack {
    /// The id of the Sticker Pack
    public let id: String
    /// The name of  the Sticker Pack
    public let name: String
    /// The url of the Sticker Pack's image
    public let file: URL
    /// The  stickers of the Sticker Pack
    public let stickers: [Sticker]

    let url: URL?
}

extension StickerPack: Decodable { }
