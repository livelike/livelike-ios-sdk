//
//  GetChatRoomMembershipsRequest.swift
//  
//
//  Created by Jelzon Monzon on 6/23/23.
//

import Foundation
import LiveLikeCore

/// Options for a GetChatRoomMembershipsRequest
public struct GetChatRoomMembershipsRequestOptions {
    let roomID: String?
    let profileIDs: Set<String>?
    
    /// - Parameters:
    ///   - roomID: filter by a chat room id
    ///   - profileIDs: filter by a profile id(s)
    public init(
        roomID: String? = nil,
        profileIDs: Set<String>? = nil
    ) {
        self.roomID = roomID
        self.profileIDs = profileIDs
    }
}

class GetChatRoomMembershipsRequest: LLRequest {
    
    private let options: GetChatRoomMembershipsRequestOptions?
    private let networking: LLNetworking
    private let accessToken: String
    
    init(
        options: GetChatRoomMembershipsRequestOptions?,
        networking: LLNetworking,
        accessToken: String
    ) {
        self.options = options
        self.networking = networking
        self.accessToken = accessToken
    }
    
    func execute(
        url: URL,
        completion: @escaping (Result<PaginatedResult<ChatRoomMember>, Error>) -> Void
    ) {
        struct ChatRoomMembershipPage: Decodable {
            var results: [ChatRoomMember]
            var next: URL?
            var previous: URL?
        }
        let resource = Resource<ChatRoomMembershipPage>(
            get: url,
            accessToken: accessToken
        )
        self.networking.task(resource) { result in
            completion(result.map { chatRoomMembershipPage in
                PaginatedResult<ChatRoomMember>(
                    previous: chatRoomMembershipPage.previous,
                    count: chatRoomMembershipPage.results.count,
                    next: chatRoomMembershipPage.next,
                    items: chatRoomMembershipPage.results
                )
            })
        }
    }
}

extension GetChatRoomMembershipsRequest: LLPaginatedRequest {
    typealias ElementType = ChatRoomMember
    
    var requestID: String {
        var id = "getChatRoomMemberships"
        
        if let profileIDs = options?.profileIDs {
            id += profileIDs.joined()
        }
        
        if let roomID = options?.roomID {
            id += roomID
        }
        
        return id
    }
}
