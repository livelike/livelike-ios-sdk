//
//  PubSubChatMessageDecoder.swift
//
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation
import LiveLikeCore

/// Used to decode chat messages from a PubSubService
class PubSubChatMessageDecoder {
    enum PubSubChatEventWithPayload {
        case messageCreated(PubSubChatPayload)
        case messageDeleted(MessageDeletedPayload)
        case imageCreated(PubSubChatPayload)
        case imageDeleted(MessageDeletedPayload)
        case customMessageCreated(PubSubChatPayload)
        case chatroomUpdated(PubSubChatRoomPayload)
        case addedToChatRoom(PubSubAddToChatPayload)
        case invitedToChatRoom(PubSubChatRoomInvitationPayload)
        case profileBlocked(BlockInfo)
        case profileUnblocked(UnblockInfo)
        case messagePinned(PinMessageInfo)
        case messageUnpinned(String)
        case userReactionAdded(UserReaction)
        case userReactionRemoved(UserReaction)
        case reactionSpaceUpdated(ReactionSpace)
    }

    private let decoder: LLJSONDecoder = LLJSONDecoder()

    static let shared = PubSubChatMessageDecoder()

    func decode(dict: [String: Any]) throws -> PubSubChatEventWithPayload {
        guard let eventString = dict["event"] as? String else {
            throw PubSubChatRoomDecodeError.failedToDecodePayload(decodingError: "'event' is missing from PubSubChatEventWithPayload")
        }

        guard let event: PubSubChatEvent = PubSubChatEvent(rawValue: eventString) else {
            throw PubSubChatRoomDecodeError.failedToDecodePayload(decodingError: "Failed to create PubSubChatEvent")
        }

        guard let messagePayload = dict["payload"] else {
            throw PubSubChatRoomDecodeError.missingPayload
        }

        let payloadData = try JSONSerialization.data(
            withJSONObject: messagePayload,
            options: .prettyPrinted
        )

        switch event {
        case .messageCreated:
            return .messageCreated(
                try self.decoder.decode(
                    PubSubChatPayload.self,
                    from: payloadData
                )
            )
        case .messageDeleted:
            return .messageDeleted(
                try self.decoder.decode(
                    MessageDeletedPayload.self,
                    from: payloadData
                )
            )
        case .imageCreated:
            return .imageCreated(
                try self.decoder.decode(
                    PubSubChatPayload.self,
                    from: payloadData
                )
            )
        case .imageDeleted:
            return .imageDeleted(
                try self.decoder.decode(
                    MessageDeletedPayload.self,
                    from: payloadData
                )
            )
        case .customMessageCreated:
            return .customMessageCreated(
                try self.decoder.decode(
                    PubSubChatPayload.self,
                    from: payloadData
                )
            )
        case .chatroomUpdated:
            return .chatroomUpdated(
                try self.decoder.decode(
                    PubSubChatRoomPayload.self,
                    from: payloadData
                )
            )
        case .addedToChatroom:
            return .addedToChatRoom(
                try self.decoder.decode(
                    PubSubAddToChatPayload.self,
                    from: payloadData
                )
            )
        case .invitedToChatroom:
            return .invitedToChatRoom(
                try self.decoder.decode(
                    PubSubChatRoomInvitationPayload.self,
                    from: payloadData
                )
            )
        case .profileBlocked:
            return .profileBlocked(
                try self.decoder.decode(
                    BlockInfo.self,
                    from: payloadData
                )
            )
        case .profileUnblocked:
            return .profileUnblocked(
                try self.decoder.decode(
                    UnblockInfo.self,
                    from: payloadData
                )
            )
        case .messagePinned:
            return .messagePinned(
                try self.decoder.decode(
                    PinMessageInfo.self,
                    from: payloadData
                )
            )
        case .messageUnpinned:
            return .messageUnpinned(
                try self.decoder.decode(
                    String.self,
                    from: payloadData
                )
            )
        case .userReactionAdded:
            return .userReactionAdded(
                try self.decoder.decode(
                    UserReaction.self,
                    from: payloadData
                )
            )
        case .userReactionRemoved:
            return .userReactionRemoved(
                try self.decoder.decode(
                    UserReaction.self,
                    from: payloadData
                )
            )
        case .reactionSpaceUpdated:
            return .reactionSpaceUpdated(
                try self.decoder.decode(
                    ReactionSpace.self,
                    from: payloadData
                )
            )
        }
    }
}
