//
//  UserChatRoomMembershipsResult.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

struct UserChatRoomMembershipsResult {
    let chatRooms: [ChatRoomInfo]
    let next: URL?
    let previous: URL?
}
