//
//  ChatRoomReactionVendor.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 5/27/20.
//

import Foundation
import LiveLikeCore

class ChatRoomReactionVendor: ReactionVendor {
    private let reactionPacksUrl: URL
    private let isPubnubEnabled: Bool

    init(reactionPacksUrl: URL, isPubnubEnabled: Bool) {
        self.reactionPacksUrl = reactionPacksUrl
        self.isPubnubEnabled = isPubnubEnabled
    }

    func getReactions() -> Promise<[Reaction]> {
        return firstly {
            loadedReactions
        }.recover { error in
            log.error("ChatRoomReactionVendor.getReactions() recovering from error: \(error.localizedDescription)")
            return Promise(value: [])
        }
    }

    private lazy var loadedReactions: Promise<[Reaction]> = {
        return firstly {
            return self.loadReactionPack(atURL: reactionPacksUrl)
        }.then { reactionPacks -> Promise<[Reaction]> in
            guard let reactionPack = reactionPacks.results.first else {
                log.debug("Reaction Packs Resource is Empty")
                return Promise(value: [])
            }
            let reactionAssets = reactionPack.emojis.map { $0 }
            return Promise(value: reactionAssets)
        }
    }()

    private func loadReactionPack(atURL url: URL) -> Promise<ReactionPacks> {
        if isPubnubEnabled {
            let resource = Resource<ReactionPacks>(get: url)
            return LiveLike.networking.load(resource)
        } else {
            return Promise(value: ReactionPacks(results: []))
        }
    }
}
