//
//  ChatRoomEventPayload.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

struct ChatRoomEventPayload: Decodable {

    var id: String
    var clientMessageId: String
    var message: String?
    var imageURL: URL?
    var imageHeight: Int?
    var imageWidth: Int?
    var customData: String?
    var senderId: String?
    var senderNickname: String?
    var senderImageUrl: URL?
    var programDateTime: Date?
    var createdAt: Date
    var updatedAt: Date?
    var pubnubTimetoken: String?
    // The message after it has been filtered
    var filteredMessage: String?
    // The reasons why a message was filtered
    var contentFilter: [ChatFilter]?

    var filteredSet: Set<ChatFilter> {
        return Set(contentFilter ?? [])
    }
    var messageEvent: PubSubChatEvent?

    var quoteMessageId: String?
    var quoteMessage: ChatMessagePayload?
    var chatRoomId: String?
    var messageMetadata: MessageMetadataCodable?

    private enum CodingKeys: String, CodingKey {
        case id
        case clientMessageId
        case message
        case imageUrl
        case imageHeight
        case imageWidth
        case customData
        case senderId
        case senderNickname
        case senderImageUrl
        case programDateTime
        case filteredMessage
        case contentFilter
        case messageEvent
        case quoteMessage
        case quoteMessageId
        case chatRoomId
        case messageMetadata
        case createdAt
        case updatedAt
        case pubnubTimetoken
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id).lowercased()
        self.clientMessageId = try (container.decodeIfPresent(String.self, forKey: .clientMessageId) ?? "").lowercased()
        self.message = try? container.decodeIfPresent(String.self, forKey: .message)?.decodedTextMessage
        self.imageURL = try container.decodeIfPresent(URL.self, forKey: .imageUrl)
        self.imageHeight = try container.decodeIfPresent(Int.self, forKey: .imageHeight)
        self.imageWidth = try container.decodeIfPresent(Int.self, forKey: .imageWidth)
        self.customData = try container.decodeIfPresent(String.self, forKey: .customData)
        self.senderId = try? container.decode(String.self, forKey: .senderId)
        self.senderNickname = try? container.decode(String.self, forKey: .senderNickname)
        self.senderImageUrl = try? container.decode(URL.self, forKey: .senderImageUrl)
        self.programDateTime = try? container.decode(Date.self, forKey: .programDateTime)
        self.filteredMessage = try? container.decode(String.self, forKey: .filteredMessage)
        self.contentFilter = try? container.decode([ChatFilter].self, forKey: .contentFilter)
        self.messageEvent = try? container.decode(PubSubChatEvent.self, forKey: .messageEvent)
        self.quoteMessage = try container.decodeIfPresent(ChatMessagePayload.self, forKey: .quoteMessage)
        self.quoteMessageId = try container.decodeIfPresent(String.self, forKey: .quoteMessageId)
        self.chatRoomId = try? container.decode(String.self, forKey: .chatRoomId)
        self.messageMetadata = try container.decodeIfPresent(MessageMetadataCodable.self, forKey: .messageMetadata)
        self.createdAt = try container.decode(Date.self, forKey: .createdAt)
        self.updatedAt = try? container.decodeIfPresent(Date.self, forKey: .updatedAt)
        self.pubnubTimetoken = try? container.decodeIfPresent(String.self, forKey: .pubnubTimetoken)
    }

    init(
        id: String?,
        clientMessageId: String,
        message: String?,
        imageURL: URL?,
        imageHeight: Int?,
        imageWidth: Int?,
        customData: String?,
        senderId: String?,
        senderNickname: String?,
        senderImageUrl: URL?,
        programDateTime: Date?,
        filteredMessage: String?,
        contentFilter: [ChatFilter]?,
        messageEvent: PubSubChatEvent?,
        quoteMessage: ChatMessagePayload?,
        chatRoomId: String?,
        messageMetadata: MessageMetadataCodable?,
        createdAt: Date,
        updatedAt: Date?,
        pubnubTimetoken: String?
    ) {
        self.id = (id ?? "").lowercased()
        self.clientMessageId = clientMessageId.lowercased()
        self.message = message
        self.imageURL = imageURL
        self.imageHeight = imageHeight
        self.imageWidth = imageWidth
        self.customData = customData
        self.senderId = senderId
        self.senderNickname = senderNickname
        self.senderImageUrl = senderImageUrl
        self.programDateTime = programDateTime
        self.filteredMessage = filteredMessage
        self.contentFilter = contentFilter
        self.messageEvent = messageEvent
        self.quoteMessage = quoteMessage
        self.quoteMessageId = quoteMessage?.id
        self.chatRoomId = chatRoomId
        self.messageMetadata = messageMetadata
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.pubnubTimetoken = pubnubTimetoken
    }
}
