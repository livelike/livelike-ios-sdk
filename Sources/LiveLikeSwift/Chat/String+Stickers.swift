//
//  String+Stickers.swift
//  EngagementSDK
//
//  Created by Heinrich Dahms on 2019-06-17.
//

import Foundation

extension String {
    func getStickerShortcodes(stickerPacks: [StickerPack]) -> [String] {
        var stickerShortcodes = [String]()
        let allStickers = stickerPacks.flatMap { $0.stickers }

        do {
            let regex = try NSRegularExpression(pattern: ":(.*?):", options: [])
            let regexRange = NSRange(location: 0, length: utf16.count)

            let matches = regex.matches(in: self, options: [], range: regexRange)

            for match in matches {
                let r = match.range(at: 1)
                if let range = Range(r, in: self) {
                    let shortcode = String(self[range])
                    if let validShortcode = allStickers.first(where: { $0.shortcode == shortcode.replacingOccurrences(of: ":", with: "") })?.shortcode {
                        stickerShortcodes.append(validShortcode)
                    }
                }
            }
        } catch {}

        return stickerShortcodes
    }    
}
