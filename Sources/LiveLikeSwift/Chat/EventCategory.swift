//
//  EventCategory.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

enum EventCategory: String, Codable {
    case chatMessage = "chat-message"
}
