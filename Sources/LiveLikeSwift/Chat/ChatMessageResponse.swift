//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

struct ChatMessageResponse: Decodable {

    let id: String
    let clientMessageID: String?
    let message: String?
    let senderID: String
    let senderNickname: String
    let filteredMessage: String?
    let filteredReasons: Set<ChatFilter>
    let imageURL: URL?
    let imageHeight: Int?
    let imageWidth: Int?
    let customData: String?
    let timestamp: Date
    let createdAt: TimeToken
    let roomID: String
    let videoTimestamp: EpochTime?
    let reactions: ReactionVotes
    let profileImageURL: URL?
    let quoteMessage: ChatMessagePayload?
    let quoteMessageID: String?
    let messageEvent: PubSubChatEvent?
    let pubnubTimetoken: TimeToken
    let senderProfileURL: URL?
    let messageMetadata: MessageMetadataCodable?
    let repliesCount: Int?
    let repliesUrl: URL?
    let parentMessageId: String?

    enum CodingKeys: String, CodingKey {
        case id
        case clientMessageId
        case message
        case filteredMessage
        case senderId
        case senderNickname
        case senderImageUrl
        case programDateTime
        case createdAt
        case imageUrl
        case imageWidth
        case imageHeight
        case quoteMessage
        case quoteMessageId
        case messageEvent
        case contentFilter
        case chatRoomId
        case customData
        case pubnubTimetoken
        case reactions
        case senderProfileUrl
        case messageMetadata
        case repliesCount
        case repliesUrl
        case parentMessageId
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let id = try container.decode(String.self, forKey: .id)
        self.id = id
        self.clientMessageID = try? container.decode(String.self, forKey: .clientMessageId)
        self.roomID = (try container.decodeIfPresent(String.self, forKey: .chatRoomId) ?? "")
        self.message = try container.decodeIfPresent(String.self, forKey: .message)?.decodedTextMessage
        self.videoTimestamp = try? container.decodeIfPresent(Date.self, forKey: .programDateTime)?.timeIntervalSince1970
        let reactionsArray = try? container.decodeIfPresent([String: [ChatMessageReaction]].self, forKey: .reactions)
        if let reactionsArray = reactionsArray {
            self.reactions = {
                var allVotes: [ReactionVote] = []
                reactionsArray.keys.forEach { reactionKey in
                    reactionsArray[reactionKey]?.forEach { reaction in
                        let voteID = reaction.reactionVoteID()
                        let reactionID = ReactionID(fromString: reactionKey)
                        let reaction = ReactionVote(
                            voteID: voteID,
                            reactionID: reactionID,
                            senderID: reaction.uuid,
                            chatMessageID: id
                        )
                        allVotes.append(reaction)
                    }
                }
                return ReactionVotes(allVotes: allVotes)
            }()
        } else {
            self.reactions = ReactionVotes.empty
        }
        self.profileImageURL = try container.decodeIfPresent(URL.self, forKey: .senderImageUrl)
        let createdAtDate = try container.decodeIfPresent(Date.self, forKey: .createdAt)
        self.timestamp = createdAtDate ?? Date()
        self.createdAt = TimeToken(date: createdAtDate ?? Date())
        self.imageURL = try container.decodeIfPresent(URL.self, forKey: .imageUrl)
        self.imageWidth = try container.decodeIfPresent(Int.self, forKey: .imageWidth)
        self.imageHeight = try container.decodeIfPresent(Int.self, forKey: .imageHeight)
        self.filteredMessage = try container.decodeIfPresent(String.self, forKey: .filteredMessage)
        let contentFilter = try container.decode([ChatFilter].self, forKey: .contentFilter)
        self.filteredReasons = Set(contentFilter)
        self.senderID = (try container.decodeIfPresent(String.self, forKey: .senderId) ?? "")
        self.senderNickname = (try container.decodeIfPresent(String.self, forKey: .senderNickname) ?? "")
        self.customData = try container.decodeIfPresent(String.self, forKey: .customData)
        self.quoteMessage = try container.decodeIfPresent(ChatMessagePayload.self, forKey: .quoteMessage)
        self.quoteMessageID = try container.decodeIfPresent(String.self, forKey: .quoteMessageId)
        self.messageEvent = try container.decodeIfPresent(PubSubChatEvent.self, forKey: .messageEvent)
        self.senderProfileURL = try container.decodeIfPresent(URL.self, forKey: .senderProfileUrl)
        if let pubnubTimetokenInt = UInt64(try container.decodeIfPresent(String.self, forKey: .pubnubTimetoken) ?? "") {
            self.pubnubTimetoken = TimeToken(pubnubTimetoken: pubnubTimetokenInt)
        } else {
            self.pubnubTimetoken = self.createdAt
        }
        self.messageMetadata = try container.decodeIfPresent(MessageMetadataCodable.self, forKey: .messageMetadata)
        self.repliesCount = try container.decodeIfPresent(Int.self, forKey: .repliesCount)
        self.repliesUrl = try container.decodeIfPresent(URL.self, forKey: .repliesUrl)
        self.parentMessageId = try container.decodeIfPresent(String.self, forKey: .parentMessageId)
    }

    init(
        id: String,
        clientMessageID: String?,
        roomID: String,
        senderID: String,
        senderNickname: String,
        message: String?,
        videoTimestamp: EpochTime?,
        reactions: ReactionVotes,
        timestamp: Date,
        profileImageURL: URL?,
        createdAt: TimeToken,
        bodyImageURL: URL?,
        bodyImageHeight: Int?,
        bodyImageWidth: Int?,
        filteredMessage: String?,
        filteredReasons: Set<ChatFilter>,
        customData: String?,
        quoteMessage: ChatMessagePayload?,
        messageEvent: PubSubChatEvent?,
        pubnubTimetoken: TimeToken,
        senderProfileURL: URL?,
        messageMetadata: MessageMetadataCodable?,
        repliesCount: Int?,
        repliesUrl: URL?,
        parentMessageId: String?
    ) {
        self.id = id
        self.clientMessageID = clientMessageID
        self.roomID = roomID
        self.message = message
        self.videoTimestamp = videoTimestamp
        self.reactions = reactions
        self.profileImageURL = profileImageURL
        self.timestamp = timestamp
        self.createdAt = createdAt
        self.imageURL = bodyImageURL
        self.imageHeight = bodyImageHeight
        self.imageWidth = bodyImageWidth
        self.filteredMessage = filteredMessage
        self.filteredReasons = filteredReasons
        self.senderID = senderID
        self.senderNickname = senderNickname
        self.customData = customData
        self.quoteMessage = quoteMessage
        self.quoteMessageID = quoteMessage?.id
        self.messageEvent = messageEvent
        self.pubnubTimetoken = pubnubTimetoken
        self.senderProfileURL = senderProfileURL
        self.messageMetadata = messageMetadata
        self.repliesCount = repliesCount
        self.repliesUrl = repliesUrl
        self.parentMessageId = parentMessageId
    }
}
