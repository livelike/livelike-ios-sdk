//
//  File.swift
//  
//
//  Created by apple on 21/06/23.
//

import Foundation
import LiveLikeCore

struct GetSmartContractsRequest {

    private let accessToken: String
    private let networking: LLNetworking

    init(
        accessToken: String,
        networking: LLNetworking
    ) {
        self.accessToken = accessToken
        self.networking = networking
    }
}

extension GetSmartContractsRequest: LLRequest {
    typealias ResponseType = PaginatedResult<SmartContractInfo>

    func execute(
        url: URL,
        completion: @escaping (Result<PaginatedResult<SmartContractInfo>, Error>) -> Void
    ) {
        let resource = Resource<PaginatedResult<SmartContractInfo>>(
            get: url,
            accessToken: accessToken
        )
        networking.task(resource, completion: completion)
    }
}

extension GetSmartContractsRequest: LLPaginatedRequest {
    typealias ElementType = SmartContractInfo

    var requestID: String {
        let id: String = "getSmartContracts"
        return id
    }
}
