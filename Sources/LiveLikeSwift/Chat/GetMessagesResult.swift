//
//  GetMessagesResult.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

struct GetMessagesResult: Decodable {
    let results: [ChatMessageResponse]
    let previous: URL?
}
