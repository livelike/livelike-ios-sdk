//
//  PubSubAddToChatPayload.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

struct PubSubAddToChatPayload: Codable {
    var id: String
    var chatRoomId: String
    var chatRoomTitle: String?
    var senderId: String
    var senderNickname: String
    var senderImageUrl: URL?
    var badgeImageUrl: URL?
    var senderProfileUrl: URL
    var createdAt: Date

    private enum CodingKeys: String, CodingKey {
        case id
        case chatRoomId
        case chatRoomTitle
        case senderId
        case senderNickname
        case senderImageUrl
        case badgeImageUrl
        case senderProfileUrl
        case createdAt
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id).lowercased()
        self.chatRoomId = try container.decode(String.self, forKey: .chatRoomId)
        self.chatRoomTitle = try? container.decode(String.self, forKey: .chatRoomTitle)
        self.senderId = try container.decode(String.self, forKey: .senderId)
        self.senderNickname = try container.decode(String.self, forKey: .senderNickname)
        self.senderImageUrl = try? container.decode(URL.self, forKey: .senderImageUrl)
        self.badgeImageUrl = try? container.decode(URL.self, forKey: .badgeImageUrl)
        self.senderProfileUrl = try container.decode(URL.self, forKey: .senderProfileUrl)
        self.createdAt = try container.decode(Date.self, forKey: .createdAt)
    }

    init(
        id: String,
        chatRoomId: String,
        chatRoomTitle: String?,
        senderId: String,
        senderNickname: String,
        senderImageUrl: URL?,
        badgeImageUrl: URL?,
        senderProfileUrl: URL,
        createdAt: Date
    ) {
        self.id = id.lowercased()
        self.chatRoomId = chatRoomId
        self.chatRoomTitle = chatRoomTitle
        self.senderId = senderId
        self.senderNickname = senderNickname
        self.senderImageUrl = senderImageUrl
        self.badgeImageUrl = badgeImageUrl
        self.senderProfileUrl = senderProfileUrl
        self.createdAt = createdAt
    }
}
