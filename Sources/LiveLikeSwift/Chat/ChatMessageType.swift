//
//  ChatMessage.swift
//  LiveLikeSDK
//
//  Created by Cory Sullivan on 2019-02-14.
//

import Foundation
#if canImport(UIKit)
    import UIKit
#endif

/// The `UserMessage` struct represents the user **message**.
public struct ChatMessage: Equatable, Decodable {
    
    // MARK: - Public Properties
    
    /// The unique identifer of the message
    public var id: ChatMessageID {
        return ChatMessageID(id: pubnubID, clientMessageID: clientMessageID)
    }
    
    private let pubnubID: String?
    private let clientMessageID: String?
    
    @available(*, deprecated, renamed: "text")
    public var message: String {
        return text ?? ""
    }
    
    /// The unique id of the message's sender
    public let senderID: String
    
    /// The nickname of the message's sender
    public let senderNickname: String
    
    @available(*, deprecated, renamed: "senderNickname")
    public var nickname: String {
        return senderNickname
    }
    
    /// The message after it has been filtered.
    public var filteredMessage: String?
    
    /// The reason(s) why a message was filtered.
    public var filteredReasons: Set<ChatFilter>
    
    /// Has the message been filtered.
    public var isMessageFiltered: Bool {
        return filteredReasons.count > 0
    }
    
    /// The text component of the message
    public let text: String?
    
    /// The URL of the message's image component
    public var imageURL: URL?
    
    public let imageHeight: Int?
    public let imageWidth: Int?
    
    /// The customData string of a custom message
    public let customData: String?
    
    /// The timestamp of when this message was created
    public let timestamp: Date

    /// The TimeToken of when this message was created.
    /// Used for `getMessages` and `getMessageCount` methods of `ContentSession`
    public let createdAt: TimeToken
    
    /// An optional image of the sender of this message
    public let profileImageURL: URL?
    
    public let messageMetadata: MessageMetadata?
    
    // MARK: - Internal Properties
    
    /// Chat Room ID
    public let roomID: String

    /// The UNIX Epoch for the senders playhead position.
    public let videoTimestamp: EpochTime?

    public var reactions: ReactionVotes
    
    public let quoteMessage: ChatMessagePayload?
    
    let quoteMessageId: String?
    
    let messageEvent: PubSubChatEvent?
    
    /// True if this message is a custom message. If true, the message contents will be in `customData`.
    public var isCustomMessage: Bool {
        return messageEvent == .customMessageCreated
    }
    
    let pubnubTimetoken: TimeToken
    
    var isMessageShadowMuted: Bool {
        return filteredReasons.contains(.shadowMuted)
    }
    
    var isMessageContentFiltered: Bool {
        return !filteredReasons.contains(.shadowMuted) && filteredReasons.count > 0
    }
    
    public let parentMessageID: String?
    public let repliesCount: Int?
    public let repliesURL: URL?
    
    enum CodingKeys: String, CodingKey {
        case id
        case clientMessageId
        case message
        case filteredMessage
        case senderId
        case senderNickname
        case senderImageUrl
        case programDateTime
        case createdAt
        case imageUrl
        case imageWidth
        case imageHeight
        case quoteMessage
        case quoteMessageId
        case messageEvent
        case contentFilter
        case chatRoomId
        case customData
        case pubnubTimetoken
        case reactions
        case messageMetadata
        case parentMessageId
        case repliesCount
        case repliesUrl
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let id = try container.decode(String.self, forKey: .id)
        self.pubnubID = id
        self.clientMessageID = try? container.decode(String.self, forKey: .clientMessageId)
        self.roomID = (try container.decodeIfPresent(String.self, forKey: .chatRoomId) ?? "")
        self.text = try container.decodeIfPresent(String.self, forKey: .message)?.decodedTextMessage
        self.videoTimestamp = try? container.decodeIfPresent(Date.self, forKey: .programDateTime)?.timeIntervalSince1970
        let reactionsArray = try? container.decodeIfPresent([String: [ChatMessageReaction]].self, forKey: .reactions)
        if let reactionsArray = reactionsArray {
            self.reactions = {
                var allVotes: [ReactionVote] = []
                reactionsArray.keys.forEach { reactionKey in
                    reactionsArray[reactionKey]?.forEach { reaction in
                        let voteID = reaction.reactionVoteID()
                        let reactionID = ReactionID(fromString: reactionKey)
                        let reaction = ReactionVote(
                            voteID: voteID,
                            reactionID: reactionID,
                            senderID: reaction.uuid,
                            chatMessageID: id
                        )
                        allVotes.append(reaction)
                    }
                }
                return ReactionVotes(allVotes: allVotes)
            }()
        } else {
            self.reactions = ReactionVotes.empty
        }
        self.profileImageURL = try container.decodeIfPresent(URL.self, forKey: .senderImageUrl)
        let createdAtDate = try container.decodeIfPresent(Date.self, forKey: .createdAt)
        self.timestamp = createdAtDate ?? Date()
        self.createdAt = TimeToken(date: createdAtDate ?? Date())
        self.imageURL = try container.decodeIfPresent(URL.self, forKey: .imageUrl)
        self.imageWidth = try container.decodeIfPresent(Int.self, forKey: .imageWidth)
        self.imageHeight = try container.decodeIfPresent(Int.self, forKey: .imageHeight)
        self.filteredMessage = try container.decodeIfPresent(String.self, forKey: .filteredMessage)
        let contentFilter = try container.decodeIfPresent([ChatFilter].self, forKey: .contentFilter) ?? []
        self.filteredReasons = Set(contentFilter)
        self.senderID = (try container.decodeIfPresent(String.self, forKey: .senderId) ?? "")
        self.senderNickname = (try container.decodeIfPresent(String.self, forKey: .senderNickname) ?? "")
        self.customData = try container.decodeIfPresent(String.self, forKey: .customData)
        self.quoteMessage = try container.decodeIfPresent(ChatMessagePayload.self, forKey: .quoteMessage)
        self.quoteMessageId = try container.decodeIfPresent(String.self, forKey: .quoteMessageId)
        self.messageEvent = try container.decodeIfPresent(PubSubChatEvent.self, forKey: .messageEvent)
        
        // timetoken may be a String (image message) or UInt64 (custom message)
        if
            let pubnubTimetokenString = try? container.decodeIfPresent(String.self, forKey: .pubnubTimetoken),
            let pubnubTimetokenInt = UInt64(pubnubTimetokenString)
        {
            self.pubnubTimetoken = TimeToken(pubnubTimetoken: pubnubTimetokenInt)
        } else if let pubnubTimetokenInt = try container.decodeIfPresent(UInt64.self, forKey: .pubnubTimetoken) {
            self.pubnubTimetoken = TimeToken(pubnubTimetoken: pubnubTimetokenInt)
        } else {
            self.pubnubTimetoken = self.createdAt
        }
        self.messageMetadata = try container.decodeIfPresent(MessageMetadataCodable.self, forKey: .messageMetadata)?.compactMapValues { $0.value }
        self.parentMessageID = try container.decodeIfPresent(String.self, forKey: .parentMessageId)
        self.repliesURL = try container.decodeIfPresent(URL.self, forKey: .repliesUrl)
        self.repliesCount = try container.decodeIfPresent(Int.self, forKey: .repliesCount)
    }

    init(
        id: String?,
        clientMessageID: String?,
        roomID: String,
        message: String?,
        senderID: String,
        senderNickname: String,
        videoTimestamp: EpochTime?,
        reactions: ReactionVotes,
        timestamp: Date,
        profileImageUrl: URL?,
        createdAt: TimeToken,
        bodyImageUrl: URL?,
        bodyImageHeight: Int?,
        bodyImageWidth: Int?,
        filteredMessage: String?,
        filteredReasons: Set<ChatFilter>,
        customData: String?,
        quoteMessage: ChatMessagePayload?,
        messageEvent: PubSubChatEvent?,
        pubnubTimetoken: TimeToken,
        messageMetadata: MessageMetadata?,
        parentMessageID: String?,
        repliesCount: Int?,
        repliesURL: URL?
    ) {
        self.pubnubID = id
        self.clientMessageID = clientMessageID
        self.roomID = roomID
        self.text = message
        self.videoTimestamp = videoTimestamp
        self.reactions = reactions
        self.profileImageURL = profileImageUrl
        self.timestamp = timestamp
        self.createdAt = createdAt
        self.imageURL = bodyImageUrl
        self.imageHeight = bodyImageHeight
        self.imageWidth = bodyImageWidth
        self.filteredMessage = filteredMessage
        self.filteredReasons = filteredReasons
        self.senderID = senderID
        self.senderNickname = senderNickname
        self.customData = customData
        self.quoteMessage = quoteMessage
        self.quoteMessageId = quoteMessage?.id
        self.messageEvent = messageEvent
        self.pubnubTimetoken = pubnubTimetoken
        self.messageMetadata = messageMetadata
        self.parentMessageID = parentMessageID
        self.repliesURL = repliesURL
        self.repliesCount = repliesCount
    }
    
    // MARK: - Equatable
    
    public static func == (lhs: ChatMessage, rhs: ChatMessage) -> Bool {
        return lhs.pubnubID == rhs.pubnubID && lhs.clientMessageID == rhs.clientMessageID
    }

    
}

// MARK: - Convenience Init

extension ChatMessage {
    init(
        from chatPubnubMessage: PubSubChatPayload,
        chatRoomID: String,
        timetoken: TimeToken,
        actions: [PubSubMessageAction]
    ) {
        let reactions: ReactionVotes = {
            var allVotes: [ReactionVote] = []
            actions.forEach { action in
                guard action.type == MessageActionType.reactionCreated.rawValue else { return }
                let voteID = String(action.id)
                let reactionID = ReactionID(fromString: action.value)
                let reaction = ReactionVote(
                    voteID: voteID,
                    reactionID: reactionID,
                    senderID: action.sender,
                    chatMessageID: chatPubnubMessage.id
                )
                allVotes.append(reaction)
            }
            return ReactionVotes(allVotes: allVotes)
        }()

        self.init(
            id: chatPubnubMessage.id,
            clientMessageID: chatPubnubMessage.clientMessageId,
            roomID: chatRoomID,
            message: chatPubnubMessage.message?.decodedTextMessage ?? "deleted_\(chatPubnubMessage.id)",
            senderID: chatPubnubMessage.senderId ?? "deleted_\(chatPubnubMessage.id)",
            senderNickname: chatPubnubMessage.senderNickname ?? "deleted_\(chatPubnubMessage.id)",
            videoTimestamp: chatPubnubMessage.programDateTime?.timeIntervalSince1970,
            reactions: reactions,
            timestamp: timetoken.approximateDate,
            profileImageUrl: chatPubnubMessage.senderImageUrl,
            createdAt: timetoken,
            bodyImageUrl: chatPubnubMessage.imageUrl,
            bodyImageHeight: chatPubnubMessage.imageHeight,
            bodyImageWidth: chatPubnubMessage.imageWidth,
            filteredMessage: chatPubnubMessage.filteredMessage,
            filteredReasons: chatPubnubMessage.filteredSet,
            customData: chatPubnubMessage.customData,
            quoteMessage: chatPubnubMessage.quoteMessage,
            messageEvent: chatPubnubMessage.messageEvent,
            pubnubTimetoken: timetoken,
            messageMetadata: chatPubnubMessage.messageMetadata, 
            parentMessageID: chatPubnubMessage.parentMessageId,
            repliesCount: chatPubnubMessage.repliesCount,
            repliesURL: chatPubnubMessage.repliesUrl
        )
    }

    init(
        chatMessageResponse: ChatMessageResponse
    ) {
        self.init(
            id: chatMessageResponse.id,
            clientMessageID: chatMessageResponse.clientMessageID,
            roomID: chatMessageResponse.roomID,
            message: chatMessageResponse.message?.decodedTextMessage,
            senderID: chatMessageResponse.senderID,
            senderNickname: chatMessageResponse.senderNickname,
            videoTimestamp: chatMessageResponse.videoTimestamp,
            reactions: chatMessageResponse.reactions,
            timestamp: chatMessageResponse.timestamp,
            profileImageUrl: chatMessageResponse.profileImageURL,
            createdAt: chatMessageResponse.createdAt,
            bodyImageUrl: chatMessageResponse.imageURL,
            bodyImageHeight: chatMessageResponse.imageHeight,
            bodyImageWidth: chatMessageResponse.imageWidth,
            filteredMessage: chatMessageResponse.filteredMessage,
            filteredReasons: chatMessageResponse.filteredReasons,
            customData: chatMessageResponse.customData,
            quoteMessage: chatMessageResponse.quoteMessage,
            messageEvent: chatMessageResponse.messageEvent,
            pubnubTimetoken: chatMessageResponse.pubnubTimetoken,
            messageMetadata: chatMessageResponse.messageMetadata,
            parentMessageID: chatMessageResponse.parentMessageId,
            repliesCount: chatMessageResponse.repliesCount,
            repliesURL: chatMessageResponse.repliesUrl
        )
    }
    
    init(
        from chatMessageEvent: ChatRoomEventPayload,
        chatRoomID: String,
        timetoken: TimeToken,
        userID: String
    ) {
        let reactions: ReactionVotes = {
            let allVotes: [ReactionVote] = []
            return ReactionVotes(allVotes: allVotes)
        }()

        self.init(
            id: chatMessageEvent.id,
            clientMessageID: chatMessageEvent.clientMessageId,
            roomID: chatRoomID,
            message: chatMessageEvent.message?.decodedTextMessage ?? "deleted_\(chatMessageEvent.id)",
            senderID: chatMessageEvent.senderId ?? "deleted_\(chatMessageEvent.id)",
            senderNickname: chatMessageEvent.senderNickname ?? "deleted_\(chatMessageEvent.id)",
            videoTimestamp: chatMessageEvent.programDateTime?.timeIntervalSince1970,
            reactions: reactions,
            timestamp: timetoken.approximateDate,
            profileImageUrl: chatMessageEvent.senderImageUrl,
            createdAt: timetoken,
            bodyImageUrl: chatMessageEvent.imageURL,
            bodyImageHeight: chatMessageEvent.imageHeight,
            bodyImageWidth: chatMessageEvent.imageWidth,
            filteredMessage: chatMessageEvent.filteredMessage,
            filteredReasons: chatMessageEvent.filteredSet,
            customData: nil,
            quoteMessage: chatMessageEvent.quoteMessage,
            messageEvent: chatMessageEvent.messageEvent,
            pubnubTimetoken: timetoken,
            messageMetadata: chatMessageEvent.messageMetadata, 
            parentMessageID: nil,
            repliesCount: nil,
            repliesURL: nil
        )
    }
}

/// The filter type that has been applied to the chat message
public enum ChatFilter: String, Codable {
    /// Catch-all type for any kind of filtering
    case filtered
    
    /// Chat message has been filtered for profanity
    case profanity
    
    /// Chat Messages only allowed for producers
    case producer
    
    /// A chat message is from a user who has been shadow muted
    case shadowMuted = "shadow-muted"
    
    /// No filters set
    case none
}

/// The order type to sort list of pinned chat messages
public enum Ordering: String, Codable {
    
    /// Sort list in ascending order
    case ascending
    
    /// Sort list in descending order
    case descending
}

/// The PinMessageInfo struct represents messages that have been pinned
public struct PinMessageInfo: Decodable {
    public var id: String
    var url: URL
    public var messageID: String
    public var messagePayload: ChatMessagePayload
    public var chatRoomID: String
    public var pinnedByID: String
    public var pinnedBy: ProfileResource

    private enum CodingKeys: String, CodingKey {
        case id
        case url
        case messageId
        case messagePayload
        case chatRoomId
        case pinnedById
        case pinnedBy
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id).lowercased()
        self.url = try container.decode(URL.self, forKey: .url)
        self.messageID = try container.decode(String.self, forKey: .messageId)
        self.messagePayload = try container.decode(ChatMessagePayload.self, forKey: .messagePayload)
        self.chatRoomID = try container.decode(String.self, forKey: .chatRoomId)
        self.pinnedByID = try container.decode(String.self, forKey: .pinnedById)
        self.pinnedBy = try container.decode(ProfileResource.self, forKey: .pinnedBy)
    }

    init(
        id: String,
        url: URL,
        messageID: String,
        messagePayload: ChatMessagePayload,
        chatRoomID: String,
        pinnedByID: String,
        pinnedBy: ProfileResource
    ) {
        self.id = id.lowercased()
        self.url = url
        self.messageID = messageID
        self.messagePayload = messagePayload
        self.chatRoomID = chatRoomID
        self.pinnedByID = pinnedByID
        self.pinnedBy = pinnedBy
    }
}
