//
//  PubSubChatPayload.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

struct PubSubChatPayload: Codable {
    
    var id: String
    var clientMessageId: String
    var message: String?
    
    var senderId: String?
    var senderNickname: String?
    var senderImageUrl: URL?
    var programDateTime: Date?
    

    var filteredSet: Set<ChatFilter> {
        return Set(contentFilter ?? [])
    }
    var messageEvent: PubSubChatEvent?

    var quoteMessageId: String?
    var quoteMessage: ChatMessagePayload?
    var chatRoomId: String?
    var messageMetadata: MessageMetadataCodable?
    
    // MARK: Threading
    let parentMessageId: String?
    let repliesCount: Int?
    let repliesUrl: URL?
    
    // MARK: image message only
    let imageUrl: URL?
    let imageHeight: Int?
    let imageWidth: Int?
    // The message after it has been filtered
    var filteredMessage: String?
    // The reasons why a message was filtered
    var contentFilter: [ChatFilter]?
    
    // MARK: custom message only
    let customData: String?

    private enum CodingKeys: String, CodingKey {
        case id
        case clientMessageId
        case message
        case imageUrl
        case imageHeight
        case imageWidth
        case senderId
        case senderNickname
        case senderImageUrl
        case programDateTime
        case filteredMessage
        case contentFilter
        case messageEvent
        case quoteMessage
        case quoteMessageId
        case chatRoomId
        case messageMetadata
        case parentMessageId
        case repliesCount
        case repliesUrl
        case customData
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id).lowercased()
        self.clientMessageId = try (container.decodeIfPresent(String.self, forKey: .clientMessageId) ?? "").lowercased()
        self.message = try? container.decode(String.self, forKey: .message).decodedTextMessage
        self.senderId = try? container.decode(String.self, forKey: .senderId)
        self.senderNickname = try? container.decode(String.self, forKey: .senderNickname)
        self.senderImageUrl = try? container.decode(URL.self, forKey: .senderImageUrl)
        self.programDateTime = try? container.decode(Date.self, forKey: .programDateTime)
        self.filteredMessage = try? container.decode(String.self, forKey: .filteredMessage)
        self.contentFilter = try? container.decode([ChatFilter].self, forKey: .contentFilter)
        self.messageEvent = try? container.decode(PubSubChatEvent.self, forKey: .messageEvent)
        self.quoteMessage = try container.decodeIfPresent(ChatMessagePayload.self, forKey: .quoteMessage)
        self.quoteMessageId = try container.decodeIfPresent(String.self, forKey: .quoteMessageId)
        self.chatRoomId = try? container.decode(String.self, forKey: .chatRoomId)
        self.messageMetadata = try container.decodeIfPresent(MessageMetadataCodable.self, forKey: .messageMetadata)
        self.parentMessageId = try container.decodeIfPresent(String.self, forKey: .parentMessageId)
        self.repliesUrl = try container.decodeIfPresent(URL.self, forKey: .repliesUrl)
        self.repliesCount = try container.decodeIfPresent(Int.self, forKey: .repliesCount)
        self.customData = try container.decodeIfPresent(String.self, forKey: .customData)
        self.imageUrl = try container.decodeIfPresent(URL.self, forKey: .imageUrl)
        self.imageHeight = try container.decodeIfPresent(Int.self, forKey: .imageHeight)
        self.imageWidth = try container.decodeIfPresent(Int.self, forKey: .imageWidth)
    }

    init(
        id: String?,
        clientMessageId: String,
        message: String?,
        imageUrl: URL?,
        imageHeight: Int?,
        imageWidth: Int?,
        senderId: String?,
        senderNickname: String?,
        senderImageUrl: URL?,
        programDateTime: Date?,
        filteredMessage: String?,
        contentFilter: [ChatFilter]?,
        messageEvent: PubSubChatEvent?,
        quoteMessage: ChatMessagePayload?,
        chatRoomId: String?,
        messageMetadata: MessageMetadataCodable?,
        parentMessageId: String?,
        repliesCount: Int?,
        repliesUrl: URL?,
        customData: String?
    ) {
        self.id = (id ?? "").lowercased()
        self.clientMessageId = clientMessageId.lowercased()
        self.message = message
        self.senderId = senderId
        self.senderNickname = senderNickname
        self.senderImageUrl = senderImageUrl
        self.programDateTime = programDateTime
        self.filteredMessage = filteredMessage
        self.contentFilter = contentFilter
        self.messageEvent = messageEvent
        self.quoteMessage = quoteMessage
        self.quoteMessageId = quoteMessage?.id
        self.chatRoomId = chatRoomId
        self.messageMetadata = messageMetadata
        
        self.parentMessageId = parentMessageId
        self.repliesUrl = repliesUrl
        self.repliesCount = repliesCount
        
        self.customData = customData
        
        self.imageUrl = imageUrl
        self.imageHeight = imageHeight
        self.imageWidth = imageWidth
    }
}
