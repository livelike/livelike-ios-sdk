//
//  LiveLikeChatAPI.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 5/20/21.
//

import Foundation
import LiveLikeCore

protocol LiveLikeChatAPIProtocol {
    /// Retrieves a `ChatRoomResource` by chat room id
    func getChatRoomResource(roomID: String, accessToken: AccessToken) -> Promise<ChatRoomResource>

    /// Retrieves details if the user is allowed to access a token-gated chat room.
    func checkTokenGatedChatAccess(roomID: String, walletAddress: String, accessToken: AccessToken) -> Promise<TokenGatedChat>

    /// Creates a `ChatRoomResource`
    func createChatRoomResource(
        title: String?,
        visibility: ChatRoomVisibilty,
        accessToken: AccessToken,
        appConfig: ApplicationConfiguration,
        tokenGates: [TokenGate]?
    ) -> Promise<ChatRoomResource>

    /// Retrieve all Chat Rooms the current user is a member of
    func getUserChatRoomMemberships(
        url: URL,
        accessToken: AccessToken,
        page: ChatRoomMembershipPagination
    ) -> Promise<UserChatRoomMembershipsResult>

    /// Create a membership between the current user and a Chat Room
    func createChatRoomMembership(
        roomID: String,
        accessToken: AccessToken
    ) -> Promise<ChatRoomMember>

    /// Create a membership between a user(using their userId) and a Chat Room
    func addNewMemberToChatRoom(
        membershipUrl: URL,
        profileID: String,
        accessToken: AccessToken
    ) -> Promise<ChatRoomMember>

    /// Send an invite to a chatroom to another User
    func sendChatRoomInviteToUser(
        chatRoomInviteUrl: URL,
        chatRoomID: String,
        profileID: String,
        accessToken: AccessToken
    ) -> Promise<ChatRoomInvitation>

    /// Update the status of an invitation to a particular chatroom for a user (Accepted, Pending, Rejected)
    func updateChatRoomInvitationStatus(
        chatRoomInvitationUrl: URL,
        invitationStatus: ChatRoomInvitationStatus,
        accessToken: AccessToken
    ) -> Promise<ChatRoomInvitation>

    /// Get List of Invitations sent to the user filtered by status of invitation
    func getInvitationsForUserWithInvitationStatus(
        invitationStatus: ChatRoomInvitationStatus,
        chatRoomInvitationsUrlTemplate: String,
        profileID: String,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<ChatRoomInvitation>>

    /// Get List of Invitations sent by the user filtered by status of invitation
    func getInvitationsByUserWithInvitationStatus(
        invitationStatus: ChatRoomInvitationStatus,
        chatRoomInvitationsUrlTemplate: String,
        profileID: String,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<ChatRoomInvitation>>

    func deleteChatRoomMembership(
        roomID: String,
        accessToken: AccessToken
    ) -> Promise<Bool>

    func getChatUserMutedStatus(profileID: String, roomID: String, accessToken: AccessToken) -> Promise<ChatUserMuteStatusResource>

    @discardableResult
    func sendCustomMessage(chatRoomID: String, customMessagesUrl: URL, accessToken: AccessToken, customData: String) -> Promise<ChatMessage>

    /// Send Messages in a chatroom
    @discardableResult
    func sendMessage<T: Encodable>(chatroomMessagesURL: URL, accessToken: AccessToken, messagePayload: T) -> Promise<ChatMessage>

    func deleteMessage(deleteMessagesURL: URL, messageID: String, timetoken: UInt64?, accessToken: AccessToken) -> Promise<Bool>

    /// Get Messages (Message History) of a chatroom
    func getMessages(chatroomMessagesURL: URL, accessToken: AccessToken) -> Promise<GetMessagesResult>

    /// Get Message Count for messages in a chatroom
    func getMessagesCount(chatroomMessagesCountURL: URL, accessToken: AccessToken) -> Promise<GetMessagesCountResult>

    /// Poll for messages using polling interval
    func getMessageEvents(chatroomEventsURL: URL) -> Promise<PaginatedResult<ChatRoomEvent>>

    /// Pin messages in a chatroom (Can only be done by moderators or ChatRoom creators)
    func pinMessage(message: ChatMessagePayload, chatRoomID: String, pinnedMessagesURL: URL, accessToken: AccessToken) -> Promise<PinMessageInfo>

    /// Unpin messages from a chatroom
    func unpinMessage(pinnedMessagesURL: URL, accessToken: AccessToken) -> Promise<Bool>

    /// Get a list of messages that have been pinned in a chatroom
    func getPinMessageInfoList(
        pinMessageInfoListURL: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<PinMessageInfo>>

    /// Block Invitations for chatrooms from a particular user
    func blockProfile(
        blockProfileURL: URL,
        profileID: String,
        accessToken: AccessToken
    ) -> Promise<BlockInfo>

    /// Unblock receiving invitations to chatrooms from a particular user
    func unblockProfile(
        unblockProfileURL: URL,
        accessToken: AccessToken
    ) -> Promise<Bool>

    func getBlockedProfileList(
        blockProfileURL: URL,
        blockedProfileID: String?,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<BlockInfo>>

    func getBlockProfileInfo(
        blockProfileInfoURL: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<BlockInfo>>

    func getBlockedProfileIDList(
        blockedIDListURL: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<String>>


}

class LiveLikeChatAPI: LiveLikeChatAPIProtocol {

    private let coreAPI: LiveLikeCoreAPIProtocol

    init(coreAPI: LiveLikeCoreAPIProtocol) {
        self.coreAPI = coreAPI
    }

    func getChatUserMutedStatus(profileID: String, roomID: String, accessToken: AccessToken) -> Promise<ChatUserMuteStatusResource> {
        return firstly {
            self.getChatRoomResource(roomID: roomID, accessToken: accessToken)
        }.then { chatRoomResource -> Promise<ChatUserMuteStatusResource> in

            let stringToReplace = "{profile_id}"
            guard chatRoomResource.mutedStatusUrlTemplate.contains(stringToReplace) else {
                return Promise(error: ContentSessionError.invalidUserMutedStatusURLTemplate)
            }
            let urlTemplateFilled = chatRoomResource.mutedStatusUrlTemplate.replacingOccurrences(of: stringToReplace, with: profileID)
            guard let chatUserMutedStatusURL = URL(string: urlTemplateFilled) else {
                return Promise(error: ContentSessionError.invalidUserMutedStatusURL)
            }
            let resource = Resource<ChatUserMuteStatusResource>(get: chatUserMutedStatusURL)
            return LiveLike.networking.load(resource)
        }
    }

    func getChatRoomResource(roomID: String, accessToken: AccessToken) -> Promise<ChatRoomResource> {
        return firstly {
            coreAPI.whenApplicationConfig
        }.then { (appConfig: ApplicationConfiguration) in
            let stringToReplace = "{chat_room_id}"
            guard appConfig.chatRoomDetailUrlTemplate.contains(stringToReplace) else {
                return Promise(error: ContentSessionError.invalidChatRoomURLTemplate)
            }
            let urlTemplateFilled = appConfig.chatRoomDetailUrlTemplate.replacingOccurrences(of: stringToReplace, with: roomID)
            guard let chatRoomURL = URL(string: urlTemplateFilled) else {
                return Promise(error: ContentSessionError.invalidChatRoomURL)
            }
            let resource = Resource<ChatRoomResource>(get: chatRoomURL, accessToken: accessToken.asString)
            return LiveLike.networking.load(resource)
        }
    }

    func checkTokenGatedChatAccess(roomID: String, walletAddress: String, accessToken: AccessToken) -> Promise<TokenGatedChat> {
        return firstly {
            self.getChatRoomResource(roomID: roomID, accessToken: accessToken)
        }.then { chatRoomResource -> Promise<TokenGatedChat> in
            let stringToReplace = "{wallet_address}"
            guard chatRoomResource.chatroomTokenGateAccessUrlTemplate.contains(stringToReplace) else {
                return Promise(error: ContentSessionError.invalidChatroomTokenGateAccessURLTemplate)
            }
            let urlTemplateFilled = chatRoomResource.chatroomTokenGateAccessUrlTemplate.replacingOccurrences(of: stringToReplace, with: walletAddress)
            guard let checkTokenGateAccessURL = URL(string: urlTemplateFilled) else {
                return Promise(error: ContentSessionError.invalidChatroomTokenGateAccessURL)
            }
            let resource = Resource<TokenGatedChat>(get: checkTokenGateAccessURL)
            return LiveLike.networking.load(resource)
        }
    }

    func createChatRoomResource(
        title: String?,
        visibility: ChatRoomVisibilty,
        accessToken: AccessToken,
        appConfig: ApplicationConfiguration,
        tokenGates: [TokenGate]?
    ) -> Promise<ChatRoomResource> {
        guard let createChatRoomURL = URL(string: appConfig.createChatRoomUrl) else {
            return Promise(error: CreateChatRoomResourceError.failedCreatingChatRoomUrl)
        }
        let createChatRoomBody = CreateChatRoomBody(
            title: title,
            visibility: visibility.rawValue,
            tokenGates: tokenGates
        )
        let resource = Resource<ChatRoomResource>.init(
            url: createChatRoomURL,
            method: .post(createChatRoomBody),
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func getUserChatRoomMemberships(
        url: URL,
        accessToken: AccessToken,
        page: ChatRoomMembershipPagination
    ) -> Promise<UserChatRoomMembershipsResult> {
        return firstly { () -> Promise<UserChatRoomMembershipPage> in
            let resource = Resource<UserChatRoomMembershipPage>(
                get: url,
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { userChatMemberships in
            let chatRooms: [ChatRoomInfo] = userChatMemberships.results.map {
                ChatRoomInfo(chatroomResource: $0.chatRoom)
            }
            return Promise(value: UserChatRoomMembershipsResult(
                chatRooms: chatRooms,
                next: userChatMemberships.next,
                previous: userChatMemberships.previous
            ))
        }
    }

    func createChatRoomMembership(roomID: String, accessToken: AccessToken) -> Promise<ChatRoomMember> {
        return firstly {
            self.getChatRoomResource(roomID: roomID, accessToken: accessToken)
        }.then { chatRoomResource -> Promise<ChatRoomMember> in
            let resource = Resource<ChatRoomMember>(
                url: chatRoomResource.membershipsUrl,
                method: .post(EmptyBody()),
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { chatRoomMember in
            return Promise(value: chatRoomMember)
        }
    }

    func addNewMemberToChatRoom(membershipUrl: URL, profileID: String, accessToken: AccessToken) -> Promise<ChatRoomMember> {
        return firstly {
            struct Payload: Encodable {
                let profileId: String
            }
            let payload = Payload(profileId: profileID)

            let resource = Resource<ChatRoomMember>(
                url: membershipUrl,
                method: .post(payload),
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { chatRoomMember in
            return Promise(value: chatRoomMember)
        }
    }

    func sendChatRoomInviteToUser(chatRoomInviteUrl: URL, chatRoomID: String, profileID: String, accessToken: AccessToken) -> Promise<ChatRoomInvitation> {
        return firstly {
            struct Payload: Encodable {
                let invitedProfileId: String
                let chatRoomId: String
            }
            let payload = Payload(invitedProfileId: profileID, chatRoomId: chatRoomID)

            let resource = Resource<ChatRoomInvitation>(
                url: chatRoomInviteUrl,
                method: .post(payload),
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { chatRoomInvitation in
            return Promise(value: chatRoomInvitation)
        }
    }

    func updateChatRoomInvitationStatus(
        chatRoomInvitationUrl: URL,
        invitationStatus: ChatRoomInvitationStatus,
        accessToken: AccessToken
    ) -> Promise<ChatRoomInvitation> {
        return firstly {
            struct Payload: Encodable {
                let status: String
            }
            let payload = Payload(status: invitationStatus.rawValue)

            let resource = Resource<ChatRoomInvitation>(
                url: chatRoomInvitationUrl,
                method: .patch(payload),
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { chatRoomInvitation in
            return Promise(value: chatRoomInvitation)
        }
    }

    func getInvitationsForUserWithInvitationStatus(
        invitationStatus: ChatRoomInvitationStatus,
        chatRoomInvitationsUrlTemplate: String,
        profileID: String,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<ChatRoomInvitation>> {

        let urlTemplate = chatRoomInvitationsUrlTemplate.replacingOccurrences(
            of: "{profile_id}",
            with: profileID
        )

        let urlTemplateFilled = urlTemplate.replacingOccurrences(
            of: "{status",
            with: invitationStatus.rawValue
        )

        guard let profileChatRoomInvitationsUrl = URL(string: urlTemplateFilled) else {
            return Promise(error: ContentSessionError.invalidChatRoomInvitationURLTemplate)
        }

        return firstly { () -> Promise<PaginatedResult<ChatRoomInvitation>> in
            let resource = Resource<PaginatedResult<ChatRoomInvitation>>(
                get: profileChatRoomInvitationsUrl,
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { chatRoomInvitations in
            return Promise(value: chatRoomInvitations)
        }
    }

    func getInvitationsByUserWithInvitationStatus(
        invitationStatus: ChatRoomInvitationStatus,
        chatRoomInvitationsUrlTemplate: String,
        profileID: String,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<ChatRoomInvitation>> {

        let urlTemplate = chatRoomInvitationsUrlTemplate.replacingOccurrences(
            of: "{invited_by_id}",
            with: profileID
        )

        let urlTemplateFilled = urlTemplate.replacingOccurrences(
            of: "{status}",
            with: invitationStatus.rawValue
        )

        guard let profileChatRoomInvitationsUrl = URL(string: urlTemplateFilled) else {
            return Promise(error: ContentSessionError.invalidChatRoomInvitationURLTemplate)
        }

        return firstly { () -> Promise<PaginatedResult<ChatRoomInvitation>> in
            let resource = Resource<PaginatedResult<ChatRoomInvitation>>(
                get: profileChatRoomInvitationsUrl,
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { chatRoomInvitations in
            return Promise(value: chatRoomInvitations)
        }
    }

    func deleteChatRoomMembership(roomID: String, accessToken: AccessToken) -> Promise<Bool> {
        return firstly {
            self.getChatRoomResource(roomID: roomID, accessToken: accessToken)
        }.then { chatRoomResource -> Promise<Bool> in
            let resource = Resource<Bool>(
                url: chatRoomResource.membershipsUrl,
                method: .delete(EmptyBody()),
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { chatRoomMember in
            return Promise(value: chatRoomMember)
        }
    }

    func sendCustomMessage(
        chatRoomID: String,
        customMessagesUrl: URL,
        accessToken: AccessToken,
        customData: String
    ) -> Promise<ChatMessage> {
        struct Payload: Encodable {
            let customData: String
        }
        let resource = Resource<ChatMessage>(
            url: customMessagesUrl,
            method: .post(Payload(customData: customData)),
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func sendMessage<T: Encodable>(
        chatroomMessagesURL: URL,
        accessToken: AccessToken,
        messagePayload: T
    ) -> Promise<ChatMessage> {

        let resource = Resource<ChatMessage>(
            url: chatroomMessagesURL,
            method: .post(messagePayload),
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func getMessages(
        chatroomMessagesURL: URL,
        accessToken: AccessToken
    ) -> Promise<GetMessagesResult> {
        return firstly { () -> Promise<GetMessagesResult> in
            let resource = Resource<GetMessagesResult>(
                get: chatroomMessagesURL,
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { messagesList in
            return Promise(value: messagesList)
        }.catch { error in
            log.error(error)
        }
    }

    func getMessagesCount(
        chatroomMessagesCountURL: URL,
        accessToken: AccessToken
    ) -> Promise<GetMessagesCountResult> {
        return firstly { () -> Promise<GetMessagesCountResult> in
            let resource = Resource<GetMessagesCountResult>(
                get: chatroomMessagesCountURL,
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { count in
            return Promise(value: count)
        }.catch { error in
            log.error(error)
        }
    }

    func getMessageEvents(chatroomEventsURL: URL) -> Promise<PaginatedResult<ChatRoomEvent>> {
        return firstly { () -> Promise<PaginatedResult<ChatRoomEvent>> in
            let resource = Resource<PaginatedResult<ChatRoomEvent>>(
                get: chatroomEventsURL
            )
            return LiveLike.networking.load(resource)
        }.then { messagesList in
            return Promise(value: messagesList)
        }.catch { error in
            log.error(error)
        }
    }

    func deleteMessage(deleteMessagesURL: URL, messageID: String, timetoken: UInt64?, accessToken: AccessToken) -> Promise<Bool> {
        struct Payload: Encodable {
            let messageId: String
            let pubnubTimetoken: UInt64?
        }
        let request = URLRequest(
            url: deleteMessagesURL,
            method: .post(
                Payload(
                    messageId: messageID,
                    pubnubTimetoken: timetoken
                )
            ),
            accessToken: accessToken.asString
        )
        return Promise { fulfill, reject in
            LiveLike.networking.task(
                request: request
            ) { result in
                switch result {
                case .failure(let error):
                    reject(error)
                case .success:
                    fulfill(true)
                }
            }
        }
    }

    func pinMessage(message: ChatMessagePayload, chatRoomID: String, pinnedMessagesURL: URL, accessToken: AccessToken) -> Promise<PinMessageInfo> {
        struct Payload: Encodable {
            let messagePayload: ChatMessagePayload
            let chatRoomId: String
            let messageId: String
        }

        let resource = Resource<PinMessageInfo>(
            url: pinnedMessagesURL,
            method: .post(
                Payload(
                    messagePayload: message,
                    chatRoomId: chatRoomID,
                    messageId: message.id
                )
            ),
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func unpinMessage(pinnedMessagesURL: URL, accessToken: AccessToken) -> Promise<Bool> {
        return firstly {
            let resource = Resource<Bool>(
                url: pinnedMessagesURL,
                method: .delete(EmptyBody()),
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { result in
            return Promise(value: result)
        }
    }

    func getPinMessageInfoList(
        pinMessageInfoListURL: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<PinMessageInfo>> {

        return firstly { () -> Promise<PaginatedResult<PinMessageInfo>> in
            let resource = Resource<PaginatedResult<PinMessageInfo>>(
                get: pinMessageInfoListURL,
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { pinMessageInfoList in
            return Promise(value: pinMessageInfoList)
        }
    }

    func blockProfile(
        blockProfileURL: URL,
        profileID: String,
        accessToken: AccessToken
    ) -> Promise<BlockInfo> {
        return firstly {
            struct Payload: Encodable {
                let blockedProfileId: String
            }
            let payload = Payload(
                blockedProfileId: profileID
            )

            let resource = Resource<BlockInfo>(
                url: blockProfileURL,
                method: .post(payload),
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { blockInfo in
            return Promise(value: blockInfo)
        }
    }

    func unblockProfile(
        unblockProfileURL: URL,
        accessToken: AccessToken
    ) -> Promise<Bool> {
        return firstly {
            let resource = Resource<Bool>(
                url: unblockProfileURL,
                method: .delete(EmptyBody()),
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { result in
            return Promise(value: result)
        }
    }

    func getBlockedProfileList(
        blockProfileURL: URL,
        blockedProfileID: String?,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<BlockInfo>> {

        return firstly { () -> Promise<PaginatedResult<BlockInfo>> in
            let resource = Resource<PaginatedResult<BlockInfo>>(
                get: blockProfileURL,
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { blockInfoList in
            return Promise(value: blockInfoList)
        }
    }

    func getBlockProfileInfo(
        blockProfileInfoURL: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<BlockInfo>> {
        return firstly { () -> Promise<PaginatedResult<BlockInfo>> in
            let resource = Resource<PaginatedResult<BlockInfo>>(
                get: blockProfileInfoURL,
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { blockInfoList in
            return Promise(value: blockInfoList)
        }
    }

    func getBlockedProfileIDList(blockedIDListURL: URL, accessToken: AccessToken) -> Promise<PaginatedResult<String>> {
        return firstly { () -> Promise<PaginatedResult<String>> in
            let resource = Resource<PaginatedResult<String>>(
                get: blockedIDListURL,
                accessToken: accessToken.asString
            )
            return LiveLike.networking.load(resource)
        }.then { blockInfoList in
            return Promise(value: blockInfoList)
        }
    }
}
