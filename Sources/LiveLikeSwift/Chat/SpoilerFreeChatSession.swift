//
//  SpoilerFreeChatSession.swift
//  LiveLikeSDK
//
//  Created by Jelzon Monzon on 2020-03-14.
//

import Foundation
import LiveLikeCore

class SpoilerFreeChatSession: ChatSession {
    
    private var realChatRoom: ChatSession
    
    /// Messages where videoTimestamp `> playerTimeSource. Sorted in ascending order `[n] <= [n+1]`
    @Atomic private(set) var sortedUnsynchronizedMessages: [ChatMessage] = []
    private var playerTimeSource: PlayerTimeSource
    private var timer: DispatchSourceTimer?
    
    private let publicDelegates: Listener<ChatSessionDelegate> = Listener()
    
    var roomID: String
    var title: String?
    var userID: String
    /// Messages where videoTimestamp `<=` playerTimeSource 
    @Atomic private(set) var messages: [ChatMessage] = []
    var isReportingEnabled: Bool
    var isAvatarDisplayed: Bool
    var isURLEnabled: Bool
    var chatMessageUrlPatterns: String?
    var avatarURL: URL?
    var containsFilteredMessages: Bool
    var chatClient: ChatClient
    var userClient: UserClient

    init(realChatRoom: ChatSession, startTimer: Bool, playerTimeSource: @escaping PlayerTimeSource) {
        self.realChatRoom = realChatRoom
        self.playerTimeSource = playerTimeSource
        self.title = realChatRoom.title
        self.roomID = realChatRoom.roomID
        self.userID = realChatRoom.userID
        self.isAvatarDisplayed = realChatRoom.isAvatarDisplayed
        self.isURLEnabled = realChatRoom.isURLEnabled
        self.chatMessageUrlPatterns = realChatRoom.chatMessageUrlPatterns
        self.avatarURL = realChatRoom.avatarURL
        self.isReportingEnabled = realChatRoom.isReportingEnabled
        self.containsFilteredMessages = realChatRoom.containsFilteredMessages
        self.chatClient = realChatRoom.chatClient
        self.userClient = realChatRoom.userClient
        
        self.realChatRoom.addDelegate(self)
        if startTimer {
            timer = processQueueForEligibleScheduledEvent()
        }
    }

    deinit {
        self.timer?.cancel()
    }
    
    func getReactions(
        completion: @escaping (Result<[Reaction], Error>) -> Void
    ) {
        self.realChatRoom.getReactions(
            completion: completion
        )
    }

    func getStickerPacks(
        completion: @escaping (Result<[StickerPack], Error>) -> Void
    ) {
        self.realChatRoom.getStickerPacks(completion: completion)
    }
    
    func addDelegate(_ delegate: ChatSessionDelegate) {
        publicDelegates.addListener(delegate)
    }
    
    func removeDelegate(_ delegate: ChatSessionDelegate) {
        publicDelegates.removeListener(delegate)
    }
    
    func disconnect() {
        realChatRoom.disconnect()
    }

    func reportMessage(
        messageID: String,
        completion: @escaping (Result<Void, Error>) -> Void
    ) {
        realChatRoom.reportMessage(
            messageID: messageID,
            completion: completion
        )
    }
    
    func addMessageReaction(
        messageID: String,
        reaction: String,
        completion: @escaping (Result<Void, Error>) -> Void
    ) {
        self.realChatRoom.addMessageReaction(
            messageID: messageID,
            reactionID: reaction
        ) { (result: Result<ReactionVote, Error>) in
            completion(result.map { _ in return () })
        }
    }
    
    func addMessageReaction(
        messageID: String,
        reactionID: String,
        completion: @escaping (Result<ReactionVote, Error>) -> Void
    ) {
        self.realChatRoom.addMessageReaction(
            messageID: messageID,
            reactionID: reactionID,
            completion: completion
        )
    }
    
    func removeMessageReaction(reactionVoteID: String, messageID: String, completion: @escaping (Result<Void, Error>) -> Void) {
        self.realChatRoom.removeMessageReaction(reactionVoteID: reactionVoteID, messageID: messageID, completion: completion)
    }
    
    func loadNextHistory(completion: @escaping (Result<[ChatMessage], Error>) -> Void) {
        realChatRoom.loadNextHistory { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                log.error(error)
                completion(.failure(error))
            case .success(let messagesFromHistory):
                // If there is no sync time source then publish immediately
                guard let playerTimeSourceNow = self.playerTimeSource() else {
                    self.messages.insert(contentsOf: messagesFromHistory, at: 0)
                    completion(.success(messagesFromHistory))
                    return
                }
              
                let splitMessages = self.split(messages: self.realChatRoom.messages, byTimestamp: playerTimeSourceNow)
                self.messages.insert(contentsOf: splitMessages.messagesBeforeTimestamp, at: 0)
                self.sortedUnsynchronizedMessages.append(contentsOf: splitMessages.messagesAfterTimestamp)
                self.sortedUnsynchronizedMessages.sort(by: self.sortCondition)
                
                // Publish messages that were earlier than sync timestamp
                completion(.success(splitMessages.messagesBeforeTimestamp))
            }
        }
    }

    func loadInitialHistory(completion: @escaping (Result<Void, Error>) -> Void) {
        self.realChatRoom.loadInitialHistory { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success:
                // If there is no sync time source then publish immediately
                guard let playerTimeSourceNow = self.playerTimeSource() else {
                    self.messages.insert(contentsOf: self.realChatRoom.messages, at: 0)
                    completion(.success(()))
                    return
                }
               
                let splitMessages = self.split(messages: self.realChatRoom.messages, byTimestamp: playerTimeSourceNow)
                self.messages.insert(contentsOf: splitMessages.messagesBeforeTimestamp, at: 0)
                self.sortedUnsynchronizedMessages.append(contentsOf: splitMessages.messagesAfterTimestamp)
                self.sortedUnsynchronizedMessages.sort(by: self.sortCondition)
            
                completion(.success(()))
            }
        }
    }

    func pause() {}

    func resume() {}
    
    func getMessageCount(since timestamp: TimeToken, completion: @escaping (Result<Int, Error>) -> Void) {
        realChatRoom.getMessageCount(since: timestamp, completion: completion)
    }
    
    func getMessages(since timestamp: TimeToken, completion: @escaping (Result<[ChatMessage], Error>) -> Void) {
        realChatRoom.getMessages(since: timestamp) { result in
            switch result {
            case .success(let messages):
                if messages.last?.videoTimestamp != nil {
                    let spoilerMessages = messages.filter {
                        if let timestamp = $0.videoTimestamp,
                           let timesource = self.playerTimeSource()
                        {
                            return timestamp <= timesource
                        } else {
                            return false
                        }
                    }
                    completion(.success(spoilerMessages))
                } else {
                    completion(.success(messages))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func updateUserChatRoomImage(url: URL, completion: @escaping (Result<Void, Error>) -> Void) {
        realChatRoom.avatarURL = url
        completion(.success(()))
    }

    func sendMessage(_ chatMessage: NewChatMessage, completion: @escaping (Result<ChatMessage, Error>) -> Void) -> ChatMessage {
        var chatMessage = chatMessage
        chatMessage.timeStamp = self.playerTimeSource()
        return realChatRoom.sendMessage(chatMessage, completion: completion)
    }
    
    func deleteMessage(message: ChatMessage, completion: @escaping (Result<Bool, Error>) -> Void) {
        return realChatRoom.deleteMessage(message: message, completion: completion)
    }
    
    func deleteMessage(
        options: DeleteMessageRequestOptions,
        completion: @escaping (Result<Bool, any Error>) -> Void
    ) {
        return realChatRoom.deleteMessage(options: options, completion: completion)
    }
    
    func sendCustomMessage(_ customData: String, completion: @escaping (Result<ChatMessage, Error>) -> Void) -> ChatMessage {
        return realChatRoom.sendCustomMessage(customData, completion: completion)
    }
    
    func pinMessage(_ message: ChatMessage, completion: @escaping (Result<PinMessageInfo, Error>) -> Void) {
        realChatRoom.pinMessage(message, completion: completion)
    }
    
    func unpinMessage(pinMessageInfoID: String, completion: @escaping (Result<Bool, Error>) -> Void) {
        realChatRoom.unpinMessage(pinMessageInfoID: pinMessageInfoID, completion: completion)
    }
    
    func quoteMessage(_ chatMessage: NewChatMessage, quoteMessage: ChatMessagePayload, completion: @escaping (Result<ChatMessage, Error>) -> Void) -> ChatMessage {
        var chatMessage = chatMessage
        chatMessage.timeStamp = self.playerTimeSource()
        return realChatRoom.quoteMessage(chatMessage, quoteMessage: quoteMessage, completion: completion)
    }
    
    func getPinMessageInfoList(orderBy: Ordering, page: Pagination, completion: @escaping (Result<[PinMessageInfo], Error>) -> Void) {
        realChatRoom.getPinMessageInfoList(orderBy: orderBy, page: page, completion: completion)
    }
    
    private func sortCondition(lhs: ChatMessage, rhs: ChatMessage) -> Bool {
        // If lhs or rhs doesn't have a timestamp
        // Then return true to sort them to the front
        guard
            let lhsVideoTimestamp = lhs.videoTimestamp,
            let rhsVideoTimestamp = rhs.videoTimestamp
        else {
            return true
        }
        
        return lhsVideoTimestamp <= rhsVideoTimestamp
    }
    
    func synchronizeNextMessage() {
        guard let nextMessage = self.sortedUnsynchronizedMessages.first else { return }
        // Send message when timeSource has passed timeStamp
        if
            let timeStamp = nextMessage.videoTimestamp,
            let timeSource = self.playerTimeSource(),
            timeStamp <= timeSource
        {
            self.messages.append(nextMessage)
            self.publicDelegates.publish {
                $0.chatSession(self, didRecieveNewMessage: nextMessage)
            }
            self.sortedUnsynchronizedMessages.remove(at: 0)
            return
        }
    }
}

extension SpoilerFreeChatSession: ChatSessionDelegate {
    func chatSession(_ chatSession: ChatSession, didRecieveRoomUpdate chatRoom: ChatRoomInfo) {
        self.publicDelegates.publish { $0.chatSession(self, didRecieveRoomUpdate: chatRoom) }
    }
    
    func chatSession(_ chatSession: ChatSession, didRecieveNewMessage newMessage: ChatMessage) {
        if newMessage.videoTimestamp != nil {
            self.sortedUnsynchronizedMessages.append(newMessage)
            self.sortedUnsynchronizedMessages.sort(by: self.sortCondition)
        } else {
            // Messages without PDT are sent to the view right away
            self.messages.append(newMessage)
            self.publicDelegates.publish {
                $0.chatSession(self, didRecieveNewMessage: newMessage)
            }
        }
    }
    
    func chatSession(_ chatSession: ChatSession, didRecieveMessageUpdate message: ChatMessage) {
        self.publicDelegates.publish {
            $0.chatSession(self, didRecieveMessageUpdate: message)
        }
    }
    
    func chatSession(_ chatSession: ChatSession, didDeleteMessage messageID: ChatMessageID) {
        self.publicDelegates.publish {
            $0.chatSession(self, didDeleteMessage: messageID)
        }
    }
    
    func chatSession(_ chatSession: ChatSession, didPinMessage message: PinMessageInfo) {
        self.publicDelegates.publish {
            $0.chatSession(self, didPinMessage: message)
        }
    }
    
    func chatSession(_ chatSession: ChatSession, didUnpinMessage pinMessageInfoID: String) {
        self.publicDelegates.publish {
            $0.chatSession(self, didUnpinMessage: pinMessageInfoID)
        }
    }
}

private extension SpoilerFreeChatSession {
    // Helper method to split an array of messages into two arrays by timestamp
    // messageBeforeTimestamp are the messages with timestamps before the given timestamp
    // messagesAfterTimestamp are the messages with timestamps after the given timestamp
    func split(
        messages: [ChatMessage],
        byTimestamp timestamp: TimeInterval
    ) -> (messagesBeforeTimestamp: [ChatMessage], messagesAfterTimestamp: [ChatMessage]) {
        
        // The messages that are earlier than the sync timestamp
        // This includes messages that do not have PDT and should be placed as first in the array
        // These will be shown immediately.
        var beforeTimestamp = messages.filter { $0.videoTimestamp == nil }.sorted { $0.createdAt < $1.createdAt }
      
        // The messages that are later than the sync timestamp. These will be queued for sync.
        var afterTimestamp = [ChatMessage]()
        
        let sortedPDTMessages = messages.filter { $0.videoTimestamp != nil }.sorted(by: self.sortCondition)
        
        if let firstFutureTimestampIndex = sortedPDTMessages.firstIndex(where: { $0.videoTimestamp! >= timestamp }) {
            beforeTimestamp.append(contentsOf: sortedPDTMessages.prefix(upTo: firstFutureTimestampIndex))
            afterTimestamp.append(contentsOf: sortedPDTMessages.suffix(from: firstFutureTimestampIndex))
        } else {
            beforeTimestamp.append(contentsOf: sortedPDTMessages)
        }

        return (beforeTimestamp, afterTimestamp)
    }

    func processQueueForEligibleScheduledEvent() -> DispatchSourceTimer {
        self.timer?.cancel()
        let timer = DispatchSource.makeTimerSource()
        timer.schedule(deadline: .now(), repeating: .milliseconds(200))
        timer.setEventHandler { [weak self] in
            guard let self = self else { return }
            self.synchronizeNextMessage()
        }
        timer.resume()
        return timer
    }
}
