//
//  MessageMetadata.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 10/11/22.
//

import Foundation

public typealias MessageMetadata = [String: Any]
extension MessageMetadata {
    func asMessageMetadataCodable() -> MessageMetadataCodable {
        return self.compactMapValues {
            guard $0 is Codable else { return nil }
            return AnyCodable($0)
        }
    }
}

typealias MessageMetadataCodable = [String: AnyCodable]
