//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

public struct DeleteMessageRequestOptions {

    let messageID: String
    let pubnubTimetoken: UInt64?

    public init(
        messageID: String,
        pubnubTimetoken: UInt64?
    ) {
        self.messageID = messageID
        self.pubnubTimetoken = pubnubTimetoken
    }
}
