//
//  ChatRoomEvent.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

struct ChatRoomEvent: Decodable {
    let id: String
    let category: EventCategory
    let payload: ChatRoomEventPayload

    init(id: String, category: EventCategory, payload: ChatRoomEventPayload) {
        self.id = id
        self.category = category
        self.payload = payload
    }
}
