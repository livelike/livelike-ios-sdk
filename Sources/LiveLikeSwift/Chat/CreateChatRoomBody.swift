//
//  CreateChatRoomBody.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

struct CreateChatRoomBody: Encodable {
    let title: String?
    let visibility: String
    let tokenGates: [TokenGate]?
}
