//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//
	

import Foundation

public struct ChatMessagePayload: Codable {
    /// The unique identifer of the message payload
    public var id: String
    
    /// The text component of the message
    public var message: String?
    
    /// The unique id of the message's sender
    public let senderID: String?
    
    /// The nickname of the message's sender
    public let senderNickname: String?
    
    /// The URL of the avatar image of the sender of the message
    public var senderImageURL: URL?
    
    /// The timestamp of the message
    public var programDateTime: Date?
    
    /// The message after it has been filtered.
    public var filteredMessage: String?
    
    /// The content filters applied to the message that has been sent
    public var contentFilter: [ChatFilter]?
    
    public var imageURL: URL?
    public var imageHeight: Int?
    public var imageWidth: Int?
    public var customData: String?
    
    public var parentMessageID: String?
    public var repliesCount: Int?
    public var repliesUrl: URL?

    private enum CodingKeys: String, CodingKey {
        case id
        case message
        case senderId
        case senderNickname
        case senderImageUrl
        case programDateTime
        case filteredMessage
        case contentFilter
        case imageUrl
        case imageHeight
        case imageWidth
        case customData
        case repliesUrl
        case repliesCount
        case parentMessageId
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id).lowercased()
        self.message = try container.decodeIfPresent(String.self, forKey: .message)?.decodedTextMessage
        self.senderID = try container.decodeIfPresent(String.self, forKey: .senderId)
        self.senderNickname = try container.decodeIfPresent(String.self, forKey: .senderNickname)
        self.senderImageURL = try container.decodeIfPresent(URL.self, forKey: .senderImageUrl)
        self.programDateTime = try container.decodeIfPresent(Date.self, forKey: .programDateTime)
        self.filteredMessage = try container.decodeIfPresent(String.self, forKey: .filteredMessage)
        self.contentFilter = try? container.decode([ChatFilter].self, forKey: .contentFilter)
        self.imageURL = try container.decodeIfPresent(URL.self, forKey: .imageUrl)
        self.imageHeight = try container.decodeIfPresent(Int.self, forKey: .imageHeight)
        self.imageWidth = try container.decodeIfPresent(Int.self, forKey: .imageWidth)
        self.customData = try container.decodeIfPresent(String.self, forKey: .customData)
        self.parentMessageID = try container.decodeIfPresent(String.self, forKey: .parentMessageId)
        self.repliesUrl = try container.decodeIfPresent(URL.self, forKey: .repliesUrl)
        self.repliesCount = try container.decodeIfPresent(Int.self, forKey: .repliesCount)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(message, forKey: .message)
        try container.encode(senderID, forKey: .senderId)
        try container.encode(senderNickname, forKey: .senderNickname)
        try container.encode(senderImageURL, forKey: .senderImageUrl)
        try container.encode(programDateTime, forKey: .programDateTime)
        try container.encode(filteredMessage, forKey: .filteredMessage)
        try container.encode(contentFilter, forKey: .contentFilter)
        try container.encode(imageURL, forKey: .imageUrl)
        try container.encode(imageHeight, forKey: .imageHeight)
        try container.encode(imageWidth, forKey: .imageWidth)
        try container.encode(customData, forKey: .customData)
        try container.encode(parentMessageID, forKey: .parentMessageId)
    }
    
    public init(
        id: String,
        message: String?,
        senderID: String?,
        senderNickname: String?,
        senderImageURL: URL?,
        programDateTime: Date?,
        filteredMessage: String?,
        contentFilter: [ChatFilter]?,
        imageURL: URL?,
        imageHeight: Int?,
        imageWidth: Int?,
        customData: String?,
        parentMessageId: String?,
        repliesCount: Int?,
        repliesUrl: URL?
    ) {
        self.id = id.lowercased()
        self.message = message
        self.senderID = senderID
        self.senderNickname = senderNickname
        self.senderImageURL = senderImageURL
        self.programDateTime = programDateTime
        self.filteredMessage = filteredMessage
        self.contentFilter = contentFilter
        self.imageURL = imageURL
        self.imageHeight = imageHeight
        self.imageWidth = imageWidth
        self.customData = customData
        self.repliesUrl = repliesUrl
        self.repliesCount = repliesCount
        self.parentMessageID = parentMessageId
    }
    
    public init(chatMessage: ChatMessage) {
        self.id = chatMessage.id.asString.lowercased()
        self.message = chatMessage.text
        self.senderID = chatMessage.senderID
        self.senderNickname = chatMessage.senderNickname
        self.senderImageURL = chatMessage.profileImageURL
        self.programDateTime = nil
        self.filteredMessage = chatMessage.filteredMessage
        self.contentFilter = Array(chatMessage.filteredReasons)
        self.imageURL = chatMessage.imageURL
        self.imageHeight = chatMessage.imageHeight != nil ? Int(chatMessage.imageHeight ?? 0) : nil
        self.imageWidth = chatMessage.imageWidth != nil ? Int(chatMessage.imageWidth ?? 0) : nil
        self.customData = chatMessage.customData
        self.parentMessageID = chatMessage.parentMessageID
    }
}
