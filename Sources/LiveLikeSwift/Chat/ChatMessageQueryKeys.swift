//
//  ChatMessageQueryKeys.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

enum ChatMessageQueryKeys: String {
    case chatRoomID = "chat_room_id"
    case pageSize = "page_size"
    case since = "since"
    case until = "until"
    case ordering = "ordering"
    case parentMessageID = "parent_message_id"

}
