//
//  ChatClient.swift
//  EngagementSDK
//
//  Created by Keval Shah on 9/16/21.
//

import Foundation
import LiveLikeCore

// MARK: - Protocols

public protocol ChatClientDelegate: AnyObject {
    /**
     Called when the current user is added to the chatroom by another user
     
     - parameter newChatMembershipInfo: An object of type NewChatMembershipInfo containing details of the chatroom and sender of the invite
     */
    func chatClient(_ chatClient: ChatClient, userDidBecomeMemberOfChatRoom newChatMembershipInfo: NewChatMembershipInfo)
    /**
     Called when the current user receives an invitation to join a chatroom
     
     - parameter chatRoomInvitation: An object of type ChatRoomInvitation containing details of the invitation sent to the user for an invite
     */
    func chatClient(_ chatClient: ChatClient, userDidReceiveInvitation chatRoomInvitation: ChatRoomInvitation)
    
    @available(*, deprecated, message: "Please use the function in `UserClientDelegate` instead")
    /**
     Called when the current user blocks another user
     
     - parameter blockInfo: An object of type BlockInfo containing details of the blocked profile, type of block and details of the profile requesting the block.
     */
    func chatClient(_ chatClient: ChatClient, userDidGetBlocked blockInfo: BlockInfo)
    
    @available(*, deprecated, message: "Please use the function in `UserClientDelegate` instead")
    /**
     Called when the current user is added to the chatroom by another user
     
     - parameter unblockInfo: An object of type unblockedProfileInfo containing the id of the profile that was unblocked..
     */
    func chatClient(_ chatClient: ChatClient, userDidGetUnblocked unblockInfo: UnblockInfo)

}

public protocol ChatClient: AnyObject {
    
    var delegate: ChatClientDelegate? { get set }
    
    func getChatMessages(
        page: Pagination,
        options: GetChatMessageRequestOptions,
        completion: @escaping (Result<[ChatMessage], Error>) -> Void
    )
    
    /// Add another user to a Chat Room using their userId
    func addNewMemberToChatRoom(
        roomID: String,
        profileID: String,
        completion: @escaping (Result<ChatRoomMember, Error>) -> Void
    )
    
    /// Send an invitation for a chatroom to another user using the chatroom id and profile id
    func sendChatRoomInviteToUser(
        roomID: String,
        profileID: String,
        completion: @escaping (Result<ChatRoomInvitation, Error>) -> Void
    )
    
    /// Update the status (Accepted, Pending (default), Rejected) of an invitation for a chatroom received by the user
    func updateChatRoomInviteStatus(
        chatRoomInvitation: ChatRoomInvitation,
        invitationStatus: ChatRoomInvitationStatus,
        completion: @escaping (Result<ChatRoomInvitation, Error>) -> Void
    )
    
    /// Get a paginated list of invitations received by the current user
    func getInvitationsForUserWithInvitationStatus(
        invitationStatus: ChatRoomInvitationStatus,
        page: Pagination,
        completion: @escaping (Result<[ChatRoomInvitation], Error>) -> Void
    )
    
    /// Get a paginated list of invitations sent by the current user
    func getInvitationsByUserWithInvitationStatus(
        invitationStatus: ChatRoomInvitationStatus,
        page: Pagination,
        completion: @escaping (Result<[ChatRoomInvitation], Error>) -> Void
    )
    
    @available(*, deprecated, message: "Please use the function in `UserClient` instead")
    /// Block a profile
    func blockProfile(
        profileID: String,
        completion: @escaping (Result<BlockInfo, Error>) -> Void
    )
    
    @available(*, deprecated, message: "Please use the function in `UserClient` instead")
    /// Unblock a previously blocked profile
    func unblockProfile(
        blockRequestID: String,
        completion: @escaping (Result<Bool, Error>) -> Void
    )
    
    @available(*, deprecated, message: "Please use the function in `UserClient` instead")
    /**
      Get a paginated list of profiles blocked by the current user. The list can be filtered using the parameters.
     
      - parameter blockedProfileID: String type which can be used to filter the list based on the id of the blocked profile
     */
    func getBlockedProfileList(
        blockedProfileID: String?,
        page: Pagination,
        completion: @escaping (Result<[BlockInfo], Error>) -> Void
    )
    
    @available(*, deprecated, message: "Please use the function in `UserClient` instead")
    /**
     Get details of a blocked profile for a particular profileID
     
     - parameter profileID: String type parameter of the profile ID for which the block information is requested.
     */
    func getProfileBlockInfo(
        profileID: String,
        completion: @escaping (Result<BlockInfo, Error>) -> Void
    )
    
    @available(*, deprecated, message: "Please use the function in `UserClient` instead")
    /**
     Get a list of all blocked profile IDs for a particular profileID
     
     - parameter profileID: String type parameter of the profile ID for which the blocked profiles are requested.
     */
    func getBlockedProfileIDList(
        profileID: String,
        page: Pagination,
        completion: @escaping (Result<PaginatedResult<String>, Error>) -> Void
    )
    
    @available(*, deprecated, message: "Please use the function in `UserClient` instead")
    /// Returns a complete list of blocked profile ids
    func getBlockedProfileIDListComplete(
        completion: @escaping (Result<[String], Error>) -> Void
    )
    
    /// Retrieve details about user access to a token gated chat room
    func getTokenGatedChatAccessDetails(roomID: String, walletAddress: String, completion: @escaping (Result<TokenGatedChatAccessInfo, Error>) -> Void)
}

// MARK: - Internal

final class InternalChatClient: ChatClient, PubSubChannelDelegate {
    
    weak var delegate: ChatClientDelegate?
    
    private var userChannel: PubSubChannel?
    private let whenAccessToken: Promise<AccessToken>
    private let whenUserProfile: Promise<UserProfile>
    private let whenProfileResource: Promise<ProfileResource>
    private let coreAPI: LiveLikeCoreAPIProtocol
    private let chatAPI: LiveLikeChatAPIProtocol
    
    private var chatRoomsInvitationsUrl: URL?
    private var chatRoomInvitationDetailUrlTemplate: String?
    private var createChatRoomInvitationUrl: URL?
    private var profileChatRoomReceivedInvitationsUrlTemplate: String?
    private var profileChatRoomSentInvitationsUrlTemplate: String?
    
    private var listeners = Listener<ChatClientDelegate>(dispatchQueueLabel: "com.livelike.chatClientListeners")
    
    struct ChatClientPaginationProgress {
        var next: URL?
        var previous: URL?
        var total: Int = 0
    }
    
    var chatClientPagination: ChatClientPaginationProgress
    private var clientPageURLRepo = [String: PaginatedResultURL]()
    
    lazy var whenBlockList: Promise<BlockList> = {
        firstly {
            self.whenUserProfile
        }.then { userProfile in
            let blockList = BlockList(for: userProfile.userID.asString)
            return Promise { fulfill, reject in
                self.getBlockedProfileIDList(profileID: userProfile.userID.asString, page: .first) { [weak self] result in
                    guard let self = self else { return }
                    switch result {
                    case .success(let blockedIDList):
                        blockList.clear()
                        for blockId in blockedIDList.items {
                            blockList.block(userWithID: blockId)
                        }
                        self.loadNextBlockedProfilesPageRecursive(
                            blockList: blockList,
                            blockedIDList: blockedIDList,
                            profileID: userProfile.userID.asString
                        ) { blockList in
                            fulfill(blockList)
                        }
                    case .failure(let error):
                        log.error("There was an error loading a page of the block list. The block list may be incomplete. Error: \(error.localizedDescription)")
                        fulfill(blockList)
                    }
                }
            }
        }
    }()
    
    init(
        whenAccessToken: Promise<AccessToken>,
        whenUserProfile: Promise<UserProfile>,
        whenProfileResource: Promise<ProfileResource>,
        whenPubSubService: Promise<PubSubService?>,
        coreAPI: LiveLikeCoreAPIProtocol,
        chatAPI: LiveLikeChatAPIProtocol
    ) {
        self.whenUserProfile = whenUserProfile
        self.whenAccessToken = whenAccessToken
        self.whenProfileResource = whenProfileResource
        self.coreAPI = coreAPI
        self.chatAPI = chatAPI
        
        self.chatClientPagination = ChatClientPaginationProgress()
        firstly {
            coreAPI.whenApplicationConfig
        }.then { application in
            return Promises.zip(
                Promise(value: application),
                whenPubSubService,
                whenProfileResource
            )
        }.then { application, pubsubService, profileResource in
            self.chatRoomsInvitationsUrl = application.chatRoomsInvitationsUrl
            self.chatRoomInvitationDetailUrlTemplate = application.chatRoomInvitationDetailUrlTemplate
            self.createChatRoomInvitationUrl = application.createChatRoomInvitationUrl
            self.profileChatRoomReceivedInvitationsUrlTemplate = application.profileChatRoomReceivedInvitationsUrlTemplate
            self.profileChatRoomSentInvitationsUrlTemplate = application.profileChatRoomSentInvitationsUrlTemplate
            let userPubnubChannel = profileResource.subscribeChannel
            let userChannel = pubsubService?.subscribe(userPubnubChannel)
            self.userChannel = userChannel
            self.userChannel?.delegate = self
        }
    }
    
    deinit {
        log.info("User Session has ended.")
    }
    
    func getChatMessages(
        page: Pagination,
        options: GetChatMessageRequestOptions,
        completion: @escaping (Result<[ChatMessage], Error>) -> Void
    ) {
        firstly {
            self.whenAccessToken
        }.then { accessToken in
            Promises.zip(
                self.whenAccessToken,
                self.chatAPI.getChatRoomResource(
                    roomID: options.chatRoomID,
                    accessToken: accessToken
                )
            )
        }.then(on: DispatchQueue.global()) { accessToken, chatRoom in
            GetChatMesssagesRequest(
                accessToken: accessToken.asString,
                networking: LiveLike.networking,
                options: options
            ).execute(
                url: chatRoom.chatroomMessagesUrl
            ) {
                completion($0.map {
                    $0.results.map {
                        ChatMessage(chatMessageResponse: $0)
                    }
                })
            }
        }
    }
    
    /// Add another user to a Chat Room using their userId
    func addNewMemberToChatRoom(
        roomID: String,
        profileID: String,
        completion: @escaping (Result<ChatRoomMember, Error>) -> Void
    ) {
        firstly {
            whenAccessToken
        }.then { accessToken in
            return Promises.zip(
                Promise(value: accessToken),
                self.chatAPI.getChatRoomResource(roomID: roomID, accessToken: accessToken)
            )
        }.then { (accessToken, chatRoomResource) -> Promise<ChatRoomMember> in
            self.chatAPI.addNewMemberToChatRoom(
                membershipUrl: chatRoomResource.membershipsUrl,
                profileID: profileID,
                accessToken: accessToken
            )
        }.then { chatRoomMember in
            completion(.success(chatRoomMember))
        }.catch { error in
            log.error("Error adding user with user id: \(profileID) : \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func sendChatRoomInviteToUser(
        roomID: String,
        profileID: String,
        completion: @escaping (Result<ChatRoomInvitation, Error>) -> Void
    ) {
        guard let invitationUrl = self.createChatRoomInvitationUrl else {
            log.error("Error Invitation URL not found")
            return
        }
        
        firstly {
            whenAccessToken
        }.then { accessToken -> Promise<ChatRoomInvitation> in
            self.chatAPI.sendChatRoomInviteToUser(chatRoomInviteUrl: invitationUrl, chatRoomID: roomID, profileID: profileID, accessToken: accessToken)
        }.then { chatRoomInvitation in
            completion(.success(chatRoomInvitation))
        }.catch { error in
            log.error("Error Inviting User with userId: \(profileID): \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func updateChatRoomInviteStatus(
        chatRoomInvitation: ChatRoomInvitation,
        invitationStatus: ChatRoomInvitationStatus,
        completion: @escaping (Result<ChatRoomInvitation, Error>) -> Void
    ) {
        firstly {
            whenAccessToken
        }.then { accessToken -> Promise<ChatRoomInvitation> in
            self.chatAPI.updateChatRoomInvitationStatus(chatRoomInvitationUrl: chatRoomInvitation.url, invitationStatus: invitationStatus, accessToken: accessToken)
        }.then { chatRoomInvitation in
            completion(.success(chatRoomInvitation))
        }.catch { error in
            log.error("Error Updating status for: \(chatRoomInvitation.id): \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func getInvitationsForUserWithInvitationStatus(
        invitationStatus: ChatRoomInvitationStatus,
        page: Pagination,
        completion: @escaping (Result<[ChatRoomInvitation], Error>) -> Void
    ) {
        
        let pageResultURLID = "getInvitationsForUserWithInvitationStatus_\(invitationStatus.rawValue)"
        let profileReceivedInvitationURLTemplate: Promise<String> = Promise { fulfill, reject in
            switch page {
            case .first:

                // Reset result repo cache if the user is calling `.first` page
                self.clientPageURLRepo[pageResultURLID] = nil

                if let profileReceivedInvitationsUrlTemplate = self.profileChatRoomReceivedInvitationsUrlTemplate {
                    fulfill(profileReceivedInvitationsUrlTemplate)
                } else {
                    log.error("Error getting profileChatRoomInvitationsUrlTemplate")
                    throw ContentSessionError.invalidChatRoomInvitationURLTemplate
                }
            case .next:
                if let url = self.clientPageURLRepo[pageResultURLID]?.nextURL {
                    fulfill(url.absoluteString)
                } else {
                    reject(PaginationErrors.nextPageUnavailable)
                }
            case .previous:
                if let url = self.clientPageURLRepo[pageResultURLID]?.previousURL {
                    fulfill(url.absoluteString)
                } else {
                    reject(PaginationErrors.previousPageUnavailable)
                }
            }
        }

        firstly {
            Promises.zip(
                profileReceivedInvitationURLTemplate,
                whenAccessToken,
                whenProfileResource
            )
        }.then { url, accessToken, userProfile in
            self.chatAPI.getInvitationsForUserWithInvitationStatus(
                invitationStatus: invitationStatus,
                chatRoomInvitationsUrlTemplate: url,
                profileID: userProfile.id,
                accessToken: accessToken
            )
        }.then { paginatedInvitationsResource in

            self.clientPageURLRepo[pageResultURLID] = PaginatedResultURL(
                nextURL: paginatedInvitationsResource.next,
                previousURL: paginatedInvitationsResource.previous
            )
            completion(.success(paginatedInvitationsResource.items))
        }.catch { error in
            log.error("Error getting invitations: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func getInvitationsByUserWithInvitationStatus(
        invitationStatus: ChatRoomInvitationStatus,
        page: Pagination,
        completion: @escaping (Result<[ChatRoomInvitation], Error>) -> Void
    ) {
        
        let pageResultURLID = "getInvitationsByUserWithInvitationStatus_\(invitationStatus.rawValue)"
        let profileSentInvitationURLTemplate: Promise<String> = Promise { fulfill, reject in
            switch page {
            case .first:

                // Reset result repo cache if the user is calling `.first` page
                self.clientPageURLRepo[pageResultURLID] = nil

                if let profileSentInvitationsUrlTemplate = self.profileChatRoomSentInvitationsUrlTemplate {
                    fulfill(profileSentInvitationsUrlTemplate)
                } else {
                    log.error("Error getting profileChatRoomInvitationsUrlTemplate")
                    throw ContentSessionError.invalidChatRoomInvitationURLTemplate
                }
            case .next:
                if let url = self.clientPageURLRepo[pageResultURLID]?.nextURL {
                    fulfill(url.absoluteString)
                } else {
                    reject(PaginationErrors.nextPageUnavailable)
                }
            case .previous:
                if let url = self.clientPageURLRepo[pageResultURLID]?.previousURL {
                    fulfill(url.absoluteString)
                } else {
                    reject(PaginationErrors.previousPageUnavailable)
                }
            }
        }

        firstly {
            Promises.zip(
                profileSentInvitationURLTemplate,
                whenAccessToken,
                whenProfileResource
            )
        }.then { url, accessToken, userProfile in
            self.chatAPI.getInvitationsByUserWithInvitationStatus(
                invitationStatus: invitationStatus,
                chatRoomInvitationsUrlTemplate: url,
                profileID: userProfile.id,
                accessToken: accessToken
            )
        }.then { paginatedInvitationsResource in

            self.clientPageURLRepo[pageResultURLID] = PaginatedResultURL(
                nextURL: paginatedInvitationsResource.next,
                previousURL: paginatedInvitationsResource.previous
            )
            completion(.success(paginatedInvitationsResource.items))
        }.catch { error in
            log.error("Error getting invitations: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func channel(_ channel: PubSubChannel, messageCreated message: PubSubChannelMessage) {
        guard let payload = try? PubSubChatMessageDecoder.shared.decode(dict: message.message) else {
            log.error("Failed to decode pub sub chat message.")
            return
        }
        
        switch payload {
        case .messageCreated:
            break
        case .messageDeleted:
            break
        case .imageCreated:
            break
        case .imageDeleted:
            break
        case .customMessageCreated:
            break
        case .chatroomUpdated:
            break
        case .reactionSpaceUpdated:
            break
        case .userReactionAdded:
            break
        case .userReactionRemoved:
            break
        case .addedToChatRoom(let payload):
            let newMemberInfo = NewChatMembershipInfo(
                id: payload.id,
                chatRoomID: payload.chatRoomId,
                chatRoomTitle: payload.chatRoomTitle,
                senderID: payload.senderId,
                senderNickName: payload.senderNickname,
                senderImageURL: payload.senderImageUrl,
                badgeImageURL: payload.badgeImageUrl,
                senderProfileURL: payload.senderProfileUrl,
                createdAt: payload.createdAt
            )
            listeners.publish { $0.chatClient(self, userDidBecomeMemberOfChatRoom: newMemberInfo)}
            if let delegate = delegate {
                delegate.chatClient(self, userDidBecomeMemberOfChatRoom: newMemberInfo)
            }
        case .invitedToChatRoom(let payload):
            let chatRoomInvitation = ChatRoomInvitation(
                id: payload.id,
                chatRoomID: payload.chatRoomID,
                url: payload.url,
                createdAt: payload.createdAt,
                status: payload.status,
                invitedProfile: payload.invitedProfile,
                chatRoom: payload.chatRoom,
                invitedBy: payload.invitedBy,
                invitedProfileID: payload.invitedProfileID
            )
            listeners.publish { $0.chatClient(self, userDidReceiveInvitation: chatRoomInvitation)}
            if let delegate = delegate {
                delegate.chatClient(self, userDidReceiveInvitation: chatRoomInvitation)
            }
        case .profileBlocked(let payload):
            let blockInfo = BlockInfo(
                id: payload.id,
                url: payload.url,
                blockedProfileID: payload.blockedProfileID,
                blockedByProfileID: payload.blockedByProfileID,
                blockedProfile: payload.blockedProfile,
                blockedByProfile: payload.blockedByProfile
            )
            
            firstly {
                whenBlockList
            }.then { blockList in
                blockList.block(userWithID: payload.blockedProfileID)
                self.listeners.publish { $0.chatClient(self, userDidGetBlocked: blockInfo)}
                if let delegate = self.delegate {
                    delegate.chatClient(self, userDidGetBlocked: blockInfo)
                }
            }
        case .profileUnblocked(let payload):
            let unblockInfo = UnblockInfo(
                id: payload.id,
                blockedProfileID: payload.blockedProfileID
            )
            firstly {
                whenBlockList
            }.then { blockList in
                blockList.unblock(userWithID: payload.blockedProfileID)
                self.listeners.publish { $0.chatClient(self, userDidGetUnblocked: unblockInfo)}
                if let delegate = self.delegate {
                    delegate.chatClient(self, userDidGetUnblocked: unblockInfo)
                }
            }

        case .messagePinned:
            break
        case .messageUnpinned:
            break
        }
    }
    
    func channel(_ channel: PubSubChannel, messageActionCreated messageAction: PubSubMessageAction) { }
    
    func channel(_ channel: PubSubChannel, messageActionDeleted messageActionID: String, messageID: String) { }
    
    func blockProfile(profileID: String, completion: @escaping (Result<BlockInfo, Error>) -> Void) {
        firstly {
            Promises.zip(
                whenAccessToken,
                whenProfileResource,
                whenBlockList
            )
        }.then { accessToken, userProfile, blockList in
            Promises.zip(
                self.chatAPI.blockProfile(
                    blockProfileURL: userProfile.blockProfileUrl,
                    profileID: profileID,
                    accessToken: accessToken
                ),
                Promise(value: blockList)
            )
        }.then { blockedProfile, blockList in
            blockList.block(userWithID: profileID)
            completion(.success(blockedProfile))
        }.catch { error in
            log.error("Error Blocking User with userId: \(profileID): \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func unblockProfile(blockRequestID: String, completion: @escaping (Result<Bool, Error>) -> Void) {
        firstly {
            Promises.zip(
                whenAccessToken,
                whenProfileResource
            )
        }.then { accessToken, userProfile -> Promise<Bool> in
            self.chatAPI.unblockProfile(
                unblockProfileURL: userProfile.blockProfileUrl.appendingPathComponent(blockRequestID, isDirectory: true),
                accessToken: accessToken
            )
        }.then { result in
            completion(.success(result))
        }.catch { error in
            log.error("Error Unblocking User with requestID: \(blockRequestID): \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func getBlockedProfileList(
        blockedProfileID: String?,
        page: Pagination,
        completion: @escaping (Result<[BlockInfo], Error>
        ) -> Void
    ) {
        
        let pageResultURLID = "getBlockedProfileList_\(blockedProfileID ?? "")"
        let blockedProfileListURL: Promise<URL> = Promise { fulfill, reject in
            switch page {
            case .first:

                // Reset result repo cache if the user is calling `.first` page
                self.clientPageURLRepo[pageResultURLID] = nil

                firstly {
                    self.whenProfileResource
                }.then { profileResource in
                    let urlTemplate = profileResource.blockedProfilesTemplateUrl.replacingOccurrences(
                        of: "{blocked_profile_id}",
                        with: blockedProfileID ?? ""
                    )
                    
                    if let blockProfileURL = URL(string: urlTemplate) {
                        fulfill(blockProfileURL)
                    } else {
                        reject(ChatClientError.invalidBlockProfileURLTemplate)
                    }
                }.catch {
                    reject($0)
                }

            case .next:
                if let url = self.clientPageURLRepo[pageResultURLID]?.nextURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.nextPageUnavailable)
                }
            case .previous:
                if let url = self.clientPageURLRepo[pageResultURLID]?.previousURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.previousPageUnavailable)
                }
            }
        }

        firstly {
            Promises.zip(
                blockedProfileListURL,
                whenAccessToken
            )
        }.then { url, accessToken in
            self.chatAPI.getBlockedProfileList(
                blockProfileURL: url,
                blockedProfileID: blockedProfileID,
                accessToken: accessToken
            )
        }.then { paginatedBlockedList in
            self.clientPageURLRepo[pageResultURLID] = PaginatedResultURL(
                nextURL: paginatedBlockedList.next,
                previousURL: paginatedBlockedList.previous
            )
            completion(.success(paginatedBlockedList.items))
        }.catch { error in
            log.error("Failed getting list of blocked transfers")
            completion(.failure(error))
        }
    }
    
    func getProfileBlockInfo(
        profileID: String,
        completion: @escaping (Result<BlockInfo, Error>) -> Void
    ) {
        let blockProfileInfoURL: Promise<URL> = Promise { fulfill, reject in
            
            firstly {
                self.whenProfileResource
            }.then { profileResource in
                let urlTemplate = profileResource.blockedProfilesTemplateUrl.replacingOccurrences(
                    of: "{blocked_profile_id}",
                    with: profileID
                )
                
                if let blockProfileInfoURL = URL(string: urlTemplate) {
                    fulfill(blockProfileInfoURL)
                } else {
                    reject(ChatClientError.invalidBlockProfileURLTemplate)
                }
            }.catch {
                reject($0)
            }
            
        }
        
        firstly {
            Promises.zip(
                blockProfileInfoURL,
                whenAccessToken
            )
        }.then { url, accessToken in
            self.chatAPI.getBlockProfileInfo(
                blockProfileInfoURL: url,
                accessToken: accessToken
            )
        }.then { blockInfo in
            if let blockProfileInfo = blockInfo.items.first {
                completion(.success(blockProfileInfo))
            } else {
                log.error("Failed getting information of blocked profile")
                completion(.failure(ChatClientError.decodingFailedForBlockInfo))
            }
        }.catch { error in
            log.error("Failed getting information of blocked profile")
            completion(.failure(error))
        }
    }
    
    func getBlockedProfileIDList(
        profileID: String,
        page: Pagination,
        completion: @escaping (Result<PaginatedResult<String>, Error>) -> Void
    ) {
        
        let pageResultURLID = "getBlockedProfileIDList_\(profileID)"
        let blockedProfileIDListURL: Promise<URL> = Promise { fulfill, reject in
            switch page {
            case .first:

                // Reset result repo cache if the user is calling `.first` page
                self.clientPageURLRepo[pageResultURLID] = nil

                firstly {
                    self.whenProfileResource
                }.then { profileResource in
                    let url = profileResource.blockedProfileIdsUrl
                    fulfill(url)
                }.catch {
                    reject($0)
                }

            case .next:
                if let url = self.clientPageURLRepo[pageResultURLID]?.nextURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.nextPageUnavailable)
                }
            case .previous:
                if let url = self.clientPageURLRepo[pageResultURLID]?.previousURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.previousPageUnavailable)
                }
            }
        }

        firstly {
            Promises.zip(
                blockedProfileIDListURL,
                whenAccessToken
            )
        }.then { url, accessToken in
            self.chatAPI.getBlockedProfileIDList(
                blockedIDListURL: url,
                accessToken: accessToken
            )
        }.then { paginatedBlockedList in
            self.clientPageURLRepo[pageResultURLID] = PaginatedResultURL(
                nextURL: paginatedBlockedList.next,
                previousURL: paginatedBlockedList.previous
            )
            completion(.success(paginatedBlockedList))
        }.catch { error in
            log.error("Failed getting list of blocked transfers")
            completion(.failure(error))
        }
    }
    
    func getBlockedProfileIDListComplete(
        completion: @escaping (Result<[String], Error>) -> Void
    ) {
        firstly {
            self.whenBlockList
        }.then {
            completion(.success($0.blockedUserIDs.allObjects))
        }.catch {
            completion(.failure($0))
        }
    }
    
    func getTokenGatedChatAccessDetails(roomID: String, walletAddress: String, completion: @escaping (Result<TokenGatedChatAccessInfo, Error>) -> Void) {
        firstly {
            whenAccessToken
        }.then { accessToken in
            self.chatAPI.checkTokenGatedChatAccess(roomID: roomID, walletAddress: walletAddress, accessToken: accessToken)
        }.then { tokenGatedChat in
            completion(.success(TokenGatedChatAccessInfo(tokenGatedChat: tokenGatedChat)))
        }.catch { error in
            log.error("Error while checking access to token gated chat room: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    private func loadNextBlockedProfilesPageRecursive(
        blockList: BlockList,
        blockedIDList: PaginatedResult<String>,
        profileID: String,
        completion: @escaping (BlockList) -> Void
    ) {
        if blockedIDList.hasNext {
            self.getBlockedProfileIDList(profileID: profileID, page: .next) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let blockedIDs):
                    for blockId in blockedIDs.items {
                        blockList.block(userWithID: blockId)
                    }
                    self.loadNextBlockedProfilesPageRecursive(
                        blockList: blockList,
                        blockedIDList: blockedIDs,
                        profileID: profileID,
                        completion: completion
                    )
                case .failure(let error):
                    log.error("There was an error loading a page of the block list. The block list may be incomplete. Error: \(error.localizedDescription)")
                }
            }
        } else {
            completion(blockList)
        }
        
    }

}
