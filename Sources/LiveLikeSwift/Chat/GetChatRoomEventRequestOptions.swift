//
//  GetChatRoomEventRequestOptions.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

public struct GetChatRoomEventRequestOptions {
    // Is an array of `URLQueryItem` used to compile URL parameters
    let urlQueries: [URLQueryItem]

    let roomID: String

    public init(
        roomID: String
    ) {
        self.roomID = roomID

        self.urlQueries = {
            var urlQueries = [URLQueryItem]()
            urlQueries.append(
                URLQueryItem(name: "chat_room_id", value: roomID)
            )
            urlQueries.append(
                URLQueryItem(
                    name: "since",
                    value: DateFormatter.iso8601FullWithPeriod.currentDateString
                )
            )
            return urlQueries
        }()
    }
}
