//
//  ChatMessageID.swift
//  
//
//  Created by Jelzon Monzon on 11/10/22.
//

import Foundation

/// A type representing a unique ID of a `Chat Message`
public struct ChatMessageID: Equatable, Hashable {

    /// The stringified id
    public var asString: String {
        if let id = self.id {
            return id
        } else if let clientMessageID = clientMessageID {
            return clientMessageID
        } else {
            return ""
        }
    }

    /// The client generated id when message was created
    public let clientMessageID: String?
    /// The id used for server operations
    public let id: String?

    init(id: String?, clientMessageID: String?) {
        self.clientMessageID = clientMessageID
        self.id = id
    }

    public static func == (lhs: ChatMessageID, rhs: ChatMessageID) -> Bool {
        if let lhsID = lhs.id, let rhsID = rhs.id {
            return lhsID == rhsID
        } else if let lhsClientMessageID = lhs.clientMessageID, let rhsClientMessageID = rhs.clientMessageID {
            return lhsClientMessageID == rhsClientMessageID
        }
        return false
    }
}
