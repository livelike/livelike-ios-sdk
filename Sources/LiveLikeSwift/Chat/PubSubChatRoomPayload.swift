//
//  PubSubChatRoomPayload.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

struct PubSubChatRoomPayload: Codable {
    var id: String
    var visibility: String?
    var title: String?
    var customData: String?
    var contentFilter: ChatFilter?
    var tokenGates: [ChatRoomTokenGate]
    var reactionSpaceID: String?

    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case visibility
        case customData
        case contentFilter = "content_filter"
        case tokenGates
        case reactionSpaceID = "reaction_space_id"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id).lowercased()
        self.title = try? container.decode(String.self, forKey: .title)
        self.visibility = try? container.decode(String.self, forKey: .visibility)
        self.customData = try? container.decode(String.self, forKey: .customData)
        self.contentFilter = try? container.decode(ChatFilter.self, forKey: .contentFilter)
        self.tokenGates = try container.decode([ChatRoomTokenGate].self, forKey: .tokenGates)
        self.reactionSpaceID = try container.decodeIfPresent(String.self, forKey: .reactionSpaceID)
    }

    init(
        id: String,
        title: String?,
        visibility: String?,
        customData: String?,
        contentFilter: ChatFilter?,
        tokenGates: [ChatRoomTokenGate],
        reactionSpaceID: String?
    ) {
        self.id = id.lowercased()
        self.title = title
        self.visibility = visibility
        self.customData = customData
        self.contentFilter = contentFilter
        self.tokenGates = tokenGates
        self.reactionSpaceID = reactionSpaceID
    }
}
