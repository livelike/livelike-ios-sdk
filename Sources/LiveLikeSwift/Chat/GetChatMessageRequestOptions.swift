//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//
	

import Foundation

public struct GetChatMessageRequestOptions {
    
    let chatRoomID: String
    let parentMessageID: String?
    let since: TimeToken?
    let pageSize: Int?
    
    public init(
        chatRoomID: String,
        parentMessageID: String? = nil,
        since: TimeToken? = nil,
        pageSize: Int? = nil
    ) {
        self.chatRoomID = chatRoomID
        self.parentMessageID = parentMessageID
        self.since = since
        self.pageSize = pageSize
    }
    
}
