//
//  ChatRoomResource.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 11/25/19.
//

import Foundation

struct ChatRoomResource: Decodable {
    var id: String
    var channels: Channels
    var uploadUrl: URL
    var title: String?
    var reportMessageUrl: URL
    var stickerPacksUrl: URL
    var reactionPacksUrl: URL
    var membershipsUrl: URL
    var mutedStatusUrlTemplate: String
    var visibility: ChatRoomVisibilty
    var contentFilter: ChatFilter?
    var customData: String?
    var customMessagesUrl: URL
    var sponsorsUrl: URL
    var chatroomMessagesUrl: URL
    var chatroomMessagesCountUrl: URL
    var deleteMessageUrl: URL
    var tokenGates: [ChatRoomTokenGate]
    var reactionSpaceId: String?
    var chatroomTokenGateAccessUrlTemplate: String
    var chatroomAccessRuleUrl: URL

    struct Channels: Decodable {
        var chat: Channel
        var reactions: Channel?
        var control: Channel?
    }

    struct Channel: Decodable {
        var pubnub: String?
    }
}

struct ChatUserMuteStatusResource: Decodable {
    let isMuted: Bool
}

/// Used to signify current chat user's mute status
public struct ChatUserMuteStatus {
    public let isMuted: Bool
}

/// Used to signify the visibility of a chat room
public enum ChatRoomVisibilty: String, Codable, CaseIterable {
    case members
    case everyone
}

/// Used as a return object when calling `getChatRoomInfo()`
public struct ChatRoomInfo: Codable {
    public let id: String
    public let title: String?

    /// Assigned access level to the chat room
    public let visibility: ChatRoomVisibilty

    /// Assigned filter to the chat room
    public let contentFilter: ChatFilter?

    /// Custom string data attached to the chat room
    public let customData: String?

    /// Available token gates configured for a chat room
    public let tokenGates: [ChatRoomTokenGate]

    /// Reaction Space ID linked to the created Chat Room
    public let reactionSpaceID: String?
}

extension ChatRoomInfo {
    init(chatroomResource: ChatRoomResource) {
        self.init(
            id: chatroomResource.id,
            title: chatroomResource.title,
            visibility: chatroomResource.visibility,
            contentFilter: chatroomResource.contentFilter,
            customData: chatroomResource.customData,
            tokenGates: chatroomResource.tokenGates,
            reactionSpaceID: chatroomResource.reactionSpaceId
        )
    }
}

/// Used as a result object for `didBecomeMemberOfChatRoom()`
public struct NewChatMembershipInfo {
    public let id: String
    public let chatRoomID: String
    public let chatRoomTitle: String?
    public let senderID: String
    public let senderNickName: String
    public let senderImageURL: URL?
    public let badgeImageURL: URL?
    public let senderProfileURL: URL
    public let createdAt: Date
}

/// A Token Gate can be used to determine access to an SDK functionality
public struct ChatRoomTokenGate: Codable {
    /// Smart contract address
    public let contractAddress: String
    /// blockchain platform network
    public let networkType: BlockChainNetworkType
    /// web3 token type
    public let tokenType: BlockChainTokenType
    /// represents attributes needed for tokens
    public let attributes: [NFTAttribute]
}

// MARK: - NFTAttribute
public struct NFTAttribute: Codable {
    /// meta data trait type
    public let traitType: String
    /// value for a trait
    public let value: String

    public init(traitType: String, value: String) {
        self.traitType = traitType
        self.value = value
    }
}
/// Used to specify blockchain network type
public enum BlockChainNetworkType: String, CaseIterable, Codable {
    case ethereum = "ethereum"
    case polygon = "polygon"
    case chiliz = "chiliz"
    case hedera = "hedera"
}
