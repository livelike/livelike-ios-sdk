//
//  TokenGate.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

/// Create Token Gate by specifying smartcontract address and Blockchain network type
public struct TokenGate: Encodable {
    /// Smart contract address
    public let contractAddress: String
    /// blockchain platform network
    public let networkType: BlockChainNetworkType
    /// web3 token type
    public let tokenType: BlockChainTokenType
    /// represents attributes needed for tokens
    public let attributes: [NFTAttribute]?

    public init(contractAddress: String, networkType: BlockChainNetworkType, tokenType: BlockChainTokenType, attributes: [NFTAttribute]) {
        self.contractAddress = contractAddress
        self.networkType = networkType
        self.tokenType = tokenType
        self.attributes = attributes
    }
}
