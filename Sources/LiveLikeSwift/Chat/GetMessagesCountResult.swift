//
//  GetMessagesCountResult.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

struct GetMessagesCountResult: Decodable {
    let count: Int
}
