//
//  BlockList.swift
//  EngagementSDK
//
//  Created by Xavi Matos on 6/27/19.
//

import Foundation

class BlockList {
    private(set) var blockedUserIDs: Set<String>
    private var listOwnerUserID: String

    init(for userID: String) {
        listOwnerUserID = userID
        blockedUserIDs = BlockList.loadBlockedUserIDs(for: userID)
    }
}

internal extension BlockList {
    func contains(userID: String) -> Bool {
        return blockedUserIDs.contains(userID)
    }

    func block(userWithID id: String) {
        guard listOwnerUserID != id else { return }
        blockedUserIDs.insert(id)
        try? save()
    }

    func unblock(userWithID id: String) {
        guard listOwnerUserID != id else { return }
        blockedUserIDs.remove(id)
        try? save()
    }

    func clear() {
        blockedUserIDs.removeAll()
    }
}

private extension BlockList {
    static let liveLikeUserDataFolderName = "LiveLikeUserData"
    static let blockListsFolderName = "BlockLists"

    static var decorder: PropertyListDecoder { return PropertyListDecoder() }
    static var encoder: PropertyListEncoder { return PropertyListEncoder() }

    static func fileURL(for userID: String) -> URL {
        return FileManager.default
            .urls(for: .applicationSupportDirectory, in: .userDomainMask)[0]
            .appendingPathComponent(liveLikeUserDataFolderName)
            .appendingPathComponent(blockListsFolderName)
            .appendingPathComponent(userID)
    }

    static func loadBlockedUserIDs(for userID: String) -> Set<String> {
        do {
            let data = try Data(contentsOf: fileURL(for: userID))
            return try decorder.decode(Set<String>.self, from: data)
        } catch {
            return .init()
        }
    }

    func save() throws {
        let data = try BlockList.encoder.encode(blockedUserIDs)
        let fileURL = BlockList.fileURL(for: listOwnerUserID)

        if !FileManager.default.fileExists(atPath: fileURL.path) {
            try FileManager.default.createDirectory(
                at: fileURL.deletingLastPathComponent(),
                withIntermediateDirectories: true,
                attributes: nil
            )
        }

        try data.write(to: fileURL)
    }
}
