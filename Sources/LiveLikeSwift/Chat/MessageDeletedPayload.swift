//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

struct MessageDeletedPayload: Decodable {
    let id: String

    enum CodingKeys: CodingKey {
        case id
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
    }
}
