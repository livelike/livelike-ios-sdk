//
//  ChatRoomMembershipModels.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 6/17/20.
//

import Foundation

// Represents a result page when retrieving all chat rooms a user is a member of
struct UserChatRoomMembershipPage: Decodable {
    var results: [ChatRoomMembership]
    var next: URL?
    var previous: URL?
}

/// Represents a user member that belonds to a chat room
public struct ChatRoomMember: Decodable {
    public let id: String
    public let url: URL
    public let profile: ProfileResource
}

// Represents a chat room when querying the backend for user's chat room memberships
struct ChatRoomMembership: Decodable {
    public let id: String
    public let url: URL
    public let chatRoom: ChatRoomResource
}

/// Represents the object returned when a chatroom invitation is sent or received.
public struct ChatRoomInvitation: Decodable {
    public let id: String
    public let chatRoomID: String
    internal let url: URL
    public let createdAt: Date
    public let status: ChatRoomInvitationStatus
    public let invitedProfile: ProfileResource
    public let chatRoom: ChatRoomInfo
    public let invitedBy: ProfileResource
    public let invitedProfileID: String

    enum CodingKeys: String, CodingKey {
        case id
        case chatRoomId
        case url
        case createdAt
        case status
        case invitedProfile
        case chatRoom
        case invitedBy
        case invitedProfileId
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.url = try container.decode(URL.self, forKey: .url)
        self.createdAt = try container.decode(Date.self, forKey: .createdAt)
        self.status = try container.decode(ChatRoomInvitationStatus.self, forKey: .status)
        self.invitedProfile = try container.decode(ProfileResource.self, forKey: .invitedProfile)
        self.chatRoom = try container.decode(ChatRoomInfo.self, forKey: .chatRoom)
        self.invitedBy = try container.decode(ProfileResource.self, forKey: .invitedBy)
        self.chatRoomID = try container.decode(String.self, forKey: .chatRoomId)
        self.invitedProfileID = try container.decode(String.self, forKey: .invitedProfileId)
    }

    public init(
        id: String,
        chatRoomID: String,
        url: URL,
        createdAt: Date,
        status: ChatRoomInvitationStatus,
        invitedProfile: ProfileResource,
        chatRoom: ChatRoomInfo,
        invitedBy: ProfileResource,
        invitedProfileID: String
    ) {
        self.id = id
        self.chatRoomID = chatRoomID
        self.url = url
        self.createdAt = createdAt
        self.status = status
        self.invitedProfile = invitedProfile
        self.chatRoom = chatRoom
        self.invitedBy = invitedBy
        self.invitedProfileID = invitedProfileID
    }
}

/// Represents the different states of a a chatroom invitation
public enum ChatRoomInvitationStatus: String, Codable {
    case accepted
    case rejected
    case pending
}
