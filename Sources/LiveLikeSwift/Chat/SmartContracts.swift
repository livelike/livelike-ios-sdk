//
//  File.swift
//  
//
//  Created by apple on 22/05/23.
//

import Foundation

/// Used as a return object when calling `getSmartContracts()`
// MARK: - SmartContractInfo
public struct SmartContractInfo: Decodable {
    /// blockchain platform network
    public let networkType: BlockChainNetworkType
    /// Smart contract address alias
    public let contractName: String
    /// Smart contract address
    public let contractAddress: String
}
