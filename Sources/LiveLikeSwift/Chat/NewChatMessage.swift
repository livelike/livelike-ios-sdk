//
//  ChatUserMessage.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 3/18/21.
//

import Foundation

/// Is used to construct a chat message obect which can be passed
/// into the `sendMessage()` function of a `ChatSession`
public struct NewChatMessage {
    
    public init(
        imageURL: URL?,
        imageHeight: Int,
        imageWidth: Int,
        messageMetadata: MessageMetadata? = nil,
        parentMessageID: String? = nil
    ) {
        self.imageURL = imageURL
        self.imageHeight = imageHeight
        self.imageWidth = imageWidth
        self.messageMetadata = messageMetadata
        self.text = nil
        self.parentMessageID = parentMessageID
    }
    
    /// Represents the text of a chat message, including sticker shortcodes
    public let text: String?
    
    /// A `URL` that references an image
    public let imageURL: URL?
    
    public let imageHeight: Int?
    public let imageWidth: Int?
    
    /// A JSON serialzable dictionary of addiitional information to be attached to the message
    public let messageMetadata: MessageMetadata?
    
    public let parentMessageID: String?
    
    /// The `timeStamp` represents the time at which a chat message is to be shown to the user
    /// By default it is set to the time the message is published
    /// In the case of Spoiler Prevention functionality being turned on, its value is provided by the
    /// `syncTimeSource` set in `ChatSessionConfig` or `SessionConfiguration`
    var timeStamp: EpochTime?
    
    /// A `String` typed message used to pass custom data such as JSON
    var customData: String?
    
    /// Init method for a chat message containing text or stickers
    /// - Parameters:
    ///   - message: text of the message, can also contain shortcodes for stickers
    public init(
        text: String,
        messageMetadata: MessageMetadata? = nil,
        parentMessageID: String? = nil
    ) {
        self.text = text
        self.imageURL = nil
        self.imageWidth = nil
        self.imageHeight = nil
        self.customData = nil
        self.messageMetadata = messageMetadata
        self.parentMessageID = parentMessageID
    }
    
    /// Init method for a chat message containing a custom text message such as JSON
    /// - Parameters:
    ///   - customData: A  message used to pass custom data such as JSON
    public init(
        customData: String,
        parentMessageID: String? = nil
    ) {
        self.customData = customData
        self.text = nil
        self.imageURL = nil
        self.imageHeight = nil
        self.imageWidth = nil
        self.messageMetadata = nil
        self.parentMessageID = parentMessageID
    }
}


