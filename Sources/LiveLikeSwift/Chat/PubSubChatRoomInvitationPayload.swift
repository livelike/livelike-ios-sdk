//
//  PubSubChatRoomInvitationPayload.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

struct PubSubChatRoomInvitationPayload: Decodable {
    var id: String
    var chatRoomID: String
    var url: URL
    var createdAt: Date
    var status: ChatRoomInvitationStatus
    var invitedProfile: ProfileResource
    var chatRoom: ChatRoomInfo
    var invitedBy: ProfileResource
    var invitedProfileID: String

    private enum CodingKeys: String, CodingKey {
        case id
        case chatRoomId
        case url
        case createdAt
        case status
        case invitedProfile
        case chatRoom
        case invitedBy
        case invitedProfileId
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id).lowercased()
        self.chatRoomID = try container.decode(String.self, forKey: .chatRoomId)
        self.url = try container.decode(URL.self, forKey: .url)
        self.status = try container.decode(ChatRoomInvitationStatus.self, forKey: .status)
        self.invitedProfile = try container.decode(ProfileResource.self, forKey: .invitedProfile)
        self.chatRoom = try container.decode(ChatRoomInfo.self, forKey: .chatRoom)
        self.invitedBy = try container.decode(ProfileResource.self, forKey: .invitedBy)
        self.invitedProfileID = try container.decode(String.self, forKey: .invitedProfileId)
        self.createdAt = try container.decode(Date.self, forKey: .createdAt)
    }

    init(
        id: String,
        chatRoomID: String,
        url: URL,
        status: ChatRoomInvitationStatus,
        invitedProfile: ProfileResource,
        chatRoom: ChatRoomInfo,
        invitedBy: ProfileResource,
        invitedProfileID: String,
        createdAt: Date
    ) {
        self.id = id.lowercased()
        self.chatRoomID = chatRoomID
        self.url = url
        self.status = status
        self.invitedProfile = invitedProfile
        self.chatRoom = chatRoom
        self.invitedBy = invitedBy
        self.invitedProfileID = invitedProfileID
        self.createdAt = createdAt
    }
}
