//
//  ChatSentMessageProperties.swift
//  EngagementSDK
//
//  Created by Cory Sullivan on 2019-04-29.
//

import Foundation

struct ChatSentMessageProperties {
    let characterCount: Int
    let messageId: String
    let chatRoomId: String
    let stickerShortcodes: [String]
    let stickerCount: Int
    let stickerIndices: [[Int: Int]]
    let hasExternalImage: Bool
}

extension ChatSentMessageProperties {
    static func calculateStickerIndices(stickerIDs: [String], stickers: [StickerPack]) -> [[Int: Int]] {
        var indices = [[Int: Int]]()

        for id in stickerIDs {
            for (index, stickerPack) in stickers.enumerated() {
                if let resultIndex = stickerPack.stickers.firstIndex(where: { $0.shortcode == id.replacingOccurrences(of: ":", with: "") }) {
                    indices.append([index + 1: resultIndex + 1]) // Reporting indexes need to start at 1, not 0.
                }
            }
        }

        return indices
    }

    static func validateStickerShortcodes(stickerShortcodes: [String], stickers: [StickerPack]) -> [String] {
        var validStickerShortcodes = [String]()

        for id in stickerShortcodes {
            for stickerPack in stickers {
                if let validSticker = stickerPack.stickers.first(where: { $0.shortcode == id.replacingOccurrences(of: ":", with: "") }) {
                    validStickerShortcodes.append(validSticker.shortcode) 
                }
            }
        }

        return validStickerShortcodes
    }
}
