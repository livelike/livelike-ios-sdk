//
//  MessageActionType.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

enum MessageActionType: String, Decodable {
    case reactionCreated = "rc"
}
