//
//  TokenGatedChatResource.swift
//  
//
//  Created by apple on 18/04/23.
//

import Foundation

// MARK: - TokenGatedChat
/// Used to give us Token Gated Chatroom access details
struct TokenGatedChat: Decodable {
    let chatRoomID: String
    let access: TokenGatedChatAccessType
    let walletDetails: WalletDetails
    let details: String

    enum CodingKeys: String, CodingKey {
        case chatRoomID = "chatRoomId"
        case access
        case walletDetails
        case details
    }
}

// MARK: - TokenGatedChatAccessType
/// Used to specify token gated chat access type
public enum TokenGatedChatAccessType: String, Decodable {
    case allowed = "allowed"
    case disallowed = "disallowed"
}

// MARK: - WalletDetails
/// Used to give us wallet details
public struct WalletDetails: Decodable {
    /// Web3 wallet address
    public let walletAddress: String
    /// Smart Contract balance
    public let contractBalances: [ContractBalance]?
}

// MARK: - ContractBalance
/// Used to gives us contract balance with respect to wallet address passed
public struct ContractBalance: Decodable {
    /// Smart contract address
    public let contractAddress: String
    /// Boolean value which represents token gate access
    public let gatePassed: Bool
    /// An array of failed attributes that the tokens don't contain
    public let failedAttributes: [String]?
    /// web3 token type
    public let tokenType: BlockChainTokenType
    /// Blockchain platform network
    public let networkType: BlockChainNetworkType
    /// Balance in web3 wallet
    public let balance: Int
    /// NFT metadata
    public let metadata: NFTMetadata?
}

// MARK: - NFTMetadata
public struct NFTMetadata: Decodable {
    /// represents attributes needed for tokens
    public let attributes: [NFTAttribute]?
    /// represents that how many nft user owns with above traits
    public let nftWithAttributesCount: Int?
}

// MARK: - BlockChainTokenType
/// Used to specify blockchain token type
public enum BlockChainTokenType: String, Codable, CaseIterable {
    case nonfungible = "non-fungible"
    case fungible = "fungible"
}

// MARK: - TokenGatedChatAccessInfo
/// A Token Gate can be used to determine access to an SDK functionality
public struct TokenGatedChatAccessInfo: Decodable {
    /// User access for token gated chat room
    public let access: TokenGatedChatAccessType
    /// Gives the detailed message on process
    public let details: String
    /// Chat Room Id
    public let chatRoomID: String
    /// Gives details about web3 wallet and smart contract
    public let walletDetails: WalletDetails?
}

extension TokenGatedChatAccessInfo {
    init(tokenGatedChat: TokenGatedChat) {
        self.init(
            access: tokenGatedChat.access,
            details: tokenGatedChat.details,
            chatRoomID: tokenGatedChat.chatRoomID,
            walletDetails: tokenGatedChat.walletDetails
        )
    }
}
