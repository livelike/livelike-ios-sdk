//
//  ChatRoomMembershipsResult.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

struct ChatRoomMembershipsResult {
    let members: [ChatRoomMember]
    let next: URL?
    let previous: URL?
}
