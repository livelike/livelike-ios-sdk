//
//  ChatRoomMembershipPagination.swift
//  
//
//  Created by Jelzon Monzon on 6/23/23.
//

import Foundation
import LiveLikeCore

/// Represents the different  pagination types that can be passed down to when working with Chat Room Membership
public enum ChatRoomMembershipPagination {
    case first
    case next
    case previous
}

extension ChatRoomMembershipPagination {
    var asPagination: Pagination {
        switch self {
        case .first:
            return .first
        case .previous:
            return .previous
        case .next:
            return .next
        }
    }
}
