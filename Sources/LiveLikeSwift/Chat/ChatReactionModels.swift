//
//  ChatReactionModels.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 9/19/19.
//

import Foundation
import LiveLikeCore

// MARK: - API

/// Represents the reaction pack resource from the backend
public struct ReactionPack: Decodable {
    /// Unique identifier for the Reaction Pack
    public let id: String
    /// Array of Reaction Resources associated with the reaction pack
    public let emojis: [Reaction]
}

/// Represents an array of reaction packs
public struct ReactionPacks: Decodable {
    public let results: [ReactionPack]
}

struct ChatMessageReaction: Decodable {
    let actionTimetoken: String
    let uuid: String
    let userReactionId: String?

    // Function to return the vote ID of a reaction. Backend based implementation returns a `userReactionId` whereas the original implementation through Pubnub returns an `actionTimeToken`. The function is to support backward compatibility with previous versions which aren't using an SDK version that uses backend functionality for reactions.
    func reactionVoteID() -> String {
        return userReactionId ?? actionTimetoken
    }
}

// MARK: Data

/// The id used to identify reactions
public struct ReactionID: Hashable {

    private let internalID: String

    public var asString: String {
        return internalID
    }

    init(fromString id: String) {
        internalID = id
    }
}

extension ReactionID: Equatable {
    public static func == (lhs: ReactionID, rhs: ReactionID) -> Bool {
        return lhs.internalID == rhs.internalID
    }
}

/// Represents a single vote on a reaction
public struct ReactionVote {
    public let voteID: String
    public let reactionID: ReactionID
    public let senderID: String
    public let chatMessageID: String
}

extension ReactionVote {
    public init(userReaction: UserReaction) {
        self.voteID = userReaction.id
        self.reactionID = ReactionID(fromString: userReaction.reactionID)
        self.senderID = userReaction.reactedByID
        self.chatMessageID = userReaction.targetID
    }
}

/// Represents the collection of all reaction votes
public struct ReactionVotes {
    public internal(set) var allVotes: [ReactionVote]

    public var reactionIDs: Set<String> {
        return Set(allVotes.map { $0.reactionID.asString })
    }

    public func voteCount(forID id: String) -> Int {
        return allVotes.filter({$0.reactionID.asString == id}).count
    }

    static let empty: ReactionVotes = ReactionVotes(allVotes: [])
}
