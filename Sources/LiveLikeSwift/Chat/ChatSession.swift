//
//  ChatSession.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 3/6/20.
//

import Foundation
import LiveLikeCore

public protocol ChatSessionDelegate: AnyObject {
    /// Notifies the delegate that a new message was received in the `Chat Session`
    /// - Parameters:
    ///   - chatSession: The `ChatSession` object informing the delegate of this event
    ///   - message: The `ChatMessage` object of the message that was recieved
    func chatSession(_ chatSession: ChatSession, didRecieveNewMessage message: ChatMessage)
    /// Notifies the delegate that the `ChatRoomInfo` was updated in the `Chat Session`
    /// - Parameters:
    ///   - chatSession: The `ChatSession` object informing the delegate of this event
    ///   - chatRoom: The `ChatRoomInfo` object of the Chat Room that was updated
    func chatSession(_ chatSession: ChatSession, didRecieveRoomUpdate chatRoom: ChatRoomInfo)
    /// Notifies the delegate that a  message was deleted in the `Chat Session`
    /// - Parameters:
    ///   - chatSession: The `ChatSession` object informing the delegate of this event
    ///   - messageID: The  unique identifier`ChatMessageID` value of the message that was deleted
    func chatSession(_ chatSession: ChatSession, didDeleteMessage messageID: ChatMessageID)
    /// Notifies the delegate that a message was pinned in the `Chat Session`
    /// - Parameters:
    ///   - chatSession: The `ChatSession` object informing the delegate of this event
    ///   - message: The `ChatMessage` object of the message that was pinned
    func chatSession(_ chatSession: ChatSession, didPinMessage message: PinMessageInfo)
    /// Notifies the delegate that a message was unpinned in the `Chat Session`
    /// - Parameters:
    ///   - chatSession: The `ChatSession` object informing the delegate of this event
    ///   - pinMessageInfoID: The  unique identifier `String` value of the message that was unpinned
    func chatSession(_ chatSession: ChatSession, didUnpinMessage pinMessageInfoID: String)
    
    /// Notifies the delegate that a message has been updated
    /// - Parameters:
    ///   - chatSession: The `ChatSession` object informing the delegate of this event
    ///   - message: The message that was updated
    func chatSession(_ chatSession: ChatSession, didRecieveMessageUpdate message: ChatMessage)
    /// Notifies the delegate that a new message reaction was received in the `Chat Session`
    /// - Parameters:
    ///   - chatSession: The `ChatSession` object informing the delegate of this event
    ///   - reactionVote: The `ReactionVote` object of the message that was recieved
    func chatSession(_ chatSession: ChatSession, didRecieveAddMessageReaction reactionVote: ReactionVote)
    /// Notifies the delegate that a new message reaction was received in the `Chat Session`
    /// - Parameters:
    ///   - chatSession: The `ChatSession` object informing the delegate of this event
    ///   - reactionVote: The `ReactionVote` object of the message that was recieved
    func chatSession(_ chatSession: ChatSession, diRecieveRemoveMessageReaction reactionVote: ReactionVote)
}

/// Default implementations
extension ChatSessionDelegate {
    public func chatSession(_ chatSession: ChatSession, didRecieveAddMessageReaction reactionVote: ReactionVote) { }
    
    public func chatSession(_ chatSession: ChatSession, diRecieveRemoveMessageReaction reactionVote: ReactionVote) { }
    
    public func chatSession(_ chatSession: ChatSession, didRecieveMessageUpdate message: ChatMessage) { }
    
    public func chatSession(_ chatSession: ChatSession, didRecieveRoomUpdate chatRoom: ChatRoomInfo) { }

    public func chatSession(_ chatSession: ChatSession, didRecieveError error: Error) { }

    public func chatSession(_ chatSession: ChatSession, didRecieveNewMessage message: ChatMessage) { }

    public func chatSession(_ chatSession: ChatSession, didDeleteMessage messageID: ChatMessageID) { }

    public func chatSession(_ chatSession: ChatSession, didPinMessage message: PinMessageInfo) { }

    public func chatSession(_ chatSession: ChatSession, didUnpinMessage pinMessageInfoID: String) { }
}

public enum ChatSessionError: LocalizedError {
    case concurrentLoadHistoryCalls
    case invalidPinnedMessagesURLTemplate
    case noHistoryMessages
    
    public var errorDescription: String? {
        switch self {
        case .concurrentLoadHistoryCalls:
            return "Cannot make concurrent calls to `loadNextHistory`. Wait until `loadNextHistory` completes before calling again."
        case .invalidPinnedMessagesURLTemplate:
            return "Invalid Pinned Messages URL Template. Missing Chat Room ID or ordering value."
        case .noHistoryMessages:
            return "No More messages to load from history"
        }
    }
}

/// A connection to a chat room
public protocol ChatSession: AnyObject {
    var title: String? { get }
    var roomID: String { get }
    var userID: String { get }
    
    /// The array of all chat room messages that have been loaded
    ///
    /// The messages are in descending order of TimeToken.
    ///
    /// `messages.first` is the oldest message (if it exists)
    ///
    /// `messages.last` is the most recent message. (if it exists)
    ///
    /// When more messages are loaded from history, they are inserted into the array at index 0. Array access by index is not advised.
    var messages: [ChatMessage] { get }
    
    /// Are chat avatars displayed next to chat messages
    var isAvatarDisplayed: Bool { get }
    
    /// Is URL Highlighting enabled
    var isURLEnabled: Bool { get }
    
    /// Regex for customized validation
    var chatMessageUrlPatterns: String? { get }
    
    /// Set an avatar image to display next to a chat message
    var avatarURL: URL? { get set }
    
    /// Whether content filtered messages are included in `messages` variable
    var containsFilteredMessages: Bool { get }
    
    var isReportingEnabled: Bool { get }
    
    var chatClient: ChatClient { get }
    
    var userClient: UserClient { get }
    
    @available(*, deprecated, message: "Please use `shouldShowIncomingMessages` and `isChatInputVisible` to achieve this")
    func pause()

    @available(*, deprecated, message: "Please use `shouldShowIncomingMessages` and `isChatInputVisible` to achieve this")
    func resume()

    func addDelegate(_ delegate: ChatSessionDelegate)
    func removeDelegate(_ delegate: ChatSessionDelegate)
    
    func getMessages(since timestamp: TimeToken, completion: @escaping (Result<[ChatMessage], Error>) -> Void)
    func getMessageCount(since timestamp: TimeToken, completion: @escaping (Result<Int, Error>) -> Void)
    
    /// Sends a user message.
    ///
    /// - Parameters:
    ///   - chatMessage: An object representing the contents of a chat message
    ///   - completion: A callback indicating whether the message was sent or failed where `ChatMessage` represents
    ///   the newly sent message
    /// - Returns: the newly sent message
    @discardableResult
    func sendMessage(_ chatMessage: NewChatMessage, completion: @escaping (Result<ChatMessage, Error>) -> Void) -> ChatMessage
    
    /// Sends a custom message.
    ///
    /// - Parameters:
    ///   - customData: A String typed parameter which can be used to pass custom data such as JSON
    ///   - completion: A callback indicating whether the message was sent or failed where `ChatMessage` represents
    ///   the newly sent message
    /// - Returns: the newly sent message
    @discardableResult
    func sendCustomMessage(_ customData: String, completion: @escaping (Result<ChatMessage, Error>) -> Void) -> ChatMessage
    
    @available(*, deprecated, message: "Please set property `avatarURL` to update user chat room avatar")
    func updateUserChatRoomImage(url: URL, completion: @escaping (Result<Void, Error>) -> Void)
    
    /// Loads the first  page of history into `messages`.
    /// In most cases the first page will already be loaded, use `loadNextHistory` to get the next  page.
    func loadInitialHistory(completion: @escaping (Result<Void, Error>) -> Void)
    
    /// Loads older chat messages from history.
    /// The loaded messages are inserted into `messages` at index 0.
    /// - Parameter completion: Returns the messages that were loaded.
    func loadNextHistory(completion: @escaping (Result<[ChatMessage], Error>) -> Void)
    
    /// Allows moderators/chatroom creators to pin/focus on a particular message
    ///
    /// - Parameters:
    ///   - message: A ChatMessage typed parameter which can be used to pass the selected message to focus on.
    ///   - completion: A callback indicating whether the message was pinned or failed where `PinMessageInfo` represents the focused message
    ///
    func pinMessage(_ message: ChatMessage, completion: @escaping (Result<PinMessageInfo, Error>) -> Void)
    
    /// Allows moderators/chatroom creators to unpin a previously pinned message
    ///
    /// - Parameters:
    ///   - pinMessageInfoID: A String typed parameter which can be used to pass the id of the `PinMessageInfo` object of the message to be unpinned.
    ///
    func unpinMessage(pinMessageInfoID: String, completion: @escaping (Result<Bool, Error>) -> Void)
    
    /// Returns a list of messages that have been pinned
    ///
    /// - Parameters:
    ///   - orderBy: A `Ordering` typed parameter which can be used to pass listing order for PinMessageInfo Objects (can be either `asc` (ascending) or `desc` (descending)
    ///   - page: a `Pagination` typed parameter passed to allow paginated responses
    ///   - completion: A callback that receives the list of `PinMessageInfo` objects for messages that were pinned
    ///
    func getPinMessageInfoList(orderBy: Ordering, page: Pagination, completion: @escaping (Result<[PinMessageInfo], Error>) -> Void)
    
    /// Allows the user message to quote a message.
    ///
    /// - Parameters:
    ///   - chatMessage: An object representing the contents of a chat message
    ///   - quoteMessage: An object representing the contents of the quoted message.
    ///   - completion: A callback indicating whether the message was sent or failed where `ChatMessage` represents
    ///   the newly sent message
    /// - Returns: the newly sent message and the quoted message.
    @discardableResult
    func quoteMessage(_ chatMessage: NewChatMessage, quoteMessage: ChatMessagePayload, completion: @escaping (Result<ChatMessage, Error>) -> Void) -> ChatMessage
    
    /// Allows the user to delete a message
    ///
    /// - Parameters:
    ///  - message: A `ChatMessage` type parameter that represents the `ChatMessage` to be deleted.
    func deleteMessage(message: ChatMessage, completion: @escaping (Result<Bool, Error>) -> Void)
    
    func deleteMessage(
        options: DeleteMessageRequestOptions, 
        completion: @escaping (Result<Bool, Error>) -> Void
    )
    
    /// Returns the sticker packs associted to the Chat room
    func getStickerPacks(
        completion: @escaping (Result<[StickerPack], Error>) -> Void
    )
    
    /// Returns the reactions associted to the Chat room
    func getReactions(
        completion: @escaping (Result<[Reaction], Error>) -> Void
    )
    
    /// Reports a message for moderation
    /// - Parameters:
    ///   - messageID: The id of the message to report
    func reportMessage(
        messageID: String,
        completion: @escaping (Result<Void, Error>) -> Void
    )
    
    /// Adds a reaction to a message
    /// - Parameters:
    ///   - messageID: The id of the message to react
    ///   - reaction: The id of the reaction
    func addMessageReaction(
        messageID: String,
        reaction: String,
        completion: @escaping (Result<Void, Error>) -> Void
    )
    
    /// Removes a reaction from a message
    /// - Parameters:
    ///   - reactionVoteID: The id of the reaction vote
    ///   - messageID: The id of the message
    func removeMessageReaction(
        reactionVoteID: String,
        messageID: String,
        completion: @escaping (Result<Void, Error>) -> Void
    )
    
    /// Disconnects from messaging client. If this method is invoked,
    /// the current user will be invalidated.
    func disconnect()
    
    /// Adds a reaction to a message
    /// - Parameters:
    ///   - messageID: The id of the message to react
    ///   - reaction: The id of the reaction
    func addMessageReaction(
        messageID: String,
        reactionID: String,
        completion: @escaping (Result<ReactionVote, Error>) -> Void
    )
}
