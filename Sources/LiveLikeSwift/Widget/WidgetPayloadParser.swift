//
//  WidgetPayloadParser.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 4/8/20.
//

import Foundation
import LiveLikeCore

struct WidgetPayloadParser {

    private init() { }

    static func parse(_ jsonObject: Any) throws -> WidgetResource {
        let jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: [])
        let decoder = LLJSONDecoder()
        return try decoder.decode(WidgetResource.self, from: jsonData)
    }
}
