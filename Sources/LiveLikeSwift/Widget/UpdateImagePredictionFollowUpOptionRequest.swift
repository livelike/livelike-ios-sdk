//
//  UpdateImagePredictionFollowUpOptionRequest.swift
//  
//
//  Created by Jelzon Monzon on 8/8/23.
//

import Foundation
import LiveLikeCore

struct UpdateImagePredictionFollowUpOptionRequest: LLRequest {

    private let networking: LLNetworking
    private let accessToken: String
    private let isCorrect: Bool?

    init(
        networking: LLNetworking,
        accessToken: String,
        isCorrect: Bool?
    ) {
        self.networking = networking
        self.accessToken = accessToken
        self.isCorrect = isCorrect
    }

    func execute(
        url: URL,
        completion: @escaping (Result<ImagePredictionFollowUpOption, Error>) -> Void
    ) {
        struct PredictionOptionPatch: Encodable {
            let isCorrect: Bool?
        }
        let resource = Resource<ImagePredictionFollowUpOption>(
            url: url,
            method: .patch(PredictionOptionPatch(isCorrect: isCorrect)),
            accessToken: accessToken
        )
        networking.task(resource, completion: completion)
    }
}
