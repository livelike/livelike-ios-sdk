//
//  MessagingServiceAPI.swift
//  LiveLikeSDK
//
//  Created by Cory Sullivan on 2019-01-21.
//

import PubNub
import Foundation
import LiveLikeCore

/// Concrete implementation of `WidgetMessagingClient`
/// based on [PubNub](https://www.pubnub.com/docs/ios-objective-c/pubnub-objective-c-sdk)
class InternalPubSubWidgetClient: PubSubWidgetClient {
    /// Internal
    var widgetListeners = ChannelListeners()

    /// Private
    private let subscribeKey: String
    private let origin: String?
    private let userID: String
    private let heartbeatInterval: Int
    private let heartbeatTimeout: Int

    private let messagingSerialQueue = DispatchQueue(label: "com.livelike.pubnub.widget")
    private let listener: SubscriptionListener

    private let client: PubNub

    init(
        subscribeKey: String,
        origin: String?,
        userID: String,
        heartbeatTimeout: Int,
        heartbeatInterval: Int
    ) {
        self.subscribeKey = subscribeKey
        self.origin = origin
        self.userID = userID
        self.heartbeatTimeout = heartbeatTimeout
        self.heartbeatInterval = heartbeatInterval

        log.info("PubNub is being initialized with userID '\(userID)' and subscribeKey '\(subscribeKey)'.")
        var config = PubNubConfiguration(
            publishKey: nil,
            subscribeKey: self.subscribeKey,
            userId: userID
        )
        if let origin = origin {
            config.origin = origin
        }
        // Configure Presence heartbeat
        config.heartbeatInterval = UInt(heartbeatInterval)
        config.durationUntilTimeout = UInt(heartbeatTimeout)
        config.automaticRetry = AutomaticRetry(
            retryLimit: 9,
            policy: .exponential(minDelay: 3, maxDelay: 60)
        )

        client = PubNub(configuration: config)

        listener = SubscriptionListener(
            queue: DispatchQueue(label: "com.livelike.pubnub.widget")
        )

        listener.didReceiveMessage = { [weak self] message in
            guard let self = self else { return }

            // Unsubscribe from the channel if there are no listeners
            guard !self.widgetListeners.isEmpty(forChannel: message.channel) else {
                self.client.unsubscribe(from: [message.channel])
                return
            }

            do {
                let clientEvent = try self.processMessage(message: message)
                self.widgetListeners.publish(channel: message.channel) {
                    $0.publish(event: WidgetProxyPublishData(clientEvent: clientEvent))
                }
            } catch {
                log.error(error)
                self.widgetListeners.publish(channel: message.channel) { $0.error(error) }
            }
        }

        listener.didReceiveStatus = { (status: SubscriptionListener.StatusEvent) in
            log.info("[InternalPubSubWidgetClient] PubNub Status: \(status)")
        }

        client.add(listener)
    }

    func addListener(_ listener: WidgetProxyInput, toChannel channel: String) {
        widgetListeners.addListener(listener, forChannel: channel)
        client.subscribe(to: [channel], withPresence: false)
        log.debug("[PubNub] Connected to PubNub channel. Connected to \(client.subscriptionCount) channels.")
    }

    func removeListener(_ listener: WidgetProxyInput, fromChannel channel: String) {
        widgetListeners.removeListener(listener, forChannel: channel)
        unsubscribe(fromChannel: channel)
    }

    /// Unsubscribes from a channel if there are no more listeners
    func unsubscribe(fromChannel channel: String) {
        if widgetListeners.isEmpty(forChannel: channel) {
            client.unsubscribe(from: [channel])
            log.debug("[PubNub] Disconnected from PubNub channel. Connected to \(client.subscriptionCount) channels.")
        }
    }

    func removeAllListeners() {
        widgetListeners.removeAll()
        client.unsubscribeAll()
    }
}

private extension InternalPubSubWidgetClient {
    /// We are expecting all messages to have the following schema
    ///
    /// ```
    ///    {
    ///      event = "the_event_name" // predefined `EventName`
    ///      payload = {
    ///       ...
    ///      }
    ///    }
    /// ```
    func processMessage(message: PubNubMessage) throws -> ClientEvent {
        guard let message = (message.payload as? AnyJSON)?.dictionaryOptional else {
            throw MessagingClientError.invalidEvent(event: "The message is empty")
        }
        let jsonData = try JSONSerialization.data(withJSONObject: message, options: .prettyPrinted)
        let decoder = LLJSONDecoder()
        return try decoder.decode(ClientEvent.self, from: jsonData)
    }
}
