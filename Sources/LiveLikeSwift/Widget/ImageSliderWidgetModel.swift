//
//  ImageSliderWidgetModel.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 7/2/20.
//

import Foundation
import LiveLikeCore

/// Methods for observing changes to a `ImageSliderWidgetModel`
public protocol ImageSliderWidgetModelDelegate: AnyObject {
    /// Called when the server sends updated average magnitude
    func imageSliderWidgetModel(
        _ model: ImageSliderWidgetModel,
        averageMagnitudeDidChange averageMagnitude: Double
    )
}

/// An object that reflects the state of a Image Slider widget on the server
public class ImageSliderWidgetModel: BaseWidgetModel, ImageSliderWidgetModelable {

    /// The object that acts as the delegate for the `ImageSliderWidgetModel`
    public weak var delegate: ImageSliderWidgetModelDelegate?

    // MARK: Data

    /// The question posed by the Image Slider widget
    public let question: String
    /// An initial magnitude set by a Producer
    public let initialMagnitude: Double
    /// The latest average magnitude
    @Atomic public internal(set) var averageMagnitude: Double
    /// The options of the Image Slider widget
    public let options: [Option]
    /// The user's votes on this Image Slider
    /// Call `loadInteractionHistory` to fetch votes before using
    public var userVotes: [Vote] {
        return self.widgetInteractionRepo.get(widgetID: self.id)?.imageSliderVotes ?? []
    }
    /// The user's vote with the most recent createdAt date
    public var mostRecentVote: Vote? {
        return userVotes.max(by: { $0.createdAt <= $1.createdAt })
    }
    /// Sponsors linked to the Image Slider Widget
    public let sponsors: [Sponsor]

    /// The number of unique votes
    public let engagementCount: Int

    // MARK: Private Properties

    private let rewardItems: [RewardItem]
    private let leaderboardManager: LeaderboardsManager
    private let voteURL: URL
    private let widgetClient: PubSubWidgetClient?
    private let widgetInteractionRepo: WidgetInteractionRepository

    let interactionURL: URL?

    init(
        data: ImageSliderCreated,
        eventRecorder: EventRecorder,
        userProfile: UserProfileProtocol,
        rewardItems: [RewardItem],
        leaderboardManager: LeaderboardsManager,
        livelikeAPI: LiveLikeWidgetAPIProtocol,
        widgetClient: PubSubWidgetClient?,
        widgetInteractionRepo: WidgetInteractionRepository
    ) {
        self.question = data.question
        self.voteURL = data.voteUrl
        self.initialMagnitude = Double(data.initialMagnitude) ?? 0
        self.options = data.options.map {
            Option(id: $0.id, imageURL: $0.imageUrl)
        }
        self.averageMagnitude = {
            guard
                let averageMagnitudeString = data.averageMagnitude,
                let averageMagnitude = Double(averageMagnitudeString)
            else {
                return 0.5
            }
            return averageMagnitude
        }()
        self.rewardItems = rewardItems
        self.leaderboardManager = leaderboardManager
        self.widgetClient = widgetClient
        self.interactionURL = try? WidgetResource.getWidgetInteractionsURL(
            fromTemplate: data.widgetInteractionsUrlTemplate,
            profileID: userProfile.userID.asString
        )
        self.widgetInteractionRepo = widgetInteractionRepo
        self.sponsors = data.sponsors
        self.engagementCount = data.engagementCount
        super.init(
            baseData: data,
            eventRecorder: eventRecorder,
            livelikeAPI: livelikeAPI,
            userProfile: userProfile
        )

        if let subscribeChannel = subscribeChannel {
            self.widgetClient?.addListener(self, toChannel: subscribeChannel)
        }
    }

    // MARK: Methods

    /// Call this to load the user's interaction history for this Widget
    /// - Parameter completion: Returns the loaded votes
    public func loadInteractionHistory(completion: @escaping (Result<[Vote], Error>) -> Void = { _ in }) {
        self.widgetInteractionRepo.get(widgetModel: .imageSlider(self)) { result in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let interactions):
                completion(.success(interactions.imageSliderVotes))
            }
        }
    }

    /// Locks in a vote for the ImageSlider. A user can only have one vote so call this when the user has made their final decision.
    /// Magnitude must by within range [0,1]
    public func lockInVote(magnitude: Double, completion: @escaping (Result<Vote, Error>) -> Void = { _ in }) {
        self.eventRecorder.record(
            .widgetEngaged(widgetModel: .imageSlider(self))
        )
        firstly {
            self.livelikeAPI.createImageSliderVote(
                voteURL: self.voteURL,
                magnitude: magnitude,
                accessToken: userProfile.accessToken
            )
        }.then { voteResource in
            self.widgetInteractionRepo.add(interactions: [.imageSliderVote(voteResource)])
            self.leaderboardManager.notifyCurrentPositionChange(rewards: voteResource.rewards)
            completion(.success(voteResource))
        }.catch { error in
            completion(.failure(error))
        }
    }

    // MARK: Types

    /// An option of the Image Slider
    public struct Option {
        /// The option's id
        public let id: String
        /// The URL of an image representing the Option
        public let imageURL: URL

        init(id: String, imageURL: URL) {
            self.id = id
            self.imageURL = imageURL
        }
    }

    /// An object representing a vote on an Image Slider
    public struct Vote: Decodable {
        public let id: String
        /// The date that this Vote was created
        public let createdAt: Date
        /// The magnitude of the Vote [0, 1]
        public let magnitude: Double

        let widgetID: String
        let rewards: [RewardResource]
        let widgetKind: WidgetKind

        enum CodingKeys: String, CodingKey {
            case id
            case widgetId
            case magnitude
            case rewards
            case createdAt
            case widgetKind
        }

        public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.id = try container.decode(String.self, forKey: .id)
            self.widgetID = try container.decode(String.self, forKey: .widgetId)
            self.rewards = try container.decodeIfPresent([RewardResource].self, forKey: .rewards) ?? []
            self.createdAt = try container.decode(Date.self, forKey: .createdAt)
            let magnitudeString = try container.decode(String.self, forKey: .magnitude)
            if let magnitude = Double(magnitudeString) {
                self.magnitude = magnitude
            } else {
                throw DecodingError.dataCorruptedError(
                    forKey: .magnitude,
                    in: container,
                    debugDescription: "Failed to decode \(magnitudeString) as a Double."
                )
            }
            self.widgetKind = try container.decode(WidgetKind.self, forKey: .widgetKind)
        }
    }
}

/// :nodoc:
extension ImageSliderWidgetModel: WidgetProxyInput {
    func publish(event: WidgetProxyPublishData) {
        switch event.clientEvent {
        case let .imageSliderResults(results):
            guard results.id == self.id else { return }
            guard
                let averageMagnitudeString = results.averageMagnitude,
                let averageMagnitude = Double(averageMagnitudeString)
            else {
                return
            }
            self.averageMagnitude = averageMagnitude
            self.delegate?.imageSliderWidgetModel(self, averageMagnitudeDidChange: averageMagnitude)
        default:
            log.verbose("Received event \(event.clientEvent.description) in ImageSliderLiveResultsClient when only .imageSliderResults were expected.")
        }
    }

    func discard(event: WidgetProxyPublishData, reason: DiscardedReason) {}
    func connectionStatusDidChange(_ status: ConnectionStatus) {}

    func error(_ error: Error) {
        log.error(error.localizedDescription)
    }
}
