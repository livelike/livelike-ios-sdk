//
//  WidgetStatus.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

/// A status/state a widget can have
public enum WidgetStatus: String, Decodable, CaseIterable {
    case published
    case pending
    case scheduled
}
