//
//  CreateImagePrediction.swift
//  
//
//  Created by Jelzon Monzon on 8/3/23.
//

import Foundation

public struct CreateImagePredictionRequestOptions {

    public init(
        common: CommonCreateWidgetOptions,
        question: String,
        choices: [Choice]
    ) {
        self.common = common
        self.question = question
        self.choices = choices
    }

    public let common: CommonCreateWidgetOptions
    public let question: String
    public let choices: [Choice]

    public struct Choice {
        public init(text: String, imageURL: URL) {
            self.text = text
            self.imageURL = imageURL
        }

        public let text: String
        public let imageURL: URL
    }
}

extension CreateImagePredictionRequestOptions: Encodable {
    enum CodingKeys: String, CodingKey {
        case question
        case options
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(question, forKey: .question)
        try container.encode(choices, forKey: .options)
        try common.encode(to: encoder)
    }
}

extension CreateImagePredictionRequestOptions.Choice: Encodable {
    enum CodingKeys: String, CodingKey {
        case text = "description"
        case imageURL = "imageUrl"
    }
}
