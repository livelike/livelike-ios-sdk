//
//  CreateAlertRequestOptions.swift
//  
//
//  Created by Jelzon Monzon on 8/3/23.
//

import Foundation

public struct CreateAlertRequestOptions {
    /// - Parameters:
    ///   - title: The optional title of the Alert
    ///   - text: The optional text of the Alert
    ///   - imageURL: The optional url of the image of the Alert
    ///   - linkLabel: The optional text of the link of the Alert
    ///   - linkURL: The optional url of the link of the Alert
    public init(
        common: CommonCreateWidgetOptions,
        title: String? = nil,
        text: String? = nil,
        imageURL: URL? = nil,
        linkLabel: String? = nil,
        linkURL: URL? = nil
    ) {
        self.common = common
        self.title = title
        self.text = text
        self.imageURL = imageURL
        self.linkLabel = linkLabel
        self.linkURL = linkURL
    }

    public let common: CommonCreateWidgetOptions

    public let title: String?
    public let text: String?
    public let imageURL: URL?
    public let linkLabel: String?
    public let linkURL: URL?
}

extension CreateAlertRequestOptions: Encodable {
    enum CodingKeys: String, CodingKey {
        case title
        case text
        case imageURL = "imageUrl"
        case linkLabel
        case linkURL = "linkUrl"
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(title, forKey: .title)
        try container.encodeIfPresent(text, forKey: .text)
        try container.encodeIfPresent(imageURL, forKey: .imageURL)
        try container.encodeIfPresent(linkLabel, forKey: .linkLabel)
        try container.encodeIfPresent(linkURL, forKey: .linkURL)
        try common.encode(to: encoder)
    }
}
