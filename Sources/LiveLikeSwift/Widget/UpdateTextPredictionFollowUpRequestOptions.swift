//
//  UpdateTextPredictionFollowUpRequestOptions.swift
//  
//
//  Created by Jelzon Monzon on 8/11/23.
//

import Foundation

public class UpdateTextPredictionFollowUpRequestOptions {
    public let predictionID: String
    public let optionID: String
    public let isCorrect: Bool?

    /// Options for updating an Option of a Prediction Follow Up Widget
    /// - Parameters:
    ///   - predictionID: The id of the Prediction associated with the Follow Up
    ///   - optionID: The id of the Option of the Prediction or Follow Up
    ///   - isCorrect: Updates the isCorrect property of the Prediction Follow Up Option
    public init(
        predictionID: String,
        optionID: String,
        isCorrect: Bool?
    ) {
        self.predictionID = predictionID
        self.optionID = optionID
        self.isCorrect = isCorrect
    }
}
