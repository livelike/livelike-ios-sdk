//
//  WidgetOrdering.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

/// Possible order of widgets result set
public enum WidgetOrdering: String, Decodable, CaseIterable {
    case recent
    case oldest
}
