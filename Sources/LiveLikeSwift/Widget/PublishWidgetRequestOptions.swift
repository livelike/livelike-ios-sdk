//
//  ScheduleWidgetRequestOptions.swift
//  
//
//  Created by Jelzon Monzon on 8/7/23.
//

import Foundation

public struct PublishWidgetRequestOptions {
    public let kind: WidgetKind
    public let id: String
    public let publishDelaySeconds: UInt?
    public let programDateTime: Date?

    /// - Parameters:
    ///   - kind: The WidgetKind of the Widget to Publish
    ///   - id: The id of the Widget to Publish
    ///   - publishDelaySeconds: An optional delay in seconds that this widget should be delayed to be published
    ///   - programDateTime: An optional PDT used for video synchronization
    public init(
        kind: WidgetKind,
        id: String,
        publishDelaySeconds: UInt? = nil,
        programDateTime: Date? = nil
    ) {
        self.kind = kind
        self.id = id
        self.publishDelaySeconds = publishDelaySeconds
        self.programDateTime = programDateTime
    }
}
