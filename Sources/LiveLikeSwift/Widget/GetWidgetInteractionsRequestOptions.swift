//
//  GetWidgetInteractionsRequestOptions.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

/// Used to query widget interactions
public struct GetWidgetInteractionsRequestOptions {
    // Is an array of `URLQueryItem` used to compile URL parameters
    let urlQueries: [URLQueryItem]

    let widgetKind: [WidgetKind]

    public init(
        widgetKind: [WidgetKind] = []
    ) {
        self.widgetKind = widgetKind

        self.urlQueries = {
            var urlQueries = [URLQueryItem]()
            widgetKind.forEach {
                urlQueries.append(
                    URLQueryItem(name: "widget_kind", value: $0.stringValue)
                )
            }

            return urlQueries
        }()
    }
}
