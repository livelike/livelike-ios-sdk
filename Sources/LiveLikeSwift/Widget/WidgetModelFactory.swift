//
//  WidgetModelFactory.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 10/26/20.
//

import Foundation

protocol PredictionWidgetModelFactoryProtocol {
    func makePredictionFollowUpModel(resource: TextPredictionFollowUp) -> PredictionFollowUpWidgetModel
    func makePredictionFollowUpModel(resource: ImagePredictionFollowUp) -> PredictionFollowUpWidgetModel
    func makePredictionFollowUpModel(resource: NumberPredictionFollowUp) -> NumberPredictionFollowUpWidgetModel
}

class WidgetModelFactory: PredictionWidgetModelFactoryProtocol {
    private let eventRecorder: EventRecorder
    private let userProfile: UserProfile
    private let leaderboardsManager: LeaderboardsManager
    private let widgetClient: PubSubWidgetClient?
    private let widgetAPI: LiveLikeWidgetAPIProtocol
    private let leaderboardAPI: LiveLikeLeaderboardAPIProtocol
    private let rewardItems: [RewardItem]
    private let predictionVoteRepo: PredictionVoteRepository
    private let widgetInteractionRepo: WidgetInteractionRepository

    init(
        eventRecorder: EventRecorder,
        userProfile: UserProfile,
        rewardItems: [RewardItem],
        leaderboardsManager: LeaderboardsManager,
        widgetClient: PubSubWidgetClient?,
        widgetAPI: LiveLikeWidgetAPIProtocol,
        leaderboardAPI: LiveLikeLeaderboardAPIProtocol,
        predictionVoteRepo: PredictionVoteRepository,
        widgetInteractionRepo: WidgetInteractionRepository
    ) {
        self.eventRecorder = eventRecorder
        self.userProfile = userProfile
        self.rewardItems = rewardItems
        self.leaderboardsManager = leaderboardsManager
        self.widgetClient = widgetClient
        self.widgetAPI = widgetAPI
        self.leaderboardAPI = leaderboardAPI
        self.predictionVoteRepo = predictionVoteRepo
        self.widgetInteractionRepo = widgetInteractionRepo
    }

    func make(from widgetResource: WidgetResource, programSubscribeChannel: String?) throws -> WidgetModel {
        switch widgetResource {
        case .numberPredictionFollowUp(let payload):
            return .numberPredictionFollowUp(
                self.makePredictionFollowUpModel(resource: payload)
            )
        case .numberPrediction(let payload):
            let model = makeNumberPrediction(
                data: payload,
                programSubscribeChannel: programSubscribeChannel
            )
            return .numberPrediction(model)
        case .cheerMeterCreated(let payload):
            let cheerMeterModel = CheerMeterWidgetModel(
                data: payload,
                userProfile: userProfile,
                rewardItems: rewardItems,
                leaderboardManager: leaderboardsManager,
                livelikeAPI: widgetAPI,
                widgetClient: widgetClient,
                eventRecorder: eventRecorder,
                widgetInteractionRepo: self.widgetInteractionRepo
            )
            return .cheerMeter(cheerMeterModel)
        case .alertCreated(let payload):
            return .alert(self.makeAlertModel(data: payload))
        case .textQuizCreated(let payload):
            let model = QuizWidgetModel(
                data: payload,
                eventRecorder: self.eventRecorder,
                userProfile: self.userProfile,
                rewardItems: rewardItems,
                leaderboardsManager: self.leaderboardsManager,
                widgetClient: self.widgetClient,
                livelikeAPI: self.widgetAPI,
                widgetInteractionRepo: self.widgetInteractionRepo
            )
            return .quiz(model)
        case .imageQuizCreated(let payload):
            let model = QuizWidgetModel(
                data: payload,
                eventRecorder: self.eventRecorder,
                userProfile: self.userProfile,
                rewardItems: rewardItems,
                leaderboardsManager: self.leaderboardsManager,
                widgetClient: self.widgetClient,
                livelikeAPI: self.widgetAPI,
                widgetInteractionRepo: self.widgetInteractionRepo
            )
            return .quiz(model)
        case .textPredictionCreated(let payload):
            return .prediction(self.makePredictionModel(
                data: payload,
                programSubscribeChannel: programSubscribeChannel
            ))
        case .imagePredictionCreated(let payload):
            return .prediction(self.makePredictionModel(
                data: payload,
                programSubscribeChannel: programSubscribeChannel
            ))
        case .textPredictionFollowUp(let payload):
            return .predictionFollowUp(self.makePredictionFollowUpModel(resource: payload))
        case .imagePredictionFollowUp(let payload):
            return .predictionFollowUp(self.makePredictionFollowUpModel(resource: payload))
        case .textPollCreated(let payload):
            return .poll(self.makePollModel(data: payload))
        case .imagePollCreated(let payload):
            return .poll(self.makePollModel(data: payload))
        case .imageSliderCreated(let payload):
            let model = ImageSliderWidgetModel(
                data: payload,
                eventRecorder: self.eventRecorder,
                userProfile: self.userProfile,
                rewardItems: self.rewardItems,
                leaderboardManager: self.leaderboardsManager,
                livelikeAPI: self.widgetAPI,
                widgetClient: self.widgetClient,
                widgetInteractionRepo: self.widgetInteractionRepo
            )
            return .imageSlider(model)
        case .socialEmbed(let payload):
            let model = SocialEmbedWidgetModel(
                resource: payload,
                eventRecorder: self.eventRecorder,
                livelikeAPI: self.widgetAPI,
                userProfile: self.userProfile
            )
            return .socialEmbed(model)
        case .videoAlert(let payload):
            let model = VideoAlertWidgetModel(
                resource: payload,
                eventRecorder: self.eventRecorder,
                widgetAPI: self.widgetAPI,
                userProfile: self.userProfile
            )
            return .videoAlert(model)
        case .textAsk(let payload):
            let model = makeTextAsk(data: payload)
            return .textAsk(model)
        case .richPost(let payload):
            let model = makeRichPost(data: payload)
            return .richPost(model)
        }
    }

    func make(from widgetResources: [WidgetResource], programSubscribeChannel: String) -> [WidgetModel] {
        return widgetResources.compactMap { try? self.make(from: $0, programSubscribeChannel: programSubscribeChannel) }
    }

    func makePredictionFollowUpModel(resource: TextPredictionFollowUp) -> PredictionFollowUpWidgetModel {
        let model = PredictionFollowUpWidgetModel(
            resource: resource,
            eventRecorder: self.eventRecorder,
            livelikeAPI: self.widgetAPI,
            leaderboardAPI: self.leaderboardAPI,
            rewardItems: self.rewardItems,
            leaderboardsManager: self.leaderboardsManager,
            userProfile: self.userProfile,
            voteRepo: self.predictionVoteRepo,
            widgetInteractionRepo: self.widgetInteractionRepo
        )
        return model
    }

    func makePredictionFollowUpModel(resource: ImagePredictionFollowUp) -> PredictionFollowUpWidgetModel {
        let model = PredictionFollowUpWidgetModel(
            resource: resource,
            eventRecorder: self.eventRecorder,
            livelikeAPI: self.widgetAPI,
            leaderboardAPI: self.leaderboardAPI,
            rewardItems: self.rewardItems,
            leaderboardsManager: self.leaderboardsManager,
            userProfile: self.userProfile,
            voteRepo: self.predictionVoteRepo,
            widgetInteractionRepo: self.widgetInteractionRepo
        )
        return model
    }

    func makePredictionFollowUpModel(resource: NumberPredictionFollowUp) -> NumberPredictionFollowUpWidgetModel {
        let model = InternalNumberPredictionFollowUpWidgetModel(
            resource: resource,
            eventRecorder: self.eventRecorder,
            widgetAPI: self.widgetAPI,
            leaderboardAPI: self.leaderboardAPI,
            userProfile: self.userProfile,
            widgetInteractionRepo: self.widgetInteractionRepo,
            leaderboardsManager: self.leaderboardsManager,
            rewardItems: self.rewardItems
        )
        return model
    }

    func makePollModel(data: TextPollCreated) -> PollWidgetModel {
        let model = PollWidgetModel(
            data: data,
            userProfile: self.userProfile,
            rewardItems: self.rewardItems,
            leaderboardManager: self.leaderboardsManager,
            livelikeAPI: self.widgetAPI,
            widgetClient: self.widgetClient,
            eventRecorder: self.eventRecorder,
            widgetInteractionRepo: self.widgetInteractionRepo
        )
        return model
    }

    func makePollModel(data: ImagePollCreated) -> PollWidgetModel {
        let model = PollWidgetModel(
            data: data,
            userProfile: self.userProfile,
            rewardItems: self.rewardItems,
            leaderboardManager: self.leaderboardsManager,
            livelikeAPI: self.widgetAPI,
            widgetClient: self.widgetClient,
            eventRecorder: self.eventRecorder,
            widgetInteractionRepo: self.widgetInteractionRepo
        )
        return model
    }

    func makeAlertModel(data: AlertCreated) -> AlertWidgetModel {
        return AlertWidgetModel(
            data: data,
            eventRecorder: self.eventRecorder,
            livelikeAPI: self.widgetAPI,
            userProfile: self.userProfile
        )
    }

    func makePredictionModel(
        data: TextPredictionCreated,
        programSubscribeChannel: String?
    ) -> PredictionWidgetModel {
        return PredictionWidgetModel(
            resource: data,
            eventRecorder: self.eventRecorder,
            userProfile: self.userProfile,
            rewardItems: self.rewardItems,
            voteRepo: self.predictionVoteRepo,
            leaderboardManager: self.leaderboardsManager,
            livelikeAPI: self.widgetAPI,
            widgetClient: self.widgetClient,
            widgetInteractionRepo: self.widgetInteractionRepo,
            programSubscribeChannel: programSubscribeChannel,
            followUpModelFactory: self
        )
    }

    func makePredictionModel(
        data: ImagePredictionCreated,
        programSubscribeChannel: String?
    ) -> PredictionWidgetModel {
        return PredictionWidgetModel(
            resource: data,
            eventRecorder: self.eventRecorder,
            userProfile: self.userProfile,
            rewardItems: self.rewardItems,
            voteRepo: self.predictionVoteRepo,
            leaderboardManager: self.leaderboardsManager,
            livelikeAPI: self.widgetAPI,
            widgetClient: self.widgetClient,
            widgetInteractionRepo: self.widgetInteractionRepo,
            programSubscribeChannel: programSubscribeChannel,
            followUpModelFactory: self
        )
    }

    func makeTextAsk(data: TextAskWidgetResource) -> TextAskWidgetModel {
        return TextAskWidgetModel(
            resource: data,
            eventRecorder: self.eventRecorder,
            widgetAPI: self.widgetAPI,
            userProfile: self.userProfile,
            widgetInteractionRepo: self.widgetInteractionRepo
        )
    }

    func makeQuizModel(
        data: TextQuizCreated
    ) -> QuizWidgetModel {
        return QuizWidgetModel(
            data: data,
            eventRecorder: self.eventRecorder,
            userProfile: self.userProfile,
            rewardItems: rewardItems,
            leaderboardsManager: self.leaderboardsManager,
            widgetClient: self.widgetClient,
            livelikeAPI: self.widgetAPI,
            widgetInteractionRepo: self.widgetInteractionRepo
        )
    }

    func makeQuizModel(
        data: ImageQuizCreated
    ) -> QuizWidgetModel {
        return QuizWidgetModel(
            data: data,
            eventRecorder: self.eventRecorder,
            userProfile: self.userProfile,
            rewardItems: rewardItems,
            leaderboardsManager: self.leaderboardsManager,
            widgetClient: self.widgetClient,
            livelikeAPI: self.widgetAPI,
            widgetInteractionRepo: self.widgetInteractionRepo
        )
    }

    func makeRichPost(data: RichPost) -> RichPostWidgetModel {
        return RichPostWidgetModel(
            richPost: data,
            eventRecorder: self.eventRecorder,
            livelikeAPI: self.widgetAPI,
            userProfile: self.userProfile
        )
    }

    func makeNumberPrediction(
        data: NumberPrediction, 
        programSubscribeChannel: String?
    ) -> NumberPredictionWidgetModel {
        let model = InternalNumberPredictionWidgetModel(
            resource: data,
            eventRecorder: eventRecorder,
            widgetAPI: widgetAPI,
            userProfile: userProfile,
            widgetInteractionRepo: widgetInteractionRepo,
            followUpModelFactory: self,
            widgetClient: widgetClient,
            programSubscribeChannel: programSubscribeChannel
        )
        return model
    }

    enum Errors: LocalizedError {
        case unsupportedWidget

        var errorDescription: String? {
            switch self {
            case .unsupportedWidget:
                return "Failed to create a widget model because the widget kind is not supported"
            }
        }
    }

}
