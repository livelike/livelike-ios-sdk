//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation


public struct CreateTextAskWidgetOptions {

    /// Options for creating a Text Ask Widget
    /// - Parameters:
    ///   - common: Options that are common between all Widgets
    ///   - title: The title of the Text Ask widget
    ///   - prompt: A prompt of the Text Ask widget for what users should ask about
    ///   - confirmationMessage: An optional confirmation message to be displayed after the users submits their response
    public init(
        common: CommonCreateWidgetOptions,
        title: String,
        prompt: String,
        confirmationMessage: String? = nil
    ) {
        self.common = common
        self.title = title
        self.prompt = prompt
        self.confirmationMessage = confirmationMessage
    }

    public let common: CommonCreateWidgetOptions
    public let title: String
    public let prompt: String
    public let confirmationMessage: String?

}

extension CreateTextAskWidgetOptions: Encodable {
    enum CodingKeys: String, CodingKey {
        case title
        case prompt
        case confirmationMessage
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(title, forKey: .title)
        try container.encode(prompt, forKey: .prompt)
        try container.encodeIfPresent(confirmationMessage, forKey: .confirmationMessage)
        try common.encode(to: encoder)
    }
}
