//
//  LiveLikeWidgetAPIs.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 5/20/21.
//

import Foundation
import LiveLikeCore

protocol LiveLikeWidgetAPIProtocol {
    /// Get widget details of a widget
    func getWidget(id: String, kind: WidgetKind) -> Promise<WidgetResource>
    func getWidgetResources(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<WidgetResource>>
    func createCheerMeterVote(voteCount: Int, voteURL: URL, accessToken: AccessToken) -> Promise<CheerMeterWidgetModel.Vote>
    func updateCheerMeterVote(voteCount: Int, voteURL: URL, accessToken: AccessToken) -> Promise<CheerMeterWidgetModel.Vote>

    func getTimeline(timelineURL: URL, accessToken: AccessToken) -> Promise<WidgetTimelineResource>

    func createImpression(impressionURL: URL, userSessionID: String, accessToken: AccessToken) -> Promise<ImpressionResponse>

    func createQuizAnswer(answerURL: URL, accessToken: AccessToken) -> Promise<QuizWidgetModel.Answer>

    func createPredictionVote(voteURL: URL, accessToken: AccessToken, completion: @escaping (Result<PredictionVoteResource, Error>) -> Void) -> URLSessionDataTask

    func updatePredictionVote(optionID: String, optionURL: URL, accessToken: AccessToken, completion: @escaping (Result<PredictionVoteResource, Error>) -> Void) -> URLSessionDataTask

    func createVoteOnPoll(for optionURL: URL, accessToken: AccessToken) -> Promise<PollWidgetModel.Vote>

    func updateVoteOnPoll(for optionID: String, optionURL: URL, accessToken: AccessToken) -> Promise<PollWidgetModel.Vote>

    func createImageSliderVote(voteURL: URL, magnitude: Double, accessToken: AccessToken) -> Promise<ImageSliderWidgetModel.Vote>

    func getTimelineInteractions(
        timelineInteractionURL: URL,
        accessToken: AccessToken
    ) -> Promise<WidgetTimelineInteractionsResource>

    func getUnclaimedWidgetInteractions(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<WidgetInteraction>>

    func getWidgetInteractions(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<WidgetTimelineInteractionsResource>

    func createTextAskReply(
        url: URL,
        reply: String,
        accessToken: AccessToken
    ) -> Promise<TextAskWidgetModel.Reply>

    func createNumberPredictionVote(
        voteURL: URL,
        votes: [NumberPredictionVoteSubmission],
        accessToken: AccessToken
    ) -> Promise<NumberPredictionVote>
}

class LiveLikeWidgetAPI: LiveLikeWidgetAPIProtocol {
    private let coreAPI: LiveLikeCoreAPIProtocol

    init(coreAPI: LiveLikeCoreAPI) {
        self.coreAPI = coreAPI
    }

    func createNumberPredictionVote(
        voteURL: URL,
        votes: [NumberPredictionVoteSubmission],
        accessToken: AccessToken
    ) -> Promise<NumberPredictionVote> {
        struct Payload: Encodable {
            let votes: [NumberPredictionVoteSubmission]
        }
        let payload = Payload(votes: votes)
        let resource = Resource<NumberPredictionVote>(
            url: voteURL,
            method: .post(payload),
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func getTimelineInteractions(
        timelineInteractionURL: URL,
        accessToken: AccessToken
    ) -> Promise<WidgetTimelineInteractionsResource> {
        let resource = Resource<WidgetTimelineInteractionsResource>(
            get: timelineInteractionURL,
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func getWidgetInteractions(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<WidgetTimelineInteractionsResource> {
        let resource = Resource<WidgetTimelineInteractionsResource>(
            get: url, accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func getWidget(id: String, kind: WidgetKind) -> Promise<WidgetResource> {
        return firstly {
            coreAPI.whenApplicationConfig.then {
                try $0.getWidgetDetailURL(id: id, kind: kind)
            }
        }.then { widgetDetailURL in
            GetWidgetRequest(
                networking: LiveLike.networking
            ).execute(url: widgetDetailURL)
        }
    }

    func getWidgetResources(
        url: URL,
        accessToken: AccessToken
    ) -> Promise<PaginatedResult<WidgetResource>> {

        let resource = Resource<PaginatedResult<WidgetResource>>(
            get: url,
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)

    }

    func getTimeline(timelineURL: URL, accessToken: AccessToken) -> Promise<WidgetTimelineResource> {
        let resource = Resource<WidgetTimelineResource>(
            get: timelineURL,
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func createImpression(impressionURL: URL, userSessionID: String, accessToken: AccessToken) -> Promise<ImpressionResponse> {
        struct ImpressionBody: Encodable {
            var sessionId: String
        }
        let resource = Resource<ImpressionResponse>(
            url: impressionURL,
            method: .post(ImpressionBody(sessionId: userSessionID)),
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }

    func createCheerMeterVote(voteCount: Int, voteURL: URL, accessToken: AccessToken) -> Promise<CheerMeterWidgetModel.Vote> {
        let vote = CheerMeterVote(voteCount: voteCount)
        let resource = Resource<CheerMeterWidgetModel.Vote>(url: voteURL, method: .post(vote), accessToken: accessToken.asString)
        return LiveLike.networking.load(resource)
    }

    func updateCheerMeterVote(voteCount: Int, voteURL: URL, accessToken: AccessToken) -> Promise<CheerMeterWidgetModel.Vote> {
        let vote = CheerMeterVote(voteCount: voteCount)
        let resource = Resource<CheerMeterWidgetModel.Vote>(url: voteURL, method: .patch(vote), accessToken: accessToken.asString)
        return LiveLike.networking.load(resource)
    }

    func createQuizAnswer(answerURL: URL, accessToken: AccessToken) -> Promise<QuizWidgetModel.Answer> {
        let resource = Resource<QuizWidgetModel.Answer>(url: answerURL, method: .post(EmptyBody()), accessToken: accessToken.asString)
        return LiveLike.networking.load(resource)
    }

    func createPredictionVote(
        voteURL: URL,
        accessToken: AccessToken,
        completion: @escaping (Result<PredictionVoteResource, Error>) -> Void
    ) -> URLSessionDataTask {
        let resource = Resource<PredictionVoteResource>(
            url: voteURL,
            method: .post(EmptyBody()),
            accessToken: accessToken.asString
        )
        return LiveLike.networking.task(resource, completion: completion)
    }

    func updatePredictionVote(
        optionID: String,
        optionURL: URL,
        accessToken: AccessToken,
        completion: @escaping (Result<PredictionVoteResource, Error>) -> Void
    ) -> URLSessionDataTask {
        let voteBody = VoteBody(optionId: optionID)
        let resource = Resource<PredictionVoteResource>(
            url: optionURL,
            method: .patch(voteBody),
            accessToken: accessToken.asString
        )
        return LiveLike.networking.task(resource, completion: completion)
    }

    func createVoteOnPoll(for optionURL: URL, accessToken: AccessToken) -> Promise<PollWidgetModel.Vote> {
        let resource = Resource<PollWidgetModel.Vote>(url: optionURL, method: .post(EmptyBody()), accessToken: accessToken.asString)
        return LiveLike.networking.load(resource)
    }

    func updateVoteOnPoll(for optionID: String, optionURL: URL, accessToken: AccessToken) -> Promise<PollWidgetModel.Vote> {
        let voteBody = VoteBody(optionId: optionID)
        let resource = Resource<PollWidgetModel.Vote>(url: optionURL, method: .patch(voteBody), accessToken: accessToken.asString)
        return LiveLike.networking.load(resource)
    }

    func createImageSliderVote(voteURL: URL, magnitude: Double, accessToken: AccessToken) -> Promise<ImageSliderWidgetModel.Vote> {
        struct ImageSliderVote: Encodable {
            let magnitude: String
        }
        let magnitudeString = String(format: "%.3f", magnitude) // Server expects <= 3 decimal places
        let vote = ImageSliderVote(magnitude: magnitudeString)
        let resource = Resource<ImageSliderWidgetModel.Vote>(url: voteURL, method: .post(vote), accessToken: accessToken.asString)
        return LiveLike.networking.load(resource)
    }

    func getUnclaimedWidgetInteractions(url: URL, accessToken: AccessToken) -> Promise<PaginatedResult<WidgetInteraction>> {
        let resource = Resource<PaginatedResult<WidgetInteraction>>(get: url, accessToken: accessToken.asString)
        return LiveLike.networking.load(resource)
    }

    func createTextAskReply(
        url: URL,
        reply: String,
        accessToken: AccessToken
    ) -> Promise<TextAskWidgetModel.Reply> {
        struct Payload: Encodable {
            let text: String
        }
        let payload = Payload(text: reply)
        let resource = Resource<TextAskWidgetModel.Reply>(
            url: url,
            method: .post(payload),
            accessToken: accessToken.asString
        )
        return LiveLike.networking.load(resource)
    }
}

// MARK: - Models

class WidgetTimelineResource: PaginatedResult<WidgetResource> {
    internal init(previous: URL?, count: Int, next: URL?, results: [WidgetResource], widgetInteractionsURLTemplate: String) {
        self.widgetInteractionsURLTemplate = widgetInteractionsURLTemplate
        super.init(previous: previous, count: count, next: next, items: results)
    }

    enum CodingKeys: CodingKey {
        case widgetInteractionsUrlTemplate
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.widgetInteractionsURLTemplate = try container.decode(String.self, forKey: .widgetInteractionsUrlTemplate)
        try super.init(from: decoder)
    }

    let widgetInteractionsURLTemplate: String

    func getWidgetInteractionsURL(profileID: String) throws -> URL {
        struct InvalidTemplateError: Error { }
        struct InvalidURLError: Error { }
        let stringToReplace = "{profile_id}"
        guard widgetInteractionsURLTemplate.contains(stringToReplace) else {
            log.error("Failed to find string to replace in widgetInteractionsURLTemplate")
            throw InvalidTemplateError()
        }
        let urlTemplateFilled = widgetInteractionsURLTemplate.replacingOccurrences(
            of: stringToReplace,
            with: profileID
        )
        guard let url = URL(string: urlTemplateFilled) else {
            throw InvalidURLError()
        }
        return url
    }
}

struct PredictionVoteResource: Decodable {
    internal init(id: String, widgetID: String, url: URL, optionId: String, rewards: [RewardResource], createdAt: Date, claimToken: String?, widgetKind: WidgetKind) {
        self.id = id
        self.widgetID = widgetID
        self.url = url
        self.optionId = optionId
        self.rewards = rewards
        self.claimToken = claimToken
        self.createdAt = createdAt
        self.widgetKind = widgetKind
    }

    let id: String
    let widgetID: String
    let url: URL
    let optionId: String
    let rewards: [RewardResource]
    let createdAt: Date
    let claimToken: String?
    let widgetKind: WidgetKind

    enum CodingKeys: String, CodingKey {
        case id
        case widgetId
        case url
        case optionId
        case rewards
        case claimToken
        case createdAt
        case widgetKind
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.widgetID = try container.decode(String.self, forKey: .widgetId)
        self.url = try container.decode(URL.self, forKey: .url)
        self.optionId = try container.decode(String.self, forKey: .optionId)
        self.rewards = try container.decodeIfPresent([RewardResource].self, forKey: .rewards) ?? []
        self.createdAt = try container.decode(Date.self, forKey: .createdAt)
        self.claimToken = try? container.decode(String.self, forKey: .claimToken)
        self.widgetKind = try container.decode(WidgetKind.self, forKey: .widgetKind)
    }
}

struct CheerMeterVote: Encodable {
    let voteCount: Int
}

public enum WidgetInteraction: Decodable {

    enum CodingKeys: CodingKey {
        case widgetKind
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let kind: WidgetKind = try container.decode(WidgetKind.self, forKey: .widgetKind)
        switch kind {
        case .textQuiz, .imageQuiz:
            let answer = try QuizWidgetModel.Answer(from: decoder)
            self = .quizAnswer(answer)
        case .textPoll, .imagePoll:
            let vote = try PollWidgetModel.Vote(from: decoder)
            self = .pollVote(vote)
        case .textPrediction, .imagePrediction:
            let vote = try PredictionVoteResource(from: decoder)
            self = .predictionVote(PredictionVote(resource: vote))
        case .imageSlider:
            let vote = try ImageSliderWidgetModel.Vote(from: decoder)
            self = .imageSliderVote(vote)
        case .cheerMeter:
            let vote = try CheerMeterWidgetModel.Vote(from: decoder)
            self = .cheerMeterVote(vote)
        case .textAsk:
            let reply = try TextAskWidgetModel.Reply(from: decoder)
            self = .textAskReply(reply)
        case .imageNumberPrediction, .textNumberPrediction:
            let vote = try NumberPredictionVote(from: decoder)
            self = .numberPredictionVote(vote)
        default:
            let error = WidgetInteractionErrors.unexpectedInteractionForWidgetKind(kind: kind)
            log.error(error)
            throw error
        }
    }

    case quizAnswer(QuizWidgetModel.Answer)
    case pollVote(PollWidgetModel.Vote)
    case predictionVote(PredictionVote)
    case imageSliderVote(ImageSliderWidgetModel.Vote)
    case cheerMeterVote(CheerMeterWidgetModel.Vote)
    case textAskReply(TextAskWidgetModel.Reply)
    case numberPredictionVote(NumberPredictionVote)

    public var widgetID: String {
        switch self {
        case .quizAnswer(let answer):
            return answer.widgetID
        case .pollVote(let vote):
            return vote.widgetID
        case .predictionVote(let vote):
            return vote.widgetID
        case .imageSliderVote(let vote):
            return vote.widgetID
        case .cheerMeterVote(let vote):
            return vote.widgetID
        case .textAskReply(let reply):
            return reply.widgetID
        case .numberPredictionVote(let vote):
            return vote.widgetID
        }
    }

    public var widgetKind: WidgetKind {
        switch self {
        case .quizAnswer(let answer):
            return answer.widgetKind
        case .pollVote(let vote):
            return vote.widgetKind
        case .predictionVote(let vote):
            return vote.widgetKind ?? .textPrediction
        case .imageSliderVote(let vote):
            return vote.widgetKind
        case .cheerMeterVote(let vote):
            return vote.widgetKind
        case .textAskReply(let reply):
            return reply.widgetKind
        case .numberPredictionVote(let vote):
            return vote.widgetKind
        }
    }
}

extension Array where Element == WidgetInteraction {
    var numberPredictionVotes: [NumberPredictionVote] {
        self.compactMap {
            guard case let .numberPredictionVote(vote) = $0 else { return nil }
            return vote
        }
    }

    var textAskReplies: [TextAskWidgetModel.Reply] {
        self.compactMap {
            guard case let .textAskReply(reply) = $0 else { return nil }
            return reply
        }
    }

    var quizAnswers: [QuizWidgetModel.Answer] {
        self.compactMap {
            guard case let .quizAnswer(answer) = $0 else { return nil }
            return answer
        }
    }

    var pollVotes: [PollWidgetModel.Vote] {
        self.compactMap {
            guard case let .pollVote(vote) = $0 else { return nil }
            return vote
        }
    }

    var predictionVotes: [PredictionVote] {
        self.compactMap {
            guard case let .predictionVote(answer) = $0 else { return nil }
            return answer
        }
    }

    var imageSliderVotes: [ImageSliderWidgetModel.Vote] {
        self.compactMap {
            guard case let .imageSliderVote(answer) = $0 else { return nil }
            return answer
        }
    }

    var cheerMeterVotes: [CheerMeterWidgetModel.Vote] {
        self.compactMap {
            guard case let .cheerMeterVote(answer) = $0 else { return nil }
            return answer
        }
    }
}

struct WidgetTimelineInteractionsResource: Decodable {
    internal init(interactionsByKind: [WidgetKind: [WidgetInteraction]]) {
        self.interactionsByKind = interactionsByKind
    }

    var interactionsByKind: [WidgetKind: [WidgetInteraction]]

    enum CodingKeys: CodingKey {
        case interactions
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let subContainer = try container.nestedContainer(keyedBy: GenericCodingKeys.self, forKey: .interactions)
        var interactionsByKind = [WidgetKind: [WidgetInteraction]]()
        for key in subContainer.allKeys {
            guard let kind = WidgetKind(stringValue: key.stringValue) else {
                log.error("invalid widget kind \(key.stringValue)")
                break
            }
            interactionsByKind[kind] = try subContainer.decode([WidgetInteraction].self, forKey: key)
        }
        self.interactionsByKind = interactionsByKind
    }
}

// A struct that conforms to the CodingKey protocol
// It defines no key by itself, hence the name "Generic"
struct GenericCodingKeys: CodingKey {
    var stringValue: String
    var intValue: Int?

    init?(stringValue: String) { self.stringValue = stringValue }
    init?(intValue: Int) { self.intValue = intValue; self.stringValue = "\(intValue)" }
}
