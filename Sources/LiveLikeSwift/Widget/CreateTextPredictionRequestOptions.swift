//
//  CreateImagePredictionRequestOptions.swift
//
//
//  Created by Jelzon Monzon on 8/3/23.
//

import Foundation

public struct CreateTextPredictionRequestOptions {

    /// - Parameters:
    ///   - question: The question the Prediction will ask
    ///   - options: The possible options of the Prediction
    public init(
        common: CommonCreateWidgetOptions,
        question: String,
        choices: [Choice]
    ) {
        self.common = common
        self.question = question
        self.choices = choices
    }

    public let common: CommonCreateWidgetOptions
    public let question: String
    public let choices: [Choice]

    public struct Choice {
        /// - Parameters:
        ///   - text: The text describing the Option
        public init(text: String) {
            self.text = text
        }

        public let text: String
    }
}

extension CreateTextPredictionRequestOptions: Encodable {
    enum CodingKeys: String, CodingKey {
        case question
        case options
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(question, forKey: .question)
        try container.encode(choices, forKey: .options)
        try common.encode(to: encoder)
    }
}

extension CreateTextPredictionRequestOptions.Choice: Encodable {
    enum CodingKeys: String, CodingKey {
        case text = "description"
    }
}
