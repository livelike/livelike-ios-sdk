//
//  WidgetPublishStatus.swift
//  
//
//  Created by Jelzon Monzon on 8/7/23.
//

import Foundation

public enum WidgetPublishStatus: String {
    case pending
    case inflight
    case scheduled
    case published
}

extension WidgetPublishStatus: Decodable { }

