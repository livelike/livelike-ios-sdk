//
//  InternalWidgetClient.swift
//  
//
//  Created by Jelzon Monzon on 8/4/23.
//

import Foundation
import LiveLikeCore

class InternalWidgetClient {
    private let networking: LLNetworking
    private let accessToken: String
    private let widgetModelFactory: WidgetModelFactory
    private let application: ApplicationConfiguration
    private let coreAPI: LiveLikeCoreAPIProtocol

    init(
        networking: LLNetworking,
        accessToken: String,
        widgetModelFactory: WidgetModelFactory,
        application: ApplicationConfiguration,
        coreAPI: LiveLikeCoreAPIProtocol
    ) {
        self.networking = networking
        self.accessToken = accessToken
        self.widgetModelFactory = widgetModelFactory
        self.application = application
        self.coreAPI = coreAPI
    }
}

extension InternalWidgetClient: WidgetClient {

    func publishWidget(
        options: PublishWidgetRequestOptions,
        completion: @escaping (Result<PublishedWidgetInfo, Error>) -> Void
    ) {
        getWidget(
            id: options.id,
            kind: options.kind
        ) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let widget):
                PublishWidgetRequest(
                    networking: self.networking,
                    accessToken: self.accessToken,
                    publishDelay: options.publishDelaySeconds,
                    programDateTime: options.programDateTime
                ).execute(
                    url: widget.baseWidgetResource.scheduleURL,
                    completion: completion
                )
            }
        }

    }

    func deleteWidget(
        options: DeleteWidgetRequestOptions,
        completion: @escaping (Result<Bool, Error>) -> Void
    ) {
        getWidget(
            id: options.id,
            kind: options.kind
        ) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let widget):
                DeleteWidgetRequest(
                    networking: self.networking,
                    accessToken: self.accessToken
                ).execute(
                    url: widget.baseWidgetResource.deleteURL,
                    completion: completion
                )
            }
        }
    }

    func createTextPollWidget(
        options: CreateTextPollRequestOptions,
        completion: @escaping (Result<PollWidgetModel, Error>) -> Void
    ) {
        AnyCreateWidgetRequest<CreateTextPollRequestOptions, TextPollCreated>(
            body: options,
            networking: networking,
            accessToken: accessToken
        ).execute(
            url: application.textPollsUrl
        ) { [weak self] result in
            guard let self = self else { return }
            completion(result.map { data in
                self.widgetModelFactory.makePollModel(data: data)
            })
        }
    }

    func createImagePollWidget(
        options: CreateImagePollRequestOptions,
        completion: @escaping (Result<PollWidgetModel, Error>) -> Void
    ) {
        AnyCreateWidgetRequest<CreateImagePollRequestOptions, ImagePollCreated>(
            body: options,
            networking: networking,
            accessToken: accessToken
        ).execute(
            url: application.imagePollsUrl
        ) { [weak self] result in
            guard let self = self else { return }
            completion(result.map { data in
                self.widgetModelFactory.makePollModel(data: data)
            })
        }
    }

    func createAlertWidget(
        options: CreateAlertRequestOptions,
        completion: @escaping (Result<AlertWidgetModel, Error>) -> Void
    ) {
        AnyCreateWidgetRequest<CreateAlertRequestOptions, AlertCreated>(
            body: options,
            networking: networking,
            accessToken: accessToken
        ).execute(
            url: application.alertsUrl
        ) { [weak self] result in
            guard let self = self else { return }
            completion(result.map { data in
                self.widgetModelFactory.makeAlertModel(data: data)
            })
        }
    }

    func createTextPredictionWidget(
        options: CreateTextPredictionRequestOptions,
        completion: @escaping (Result<PredictionWidgetModel, Error>) -> Void
    ) {
        AnyCreateWidgetRequest<CreateTextPredictionRequestOptions, TextPredictionCreated>(
            body: options,
            networking: networking,
            accessToken: accessToken
        ).execute(
            url: application.textPredictionsUrl
        ) { [weak self] result in
            guard let self = self else { return }

            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let data):
                firstly {
                    self.coreAPI.getProgramDetail(programID: data.programID)
                }.then { program in
                    let model = self.widgetModelFactory.makePredictionModel(
                        data: data,
                        programSubscribeChannel: program.getWidgetPublishChannel()
                    )
                    completion(.success(model))
                }.catch {
                    completion(.failure($0))
                }
            }
        }
    }

    func createImagePredictionWidget(
        options: CreateImagePredictionRequestOptions,
        completion: @escaping (Result<PredictionWidgetModel, Error>) -> Void
    ) {
        AnyCreateWidgetRequest<CreateImagePredictionRequestOptions, ImagePredictionCreated>(
            body: options,
            networking: networking,
            accessToken: accessToken
        ).execute(
            url: application.imagePredictionsUrl
        ) { [weak self] result in
            guard let self = self else { return }

            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let data):
                firstly {
                    self.coreAPI.getProgramDetail(programID: data.programID)
                }.then { program in
                    let model = self.widgetModelFactory.makePredictionModel(
                        data: data,
                        programSubscribeChannel: program.getWidgetPublishChannel()
                    )
                    completion(.success(model))
                }.catch {
                    completion(.failure($0))
                }
            }
        }
    }

    func updateTextPredictionFollowUpWidgetOption(
        options: UpdateTextPredictionFollowUpRequestOptions,
        completion: @escaping (Result<PredictionFollowUpWidgetModel.Option, Error>) -> Void
    ) {
        getWidget(
            id: options.predictionID,
            kind: .textPrediction
        ) { [weak self] result in
            guard let self = self else { return }

            switch result {
            case .failure(let error):
                completion(.failure(error))

            case .success(let widget):
                switch widget {
                case .textPredictionCreated(let data):
                    guard let predictionOption = data.followUps.first?.options
                        .first(where: { $0.id == options.optionID})
                    else {
                        return completion(.failure(GetWidgetError.widgetDoesNotExist))
                    }

                    UpdateTextPredictionFollowUpOptionRequest(
                        networking: self.networking,
                        accessToken: self.accessToken,
                        isCorrect: options.isCorrect
                    ).execute(
                        url: predictionOption.url,
                        completion: {
                            completion($0.map {
                                PredictionFollowUpWidgetModel.Option(resource: $0)
                            })
                        }
                    )

                default:
                    completion(.failure(GetWidgetError.widgetDoesNotExist))
                }
            }
        }
    }

    func updateImagePredictionFollowUpWidgetOption(
        options: UpdateImagePredictionFollowUpRequestOptions,
        completion: @escaping (Result<PredictionFollowUpWidgetModel.Option, Error>) -> Void
    ) {
        getWidget(
            id: options.predictionID,
            kind: .imagePrediction
        ) { [weak self] result in
            guard let self = self else { return }

            switch result {
            case .failure(let error):
                completion(.failure(error))

            case .success(let widget):
                switch widget {
                case .imagePredictionCreated(let data):
                    guard let predictionOption = data.followUps.first?.options
                        .first(where: { $0.id == options.optionID})
                    else {
                        return completion(.failure(GetWidgetError.widgetDoesNotExist))
                    }

                    UpdateImagePredictionFollowUpOptionRequest(
                        networking: self.networking,
                        accessToken: self.accessToken,
                        isCorrect: options.isCorrect
                    ).execute(
                        url: predictionOption.url,
                        completion: {
                            completion($0.map {
                                PredictionFollowUpWidgetModel.Option(resource: $0)
                            })
                        }
                    )

                default:
                    completion(.failure(GetWidgetError.widgetDoesNotExist))
                }
            }
        }
    }

    func createTextAskWidget(
        options: CreateTextAskWidgetOptions,
        completion: @escaping (Result<TextAskWidgetModel, Error>) -> Void
    ) {
        AnyCreateWidgetRequest<CreateTextAskWidgetOptions, TextAskWidgetResource>(
            body: options,
            networking: self.networking,
            accessToken: self.accessToken
        ).execute(
            url: self.application.textAsksUrl
        ) { [weak self] result in
            guard let self = self else { return }
            completion(result.map {
                self.widgetModelFactory.makeTextAsk(data: $0)
            })
        }
    }

    func createTextQuizWidget(
        options: CreateTextQuizWidgetOptions,
        completion: @escaping (Result<QuizWidgetModel, Error>) -> Void
    ) {
        AnyCreateWidgetRequest<CreateTextQuizWidgetOptions, TextQuizCreated>(
            body: options,
            networking: networking,
            accessToken: accessToken
        ).execute(
            url: application.textQuizzesUrl
        ) { [weak self] result in
            guard let self = self else { return }
            completion(result.map { data in
                self.widgetModelFactory.makeQuizModel(data: data)
            })
        }
    }

    func createImageQuizWidget(
        options: CreateImageQuizWidgetOptions,
        completion: @escaping (Result<QuizWidgetModel, Error>) -> Void
    ) {
        AnyCreateWidgetRequest<CreateImageQuizWidgetOptions, ImageQuizCreated>(
            body: options,
            networking: networking,
            accessToken: accessToken
        ).execute(
            url: application.imageQuizzesUrl
        ) { [weak self] result in
            guard let self = self else { return }
            completion(result.map { data in
                self.widgetModelFactory.makeQuizModel(data: data)
            })
        }
    }

    func createNumberPredictionWidget(
        options: CreateNumberPredictionWidgetOptions,
        completion: @escaping (Result<NumberPredictionWidgetModel, Error>) -> Void
    ) {
        AnyCreateWidgetRequest<CreateNumberPredictionWidgetOptions, NumberPrediction>(
            body: options, 
            networking: self.networking,
            accessToken: self.accessToken
        ).execute(
            url: self.application.imageNumberPredictionsUrl
        ) { [weak self] result in
            guard let self = self else { return }

            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let data):
                firstly {
                    self.coreAPI.getProgramDetail(programID: data.programID)
                }.then { program in
                    let model = self.widgetModelFactory.makeNumberPrediction(
                        data: data,
                        programSubscribeChannel: program.getWidgetPublishChannel()
                    )
                    completion(.success(model))
                }.catch {
                    completion(.failure($0))
                }
            }
        }
    }

    func updateNumberPredictionFollowUpOption(
        options: UpdateNumberPredictionFollowUpOption,
        completion: @escaping (Result<NumberPredictionOption, Error>) -> Void
    ) {
        getWidget(
            id: options.predictionFollowUpID,
            kind: .imageNumberPredictionFollowUp
        ) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let widget):
                switch widget {
                case .numberPredictionFollowUp(let numberPredictionFollowUp):

                    guard let optionURL = numberPredictionFollowUp.options
                        .first(where: { $0.id == options.optionID})?.url
                    else {
                        return completion(.failure(GetWidgetError.widgetDoesNotExist))
                    }
                    UpdateNumberPredictionFollowUpOptionRequest(
                        option: options,
                        networking: self.networking,
                        accessToken: self.accessToken
                    ).execute(
                        url: optionURL,
                        completion: completion
                    )
                default:
                    completion(.failure(GetWidgetError.widgetDoesNotExist))
                }
            }
        }
    }

    private func getWidget(
        id: String,
        kind: WidgetKind,
        completion: @escaping (Result<WidgetResource, Error>) -> Void
    ) {
        GetWidgetRequest(
            networking: networking
        ).execute(
            tryURL: try self.application.getWidgetDetailURL(
                id: id,
                kind: kind
            ),
            completion: completion
        )
    }

    func createRichPost(
        options: CreateRichPostWidgetOptions,
        completion: @escaping (Result<RichPostWidgetModel, Error>) -> Void
    ) {
        AnyCreateWidgetRequest<CreateRichPostWidgetOptions, RichPost>(
            body: options, 
            networking: self.networking,
            accessToken: self.accessToken
        ).execute(url: self.application.richPostsUrl) { result in
            completion(result.map {
                self.widgetModelFactory.makeRichPost(data: $0)
            })
        }
    }

}
