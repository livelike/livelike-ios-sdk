//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//
	

import Foundation

/// An option of a Number Prediction
public struct NumberPredictionOption: Decodable, Equatable {
    
    /// The option's id
    public let id: String
    /// An image that describes the option
    public let imageURL: URL?
    /// Some text that describes the options
    public let text: String
    /// The correct number of the options (nil if unknown)
    public let correctNumber: Int?
    
    let url: URL
    
    public init(
        id: String,
        imageURL: URL?,
        text: String,
        correctNumber: Int?,
        url: URL
    ) {
        self.id = id
        self.imageURL = imageURL
        self.text = text
        self.correctNumber = correctNumber
        self.url = url
    }
    
    public init(from decoder: Decoder) throws {
        enum CodingKeys: CodingKey {
            case id
            case imageUrl
            case description
            case correctNumber
            case url
        }
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.imageURL = try container.decodeIfPresent(URL.self, forKey: .imageUrl)
        self.text = try container.decode(String.self, forKey: .description)
        self.correctNumber = try container.decodeIfPresent(Int.self, forKey: .correctNumber)
        self.url = try container.decode(URL.self, forKey: .url)
    }
    
    public static func == (
        lhs: NumberPredictionOption,
        rhs: NumberPredictionOption
    ) -> Bool {
        return
            lhs.id == rhs.id &&
            lhs.imageURL == rhs.imageURL &&
            lhs.text == rhs.text &&
            lhs.correctNumber == rhs.correctNumber
    }
}
