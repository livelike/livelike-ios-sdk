//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//
	

import Foundation

/// Options for creating a Number Prediction Widget
public struct CreateNumberPredictionWidgetOptions {
    
    let common: CommonCreateWidgetOptions
    let question: String
    let options: [Option]
    
    /// - Parameters:
    ///   - common: Common options for creating Widgets
    ///   - question: The question prompt for the Number Predictions
    ///   - options: The options of the Number Prediction
    public init(
        common: CommonCreateWidgetOptions,
        question: String,
        options: [CreateNumberPredictionWidgetOptions.Option]
    ) {
        self.common = common
        self.question = question
        self.options = options
    }
    
    /// Options for creating a Number Prediction option
    public struct Option {
        let text: String
        let imageURL: URL
        
        /// - Parameters:
        ///   - text: The text describing the option
        ///   - imageURL: An image url representing the option
        public init(
            text: String,
            imageURL: URL
        ) {
            self.text = text
            self.imageURL = imageURL
        }
    }
}

extension CreateNumberPredictionWidgetOptions: Encodable {
    
    enum CodingKeys: CodingKey {
        case question
        case options
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(question, forKey: .question)
        try container.encode(options, forKey: .options)
        try common.encode(to: encoder)
    }
    
}

extension CreateNumberPredictionWidgetOptions.Option: Encodable {
    enum CodingKeys: String, CodingKey {
        case text = "description"
        case imageURL = "imageUrl"
    }
}
