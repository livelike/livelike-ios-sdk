//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

/// Options for updating a number prediction follow up option
public struct UpdateNumberPredictionFollowUpOption {

    let predictionFollowUpID: String
    let optionID: String
    let rewardItemID: String?
    let rewardItemAmount: Int?
    let correctNumber: Int?
    let text: String
    let imageURL: URL?

    /// Options for updating a number prediction follow up option
    /// - Parameters:
    ///   - predictionFollowUpID: The id of the prediction follow up
    ///   - optionID: The id of the option to update
    ///   - rewardItemID: Change the reward item id for answering correctly
    ///   - rewardItemAmount: Change the reward item amount for answering correctly
    ///   - correctNumber: Change the correct answer for the option
    ///   - text: Change the text of the option
    ///   - imageURL: Change the image of the option
    public init(
        predictionFollowUpID: String,
        optionID: String,
        rewardItemID: String? = nil,
        rewardItemAmount: Int? = nil,
        correctNumber: Int? = nil,
        text: String,
        imageURL: URL? = nil
    ) {
        self.predictionFollowUpID = predictionFollowUpID
        self.optionID = optionID
        self.rewardItemID = rewardItemID
        self.rewardItemAmount = rewardItemAmount
        self.correctNumber = correctNumber
        self.text = text
        self.imageURL = imageURL
    }
}
