//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

/// An object to submit an vote for a single option
public struct NumberPredictionVoteSubmission: Encodable {

    /// The id of the option
    public let optionID: String
    /// The number value of the vote submission
    public let number: Int

    public init(
        optionID: String,
        number: Int
    ) {
        self.optionID = optionID
        self.number = number
    }

    enum CodingKeys: CodingKey {
        case number
        case optionId
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(optionID, forKey: .optionId)
        try container.encode(number, forKey: .number)
    }
}
