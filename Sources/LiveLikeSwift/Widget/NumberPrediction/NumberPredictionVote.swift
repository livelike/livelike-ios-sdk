//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//
	

import Foundation

/// Represents a locked Number Prediction vote
public struct NumberPredictionVote: Decodable {
    /// The id of the Number Prediction Widget
    public let widgetID: String
    /// The kind of the Number Prediction Widget
    public let widgetKind: WidgetKind
    /// The individual vote for each option
    public let votes: [OptionVote]
    
    let claimToken: String
    
    public init(
        widgetID: String,
        widgetKind: WidgetKind,
        votes: [NumberPredictionVote.OptionVote],
        claimToken: String
    ) {
        self.widgetID = widgetID
        self.widgetKind = widgetKind
        self.votes = votes
        self.claimToken = claimToken
    }
    
    enum CodingKeys: CodingKey {
        case widgetId
        case widgetKind
        case votes
        case claimToken
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.widgetID = try container.decode(String.self, forKey: .widgetId)
        self.widgetKind = try container.decode(WidgetKind.self, forKey: .widgetKind)
        self.votes = try container.decode([OptionVote].self, forKey: .votes)
        self.claimToken = try container.decode(String.self, forKey: .claimToken)
    }
    
    /// Represents a vote of a single Number Prediction option
    public struct OptionVote: Decodable {
        /// The id of the option vote
        public let id: String
        /// The id of the option
        public let optionID: String
        /// The number value that was voted
        public let number: Int
        /// The date the vote was created
        public let createdAt: Date
        
        public init(id: String, optionID: String, number: Int, createdAt: Date) {
            self.id = id
            self.optionID = optionID
            self.number = number
            self.createdAt = createdAt
        }
        
        enum CodingKeys: CodingKey {
            case id
            case optionId
            case number
            case createdAt
        }
        
        public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.id = try container.decode(String.self, forKey: .id)
            self.optionID = try container.decode(String.self, forKey: .optionId)
            self.number = try container.decode(Int.self, forKey: .number)
            self.createdAt = try container.decode(Date.self, forKey: .createdAt)
        }
    }
}
