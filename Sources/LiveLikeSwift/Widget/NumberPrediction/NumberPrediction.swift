//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//
	

import Foundation

class NumberPrediction: BaseWidgetResource {
    internal init(baseData: BaseWidgetResource, question: String, options: [NumberPredictionOption], voteURL: URL, widgetInteractionsURLTemplate: String, followUps: [NumberPredictionFollowUp], confirmationMessage: String) {
        self.question = question
        self.options = options
        self.voteURL = voteURL
        self.widgetInteractionsURLTemplate = widgetInteractionsURLTemplate
        self.followUps = followUps
        self.confirmationMessage = confirmationMessage
        super.init(baseData: baseData)
    }
    
    let question: String
    let options: [NumberPredictionOption]
    let voteURL: URL
    let widgetInteractionsURLTemplate: String
    let followUps: [NumberPredictionFollowUp]
    let confirmationMessage: String
    
    required init(from decoder: Decoder) throws {
        enum CodingKeys: CodingKey {
            case question
            case options
            case voteUrl
            case widgetInteractionsUrlTemplate
            case followUps
            case confirmationMessage
        }
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.question = try container.decode(String.self, forKey: .question)
        self.options = try container.decode([NumberPredictionOption].self, forKey: .options)
        self.voteURL = try container.decode(URL.self, forKey: .voteUrl)
        self.widgetInteractionsURLTemplate = try container.decode(String.self, forKey: .widgetInteractionsUrlTemplate)
        self.followUps = try container.decode([NumberPredictionFollowUp].self, forKey: .followUps)
        self.confirmationMessage = try container.decode(String.self, forKey: .confirmationMessage)
        try super.init(from: decoder)
    }
}
