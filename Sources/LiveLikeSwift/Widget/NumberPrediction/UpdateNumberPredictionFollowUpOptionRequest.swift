//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation
import LiveLikeCore

struct UpdateNumberPredictionFollowUpOptionRequest {
    let option: UpdateNumberPredictionFollowUpOption
    let networking: LLNetworking
    let accessToken: String
}

extension UpdateNumberPredictionFollowUpOptionRequest: LLRequest {
    typealias ResponseType = NumberPredictionOption

    func execute(
        url: URL,
        completion: @escaping (Result<NumberPredictionOption, Error>) -> Void
    ) {
        do {
            let url = try url.with(queryItems: [
                URLQueryItem(
                    name: "option_id",
                    value: option.optionID
                )
            ])
            struct Payload: Encodable {
                let rewardItemId: String?
                let rewardItemAmount: Int?
                let correctNumber: Int?
                let description: String?
                let imageUrl: URL?
            }
            let resource = Resource<NumberPredictionOption>(
                url: url,
                method: .patch(
                    Payload(
                        rewardItemId: option.rewardItemID,
                        rewardItemAmount: option.rewardItemAmount,
                        correctNumber: option.correctNumber,
                        description: option.text,
                        imageUrl: option.imageURL
                    )
                ),
                accessToken: accessToken
            )
            networking.task(resource, completion: completion)
        } catch {
            completion(.failure(error))
        }

    }

}
