//
//  InternalWidgetClientAsyncWrapper.swift
//  
//
//  Created by Jelzon Monzon on 8/4/23.
//

import Foundation
import LiveLikeCore

class InternalWidgetClientAsyncWrapper {
    
    private let whenAccessToken: Promise<String>
    private let whenWidgetModelFactory: Promise<WidgetModelFactory>
    private let networking: LLNetworking
    private let whenWidgetClient: Promise<WidgetClient>
    private let dispatchQueue: DispatchQueue = .global()
    private let whenApplication: Promise<ApplicationConfiguration>
    
    init(
        whenAccessToken: Promise<String>,
        whenWidgetModelFactory: Promise<WidgetModelFactory>,
        networking: LLNetworking,
        whenApplication: Promise<ApplicationConfiguration>,
        coreAPI: LiveLikeCoreAPIProtocol
    ) {
        self.whenAccessToken = whenAccessToken
        self.whenWidgetModelFactory = whenWidgetModelFactory
        self.networking = networking
        self.whenApplication = whenApplication
        self.whenWidgetClient = firstly {
            Promises.zip(
                whenAccessToken,
                whenWidgetModelFactory,
                whenApplication
            )
        }.then { accessToken, widgetModelFactory, application in
            return InternalWidgetClient(
                networking: networking,
                accessToken: accessToken,
                widgetModelFactory: widgetModelFactory,
                application: application,
                coreAPI: coreAPI
            )
        }
    }
}

extension InternalWidgetClientAsyncWrapper: WidgetClient {
    
    func publishWidget(
        options: PublishWidgetRequestOptions,
        completion: @escaping (Result<PublishedWidgetInfo, Error>) -> Void
    ) {
        firstly {
            whenWidgetClient
        }.then(on: dispatchQueue) {
            $0.publishWidget(options: options, completion: completion)
        }.catch {
            completion(.failure($0))
        }
    }
    
    func deleteWidget(
        options: DeleteWidgetRequestOptions,
        completion: @escaping (Result<Bool, Error>) -> Void
    ) {
        firstly {
            whenWidgetClient
        }.then(on: dispatchQueue) {
            $0.deleteWidget(options: options, completion: completion)
        }.catch {
            completion(.failure($0))
        }
    }
    
    func createTextPollWidget(options: CreateTextPollRequestOptions, completion: @escaping (Result<PollWidgetModel, Error>) -> Void) {
        firstly {
            whenWidgetClient
        }.then(on: dispatchQueue) {
            $0.createTextPollWidget(options: options, completion: completion)
        }.catch {
            completion(.failure($0))
        }
    }
    
    func createImagePollWidget(options: CreateImagePollRequestOptions, completion: @escaping (Result<PollWidgetModel, Error>) -> Void) {
        firstly {
            whenWidgetClient
        }.then(on: dispatchQueue) {
            $0.createImagePollWidget(options: options, completion: completion)
        }.catch {
            completion(.failure($0))
        }
    }
    
    func createAlertWidget(options: CreateAlertRequestOptions, completion: @escaping (Result<AlertWidgetModel, Error>) -> Void) {
        firstly {
            whenWidgetClient
        }.then(on: dispatchQueue) {
            $0.createAlertWidget(options: options, completion: completion)
        }.catch {
            completion(.failure($0))
        }
    }
    
    func createTextPredictionWidget(options: CreateTextPredictionRequestOptions, completion: @escaping (Result<PredictionWidgetModel, Error>) -> Void) {
        firstly {
            whenWidgetClient
        }.then(on: dispatchQueue) {
            $0.createTextPredictionWidget(options: options, completion: completion)
        }.catch {
            completion(.failure($0))
        }
    }
    
    func createImagePredictionWidget(options: CreateImagePredictionRequestOptions, completion: @escaping (Result<PredictionWidgetModel, Error>) -> Void) {
        firstly {
            whenWidgetClient
        }.then(on: dispatchQueue) {
            $0.createImagePredictionWidget(options: options, completion: completion)
        }.catch {
            completion(.failure($0))
        }
    }
    
    func updateTextPredictionFollowUpWidgetOption(options: UpdateTextPredictionFollowUpRequestOptions, completion: @escaping (Result<PredictionFollowUpWidgetModel.Option, Error>) -> Void) {
        firstly {
            whenWidgetClient
        }.then(on: dispatchQueue) {
            $0.updateTextPredictionFollowUpWidgetOption(
                options: options,
                completion: completion
            )
        }.catch {
            completion(.failure($0))
        }
    }
    
    func updateImagePredictionFollowUpWidgetOption(
        options: UpdateImagePredictionFollowUpRequestOptions,
        completion: @escaping (Result<PredictionFollowUpWidgetModel.Option, Error>) -> Void
    ) {
        firstly {
            whenWidgetClient
        }.then(on: dispatchQueue) {
            $0.updateImagePredictionFollowUpWidgetOption(
                options: options,
                completion: completion
            )
        }.catch {
            completion(.failure($0))
        }
    }
    
    func createTextAskWidget(
        options: CreateTextAskWidgetOptions,
        completion: @escaping (Result<TextAskWidgetModel, Error>) -> Void
    ) {
        firstly {
            whenWidgetClient
        }.then(on: dispatchQueue) {
            $0.createTextAskWidget(
                options: options,
                completion: completion
            )
        }.catch {
            completion(.failure($0))
        }
    }
    
    func createTextQuizWidget(
        options: CreateTextQuizWidgetOptions,
        completion: @escaping (Result<QuizWidgetModel, Error>) -> Void
    ) {
        firstly {
            whenWidgetClient
        }.then(on: dispatchQueue) {
            $0.createTextQuizWidget(
                options: options,
                completion: completion
            )
        }.catch {
            completion(.failure($0))
        }
    }
    
    func createImageQuizWidget(
        options: CreateImageQuizWidgetOptions,
        completion: @escaping (Result<QuizWidgetModel, Error>) -> Void
    ) {
        firstly {
            whenWidgetClient
        }.then(on: dispatchQueue) {
            $0.createImageQuizWidget(
                options: options,
                completion: completion
            )
        }.catch {
            completion(.failure($0))
        }
    }
    
    func createRichPost(
        options: CreateRichPostWidgetOptions,
        completion: @escaping (Result<RichPostWidgetModel, Error>) -> Void
    ) {
        firstly {
            whenWidgetClient
        }.then(on: dispatchQueue) {
            $0.createRichPost(
                options: options,
                completion: completion
            )
        }.catch {
            completion(.failure($0))
        }
    }
    func createNumberPredictionWidget(options: CreateNumberPredictionWidgetOptions, completion: @escaping (Result<NumberPredictionWidgetModel, Error>) -> Void) {
        firstly {
            whenWidgetClient
        }.then(on: dispatchQueue) {
            $0.createNumberPredictionWidget(
                options: options,
                completion: completion
            )
        }.catch {
            completion(.failure($0))
        }
    }
    
    func updateNumberPredictionFollowUpOption(
        options: UpdateNumberPredictionFollowUpOption,
        completion: @escaping (Result<NumberPredictionOption, Error>) -> Void
    ) {
        firstly {
            whenWidgetClient
        }.then(on: dispatchQueue) {
            $0.updateNumberPredictionFollowUpOption(
                options: options,
                completion: completion
            )
        }.catch {
            completion(.failure($0))
        }
    }
    
}
