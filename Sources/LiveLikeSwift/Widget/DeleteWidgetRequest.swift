//
//  DeleteWidgetRequest.swift
//  
//
//  Created by Jelzon Monzon on 8/7/23.
//

import Foundation
import LiveLikeCore

struct DeleteWidgetRequest: LLRequest {

    private let networking: LLNetworking
    private let accessToken: String

    init(networking: LLNetworking, accessToken: String) {
        self.networking = networking
        self.accessToken = accessToken
    }

    func execute(url: URL, completion: @escaping (Result<Bool, Error>) -> Void) {
        let resource = Resource<Bool>(
            url: url,
            method: .delete(EmptyBody()),
            accessToken: accessToken
        )
        networking.task(resource, completion: completion)
    }
}
