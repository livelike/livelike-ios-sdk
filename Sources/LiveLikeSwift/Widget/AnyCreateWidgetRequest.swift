//
//  AnyCreateWidgetRequest.swift
//  
//
//  Created by Jelzon Monzon on 8/3/23.
//

import Foundation
import LiveLikeCore

class AnyCreateWidgetRequest<
    Body: Encodable,
    WidgetData: BaseWidgetResource
>: LLRequest {
    private let body: Body
    private let networking: LLNetworking
    private let accessToken: String

    init(
        body: Body,
        networking: LLNetworking,
        accessToken: String
    ) {
        self.body = body
        self.networking = networking
        self.accessToken = accessToken
    }

    func execute(
        url: URL,
        completion: @escaping (Result<WidgetData, Error>) -> Void
    ) {
        let resource = Resource<WidgetData>(
            url: url,
            method: .post(self.body),
            accessToken: accessToken
        )
        networking.task(resource, completion: completion)
    }
}
