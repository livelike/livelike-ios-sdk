//
//  ClientMessage.swift
//  LiveLikeSDK
//
//  Created by Cory Sullivan on 2019-01-24.
//

import Foundation

enum ClientEvent: CustomStringConvertible, Decodable {
    /// Enum outlining the valid event names we can receive from
    /// the messaging service (e.g. PubNub)
    enum EventName: String, Codable {
        // Widget Events
        case textPredictionCreated = "text-prediction-created"
        case textPredictionResults = "text-prediction-results"
        case textPredictionFollowUpCreated = "text-prediction-follow-up-updated"
        case imagePredictionCreated = "image-prediction-created"
        case imagePredictionResults = "image-prediction-results"
        case imagePredictionFollowUpCreated = "image-prediction-follow-up-updated"
        case imagePollCreated = "image-poll-created"
        case imagePollResults = "image-poll-results"
        case textPollCreated = "text-poll-created"
        case textPollResults = "text-poll-results"
        case alertCreated = "alert-created"
        case textQuizCreated = "text-quiz-created"
        case textQuizResults = "text-quiz-results"
        case imageQuizCreated = "image-quiz-created"
        case imageQuizResults = "image-quiz-results"
        case imageSliderCreated = "emoji-slider-created"
        case imageSliderResults = "emoji-slider-results"
        case cheerMeterCreated = "cheer-meter-created"
        case cheerMeterResults = "cheer-meter-results"
        case socialEmbedCreated = "social-embed-created"
        case videoAlertCreated = "video-alert-created"
        case textAskCreated = "text-ask-created"
        case textNumberPredictionCreated = "text-number-prediction-created"
        case imageNumberPredictionCreated = "image-number-prediction-created"
        case textNumberPredictionFollowUpCreated = "text-number-prediction-follow-up-updated"
        case imageNumberPredictionFollowUpCreated = "image-number-prediction-follow-up-updated"
    }

    enum CodingKeys: CodingKey {
        case event
        case payload
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let event = try container.decode(EventName.self, forKey: .event)

        switch event {
        case .textPredictionCreated,
             .textPredictionFollowUpCreated,
             .imagePredictionCreated,
             .imagePredictionFollowUpCreated,
             .imagePollCreated,
             .textPollCreated,
             .alertCreated,
             .textQuizCreated,
             .imageQuizCreated,
             .imageSliderCreated,
             .cheerMeterCreated,
             .socialEmbedCreated,
             .videoAlertCreated,
             .textAskCreated,
             .textNumberPredictionCreated,
             .imageNumberPredictionCreated,
             .textNumberPredictionFollowUpCreated,
             .imageNumberPredictionFollowUpCreated:
            self = try .widget(container.decode(WidgetResource.self, forKey: .payload))
        case .textPredictionResults:
            self = try .textPredictionResults(container.decode(PredictionResults.self, forKey: .payload))
        case .imagePredictionResults:
            self = try .imagePredictionResults(container.decode(PredictionResults.self, forKey: .payload))
        case .imagePollResults, .textPollResults:
            self = try .imagePollResults(container.decode(PollResults.self, forKey: .payload))
        case .textQuizResults:
            self = try .textQuizResults(container.decode(QuizResults.self, forKey: .payload))
        case .imageQuizResults:
            self = try .imageQuizResults(container.decode(QuizResults.self, forKey: .payload))
        case .imageSliderResults:
            self = try .imageSliderResults(container.decode(ImageSliderResults.self, forKey: .payload))
        case .cheerMeterResults:
            self = try .cheerMeterResults(container.decode(CheerMeterResults.self, forKey: .payload))
        }
    }

    case widget(WidgetResource)
    case textPredictionResults(PredictionResults)
    case imagePredictionResults(PredictionResults)
    case imagePollResults(PollResults)
    case textQuizResults(QuizResults)
    case imageQuizResults(QuizResults)
    case imageSliderResults(ImageSliderResults)
    case cheerMeterResults(CheerMeterResults)

    var description: String {
        switch self {
        case .widget(let resource):
            return resource.description
        case .imagePollResults:
            return "Image Poll Results"
        case .textQuizResults:
            return "Text Quiz Results"
        case .imageQuizResults:
            return "Image Quiz Results"
        case .imageSliderResults:
            return "Image Slider Results"
        case .cheerMeterResults:
            return "Cheer Meter Results"
        case .textPredictionResults:
            return "Text Prediction Results"
        case .imagePredictionResults:
            return "Image Prediction Results"
        }
    }

    var minimumScheduledTime: EpochTime? {
        switch self {
        case .widget(let resource):
            return resource.minimumScheduledTime
        case .imagePollResults,
             .textQuizResults,
             .imageQuizResults,
             .imageSliderResults,
             .cheerMeterResults,
             .textPredictionResults,
             .imagePredictionResults:
            return nil
        }
    }
}
