//
//  WidgetResource.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 10/27/20.
//

import Foundation
import LiveLikeCore

enum WidgetResource: CustomStringConvertible, Decodable {

    enum CodingKeys: CodingKey {
        case kind
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let kind = try container.decode(WidgetKind.self, forKey: .kind)
        switch kind {
        case .textPrediction:
            self = try .textPredictionCreated(TextPredictionCreated(from: decoder))
        case .textPredictionFollowUp:
            self = try .textPredictionFollowUp(TextPredictionFollowUp(from: decoder))
        case .imagePrediction:
            self = try .imagePredictionCreated(ImagePredictionCreated(from: decoder))
        case .imagePredictionFollowUp:
            self = try .imagePredictionFollowUp(ImagePredictionFollowUp(from: decoder))
        case .imagePoll:
            self = try .imagePollCreated(ImagePollCreated(from: decoder))
        case .textPoll:
            self = try .textPollCreated(TextPollCreated(from: decoder))
        case .alert:
            self = try .alertCreated(AlertCreated(from: decoder))
        case .textQuiz:
            self = try .textQuizCreated(TextQuizCreated(from: decoder))
        case .imageQuiz:
            self = try .imageQuizCreated(ImageQuizCreated(from: decoder))
        case .imageSlider:
            self = try .imageSliderCreated(ImageSliderCreated(from: decoder))
        case .cheerMeter:
            self = try .cheerMeterCreated(CheerMeterCreated(from: decoder))
        case .socialEmbed:
            self = try .socialEmbed(SocialEmbedWidgetResource(from: decoder))
        case .videoAlert:
            self = try .videoAlert(VideoWidgetResource(from: decoder))
        case .textAsk:
            self = try .textAsk(TextAskWidgetResource(from: decoder))
        case .textNumberPrediction:
            self = try .numberPrediction(NumberPrediction(from: decoder))
        case .imageNumberPrediction:
            self = try .numberPrediction(NumberPrediction(from: decoder))
        case .textNumberPredictionFollowUp:
            self = try .numberPredictionFollowUp(NumberPredictionFollowUp(from: decoder))
        case .imageNumberPredictionFollowUp:
            self = try .numberPredictionFollowUp(NumberPredictionFollowUp(from: decoder))
        case .richPost:
            self = try .richPost(RichPost(from: decoder))
        }
    }

    case textPredictionCreated(TextPredictionCreated)
    case textPredictionFollowUp(TextPredictionFollowUp)
    case imagePredictionCreated(ImagePredictionCreated)
    case imagePredictionFollowUp(ImagePredictionFollowUp)
    case imagePollCreated(ImagePollCreated)
    case textPollCreated(TextPollCreated)
    case alertCreated(AlertCreated)
    case textQuizCreated(TextQuizCreated)
    case imageQuizCreated(ImageQuizCreated)
    case imageSliderCreated(ImageSliderCreated)
    case cheerMeterCreated(CheerMeterCreated)
    case socialEmbed(SocialEmbedWidgetResource)
    case videoAlert(VideoWidgetResource)
    case textAsk(TextAskWidgetResource)
    case numberPrediction(NumberPrediction)
    case numberPredictionFollowUp(NumberPredictionFollowUp)
    case richPost(RichPost)

    var id: String {
        switch self {
        case let .textPredictionCreated(payload):
            return payload.id
        case let .textPredictionFollowUp(payload):
            return payload.id
        case let .imagePredictionCreated(payload):
            return payload.id
        case let .imagePredictionFollowUp(payload):
            return payload.id
        case let .imagePollCreated(payload):
            return payload.id
        case let .textPollCreated(payload):
            return payload.id
        case let .alertCreated(payload):
            return payload.id
        case let .textQuizCreated(payload):
            return payload.id
        case let .imageQuizCreated(payload):
            return payload.id
        case let .imageSliderCreated(payload):
            return payload.id
        case let .cheerMeterCreated(payload):
            return payload.id
        case let .socialEmbed(payload):
            return payload.id
        case let .videoAlert(payload):
            return payload.id
        case let .textAsk(payload):
            return payload.id
        case let .numberPrediction(payload):
            return payload.id
        case let .numberPredictionFollowUp(payload):
            return payload.id
        case let .richPost(payload):
            return payload.id
        }
    }

    var kind: WidgetKind {
        switch self {
        case let .textPredictionCreated(payload):
            return payload.kind
        case let .textPredictionFollowUp(payload):
            return payload.kind
        case let .imagePredictionCreated(payload):
            return payload.kind
        case let .imagePredictionFollowUp(payload):
            return payload.kind
        case let .imagePollCreated(payload):
            return payload.kind
        case let .textPollCreated(payload):
            return payload.kind
        case let .alertCreated(payload):
            return payload.kind
        case let .textQuizCreated(payload):
            return payload.kind
        case let .imageQuizCreated(payload):
            return payload.kind
        case let .imageSliderCreated(payload):
            return payload.kind
        case let .cheerMeterCreated(payload):
            return payload.kind
        case let .socialEmbed(payload):
            return payload.kind
        case let .videoAlert(payload):
            return payload.kind
        case let .textAsk(payload):
            return payload.kind
        case let .numberPrediction(payload):
            return payload.kind
        case let .numberPredictionFollowUp(payload):
            return payload.kind
        case let .richPost(payload):
            return payload.kind
        }
    }

    var programID: String {
        switch self {
        case let .textPredictionCreated(payload):
            return payload.programID
        case let .textPredictionFollowUp(payload):
            return payload.programID
        case let .imagePredictionCreated(payload):
            return payload.programID
        case let .imagePredictionFollowUp(payload):
            return payload.programID
        case let .imagePollCreated(payload):
            return payload.programID
        case let .textPollCreated(payload):
            return payload.programID
        case let .alertCreated(payload):
            return payload.programID
        case let .textQuizCreated(payload):
            return payload.programID
        case let .imageQuizCreated(payload):
            return payload.programID
        case let .imageSliderCreated(payload):
            return payload.programID
        case let .cheerMeterCreated(payload):
            return payload.programID
        case let .socialEmbed(payload):
            return payload.programID
        case let .videoAlert(payload):
            return payload.programID
        case let .textAsk(payload):
            return payload.programID
        case let .numberPrediction(payload):
            return payload.programID
        case let .numberPredictionFollowUp(payload):
            return payload.programID
        case let .richPost(payload):
            return payload.programID
        }
    }

    var minimumScheduledTime: EpochTime? {
        switch self {
        case let .textPredictionCreated(payload):
            return payload.programDateTime?.timeIntervalSince1970.rounded()
        case let .textPredictionFollowUp(payload):
            return payload.programDateTime?.timeIntervalSince1970.rounded()
        case let .imagePredictionCreated(payload):
            return payload.programDateTime?.timeIntervalSince1970.rounded()
        case let .imagePredictionFollowUp(payload):
            return payload.programDateTime?.timeIntervalSince1970.rounded()
        case let .imagePollCreated(payload):
            return payload.programDateTime?.timeIntervalSince1970.rounded()
        case let .textPollCreated(payload):
            return payload.programDateTime?.timeIntervalSince1970.rounded()
        case let .alertCreated(payload):
            return payload.programDateTime?.timeIntervalSince1970.rounded()
        case let .textQuizCreated(payload):
            return payload.programDateTime?.timeIntervalSince1970.rounded()
        case let .imageQuizCreated(payload):
            return payload.programDateTime?.timeIntervalSince1970.rounded()
        case let .imageSliderCreated(payload):
            return payload.programDateTime?.timeIntervalSince1970.rounded()
        case let .cheerMeterCreated(payload):
            return payload.programDateTime?.timeIntervalSince1970.rounded()
        case let .socialEmbed(payload):
            return payload.programDateTime?.timeIntervalSince1970.rounded()
        case let .videoAlert(payload):
            return payload.programDateTime?.timeIntervalSince1970.rounded()
        case let .textAsk(payload):
            return payload.programDateTime?.timeIntervalSince1970.rounded()
        case let .numberPrediction(payload):
            return payload.programDateTime?.timeIntervalSince1970.rounded()
        case let .numberPredictionFollowUp(payload):
            return payload.programDateTime?.timeIntervalSince1970.rounded()
        case let .richPost(payload):
            return payload.programDateTime?.timeIntervalSince1970.rounded()
        }
    }

    var description: String {
        switch self {
        case let .textPredictionCreated(payload):
            return ("\(payload.kind.stringValue) Titled: \(payload.question)")
        case let .textPredictionFollowUp(payload):
            return ("\(payload.kind.stringValue) Titled: \(payload.question)")
        case let .imagePredictionCreated(payload):
            return "\(payload.kind.stringValue) Titled: \(payload.question)"
        case let .imagePredictionFollowUp(payload):
            return "\(payload.kind.stringValue) Titled: \(payload.question)"
        case let .imagePollCreated(payload):
            return "\(payload.kind.stringValue) Titled: \(payload.question)"
        case let .textPollCreated(payload):
            return "\(payload.kind.stringValue) Titled: \(payload.question)"
        case .alertCreated:
            return "Alert Created Widget"
        case let .textQuizCreated(payload):
            return "\(payload.kind.stringValue) Titled: \(payload.question)"
        case let .imageQuizCreated(payload):
            return "\(payload.kind.stringValue) Titled: \(payload.question)"
        case let .imageSliderCreated(payload):
            return "\(payload.kind.stringValue) Titled: \(payload.question)"
        case let .cheerMeterCreated(payload):
            return "\(payload.kind.stringValue) Titled: \(payload.question)"
        case let .socialEmbed(payload):
            return "\(payload.kind.stringValue) Titled: \(String(describing: payload.comment))"
        case let .videoAlert(payload):
            return "\(payload.kind.stringValue)"
        case let .textAsk(payload):
            return "\(payload.kind.stringValue)"
        case let .numberPrediction(payload):
            return "\(payload.kind.stringValue)"
        case let .numberPredictionFollowUp(payload):
            return "\(payload.kind.stringValue)"
        case let .richPost(payload):
            return "\(payload.kind.stringValue)"
        }
    }

    var baseWidgetResource: BaseWidgetResource {
        switch self {
        case let .textPredictionCreated(payload):
            return payload
        case let .textPredictionFollowUp(payload):
            return payload
        case let .imagePredictionCreated(payload):
            return payload
        case let .imagePredictionFollowUp(payload):
            return payload
        case let .imagePollCreated(payload):
            return payload
        case let .textPollCreated(payload):
            return payload
        case let .alertCreated(payload):
            return payload
        case let .textQuizCreated(payload):
            return payload
        case let .imageQuizCreated(payload):
            return payload
        case let .imageSliderCreated(payload):
            return payload
        case let .cheerMeterCreated(payload):
            return payload
        case let .socialEmbed(payload):
            return payload
        case let .videoAlert(payload):
            return payload
        case let .textAsk(payload):
            return payload
        case let .numberPrediction(payload):
            return payload
        case let .numberPredictionFollowUp(payload):
            return payload
        case let .richPost(payload):
            return payload
        }
    }


    static func getWidgetInteractionsURL(fromTemplate template: String, profileID: String) throws -> URL {
        let stringToReplace = "{profile_id}"
        guard template.contains(stringToReplace) else {
            log.error("Failed to find string to replace in widgetInteractionsURLTemplate")
            throw WidgetInteractionErrors.invalidUnclaimedInteractionURLTemplate
        }
        let urlTemplateFilled = template.replacingOccurrences(
            of: stringToReplace,
            with: profileID
        )
        guard let url = URL(string: urlTemplateFilled) else {
            throw WidgetInteractionErrors.invalidUnclaimedInteractionURL
        }
        return url
    }
}
