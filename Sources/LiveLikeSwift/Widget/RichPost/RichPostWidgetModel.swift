//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

/// A model containing the data of a Rich Post Widget
public class RichPostWidgetModel: BaseWidgetModel {

    /// The html content of the Rich Post Widget
    public let content: String
    /// The optional title of the Rich Post Widget
    public let title: String?

    init(
        richPost: RichPost,
        eventRecorder: EventRecorder,
        livelikeAPI: LiveLikeWidgetAPIProtocol,
        userProfile: UserProfileProtocol
    ) {
        self.content = richPost.content
        self.title = richPost.title
        super.init(
            baseData: richPost,
            eventRecorder: eventRecorder,
            livelikeAPI: livelikeAPI,
            userProfile: userProfile
        )
    }
}
