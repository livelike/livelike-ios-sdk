//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

class RichPost: BaseWidgetResource {
    let content: String
    let title: String?

    enum CodingKeys: CodingKey {
        case content
        case title
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.content = try container.decode(String.self, forKey: .content)
        self.title = try container.decodeIfPresent(String.self, forKey: .title)
        try super.init(from: decoder)
    }
}
