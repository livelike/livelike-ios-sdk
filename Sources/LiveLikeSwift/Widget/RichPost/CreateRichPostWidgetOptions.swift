//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

/// Options for creating a Rich Post Widget
public struct CreateRichPostWidgetOptions {
    public let common: CommonCreateWidgetOptions
    public let title: String?
    public let content: String


    /// Options for creating a Rich Post Widget
    /// - Parameters:
    ///   - content: The HTML content of the Widget
    ///   - title: An optional title describing the Widget
    public init(
        common: CommonCreateWidgetOptions,
        content: String,
        title: String? = nil
    ) {
        self.common = common
        self.content = content
        self.title = title
    }
}

extension CreateRichPostWidgetOptions: Encodable {
    enum CodingKeys: CodingKey {
        case content
        case title
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(title, forKey: .title)
        try container.encode(content, forKey: .content)
        try common.encode(to: encoder)
    }
}
