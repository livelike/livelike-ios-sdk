//
//  CommonCreateWidgetOptions.swift
//  
//
//  Created by Jelzon Monzon on 8/22/23.
//

import Foundation

public struct CommonCreateWidgetOptions {
    /// - Parameters:
    ///   - programID: The id of the Program to create the Widget
    ///   - timeoutSeconds: Optional hint in seconds for when a Widget should be timed out
    ///   - customData: Optional string to attach additional data to a Widget
    ///   - interactiveUntil: Optional date for which the Widget should be interactive until
    ///   - playbackTimeMilliseconds: Optional hint in milliseconds for when a Widget should be synced to (eg. video playback)
    ///   - widgetAttributes: Optional array of key-value pairs to attach additional data to a Widget
    ///   - sponsorIDs: Optional array of ids of Sponsors of the Widget
    public init(
        programID: String,
        timeoutSeconds: UInt? = nil,
        customData: String? = nil,
        interactiveUntil: Date? = nil,
        playbackTimeMilliseconds: UInt? = nil,
        widgetAttributes: [Attribute]? = nil,
        sponsorIDs: [String]? = nil
    ) {
        self.programID = programID
        self.timeoutSeconds = timeoutSeconds
        self.customData = customData
        self.interactiveUntil = interactiveUntil
        self.playbackTimeMilliseconds = playbackTimeMilliseconds
        self.widgetAttributes = widgetAttributes
        self.sponsorIDs = sponsorIDs
    }

    public let programID: String

    public let timeoutSeconds: UInt?
    public let customData: String?
    public let interactiveUntil: Date?
    public let playbackTimeMilliseconds: UInt?
    public let widgetAttributes: [Attribute]?
    public let sponsorIDs: [String]?
}

extension CommonCreateWidgetOptions: Encodable {
    enum CodingKeys: String, CodingKey {
        case programID = "programId"
        case timeout
        case customData
        case interactiveUntil
        case playbackTimeMilliseconds = "playbackTimeMs"
        case widgetAttributes
        case sponsorIds
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(programID, forKey: .programID)

        if let timeoutSeconds = self.timeoutSeconds {
            try container.encode(
                "P0DT00H00M\(timeoutSeconds)S",
                forKey: .timeout
            )
        }

        if let interactiveUntil = self.interactiveUntil {
            try container.encodeIfPresent(
                DateFormatter.iso8601FullWithPeriod.string(from: interactiveUntil),
                forKey: .interactiveUntil
            )
        }

        try container.encodeIfPresent(customData, forKey: .customData)
        try container.encodeIfPresent(playbackTimeMilliseconds, forKey: .playbackTimeMilliseconds)
        try container.encodeIfPresent(widgetAttributes, forKey: .widgetAttributes)
        try container.encodeIfPresent(sponsorIDs, forKey: .sponsorIds)
    }
}
