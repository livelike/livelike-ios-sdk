//
//  QuizWidgetModel.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 7/2/20.
//

import Foundation
import LiveLikeCore

/// Methods for observing changes to a `QuizWidgetModel`
public protocol QuizWidgetModelDelegate: AnyObject {
    /// Called when the sends updated vote counts
    func quizWidgetModel(
        _ model: QuizWidgetModel,
        answerCountDidChange answerCount: Int,
        forChoice choiceID: String
    )
}

/// An object that reflects the state of a Quiz widget on the server
public class QuizWidgetModel: BaseWidgetModel, QuizWidgetModelable {

    /// The object that acts as the delegate for the `QuizWidgetModel`.
    public weak var delegate: QuizWidgetModelDelegate?

    // MARK: Data

    /// The question of the Quiz
    public let question: String
    /// The choices of the Quiz
    public let choices: [Choice]
    /// Does the Quiz contain images
    public let containsImages: Bool
    /// The updated total count of answers on the Quiz
    @Atomic public internal(set) var totalAnswerCount: Int
    /// The user's answers on this Quiz
    /// Call `loadInteractionHistory` to fetch votes before using
    public var userAnswers: [Answer] {
        return self.widgetInteractionRepo.get(widgetID: self.id)?.quizAnswers ?? []
    }
    /// The user's answer with the most recent createdAt date
    public var mostRecentAnswer: Answer? {
        return userAnswers.max(by: { $0.createdAt <= $1.createdAt })
    }

    /// Sponsors linked to the Quiz Widget
    public let sponsors: [Sponsor]

    // MARK: Private Properties

    private let widgetClient: PubSubWidgetClient?
    private let rewardItems: [RewardItem]
    private let leaderboardsManager: LeaderboardsManager
    private let impressionURL: URL?
    private var isLockInAnswerInProgress: Bool = false
    let interactionURL: URL?
    private let widgetInteractionRepo: WidgetInteractionRepository

    init(
        data: TextQuizCreated,
        eventRecorder: EventRecorder,
        userProfile: UserProfileProtocol,
        rewardItems: [RewardItem],
        leaderboardsManager: LeaderboardsManager,
        widgetClient: PubSubWidgetClient?,
        livelikeAPI: LiveLikeWidgetAPIProtocol,
        widgetInteractionRepo: WidgetInteractionRepository
    ) {
        self.question = data.question
        self.choices = data.choices.map { Choice(data: $0) }
        self.totalAnswerCount = data.choices.map { $0.answerCount }.reduce(0, +)
        self.rewardItems = rewardItems
        self.leaderboardsManager = leaderboardsManager
        self.containsImages = false
        self.widgetClient = widgetClient
        self.impressionURL = data.impressionURL
        self.interactionURL = try? WidgetResource.getWidgetInteractionsURL(
            fromTemplate: data.widgetInteractionsUrlTemplate,
            profileID: userProfile.userID.asString
        )
        self.widgetInteractionRepo = widgetInteractionRepo
        self.sponsors = data.sponsors

        super.init(
            baseData: data,
            eventRecorder: eventRecorder,
            livelikeAPI: livelikeAPI,
            userProfile: userProfile
        )

        if let subscribeChannel = subscribeChannel {
            self.widgetClient?.addListener(self, toChannel: subscribeChannel)
        }
    }

    init(
        data: ImageQuizCreated,
        eventRecorder: EventRecorder,
        userProfile: UserProfileProtocol,
        rewardItems: [RewardItem],
        leaderboardsManager: LeaderboardsManager,
        widgetClient: PubSubWidgetClient?,
        livelikeAPI: LiveLikeWidgetAPIProtocol,
        widgetInteractionRepo: WidgetInteractionRepository
    ) {
        self.question = data.question
        self.choices = data.choices.map { Choice(data: $0) }
        self.totalAnswerCount = data.choices.map { $0.answerCount }.reduce(0, +)
        self.rewardItems = rewardItems
        self.leaderboardsManager = leaderboardsManager
        self.containsImages = true
        self.widgetClient = widgetClient
        self.impressionURL = data.impressionURL
        self.interactionURL = try? WidgetResource.getWidgetInteractionsURL(
            fromTemplate: data.widgetInteractionsUrlTemplate,
            profileID: userProfile.userID.asString
        )
        self.widgetInteractionRepo = widgetInteractionRepo
        self.sponsors = data.sponsors

        super.init(
            baseData: data,
            eventRecorder: eventRecorder,
            livelikeAPI: livelikeAPI,
            userProfile: userProfile
        )

        if let subscribeChannel = subscribeChannel {
            self.widgetClient?.addListener(self, toChannel: subscribeChannel)
        }
    }

    // MARK: Methods

    /// Call this to load the user's interaction history for this Widget
    /// - Parameter completion: Returns the loaded answers
    public func loadInteractionHistory(completion: @escaping (Result<[Answer], Error>) -> Void) {
        self.widgetInteractionRepo.get(widgetModel: .quiz(self)) { result in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let interactions):
                completion(.success(interactions.quizAnswers))
            }   
        }
    }

    /// Locks in an answer for the Quiz. Call this when the user has made their final decision. Once an answer is locked it cannot be changed.
    public func lockInAnswer(choiceID: String, completion: @escaping (Result<Answer, Error>) -> Void) {
        self.eventRecorder.record(
            .widgetEngaged(widgetModel: .quiz(self))
        )

        guard !isLockInAnswerInProgress else {
            completion(.failure(QuizWidgetModelError.concurrentLockInAnswer))
            return
        }
        guard let answerURL = choices.first(where: { $0.id == choiceID})?.answerURL else {
            completion(.failure(QuizWidgetModelError.invalidChoiceID(choiceID)))
            return
        }

        self.isLockInAnswerInProgress = true
        firstly {
            livelikeAPI.createQuizAnswer(answerURL: answerURL, accessToken: self.userProfile.accessToken)
        }.then { answer in
            self.widgetInteractionRepo.add(interactions: [.quizAnswer(answer)])
            self.leaderboardsManager.notifyCurrentPositionChange(rewards: answer.rewards)
            self.isLockInAnswerInProgress = false
            completion(.success(answer))
        }.catch { error in
            self.isLockInAnswerInProgress = false
            completion(.failure(error))
        }
    }

    // MARK: Types

    enum QuizWidgetModelError: LocalizedError {
        case concurrentLockInAnswer
        case invalidChoiceID(String)

        var errorDescription: String? {
            switch self {
            case .concurrentLockInAnswer:
                return "Cannot make concurrent calls to `lockInAnswer`. Wait until the `completion` is called before calling again."
            case .invalidChoiceID(let choiceID):
                return "Could not find option with id \(choiceID)"
            }
        }
    }

    /// A Quiz choice
    public class Choice {
        /// The id of the Choice. Use this to `lockInAnswer`.
        public let id: String
        /// The text of the Choice
        public let text: String
        /// Whether or not the Choice is correct
        public let isCorrect: Bool
        /// The image of the Choice
        public let imageURL: URL?
        /// The updated total answer count
        @Atomic public internal(set) var answerCount: Int

        let answerURL: URL

        init(data: TextQuizChoice) {
            self.id = data.id
            self.text = data.description
            self.isCorrect = data.isCorrect
            self.answerURL = data.answerUrl
            self.imageURL = nil
            self.answerCount = data.answerCount
        }

        init(data: ImageQuizChoice) {
            self.id = data.id
            self.text = data.description
            self.isCorrect = data.isCorrect
            self.answerCount = data.answerCount
            self.answerURL = data.answerUrl
            self.imageURL = data.imageUrl
        }
    }

    /// An object representing the user's Answer to a Quiz
    public struct Answer: Decodable {
        /// The id of this answer
        public let id: String
        /// The id of the choice of their Answer
        public let choiceID: String
        /// The date that this Answer was created
        public let createdAt: Date

        let widgetID: String
        let url: URL
        let isCorrect: Bool
        let rewards: [RewardResource]
        let widgetKind: WidgetKind

        enum CodingKeys: String, CodingKey {
            case id
            case widgetId
            case url
            case choiceId
            case isCorrect
            case rewards
            case createdAt
            case widgetKind
        }

        public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.id = try container.decode(String.self, forKey: .id)
            self.widgetID = try container.decode(String.self, forKey: .widgetId)
            self.url = try container.decode(URL.self, forKey: .url)
            self.choiceID = try container.decode(String.self, forKey: .choiceId)
            self.isCorrect = try container.decode(Bool.self, forKey: .isCorrect)
            self.rewards = try container.decodeIfPresent([RewardResource].self, forKey: .rewards) ?? []
            self.createdAt = try container.decode(Date.self, forKey: .createdAt)
            self.widgetKind = try container.decode(WidgetKind.self, forKey: .widgetKind)
        }

        internal init(id: String, widgetID: String, url: URL, choiceID: String, isCorrect: Bool, rewards: [RewardResource], createdAt: Date, widgetKind: WidgetKind) {
            self.id = id
            self.widgetID = widgetID
            self.url = url
            self.choiceID = choiceID
            self.isCorrect = isCorrect
            self.rewards = rewards
            self.createdAt = createdAt
            self.widgetKind = widgetKind
        }
    }
}

// MARK: WidgetProxyInput

/// :nodoc:
extension QuizWidgetModel: WidgetProxyInput {
    func publish(event: WidgetProxyPublishData) {
        switch event.clientEvent {
        case let .textQuizResults(results), let .imageQuizResults(results):
            guard results.id == self.id else { return }
            // Update model
            self.totalAnswerCount = results.choices.map { $0.answerCount }.reduce(0, +)
            results.choices.forEach { choice in
                self.choices.first(where: { $0.id == choice.id})?.answerCount = choice.answerCount
            }
            // Notify delegate
            guard let delegate = self.delegate else { return }
            results.choices.forEach { choice in
                delegate.quizWidgetModel(self, answerCountDidChange: choice.answerCount, forChoice: choice.id)
            }
        default:
            log.verbose("Received event \(event.clientEvent.description) in QuizWidgetLiveResultsClient when only .textQuizResults were expected.")
        }
    }

    func discard(event: WidgetProxyPublishData, reason: DiscardedReason) {}
    func connectionStatusDidChange(_ status: ConnectionStatus) {}

    func error(_ error: Error) {
        log.error(error.localizedDescription)
    }
}
