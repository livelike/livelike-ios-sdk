//
//  PredictionFollowUpWidgetModel.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 11/19/20.
//

import Foundation
import LiveLikeCore

/// An object that reflects the state of a Prediction Follow Up widget on the server
public class PredictionFollowUpWidgetModel: BaseWidgetModel, PredictionFollowUpWidgetModelable {

    // MARK: Data

    /// The question of the Prediction Follow Up
    public let question: String
    /// The options of the Prediction Follow Up
    public let options: [Option]
    /// Does the Prediction Follow Up widget contain images
    public let containsImages: Bool
    /// The user's votes on this Prediction
    /// Call `loadInteractionHistory` to fetch votes before using
    public var userVotes: [PredictionVote] {
        return self.widgetInteractionRepo.get(widgetID: self.associatedPredictionID)?.predictionVotes ?? []
    }
    /// The vote with the most recent createdAt date
    public var mostRecentVote: PredictionVote? {
        return userVotes.max(by: { $0.createdAt <= $1.createdAt })
    }
    // MARK: Metadata

    /// The id of the associated Prediction widget
    public let associatedPredictionID: String
    /// The kind of the associated Prediction widget
    public let associatedPredictionKind: WidgetKind
    /// Sponsors linked to the Prediction Follow Up Widget
    public let sponsors: [Sponsor]

    // MARK: Internal Properties

    let interactionURL: URL?

    // MARK: Private Properties

    private let leaderboardAPI: LiveLikeLeaderboardAPIProtocol
    private let rewardItems: [RewardItem]
    private let leaderboardsManager: LeaderboardsManager
    private let impressionURL: URL?
    private let claimURL: URL
    private let voteRepo: PredictionVoteRepository
    private let widgetInteractionRepo: WidgetInteractionRepository
    private var isGetVoteInProgress: Bool = false

    init(
        resource: TextPredictionFollowUp,
        eventRecorder: EventRecorder,
        livelikeAPI: LiveLikeWidgetAPIProtocol,
        leaderboardAPI: LiveLikeLeaderboardAPIProtocol,
        rewardItems: [RewardItem],
        leaderboardsManager: LeaderboardsManager,
        userProfile: UserProfileProtocol,
        voteRepo: PredictionVoteRepository,
        widgetInteractionRepo: WidgetInteractionRepository
    ) {
        self.question = resource.question
        self.options = resource.options.map { Option(resource: $0) }
        self.containsImages = false
        self.impressionURL = resource.impressionURL
        self.claimURL = resource.claimUrl
        self.leaderboardAPI = leaderboardAPI
        self.rewardItems = rewardItems
        self.leaderboardsManager = leaderboardsManager
        self.associatedPredictionID = resource.textPredictionId
        self.associatedPredictionKind = .textPrediction
        self.voteRepo = voteRepo
        self.interactionURL = try? WidgetResource.getWidgetInteractionsURL(
            fromTemplate: resource.widgetInteractionsUrlTemplate,
            profileID: userProfile.userID.asString
        )
        self.widgetInteractionRepo = widgetInteractionRepo
        self.sponsors = resource.sponsors
        super.init(
            baseData: resource,
            eventRecorder: eventRecorder,
            livelikeAPI: livelikeAPI,
            userProfile: userProfile
        )
    }

    init(
        resource: ImagePredictionFollowUp,
        eventRecorder: EventRecorder,
        livelikeAPI: LiveLikeWidgetAPIProtocol,
        leaderboardAPI: LiveLikeLeaderboardAPIProtocol,
        rewardItems: [RewardItem],
        leaderboardsManager: LeaderboardsManager,
        userProfile: UserProfileProtocol,
        voteRepo: PredictionVoteRepository,
        widgetInteractionRepo: WidgetInteractionRepository
    ) {
        self.question = resource.question
        self.options = resource.options.map { Option(resource: $0) }
        self.containsImages = true
        self.impressionURL = resource.impressionURL
        self.claimURL = resource.claimUrl
        self.leaderboardAPI = leaderboardAPI
        self.rewardItems = rewardItems
        self.leaderboardsManager = leaderboardsManager
        self.associatedPredictionID = resource.imagePredictionId
        self.associatedPredictionKind = .imagePrediction
        self.voteRepo = voteRepo
        self.interactionURL = resource.interactionURL
        self.widgetInteractionRepo = widgetInteractionRepo
        self.sponsors = resource.sponsors

        super.init(
            baseData: resource,
            eventRecorder: eventRecorder,
            livelikeAPI: livelikeAPI,
            userProfile: userProfile
        )
    }

    // MARK: Methods

    /// Call this to load the user's interaction history for the associated Prediction Widget
    /// - Parameter completion: Returns the loaded votes
    public func loadInteractionHistory(completion: @escaping (Result<[PredictionVote], Error>) -> Void = { _ in }) {
        self.widgetInteractionRepo.get(widgetModel: .predictionFollowUp(self)) { result in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let interactions):
                completion(.success(interactions.predictionVotes))
            }
        }
    }

    /// Returns the User's `PredictionVote` if any exists.
    @available(*, deprecated, message: "Use loadInteractionHistory instead.")
    public func getVote(completion: @escaping (Result<PredictionVote, Error>) -> Void) {
        guard !isGetVoteInProgress else {
            completion(.failure(PredictionFollowUpModelErrors.concurrentGetVote))
            return
        }
        self.isGetVoteInProgress = true
        voteRepo.get(by: associatedPredictionID) { [weak self] vote in
            guard let self = self else { return }
            guard let vote = vote else {
                completion(.failure(PredictionFollowUpModelErrors.couldNotFindVote))
                return
            }
            completion(.success(vote))
            self.isGetVoteInProgress = false
        }
    }

    /// Call this method with the user's `PredictionVote` to claim the rewards the user has earned
    public func claimRewards(vote: PredictionVote, completion: @escaping ((Result<[Reward], Error>) -> Void) = { _ in }) {
        guard let claimToken = vote.claimToken else {
            completion(.failure(PredictionFollowUpModelErrors.claimTokenNotFound))
            return
        }
        firstly {
            self.leaderboardAPI.claimRewards(
                claimURL: self.claimURL,
                claimToken: claimToken,
                accessToken: self.userProfile.accessToken
            )
        }.then {
            let rewards = Reward.createRewards(
                availableRewardItems: self.rewardItems,
                rewardResources: $0.rewards,
                widgetInfo: .init(id: self.id, kind: self.kind)
            )
            self.leaderboardsManager.notifyCurrentPositionChange(rewards: $0.rewards)
            log.info("Successfully claimed rewards for prediction \(self.id)")
            completion(.success(rewards))
        }.catch { error in
            log.error(error)
            completion(.failure(error))
        }
    }

    // MARK: Types

    /// A Prediction Follow Up Option
    public class Option {
        /// The id of the Option
        public let id: String
        /// The text description of the Option
        public let text: String
        /// The text description of the Option
        public let imageURL: URL?
        /// Is this Option correct
        public let isCorrect: Bool
        /// The final vote count of the Option
        public let voteCount: Int

        init(resource: TextPredictionFollowUpOption) {
            self.id = resource.id
            self.text = resource.description
            self.imageURL = nil
            self.isCorrect = resource.isCorrect
            self.voteCount = resource.voteCount
        }

        init(resource: ImagePredictionFollowUpOption) {
            self.id = resource.id
            self.text = resource.description
            self.imageURL = resource.imageUrl
            self.isCorrect = resource.isCorrect
            self.voteCount = resource.voteCount
        }
    }
}
