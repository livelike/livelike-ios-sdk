//
//  GetWidgetModelsRequestOptions.swift
//  
//
//  Created by Jelzon Monzon on 6/21/23.
//

import Foundation

/// Options parameter which can be used to query the `getWidgetModels()` request
public struct GetWidgetModelsRequestOptions {

    let widgetKind: Set<WidgetKind>?
    let widgetStatus: WidgetStatus?
    let widgetOrdering: WidgetOrdering?
    let interactive: Bool?
    let since: Date?
    let sincePlaybackTimeMilliseconds: Int?
    let untilPlaybackTimeMilliseconds: Int?
    let widgetAttributes: [Attribute]?
    let pageSize: Int?

    /// - Parameters:
    ///   - widgetStatus: filter by widget status
    ///   - widgetKind: filter by widget kind(s)
    ///   - widgetOrdering: order of widgets
    ///   - interactive: filter by widget interactivity
    ///   - since: filter by widget createdAt > since
    ///   - sincePlaybackTime: filter by widget playbackTimeMilliseconds  > sincePlaybackTimeMilliseconds
    ///   - untilPlaybackTime: filter by widget playbackTimeMilliseconds  < untilPlaybackTimeMilliseconds
    ///   - widgetAttributes: filter by custom attributes of the widgets. Multiple attribute filters will be applied in conjunction (AND).
    ///   - pageSize: numbet of widgets per page
    public init(
        widgetStatus: WidgetStatus? = nil,
        widgetKind: Set<WidgetKind>? = nil,
        widgetOrdering: WidgetOrdering? = nil,
        interactive: Bool? = nil,
        since: Date? = nil,
        sincePlaybackTimeMilliseconds: Int? = nil,
        untilPlaybackTimeMilliseconds: Int? = nil,
        widgetAttributes: [Attribute]? = nil,
        pageSize: Int? = nil
    ) {
        self.widgetKind = widgetKind
        self.widgetStatus = widgetStatus
        self.widgetOrdering = widgetOrdering
        self.interactive = interactive
        self.since = since
        self.sincePlaybackTimeMilliseconds = sincePlaybackTimeMilliseconds
        self.untilPlaybackTimeMilliseconds = untilPlaybackTimeMilliseconds
        self.widgetAttributes = widgetAttributes
        self.pageSize = pageSize
    }
}
