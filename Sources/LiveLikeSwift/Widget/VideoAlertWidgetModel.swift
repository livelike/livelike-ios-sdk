//
//  VideoAlertWidgetModel.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 7/12/21.
//

import Foundation

/// A model containing the data and methods for a Video Alert Widget
public class VideoAlertWidgetModel: BaseWidgetModel {

    // MARK: Data

    /// The optional title of the Widget
    public let title: String?
    /// The optional text of the Widget
    public let text: String?
    /// The optional text description of a link url
    public let linkText: String?
    /// The optional link url of the Widget
    public let linkURL: URL?
    /// The video url of the Widgewt
    public let videoURL: URL
    /// Sponsors linked to the Video Alert Widget
    public let sponsors: [Sponsor]

    // MARK: Private

    init(
        resource: VideoWidgetResource,
        eventRecorder: EventRecorder,
        widgetAPI: LiveLikeWidgetAPIProtocol,
        userProfile: UserProfileProtocol
    ) {
        self.title = resource.title
        self.text = resource.text
        self.linkText = resource.linkLabel
        self.linkURL = resource.linkURL
        self.videoURL = resource.videoURL
        self.sponsors = resource.sponsors
        super.init(
            baseData: resource,
            eventRecorder: eventRecorder,
            livelikeAPI: widgetAPI,
            userProfile: userProfile
        )
    }

    // MARK: Methods

    /// Call this when the user has opened a link
    public func registerLinkOpened() {
        if let linkURL = self.linkURL {
            eventRecorder.record(
                .widgetEngaged(widgetModel: .videoAlert(self))
            )
            eventRecorder.record(.alertWidgetLinkOpened(alertId: id, programId: programID, linkUrl: linkURL.absoluteString, kind: kind))
        }
    }

    /// Call this when the user has started to play the video
    public func registerPlayStarted() {
        eventRecorder.record(.videoAlertPlayStarted(widgetID: id, programID: programID, widgetKind: kind, videoURL: videoURL))
    }

}
