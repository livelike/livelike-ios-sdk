//
//  NumberPredictionWidgetModel.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 8/30/21.
//

import Foundation
import LiveLikeCore

/// Methods for observing changes to a `NumberPredictionWidgetModel`
public protocol NumberPredictionWidgetModelDelegate: AnyObject {
    /// Notifies the delegate that a follow up to the Number Prediction was published
    func numberPredictionModel(
        _ model: NumberPredictionWidgetModel,
        didReceiveFollowUp id: String,
        kind: WidgetKind
    )
}

class InternalNumberPredictionWidgetModel: BaseWidgetModel, NumberPredictionWidgetModel {

    weak var delegate: NumberPredictionWidgetModelDelegate?
    
    // MARK: - Data
    
    let question: String
    let options: [NumberPredictionOption]
    let confirmationMessage: String
    let containsImages: Bool
    
    var userVotes: [NumberPredictionVote] {
        return self.widgetInteractionRepo.get(widgetID: self.id)?.numberPredictionVotes ?? []
    }
    
    @Atomic private(set) var isFollowUpPublished: Bool
    @Atomic private(set) var followUpWidgetModels: [NumberPredictionFollowUpWidgetModel] = []
    
    // MARK: - Metadata
    
    let interactionURL: URL?
    
    private let voteURL: URL
    private let widgetInteractionRepo: WidgetInteractionRepository
    private let followUpModelFactory: PredictionWidgetModelFactoryProtocol
    private let widgetClient: PubSubWidgetClient?
    private let programSubscribeChannel: String?
    
    init(
        resource: NumberPrediction,
        eventRecorder: EventRecorder,
        widgetAPI: LiveLikeWidgetAPIProtocol,
        userProfile: UserProfileProtocol,
        widgetInteractionRepo: WidgetInteractionRepository,
        followUpModelFactory: PredictionWidgetModelFactoryProtocol,
        widgetClient: PubSubWidgetClient?,
        programSubscribeChannel: String?
    ) {
        self.question = resource.question
        self.options = resource.options
        self.voteURL = resource.voteURL
        self.widgetInteractionRepo = widgetInteractionRepo
        self.interactionURL = try? WidgetResource.getWidgetInteractionsURL(
            fromTemplate: resource.widgetInteractionsURLTemplate,
            profileID: userProfile.userID.asString
        )
        self.isFollowUpPublished = resource.followUps.contains(where: { $0.status == .published})
        self.confirmationMessage = resource.confirmationMessage
        self.containsImages = resource.options.allSatisfy { $0.imageURL != nil }
        self.followUpModelFactory = followUpModelFactory
        self.widgetClient = widgetClient
        self.programSubscribeChannel = programSubscribeChannel
        super.init(
            baseData: resource,
            eventRecorder: eventRecorder,
            livelikeAPI: widgetAPI,
            userProfile: userProfile
        )
        
        self.followUpWidgetModels = resource.followUps.compactMap { followUp in
            return self.followUpModelFactory.makePredictionFollowUpModel(resource: followUp)
        }
        
        if let programSubscribeChannel = programSubscribeChannel {
            self.widgetClient?.addListener(self, toChannel: programSubscribeChannel)
        }
    }
    
    func loadInteractionHistory(completion: @escaping (Result<[NumberPredictionVote], Error>) -> Void = { _ in }) {
        self.widgetInteractionRepo.get(widgetModel: .numberPrediction(self)) { result in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let interactions):
                completion(.success(interactions.numberPredictionVotes))
            }
        }
    }
    
    func lockVotes(
        submissions: [NumberPredictionVoteSubmission],
        completion: @escaping (Result<NumberPredictionVote, Error>) -> Void
    ) {
        self.eventRecorder.record(
            .widgetEngaged(widgetModel: .numberPrediction(self))
        )
        firstly {
            livelikeAPI.createNumberPredictionVote(
                voteURL: self.voteURL,
                votes: submissions,
                accessToken: self.userProfile.accessToken
            )
        }.then { vote in
            self.widgetInteractionRepo.add(interactions: [.numberPredictionVote(vote)])
            completion(.success(vote))
        }.catch { error in
            completion(.failure(error))
        }
    }
}

/// :nodoc:
extension InternalNumberPredictionWidgetModel: WidgetProxyInput {
    func publish(event: WidgetProxyPublishData) {
        switch event.clientEvent {
        case .widget(let widgetResource):
            switch widgetResource {
            case .numberPredictionFollowUp(let payload):
                guard payload.predictionID == id else { return }
                self.isFollowUpPublished = true
                self.followUpWidgetModels.append(self.followUpModelFactory.makePredictionFollowUpModel(resource: payload))
                self.delegate?.numberPredictionModel(
                    self,
                    didReceiveFollowUp: payload.id,
                    kind: payload.kind
                )
            default:
                break
            }
        default:
            log.verbose("Unexpected message payload on this channel.")
        }
    }

    func error(_ error: Error) {
        log.error(error.localizedDescription)
    }

    // Not implemented
    func discard(event: WidgetProxyPublishData, reason: DiscardedReason) {}
    func connectionStatusDidChange(_ status: ConnectionStatus) {}
}
