//
//  PollWidgetModel.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 11/23/20.
//

import Foundation
import LiveLikeCore

/// Methods for observing changes to a `PollWidgetModel`
public protocol PollWidgetModelDelegate: AnyObject {
    /// Called when the server sends updated vote counts
    func pollWidgetModel(
        _ model: PollWidgetModel,
        voteCountDidChange voteCount: Int,
        forOption optionID: String
    )
}

/// An object that represents the Poll widget on the server.
public class PollWidgetModel: BaseWidgetModel, PollWidgetModelable {

    /// The object that acts as the delegate for the `PollWidgetModel`.
    public weak var delegate: PollWidgetModelDelegate?

    // MARK: Data

    /// The `Option`s of the Poll Widget
    public let options: [Option]

    /// The question of the Poll Widget
    public let question: String

    /// The updated total count of votes on the Poll
    @Atomic public internal(set) var totalVoteCount: Int
    /// The user's votes on this Poll
    /// Call `loadInteractionHistory` to fetch votes before using
    public var userVotes: [Vote] {
        return self.widgetInteractionRepo.get(widgetID: self.id)?.pollVotes ?? []
    }
    /// The user's vote with the most recent createdAt date
    public var mostRecentVote: Vote? {
        return userVotes.max(by: { $0.createdAt <= $1.createdAt })
    }

    /// Sponsors linked to the Poll Widget
    public let sponsors: [Sponsor]

    // MARK: Metadata

    public let containsImages: Bool

    // MARK: Internal Properties

    let interactionURL: URL?

    // MARK: Private Properties

    private let rewardItems: [RewardItem]
    private let leaderboardManager: LeaderboardsManager
    private let widgetClient: PubSubWidgetClient?
    private var isVotingInProgress: Bool = false
    private let widgetInteractionRepo: WidgetInteractionRepository

    init(
        data: TextPollCreated,
        userProfile: UserProfileProtocol,
        rewardItems: [RewardItem],
        leaderboardManager: LeaderboardsManager,
        livelikeAPI: LiveLikeWidgetAPIProtocol,
        widgetClient: PubSubWidgetClient?,
        eventRecorder: EventRecorder,
        widgetInteractionRepo: WidgetInteractionRepository
    ) {
        self.rewardItems = rewardItems
        self.leaderboardManager = leaderboardManager
        self.widgetClient = widgetClient
        self.question = data.question
        self.options = data.options.map {
            Option(
                id: $0.id,
                voteURL: $0.voteUrl,
                description: $0.description,
                imageURL: nil,
                voteCount: $0.voteCount
            )
        }
        self.totalVoteCount = data.options.map { $0.voteCount }.reduce(0, +)
        self.containsImages = false
        self.interactionURL = try? WidgetResource.getWidgetInteractionsURL(
            fromTemplate: data.widgetInteractionsUrlTemplate,
            profileID: userProfile.userID.asString
        )
        self.widgetInteractionRepo = widgetInteractionRepo
        self.sponsors = data.sponsors
        super.init(
            baseData: data,
            eventRecorder: eventRecorder,
            livelikeAPI: livelikeAPI,
            userProfile: userProfile
        )

        if let subscribeChannel = subscribeChannel {
            self.widgetClient?.addListener(self, toChannel: subscribeChannel)
        }
    }

    init(
        data: ImagePollCreated,
        userProfile: UserProfileProtocol,
        rewardItems: [RewardItem],
        leaderboardManager: LeaderboardsManager,
        livelikeAPI: LiveLikeWidgetAPIProtocol,
        widgetClient: PubSubWidgetClient?,
        eventRecorder: EventRecorder,
        widgetInteractionRepo: WidgetInteractionRepository
    ) {
        self.rewardItems = rewardItems
        self.leaderboardManager = leaderboardManager
        self.widgetClient = widgetClient
        self.question = data.question
        self.options = data.options.map {
            Option(
                id: $0.id,
                voteURL: $0.voteUrl,
                description: $0.description,
                imageURL: $0.imageUrl,
                voteCount: $0.voteCount
            )
        }
        self.totalVoteCount = data.options.map { $0.voteCount }.reduce(0, +)
        self.containsImages = true
        self.interactionURL = try? WidgetResource.getWidgetInteractionsURL(
            fromTemplate: data.widgetInteractionsUrlTemplate,
            profileID: userProfile.userID.asString
        )
        self.widgetInteractionRepo = widgetInteractionRepo
        self.sponsors = data.sponsors
        super.init(
            baseData: data,
            eventRecorder: eventRecorder,
            livelikeAPI: livelikeAPI,
            userProfile: userProfile
        )

        if let subscribeChannel = subscribeChannel {
            self.widgetClient?.addListener(self, toChannel: subscribeChannel)
        }
    }

    // MARK: Methods

    /// Call this to load the user's interaction history for this Widget
    /// - Parameter completion: Returns the loaded votes
    public func loadInteractionHistory(
        completion: @escaping (Result<[Vote], Error>) -> Void = { _ in }
    ) {
        self.widgetInteractionRepo.get(widgetModel: .poll(self)) { result in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let interactions):
                completion(.success(interactions.pollVotes))
            }
        }
    }

    /// Submit or update an already submitted vote by `optionID`
    public func submitVote(optionID: String, completion: @escaping (Result<PollWidgetModel.Vote, Error>) -> Void = { _ in }) {
        self.eventRecorder.record(
            .widgetEngaged(widgetModel: .poll(self))
        )

        // Avoid repeat vote on a previously voted option
        guard mostRecentVote?.optionID != optionID else {
            log.error("\(PollWidgetModelError.voteAlreadySubmitted.localizedDescription)")
            completion(.failure(PollWidgetModelError.voteAlreadySubmitted))
            return
        }

        guard let voteURL = options.first(where: { $0.id == optionID})?.voteURL else {
            log.error("\(PollWidgetModelError.failedDueToInvalidOptionID.localizedDescription)")
            completion(.failure(PollWidgetModelError.failedDueToInvalidOptionID))
            return
        }

        let votePromise: Promise<Void>
        self.isVotingInProgress = true
        if let mostRecentVote = self.mostRecentVote {
            guard let patchURL = mostRecentVote.patchURL else {
                completion(.failure(PollWidgetModelError.cannotUpdateVote))
                return
            }
            // Update vote
            votePromise = firstly {
                livelikeAPI.updateVoteOnPoll(
                    for: optionID,
                    optionURL: patchURL,
                    accessToken: self.userProfile.accessToken
                )
            }.then { vote in
                self.widgetInteractionRepo.add(interactions: [.pollVote(vote)])
                log.debug("Poll vote successfuly updated for id \(vote.id)")
                completion(.success(vote))
            }.asVoid()
        } else {
            // Create first vote
            votePromise = firstly {
                livelikeAPI.createVoteOnPoll(for: voteURL, accessToken: self.userProfile.accessToken)
            }.then { vote in
                self.widgetInteractionRepo.add(interactions: [.pollVote(vote)])
                self.leaderboardManager.notifyCurrentPositionChange(rewards: vote.rewards)
                log.debug("Poll vote successfuly created for id \(vote.id)")
                completion(.success(vote))
            }.asVoid()
        }

        votePromise.catch { error in
            completion(.failure(error))
            log.error("Failed to submit vote because: \(error.localizedDescription)")
        }.always {
            self.isVotingInProgress = false
        }
    }

    // MARK: Types

    // A Poll option
    public class Option {

        internal init(
            id: String,
            voteURL: URL,
            description: String,
            imageURL: URL?,
            voteCount: Int
        ) {
            self.id = id
            self.voteURL = voteURL
            self.text = description
            self.imageURL = imageURL
            self.voteCount = voteCount
        }

        /// The id of the option. Use this to submit votes.
        public let id: String

        let voteURL: URL

        /// The option's text.
        public let text: String

        /// The option's image url.
        public let imageURL: URL?

        /// The updated total number of votes for this option.
        /// This property is Atomic
        @Atomic public internal(set) var voteCount: Int
    }

    /// An object representing a successfully submitted vote
    public struct Vote: Decodable {
        /// The vote id
        public let id: String
        /// The option ID the user pressed
        public let optionID: String
        /// The date that this Vote was created
        public let createdAt: Date

        public var canBeUpdated: Bool {
            return patchURL != nil
        }

        let widgetID: String
        /// The url used to update the vote
        let patchURL: URL?
        let rewards: [RewardResource]
        let widgetKind: WidgetKind

        enum CodingKeys: String, CodingKey {
            case id
            case widgetId
            case url
            case optionId
            case rewards
            case createdAt
            case widgetKind
        }

        public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.id = try container.decode(String.self, forKey: .id)
            self.widgetID = try container.decode(String.self, forKey: .widgetId)
            self.patchURL = try container.decode(URL.self, forKey: .url)
            self.optionID = try container.decode(String.self, forKey: .optionId)
            self.rewards = try container.decodeIfPresent([RewardResource].self, forKey: .rewards) ?? []
            self.createdAt = try container.decode(Date.self, forKey: .createdAt)
            self.widgetKind = try container.decode(WidgetKind.self, forKey: .widgetKind)
        }

        internal init(id: String, widgetID: String, url: URL?, optionID: String, rewards: [RewardResource], createdAt: Date, widgetKind: WidgetKind) {
            self.id = id
            self.widgetID = widgetID
            self.patchURL = url
            self.optionID = optionID
            self.rewards = rewards
            self.createdAt = createdAt
            self.widgetKind = widgetKind
        }
    }
}

/// :nodoc:
extension PollWidgetModel: WidgetProxyInput {
    func publish(event: WidgetProxyPublishData) {
        switch event.clientEvent {
        case let .imagePollResults(results):
            guard results.id == self.id else { return }
            // Update model
            self.totalVoteCount = results.options.map { $0.voteCount }.reduce(0, +)
            results.options.forEach { option in
                self.options.first(where: { $0.id == option.id})?.voteCount = option.voteCount
            }
            // Notify delegate
            guard let delegate = self.delegate else { return }
            results.options.forEach { option in
                delegate.pollWidgetModel(
                    self,
                    voteCountDidChange: option.voteCount,
                    forOption: option.id
                )
            }
        default:
            log.verbose("Failed processing event for PollWidgetModel with error - \(event.clientEvent.description)")
        }
    }

    func discard(event: WidgetProxyPublishData, reason: DiscardedReason) {}
    func connectionStatusDidChange(_ status: ConnectionStatus) {}
    func error(_ error: Error) { log.error(error.localizedDescription) }
}
