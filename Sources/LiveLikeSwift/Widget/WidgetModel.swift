//
//  WidgetModel.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 10/19/20.
//

import Foundation

/// An enum of Widget types and their associated Widget Model object
public enum WidgetModel {
    case cheerMeter(CheerMeterWidgetModel)
    case alert(AlertWidgetModel)
    case quiz(QuizWidgetModel)
    case prediction(PredictionWidgetModel)
    case predictionFollowUp(PredictionFollowUpWidgetModel)
    case poll(PollWidgetModel)
    case imageSlider(ImageSliderWidgetModel)
    case socialEmbed(SocialEmbedWidgetModel)
    case videoAlert(VideoAlertWidgetModel)
    case textAsk(TextAskWidgetModel)
    case numberPrediction(NumberPredictionWidgetModel)
    case numberPredictionFollowUp(NumberPredictionFollowUpWidgetModel)
    case richPost(RichPostWidgetModel)

    public var id: String {
        return baseWidgetModel.id
    }

    public var kind: WidgetKind {
        return baseWidgetModel.kind
    }

    public var programID: String {
        return baseWidgetModel.programID
    }

    public var interactiveUntil: Date? {
        return baseWidgetModel.interactiveUntil
    }

    var baseWidgetModel: WidgetModelable {
        switch self {
        case .alert(let model):
            return model
        case .cheerMeter(let model):
            return model
        case .quiz(let model):
            return model
        case .prediction(let model):
            return model
        case .predictionFollowUp(let model):
            return model
        case .poll(let model):
            return model
        case .imageSlider(let model):
            return model
        case .socialEmbed(let model):
            return model
        case .videoAlert(let model):
            return model
        case .textAsk(let model):
            return model
        case .numberPrediction(let model):
            return model
        case .numberPredictionFollowUp(let model):
            return model
        case .richPost(let model):
            return model
        }
    }

    public func getSecondsUntilInteractivityExpiration() -> TimeInterval? {
        return baseWidgetModel.getSecondsUntilInteractivityExpiration()
    }
}
