//
//  CheerMeter.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 7/2/20.
//

import Foundation
import LiveLikeCore

/// Methods for observing changes to a `CheerMeterWidgetModel`
public protocol CheerMeterWidgetModelDelegate: AnyObject {
    /// Called when server sends updated vote counts
    func cheerMeterWidgetModel(
        _ model: CheerMeterWidgetModel,
        voteCountDidChange voteCount: Int,
        forOption optionID: String
    )

    /// Called when a vote request completes
    func cheerMeterWidgetModel(
        _ model: CheerMeterWidgetModel,
        voteRequest: CheerMeterWidgetModel.VoteRequest,
        didComplete result: Result<CheerMeterWidgetModel.Vote, Error>
    )
}

/// This model reflects the state of a Cheer Meter widget on the server
public class CheerMeterWidgetModel: BaseWidgetModel, CheerMeterWidgetModelable {

    /// The object that acts as the delegate for the `CheerMeterWidgetModel`.
    public weak var delegate: CheerMeterWidgetModelDelegate?

    // MARK: Data

    /// The `Option`s of the Cheer Meter
    public let options: [Option]
    /// The title of the Cheer Meter
    public let title: String
    /// The user's votes on this Cheer Meter
    /// Call `loadInteractionHistory` to fetch votes before using
    public var userVotes: [Vote] {
        return self.widgetInteractionRepo.get(widgetID: self.id)?.cheerMeterVotes ?? []
    }
    /// The user's vote with the most recent createdAt date
    public var mostRecentVote: Vote? {
        return userVotes.max(by: { $0.createdAt <= $1.createdAt})
    }
    /// The sum of the user's vote count
    public var userVoteTotal: Int {
        var userVoteTotal = 0
        options.forEach { option in
            userVoteTotal += userVotes.filter { $0.optionID == option.id }.max(by: { $0.voteCount < $1.voteCount })?.voteCount ?? 0
        }
        return userVoteTotal
    }
    /// Sponsors linked to the Cheer Meter Widget
    public let sponsors: [Sponsor]

    // MARK: Internal Properties

    let interactionURL: URL?

    // MARK: Private Properties

    private let rewardItems: [RewardItem]
    private let leaderboardManager: LeaderboardsManager
    private let widgetClient: PubSubWidgetClient?
    private let impressionURL: URL?
    private let widgetInteractionRepo: WidgetInteractionRepository

    private var batchedVoteCounterByID: [String: Int] = [:]
    private var throttleTimer: Timer?

    init(
        data: CheerMeterCreated,
        userProfile: UserProfileProtocol,
        rewardItems: [RewardItem],
        leaderboardManager: LeaderboardsManager,
        livelikeAPI: LiveLikeWidgetAPIProtocol,
        widgetClient: PubSubWidgetClient?,
        eventRecorder: EventRecorder,
        widgetInteractionRepo: WidgetInteractionRepository
    ) {
        self.rewardItems = rewardItems
        self.leaderboardManager = leaderboardManager
        self.widgetClient = widgetClient
        self.impressionURL = data.impressionURL

        self.title = data.question
        self.options = data.options.map {
            Option(
                id: $0.id,
                voteURL: $0.voteUrl,
                description: $0.description,
                imageURL: $0.imageUrl,
                voteCount: $0.voteCount
            )
        }
        self.interactionURL = try? WidgetResource.getWidgetInteractionsURL(
            fromTemplate: data.widgetInteractionsUrlTemplate,
            profileID: userProfile.userID.asString
        )
        self.widgetInteractionRepo = widgetInteractionRepo
        self.sponsors = data.sponsors

        super.init(
            baseData: data,
            eventRecorder: eventRecorder,
            livelikeAPI: livelikeAPI,
            userProfile: userProfile
        )

        // Subscribe for cheer meter events
        if let subscribeChannel = subscribeChannel {
            self.widgetClient?.addListener(self, toChannel: subscribeChannel)
        }
    }

    deinit {
        throttleTimer?.invalidate()
        throttleTimer = nil
    }

    // MARK: Methods

    /// Call this to load the user's interaction history for this Widget
    /// - Parameter completion: Returns the loaded votes
    public func loadInteractionHistory(completion: @escaping (Result<[Vote], Error>) -> Void = { _ in }) {
        self.widgetInteractionRepo.get(widgetModel: .cheerMeter(self)) { result in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let interactions):
                completion(.success(interactions.cheerMeterVotes))
            }
        }
    }

    /// Call this to submit a vote to the Cheer Meter
    /// This vote will not be submitted immediately. The votes are batched for performance and sent in 1 second intervals.
    /// - Parameter optionID: The id of the `Option` to submit a vote
    public func submitVote(optionID: String) {
        self.eventRecorder.record(
            .widgetEngaged(widgetModel: .cheerMeter(self))
        )

        if userVotes.filter({ $0.optionID == optionID }).count == 0 {

            /*
             * First time an option has been selected.
             * Reset throttle timer for `PATCH` requests because first time selections
             * require a `POST` request.
             */
            throttleTimer?.invalidate()
            throttleTimer = nil

            batchedVoteCounterByID[optionID] = (batchedVoteCounterByID[optionID] ?? 0) + 1
            guard let voteURL = self.options.first(where: { $0.id == optionID })?.voteURL else { return }
            let voteRequest = VoteRequest(optionID: optionID, voteCount: 1)
            firstly {
                self.livelikeAPI.createCheerMeterVote(
                    voteCount: voteRequest.voteCount,
                    voteURL: voteURL,
                    accessToken: self.userProfile.accessToken
                )
            }.then { vote in
                self.widgetInteractionRepo.add(interactions: [.cheerMeterVote(vote)])
                // Notify leaderboards of new entry
                self.leaderboardManager.notifyCurrentPositionChange(rewards: vote.rewards)

                // Notify delegate of successful vote request
                self.delegate?.cheerMeterWidgetModel(
                    self,
                    voteRequest: voteRequest,
                    didComplete: .success(vote)
                )
            }.catch { error in
                // Notify delegate of failed vote request
                self.delegate?.cheerMeterWidgetModel(
                    self,
                    voteRequest: voteRequest,
                    didComplete: .failure(error)
                )
            }
        } else {
            batchedVoteCounterByID[optionID] = (batchedVoteCounterByID[optionID] ?? 0) + 1
            // Create timer on first vote
            if throttleTimer == nil {
                Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { [weak self] timer in
                    guard let self = self else { return }
                    self.throttleTimer = timer
                    // Send batched vote requests for all options
                    self.batchedVoteCounterByID.forEach { (optionID, voteCount) in
                        guard voteCount > 0 else { return }
                        let voteRequest = VoteRequest(optionID: optionID, voteCount: voteCount)
                        guard let patchURL = self.userVotes.filter({ $0.optionID == optionID }).first?.patchURL else { return }
                        firstly {
                            self.livelikeAPI.updateCheerMeterVote(
                                voteCount: voteCount,
                                voteURL: patchURL,
                                accessToken: self.userProfile.accessToken
                            )
                        }.then { vote in
                            self.widgetInteractionRepo.add(interactions: [.cheerMeterVote(vote)])
                            // Notify leaderboards of new entry
                            self.leaderboardManager.notifyCurrentPositionChange(rewards: vote.rewards)

                            // Notify delegate of successful vote request
                            self.delegate?.cheerMeterWidgetModel(
                                self,
                                voteRequest: voteRequest,
                                didComplete: .success(vote)
                            )
                        }.catch { error in
                            // Notify delegate of failed vote request
                            self.delegate?.cheerMeterWidgetModel(
                                self,
                                voteRequest: voteRequest,
                                didComplete: .failure(error)
                            )
                        }
                    }
                    // Reset batched vote counter
                    self.batchedVoteCounterByID.removeAll()
                }
            }
        }
    }

    // MARK: Types

    /// A batched vote request
    public struct VoteRequest {
        /// The id of the option that was voted
        public let optionID: String
        /// The count of votes to submit
        public let voteCount: Int
    }

    /// Represents a batched Cheer Meter vote
    public struct Vote: Decodable {
        /// The id of the option that was voted
        public let optionID: String
        /// The count of batched votes
        public let voteCount: Int
        /// The date that this Vote was created
        public let createdAt: Date

        let widgetID: String
        /// The url used to update the vote
        let patchURL: URL?
        let rewards: [RewardResource]
        let widgetKind: WidgetKind

        enum CodingKeys: String, CodingKey {
            case voteCount
            case optionId
            case rewards
            case widgetId
            case url
            case createdAt
            case widgetKind
        }

        public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.voteCount = try container.decode(Int.self, forKey: .voteCount)
            self.widgetID = try container.decode(String.self, forKey: .widgetId)
            self.optionID = try container.decode(String.self, forKey: .optionId)
            self.patchURL = try container.decodeIfPresent(URL.self, forKey: .url)
            self.rewards = try container.decodeIfPresent([RewardResource].self, forKey: .rewards) ?? []
            self.createdAt = try container.decode(Date.self, forKey: .createdAt)
            self.widgetKind = try container.decode(WidgetKind.self, forKey: .widgetKind)
        }

        internal init(rewards: [RewardResource], widgetID: String, voteCount: Int, patchURL: URL, optionId: String, createdAt: Date, widgetKind: WidgetKind) {
            self.rewards = rewards
            self.widgetID = widgetID
            self.voteCount = voteCount
            self.optionID = optionId
            self.patchURL = patchURL
            self.createdAt = createdAt
            self.widgetKind = widgetKind
        }
    }

    /// A Cheer Meter option
    public class Option {
        internal init(id: String, voteURL: URL, description: String, imageURL: URL, voteCount: Int) {
            self.id = id
            self.voteURL = voteURL
            self.text = description
            self.imageURL = imageURL
            self.voteCount = voteCount
        }

        /// The id of the option. Use this to submit votes.
        public let id: String

        let voteURL: URL

        /// The option's text.
        public let text: String

        /// The option's image url.
        public let imageURL: URL

        /// The updated total number of votes for this option.
        /// This property is Atomic
        @Atomic public internal(set) var voteCount: Int
    }
}

// MARK: WidgetProxyInput

/// :nodoc:
extension CheerMeterWidgetModel: WidgetProxyInput {
    func publish(event: WidgetProxyPublishData) {
        guard case let .cheerMeterResults(payload) = event.clientEvent else {
            return
        }
        guard payload.id == self.id else { return }
        payload.options.forEach { option in
            /// Update the option's voteCount then notify delegate
            self.options.first(where: { $0.id == option.id })?.voteCount = option.voteCount
            self.delegate?.cheerMeterWidgetModel(self, voteCountDidChange: option.voteCount, forOption: option.id)
        }
    }

    func discard(event: WidgetProxyPublishData, reason: DiscardedReason) {}
    func connectionStatusDidChange(_ status: ConnectionStatus) {}
    func error(_ error: Error) {}
}
