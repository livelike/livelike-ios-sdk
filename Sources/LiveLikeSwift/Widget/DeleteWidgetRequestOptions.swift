//
//  DeleteWidgetOptions.swift
//  
//
//  Created by Jelzon Monzon on 8/7/23.
//

import Foundation

public struct DeleteWidgetRequestOptions {
    public let kind: WidgetKind
    public let id: String


    /// Create a Text Poll Widget
    /// - Parameters:
    ///   - id: The id of the Widget
    ///   - kind: The kind of the Widget
    public init(id: String, kind: WidgetKind) {
        self.kind = kind
        self.id = id
    }
}
