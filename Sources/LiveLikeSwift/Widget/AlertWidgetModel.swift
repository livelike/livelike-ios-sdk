//
//  AlertWidgetViewModel.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 7/2/20.
//

import Foundation
#if os(iOS)
    import UIKit
#endif

/// A model containing the data of an Alert Widget
public class AlertWidgetModel: BaseWidgetModel, AlertWidgetModelable {

    // MARK: Data

    /// The title of the Alert Widget
    public var title: String?
    /// The URL of the link of the Alert Widget
    public var linkURL: URL?
    /// The label describing the `linkURL`
    public var linkLabel: String?
    /// The text contents of the Alert Widget
    public var text: String?
    /// The URL of the image of the Alert Widget
    public var imageURL: URL?
    /// Sponsors linked to the Alert Widget
    public let sponsors: [Sponsor]

    init(
        data: AlertCreated,
        eventRecorder: EventRecorder,
        livelikeAPI: LiveLikeWidgetAPIProtocol,
        userProfile: UserProfileProtocol
    ) {
        self.title = data.title
        self.linkURL = data.linkURL
        self.imageURL = data.imageURL
        self.text = data.text
        self.linkLabel = data.linkLabel
        self.sponsors = data.sponsors
        super.init(
            baseData: data,
            eventRecorder: eventRecorder,
            livelikeAPI: livelikeAPI,
            linkURL: data.linkURL,
            userProfile: userProfile
        )
    }

    // MARK: Methods


    /// Opens the `linkURL` in a webpage if available.
    public func openLinkUrl() {
        #if os(iOS)
            if let widgetLink = linkURL {
                eventRecorder.record(
                    .widgetEngaged(widgetModel: .alert(self))
                )
                eventRecorder.record(.alertWidgetLinkOpened(alertId: id, programId: programID, linkUrl: widgetLink.absoluteString, kind: kind))

                UIApplication.shared.open(widgetLink)
            }
        #endif
    }


}
