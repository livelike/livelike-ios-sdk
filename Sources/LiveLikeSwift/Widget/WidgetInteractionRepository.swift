//
//  WidgetInteractionRepository.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 5/13/21.
//

import Foundation
import LiveLikeCore

protocol WidgetInteractionRepository {
    func add(interactions: [WidgetInteraction])

    func get(widgetID: String) -> [WidgetInteraction]?

    func get(
        interactionsForTimeline timeline: WidgetTimelineResource,
        completion: @escaping (Result<[WidgetInteraction], Error>) -> Void
    )

    func promise(
        interactionsForTimeline timeline: WidgetTimelineResource
    ) -> Promise<[WidgetInteraction]>

    func get(
        widgetModel: WidgetModel,
        completion: @escaping (Result<[WidgetInteraction], Error>) -> Void
    )
}

protocol WidgetInteractionCacheProtocol: AnyObject {
    var widgetInteractionsByWidgetID: [String: [WidgetInteraction]] { get set }
}

class WidgetInteractionCache: WidgetInteractionCacheProtocol {
    @Atomic var widgetInteractionsByWidgetID: [String: [WidgetInteraction]] = [:]
}

class ServerWidgetInteractionRepository: WidgetInteractionRepository {
    private let livelikeAPI: LiveLikeWidgetAPIProtocol
    private let userProfile: UserProfileProtocol
    /// The list of widget interactions by widget id
    /// If a key(widget id) does not exist in the dictionary then we have no knowledge of interactions and should load from server
    private let cache: WidgetInteractionCacheProtocol

    init(
        livelikeAPI: LiveLikeWidgetAPIProtocol,
        userProfile: UserProfileProtocol,
        cache: WidgetInteractionCacheProtocol
    ) {
        self.livelikeAPI = livelikeAPI
        self.userProfile = userProfile
        self.cache = cache
    }

    func add(interactions: [WidgetInteraction]) {
        interactions.forEach {
            if cache.widgetInteractionsByWidgetID[$0.widgetID] == nil {
                cache.widgetInteractionsByWidgetID[$0.widgetID] = []
            }
            cache.widgetInteractionsByWidgetID[$0.widgetID]?.append($0)
        }
    }

    func get(widgetID: String) -> [WidgetInteraction]? {
        return cache.widgetInteractionsByWidgetID[widgetID]
    }

    func get(
        interactionsForTimeline timeline: WidgetTimelineResource,
        completion: @escaping (Result<[WidgetInteraction], Error>) -> Void
    ) {
        let interactableWidgetsIDs = timeline.items.filter({ WidgetKind.interactableKinds.contains($0.kind) }).map({ $0.id })
        // Check if cache has interactions for all interactable widgets in timeline
        // If any are missing fetch timeline interactions from server
        if interactableWidgetsIDs.allSatisfy(cache.widgetInteractionsByWidgetID.keys.contains) {
            let interactions = timeline.items.reduce(into: [WidgetInteraction](), { interactions, widgetResource in
                interactions.append(contentsOf: cache.widgetInteractionsByWidgetID[widgetResource.id] ?? [])
            })
            completion(.success(interactions))
        } else {
            do {
                let timelineInteractionURL = try timeline.getWidgetInteractionsURL(profileID: self.userProfile.userID.asString)
                firstly {
                    self.livelikeAPI.getTimelineInteractions(
                        timelineInteractionURL: timelineInteractionURL,
                        accessToken: self.userProfile.accessToken
                    )
                }.then { timelineInteractions in
                    // Initialize the key for ALL widgets in timeline if it doesn't exist
                    // And not just the ones with interactions
                    // This way we know if we already attempted to load interactions from server
                    timeline.items.forEach {
                        if self.cache.widgetInteractionsByWidgetID[$0.id] == nil {
                            self.cache.widgetInteractionsByWidgetID[$0.id] = []
                        }
                    }
                    let interactions = timelineInteractions.interactionsByKind.reduce(into: [WidgetInteraction]()) { interactions, kvp in
                        interactions.append(contentsOf: kvp.value)
                    }
                    self.add(interactions: interactions)
                    completion(.success(interactions))
                }.catch {
                    completion(.failure($0))
                }
            } catch {
                completion(.failure(error))
            }
        }
    }

    func promise(
        interactionsForTimeline timeline: WidgetTimelineResource
    ) -> Promise<[WidgetInteraction]> {
        return Promise { fulfill, reject in
            self.get(interactionsForTimeline: timeline) { result in
                switch result {
                case .failure(let error):
                    reject(error)
                case .success(let interactions):
                    fulfill(interactions)
                }
            }
        }
    }

    func get(
        widgetModel: WidgetModel,
        completion: @escaping (Result<[WidgetInteraction], Error>) -> Void
    ) {
        if let interactions = self.get(widgetID: widgetModel.interactionWidgetID) {
            completion(.success(interactions))
        } else if let interactionURL = widgetModel.interactionURL {
            firstly {
                self.livelikeAPI.getTimelineInteractions(
                    timelineInteractionURL: interactionURL,
                    accessToken: self.userProfile.accessToken
                )
            }.then { voteResources in
                let votes: [WidgetInteraction] = voteResources.interactionsByKind[widgetModel.interactionWidgetKind] ?? []
                self.cache.widgetInteractionsByWidgetID[widgetModel.interactionWidgetID] = votes
                completion(.success(votes))
            }.catch {
                completion(.failure($0))
            }
        } else {
            let error = WidgetInteractionErrors.missingInteractionsURL(
                id: widgetModel.interactionWidgetID,
                kind: widgetModel.interactionWidgetKind
            )
            log.error(error)
            completion(.failure(error))
        }
    }
}

private extension WidgetModel {
    // The url to load interactions from server
    var interactionURL: URL? {
        switch self {
        case .quiz(let model):
            return model.interactionURL
        case .poll(let model):
            return model.interactionURL
        case .prediction(let model):
            return model.interactionURL
        case .predictionFollowUp(let model):
            return model.interactionURL
        case .cheerMeter(let model):
            return model.interactionURL
        case .imageSlider(let model):
            return model.interactionURL
        case .textAsk(let model):
            return model.interactionURL
        case .numberPrediction(let model):
            guard let model = model as? InternalNumberPredictionWidgetModel else { return nil }
            return model.interactionURL
        case .numberPredictionFollowUp(let model):
            guard let model = model as? InternalNumberPredictionFollowUpWidgetModel else { return nil }
            return model.interactionURL
        case .alert, .socialEmbed, .videoAlert, .richPost:
            return nil
        }
    }

    // The id used to lookup interactions
    var interactionWidgetID: String {
        switch self {
        case .numberPredictionFollowUp(let model):
            return model.associatedPredictionID
        case .predictionFollowUp(let model):
            return model.associatedPredictionID
        default:
            return self.id
        }
    }

    // The kind used to lookup interactions
    var interactionWidgetKind: WidgetKind {
        switch self {
        case .numberPredictionFollowUp(let model):
            return model.associatedPredictionKind
        case .predictionFollowUp(let model):
            return model.associatedPredictionKind
        default:
            return self.kind
        }
    }
}
