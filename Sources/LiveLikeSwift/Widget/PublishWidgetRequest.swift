//
//  PublishWidgetRequest.swift
//  
//
//  Created by Jelzon Monzon on 8/7/23.
//

import Foundation
import LiveLikeCore

struct PublishWidgetRequest: LLRequest {

    private let networking: LLNetworking
    private let accessToken: String
    private let publishDelay: UInt?
    private let programDateTime: Date?

    init(
        networking: LLNetworking,
        accessToken: String,
        publishDelay: UInt?,
        programDateTime: Date?
    ) {
        self.networking = networking
        self.accessToken = accessToken
        self.publishDelay = publishDelay
        self.programDateTime = programDateTime
    }

    func execute(
        url: URL,
        completion: @escaping (Result<PublishedWidgetInfo, Error>) -> Void
    ) {
        struct Body: Encodable {
            let publishDelay: UInt?
            let programDateTime: Date?

            enum CodingKeys: CodingKey {
                case publishDelay
                case programDateTime
            }

            func encode(to encoder: Encoder) throws {
                var container = encoder.container(keyedBy: CodingKeys.self)
                try container.encodeIfPresent(
                    publishDelay,
                    forKey: .publishDelay
                )


                if let programDateTime = self.programDateTime {
                    try container.encodeIfPresent(
                        DateFormatter.iso8601FullWithPeriod.string(from: programDateTime),
                        forKey: .programDateTime
                    )
                }

            }
        }
        let resource = Resource<PublishedWidgetInfo>(
            url: url,
            method: .put(
                Body(
                    publishDelay: publishDelay,
                    programDateTime: programDateTime
                )
            ),
            accessToken: self.accessToken
        )
        networking.task(resource, completion: completion)
    }
}
