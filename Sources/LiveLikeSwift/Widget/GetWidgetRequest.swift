//
//  GetWidgetRequest.swift
//  
//
//  Created by Jelzon Monzon on 6/12/23.
//

import Foundation
import LiveLikeCore

class GetWidgetRequest: LLRequest {
    typealias ResponseType = WidgetResource
    
    private let networking: LLNetworking
    
    init(
        networking: LLNetworking
    ) {
        self.networking = networking
    }
    
    func execute(
        url: URL,
        completion: @escaping (Result<WidgetResource, Error>) -> Void
    ) {
        let resource = Resource<WidgetResource>(
            get: url
        )
        networking.task(resource, completion: completion)
    }
    
}
