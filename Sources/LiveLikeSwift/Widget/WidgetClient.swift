//
//  WidgetClient.swift
//  
//
//  Created by Jelzon Monzon on 8/4/23.
//

import Foundation
import LiveLikeCore

/// Methods for creating, publishing, updating, and deleting Widgets
public protocol WidgetClient {
    
    /// Publishes a Widget
    /// - Parameters:
    ///   - options: Options for publishing a Widget
    ///   - completion:
    ///     - success: Info describing the published Widget
    ///     - failure: An error describing the failure
    func publishWidget(
        options: PublishWidgetRequestOptions,
        completion: @escaping (Result<PublishedWidgetInfo, Error>) -> Void
    )
    
    /// Deletes a Widget
    /// - Parameters:
    ///   - options: Options for deleting a Widget
    ///   - completion:
    ///     - success: True if the Widget was successfully deleted
    ///     - failure: An error describing the failure
    func deleteWidget(
        options: DeleteWidgetRequestOptions,
        completion: @escaping (Result<Bool, Error>) -> Void
    )
    
    /// Create a Text Poll Widget
    /// - Parameters:
    ///   - options: Options for creating the Widget
    ///   - completion:
    ///     - success: The model for the created widget
    ///     - failure: An error describing the failure
    func createTextPollWidget(
        options: CreateTextPollRequestOptions,
        completion: @escaping (Result<PollWidgetModel, Error>) -> Void
    )
    
    /// Create an Image Poll Widget
    /// - Parameters:
    ///   - options: Options for creating the Widget
    ///   - completion:
    ///     - success: The model for the created widget
    ///     - failure: An error describing the failure
    func createImagePollWidget(
        options: CreateImagePollRequestOptions,
        completion: @escaping (Result<PollWidgetModel, Error>) -> Void
    )
    
    /// Create an Alert Widget
    /// - Parameters:
    ///   - options: Options for creating the Widget
    ///   - completion:
    ///     - success: The model for the created widget
    ///     - failure: An error describing the failure
    func createAlertWidget(
        options: CreateAlertRequestOptions,
        completion: @escaping (Result<AlertWidgetModel, Error>) -> Void
    )
    
    /// Create a Text Prediction Widget
    /// - Parameters:
    ///   - options: Options for creating the Widget
    ///   - completion:
    ///     - success: The model for the created widget
    ///     - failure: An error describing the failure
    func createTextPredictionWidget(
        options: CreateTextPredictionRequestOptions,
        completion: @escaping (Result<PredictionWidgetModel, Error>) -> Void
    )
    
    /// Create an Image Prediction Widget
    /// - Parameters:
    ///   - options: Options for creating the Widget
    ///   - completion:
    ///     - success: The model for the created widget
    ///     - failure: An error describing the failure
    func createImagePredictionWidget(
        options: CreateImagePredictionRequestOptions,
        completion: @escaping (Result<PredictionWidgetModel, Error>) -> Void
    )
    
    /// Updates an Option of a Text Prediction Follow Up Widget
    /// - Parameters:
    ///   - options: Options for updating the Widget option
    ///   - completion:
    ///     - success: The updated Option of the Widget
    ///     - failure: An error describing the failure
    func updateTextPredictionFollowUpWidgetOption(
        options: UpdateTextPredictionFollowUpRequestOptions,
        completion: @escaping (Result<PredictionFollowUpWidgetModel.Option, Error>) -> Void
    )
    
    /// Updates an Option of an Image Prediction Follow Up Widget
    /// - Parameters:
    ///   - options: Options for updating the Widget option
    ///   - completion:
    ///     - success: The updated Option of the Widget
    ///     - failure: An error describing the failure
    func updateImagePredictionFollowUpWidgetOption(
        options: UpdateImagePredictionFollowUpRequestOptions,
        completion: @escaping (Result<PredictionFollowUpWidgetModel.Option, Error>) -> Void
    )
    
    /// Create an Text Ask Widget
    /// - Parameters:
    ///   - options: Options for creating the Widget
    ///   - completion:
    ///     - success: The model for the created widget
    ///     - failure: An error describing the failure
    func createTextAskWidget(
        options: CreateTextAskWidgetOptions,
        completion: @escaping (Result<TextAskWidgetModel, Error>) -> Void
    )
    
    /// Create a Text Quiz Widget
    /// - Parameters:
    ///   - options: Options for creating the Widget
    ///   - completion:
    ///     - success: The model for the created widget
    ///     - failure: An error describing the failure
    func createTextQuizWidget(
        options: CreateTextQuizWidgetOptions,
        completion: @escaping (Result<QuizWidgetModel, Error>) -> Void
    )
    
    /// Create a Image Quiz Widget
    /// - Parameters:
    ///   - options: Options for creating the Widget
    ///   - completion:
    ///     - success: The model for the created widget
    ///     - failure: An error describing the failure
    func createImageQuizWidget(
        options: CreateImageQuizWidgetOptions,
        completion: @escaping (Result<QuizWidgetModel, Error>) -> Void
    )
    
    /// Create a Rich Post Widget
    /// - Parameters:
    ///   - options: Options for creating the Widget
    ///   - completion:
    ///     - success: The model for the created widget
    ///     - failure: An error describing the failure
    func createRichPost(
        options: CreateRichPostWidgetOptions,
        completion: @escaping (Result<RichPostWidgetModel, Error>) -> Void
    )
    
    /// Create a Number Prediction Widget
    /// - Parameters:
    ///   - options: Options for creating the Widget
    ///   - completion:
    ///     - success: The model for the created widget
    ///     - failure: An error describing the failure
    func createNumberPredictionWidget(
        options: CreateNumberPredictionWidgetOptions,
        completion: @escaping (Result<NumberPredictionWidgetModel, Error>) -> Void
    )
    
    /// Updates an Option of a Number Prediction Follow Up Widget
    /// - Parameters:
    ///   - options: Options for updating the Widget option
    ///   - completion:
    ///     - success: The updated Option of the Widget
    ///     - failure: An error describing the failure
    func updateNumberPredictionFollowUpOption(
        options: UpdateNumberPredictionFollowUpOption,
        completion: @escaping (Result<NumberPredictionOption, Error>) -> Void
    )
}
