//
//  CreateImagePollOptions.swift
//  
//
//  Created by Jelzon Monzon on 8/3/23.
//

import Foundation

public struct CreateImagePollRequestOptions {
    /// - Parameters:
    ///   - question: The question the Poll will ask
    ///   - options: The possible options of the Poll
    public init(
        common: CommonCreateWidgetOptions,
        question: String,
        options: [Option]
    ) {
        self.common = common
        self.question = question
        self.options = options
    }

    public let common: CommonCreateWidgetOptions
    public let question: String
    public let options: [Option]

    public struct Option {
        /// - Parameters:
        ///   - text: The text describing the Option
        ///   - imageURL: The url of the image describing the Option
        public init(text: String, imageURL: URL) {
            self.text = text
            self.imageURL = imageURL
        }

        public let text: String
        public let imageURL: URL
    }
}

extension CreateImagePollRequestOptions: Encodable {
    enum CodingKeys: String, CodingKey {
        case question
        case options
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(question, forKey: .question)
        try container.encode(options, forKey: .options)
        try common.encode(to: encoder)
    }
}

extension CreateImagePollRequestOptions.Option: Encodable {
    enum CodingKeys: String, CodingKey {
        case text = "description"
        case imageURL = "imageUrl"
    }
}
