//
//  SocialEmbedWidgetModel.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 2/1/21.
//

import Foundation

/// An object that reflects the state of a Social Embed widget on the server
public class SocialEmbedWidgetModel: BaseWidgetModel, SocialEmbedWidgetModelable {

    // MARK: Data
    
    /// Objects that hold oembed content
    public let items: [Item]
    /// Comment / Caption of the widget
    public let comment: String?
    /// Sponsors linked to the Social Embed Widget
    public let sponsors: [Sponsor]
    
    // MARK: Private Properties
    
    private let data: SocialEmbedWidgetResource
    
    init(
        resource: SocialEmbedWidgetResource,
        eventRecorder: EventRecorder,
        livelikeAPI: LiveLikeWidgetAPIProtocol,
        userProfile: UserProfileProtocol
    ) {
        self.comment = resource.comment
        self.data = resource
        // Create embed items
        self.items = resource.items.map({
            SocialEmbedWidgetModel.Item(
                id: $0.id,
                url: $0.url,
                oembed: OEmbed(
                    html: $0.oembed.html,
                    providerName: $0.oembed.providerName
                )
            )
        })
        self.sponsors = data.sponsors
        super.init(
            baseData: resource,
            eventRecorder: eventRecorder,
            livelikeAPI: livelikeAPI,
            userProfile: userProfile
        )
    }
    
    public struct Item {
        public let id: String
        public let url: URL
        public let oembed: OEmbed
    }
    
    public struct OEmbed {
        public let html: String
        public let providerName: String
    }
}
