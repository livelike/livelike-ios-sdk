//
//  GetWidgetsRequest.swift
//  
//
//  Created by Jelzon Monzon on 6/12/23.
//

import Foundation
import LiveLikeCore

class GetWidgetsRequest {

    private let options: GetWidgetModelsRequestOptions?
    private let networking: LLNetworking
    private let accessToken: String

    init(
        options: GetWidgetModelsRequestOptions?,
        networking: LLNetworking,
        accessToken: String
    ) {
        self.options = options
        self.networking = networking
        self.accessToken = accessToken
    }
}

extension GetWidgetsRequest: LLRequest {
    func execute(
        url: URL,
        completion: @escaping (Result<PaginatedResult<WidgetResource>, Error>) -> Void
    ) {
        do {
            let resource = Resource<PaginatedResult<WidgetResource>>(
                get: try buildURL(url: url),
                accessToken: accessToken
            )
            DispatchQueue.global().async { [weak self] in
                guard let self = self else { return }
                self.networking.task(resource, completion: completion)
            }
        } catch {
            completion(.failure(error))
        }
    }

    private func buildURL(url: URL) throws -> URL {
        if let options = options {
            guard var getWidgetModelsURLComponents = URLComponents(
                url: url,
                resolvingAgainstBaseURL: false
            ) else {
                throw GetWidgetError.failedBuildingGetWidgetsURL(
                    error: "Failed building URLComponents object from `programResource.widgetsUrl`"
                )
            }

            // Keep the page param, quickfix for pagination - fixes fetching the first page every time
            var queryItems = self.buildURLQueryItems(options: options)
            if let pageItem = getWidgetModelsURLComponents.queryItems?.first(where: { $0.name == "page" }) {
                queryItems.append(pageItem)
            }

            getWidgetModelsURLComponents.queryItems = queryItems

            guard
                let getWidgetModelsURL = getWidgetModelsURLComponents.url
            else {
                throw GetWidgetError.failedBuildingGetWidgetsURL(
                    error: "`URLComponents` failed to the creation of a `URL`"
                )
            }

            return getWidgetModelsURL
        } else {
            return url
        }
    }

    private func buildURLQueryItems(
        options: GetWidgetModelsRequestOptions
    ) -> [URLQueryItem] {
        var queries = [URLQueryItem]()

        if let widgetKind = options.widgetKind {
            widgetKind.forEach { kind in
                queries.append(URLQueryItem(name: "kind", value: kind.stringValue))
            }
        }

        if let widgetStatus = options.widgetStatus {
            queries.append(URLQueryItem(name: "status", value: widgetStatus.rawValue))
        }

        if
            let widgetOrdering = options.widgetOrdering,
            widgetOrdering != .oldest
        {
            queries.append(URLQueryItem(name: "ordering", value: widgetOrdering.rawValue))
        }

        if let interactive = options.interactive {
            queries.append(URLQueryItem(name: "interactive", value: String(interactive)))
        }

        if let since = options.since {
            queries.append(URLQueryItem(name: "since", value: DateFormatter.iso8601FullWithPeriod.string(from: since)))
        }

        if let sincePlaybackTimestamp = options.sincePlaybackTimeMilliseconds {
            queries.append(
                URLQueryItem(
                    name: "since_playback_time_ms",
                    value: String(sincePlaybackTimestamp)
                )
            )
        }

        if let untilPlaybackTimestamp = options.untilPlaybackTimeMilliseconds {
            queries.append(
                URLQueryItem(
                    name: "until_playback_time_ms",
                    value: String(untilPlaybackTimestamp)
                )
            )
        }

        if let widgetAttributes = options.widgetAttributes {
            queries.append(contentsOf: widgetAttributes.map { widgetAttribute in
                URLQueryItem(
                    name: "widget_attribute",
                    value: "\(widgetAttribute.key):\(widgetAttribute.value)"
                )
            })
        }

        if let pageSize = options.pageSize {
            queries.append(URLQueryItem(name: "page_size", value: String(pageSize)))
        }

        return queries
    }
}

extension GetWidgetsRequest: LLPaginatedRequest {
    typealias ElementType = WidgetResource

    var requestID: String {
        var pageResultURLID = "getWidgetModels"

        if let options = self.options {
            let id = {
                var id: String = ""

                if let widgetKind = options.widgetKind {
                    let sortedWidgetKind = widgetKind.sorted { $0.stringValue > $1.stringValue }
                    sortedWidgetKind.forEach { id += $0.stringValue }
                }

                if let widgetStatus = options.widgetStatus {
                    id += widgetStatus.rawValue
                }

                if let widgetOrdering = options.widgetOrdering {
                    id += widgetOrdering.rawValue
                }

                if let interactive = options.interactive {
                    id += interactive.description
                }

                if let since = options.since {
                    id += since.timeIntervalSince1970.description
                }

                if let sincePlaybackTimestamp = options.sincePlaybackTimeMilliseconds {
                    id += sincePlaybackTimestamp.description
                }

                if let untilPlaybackTimestamp = options.untilPlaybackTimeMilliseconds {
                    id += untilPlaybackTimestamp.description
                }

                return id
            }()
            pageResultURLID += "_\(id)"
        }
        return pageResultURLID
    }
}
