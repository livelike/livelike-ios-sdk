//
//  PredictionWidgetModel.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 7/2/20.
//

import Foundation
import LiveLikeCore

/// Methods for observing changes to a `PredictionWidgetModel`
public protocol PredictionWidgetModelDelegate: AnyObject {
    /// Called when the server sends updated vote counts
    func predictionWidgetModel(
        _ model: PredictionWidgetModel,
        voteCountDidChange voteCount: Int,
        forOption optionID: String
    )

    func predictionWidgetModel(
        _ model: PredictionWidgetModel,
        didReceiveFollowUp id: String,
        kind: WidgetKind
    )
}

/// An object that reflects the state of a Prediction widget on the server
public class PredictionWidgetModel: BaseWidgetModel, PredictionWidgetModelable {

    /// The object that acts as the delegate for the `PredictionWidgetModel`
    public weak var delegate: PredictionWidgetModelDelegate?

    // MARK: Data

    /// The question of the Prediction
    public let question: String
    /// The options of the Predictions
    public let options: [Option]
    /// A message to show the user when their prediction is locked in
    public let confirmationMessage: String
    /// Does the Prediction widget contain images
    public let containsImages: Bool
    /// The updated total count of votes on the Prediction
    @Atomic public internal(set) var totalVoteCount: Int
    /// The user's votes on this Prediction
    /// Call `loadInteractionHistory` to fetch votes before using
    public var userVotes: [PredictionVote] {
        return self.widgetInteractionRepo.get(widgetID: self.id)?.predictionVotes ?? []
    }
    /// The user's vote with the most recent createdAt date
    public var mostRecentVote: PredictionVote? {
        return userVotes.max(by: { $0.createdAt <= $1.createdAt })
    }
    /// Whether the Follow Up for this Prediction has been published
    @Atomic public private(set) var isFollowUpPublished: Bool

    @Atomic public private(set) var followUpWidgetModels: [PredictionFollowUpWidgetModel] = []

    /// Sponsors linked to the Prediction Widget
    public let sponsors: [Sponsor]

    /// Rewards which can be earned when interacting with this Prediction
    public let earnableRewards: [EarnableReward]

    // MARK: Internal Properties

    let interactionURL: URL?

    // MARK: Private Properties

    private let rewardItems: [RewardItem]
    private let voteRepo: PredictionVoteRepository
    private let leaderboardManager: LeaderboardsManager
    private let widgetClient: PubSubWidgetClient?
    private let widgetInteractionRepo: WidgetInteractionRepository
    private let followUpModelFactory: PredictionWidgetModelFactoryProtocol
    private let programSubscribeChannel: String?

    private var voteTask: URLSessionDataTask?

    init(
        resource: TextPredictionCreated,
        eventRecorder: EventRecorder,
        userProfile: UserProfileProtocol,
        rewardItems: [RewardItem],
        voteRepo: PredictionVoteRepository,
        leaderboardManager: LeaderboardsManager,
        livelikeAPI: LiveLikeWidgetAPIProtocol,
        widgetClient: PubSubWidgetClient?,
        widgetInteractionRepo: WidgetInteractionRepository,
        programSubscribeChannel: String?,
        followUpModelFactory: PredictionWidgetModelFactoryProtocol
    ) {
        self.question = resource.question
        self.options = resource.options.map { Option(resource: $0) }
        self.confirmationMessage = resource.confirmationMessage
        self.containsImages = false
        self.totalVoteCount = resource.options.map { $0.voteCount }.reduce(0, +)
        self.rewardItems = rewardItems
        self.voteRepo = voteRepo
        self.leaderboardManager = leaderboardManager
        self.widgetClient = widgetClient
        self.interactionURL = try? WidgetResource.getWidgetInteractionsURL(
            fromTemplate: resource.widgetInteractionsUrlTemplate,
            profileID: userProfile.userID.asString
        )
        self.widgetInteractionRepo = widgetInteractionRepo
        self.isFollowUpPublished = resource.followUps.contains(where: { $0.status == .published })
        self.programSubscribeChannel = programSubscribeChannel
        self.followUpModelFactory = followUpModelFactory
        self.sponsors = resource.sponsors
        self.earnableRewards = resource.earnableRewards
        super.init(
            baseData: resource,
            eventRecorder: eventRecorder,
            livelikeAPI: livelikeAPI,
            userProfile: userProfile
        )

        self.followUpWidgetModels = resource.followUps.compactMap { followUp in
            return self.followUpModelFactory.makePredictionFollowUpModel(resource: followUp)
        }
        if let subscribeChannel = subscribeChannel {
            self.widgetClient?.addListener(self, toChannel: subscribeChannel)
        }
        if let programSubscribeChannel = programSubscribeChannel {
            self.widgetClient?.addListener(self, toChannel: programSubscribeChannel)
        }
    }

    init(
        resource: ImagePredictionCreated,
        eventRecorder: EventRecorder,
        userProfile: UserProfileProtocol,
        rewardItems: [RewardItem],
        voteRepo: PredictionVoteRepository,
        leaderboardManager: LeaderboardsManager,
        livelikeAPI: LiveLikeWidgetAPIProtocol,
        widgetClient: PubSubWidgetClient?,
        widgetInteractionRepo: WidgetInteractionRepository,
        programSubscribeChannel: String?,
        followUpModelFactory: PredictionWidgetModelFactoryProtocol
    ) {
        self.question = resource.question
        self.options = resource.options.map { Option(resource: $0) }
        self.confirmationMessage = resource.confirmationMessage
        self.containsImages = true
        self.totalVoteCount = resource.options.map { $0.voteCount }.reduce(0, +)
        self.rewardItems = rewardItems
        self.voteRepo = voteRepo
        self.leaderboardManager = leaderboardManager
        self.widgetClient = widgetClient
        self.interactionURL = try? WidgetResource.getWidgetInteractionsURL(
            fromTemplate: resource.widgetInteractionsUrlTemplate,
            profileID: userProfile.userID.asString
        )
        self.widgetInteractionRepo = widgetInteractionRepo
        self.isFollowUpPublished = resource.followUps.contains(where: { $0.status == .published })
        self.programSubscribeChannel = programSubscribeChannel
        self.followUpModelFactory = followUpModelFactory
        self.sponsors = resource.sponsors
        self.earnableRewards = resource.earnableRewards
        super.init(
            baseData: resource,
            eventRecorder: eventRecorder,
            livelikeAPI: livelikeAPI,
            userProfile: userProfile
        )

        self.followUpWidgetModels = resource.followUps.compactMap { followUp in
            return self.followUpModelFactory.makePredictionFollowUpModel(resource: followUp)
        }
        if let subscribeChannel = subscribeChannel {
            self.widgetClient?.addListener(self, toChannel: subscribeChannel)
        }
        if let programSubscribeChannel = programSubscribeChannel {
            self.widgetClient?.addListener(self, toChannel: programSubscribeChannel)
        }
    }

    // MARK: Methods

    /// Call this to load the user's interaction history for this Widget
    /// - Parameter completion: Returns the loaded votes
    public func loadInteractionHistory(completion: @escaping (Result<[PredictionVote], Error>) -> Void = { _ in }) {
        self.widgetInteractionRepo.get(widgetModel: .prediction(self)) { result in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let interactions):
                completion(.success(interactions.predictionVotes))
            }
        }
    }

    /// Submits or updates an existing vote on `optionID`
    /// - Parameters:
    ///   - optionID: The `id` of the `Option` to vote on
    public func submitVote(optionID: String, completion: @escaping (Result<PredictionVote, Error>) -> Void) {
        // Cancel the previous task - if task has already completed this will do nothing
        if let voteTask = self.voteTask, voteTask.state == .running {
            log.debug("Previous vote task is being cancelled.")
            voteTask.cancel()
        }

        self.eventRecorder.record(
            .widgetEngaged(widgetModel: .prediction(self))
        )

        // Avoid repeat vote on a previously voted option
        guard mostRecentVote?.optionID != optionID else {
            log.error("\(Errors.voteAlreadySubmitted.localizedDescription)")
            completion(.failure(Errors.voteAlreadySubmitted))
            return
        }

        guard let voteURL = options.first(where: { $0.id == optionID})?.voteURL else {
            log.error("\(Errors.failedDueToInvalidOptionID.localizedDescription)")
            completion(.failure(Errors.failedDueToInvalidOptionID))
            return
        }
        if let mostRecentVote = self.mostRecentVote {
            guard let patchURL = mostRecentVote.patchURL else {
                completion(.failure(Errors.cannotUpdateVote))
                return
            }
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                guard let self = self else { return }
                self.voteTask = self.livelikeAPI.updatePredictionVote(
                    optionID: optionID,
                    optionURL: patchURL,
                    accessToken: self.userProfile.accessToken
                ) { result in
                    switch result {
                    case .failure(let error):
                        completion(.failure(error))
                        log.error("Failed to submit vote because: \(error.localizedDescription)")
                    case .success(let voteResource):
                        let vote = PredictionVote(resource: voteResource)
                        self.widgetInteractionRepo.add(interactions: [.predictionVote(vote)])
                        self.voteRepo.add(vote: vote) { _ in }
                        log.debug("Prediction vote successfuly updated for id \(vote.id)")
                        completion(.success(vote))
                    }
                }
            }
        } else {
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                guard let self = self else { return }
                self.voteTask = self.livelikeAPI.createPredictionVote(
                    voteURL: voteURL,
                    accessToken: self.userProfile.accessToken
                ) { result in
                    switch result {
                    case .failure(let error):
                        completion(.failure(error))
                        log.error("Failed to submit vote because: \(error.localizedDescription)")
                    case .success(let voteResource):
                        let vote = PredictionVote(resource: voteResource)
                        self.widgetInteractionRepo.add(interactions: [.predictionVote(vote)])
                        self.voteRepo.add(vote: vote) { _ in }
                        self.leaderboardManager.notifyCurrentPositionChange(rewards: voteResource.rewards)
                        log.debug("Prediction vote successfuly created for id \(vote.id)")
                        completion(.success(vote))
                    }
                }
            }
        }
    }

    @available(*, deprecated, message: "The `PredictionWidgetModel` has been updated to allow updating previously submitted votes. This method has been renamed to `submitVote` and can be called multiple times per user.")
    public func lockInVote(optionID: String, completion: @escaping ((Result<PredictionVote, Error>) -> Void) = { _ in }) {
        self.submitVote(optionID: optionID, completion: completion)
    }

    // MARK: Types

    /// A Prediction Option
    public class Option {
        /// The id of the Option. Use this to `lockInVote`.
        public let id: String
        /// The text description of the Option
        public let text: String
        /// The image of the Option
        public let imageURL: URL?
        /// The update total vote count for the Option
        @Atomic public internal(set) var voteCount: Int
        /// Earnable Rewards associated with this option
        public let earnableRewards: [EarnableReward]

        let voteURL: URL

        init(resource: TextPredictionCreatedOption) {
            self.id = resource.id
            self.text = resource.description
            self.imageURL = nil
            self.voteCount = resource.voteCount
            self.voteURL = resource.voteUrl
            self.earnableRewards = resource.earnableRewards
        }

        init(resource: ImagePredictionOption) {
            self.id = resource.id
            self.text = resource.description
            self.imageURL = resource.imageUrl
            self.voteCount = resource.voteCount
            self.voteURL = resource.voteUrl
            self.earnableRewards = [] // Image Predictions do not support per-option rewards
        }
    }

    enum Errors: LocalizedError {
        case voteAlreadySubmitted
        case failedDueToInvalidOptionID
        case cannotUpdateVote
        var errorDescription: String? {
            switch self {
            case .voteAlreadySubmitted:
                return "Prediction vote already submitted"
            case .failedDueToInvalidOptionID:
                return "Vote failed due to invalid Option ID"
            case .cannotUpdateVote:
                return "Cannot update a vote from previous app session"
            }
        }
    }
}

// MARK: WidgetProxyInput

/// :nodoc:
extension PredictionWidgetModel: WidgetProxyInput {
    func publish(event: WidgetProxyPublishData) {
        switch event.clientEvent {
        case .widget(let widgetResource):
            switch widgetResource {
            case .textPredictionFollowUp(let payload):
                guard payload.textPredictionId == id else { return }
                self.isFollowUpPublished = true
                self.followUpWidgetModels.append(self.followUpModelFactory.makePredictionFollowUpModel(resource: payload))
                self.delegate?.predictionWidgetModel(self, didReceiveFollowUp: payload.id, kind: payload.kind)
            case .imagePredictionFollowUp(let payload):
                guard payload.imagePredictionId == id else { return }
                self.isFollowUpPublished = true
                self.followUpWidgetModels.append(self.followUpModelFactory.makePredictionFollowUpModel(resource: payload))
                self.delegate?.predictionWidgetModel(self, didReceiveFollowUp: payload.id, kind: payload.kind)
            default:
                break
            }
        case .textPredictionResults(let results), .imagePredictionResults(let results):
            guard results.id == self.id else { return }
            // Update model
            self.totalVoteCount = results.options.map { $0.voteCount }.reduce(0, +)
            results.options.forEach { result in
                self.options.first(where: { $0.id == result.id })?.voteCount = result.voteCount
            }
            // Notify delegate
            results.options.forEach { result in
                self.delegate?.predictionWidgetModel(self, voteCountDidChange: result.voteCount, forOption: result.id)
            }
        default:
            log.verbose("Unexpected message payload on this channel.")
        }
    }

    func error(_ error: Error) {
        log.error(error.localizedDescription)
    }

    // Not implemented
    func discard(event: WidgetProxyPublishData, reason: DiscardedReason) {}
    func connectionStatusDidChange(_ status: ConnectionStatus) {}
}
