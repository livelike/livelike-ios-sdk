//
//  WidgetKind.swift
//  LiveLikeSDK
//
//  Created by Cory Sullivan on 2019-02-04.
//

import Foundation

/// An enumeration of the different kinds of widgets.
public enum WidgetKind: Int {
    case textPrediction
    case textPredictionFollowUp
    case imagePrediction
    case imagePredictionFollowUp
    case imagePoll
    case textPoll
    case alert
    case textQuiz
    case imageQuiz
    case imageSlider
    case cheerMeter
    case socialEmbed
    case videoAlert
    case textAsk
    case textNumberPrediction
    case imageNumberPrediction
    case textNumberPredictionFollowUp
    case imageNumberPredictionFollowUp
    case richPost
}

public extension WidgetKind {
    /**
     A human readable display name for the type of widget.
     */
    var displayName: String {
        return analyticsName
    }
}

/// :nodoc:
extension WidgetKind {
    // The expected values of 'kind' from widget messages sent from PubNub
    var stringValue: String {
        switch self {
        case .textPrediction: return "text-prediction"
        case .textPredictionFollowUp: return "text-prediction-follow-up"
        case .imagePrediction: return "image-prediction"
        case .imagePredictionFollowUp: return "image-prediction-follow-up"
        case .imagePoll: return "image-poll"
        case .textPoll: return "text-poll"
        case .alert: return "alert"
        case .textQuiz: return "text-quiz"
        case .imageQuiz: return "image-quiz"
        case .imageSlider: return "emoji-slider"
        case .cheerMeter: return "cheer-meter"
        case .socialEmbed: return "social-embed"
        case .videoAlert: return "video-alert"
        case .textAsk: return "text-ask"
        case .textNumberPrediction: return "text-number-prediction"
        case .imageNumberPrediction: return "image-number-prediction"
        case .textNumberPredictionFollowUp: return "text-number-prediction-follow-up"
        case .imageNumberPredictionFollowUp: return "image-number-prediction-follow-up"
        case .richPost: return "rich-post"
        }
    }

    var analyticsName: String {
        switch self {
        case .textPrediction:
            return "Text Prediction"
        case .textPredictionFollowUp:
            return "Text Prediction Follow-Up"
        case .imagePrediction:
            return "Image Prediction"
        case .imagePredictionFollowUp:
            return "Image Prediction Follow-Up"
        case .imagePoll:
            return "Image Poll"
        case .textPoll:
            return "Text Poll"
        case .alert:
            return "Alert"
        case .textQuiz:
            return "Text Quiz"
        case .imageQuiz:
            return "Image Quiz"
        case .imageSlider:
            return "Image Slider"
        case .cheerMeter:
            return "Cheer Meter"
        case .socialEmbed:
            return "Social Embed"
        case .videoAlert:
            return "Video Alert"
        case .textAsk:
            return "Text Ask"
        case .textNumberPrediction:
            return "Text Number Prediction"
        case .imageNumberPrediction:
            return "Image Number Prediction"
        case .textNumberPredictionFollowUp: return "Text Number Prediction Follow Up"
        case .imageNumberPredictionFollowUp: return "Image Number Prediction Follow Up"
        case .richPost: return "Rich Post"
        }
    }

    var isInteractable: Bool {
        switch self {
        case .textPrediction:
            return true
        case .textPredictionFollowUp:
            return true
        case .imagePrediction:
            return true
        case .imagePredictionFollowUp:
            return true
        case .imagePoll:
            return true
        case .textPoll:
            return true
        case .alert:
            return false
        case .textQuiz:
            return true
        case .imageQuiz:
            return true
        case .imageSlider:
            return true
        case .cheerMeter:
            return true
        case .socialEmbed:
            return false
        case .videoAlert:
            return false
        case .textAsk:
            return true
        case .textNumberPrediction:
            return true
        case .imageNumberPrediction:
            return true
        case .textNumberPredictionFollowUp:
            return true
        case .imageNumberPredictionFollowUp:
            return true
        case .richPost:
            return false
        }
    }

    static var interactableKinds: Set<WidgetKind> {
        return Set(WidgetKind.allCases.filter { $0.isInteractable })
    }

}

/// :nodoc:
extension WidgetKind: Codable {
    public func encode(to encoder: Encoder) throws {
        var encoder = encoder.singleValueContainer()
        try encoder.encode(stringValue)
    }

    public init(from decoder: Decoder) throws {
        let decoder = try decoder.singleValueContainer()
        let kindString = try decoder.decode(String.self)
        guard let kind = WidgetKind(stringValue: kindString) else {
            let description = "Invalid WidgetKind string"
            throw DecodingError.dataCorruptedError(in: decoder, debugDescription: description)
        }
        self = kind
    }
}

extension WidgetKind {
    public init?(stringValue: String) {
        guard let kind = WidgetKind.allCases.first(where: { $0.stringValue == stringValue }) else {
            return nil
        }
        self = kind
    }
}

extension WidgetKind: CaseIterable {}

extension WidgetKind: Equatable {}
