//
//  PublishedWidgetInfo.swift
//  
//
//  Created by Jelzon Monzon on 8/7/23.
//

import Foundation

public struct PublishedWidgetInfo {
    /// The publish status of the widget
    public let status: WidgetPublishStatus
    /// The date which the widget is scheduled to be published
    public let scheduledAt: Date
    /// The date which the widget was published
    public let publishedAt: Date?
    /// The iso8601 duration of which the widget publishing is delayed
    public let publishDelayISO8601: String
    /// The program date time  of the Widget
    public let programDateTime: Date?
}

extension PublishedWidgetInfo: Decodable {
    enum CodingKeys: String, CodingKey {
        case status
        case scheduledAt
        case publishedAt
        case publishDelayISO8601 = "publishDelay"
        case programDateTime
    }
}
