//
//  NumberPredictionFollowUpWidgetModel.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 9/22/21.
//

import Foundation
import LiveLikeCore

/// A object used to interact with a Number Prediction Follow Up Widget
class InternalNumberPredictionFollowUpWidgetModel: BaseWidgetModel, NumberPredictionFollowUpWidgetModel {
    
    // MARK: - Data
    
    let question: String
    let options: [NumberPredictionOption]
    let containsImages: Bool
    var userVotes: [NumberPredictionVote] {
        return self.widgetInteractionRepo.get(widgetID: self.associatedPredictionID)?.numberPredictionVotes ?? []
    }
    let associatedPredictionID: String
    let associatedPredictionKind: WidgetKind
    
    private let leaderboardAPI: LiveLikeLeaderboardAPIProtocol
    private let widgetInteractionRepo: WidgetInteractionRepository
    private let claimURL: URL
    private let leaderboardsManager: LeaderboardsManager
    private let rewardItems: [RewardItem]
    
    let interactionURL: URL?
    
    init(
        resource: NumberPredictionFollowUp,
        eventRecorder: EventRecorder,
        widgetAPI: LiveLikeWidgetAPIProtocol,
        leaderboardAPI: LiveLikeLeaderboardAPIProtocol,
        userProfile: UserProfileProtocol,
        widgetInteractionRepo: WidgetInteractionRepository,
        leaderboardsManager: LeaderboardsManager,
        rewardItems: [RewardItem]
    ) {
        self.question = resource.question
        self.options = resource.options
        self.widgetInteractionRepo = widgetInteractionRepo
        self.interactionURL = try? WidgetResource.getWidgetInteractionsURL(
            fromTemplate: resource.widgetInteractionsURLTemplate,
            profileID: userProfile.userID.asString
        )
        self.containsImages = resource.options.allSatisfy { $0.imageURL != nil }
        self.associatedPredictionID = resource.predictionID
        self.associatedPredictionKind = resource.predictionKind
        self.leaderboardAPI = leaderboardAPI
        self.claimURL = resource.claimURL
        self.leaderboardsManager = leaderboardsManager
        self.rewardItems = rewardItems
        super.init(
            baseData: resource,
            eventRecorder: eventRecorder,
            livelikeAPI: widgetAPI,
            userProfile: userProfile
        )
    }
    
    /// Call this to load the user's interaction history for this Widget
    /// - Parameter completion: Returns the loaded votes
    public func loadInteractionHistory(completion: @escaping (Result<[NumberPredictionVote], Error>) -> Void = { _ in }) {
        self.widgetInteractionRepo.get(widgetModel: .numberPredictionFollowUp(self)) { result in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let interactions):
                completion(.success(interactions.numberPredictionVotes))
            }
        }
    }
    
    /// Attempts to claim rewards for this Number Prediction Follow Up
    /// This method will look-up the user's interaction history to check for an eligibile reward claim token
    /// - Parameter completion: Returns the rewards earned
    public func claimRewards(
        completion: @escaping (Result<[Reward], Error>) -> Void
    ) {
        self.loadInteractionHistory { result in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let votes):
                guard
                    let claimToken = votes.first?.claimToken
                else {
                    return completion(.failure(PredictionFollowUpModelErrors.claimTokenNotFound))
                }
                
                firstly {
                    self.leaderboardAPI.claimRewards(
                        claimURL: self.claimURL,
                        claimToken: claimToken,
                        accessToken: self.userProfile.accessToken
                    )
                }.then { rewardsResource in
                    let rewards = Reward.createRewards(
                        availableRewardItems: self.rewardItems,
                        rewardResources: rewardsResource.rewards,
                        widgetInfo: .init(id: self.id, kind: self.kind)
                    )
                    self.leaderboardsManager.notifyCurrentPositionChange(
                        rewards: rewardsResource.rewards
                    )
                    log.info("Successfully claimed rewards for number prediction \(self.id)")
                    completion(.success(rewards))
                }.catch { error in
                    log.error(error)
                    completion(.failure(error))
                }
            }
        }
    }
}

class NumberPredictionFollowUp: BaseWidgetResource {
    
    let question: String
    let options: [NumberPredictionOption]
    let widgetInteractionsURLTemplate: String
    let predictionID: String
    let predictionKind: WidgetKind
    let claimURL: URL
    let status: WidgetStatus
    
    required init(from decoder: Decoder) throws {
        enum Errors: Error {
            case unepectedWidgetKind
        }
        
        enum CodingKeys: CodingKey {
            case question
            case options
            case kind
            case widgetInteractionsUrlTemplate
            case textNumberPredictionId
            case imageNumberPredictionId
            case claimUrl
            case status
        }
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let kind = try container.decode(WidgetKind.self, forKey: .kind)
        self.question = try container.decode(String.self, forKey: .question)
        self.options = try container.decode([NumberPredictionOption].self, forKey: .options)
        self.widgetInteractionsURLTemplate = try container.decode(String.self, forKey: .widgetInteractionsUrlTemplate)
        if kind == .textNumberPredictionFollowUp {
            self.predictionID = try container.decode(String.self, forKey: .textNumberPredictionId)
            self.predictionKind = .textNumberPrediction
        } else if kind == .imageNumberPredictionFollowUp {
            self.predictionID = try container.decode(String.self, forKey: .imageNumberPredictionId)
            self.predictionKind = .imageNumberPrediction
        } else {
            throw Errors.unepectedWidgetKind
        }
        self.claimURL = try container.decode(URL.self, forKey: .claimUrl)
        self.status = try container.decode(WidgetStatus.self, forKey: .status)
        try super.init(from: decoder)
    }
    
    init(baseData: BaseWidgetResource, question: String, options: [NumberPredictionOption], widgetInteractionsURLTemplate: String, predictionID: String, predictionKind: WidgetKind, claimURL: URL, status: WidgetStatus) {
        self.question = question
        self.options = options
        self.widgetInteractionsURLTemplate = widgetInteractionsURLTemplate
        self.predictionID = predictionID
        self.predictionKind = predictionKind
        self.claimURL = claimURL
        self.status = status
        super.init(baseData: baseData)
    }
}
