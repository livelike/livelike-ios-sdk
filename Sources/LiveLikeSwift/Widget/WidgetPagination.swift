//
//  WidgetPagination.swift
//  
//
//  Created by Jelzon Monzon on 6/21/23.
//

import LiveLikeCore

/// Represents the different  pagination types that can be passed down to `getPostedWidgets`
public enum WidgetPagination {
    case first
    case next
}

extension WidgetPagination {
    var asPagination: Pagination {
        switch self {
        case .first:
            return .first
        case .next:
            return .next
        }
    }
}
