//
//  TextPollCreated.swift
//  LiveLikeSDK
//
//  Created by jelzon on 3/19/19.
//

import Foundation

class TextPollCreated: BaseWidgetResource {
    internal init(baseData: BaseWidgetResource, question: String, options: [TextPollOption], rewardsUrl: URL? = nil, widgetInteractionsUrlTemplate: String, sponsors: [Sponsor]) {
        self.question = question
        self.options = options
        self.rewardsUrl = rewardsUrl
        self.widgetInteractionsUrlTemplate = widgetInteractionsUrlTemplate
        self.sponsors = sponsors
        super.init(baseData: baseData)
    }

    var question: String
    var options: [TextPollOption]
    var rewardsUrl: URL?
    let widgetInteractionsUrlTemplate: String
    let sponsors: [Sponsor]

    enum CodingKeys: CodingKey {
        case question
        case options
        case rewardsUrl
        case widgetInteractionsUrlTemplate
        case sponsors
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.question = try container.decode(String.self, forKey: .question)
        self.options = try container.decode([TextPollOption].self, forKey: .options)
        self.rewardsUrl = try container.decodeIfPresent(URL.self, forKey: .rewardsUrl)
        self.widgetInteractionsUrlTemplate = try container.decode(String.self, forKey: .widgetInteractionsUrlTemplate)
        self.sponsors = try container.decode([Sponsor].self, forKey: .sponsors)
        try super.init(from: decoder)
    }
}

struct TextPollOption: Decodable {
    var id: String
    var description: String
    var voteCount: Int
    var voteUrl: URL
}
