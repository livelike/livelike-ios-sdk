//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

public struct CreateTextQuizWidgetOptions {

    /// - Parameters:
    ///   - question: The question the Quiz widget will ask
    ///   - options: The possible answers of the Quiz widget
    public init(common: CommonCreateWidgetOptions, question: String, choices: [Choice]) {
        self.common = common
        self.question = question
        self.choices = choices
    }

    let common: CommonCreateWidgetOptions
    let question: String
    let choices: [Choice]

    public struct Choice {
        /// - Parameters:
        ///   - text: A text description of the choice
        ///   - isCorrect: Whether the choice is a correct answer to the Quiz
        public init(text: String, isCorrect: Bool) {
            self.text = text
            self.isCorrect = isCorrect
        }

        let text: String
        let isCorrect: Bool
    }
}

extension CreateTextQuizWidgetOptions: Encodable {
    enum CodingKeys: CodingKey {
        case question
        case choices
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(question, forKey: .question)
        try container.encode(choices, forKey: .choices)
        try common.encode(to: encoder)
    }
}

extension CreateTextQuizWidgetOptions.Choice: Encodable {
    enum CodingKeys: CodingKey {
        case description
        case isCorrect
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(text, forKey: .description)
        try container.encode(isCorrect, forKey: .isCorrect)
    }
}
