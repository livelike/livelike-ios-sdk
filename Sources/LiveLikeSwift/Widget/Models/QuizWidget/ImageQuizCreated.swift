//
//  ImageQuizCreated.swift
//  LiveLikeSDK
//
//  Created by jelzon on 3/25/19.
//

import Foundation

class ImageQuizCreated: BaseWidgetResource {
    internal init(baseData: BaseWidgetResource, question: String, choices: [ImageQuizChoice], rewardsUrl: URL?, widgetInteractionsUrlTemplate: String, sponsors: [Sponsor]) {
        self.question = question
        self.choices = choices
        self.rewardsUrl = rewardsUrl
        self.widgetInteractionsUrlTemplate = widgetInteractionsUrlTemplate
        self.sponsors = sponsors
        super.init(baseData: baseData)
    }

    let question: String
    let choices: [ImageQuizChoice]
    let rewardsUrl: URL?
    let widgetInteractionsUrlTemplate: String
    let sponsors: [Sponsor]

    enum CodingKeys: CodingKey {
        case question
        case choices
        case rewardsUrl
        case widgetInteractionsUrlTemplate
        case sponsors
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.question = try container.decode(String.self, forKey: .question)
        self.choices = try container.decode([ImageQuizChoice].self, forKey: .choices)
        self.rewardsUrl = try container.decodeIfPresent(URL.self, forKey: .rewardsUrl)
        self.widgetInteractionsUrlTemplate = try container.decode(String.self, forKey: .widgetInteractionsUrlTemplate)
        self.sponsors = try container.decode([Sponsor].self, forKey: .sponsors)
        try super.init(from: decoder)
    }
}

struct ImageQuizChoice: Decodable {
    let id: String
    let description: String
    let imageUrl: URL
    let isCorrect: Bool
    let answerCount: Int
    let answerUrl: URL
}
