//
//  ImageSliderCreated.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 5/13/19.
//

import Foundation

typealias StringDouble = String

extension StringDouble {
    var number: Double? {
        return Double(self)
    }
}

class ImageSliderCreated: BaseWidgetResource {
    internal init(baseData: BaseWidgetResource, initialMagnitude: StringDouble, voteUrl: URL, question: String, url: URL, options: [ImageSliderOption], rewardsUrl: URL? = nil, averageMagnitude: String? = nil, widgetInteractionsUrlTemplate: String, sponsors: [Sponsor], engagementCount: Int) {
        self.initialMagnitude = initialMagnitude
        self.voteUrl = voteUrl
        self.question = question
        self.url = url
        self.options = options
        self.rewardsUrl = rewardsUrl
        self.averageMagnitude = averageMagnitude
        self.widgetInteractionsUrlTemplate = widgetInteractionsUrlTemplate
        self.sponsors = sponsors
        self.engagementCount = engagementCount
        super.init(baseData: baseData)
    }

    var initialMagnitude: StringDouble
    var voteUrl: URL
    var question: String
    var url: URL
    var options: [ImageSliderOption]
    var rewardsUrl: URL?
    var averageMagnitude: String?
    let widgetInteractionsUrlTemplate: String
    let sponsors: [Sponsor]
    let engagementCount: Int

    enum CodingKeys: CodingKey {
        case initialMagnitude
        case voteUrl
        case question
        case url
        case options
        case rewardsUrl
        case averageMagnitude
        case widgetInteractionsUrlTemplate
        case sponsors
        case engagementCount
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.initialMagnitude = try container.decode(StringDouble.self, forKey: .initialMagnitude)
        self.voteUrl = try container.decode(URL.self, forKey: .voteUrl)
        self.question = try container.decode(String.self, forKey: .question)
        self.url = try container.decode(URL.self, forKey: .url)
        self.options = try container.decode([ImageSliderOption].self, forKey: .options)
        self.rewardsUrl = try container.decodeIfPresent(URL.self, forKey: .rewardsUrl)
        self.averageMagnitude = try container.decodeIfPresent(String.self, forKey: .averageMagnitude)
        self.widgetInteractionsUrlTemplate = try container.decode(String.self, forKey: .widgetInteractionsUrlTemplate)
        self.sponsors = try container.decode([Sponsor].self, forKey: .sponsors)
        self.engagementCount = try container.decode(Int.self, forKey: .engagementCount)
        try super.init(from: decoder)
    }
}

struct ImageSliderOption: Decodable {
    var id: String
    var imageUrl: URL
}
