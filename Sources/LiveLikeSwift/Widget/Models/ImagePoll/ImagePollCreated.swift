//
//  ImagePollCreated.swift
//  LiveLikeSDK
//
//  Created by jelzon on 3/14/19.
//

import Foundation

typealias Timeout = String

extension Timeout {
    var timeInterval: TimeInterval {
        return timeIntervalFromISO8601Duration() ?? 7
    }
}

class ImagePollCreated: BaseWidgetResource {
    internal init(baseData: BaseWidgetResource, question: String, options: [ImagePollOption], rewardsUrl: URL? = nil, widgetInteractionsUrlTemplate: String, sponsors: [Sponsor]) {
        self.question = question
        self.options = options
        self.rewardsUrl = rewardsUrl
        self.widgetInteractionsUrlTemplate = widgetInteractionsUrlTemplate
        self.sponsors = sponsors
        super.init(baseData: baseData)
    }

    var question: String
    var options: [ImagePollOption]
    var rewardsUrl: URL?
    let widgetInteractionsUrlTemplate: String
    let sponsors: [Sponsor]

    enum CodingKeys: CodingKey {
        case question
        case options
        case rewardsUrl
        case widgetInteractionsUrlTemplate
        case sponsors
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.question = try container.decode(String.self, forKey: .question)
        self.options = try container.decode([ImagePollOption].self, forKey: .options)
        self.rewardsUrl = try container.decodeIfPresent(URL.self, forKey: .rewardsUrl)
        self.widgetInteractionsUrlTemplate = try container.decode(String.self, forKey: .widgetInteractionsUrlTemplate)
        self.sponsors = try container.decode([Sponsor].self, forKey: .sponsors)
        try super.init(from: decoder)
    }
}

struct ImagePollOption: Decodable {
    var id: String
    var description: String
    var imageUrl: URL
    var voteCount: Int
    var voteUrl: URL
}
