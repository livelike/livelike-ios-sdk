//
//  WidgetViewModelable.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 10/20/20.
//

import Foundation

/// Common properties and methods of a Widget Model
public protocol WidgetModelable {
    /// The unique id of the Widget
    var id: String { get }

    /// The kind of Widget this model represents
    var kind: WidgetKind { get }

    /// The date and time the widget has been created
    var createdAt: Date { get }

    /// The date and time the widget has been published from the Producer Suite
    var publishedAt: Date? { get }

    /// The time interval for which the user is able to interact with the widget
    var interactionTimeInterval: TimeInterval { get }

    /// Used to pass misc data 
    var customData: String? { get }

    var programID: String { get }

    /// The `Date` at which the Widget will accept interactions until
    var interactiveUntil: Date? { get }

    /// Key Value attributes associated with the widget
    var widgetAttributes: [Attribute] { get }

    /// An `impression` is used to calculate user engagement on the Producer Site.
    /// Call this once when the widget is first displayed to the user.
    func registerImpression(completion: @escaping (Result<Void, Error>) -> Void)

    /// Mark the widget as interactive to improve engagement rate calculations
    /// This should be done as soon as the user is able to interact with your UI
    func markAsInteractive()

    func getSecondsUntilInteractivityExpiration() -> TimeInterval?
}

protocol AlertWidgetModelable: WidgetModelable {

    /// alert widget title
    var title: String? { get }

    /// widget link
    var linkURL: URL? { get }

    var linkLabel: String? { get }

    /// widget content
    var text: String? { get }

    /// image url representing an image
    var imageURL: URL? { get }

    /// opens the `link` URL if it's available
    func openLinkUrl()
}

protocol CheerMeterWidgetModelable: WidgetModelable {

    var delegate: CheerMeterWidgetModelDelegate? { get set }

    var title: String { get }

    var options: [CheerMeterWidgetModel.Option] { get }

    func submitVote(optionID: String)

}

protocol QuizWidgetModelable: WidgetModelable {

    var delegate: QuizWidgetModelDelegate? { get set }

    var question: String { get }

    var choices: [QuizWidgetModel.Choice] { get }

    var totalAnswerCount: Int { get }

    func lockInAnswer(choiceID: String, completion: @escaping (Result<QuizWidgetModel.Answer, Error>) -> Void)

}

protocol PredictionWidgetModelable: WidgetModelable {
    var delegate: PredictionWidgetModelDelegate? { get set }

    var question: String { get }

    var options: [PredictionWidgetModel.Option] { get }

    var confirmationMessage: String { get }

    var totalVoteCount: Int { get }

    func lockInVote(optionID: String, completion: @escaping (Result<PredictionVote, Error>) -> Void)

}

protocol PredictionFollowUpWidgetModelable: WidgetModelable {

    var question: String { get }

    var options: [PredictionFollowUpWidgetModel.Option] { get }

    func getVote(completion: @escaping (Result<PredictionVote, Error>) -> Void)

    func claimRewards(vote: PredictionVote, completion: @escaping (Result<[Reward], Error>) -> Void)
}

protocol PollWidgetModelable: WidgetModelable {

    var delegate: PollWidgetModelDelegate? { get set }

    var question: String { get }

    var options: [PollWidgetModel.Option] { get }

    var totalVoteCount: Int { get }

    func submitVote(optionID: String, completion: @escaping (Result<PollWidgetModel.Vote, Error>) -> Void)

}

protocol ImageSliderWidgetModelable: WidgetModelable {

    var delegate: ImageSliderWidgetModelDelegate? { get set }

    var question: String { get }

    var initialMagnitude: Double { get }

    var averageMagnitude: Double { get }

    var options: [ImageSliderWidgetModel.Option] { get }

    func lockInVote(magnitude: Double, completion: @escaping (Result<ImageSliderWidgetModel.Vote, Error>) -> Void)
}

protocol SocialEmbedWidgetModelable: WidgetModelable {

    var comment: String? { get }

    var items: [SocialEmbedWidgetModel.Item] { get }
}

protocol TextAskWidgetModelable: WidgetModelable {
    var title: String { get }
    var prompt: String { get }
    var confirmationMessage: String { get }
    var userReplies: [TextAskWidgetModel.Reply] { get }
    var mostRecentReply: TextAskWidgetModel.Reply? { get }
    func submitReply(_ reply: String, completion: @escaping (Result<TextAskWidgetModel.Reply, Error>) -> Void)
    func loadInteractionHistory(completion: @escaping (Result<[TextAskWidgetModel.Reply], Error>) -> Void)
}

/// An object used to interact with Number Prediction Widgets
public protocol NumberPredictionWidgetModel: AnyObject, WidgetModelable {

    /// The object that acts as the delgate for the `NumberPredictionWidgetModel`
    var delegate: NumberPredictionWidgetModelDelegate? { get set }

    /// The question or prompt asked by the Number Prediction
    var question: String { get }

    /// The options of the Number Prediction
    var options: [NumberPredictionOption] { get }

    /// A confirmation message of the Number Prediction
    var confirmationMessage: String { get }

    /// True if the widget contains images
    var containsImages: Bool { get }

    /// The user's votes on this Prediction
    /// Call `loadInteractionHistory` to fetch votes before using
    var userVotes: [NumberPredictionVote] { get }

    /// Whether the Follow Up for this Prediction has been published
    var isFollowUpPublished: Bool { get }

    /// The follows up to this Number Prediction
    var followUpWidgetModels: [NumberPredictionFollowUpWidgetModel] { get }

    /// Locks the user's vote
    /// - Parameters:
    ///   - submissions: An array of vote submission for each options. All options must be submitted at the same time.
    func lockVotes(
        submissions: [NumberPredictionVoteSubmission],
        completion: @escaping (Result<NumberPredictionVote, Error>) -> Void
    )

    /// Call this to load the user's interaction history for the Widget
    /// - Parameter completion: Returns the loaded votes
    func loadInteractionHistory(
        completion: @escaping (Result<[NumberPredictionVote], Error>) -> Void
    )
}

/// An object used for interacting with a Number Prediction Follow Up Widget
public protocol NumberPredictionFollowUpWidgetModel: AnyObject, WidgetModelable {
    /// The question or prompt asked by the Number Prediction Follow Up
    var question: String { get }
    /// The options of the Number Prediction Follow Up
    var options: [NumberPredictionOption] { get }
    /// True if the widget contains images
    var containsImages: Bool { get }
    /// The user's votes on this Prediction
    /// Call `loadInteractionHistory` to fetch votes before using
    var userVotes: [NumberPredictionVote] { get }
    /// The id of the Number Prediction that this Widget follows up
    var associatedPredictionID: String { get }
    /// The kind of the Number Prediciton that this Widget follows up
    var associatedPredictionKind: WidgetKind { get }

    /// Call this to load the user's interaction history for the associated Number Prediction Widget
    /// - Parameter completion: Returns the loaded votes
    func loadInteractionHistory(
        completion: @escaping (Result<[NumberPredictionVote], Error>) -> Void
    )

    /// Call this method to claim the rewards the user has earned
    func claimRewards(
        completion: @escaping (Result<[Reward], Error>) -> Void
    )
}
