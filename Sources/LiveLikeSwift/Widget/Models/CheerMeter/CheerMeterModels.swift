//
//  CheerMeterModels.swift
//  EngagementSDK
//
//  Created by Xavi Matos on 6/7/19.
//

import Foundation

class CheerMeterCreated: BaseWidgetResource {
    internal init(baseData: BaseWidgetResource, rewardsUrl: URL? = nil, question: String, options: [CheerOption], widgetInteractionsUrlTemplate: String, sponsors: [Sponsor]) {
        self.rewardsUrl = rewardsUrl
        self.question = question
        self.options = options
        self.widgetInteractionsUrlTemplate = widgetInteractionsUrlTemplate
        self.sponsors = sponsors
        super.init(baseData: baseData)
    }

    var rewardsUrl: URL?
    let question: String
    let options: [CheerOption]
    let widgetInteractionsUrlTemplate: String
    let sponsors: [Sponsor]

    enum CodingKeys: CodingKey {
        case question
        case options
        case rewardsUrl
        case widgetInteractionsUrlTemplate
        case sponsors
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.question = try container.decode(String.self, forKey: .question)
        self.options = try container.decode([CheerOption].self, forKey: .options)
        self.rewardsUrl = try container.decodeIfPresent(URL.self, forKey: .rewardsUrl)
        self.widgetInteractionsUrlTemplate = try container.decode(String.self, forKey: .widgetInteractionsUrlTemplate)
        self.sponsors = try container.decode([Sponsor].self, forKey: .sponsors)
        try super.init(from: decoder)
    }

    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(rewardsUrl, forKey: .rewardsUrl)
        try container.encode(question, forKey: .question)
        try container.encode(options, forKey: .options)
        try container.encode(widgetInteractionsUrlTemplate, forKey: .widgetInteractionsUrlTemplate)
        try container.encode(sponsors, forKey: .sponsors)
        try super.encode(to: encoder)
    }
}

struct CheerOption: Codable {
    let id: String
    let description: String
    let imageUrl: URL
    let voteUrl: URL
    let voteCount: Int
}

struct CheerMeterResults: Decodable {
    let id: String
    let options: [CheerMeterResult]
}

struct CheerMeterResult: Decodable {
    let id: String
    let voteCount: Int
}
