//
//  SocialEmbedWidgetResource.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 2/1/21.
//

import Foundation

class SocialEmbedWidgetResource: BaseWidgetResource {
    let items: [Item]
    let comment: String?
    let rewardsUrl: URL?
    let sponsors: [Sponsor]

    enum CodingKeys: CodingKey {
        case items
        case comment
        case rewardsUrl
        case sponsors
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.items = try container.decode([Item].self, forKey: .items)
        self.comment = try container.decodeIfPresent(String.self, forKey: .comment)
        self.rewardsUrl = try container.decodeIfPresent(URL.self, forKey: .rewardsUrl)
        self.sponsors = try container.decode([Sponsor].self, forKey: .sponsors)
        try super.init(from: decoder)
    }

    struct Item: Decodable {
        let id: String
        let url: URL
        let oembed: OEmbed

        // swiftlint:disable nesting
        struct OEmbed: Decodable {
            let html: String
            let providerName: String
        }
    }
}
