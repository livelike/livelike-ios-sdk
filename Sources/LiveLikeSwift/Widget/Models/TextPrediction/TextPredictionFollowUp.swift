//
//  TextPredictionFollowUp.swift
//  LiveLikeSDK
//
//  Created by Cory Sullivan on 2019-02-05.
//

import Foundation

struct TextPredictionFollowUpOption: Decodable {
    let voteCount: Int
    let voteUrl: URL
    let description: String
    let id: String
    let isCorrect: Bool
    let url: URL
}

class TextPredictionFollowUp: BaseWidgetResource {
    internal init(baseData: BaseWidgetResource, options: [TextPredictionFollowUpOption], question: String, textPredictionUrl: URL, url: URL, rewardsUrl: URL?, claimUrl: URL, textPredictionId: String, widgetInteractionsUrlTemplate: String, status: WidgetStatus, sponsors: [Sponsor]) {
        self.options = options
        self.question = question
        self.textPredictionUrl = textPredictionUrl
        self.url = url
        self.rewardsUrl = rewardsUrl
        self.claimUrl = claimUrl
        self.textPredictionId = textPredictionId
        self.widgetInteractionsUrlTemplate = widgetInteractionsUrlTemplate
        self.status = status
        self.sponsors = sponsors
        super.init(baseData: baseData)
    }

    let options: [TextPredictionFollowUpOption]
    let question: String
    let textPredictionUrl: URL
    let url: URL
    let rewardsUrl: URL?
    let claimUrl: URL
    let textPredictionId: String
    let widgetInteractionsUrlTemplate: String
    let status: WidgetStatus
    let sponsors: [Sponsor]

    enum CodingKeys: String, CodingKey {
        case id
        case createdAt
        case publishedAt
        case kind
        case options
        case programId
        case question
        case subscribeChannel
        case textPredictionUrl
        case timeout
        case impressionUrl
        case rewardsUrl
        case url
        case programDateTime
        case customData
        case claimUrl
        case textPredictionId
        case widgetInteractionsUrlTemplate
        case status
        case sponsors
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        options = try container.decode([TextPredictionFollowUpOption].self, forKey: .options)
        question = try container.decode(String.self, forKey: .question)
        textPredictionUrl = try container.decode(URL.self, forKey: .textPredictionUrl)
        rewardsUrl = try? container.decode(URL.self, forKey: .rewardsUrl)
        url = try container.decode(URL.self, forKey: .url)
        claimUrl = try container.decode(URL.self, forKey: .claimUrl)
        textPredictionId = try container.decode(String.self, forKey: .textPredictionId)
        self.widgetInteractionsUrlTemplate = try container.decode(String.self, forKey: .widgetInteractionsUrlTemplate)
        self.status = try container.decode(WidgetStatus.self, forKey: .status)
        self.sponsors = try container.decode([Sponsor].self, forKey: .sponsors)
        try super.init(from: decoder)
    }
}

extension TextPredictionFollowUp {
    var correctOptionsIds: [String] {
        return options
            .filter({ $0.isCorrect })
            .map({ $0.id })
    }
}
