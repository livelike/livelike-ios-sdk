//
//  TextPredictionCreated.swift
//  LiveLikeSDK
//
//  Created by Cory Sullivan on 2019-02-04.
//

import Foundation

struct TextPredictionCreatedOption: Decodable {
    let description: String
    let id: String
    let url: URL
    let voteUrl: URL
    let voteCount: Int
    let earnableRewards: [EarnableReward]

    internal init(description: String, id: String, voteUrl: URL, voteCount: Int, earnableRewards: [EarnableReward], url: URL) {
        self.description = description
        self.id = id
        self.voteUrl = voteUrl
        self.voteCount = voteCount
        self.earnableRewards = earnableRewards
        self.url = url
    }
}

class TextPredictionCreated: BaseWidgetResource {
    internal init(
        baseData: BaseWidgetResource,
        confirmationMessage: String,
        followUpUrl: URL,
        options: [TextPredictionCreatedOption],
        question: String,
        url: URL,
        rewardsUrl: URL?,
        widgetInteractionsUrlTemplate: String,
        followUps: [TextPredictionFollowUp],
        sponsors: [Sponsor],
        earnableRewards: [EarnableReward]
    ) {
        self.confirmationMessage = confirmationMessage
        self.followUpUrl = followUpUrl
        self.options = options
        self.question = question
        self.url = url
        self.rewardsUrl = rewardsUrl
        self.widgetInteractionsUrlTemplate = widgetInteractionsUrlTemplate
        self.followUps = followUps
        self.sponsors = sponsors
        self.earnableRewards = earnableRewards
        super.init(baseData: baseData)
    }

    let confirmationMessage: String
    let followUpUrl: URL
    let options: [TextPredictionCreatedOption]
    let question: String
    let url: URL
    let rewardsUrl: URL?
    let widgetInteractionsUrlTemplate: String
    let followUps: [TextPredictionFollowUp]
    let sponsors: [Sponsor]
    let earnableRewards: [EarnableReward]

    enum CodingKeys: String, CodingKey {
        case confirmationMessage
        case createdAt
        case publishedAt
        case followUpUrl
        case id
        case kind
        case options
        case programId
        case question
        case subscribeChannel
        case timeout
        case impressionUrl
        case rewardsUrl
        case url
        case programDateTime
        case customData
        case widgetInteractionsUrlTemplate
        case followUps
        case sponsors
        case earnableRewards
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        confirmationMessage = try container.decode(String.self, forKey: .confirmationMessage)
        followUpUrl = try container.decode(URL.self, forKey: .followUpUrl)
        options = try container.decode([TextPredictionCreatedOption].self, forKey: .options)
        question = try container.decode(String.self, forKey: .question)
        rewardsUrl = try? container.decode(URL.self, forKey: .rewardsUrl)
        url = try container.decode(URL.self, forKey: .url)
        self.widgetInteractionsUrlTemplate = try container.decode(String.self, forKey: .widgetInteractionsUrlTemplate)
        self.followUps = try container.decode([TextPredictionFollowUp].self, forKey: .followUps)
        self.sponsors = try container.decode([Sponsor].self, forKey: .sponsors)
        self.earnableRewards = try container.decode([EarnableReward].self, forKey: .earnableRewards)
        try super.init(from: decoder)
    }
}
