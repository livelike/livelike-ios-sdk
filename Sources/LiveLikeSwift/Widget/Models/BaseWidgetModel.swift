//
//  BaseWidgetModel.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 4/13/22.
//

import Foundation
import LiveLikeCore

/// A class encapsulating data and functionality of every LiveLike Widget
public class BaseWidgetModel: WidgetModelable {
    
    /// The unique id of the Widget
    public let id: String
    /// The kind of Widget
    public let kind: WidgetKind
    /// The date when the Widget was created
    public let createdAt: Date
    /// The date when the Widget was published
    public let publishedAt: Date?
    /// The specified duration for interaction
    public let interactionTimeInterval: TimeInterval
    /// A string of custom data
    public let customData: String?
    /// The id of the Program this Widget belongs to
    public let programID: String
    /// The date in which the Widget will stop accepting interactions
    public let interactiveUntil: Date?
    /// A keyed set of Attributes of the Widget
    public let widgetAttributes: [Attribute]
    /// A timestamp hint for when this widget should be displayed
    public let playbackTimeMilliseconds: Int?
    
    let eventRecorder: EventRecorder
    let livelikeAPI: LiveLikeWidgetAPIProtocol
    let userProfile: UserProfileProtocol
    let subscribeChannel: String?
    let deleteURL: URL
    let scheduleURL: URL
    
    private let impressionURL: URL
    private let linkURL: URL?
    
    init(
        baseData: BaseWidgetResource,
        eventRecorder: EventRecorder,
        livelikeAPI: LiveLikeWidgetAPIProtocol,
        linkURL: URL? = nil,
        userProfile: UserProfileProtocol
    ) {
        self.id = baseData.id
        self.kind = baseData.kind
        self.createdAt = baseData.createdAt
        self.publishedAt = baseData.publishedAt
        self.interactionTimeInterval = baseData.timeout
        self.customData = baseData.customData
        self.programID = baseData.programID
        self.interactiveUntil = baseData.interactiveUntil
        self.widgetAttributes = baseData.widgetAttributes
        self.impressionURL = baseData.impressionURL
        self.playbackTimeMilliseconds = baseData.playbackTimeMilliseconds
        do {
            self.subscribeChannel = try baseData.getResultsChannel()
        } catch {
            self.subscribeChannel = nil
            log.info(error.localizedDescription)
        }
        
        self.eventRecorder = eventRecorder
        self.livelikeAPI = livelikeAPI
        self.userProfile = userProfile
        self.linkURL = linkURL
        self.deleteURL = baseData.deleteURL
        self.scheduleURL = baseData.scheduleURL
    }
    
    /// An `impression` is used to calculate user engagement on the Producer Site.
    /// Call this once when the widget is first displayed to the user.
    public func registerImpression(completion: @escaping (Result<Void, Error>) -> Void = { _ in }) {
        self.eventRecorder.record(
            .widgetDisplayed(
                programID: programID,
                kind: kind.analyticsName,
                widgetId: id,
                widgetLink: linkURL
            )
        )
        firstly {
            livelikeAPI.createImpression(
                impressionURL: impressionURL,
                userSessionID: userProfile.userID.asString,
                accessToken: userProfile.accessToken
            )
        }.then { _ in
            completion(.success(()))
        }.catch { error in
            completion(.failure(error))
        }
    }
    
    /// Mark the widget as interactive to improve engagement rate calculations
    /// This should be done as soon as the user is able to interact with your UI
    public func markAsInteractive() {
        self.eventRecorder.record(
            .widgetBecameInteractive(
                programID: programID,
                kind: kind,
                widgetID: id
            )
        )
    }
    
    public func isInteractivityExpired() -> Bool {
        guard let interactiveUntil = interactiveUntil else {
            return false
        }

        return interactiveUntil < Date()
    }
    
    public func getSecondsUntilInteractivityExpiration() -> TimeInterval? {
        guard let interactiveUntil = interactiveUntil else {
            return nil
        }
        return interactiveUntil.timeIntervalSince(Date())
    }
    
}
