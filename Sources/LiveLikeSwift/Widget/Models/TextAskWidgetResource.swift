//
//  TextAskWidgetResource.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 7/30/21.
//

import Foundation

class TextAskWidgetResource: BaseWidgetResource {
    internal init(baseData: BaseWidgetResource, title: String, prompt: String, confirmationMessage: String, replyURL: URL, widgetInteractionsURLTemplate: String) {
        self.title = title
        self.prompt = prompt
        self.confirmationMessage = confirmationMessage
        self.replyURL = replyURL
        self.widgetInteractionsURLTemplate = widgetInteractionsURLTemplate
        super.init(baseData: baseData)
    }

    let title: String
    let prompt: String
    let confirmationMessage: String
    let replyURL: URL
    let widgetInteractionsURLTemplate: String

    enum CodingKeys: CodingKey {
        case title
        case prompt
        case confirmationMessage
        case replyUrl
        case widgetInteractionsUrlTemplate
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.title = try container.decode(String.self, forKey: .title)
        self.prompt = try container.decode(String.self, forKey: .prompt)
        self.confirmationMessage = try container.decode(String.self, forKey: .confirmationMessage)
        self.replyURL = try container.decode(URL.self, forKey: .replyUrl)
        self.widgetInteractionsURLTemplate = try container.decode(String.self, forKey: .widgetInteractionsUrlTemplate)
        try super.init(from: decoder)
    }
}
