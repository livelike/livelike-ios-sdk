//
//  VideoWidgetResource.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 7/13/21.
//

import Foundation

class VideoWidgetResource: BaseWidgetResource {
    let title: String?
    let text: String?
    let videoURL: URL
    let linkURL: URL?
    let linkLabel: String?
    let interactionURL: URL
    let sponsors: [Sponsor]

    enum CodingKeys: CodingKey {
        case title
        case text
        case videoUrl
        case linkUrl
        case linkLabel
        case interactionUrl
        case sponsors
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.title = try? container.decode(String.self, forKey: .title)
        self.text = try? container.decode(String.self, forKey: .text)
        self.videoURL = try container.decode(URL.self, forKey: .videoUrl)
        self.linkURL = try? container.decode(URL.self, forKey: .linkUrl)
        self.linkLabel = try? container.decode(String.self, forKey: .linkLabel)
        self.interactionURL = try container.decode(URL.self, forKey: .interactionUrl)
        self.sponsors = try container.decode([Sponsor].self, forKey: .sponsors)
        try super.init(from: decoder)
    }
}
