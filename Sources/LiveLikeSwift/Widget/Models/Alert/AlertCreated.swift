//
//  AlertCreated.swift
//  LiveLikeSDK
//
//  Created by Heinrich Dahms on 2019-03-20.
//

import Foundation

class AlertCreated: BaseWidgetResource {

    let url: URL
    let linkLabel: String?
    let linkURL: URL?
    let text: String?
    let title: String?
    let imageURL: URL?
    var rewardsURL: URL?
    let sponsors: [Sponsor]

    enum CodingKeys: CodingKey {
        case url
        case linkLabel
        case linkUrl
        case text
        case title
        case imageUrl
        case rewardsUrl
        case sponsors
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.url = try container.decode(URL.self, forKey: .url)
        self.linkLabel = try container.decodeIfPresent(String.self, forKey: .linkLabel)
        self.linkURL = try container.decodeIfPresent(URL.self, forKey: .linkUrl)
        self.text = try container.decodeIfPresent(String.self, forKey: .text)
        self.title = try container.decodeIfPresent(String.self, forKey: .title)
        self.imageURL = try container.decodeIfPresent(URL.self, forKey: .imageUrl)
        self.rewardsURL = try container.decodeIfPresent(URL.self, forKey: .rewardsUrl)
        self.sponsors = try container.decode([Sponsor].self, forKey: .sponsors)
        try super.init(from: decoder)
    }

    init(baseData: BaseWidgetResource, url: URL, linkLabel: String?, linkURL: URL?, text: String?, title: String?, imageURL: URL?, rewardsURL: URL? = nil, sponsors: [Sponsor]) {
        self.url = url
        self.linkLabel = linkLabel
        self.linkURL = linkURL
        self.text = text
        self.title = title
        self.imageURL = imageURL
        self.rewardsURL = rewardsURL
        self.sponsors = sponsors
        super.init(baseData: baseData)
    }
}
