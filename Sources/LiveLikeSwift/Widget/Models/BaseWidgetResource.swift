//
//  BaseWidgetResource.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 8/2/21.
//

import Foundation

class BaseWidgetResource: Codable {

    private enum Errors: LocalizedError {
        case pubnubNotEnabled
    }

    let id: String
    let programID: String
    let clientID: String
    let publishedAt: Date?
    let createdAt: Date
    let timeout: TimeInterval
    let kind: WidgetKind
    private let subscribeChannel: String
    let programDateTime: Date?
    let impressionURL: URL
    let customData: String?
    let interactiveUntil: Date?
    let widgetAttributes: [Attribute]
    let playbackTimeMilliseconds: Int?
    private let isPubnubEnabled: Bool
    let deleteURL: URL
    let scheduleURL: URL

    enum CodingKeys: String, CodingKey {
        case id
        case programId
        case clientId
        case publishedAt
        case createdAt
        case timeout
        case kind
        case subscribeChannel
        case programDateTime
        case impressionUrl
        case customData
        case interactiveUntil
        case widgetAttributes
        case pubnubEnabled
        case playbackTimeMs
        case deleteURL = "url"
        case scheduleUrl
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.programID = try container.decode(String.self, forKey: .programId)
        self.clientID = try container.decode(String.self, forKey: .clientId)
        self.publishedAt = try? container.decode(Date.self, forKey: .publishedAt)
        self.createdAt = try container.decode(Date.self, forKey: .createdAt)
        let iso8601Duration = try container.decode(String.self, forKey: .timeout)
        self.timeout = iso8601Duration.timeIntervalFromISO8601Duration() ?? 7
        self.kind = try container.decode(WidgetKind.self, forKey: .kind)
        self.subscribeChannel = try container.decode(String.self, forKey: .subscribeChannel)
        self.programDateTime = try? container.decode(Date.self, forKey: .programDateTime)
        self.impressionURL = try container.decode(URL.self, forKey: .impressionUrl)
        self.customData = try? container.decode(String.self, forKey: .customData)
        self.interactiveUntil = try container.decodeIfPresent(Date.self, forKey: .interactiveUntil)
        self.widgetAttributes = try container.decode([Attribute].self, forKey: .widgetAttributes)
        self.isPubnubEnabled = try container.decodeIfPresent(Bool.self, forKey: .pubnubEnabled) ?? true
        self.playbackTimeMilliseconds = try container.decodeIfPresent(Int.self, forKey: .playbackTimeMs)
        self.deleteURL = try container.decode(URL.self, forKey: .deleteURL)
        self.scheduleURL = try container.decode(URL.self, forKey: .scheduleUrl)

    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(programID, forKey: .programId)
        try container.encode(clientID, forKey: .clientId)
        try container.encode(publishedAt, forKey: .publishedAt)
        try container.encode(createdAt, forKey: .createdAt)
        try container.encode(timeout.description, forKey: .timeout)
        try container.encode(kind, forKey: .kind)
        try container.encode(subscribeChannel, forKey: .subscribeChannel)
        try container.encode(programDateTime, forKey: .programDateTime)
        try container.encode(impressionURL, forKey: .impressionUrl)
        try container.encode(customData, forKey: .customData)
        try container.encode(widgetAttributes, forKey: .widgetAttributes)
        try container.encode(isPubnubEnabled, forKey: .pubnubEnabled)
        try container.encode(playbackTimeMilliseconds, forKey: .playbackTimeMs)
        try container.encode(deleteURL, forKey: .deleteURL)
        try container.encode(deleteURL, forKey: .scheduleUrl)
    }

    internal init(
        id: String,
        programID: String,
        clientID: String,
        publishedAt: Date?,
        createdAt: Date,
        timeout: TimeInterval,
        kind: WidgetKind,
        subscribeChannel: String,
        programDateTime: Date?,
        impressionURL: URL,
        customData: String?,
        interactiveUntil: Date?,
        attributes: [Attribute],
        isPubnubEnabled: Bool,
        playbackTime: Int?,
        deleteURL: URL,
        scheduleURL: URL
    ) {
        self.id = id
        self.programID = programID
        self.clientID = clientID
        self.publishedAt = publishedAt
        self.createdAt = createdAt
        self.timeout = timeout
        self.kind = kind
        self.subscribeChannel = subscribeChannel
        self.programDateTime = programDateTime
        self.impressionURL = impressionURL
        self.customData = customData
        self.interactiveUntil = interactiveUntil
        self.widgetAttributes = attributes
        self.isPubnubEnabled = isPubnubEnabled
        self.playbackTimeMilliseconds = playbackTime
        self.deleteURL = deleteURL
        self.scheduleURL = scheduleURL
    }

    init(baseData: BaseWidgetResource) {
        self.id = baseData.id
        self.programID = baseData.programID
        self.clientID = baseData.clientID
        self.publishedAt = baseData.publishedAt
        self.createdAt = baseData.createdAt
        self.timeout = baseData.timeout
        self.kind = baseData.kind
        self.subscribeChannel = baseData.subscribeChannel
        self.programDateTime = baseData.programDateTime
        self.impressionURL = baseData.impressionURL
        self.customData = baseData.customData
        self.interactiveUntil = baseData.interactiveUntil
        self.widgetAttributes = baseData.widgetAttributes
        self.isPubnubEnabled = baseData.isPubnubEnabled
        self.playbackTimeMilliseconds = baseData.playbackTimeMilliseconds
        self.deleteURL = baseData.deleteURL
        self.scheduleURL = baseData.scheduleURL
    }

    /// Returns the channel to subscribe to get widget result updates
    func getResultsChannel() throws -> String {
        if isPubnubEnabled {
            return subscribeChannel
        } else {
            throw Errors.pubnubNotEnabled
        }
    }
}

/// A resource representing an Attribute
public struct Attribute: Equatable {
    /// A unique key associated with an Attribute
    public let key: String
    /// The value of the Attribute
    public let value: String

    public init(key: String, value: String) {
        self.key = key
        self.value = value
    }

    public static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.key == rhs.key && lhs.value == rhs.value
    }
}

extension Attribute: Codable { }

extension Attribute {
    /// Builds a URLQueryItem as expected by LiveLike API
    func queryItem() -> URLQueryItem {
        return URLQueryItem(
            name: "attributes",
            value: "\(key):\(value)"
        )
    }
}
