//
//  TextAskWidgetModel.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 7/30/21.
//

import Foundation
import LiveLikeCore

/// A model containing the data and methods for a Text Ask Widget
public class TextAskWidgetModel: BaseWidgetModel, TextAskWidgetModelable {
    
    // MARK: - Data
    
    /// The title of the ask widget
    public let title: String
    /// The prompt of the ask widget
    public let prompt: String
    /// A confirmation message to be displyed after the user has submitted a response
    public let confirmationMessage: String
    public var userReplies: [Reply] {
        return widgetInteractionRepo.get(widgetID: id)?.textAskReplies ?? []
    }
    public var mostRecentReply: Reply? {
        return userReplies.max(by: { $0.createdAt <= $1.createdAt })
    }
    
    // MARK: - Internal Properties
    
    let widgetInteractionRepo: WidgetInteractionRepository
    let interactionURL: URL?
    let replyURL: URL

    init(
        resource: TextAskWidgetResource,
        eventRecorder: EventRecorder,
        widgetAPI: LiveLikeWidgetAPIProtocol,
        userProfile: UserProfileProtocol,
        widgetInteractionRepo: WidgetInteractionRepository
    ) {
        self.title = resource.title
        self.prompt = resource.prompt
        self.confirmationMessage = resource.confirmationMessage
        self.replyURL = resource.replyURL
        self.widgetInteractionRepo = widgetInteractionRepo
        self.interactionURL = try? WidgetResource.getWidgetInteractionsURL(
            fromTemplate: resource.widgetInteractionsURLTemplate,
            profileID: userProfile.userID.asString
        )
        super.init(
            baseData: resource,
            eventRecorder: eventRecorder,
            livelikeAPI: widgetAPI,
            userProfile: userProfile
        )
    }
    
    // MARK: - Public Methods
    
    /// Call this to load the user's interaction history for this Widget
    /// - Parameter completion: Returns the loaded votes
    public func loadInteractionHistory(completion: @escaping (Result<[Reply], Error>) -> Void = { _ in }) {
        self.widgetInteractionRepo.get(widgetModel: .textAsk(self)) { result in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let interactions):
                completion(.success(interactions.textAskReplies))
            }
        }
    }
    
    public func submitReply(
        _ reply: String,
        completion: @escaping (Result<Reply, Error>) -> Void
    ) {
        self.eventRecorder.record(
            .widgetEngaged(widgetModel: .textAsk(self))
        )
        firstly {
            self.livelikeAPI.createTextAskReply(
                url: replyURL,
                reply: reply,
                accessToken: userProfile.accessToken
            )
        }.then { reply in
            self.widgetInteractionRepo.add(interactions: [.textAskReply(reply)])
            completion(.success(reply))
        }.catch { error in
            log.error(error)
            completion(.failure(error))
        }
    }
    
    // MARK: - Models
    
    public struct Reply: Decodable {
        internal init(id: String, widgetID: String, widgetKind: WidgetKind, createdAt: Date, text: String) {
            self.id = id
            self.widgetID = widgetID
            self.widgetKind = widgetKind
            self.createdAt = createdAt
            self.text = text
        }
        
        // The text of the user's reply
        public let text: String
        
        public let id: String
        public let widgetID: String
        public let widgetKind: WidgetKind
        public let createdAt: Date
        
        public init(from decoder: Decoder) throws {
            enum CodingKeys: CodingKey {
                case id
                case widgetId
                case widgetKind
                case createdAt
                case text
            }
            
            let container = try decoder.container(keyedBy: CodingKeys.self)
            id = try container.decode(String.self, forKey: .id)
            widgetID = try container.decode(String.self, forKey: .widgetId)
            widgetKind = try container.decode(WidgetKind.self, forKey: .widgetKind)
            createdAt = try container.decode(Date.self, forKey: .createdAt)
            text = try container.decode(String.self, forKey: .text)
        }
    }
}
