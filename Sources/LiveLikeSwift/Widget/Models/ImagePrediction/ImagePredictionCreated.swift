//
//  File.swift
//  LiveLikeSDK
//
//  Created by jelzon on 2/14/19.
//

import Foundation

struct ImagePredictionOption: Decodable {
    let voteCount: Int
    let voteUrl: URL
    let imageUrl: URL
    let description: String
    let id: String
}

class ImagePredictionCreated: BaseWidgetResource {
    let url: URL
    let question: String
    let options: [ImagePredictionOption]
    let confirmationMessage: String
    let rewardsUrl: URL?
    let widgetInteractionsUrlTemplate: String
    let followUps: [ImagePredictionFollowUp]
    let sponsors: [Sponsor]
    let earnableRewards: [EarnableReward]

    enum CodingKeys: String, CodingKey {
        case confirmationMessage
        case followUpUrl
        case id
        case kind
        case options
        case programId
        case question
        case subscribeChannel
        case timeout
        case url
        case programDateTime
        case impressionUrl
        case rewardsUrl
        case customData
        case createdAt
        case publishedAt
        case widgetInteractionsUrlTemplate
        case followUps
        case sponsors
        case earnableRewards
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        confirmationMessage = try container.decode(String.self, forKey: .confirmationMessage)
        options = try container.decode([ImagePredictionOption].self, forKey: .options)
        question = try container.decode(String.self, forKey: .question)
        url = try container.decode(URL.self, forKey: .url)
        rewardsUrl = try? container.decode(URL.self, forKey: .rewardsUrl)
        self.widgetInteractionsUrlTemplate = try container.decode(String.self, forKey: .widgetInteractionsUrlTemplate)
        self.followUps = try container.decode([ImagePredictionFollowUp].self, forKey: .followUps)
        self.sponsors = try container.decode([Sponsor].self, forKey: .sponsors)
        self.earnableRewards = try container.decodeIfPresent([EarnableReward].self, forKey: .earnableRewards) ?? []
        try super.init(from: decoder)
    }
}
