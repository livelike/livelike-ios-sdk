//
//  ImagePredictionFollowUp.swift
//  LiveLikeSDK
//
//  Created by jelzon on 2/22/19.
//

import Foundation

struct ImagePredictionFollowUpOption: Decodable {
    let voteCount: Int
    let voteUrl: URL
    let imageUrl: URL
    let description: String
    let id: String
    let isCorrect: Bool
    let url: URL
}

class ImagePredictionFollowUp: BaseWidgetResource {
    internal init(baseData: BaseWidgetResource, options: [ImagePredictionFollowUpOption], question: String, imagePredictionUrl: URL, url: URL, rewardsUrl: URL? = nil, claimUrl: URL, imagePredictionId: String, interactionURL: URL, status: WidgetStatus, sponsors: [Sponsor]) {
        self.options = options
        self.question = question
        self.imagePredictionUrl = imagePredictionUrl
        self.url = url
        self.rewardsUrl = rewardsUrl
        self.claimUrl = claimUrl
        self.imagePredictionId = imagePredictionId
        self.interactionURL = interactionURL
        self.status = status
        self.sponsors = sponsors
        super.init(baseData: baseData)
    }

    let options: [ImagePredictionFollowUpOption]
    let question: String
    let imagePredictionUrl: URL
    let url: URL
    var rewardsUrl: URL?
    let claimUrl: URL
    let imagePredictionId: String
    let interactionURL: URL
    let status: WidgetStatus
    let sponsors: [Sponsor]

    enum CodingKeys: String, CodingKey {
        case id
        case createdAt
        case publishedAt
        case kind
        case options
        case programId
        case question
        case subscribeChannel
        case imagePredictionUrl
        case timeout
        case impressionUrl
        case rewardsUrl
        case url
        case programDateTime
        case customData
        case claimUrl
        case imagePredictionId
        case interactionUrl
        case status
        case sponsors
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        options = try container.decode([ImagePredictionFollowUpOption].self, forKey: .options)
        question = try container.decode(String.self, forKey: .question)
        imagePredictionUrl = try container.decode(URL.self, forKey: .imagePredictionUrl)
        url = try container.decode(URL.self, forKey: .url)
        rewardsUrl = try? container.decode(URL.self, forKey: .rewardsUrl)
        claimUrl = try container.decode(URL.self, forKey: .claimUrl)
        imagePredictionId = try container.decode(String.self, forKey: .imagePredictionId)
        self.interactionURL = try container.decode(URL.self, forKey: .interactionUrl)
        self.status = try container.decode(WidgetStatus.self, forKey: .status)
        self.sponsors = try container.decode([Sponsor].self, forKey: .sponsors)
        try super.init(from: decoder)
    }
}

extension ImagePredictionFollowUp {
    var correctOptionsIds: [String] {
        return options
            .filter({$0.isCorrect})
            .map({ $0.id })
    }
}
