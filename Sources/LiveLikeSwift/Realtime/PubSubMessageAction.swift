//
//  PubSubMessageAction.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 1/2/20.
//

import Foundation

struct PubSubMessageAction {
    var messageID: String
    var id: String
    var sender: String
    var type: String
    var value: String
    var timetoken: UInt64
    var messageTimetoken: UInt64
}
