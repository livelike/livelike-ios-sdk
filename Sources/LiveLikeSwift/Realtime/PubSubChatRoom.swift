//
//  PubSubChatRoom.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 11/19/19.
//

import Foundation
import LiveLikeCore

class PubSubChatRoom: ChatSession {
    public var messages: [ChatMessage] = []
    public let userID: String

    let isAvatarDisplayed: Bool
    let isURLEnabled: Bool
    var chatMessageUrlPatterns: String?
    var stickerRepository: StickerRepository
    var isReportingEnabled: Bool {
        return messageReporter != nil
    }
    var avatarURL: URL?

    var containsFilteredMessages: Bool {
        return includeFilteredMessages
    }

    var chatClient: ChatClient
    var userClient: UserClient
    var reactionSession: ReactionSession?
    var reactionClient: ReactionClient
    let eventRecorder: EventRecorder
    let reactionsVendor: ReactionVendor

    private var chatChannel: PubSubChannel?
    private var controlChannel: PubSubChannel?
    private var userNickname: String
    private let nicknameVendor: UserNicknameVendor
    private let _roomID: String
    private let _title: String?
    private var accessToken: AccessToken
    private let chatAPI: LiveLikeChatAPIProtocol
    private let customMessagesUrl: URL
    private let pinnedMessagesURL: URL
    private let chatroomMessageURL: URL
    private let chatroomMessagesCountURL: URL
    private let deleteMessageURL: URL
    private var previousHistoryPageURL: URL?
    // where deleted message id's are stored
    private var deletedMessageIDs: Set<ChatMessageID> = Set()
    private var imageUploader: ImageUploader
    private let messageReporter: MessageReporter?
    private let chatFilters: Set<ChatFilter>
    private let mediaRepository: MediaRepository = MediaRepository.shared
    private var delegates: Listener<ChatSessionDelegate> = Listener()
    private var isLoadingHistory: Bool = false
    private var stickerPacks: [StickerPack] = []
    private var includeFilteredMessages: Bool
    private let networking: LLNetworking

    private var chatRoomEventsURL: URL
    private var pollingInterval: Int
    @objc private var pollingURL: URL?
    private var timer: DispatchSourceTimer?

    private let decoder: LLJSONDecoder = LLJSONDecoder()

    private let encoder: LLJSONEncoder = {
        let encoder = LLJSONEncoder()
        return encoder
    }()

    private struct ChatRoomPaginationProgress {
        var next: URL?
        var previous: URL?
        var total: Int = 0
    }

    private var chatRoomPagination: ChatRoomPaginationProgress
    private var chatRoomPageURLRepo = [String: PaginatedResultURL]()

    init(
        roomID: String,
        chatChannel: PubSubChannel?,
        controlChannel: PubSubChannel?,
        userID: String,
        userNickname: String,
        nicknameVendor: UserNicknameVendor,
        imageUploader: ImageUploader,
        analytics: EventRecorder,
        reactionsVendor: ReactionVendor,
        messageReporter: MessageReporter?,
        title: String?,
        accessToken: AccessToken,
        chatFilters: Set<ChatFilter>,
        stickerRepository: StickerRepository,
        shouldDisplayAvatar: Bool,
        enableChatMessageURLs: Bool,
        chatMessageUrlPatterns: String?,
        chatAPI: LiveLikeChatAPIProtocol,
        customMessagesUrl: URL,
        pinnedMessagesURL: URL,
        chatroomMessageURL: URL,
        chatroomMessagesCountURL: URL,
        deleteMessageURL: URL,
        chatClient: ChatClient,
        userClient: UserClient,
        reactionClient: ReactionClient,
        includeFilteredMessages: Bool,
        chatRoomEventsURL: URL,
        apiPollingInterval: Int,
        reactionSpaceID: String?,
        networking: LLNetworking,
        previousHistoryPageURL: URL? = nil
    ) {
        self._roomID = roomID
        self.chatChannel = chatChannel
        self.controlChannel = controlChannel
        self.userID = userID
        self.userNickname = userNickname
        self.nicknameVendor = nicknameVendor
        self.imageUploader = imageUploader
        self.eventRecorder = analytics
        self.reactionsVendor = reactionsVendor
        self.messageReporter = messageReporter
        self._title = title
        self.accessToken = accessToken
        self.chatFilters = chatFilters
        self.stickerRepository = stickerRepository
        self.isAvatarDisplayed = shouldDisplayAvatar
        self.isURLEnabled = enableChatMessageURLs
        self.chatMessageUrlPatterns = chatMessageUrlPatterns
        self.chatAPI = chatAPI
        self.customMessagesUrl = customMessagesUrl
        self.pinnedMessagesURL = pinnedMessagesURL
        self.chatroomMessageURL = chatroomMessageURL
        self.chatroomMessagesCountURL = chatroomMessagesCountURL
        self.deleteMessageURL = deleteMessageURL
        self.chatRoomPagination = ChatRoomPaginationProgress()
        self.chatClient = chatClient
        self.userClient = userClient
        self.reactionClient = reactionClient
        self.includeFilteredMessages = includeFilteredMessages
        self.chatRoomEventsURL = chatRoomEventsURL
        self.pollingInterval = apiPollingInterval
        self.previousHistoryPageURL = previousHistoryPageURL
        self.networking = networking
        self.pollingURL = getChatRoomEventsURL(eventsURL: chatRoomEventsURL, options: GetChatRoomEventRequestOptions(roomID: roomID))
        if chatChannel == nil, controlChannel == nil {
            log.info("High Latency Chat has been enabled")
            self.timer = DispatchSource.makeTimerSource(queue: .global())
            self.timer?.schedule(
                deadline: .now(),
                repeating: .seconds(self.pollingInterval)
            )
            self.timer?.setEventHandler { [weak self] in
                guard let self = self else { return }
                guard let pollingURL = self.pollingURL else {
                    log.error("Polling Next URL Missing")
                    return
                }
                firstly {
                    self.chatAPI.getMessageEvents(chatroomEventsURL: pollingURL)
                }.then { messageEventsList in
                    guard let nextURL = messageEventsList.next else {
                        log.error("Polling Next URL Missing")
                        return
                    }
                    self.pollingURL = nextURL
                    self.processMessageEventsFromBackend(messageEvents: messageEventsList.items)
                }
            }
            self.timer?.resume()
        }

        self.chatChannel?.delegate = self
        self.controlChannel?.delegate = self
        self.nicknameVendor.nicknameDidChange.append({ [weak self] newNickname in
            guard let self = self else { return }
            self.userNickname = newNickname
        })

        stickerRepository.getStickerPacks { [weak self] result in
            guard let self = self else { return}

            DispatchQueue.main.async {
                switch result {
                case .success(let stickerPacks):
                    self.stickerPacks = stickerPacks
                case .failure(let error):
                    log.error("Failed to get sticker packs with error: \(error)")
                }
            }
        }

        if let reactionSpaceID = reactionSpaceID {
            self.getReactionSpaceInfo(reactionSpaceID: reactionSpaceID)
        } else {
            log.info("Chat room is missing reactionSpaceID")
        }
    }
    
    func getReactionSpaceInfo(reactionSpaceID: String) {
        reactionClient.getReactionSpaceInfo(reactionSpaceID: reactionSpaceID) { [weak self] result in
            guard let self = self else { return}

            switch result {
            case .success(let reactionSpace):
                self.reactionSession = self.reactionClient.createReactionSession(reactionSpace: reactionSpace)
                self.reactionSession?.sessionDelegate = self
            case .failure(let error):
                log.error("Failed to get Reaction Space with error: \(error)")
            }
        }
    }

    func getReactions(completion: @escaping (Result<[Reaction], Error>) -> Void) {
        self.reactionsVendor.getReactions().then {
            completion(.success($0))
        }.catch {
            completion(.failure($0))
        }
    }

    func getStickerPacks(
        completion: @escaping (Result<[StickerPack], Error>) -> Void
    ) {
        self.stickerRepository.getStickerPacks(completion: completion)
    }

    deinit {
        if let timer = self.timer {
            timer.cancel()
            log.info("High Latency Chat has been disabled")
        }
        log.info("Chat Session for room \(roomID) has ended.")
    }
}

// MARK: - Private methods

private extension PubSubChatRoom {
    private func createMockMessage(
        newChatMessage: NewChatMessage,
        messageID: String,
        quoteMessage: ChatMessagePayload?
    ) -> ChatMessage {

        var quoteMessagePayload = quoteMessage
        if 
            let quoteMessage = quoteMessage,
            let selectedMessage = messages.first(where: {
                $0.id.id == quoteMessage.id
            })
        {
            quoteMessagePayload = ChatMessagePayload(chatMessage: selectedMessage)
        }

        if let imageURL = newChatMessage.imageURL,
           let imageHeight = newChatMessage.imageHeight,
           let imageWidth = newChatMessage.imageWidth
        {
            return ChatMessage(
                id: nil,
                clientMessageID: messageID,
                roomID: self.roomID,
                message: nil,
                senderID: self.userID,
                senderNickname: self.userNickname,
                videoTimestamp: newChatMessage.timeStamp,
                reactions: ReactionVotes(allVotes: []),
                timestamp: Date(),
                profileImageUrl: self.avatarURL,
                createdAt: TimeToken(pubnubTimetoken: 0),
                bodyImageUrl: imageURL,
                bodyImageHeight: imageHeight,
                bodyImageWidth: imageWidth,
                filteredMessage: nil,
                filteredReasons: Set(),
                customData: nil,
                quoteMessage: quoteMessagePayload,
                messageEvent: .imageCreated,
                pubnubTimetoken: TimeToken(pubnubTimetoken: 0),
                messageMetadata: newChatMessage.messageMetadata,
                parentMessageID: newChatMessage.parentMessageID,
                repliesCount: nil,
                repliesURL: nil
            )
        } else if let customData = newChatMessage.customData {
            return ChatMessage(
                id: nil,
                clientMessageID: messageID,
                roomID: self.roomID,
                message: nil,
                senderID: self.userID,
                senderNickname: self.userNickname,
                videoTimestamp: newChatMessage.timeStamp,
                reactions: ReactionVotes(allVotes: []),
                timestamp: Date(),
                profileImageUrl: self.avatarURL,
                createdAt: TimeToken(pubnubTimetoken: 0),
                bodyImageUrl: nil,
                bodyImageHeight: nil,
                bodyImageWidth: nil,
                filteredMessage: nil,
                filteredReasons: Set(),
                customData: customData,
                quoteMessage: quoteMessagePayload,
                messageEvent: .customMessageCreated,
                pubnubTimetoken: TimeToken(pubnubTimetoken: 0),
                messageMetadata: newChatMessage.messageMetadata,
                parentMessageID: newChatMessage.parentMessageID,
                repliesCount: nil,
                repliesURL: nil
            )
        } else {
            return ChatMessage(
                id: nil,
                clientMessageID: messageID,
                roomID: self.roomID,
                message: newChatMessage.text,
                senderID: self.userID,
                senderNickname: self.userNickname,
                videoTimestamp: newChatMessage.timeStamp,
                reactions: ReactionVotes(allVotes: []),
                timestamp: Date(),
                profileImageUrl: self.avatarURL,
                createdAt: TimeToken(pubnubTimetoken: 0),
                bodyImageUrl: nil,
                bodyImageHeight: nil,
                bodyImageWidth: nil,
                filteredMessage: nil,
                filteredReasons: Set(),
                customData: nil,
                quoteMessage: quoteMessagePayload,
                messageEvent: .messageCreated,
                pubnubTimetoken: TimeToken(pubnubTimetoken: 0),
                messageMetadata: newChatMessage.messageMetadata,
                parentMessageID: newChatMessage.parentMessageID,
                repliesCount: nil,
                repliesURL: nil
            )
        }
    }

    private func sendMessage(
        messageID: String,
        mockMessage: ChatMessage,
        completion: @escaping (Result<ChatMessage, Error>) -> Void
    ) {

        let pdt: Date? = {
            if let videoTimestamp = mockMessage.videoTimestamp {
                return Date.init(timeIntervalSince1970: videoTimestamp)
            }
            return nil
        }()

        var mockChatMessage: ChatMessage = mockMessage

        // Build and Send a real message payload
        if let imageURL = mockMessage.imageURL,
           let imageHeight = mockMessage.imageHeight,
           let imageWidth = mockMessage.imageWidth
        {
            self.mediaRepository.getImage(url: imageURL) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .failure(let error):
                    completion(.failure(error))
                case .success(let success):
                    // upload image
                    self.imageUploader.upload(success.imageData) { [weak self] result in
                        switch result {
                        case .failure(let error):
                            completion(.failure(error))
                        case .success(let imageResource):
                            guard let self = self else { return }
                            let payload: PubSubChatPayload = PubSubChatPayload(
                                id: nil,
                                clientMessageId: messageID,
                                message: nil,
                                imageUrl: imageResource.imageUrl,
                                imageHeight: imageHeight,
                                imageWidth: imageWidth,
                                senderId: self.userID,
                                senderNickname: mockMessage.senderNickname,
                                senderImageUrl: self.avatarURL,
                                programDateTime: pdt,
                                filteredMessage: nil,
                                contentFilter: nil,
                                messageEvent: .imageCreated,
                                quoteMessage: mockMessage.quoteMessage,
                                chatRoomId: self.roomID,
                                messageMetadata: mockMessage.messageMetadata?.asMessageMetadataCodable(),
                                parentMessageId: mockMessage.parentMessageID,
                                repliesCount: nil,
                                repliesUrl: nil,
                                customData: nil
                            )

                            mockChatMessage.imageURL = imageResource.imageUrl
                            self.messages.append(mockChatMessage)

                            self.sendMessageToBackend(payloadMessage: payload, mockMessage: mockChatMessage) { result in
                                switch result {
                                case .success(let message):
                                    guard let index = self.messages.firstIndex(where: { $0.id.clientMessageID == message.id.clientMessageID }) else { return }
                                    self.messages[index] = message
                                    self.delegates.publish { $0.chatSession(self, didRecieveNewMessage: message) }
                                    completion(.success(message))
                                case .failure(let error):
                                    completion(.failure(error))
                                }
                            }
                        }
                    }
                }
            }

        } else if let message = mockMessage.text {
            let payload = PubSubChatPayload(
                id: nil,
                clientMessageId: messageID,
                message: message,
                imageUrl: nil,
                imageHeight: nil,
                imageWidth: nil,
                senderId: userID,
                senderNickname: mockMessage.senderNickname,
                senderImageUrl: avatarURL,
                programDateTime: pdt,
                filteredMessage: nil,
                contentFilter: nil,
                messageEvent: .messageCreated,
                quoteMessage: mockMessage.quoteMessage,
                chatRoomId: self.roomID,
                messageMetadata: mockMessage.messageMetadata?.asMessageMetadataCodable(),
                parentMessageId: mockMessage.parentMessageID,
                repliesCount: nil,
                repliesUrl: nil,
                customData: nil
            )

            self.messages.append(mockChatMessage)

            self.sendMessageToBackend(payloadMessage: payload, mockMessage: mockChatMessage) { result in
                switch result {
                case .success(let message):
                    guard let index = self.messages.firstIndex(where: { $0.id.clientMessageID == message.id.clientMessageID }) else { return }
                    self.messages[index] = message
                    self.delegates.publish { $0.chatSession(self, didRecieveNewMessage: message) }
                    completion(.success(message))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        } else if let customData = mockMessage.customData {
            self.messages.append(mockChatMessage)

            let _ = self.sendCustomMessage(customData) { result in
                switch result {
                case .failure(let error):
                    completion(.failure(error))
                case .success(let message):
                    completion(.success(message))
                }
            }
        }
    }

    private func sendMessageToBackend<T: Encodable>(
        payloadMessage: T,
        mockMessage: ChatMessage,
        completion: @escaping (Result<ChatMessage, Error>) -> Void
    ) {
        let chatMessage: ChatMessage = mockMessage
        firstly {
            self.chatAPI.sendMessage(chatroomMessagesURL: self.chatroomMessageURL, accessToken: self.accessToken, messagePayload: payloadMessage)
        }.then { message in
            completion(.success(message))
        }.catch { error in
            completion(.failure(error))
        }

        // Report Analytics
        guard let messageText = chatMessage.text else { return }
        guard let clientMessageID = chatMessage.id.clientMessageID else { return }

        let stickerShortcodes = messageText.getStickerShortcodes(stickerPacks: self.stickerPacks)
        let indices = ChatSentMessageProperties.calculateStickerIndices(stickerIDs: stickerShortcodes, stickers: self.stickerPacks)
        let sentProperties = ChatSentMessageProperties(
            characterCount: messageText.count,
            messageId: clientMessageID,
            chatRoomId: _roomID,
            stickerShortcodes: stickerShortcodes.map({ ":\($0):" }),
            stickerCount: stickerShortcodes.count,
            stickerIndices: indices,
            hasExternalImage: chatMessage.imageURL != nil
        )
        eventRecorder.record(.chatMessageSent(properties: sentProperties))
    }

    private func chatMessageIsMine(chatMessage: ChatMessage) -> Bool {
        return chatMessage.senderID == self.userID
    }

    /// Processes the chatChannel messages from history
    /// - Filters out deleted messages
    private func processMessagesFromChatroomHistory(messagesResult: GetMessagesResult) -> [ChatMessage] {
        let unfilteredMessages: [ChatMessage] = messagesResult.results.compactMap { message in
            do {
                let chatMessage = ChatMessage(chatMessageResponse: message)

                switch chatMessage.messageEvent {
                case .messageCreated:

                    // Only shadow muted messages from the current user should show
                    if chatMessage.isMessageShadowMuted, !chatMessageIsMine(chatMessage: chatMessage) {
                        return nil
                    }

                    if self.includeFilteredMessages == false,
                       chatMessage.isMessageContentFiltered == true,
                       chatMessageIsMine(chatMessage: chatMessage) == false
                    {

                        /*
                         * Exclude content filtered message that belongs to other users
                         * Do not filter out current user's message regardless of it's filter status
                         */

                        return nil
                    }
                    return chatMessage
                case .messageDeleted:
                    deletedMessageIDs.insert(chatMessage.id)
                    return nil
                case .imageCreated:
                    return chatMessage
                case .imageDeleted:
                    deletedMessageIDs.insert(chatMessage.id)
                    return nil
                case .customMessageCreated:
                    return chatMessage
                case .chatroomUpdated:
                    return nil
                case .profileBlocked:
                    return nil
                case .profileUnblocked:
                    return nil
                case .messagePinned:
                    return nil
                case .messageUnpinned:
                    return nil
                case .none:
                    return nil
                case .some(.addedToChatroom):
                    return nil
                case .some(.invitedToChatroom):
                    return nil
                case .userReactionAdded:
                    return nil
                case .userReactionRemoved:
                    return nil
                case .reactionSpaceUpdated:
                    return nil
                }
            }
        }

        let messagesToBeShown = unfilteredMessages.filter { !deletedMessageIDs.contains($0.id) }
        return messagesToBeShown
    }

    private func processMessageEventsFromBackend(messageEvents: [ChatRoomEvent]) {
        for event in messageEvents {
            switch event.payload.messageEvent {
            case .messageCreated:
                let chatMessage = ChatMessage(
                    from: event.payload,
                    chatRoomID: self.roomID,
                    timetoken: TimeToken(date: event.payload.createdAt),
                    userID: userID
                )

                guard !chatMessageIsMine(chatMessage: chatMessage) else { break }

                // Only shadow muted messages from the current user should show
                if chatMessage.isMessageShadowMuted, !chatMessageIsMine(chatMessage: chatMessage) {
                    break
                }

                if chatMessage.isMessageFiltered == true, containsFilteredMessages == false {
                    /*
                     * Stop processing a recievedfiltered  message
                     * if the integrator chose not to show filtered messages
                     */
                    break
                }

                if
                    let pubnubID = chatMessage.id.id,
                    !self.messages.contains(where: { $0.id.id == pubnubID })
                {
                    self.messages.append(chatMessage)
                    self.delegates.publish { $0.chatSession(self, didRecieveNewMessage: chatMessage)}
                }
            case .messageDeleted:
                guard let chatMessage = messages.first(where: { $0.id.id == event.payload.id }) else { break }
                self.delegates.publish { $0.chatSession(self, didDeleteMessage: chatMessage.id)}
                deletedMessageIDs.insert(chatMessage.id)
            case .imageCreated:
                let chatMessage = ChatMessage(
                    from: event.payload,
                    chatRoomID: self.roomID,
                    timetoken: TimeToken(date: event.payload.createdAt),
                    userID: userID
                )

                guard !chatMessageIsMine(chatMessage: chatMessage) else { break }

                if
                    let pubnubID = chatMessage.id.id,
                    !self.messages.contains(where: { $0.id.id == pubnubID })
                {
                    self.messages.append(chatMessage)
                    self.delegates.publish { $0.chatSession(self, didRecieveNewMessage: chatMessage)}
                }
            case .imageDeleted:
                guard let chatMessageID = messages.first(where: { $0.id.id == event.payload.id })?.id else { break }
                self.delegates.publish { $0.chatSession(self, didDeleteMessage: chatMessageID)}
                deletedMessageIDs.insert(chatMessageID)
            case .customMessageCreated:
                let chatMessage = ChatMessage(
                    from: event.payload,
                    chatRoomID: self.roomID,
                    timetoken: TimeToken(date: event.payload.createdAt),
                    userID: userID
                )

                guard !chatMessageIsMine(chatMessage: chatMessage) else { return }

                if
                    let pubnubID = chatMessage.id.id,
                    !self.messages.contains(where: { $0.id.id == pubnubID })
                {
                    self.messages.append(chatMessage)
                    self.delegates.publish { $0.chatSession(self, didRecieveNewMessage: chatMessage)}
                }
            case .chatroomUpdated:
                break
            case .profileBlocked:
                break
            case .profileUnblocked:
                break
            case .reactionSpaceUpdated:
                break
            case .userReactionAdded:
                break
            case .userReactionRemoved:
                break
            case .messagePinned:
                break
            case .messageUnpinned:
                break
            case .addedToChatroom:
                break
            case .invitedToChatroom:
                break
            case .none:
                break
            }
        }
    }
}

// MARK: - PubSubChannelDelegate

extension PubSubChatRoom: PubSubChannelDelegate {

    func channel(_ channel: PubSubChannel, messageCreated message: PubSubChannelMessage) {

        func handleChatMessageCreated(_ chatMessage: ChatMessage) {
            if
                !self.messages.contains(where: { $0.id.id == chatMessage.id.id }),
                !chatMessageIsMine(chatMessage: chatMessage)
            {
                // only publish downstream here if pubsub id doesn't exist yet
                // if pubsub id already exists then this message was mocked and published earlier
                self.messages.append(chatMessage)
                self.delegates.publish { $0.chatSession(self, didRecieveNewMessage: chatMessage)}
            }

            // Update the mocked message with version from server
            // Importantly we need the id assigned by the backend to perform other tasks like deletion
            // The mocked message only contains a clientMessageID
            if chatMessageIsMine(chatMessage: chatMessage),
               let index = messages.firstIndex(where: { $0.id.clientMessageID == chatMessage.id.clientMessageID })
            {
                self.messages[index] = chatMessage
            }
        }

        func handleChatMessageDeleted(_ payload: MessageDeletedPayload) {
            if
                let index = messages.firstIndex(where: { $0.id.id == payload.id })
            {
                let chatMessage = messages[index]
                messages.remove(at: index)
                deletedMessageIDs.insert(chatMessage.id)
                self.delegates.publish { $0.chatSession(self, didDeleteMessage: chatMessage.id)}
            } else {
                log.error("Failed to delete chat message because pubnubID cold not be found.")
            }
        }

        guard let payload = try? PubSubChatMessageDecoder.shared.decode(dict: message.message) else {
            log.error("Failed to decode pub sub chat message.")
            return
        }

        switch payload {
        case .messageCreated(let payload):
            let chatMessage = ChatMessage(
                from: payload,
                chatRoomID: self.roomID,
                timetoken: TimeToken(pubnubTimetoken: message.createdAt),
                actions: message.messageActions
            )

            // Only shadow muted messages from the current user should show
            if chatMessage.isMessageShadowMuted, !chatMessageIsMine(chatMessage: chatMessage) {
                break
            }

            if chatMessage.isMessageFiltered == true, containsFilteredMessages == false {
                /*
                 * Stop processing a recievedfiltered  message
                 * if the integrator chose not to show filtered messages
                 */
                break
            }

            handleChatMessageCreated(chatMessage)
        case .messageDeleted(let payload):
            handleChatMessageDeleted(payload)
        case .imageCreated(let payload):
            let chatMessage = ChatMessage(
                from: payload,
                chatRoomID: self.roomID,
                timetoken: TimeToken(pubnubTimetoken: message.createdAt),
                actions: message.messageActions
            )

            handleChatMessageCreated(chatMessage)
        case .imageDeleted(let payload):
            handleChatMessageDeleted(payload)
        case .customMessageCreated(let payload):
            let chatMessage = ChatMessage(
                from: payload,
                chatRoomID: self.roomID,
                timetoken: TimeToken(pubnubTimetoken: message.createdAt),
                actions: message.messageActions
            )
            handleChatMessageCreated(chatMessage)
        case .chatroomUpdated(let payload):
            let chatRoomInfo = ChatRoomInfo(
                id: payload.id,
                title: payload.title,
                visibility: (ChatRoomVisibilty(rawValue: payload.visibility!) ?? .everyone),
                contentFilter: payload.contentFilter,
                customData: payload.customData,
                tokenGates: payload.tokenGates,
                reactionSpaceID: payload.reactionSpaceID
            )
            self.delegates.publish { $0.chatSession(self, didRecieveRoomUpdate: chatRoomInfo)}
        case .addedToChatRoom:
            break
        case .invitedToChatRoom:
            break
        case .profileBlocked:
            break
        case .profileUnblocked:
            break
        case .reactionSpaceUpdated:
            break
        case .userReactionAdded:
            break
        case .userReactionRemoved:
            break
        case .messagePinned(let payload):
            let pinMessageInfo = PinMessageInfo(
                id: payload.id,
                url: payload.url,
                messageID: payload.messageID,
                messagePayload: payload.messagePayload,
                chatRoomID: payload.chatRoomID,
                pinnedByID: payload.pinnedByID,
                pinnedBy: payload.pinnedBy
            )
            self.delegates.publish {
                $0.chatSession(self, didPinMessage: pinMessageInfo)
            }
        case .messageUnpinned(let payload):
            let pinMessageInfoID: String = payload
            self.delegates.publish {
                $0.chatSession(self, didUnpinMessage: pinMessageInfoID)
            }
        }
    }

    func channel(_ channel: PubSubChannel, messageActionCreated messageAction: PubSubMessageAction) { }

    func channel(_ channel: PubSubChannel, messageActionDeleted messageActionID: String, messageID: String) { }
}

// MARK: - Reaction Session Delegate

extension PubSubChatRoom: ReactionSessionDelegate {
    func reactionSession(_ reactionSession: ReactionSession, didAddReaction reaction: UserReaction) {
        let messageID = reaction.targetID

        guard let index = messages.firstIndex(where: { $0.id.id == messageID }) else { return }

        var chatMessageType = messages[index]
        let reactionVote = ReactionVote(userReaction: reaction)
        chatMessageType.reactions.allVotes.append(reactionVote)
        messages[index] = chatMessageType
        self.delegates.publish { $0.chatSession(self, didRecieveMessageUpdate: chatMessageType)}
        self.delegates.publish {
            $0.chatSession(
                self,
                didRecieveAddMessageReaction: ReactionVote(userReaction: reaction)
            )
        }
    }

    func reactionSession(_ reactionSession: ReactionSession, didRemoveReaction reaction: UserReaction) {
        let messageID = reaction.targetID

        guard let index = messages.firstIndex(where: { $0.id.id == messageID }) else { return }

        var chatMessageType = messages[index]
        let voteID = reaction.id
        chatMessageType.reactions.allVotes.removeAll(where: { $0.voteID == voteID })
        messages[index] = chatMessageType
        self.delegates.publish { $0.chatSession(self, didRecieveMessageUpdate: chatMessageType)}
        self.delegates.publish {
            $0.chatSession(
                self,
                diRecieveRemoveMessageReaction: ReactionVote(userReaction: reaction)
            )
        }
    }
}

// MARK: - ChatMessagingOutput

extension PubSubChatRoom {

    func addDelegate(_ delegate: ChatSessionDelegate) {
        delegates.addListener(delegate)
    }

    func removeDelegate(_ delegate: ChatSessionDelegate) {
        delegates.removeListener(delegate)
    }

    var roomID: String {
        return _roomID
    }

    var title: String? {
        return _title
    }

    func getMessages(
        since timestamp: TimeToken,
        completion: @escaping (Result<[ChatMessage], Error>) -> Void
    ) {
        GetChatMesssagesRequest(
            accessToken: accessToken.asString,
            networking: networking,
            options: .init(
                chatRoomID: roomID,
                since: timestamp,
                pageSize: 100
            )
        ).execute(
            url: self.chatroomMessageURL
        ) {
            completion($0.map {
                self.processMessagesFromChatroomHistory(messagesResult: $0)
            })
        }
    }

    func getMessageCount(since timestamp: TimeToken, completion: @escaping (Result<Int, Error>) -> Void) {

        guard let encodedTimestamp = try? timestamp.encode() else { return }

        guard let chatroomMessagesCountURL = self.chatroomMessagesCountURL.appending(
            queryItems: [
                URLQueryItem(
                    name: ChatMessageQueryKeys.since.rawValue,
                    value: String(describing: encodedTimestamp)
                )
            ]
        ) else { return }

        firstly {
            self.chatAPI.getMessagesCount(chatroomMessagesCountURL: chatroomMessagesCountURL, accessToken: self.accessToken)
        }.then { result in
            completion(.success(result.count))
        }.catch { error in
            completion(.failure(error))
        }
    }

    func deleteMessage(message: ChatMessage, completion: @escaping (Result<Bool, Error>) -> Void) {
        guard let pubnubID = message.id.id else { return }
        firstly {
            self.chatAPI.deleteMessage(
                deleteMessagesURL: self.deleteMessageURL,
                messageID: pubnubID,
                timetoken: message.pubnubTimetoken.pubnubTimetoken,
                accessToken: self.accessToken
            )
        }.then { result in
            completion(.success(true))
        }.catch { error in
            completion(.failure(error))
        }
    }

    func deleteMessage(options: DeleteMessageRequestOptions, completion: @escaping (Result<Bool, any Error>) -> Void) {
        firstly {
            self.chatAPI.deleteMessage(
                deleteMessagesURL: self.deleteMessageURL,
                messageID: options.messageID,
                timetoken: options.pubnubTimetoken,
                accessToken: self.accessToken
            )
        }.then { result in
            completion(.success(true))
        }.catch { error in
            completion(.failure(error))
        }
    }

    func disconnect() {
        chatChannel?.disconnect()
    }

    func pause() {
        chatChannel?.pause()
    }

    func resume() {
        chatChannel?.resume()
    }

    func enterOpenChannel(_ channel: String) -> Promise<Void> {
        return Promise(value: ())
    }

    func enterOpenChannel(_ channel: String, sessionId: String) -> Promise<Void> {
        return self.enterOpenChannel(channel)
    }

    @discardableResult
    func exitChannel(_ channel: String) -> Promise<Void> {
        return Promise(value: ())
    }

    func exitChannel(_ channel: String, sessionId: String) {
        self.exitChannel(channel)
    }

    func reportMessage(messageID: String, completion: @escaping (Result<Void, Error>) -> Void) {
        guard let message = self.messages.first(where: { $0.id.id == messageID}) else {
            return completion(.failure(PubSubChatRoomError.failedToFindReportedChatMessage(messageId: messageID)))
        }
        guard let messageReporter = self.messageReporter else {
            return completion(.failure(PubSubChatRoomError.failedDueToMissingMessageReporter))
        }

        var messageBody = ""
        if let messageImageUrlString = message.imageURL?.absoluteString {
            messageBody = messageImageUrlString
        } else {
            messageBody = message.text ?? ""
        }

        let reportBody = ReportBody(
            channel: self.chatChannel?.name ?? "",
            profileId: message.senderID,
            nickname: message.senderNickname,
            messageId: messageID,
            message: messageBody,
            pubnubTimetoken: message.createdAt.pubnubTimetoken
        )
        messageReporter.report(reportBody: reportBody, completion: completion)
    }

    func removeMessageReaction(reactionVoteID: String, messageID: String, completion: @escaping (Result<Void, Error>) -> Void) {
        guard let reactionSession = reactionSession else {
            return completion(.failure(PubSubChatRoomError.reactionSessionMissing))
        }

        reactionSession.removeUserReaction(userReactionID: reactionVoteID) { result in
            switch result {
            case .success:
                log.dev("Successfully removed message action")
                completion(.success(()))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func addMessageReaction(
        messageID: String,
        reaction: String,
        completion: @escaping (Result<Void, Error>) -> Void
    ) {
        self.addMessageReaction(
            messageID: messageID,
            reactionID: reaction
        ) { (result: Result<ReactionVote, Error>) in
            completion(result.map { _ in return () })
        }
    }

    func addMessageReaction(
        messageID: String,
        reactionID: String,
        completion: @escaping (Result<ReactionVote, Error>) -> Void
    ) {
        guard let reactionSession = reactionSession else {
            return completion(.failure(PubSubChatRoomError.reactionSessionMissing))
        }

        // Adding reaction
        reactionSession.addUserReaction(
            targetID: messageID,
            reactionID: reactionID,
            customData: nil
        ) { result in
            switch result {
            case .success(let userReaction):
                log.dev("Successfully send message action.")
                let reactionVote = ReactionVote(userReaction: userReaction)
                completion(.success(reactionVote))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func loadInitialHistory(completion: @escaping (Result<Void, Error>) -> Void) {

        guard let chatroomMessagesHistoryURL = self.chatroomMessageURL.appending(
            queryItems: [
                URLQueryItem(
                    name: ChatMessageQueryKeys.chatRoomID.rawValue,
                    value: self.roomID
                ),
                URLQueryItem(
                    name: ChatMessageQueryKeys.pageSize.rawValue,
                    value: "100"
                )
            ]
        ) else { return }

        firstly {
            self.chatAPI.getMessages(chatroomMessagesURL: chatroomMessagesHistoryURL, accessToken: accessToken)
        }.then { historyResult in
            self.previousHistoryPageURL = historyResult.previous
            let messagesToBeShown = self.processMessagesFromChatroomHistory(messagesResult: historyResult)
            self.messages = messagesToBeShown
            completion(.success(()))
        }.catch { error in
            completion(.failure(error))
        }
    }

    func loadNextHistory(completion: @escaping (Result<[ChatMessage], Error>) -> Void) {
        guard !isLoadingHistory else {
            return completion(.failure(ChatSessionError.concurrentLoadHistoryCalls))
        }
        self.isLoadingHistory = true

        guard let previosHistoryPageURL = self.previousHistoryPageURL else {
            return completion(.failure(ChatSessionError.noHistoryMessages))
        }

        firstly {
            self.chatAPI.getMessages(
                chatroomMessagesURL: previosHistoryPageURL,
                accessToken: accessToken
            )
        }.then { historyResult in
            self.previousHistoryPageURL = historyResult.previous
            let messagesToBeShown = self.processMessagesFromChatroomHistory(messagesResult: historyResult)
            self.messages.insert(contentsOf: messagesToBeShown, at: 0)
            self.isLoadingHistory = false
            completion(.success(messagesToBeShown))
        }.catch { error in
            self.isLoadingHistory = false
            completion(.failure(error))
        }
    }

    private func getChatRoomEventsURL(eventsURL: URL, options: GetChatRoomEventRequestOptions) -> URL {
        var eventsURL = eventsURL
        if var urlComponents = URLComponents(
            url: eventsURL,
            resolvingAgainstBaseURL: false
        ) {
            urlComponents.queryItems = options.urlQueries
            if let url = urlComponents.url {
                eventsURL = url
            }
        }
        return eventsURL
    }

    func updateUserChatRoomImage(url: URL, completion: @escaping (Result<Void, Error>) -> Void) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.avatarURL = url
            completion(.success(()))
        }
    }

    func sendMessage(_ chatMessage: NewChatMessage, completion: @escaping (Result<ChatMessage, Error>) -> Void) -> ChatMessage {
        let messageID = UUID().uuidString.lowercased()
        let mockMessage = createMockMessage(newChatMessage: chatMessage, messageID: messageID, quoteMessage: nil)

        sendMessage(messageID: messageID, mockMessage: mockMessage, completion: completion)

        return mockMessage
    }

    func sendCustomMessage(_ customData: String, completion: @escaping (Result<ChatMessage, Error>) -> Void) -> ChatMessage {
        let mockMessage = createMockMessage(
            newChatMessage: NewChatMessage(customData: customData),
            messageID: UUID().uuidString.lowercased(),
            quoteMessage: nil
        )

        firstly {
            chatAPI.sendCustomMessage(chatRoomID: self.roomID, customMessagesUrl: self.customMessagesUrl, accessToken: self.accessToken, customData: customData)
        }.then { message in
            self.delegates.publish { $0.chatSession(self, didRecieveNewMessage: message) }
            completion(.success(message))
        }.catch { error in
            completion(.failure(error))
        }
        return mockMessage
    }

    func pinMessage(_ message: ChatMessage, completion: @escaping (Result<PinMessageInfo, Error>) -> Void) {
        guard let pubnubID = message.id.id else { return }

        let pubSubMessage = ChatMessagePayload(
            id: pubnubID,
            message: message.text,
            senderID: message.senderID,
            senderNickname: message.senderNickname,
            senderImageURL: message.profileImageURL,
            programDateTime: message.timestamp,
            filteredMessage: message.filteredMessage,
            contentFilter: message.filteredReasons.map { $0 },
            imageURL: message.imageURL,
            imageHeight: message.imageHeight != nil ? Int(message.imageHeight ?? 0) : nil,
            imageWidth: message.imageWidth != nil ? Int(message.imageWidth ?? 0) : nil,
            customData: message.customData,
            parentMessageId: message.parentMessageID,
            repliesCount: nil,
            repliesUrl: nil
        )

        firstly {
            chatAPI.pinMessage(message: pubSubMessage, chatRoomID: self.roomID, pinnedMessagesURL: self.pinnedMessagesURL, accessToken: self.accessToken)
        }.then { pinMessageInfo in
            completion(.success(pinMessageInfo))
        }.catch { error in
            log.error("Error Pinning Message with Id: \(pubSubMessage.id): \(error.localizedDescription)")
            completion(.failure(error))
        }
    }

    func unpinMessage(pinMessageInfoID: String, completion: @escaping (Result<Bool, Error>) -> Void) {
        let unpinMessagesURL = self.pinnedMessagesURL.appendingPathComponent(pinMessageInfoID, isDirectory: true)
        firstly {
            chatAPI.unpinMessage(pinnedMessagesURL: unpinMessagesURL, accessToken: self.accessToken)
        }.then { result in
            completion(.success(result))
        }.catch { error in
            log.error("Error unPinning Message with Id \(pinMessageInfoID): \(error.localizedDescription)")
            completion(.failure(error))
        }
    }

    func quoteMessage(_ chatMessage: NewChatMessage, quoteMessage: ChatMessagePayload, completion: @escaping (Result<ChatMessage, Error>) -> Void) -> ChatMessage {
        let messageID = UUID().uuidString.lowercased()
        let mockMessage = createMockMessage(newChatMessage: chatMessage, messageID: messageID, quoteMessage: quoteMessage)
        sendMessage(messageID: messageID, mockMessage: mockMessage, completion: completion)
        return mockMessage
    }

    func getPinMessageInfoList(orderBy: Ordering, page: Pagination, completion: @escaping (Result<[PinMessageInfo], Error>) -> Void) {
        let pageResultURLID = "getPinMessageInfoList_\(self.roomID)_\(orderBy.rawValue)"
        let pinMessageInfoURL: Promise<URL> = Promise { fulfill, reject in
            switch page {
            case .first:

                // Reset result repo cache if the user is calling `.first` page
                self.chatRoomPageURLRepo[pageResultURLID] = nil

                let queryComponents = [
                    URLQueryItem(name: ChatMessageQueryKeys.chatRoomID.rawValue, value: self.roomID),
                    URLQueryItem(name: ChatMessageQueryKeys.ordering.rawValue, value: orderBy == .ascending ? "pinned_at" : "-pinned_at")
                ]
                var pinMessagesURLComponents = URLComponents(string: self.pinnedMessagesURL.absoluteString)
                pinMessagesURLComponents?.queryItems = queryComponents

                if let pinMessageListURL = pinMessagesURLComponents?.url {
                    fulfill(pinMessageListURL)
                } else {
                    reject(ChatSessionError.invalidPinnedMessagesURLTemplate)
                }

            case .next:
                if let url = self.chatRoomPageURLRepo[pageResultURLID]?.nextURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.nextPageUnavailable)
                }
            case .previous:
                if let url = self.chatRoomPageURLRepo[pageResultURLID]?.previousURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.previousPageUnavailable)
                }
            }
        }

        firstly {
            pinMessageInfoURL
        }.then { url in
            self.chatAPI.getPinMessageInfoList(
                pinMessageInfoListURL: url,
                accessToken: self.accessToken
            )
        }.then { paginatedPinMessageInfoList in
            self.chatRoomPageURLRepo[pageResultURLID] = PaginatedResultURL(
                nextURL: paginatedPinMessageInfoList.next,
                previousURL: paginatedPinMessageInfoList.previous
            )
            completion(.success(paginatedPinMessageInfoList.items))
        }.catch { error in
            log.error("Failed getting list of pinned messages")
            completion(.failure(error))
        }
    }
}
