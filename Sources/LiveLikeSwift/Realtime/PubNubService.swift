//
//  PubNubService.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 11/25/19.
//

import PubNub
import Foundation
import LiveLikeCore

final class WeakObject<T: AnyObject> {
    private(set) weak var value: T?
    init(_ v: T) { value = v }
}

@propertyWrapper
struct Weak<Element> where Element: AnyObject {
    private var storage = [WeakObject<Element>]()

    var wrappedValue: [Element] {
        get { storage.compactMap { $0.value } }
        set {
            storage = newValue.map { WeakObject($0) }
        }
    }
}

class PubNubListener {
    var messageReceived: ((PubNubMessage) -> Void)?
    var messageActionAdded: ((PubNubMessageAction) -> Void)?
    var messageActionRemoved: ((PubNubMessageAction) -> Void)?
}

class PubNubService: PubSubService {
    private let messagingSerialQueue = DispatchQueue(label: "com.livelike.pubnub.chat")
    private let listener: SubscriptionListener

    let pubnub: PubNub
    @Weak var didReceiveMessage: [PubNubListener]
    
    init(
        publishKey: String?,
        subscribeKey: String,
        authKey: String,
        origin: String?,
        userID: String,
        heartbeatInterval: Int,
        heartbeatTimeout: Int
    ) {
        var config = PubNubConfiguration(
            publishKey: publishKey,
            subscribeKey: subscribeKey,
            userId: userID
        )
        
        // only apply Auth key when we have a publish key
        if publishKey != nil {
            config.authKey = authKey
        }
        
        if let origin = origin {
            config.origin = origin
        }
        
        // Configure Presence heartbeat
        config.heartbeatInterval = UInt(heartbeatInterval)
        config.durationUntilTimeout = UInt(heartbeatTimeout)
        
        config.automaticRetry = AutomaticRetry(
            retryLimit: 9,
            policy: .exponential(minDelay: 3, maxDelay: 60)
        )

        config.filterExpression = "!(content_filter LIKE '*filtered*') || sender_id == '\(userID)'"
        
        pubnub = PubNub(configuration: config)
        
        listener = SubscriptionListener(queue: messagingSerialQueue)
        
        listener.didReceiveStatus = { statusEvent in
            switch statusEvent {
            case .success(let connectionStatus):
                switch connectionStatus {
                case .disconnectedUnexpectedly:
                    log.info("Chat Disconnected")
                case .reconnecting:
                    log.info("Chat Reconnecting")
                case .connected:
                    log.info("Chat Connected")
                default:
                    break
                }
            case .failure(let error):
                log.error(error)
            }
        }
        
        listener.didReceiveStatus = { (status: SubscriptionListener.StatusEvent) in
            log.info("[PubNubService] PubNub Status: \(status)")
        }
        
        listener.didReceiveSubscription = { [weak self] event in
            guard let self = self else { return }
            switch event {
            case .messageReceived(let message):
                self.didReceiveMessage.forEach {
                    $0.messageReceived?(message)
                }
            case .messageActionAdded(let messageAction):
                self.didReceiveMessage.forEach {
                    $0.messageActionAdded?(messageAction)
                }
            case .messageActionRemoved(let messageAction):
                self.didReceiveMessage.forEach {
                    $0.messageActionRemoved?(messageAction)
                }
            default:
                break
            }
        }

        pubnub.add(listener)
    }
    
    deinit {
        pubnub.disconnect()
    }
    
    func subscribe(_ channel: String) -> PubSubChannel {
        return PubNubChannel(
            pubnubService: self,
            channel: channel,
            includeTimeToken: true,
            includeMessageActions: true,
            queue: messagingSerialQueue
        )
    }
}
