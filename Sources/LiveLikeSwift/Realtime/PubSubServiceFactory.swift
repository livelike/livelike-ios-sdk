//
//  PubSubServiceFactory.swift
//  
//
//  Created by Jelzon Monzon on 1/19/23.
//

import Foundation
import LiveLikeCore

protocol PubSubServiceFactory {
    func make() -> Promise<PubSubService>
}

class PubNubServiceFactory: PubSubServiceFactory {

    private let whenApplication: Promise<ApplicationConfiguration>
    private let whenAccessToken: Promise<AccessToken>
    private let whenProfile: Promise<ProfileResource>

    init(
        whenApplication: Promise<ApplicationConfiguration>,
        whenAccessToken: Promise<AccessToken>,
        whenProfile: Promise<ProfileResource>
    ) {
        self.whenApplication = whenApplication
        self.whenAccessToken = whenAccessToken
        self.whenProfile = whenProfile
    }

    func make() -> Promise<PubSubService> {
        return firstly {
            Promises.zip(
                self.whenApplication,
                self.whenAccessToken,
                self.whenProfile
            )
        }.then { application, accessToken, profile in
            if let subscribeKey = application.pubnubSubscribeKey {
                let pubSubService = PubNubService(
                    publishKey: application.pubnubPublishKey,
                    subscribeKey: subscribeKey,
                    authKey: accessToken.asString,
                    origin: application.pubnubOrigin,
                    userID: profile.id,
                    heartbeatInterval: application.pubnubHeartbeatInterval,
                    heartbeatTimeout: application.pubnubPresenceTimeout
                )
                return Promise(value: pubSubService)
            } else {
                let error = ContentSessionError.missingPubNubSubscribeKeyForChat
                log.error(error)
                return Promise(error: error)
            }
        }
    }
}
