//
//  PubNubChannel.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 11/27/19.
//

import PubNub
import Foundation

class PubNubChannel: PubSubChannel {
    weak var delegate: PubSubChannelDelegate?

    private let pubnubService: PubNubService
    private let _channel: String
    private let includeTimeToken: Bool
    private let includeMessageActions: Bool
    private let pubnubListener = PubNubListener()

    var name: String {
        return _channel
    }

    var pauseStatus: PauseStatus = .unpaused

    init(
        pubnubService: PubNubService,
        channel: String,
        includeTimeToken: Bool,
        includeMessageActions: Bool,
        queue: DispatchQueue
    ) {
        self.pubnubService = pubnubService
        self._channel = channel
        self.includeTimeToken = includeTimeToken
        self.includeMessageActions = includeMessageActions

        pubnubListener.messageReceived = { [weak self] message in
            guard let self = self else { return }
            guard message.channel == self._channel else { return }
            guard let messageDict = (message.payload as? AnyJSON)?.dictionaryOptional else { return }

            let id = String(message.published)
            let pnMessage = PubSubChannelMessage(
                pubsubID: id,
                message: messageDict,
                createdAt: message.published,
                messageActions: []
            )
            self.delegate?.channel(self, messageCreated: pnMessage)
        }

        pubnubListener.messageActionAdded = { [weak self] messageAction in
            guard let self = self else { return }
            guard messageAction.channel == self._channel else { return }
            let messageID = String(messageAction.messageTimetoken)
            let actionID = String(messageAction.actionTimetoken)
            let pnMessageAction = PubSubMessageAction(
                messageID: messageID,
                id: actionID,
                sender: messageAction.publisher,
                type: messageAction.actionType,
                value: messageAction.actionValue,
                timetoken: messageAction.actionTimetoken,
                messageTimetoken: messageAction.messageTimetoken
            )
            self.delegate?.channel(self, messageActionCreated: pnMessageAction)
        }

        pubnubListener.messageActionRemoved = { [weak self] messageAction in
            guard let self = self else { return }
            guard messageAction.channel == self._channel else { return }
            let messageID = String(messageAction.messageTimetoken)
            let actionID = String(messageAction.actionTimetoken)
            self.delegate?.channel(self, messageActionDeleted: actionID, messageID: messageID)
        }

        pubnubService.didReceiveMessage.append(pubnubListener)
        pubnubService.pubnub.subscribe(to: [channel], withPresence: false)
    }

    func send(
        _ message: [String: AnyObject],
        completion: @escaping (Result<String, Error>) -> Void
    ) {
        pubnubService.pubnub.publish(channel: self._channel, message: AnyJSON(message)) { result in
            switch result {
            case .success(let timetoken):
                let id = String(timetoken)
                completion(.success(id))
            case .failure(let error):
                return completion(.failure(PubNubChannelError.pubnubStatusError(errorStatus: error)))
            }
        }
    }

    func messageCount(
        since timestamp: TimeToken,
        completion: @escaping (Result<Int, Error>) -> Void
    ) {
        self.pubnubService.pubnub.messageCounts(
            channels: [_channel],
            timetoken: timestamp.pubnubTimetoken
        ) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let messageCountsDict):
                guard let count = messageCountsDict[self._channel] else {
                    return completion(.failure(PubNubChannelError.noMessageCountForChannel(channel: self._channel)))
                }
                completion(.success(count))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func fetchHistory(
        oldestMessageDate: TimeToken?,
        newestMessageDate: TimeToken?,
        limit: UInt,
        completion: @escaping (Result<PubSubHistoryResult, Error>) -> Void
    ) {
        self.fetchHistory(
            start: oldestMessageDate,
            end: newestMessageDate,
            reverse: false,
            limit: limit,
            completion: completion
        )
    }

    // Gets messages after the TimeToken
    func fetchMessages(
        since timestamp: TimeToken,
        limit: UInt,
        completion: @escaping (Result<PubSubHistoryResult, Error>) -> Void
    ) {
        // https://www.pubnub.com/docs/swift/storage-and-history#retrieval-scenario-2
        // Ideally we would use Scenario 3 but it doesn't work
        // So we need to use Scenario 2 and manually filter out messages with the exact timetoken
        self.fetchHistory(
            start: nil,
            end: timestamp,
            reverse: false,
            limit: limit
        ) { result in
            switch result {
            case .success(var historyResult):
                historyResult.messages.removeAll(where: { $0.createdAt == timestamp.pubnubTimetoken })
                completion(.success(historyResult))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    private func fetchHistory(
        start: TimeToken?,
        end: TimeToken?,
        reverse: Bool,
        limit: UInt,
        completion: @escaping (Result<PubSubHistoryResult, Error>) -> Void
    ) {
        self.pubnubService.pubnub.fetchMessageHistory(
            for: [_channel],
            includeActions: self.includeMessageActions,
            includeMeta: self.includeTimeToken,
            includeUUID: true,
            includeMessageType: true,
            page: PubNubBoundedPageBase(
                start: start?.pubnubTimetoken,
                end: end?.pubnubTimetoken,
                limit: Int(limit)
            )
        ) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let (messagesByChannel, _)):
                guard let channelResults = messagesByChannel[self._channel] else {
                    return completion(.failure(PubNubChannelError.foundNoResultsForChannel(channel: self._channel)))
                }

                let chatMessages: [PubSubChannelMessage] = channelResults.compactMap { historyMessage in
                    guard let messageAsDict = (historyMessage.payload as? AnyJSON)?.dictionaryOptional else {
                        return nil
                    }
                    let messageActions: [PubSubMessageAction] = historyMessage.actions.map { action in
                        return PubSubMessageAction(
                            messageID: String(action.messageTimetoken),
                            id: String(action.actionTimetoken),
                            sender: action.publisher,
                            type: action.actionType,
                            value: action.actionValue,
                            timetoken: action.actionTimetoken,
                            messageTimetoken: action.messageTimetoken
                        )
                    }

                    return PubSubChannelMessage(
                        pubsubID: String(historyMessage.published),
                        message: messageAsDict,
                        createdAt: historyMessage.published,
                        messageActions: messageActions
                    )
                }

                completion(
                    .success(
                        PubSubHistoryResult(
                            newestMessageTimetoken: TimeToken(pubnubTimetoken: chatMessages.last!.createdAt),
                            oldestMessageTimetoken: TimeToken(pubnubTimetoken: chatMessages.first!.createdAt),
                            messages: chatMessages
                        )
                    )
                )
            }
        }
    }

    func sendMessageAction(
        type: String,
        value: String,
        messageID: String,
        completion: @escaping (Result<Bool, Error>) -> Void
    ) {
        guard let messageTimetoken = UInt64(messageID) else {
            return completion(.failure(PubNubChannelError.expectedPubSubIDInternalToBeNSNumber))
        }

        pubnubService.pubnub.addMessageAction(
            channel: _channel,
            type: type,
            value: value,
            messageTimetoken: messageTimetoken
        ) { result in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let messageAction):
                let messageID = String(messageAction.messageTimetoken)
                let actionID = String(messageAction.actionTimetoken)
                let pnMessageAction = PubSubMessageAction(
                    messageID: messageID,
                    id: actionID,
                    sender: messageAction.publisher,
                    type: type,
                    value: value,
                    timetoken: messageAction.actionTimetoken,
                    messageTimetoken: messageAction.messageTimetoken
                )
                completion(.success(true))
            }
        }
    }

    func removeMessageAction(
        messageID: String,
        messageActionID: String,
        completion: @escaping (Result<Bool, Error>) -> Void
    ) {
        guard let messageTimetoken = UInt64(messageID) else {
            return completion(.failure(PubNubChannelError.expectedPubSubIDInternalToBeNSNumber))
        }

        guard let actionTimetoken = UInt64(messageActionID) else {
            return completion(.failure(PubNubChannelError.expectedPubSubIDInternalToBeNSNumber))
        }

        pubnubService.pubnub.removeMessageActions(
            channel: _channel,
            message: messageTimetoken,
            action: actionTimetoken
        ) { result in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success:
                completion(.success(true))
            }
        }
    }

    func disconnect() {
        pubnubService.pubnub.unsubscribe(from: [_channel])
    }

    func pause() {
        pauseStatus = .paused
    }

    func resume() {
        pauseStatus = .unpaused
    }
}
