//
//  UserClient.swift
//  
//
//  Created by Keval Shah on 04/04/23.
//

import Foundation
import LiveLikeCore

// MARK: - Protocols

public protocol UserClientDelegate: AnyObject {
    /**
     Called when the current user blocks another user
     
     - parameter blockInfo: An object of type BlockInfo containing details of the blocked profile, type of block and details of the profile requesting the block.
     */
    func userClient(_ userClient: UserClient, userDidGetBlocked blockInfo: BlockInfo)
    
    /**
     Called when the current user is added to the chatroom by another user
     
     - parameter unblockInfo: An object of type unblockedProfileInfo containing the id of the profile that was unblocked..
     */
    func userClient(_ userClient: UserClient, userDidGetUnblocked unblockInfo: UnblockInfo)
}

public protocol UserClient: AnyObject {
    
    var delegate: UserClientDelegate? { get set }
    
    /// Block a profile
    func blockProfile(
        profileID: String,
        completion: @escaping (Result<BlockInfo, Error>) -> Void
    )
    
    /// Unblock a previously blocked profile
    func unblockProfile(
        blockRequestID: String,
        completion: @escaping (Result<Bool, Error>) -> Void
    )
    
    /**
      Get a paginated list of profiles blocked by the current user. The list can be filtered using the parameters.
     
      - parameter blockedProfileID: String type which can be used to filter the list based on the id of the blocked profile
     */
    func getBlockedProfileList(
        blockedProfileID: String?,
        page: Pagination,
        completion: @escaping (Result<[BlockInfo], Error>) -> Void
    )
    
    /**
     Get details of a blocked profile for a particular profileID
     
     - parameter profileID: String type parameter of the profile ID for which the block information is requested.
     */
    func getProfileBlockInfo(
        profileID: String,
        completion: @escaping (Result<BlockInfo, Error>) -> Void
    )
    
    /**
     Get a list of all blocked profile IDs for a particular profileID
     */
    func getBlockedProfileIDList(
        page: Pagination,
        completion: @escaping (Result<PaginatedResult<String>, Error>) -> Void
    )
    
    /// Returns a complete list of blocked profile ids
    func getBlockedProfileIDListComplete(
        completion: @escaping (Result<[String], Error>) -> Void
    )
}

// MARK: - Internal

final class InternalUserClient: UserClient, PubSubChannelDelegate {
    
    weak var delegate: UserClientDelegate?
    
    private var userChannel: PubSubChannel?
    private let whenAccessToken: Promise<AccessToken>
    private let whenUserProfile: Promise<UserProfile>
    private let whenProfileResource: Promise<ProfileResource>
    private let coreAPI: LiveLikeCoreAPIProtocol
    private let networking: LLNetworking

    lazy var whenBlockList: Promise<BlockList> = {
        firstly {
            self.whenUserProfile
        }.then { userProfile in
            let blockList = BlockList(for: userProfile.userID.asString)
            return Promise { fulfill, reject in
                self.getBlockedProfileIDList(page: .first) { [weak self] result in
                    guard let self = self else { return }
                    switch result {
                    case .success(let blockedIDList):
                        blockList.clear()
                        for blockId in blockedIDList.items {
                            blockList.block(userWithID: blockId)
                        }
                        self.loadNextBlockedProfilesPageRecursive(
                            blockList: blockList,
                            blockedIDList: blockedIDList,
                            profileID: userProfile.userID.asString
                        ) { blockList in
                            fulfill(blockList)
                        }
                    case .failure(let error):
                        log.error("There was an error loading a page of the block list. The block list may be incomplete. Error: \(error.localizedDescription)")
                        fulfill(blockList)
                    }
                }
            }
        }
    }()
    
    private var listeners = Listener<UserClientDelegate>(dispatchQueueLabel: "com.livelike.userClientListeners")
    
    private let paginatedRequestController = PaginatedRequestController()
    
    struct UserClientPaginationProgress {
        var next: URL?
        var previous: URL?
        var total: Int = 0
    }
    
    var userClientPagination: UserClientPaginationProgress
    private var clientPageURLRepo = [String: PaginatedResultURL]()
    
    init(
        whenAccessToken: Promise<AccessToken>,
        whenUserProfile: Promise<UserProfile>,
        whenProfileResource: Promise<ProfileResource>,
        whenPubSubService: Promise<PubSubService?>,
        coreAPI: LiveLikeCoreAPIProtocol,
        networking: LLNetworking
    ) {
        self.whenUserProfile = whenUserProfile
        self.whenAccessToken = whenAccessToken
        self.whenProfileResource = whenProfileResource
        self.coreAPI = coreAPI
        self.networking = networking
        
        self.userClientPagination = UserClientPaginationProgress()
        firstly {
            coreAPI.whenApplicationConfig
        }.then { application in
            return Promises.zip(
                Promise(value: application),
                whenPubSubService,
                whenProfileResource
            )
        }.then { application, pubsubService, profileResource in
            let userPubnubChannel = profileResource.subscribeChannel
            let userChannel = pubsubService?.subscribe(userPubnubChannel)
            self.userChannel = userChannel
            self.userChannel?.delegate = self
        }
    }
    
    deinit {
        log.info("User Session has ended.")
    }
    
    func blockProfile(profileID: String, completion: @escaping (Result<BlockInfo, Error>) -> Void) {
        firstly {
            Promises.zip(
                whenProfileResource,
                whenAccessToken,
                whenBlockList
            )
        }.then(on: DispatchQueue.global()) { userProfile, accessToken, blockList in
            Promises.zip(
                CreateBlockProfileRequest(
                    blockedProfileID: profileID,
                    accessToken: accessToken.asString,
                    networking: self.networking
                ).execute(url: userProfile.blockProfileUrl),
                Promise(value: blockList)
            )
        }.then { blockProfile, blockList in
            blockList.block(userWithID: profileID)
            completion(.success(blockProfile))
        }.catch { error in
            log.error("Error Blocking User with userId: \(profileID): \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func unblockProfile(blockRequestID: String, completion: @escaping (Result<Bool, Error>) -> Void) {
        firstly {
            Promises.zip(
                whenProfileResource,
                whenAccessToken
            )
        }.then(on: DispatchQueue.global()) { userProfile, accessToken in
            return DeleteBlockProfileRequest(
                accessToken: accessToken.asString,
                networking: self.networking
            ).execute(url: userProfile.blockProfileUrl.appendingPathComponent(
                blockRequestID,
                isDirectory: true
            )
            )
        }.then { result in
            completion(.success(result))
        }.catch { error in
            log.error("Error Unblocking User with requestID: \(blockRequestID): \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func getBlockedProfileList(
        blockedProfileID: String?,
        page: Pagination,
        completion: @escaping (Result<[BlockInfo], Error>
        ) -> Void
    ) {
        firstly {
            Promises.zip(
                whenProfileResource,
                whenAccessToken
            )
        }.then(on: DispatchQueue.global()) { profileResource, accessToken in
            self.paginatedRequestController.performExecute(
                paginatedRequest: GetBlockedProfilesListRequest(
                    options: GetBlockedProfilesListRequestOptions(
                        blockedProfileID: blockedProfileID
                    ),
                    accessToken: accessToken.asString,
                    networking: self.networking
                ),
                rootURL: profileResource.blockProfileUrl,
                page: page,
                completion: completion
            )
        }
    }
    
    func getProfileBlockInfo(
        profileID: String,
        completion: @escaping (Result<BlockInfo, Error>) -> Void
    ) {
        self.getBlockedProfileList(blockedProfileID: profileID, page: .first) { result in
            switch result {
            case .success(let blockedProfiles):
                if let blockProfileInfo = blockedProfiles.first {
                    completion(.success(blockProfileInfo))
                } else {
                    log.error("Failed getting information of blocked profile")
                    completion(.failure(ChatClientError.decodingFailedForBlockInfo))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func getBlockedProfileIDList(
        page: Pagination,
        completion: @escaping (Result<PaginatedResult<String>, Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                whenProfileResource,
                whenAccessToken
            )
        }.then(on: DispatchQueue.global()) { profileResource, accessToken in
            return GetBlockedProfilesIDListRequest(
                accessToken: accessToken.asString,
                networking: self.networking
            ).execute(url: profileResource.blockedProfileIdsUrl)
        }.then { commentBoardBanDetail in
            completion(.success(commentBoardBanDetail))
        }.catch { error in
            log.error("Failed to get detail of comment board ban: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func getBlockedProfileIDListComplete(
        completion: @escaping (Result<[String], Error>) -> Void
    ) {
        firstly {
            self.whenBlockList
        }.then {
            completion(.success($0.blockedUserIDs.allObjects))
        }.catch {
            completion(.failure($0))
        }
    }
    
    func channel(_ channel: PubSubChannel, messageCreated message: PubSubChannelMessage) {
        guard let payload = try? PubSubChatMessageDecoder.shared.decode(dict: message.message) else {
            log.error("Failed to decode pub sub chat message.")
            return
        }
        
        switch payload {
        case .messageCreated:
            break
        case .messageDeleted:
            break
        case .imageCreated:
            break
        case .imageDeleted:
            break
        case .customMessageCreated:
            break
        case .chatroomUpdated:
            break
        case .reactionSpaceUpdated:
            break
        case .userReactionAdded:
            break
        case .userReactionRemoved:
            break
        case .addedToChatRoom:
            break
        case .invitedToChatRoom:
            break
        case .profileBlocked(let payload):
            let blockInfo = BlockInfo(
                id: payload.id,
                url: payload.url,
                blockedProfileID: payload.blockedProfileID,
                blockedByProfileID: payload.blockedByProfileID,
                blockedProfile: payload.blockedProfile,
                blockedByProfile: payload.blockedByProfile
            )
            
            firstly {
                whenBlockList
            }.then { blockList in
                blockList.block(userWithID: payload.blockedProfileID)
                self.listeners.publish { $0.userClient(self, userDidGetBlocked: blockInfo)}
                if let delegate = self.delegate {
                    delegate.userClient(self, userDidGetBlocked: blockInfo)
                }
            }
        case .profileUnblocked(let payload):
            let unblockInfo = UnblockInfo(
                id: payload.id,
                blockedProfileID: payload.blockedProfileID
            )
            firstly {
                whenBlockList
            }.then { blockList in
                blockList.unblock(userWithID: payload.blockedProfileID)
                self.listeners.publish { $0.userClient(self, userDidGetUnblocked: unblockInfo)}
                if let delegate = self.delegate {
                    delegate.userClient(self, userDidGetUnblocked: unblockInfo)
                }
            }
        case .messagePinned:
            break
        case .messageUnpinned:
            break
        }
    }
    
    func channel(_ channel: PubSubChannel, messageActionCreated messageAction: PubSubMessageAction) { }
    
    func channel(_ channel: PubSubChannel, messageActionDeleted messageActionID: String, messageID: String) { }
    
    private func loadNextBlockedProfilesPageRecursive(
        blockList: BlockList,
        blockedIDList: PaginatedResult<String>,
        profileID: String,
        completion: @escaping (BlockList) -> Void
    ) {
        if blockedIDList.hasNext {
            self.getBlockedProfileIDList(page: .next) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let blockedIDs):
                    for blockId in blockedIDs.items {
                        blockList.block(userWithID: blockId)
                    }
                    self.loadNextBlockedProfilesPageRecursive(
                        blockList: blockList,
                        blockedIDList: blockedIDs,
                        profileID: profileID,
                        completion: completion
                    )
                case .failure(let error):
                    log.error("There was an error loading a page of the block list. The block list may be incomplete. Error: \(error.localizedDescription)")
                }
            }
        } else {
            completion(blockList)
        }
    }
}
