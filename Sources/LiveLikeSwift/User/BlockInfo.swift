//
//  BlockInfo.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

/// Object that is returned when `blockProfile()` is called. Contains details of the profile of the blocked and blocker profile
public struct BlockInfo: Decodable {
    public let id: String
    internal let url: URL
    public let blockedProfileID: String
    public let blockedByProfileID: String
    public let blockedProfile: ProfileResource
    public let blockedByProfile: ProfileResource

    enum CodingKeys: String, CodingKey {
        case id
        case url
        case blockedProfileId
        case blockedByProfileId
        case blockedProfile
        case blockedByProfile
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.url = try container.decode(URL.self, forKey: .url)
        self.blockedProfileID = try container.decode(String.self, forKey: .blockedProfileId)
        self.blockedByProfileID = try container.decode(String.self, forKey: .blockedByProfileId)
        self.blockedProfile = try container.decode(ProfileResource.self, forKey: .blockedProfile)
        self.blockedByProfile = try container.decode(ProfileResource.self, forKey: .blockedByProfile)
    }

    init(
        id: String,
        url: URL,
        blockedProfileID: String,
        blockedByProfileID: String,
        blockedProfile: ProfileResource,
        blockedByProfile: ProfileResource
    ) {
        self.id = id
        self.url = url
        self.blockedProfileID = blockedProfileID
        self.blockedByProfileID = blockedByProfileID
        self.blockedProfile = blockedProfile
        self.blockedByProfile = blockedByProfile
    }
}
