//
//  UnblockInfo.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

/// Object that is returned when `unblockProfile()` is called. Contains id of the profile unblocked
public struct UnblockInfo: Decodable {
    public let id: String
    public let blockedProfileID: String

    enum CodingKeys: String, CodingKey {
        case id
        case blockedProfileId
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.blockedProfileID = try container.decode(String.self, forKey: .blockedProfileId)
    }

    init(
        id: String,
        blockedProfileID: String
    ) {
        self.id = id
        self.blockedProfileID = blockedProfileID
    }
}
