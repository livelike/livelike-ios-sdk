//
//  CreateBlockProfileRequest.swift
//  
//
//  Created by Keval Shah on 04/04/23.
//

import Foundation
import LiveLikeCore

struct CreateBlockProfileRequest {
    
    private let accessToken: String
    private let blockedProfileID: String
    private let networking: LLNetworking
    
    init(
        blockedProfileID: String,
        accessToken: String,
        networking: LLNetworking
    ) {
        self.blockedProfileID = blockedProfileID
        self.accessToken = accessToken
        self.networking = networking
    }
    
}

extension CreateBlockProfileRequest: LLRequest {
    typealias ResponseType = BlockInfo
    
    func execute(
        url: URL,
        completion: @escaping (Result<BlockInfo, Error>) -> Void
    ) {
        struct Payload: Encodable {
            let blockedProfileID: String
        }
        let payload = Payload(
            blockedProfileID: self.blockedProfileID
        )
        
        let resource = Resource<BlockInfo>.init(
            url: url,
            method: .post(payload),
            accessToken: accessToken
        )
        networking.task(resource, completion: completion)
    }
}
