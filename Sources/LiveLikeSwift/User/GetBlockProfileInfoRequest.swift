//
//  GetBlockProfileInfoRequest.swift
//  
//
//  Created by Keval Shah on 04/04/23.
//

import Foundation
import LiveLikeCore

struct GetBlockProfileInfoRequest {

    private let accessToken: String
    private let networking: LLNetworking

    init(
        accessToken: String,
        networking: LLNetworking
    ) {
        self.accessToken = accessToken
        self.networking = networking
    }
}

extension GetBlockProfileInfoRequest: LLRequest {
    typealias ResponseType = BlockInfo

    func execute(
        url: URL,
        completion: @escaping (Result<BlockInfo, Error>) -> Void
    ) {
        let resource = Resource<BlockInfo>(
            get: url,
            accessToken: accessToken
        )
        networking.task(resource, completion: completion)
    }
}
