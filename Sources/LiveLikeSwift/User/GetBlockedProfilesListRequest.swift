//
//  GetBlockedProfilesListRequest.swift
//  
//
//  Created by Keval Shah on 04/04/23.
//

import Foundation
import LiveLikeCore

struct GetBlockedProfilesListRequest {

    private let options: GetBlockedProfilesListRequestOptions?
    private let accessToken: String
    private let networking: LLNetworking

    init(
        options: GetBlockedProfilesListRequestOptions?,
        accessToken: String,
        networking: LLNetworking
    ) {
        self.options = options
        self.accessToken = accessToken
        self.networking = networking
    }

    private func buildURL(baseURL: URL) throws -> URL {
        guard var components = URLComponents(url: baseURL, resolvingAgainstBaseURL: false) else {
            throw URLComponentError.failedToInitURLComponents(fromURL: baseURL)
        }

        var urlQueries = [URLQueryItem]()

        if let blockedProfileID = options?.blockedProfileID {
            urlQueries.append(URLQueryItem(name: "blocked_profile_id", value: blockedProfileID))
        }

        components.queryItems = urlQueries

        guard let url = components.url else {
            throw URLComponentError.urlIsNil
        }

        return url
    }
}

extension GetBlockedProfilesListRequest: LLRequest {
    typealias ResponseType = PaginatedResult<BlockInfo>

    func execute(
        url: URL,
        completion: @escaping (Result<PaginatedResult<BlockInfo>, Error>) -> Void
    ) {
        do {
            let resource = try Resource<PaginatedResult<BlockInfo>>(
                get: buildURL(baseURL: url),
                accessToken: accessToken
            )
            networking.task(resource, completion: completion)
        } catch {
            completion(.failure(error))
        }
    }
}

extension GetBlockedProfilesListRequest: LLPaginatedRequest {
    typealias ElementType = BlockInfo

    var requestID: String {
        var id = "GetBlockedProfilesListRequest"
        id += options?.blockedProfileID ?? ""
        return id
    }
}

/// A configurable request object used to query for Blocked Profiles List
public struct GetBlockedProfilesListRequestOptions {

    let blockedProfileID: String?

    /// Initialize options to be used in the Blocked Profiles List queries
    public init(blockedProfileID: String?) {
        self.blockedProfileID = blockedProfileID
    }
}
