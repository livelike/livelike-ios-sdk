//
//  GetBlockedProfilesIDListRequest.swift
//  
//
//  Created by Keval Shah on 12/04/23.
//

import Foundation
import LiveLikeCore

struct GetBlockedProfilesIDListRequest {

    private let accessToken: String
    private let networking: LLNetworking

    init(
        accessToken: String,
        networking: LLNetworking
    ) {
        self.accessToken = accessToken
        self.networking = networking
    }
}

extension GetBlockedProfilesIDListRequest: LLRequest {
    typealias ResponseType = PaginatedResult<String>

    func execute(
        url: URL,
        completion: @escaping (Result<PaginatedResult<String>, Error>) -> Void
    ) {
        let resource = Resource<PaginatedResult<String>>(
            get: url,
            accessToken: accessToken
        )
        networking.task(resource, completion: completion)
    }
}
