//
//  CommentReport.swift
//  
//
//  Created by Keval Shah on 12/04/23.
//

import Foundation

/// The `CommentReport` struct represents the Reported Comment created by users in a `CommentBoard`
public struct CommentReport {
    // MARK: - Public Properties
    /// The unique identifer of the Comment Report
    public let id: String
    /// The unique identifier of the Comment Board where the report was created
    public let commentBoardID: String
    /// The unique identifier for the comment that was reported
    public let commentID: String
    /// The corresponding `Comment` object.
    public let comment: Comment
    /// The unique identifier of the user who reported the comment
    public let reportedBy: String
    ///  The time correspoding to the creation of the report
    public let reportedAt: Date
    /// The description for the comment report
    public let description: String?
    /// The status of the report for the comment. Can be either pending or dismissed.
    public let reportStatus: CommentReportStatus

    // MARK: Internal Properties
    internal let url: URL

    init(
        id: String,
        url: URL,
        commentBoardID: String,
        commentID: String,
        comment: Comment,
        reportedBy: String,
        reportedAt: Date,
        description: String?,
        reportStatus: CommentReportStatus
    ) {
        self.id = id
        self.url = url
        self.commentBoardID = commentBoardID
        self.commentID = commentID
        self.comment = comment
        self.reportedBy = reportedBy
        self.reportedAt = reportedAt
        self.description = description
        self.reportStatus = reportStatus
    }
}

extension CommentReport: Decodable {

    enum CodingKeys: String, CodingKey {
        case id
        case url
        case commentBoardId
        case commentId
        case comment
        case reportedById
        case reportedAt
        case description
        case reportStatus
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.url = try container.decode(URL.self, forKey: .url)
        self.commentBoardID = try container.decode(String.self, forKey: .commentBoardId)
        self.commentID = try container.decode(String.self, forKey: .commentId)
        self.comment = try container.decode(Comment.self, forKey: .comment)
        self.reportedBy = try container.decode(String.self, forKey: .reportedById)
        self.reportedAt = try container.decode(Date.self, forKey: .reportedAt)
        self.description = try container.decodeIfPresent(String.self, forKey: .description)
        self.reportStatus = try container.decode(CommentReportStatus.self, forKey: .reportStatus)
    }
}
