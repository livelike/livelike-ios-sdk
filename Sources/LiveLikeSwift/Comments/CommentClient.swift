//
//  CommentClient.swift
//  EngagementSDK
//
//  Created by Keval Shah on 14/12/22.
//

import Foundation
import LiveLikeCore

/// A client used for Comment queries
public protocol CommentClient {
    
    /// Adds a comment to the comment board
    ///
    /// - Parameters:
    ///   - text: A String typed parameter representing the text of the `Comment` to add
    ///   - completion: A callback indicating whether the comment was added or failed where `Comment` represents the comment
    /// - Returns: the comment added
    func addComment(
        text: String,
        completion: @escaping (Result<Comment, Error>) -> Void
    )
    
    /// Delete a comment added to the comment board
    ///
    /// - Parameters:
    ///   - commentID: A String typed parameter representing the id of the `Comment` object
    ///   - completion: A callback pf boolean that notifies if the comment was deleted or failed.
    ///
    func deleteComment(
        commentID: String,
        completion: @escaping (Result<Bool, Error>) -> Void
    )
    
    /// Edit a comment previously added to the `CommentBoard`
    ///
    /// - Parameters:
    ///   - commentID: A String typed parameter representing the id of the `Comment` object
    ///   - text: A String typed parameter representing the text of the `Comment` to update
    ///   - completion: A callback indicating whether the comment was updated or failed where `Comment` represents the comment
    ///
    func editComment(
        commentID: String,
        text: String,
        completion: @escaping (Result<Comment, Error>) -> Void
    )
    
    /// Adds a reply to a comment on the `Comment Board`
    ///
    /// - Parameters:
    ///   - parentCommentID: A String typed parameter representing the id of the `Comment` object to which the reply is to be added.
    ///   - text: A String typed parameter representing the text of the `Comment` to add
    ///   - completion: A callback indicating whether the comment was added or failed where `Comment` represents the comment
    func addCommentReply(
        parentCommentID: String,
        text: String,
        completion: @escaping (Result<Comment, Error>) -> Void
    )
    
    /// Returns a list of comments added to the `CommentBoard`
    ///
    /// - Parameters:
    ///   - page: a `Pagination` typed parameter passed to allow paginated responses
    ///   - options: a configurable request object used to query for Comments
    ///   - completion: A callback that receives the list of `Comment` objects for the comments that were posted
    ///
    func getComments(
        page: Pagination,
        options: GetCommentsRequestOptions?,
        completion: @escaping (Result<[Comment], Error>) -> Void
    )
    
    /// Returns a list of comments added to the `CommentBoard`
    ///
    /// - Parameters:
    ///   - page: a `Pagination` typed parameter passed to allow paginated responses
    ///   - options: a configurable request object used to query for Comments
    ///   - completion: A callback that receives the paginated result containing the list of `Comment` objects for the comments that were posted and the total comment count.
    ///
    func getCommentsPage(
        page: Pagination,
        options: GetCommentsRequestOptions?,
        completion: @escaping (Result<PaginatedResult<Comment>, Error>) -> Void
    )
    
    /// Returns a detailed object `Comment` of a specific comment in the `CommentBoard`
    ///
    /// - Parameters:
    ///   - commentID: Unique identifier that represents the specific `Comment`
    ///   - completion: A callback that receives the `Comment` object for the comment
    ///
    func getComment(
        commentID: String,
        completion: @escaping (Result<Comment, Error>) -> Void
    )
    
    /// Returns a list of replies for a comment added to the `CommentBoard`
    ///
    /// - Parameters:
    ///   - page: a `Pagination` typed parameter passed to allow paginated responses
    ///   - options: a `GetCommentRepliesRequestOptions` object containing query options for the request
    ///   - completion: A callback that receives the list of `Comment` objects for the comments that were posted
    ///
    func getCommentReplies(
        page: Pagination,
        options: GetCommentRepliesRequestOptions,
        completion: @escaping (Result<[Comment], Error>) -> Void
    )
    
    /// Create a report on a comment
    /// - Parameters:
    ///   - commentID: the unique identifier of the comment to be reported.
    ///   - options: an object of type `CreateCommentReportRequestOptions` containing more details of the report
    ///   - completion: A callback indicating whether the report was deleted or failed.
    func createCommentReport(
        commentID: String,
        options: CreateCommentReportRequestOptions?,
        completion: @escaping (Result<CommentReport, Error>) -> Void
    )
    
    /// Delete a report on a comment
    /// - Parameters:
    ///   - commentReportID: the unique identifier of the report to be deleted.
    ///   - completion: A callback indicating whether the report was deleted or failed.
    func deleteCommentReport(
        commentReportID: String,
        completion: @escaping (Result<Bool, Error>) -> Void
    )
    
    /// Dismiss a specific report on a comment
    /// - Parameters:
    ///   - commentReportID: the unique identifier of the comment report to be dismissed.
    ///   - completion: A callback indicating that the report was dismissed or pending.
    func dismissCommentReport(
        commentReportID: String,
        completion: @escaping (Result<CommentReportStatusDetail, Error>) -> Void
    )
    
    /// Dismiss all reports on a comment
    /// - Parameters:
    ///   - commentID: the unique identifier of the comment to be reported.
    ///   - completion: A callback indicating whether all the reports were dismissed or pending.
    func dismissAllCommentReports(
        commentID: String,
        completion: @escaping (Result<CommentReportStatusDetail, Error>) -> Void
    )
    
    /// Get a list of reported comments for a particular comment or comment board.
    /// - Parameters:
    ///   - page: a `Pagination` typed parameter passed to allow paginated responses
    ///   - options: An object of GetCommentReportsListRequestOptions which contains filters for the paginated response.
    ///   - completion: A callback consisting of a list of `CommentReport` representing the details of the reported comment.
    func getCommentReports(
        page: Pagination,
        options: GetCommentReportsListRequestOptions?,
        completion: @escaping (Result<[CommentReport], Error>) -> Void
    )
    
    /// Moderation Tools: Get details of a specific comment report that was created.
    /// - Parameters:
    ///   - commentReportID: the unique identifier of the comment report for which the details are requested.
    ///   - completion: A callback containing`CommentReport` which represents the details of the ban.
    func getCommentReportDetails(
        commentReportID: String,
        completion: @escaping (Result<CommentReport, Error>) -> Void
    )
}

// MARK: - Internal
final class InternalCommentClient: CommentClient {

    private let whenAccessToken: Promise<AccessToken>
    private let commentBoardClient: CommentBoardClient
    private let commentsAPI: LiveLikeCommentAPIProtocol
    private var commentBoard: CommentBoard
    private var clientID: String
    private var commentsURL: URL
    private var commentDetailUrlTemplate: String
    private let networking: LLNetworking
    private let paginatedRequestController = PaginatedRequestController()
    
    struct CommentClientPaginationProgress {
        var next: URL?
        var previous: URL?
        var total: Int = 0
    }
    
    var commentClientPagination: CommentClientPaginationProgress
    private var clientPageURLRepo = [String: PaginatedResultURL]()
    
    init(
        whenAccessToken: Promise<AccessToken>,
        commentsAPI: LiveLikeCommentAPIProtocol,
        commentBoardClient: CommentBoardClient,
        commentBoard: CommentBoard,
        networking: LLNetworking
    ) {
        
        self.whenAccessToken = whenAccessToken
        self.commentClientPagination = CommentClientPaginationProgress()
        self.commentsAPI = commentsAPI
        self.commentBoardClient = commentBoardClient
        self.commentBoard = commentBoard
        self.commentsURL = commentBoard.commentsURL
        self.commentDetailUrlTemplate = commentBoard.commentDetailUrlTemplate
        self.clientID = commentBoard.clientID
        self.networking = networking
    }
    
    deinit {
        log.info("Comment Client has ended.")
    }
    
    func addComment(
        text: String,
        completion: @escaping (Result<Comment, Error>) -> Void
    ) {
        firstly {
            whenAccessToken
        }.then { accessToken in
            self.commentsAPI.addComment(
                commentsURL: self.commentsURL,
                commentBoardID: self.commentBoard.id,
                text: text,
                parentCommentID: nil,
                accessToken: accessToken
            )
        }.then { comment in
            completion(.success(comment))
        }.catch { error in
            completion(.failure(error))
        }
    }
    
    func deleteComment(
        commentID: String,
        completion: @escaping (Result<Bool, Error>) -> Void
    ) {
        firstly {
            whenAccessToken
        }.then { accessToken -> Promise<Bool> in
            self.commentsAPI.deleteCommentBoard(
                commentBoardURL: self.commentsURL.appendingPathComponent(
                    commentID,
                    isDirectory: true
                ),
                accessToken: accessToken
            )
        }.then { result in
            completion(.success(result))
        }.catch { error in
            log.error("Error deleting Comment: \(commentID): \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func editComment(
        commentID: String,
        text: String,
        completion: @escaping (Result<Comment, Error>) -> Void
    ) {
        firstly {
            whenAccessToken
        }.then { accessToken -> Promise<Comment> in
            self.commentsAPI.editComment(
                commentURL: self.commentsURL.appendingPathComponent(
                    commentID,
                    isDirectory: true
                ),
                text: text,
                accessToken: accessToken
            )
        }.then { comment in
            completion(.success(comment))
        }.catch { error in
            log.error("Error updating Comment: \(commentID): \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func addCommentReply(
        parentCommentID: String,
        text: String,
        completion: @escaping (Result<Comment, Error>) -> Void
    ) {
        firstly {
            whenAccessToken
        }.then { accessToken in
            self.commentsAPI.addComment(
                commentsURL: self.commentsURL,
                commentBoardID: self.commentBoard.id,
                text: text,
                parentCommentID: parentCommentID,
                accessToken: accessToken
            )
        }.then { comment in
            completion(.success(comment))
        }.catch { error in
            completion(.failure(error))
        }
    }
    
    func getComments(
        page: Pagination,
        options: GetCommentsRequestOptions? = nil,
        completion: @escaping (Result<[Comment], Error>) -> Void
    ) {
        return getCommentsPage(
            page: page,
            options: options
        ) {
            completion($0.map { $0.items})
        }
    }
    
    func getCommentsPage(
        page: Pagination,
        options: GetCommentsRequestOptions? = nil,
        completion: @escaping (Result<PaginatedResult<Comment>, Error>) -> Void
    ) {
        firstly {
            self.whenAccessToken
        }.then { accessToken in
            self.paginatedRequestController.performExecuteFull(
                paginatedRequest: GetCommentsRequest(
                    options: options,
                    commentBoardID: self.commentBoard.id,
                    accessToken: accessToken.asString,
                    networking: self.networking
                ),
                rootURL: self.commentsURL,
                page: page,
                completion: completion
            )
        }
    }
    
    func getCommentReplies(
        page: Pagination,
        options: GetCommentRepliesRequestOptions,
        completion: @escaping (Result<[Comment], Error>) -> Void
    ) {
        var pageResultURLID = "getCommentReplies"
        pageResultURLID += "_\(options.parentCommentID)"

        let whenCommentsListURL: Promise<URL> = Promise { fulfill, reject in
            switch page {
            case .first:
                
                // Reset result repo cache if the user is calling `.first` page
                self.clientPageURLRepo[pageResultURLID] = nil
                
                guard var commentsURLComponents = URLComponents(
                    url: self.commentsURL,
                    resolvingAgainstBaseURL: false
                ) else {
                    completion(
                        .failure(
                            CommentClientError.invalidCommentURL
                        )
                    )
                    return
                }

                commentsURLComponents.queryItems = options.urlQueries

                guard let commentsListURL = commentsURLComponents.url else {
                    completion(
                        .failure(
                            CommentClientError.invalidCommentURL
                        )
                    )
                    return
                }
                fulfill(commentsListURL)

            case .next:
                if let url = self.clientPageURLRepo[pageResultURLID]?.nextURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.nextPageUnavailable)
                }
            case .previous:
                if let url = self.clientPageURLRepo[pageResultURLID]?.previousURL {
                    fulfill(url)
                } else {
                    reject(PaginationErrors.previousPageUnavailable)
                }
            }
        }
        
        firstly {
            Promises.zip(
                whenCommentsListURL,
                whenAccessToken
            )
        }.then { url, accessToken in
            self.commentsAPI.getComments(
                commentsURL: url,
                accessToken: accessToken
            )
        }.then { paginatedComments in
            self.clientPageURLRepo[pageResultURLID] = PaginatedResultURL(
                nextURL: paginatedComments.next,
                previousURL: paginatedComments.previous
            )
            completion(.success(paginatedComments.items))
        }.catch { error in
            log.error("Failed getting list of Replies for Comment: \(options.parentCommentID)")
            completion(.failure(error))
        }
    }
    
    func getComment(commentID: String, completion: @escaping (Result<Comment, Error>) -> Void) {
        firstly {
            whenAccessToken
        }.then { accessToken in
            return GetCommentDetailsRequest(
                accessToken: accessToken.asString,
                networking: self.networking
            ).execute(url: try self.commentBoard.getCommentDetailURL(
                commentID: commentID
            ))
        }.then { comment in
            completion(.success(comment))
        }.catch { error in
            log.error("Failed to get detail of comment: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func createCommentReport(
        commentID: String,
        options: CreateCommentReportRequestOptions?,
        completion: @escaping (Result<CommentReport, Error>) -> Void
    ) {
        firstly {
            whenAccessToken
        }.then(on: DispatchQueue.global()) { accessToken in
            return CreateCommentReportRequest(
                commentID: commentID,
                options: options,
                accessToken: accessToken.asString,
                networking: self.networking
            ).execute(url: self.commentBoard.commentReportURL)
        }.then { commentReport in
            completion(.success(commentReport))
        }.catch { error in
            log.error(error.localizedDescription)
            completion(.failure(error))
        }
    }
    
    func deleteCommentReport(
        commentReportID: String,
        completion: @escaping (Result<Bool, Error>) -> Void
    ) {
        firstly {
            whenAccessToken
        }.then(on: DispatchQueue.global()) { accessToken in
            return DeleteCommentReportRequest(
                accessToken: accessToken.asString,
                networking: self.networking
            ).execute(url: self.commentBoard.commentReportURL.appendingPathComponent(
                commentReportID,
                isDirectory: true
            ))
        }.then { result in
            completion(.success(result))
        }.catch { error in
            log.error("Error deleting Comment Report: \(commentReportID): \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func dismissCommentReport(commentReportID: String, completion: @escaping (Result<CommentReportStatusDetail, Error>) -> Void) {
        firstly {
            whenAccessToken
        }.then(on: DispatchQueue.global()) { accessToken in
            return DismissCommentReportRequest(
                accessToken: accessToken.asString,
                networking: self.networking
            ).execute(url: self.commentBoard.commentReportURL.appendingPathComponent(
                commentReportID,
                isDirectory: true
            ))
        }.then { result in
            completion(.success(result))
        }.catch { error in
            log.error("Error dismissing Comment Report: \(commentReportID): \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func dismissAllCommentReports(commentID: String, completion: @escaping (Result<CommentReportStatusDetail, Error>) -> Void) {
        firstly {
            whenAccessToken
        }.then(on: DispatchQueue.global()) { accessToken in
            return DismissCommentReportRequest(
                accessToken: accessToken.asString,
                networking: self.networking
            ).execute(url: try self.commentBoard.getDismissReportedCommentURL(
                commentID: commentID)
            )
        }.then { result in
            completion(.success(result))
        }.catch { error in
            log.error("Error dismissing all comment reports: \(commentID): \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func getCommentReports(page: Pagination, options: GetCommentReportsListRequestOptions?, completion: @escaping (Result<[CommentReport], Error>) -> Void) {
        firstly {
            whenAccessToken
        }.then(on: DispatchQueue.global()) { accessToken in
            self.paginatedRequestController.performExecute(
                paginatedRequest: GetCommentReportsRequest(
                    options: options,
                    accessToken: accessToken.asString,
                    networking: self.networking,
                    clientID: self.clientID
                ),
                rootURL: self.commentBoard.commentReportURL,
                page: page,
                completion: completion
            )
        }
    }
    
    func getCommentReportDetails(commentReportID: String, completion: @escaping (Result<CommentReport, Error>) -> Void) {
        firstly {
            whenAccessToken
        }.then { accessToken in
            return GetCommentReportDetailsRequest(
                accessToken: accessToken.asString,
                networking: self.networking
            ).execute(url: try self.commentBoard.getCommentReportDetailURL(
                commentReportID: commentReportID
            ))
        }.then { commentReport in
            completion(.success(commentReport))
        }.catch { error in
            log.error("Failed to get detail of comment report: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
}
