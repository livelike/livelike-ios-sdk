//
//  GetCommentBoardBanDetailsRequest.swift
//  
//
//  Created by Keval Shah on 27/03/23.
//

import Foundation
import LiveLikeCore

struct GetCommentBoardBanDetailsRequest {

    private let accessToken: String
    private let networking: LLNetworking

    init(
        accessToken: String,
        networking: LLNetworking
    ) {
        self.accessToken = accessToken
        self.networking = networking
    }
}

extension GetCommentBoardBanDetailsRequest: LLRequest {
    typealias ResponseType = CommentBoardBanDetails

    func execute(
        url: URL,
        completion: @escaping (Result<CommentBoardBanDetails, Error>) -> Void
    ) {
        let resource = Resource<CommentBoardBanDetails>(
            get: url,
            accessToken: accessToken
        )
        networking.task(resource, completion: completion)
    }
}
