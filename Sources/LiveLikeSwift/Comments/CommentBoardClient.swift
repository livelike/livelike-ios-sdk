//
//  CommentBoardClient.swift
//  EngagementSDK
//
//  Created by Keval Shah on 04/12/22.
//

import Foundation
import LiveLikeCore

/// A client used for Comment Board queries
public protocol CommentBoardClient {
    
    /// Create a Comment Board
    /// - Parameters:
    ///   - createCommentBoardOptions: the object of CreateCommentBoardRequestOptions
    ///   - completion: A callback indicating whether the `CommentBoard` was created or failed where `CommentBoard` represents the created Comment Board
    func createCommentBoard(
        options: CreateCommentBoardRequestOptions,
        completion: @escaping (Result<CommentBoard, Error>) -> Void
    )
    
    /// Update a Comment Board
    /// - Parameters:
    ///   - updateCommentBoardOptions: the object of UpdateCommentBoardRequestOptions
    ///   - completion: A callback indicating whether the `CommentBoard` was updated or failed where the object represents the updated `CommentBoard`
    func updateCommentBoard(
        commentBoardID: String,
        options: UpdateCommentBoardRequestOptions,
        completion: @escaping (Result<CommentBoard, Error>) -> Void
    )
    
    /// Delete a Comment Board
    /// - Parameters:
    ///   - commentBoardID: the unique identifier of the `CommentBoard` to be deleted
    ///   - completion: A callback indicating whether the `CommentBoard` was deleted or failed
    func deleteCommentBoard(
        commentBoardID: String,
        completion: @escaping (Result<Bool, Error>) -> Void
    )
    
    /// Get Details of a specific Comment Board
    /// - Parameters:
    ///   - commentBoardID: the unique identifier of the `CommentBoard`
    ///   - completion: A callback indicating  the `CommentBoard` details were received or failed where the object represents the details  of `CommentBoard`
    func getCommentBoardDetails(
        commentBoardID: String,
        completion: @escaping (Result<CommentBoard, Error>) -> Void
    )
    
    /// Get List of Comment Boards
    /// - Parameters:
    ///   - page: a `Pagination` typed parameter passed to allow paginated responses
    func getCommentBoards(
        page: Pagination,
        completion: @escaping (Result<[CommentBoard], Error>) -> Void
    )
    
    /// Moderation Tools: Ban a user from a Comment Board
    /// - Parameters:
    ///   - profileID: the unique identifier of the user to be banned.
    ///   - options: An object of CommentBoardBanRequestOptions which contains details to add when banning a user
    ///   - completion: A callback indicating whether the user ban was created or failed where `CommentBoardBanDetails` represents the details of the ban.
    func createCommentBoardBan(
        profileID: String,
        options: CommentBoardBanRequestOptions?,
        completion: @escaping (Result<CommentBoardBanDetails, Error>) -> Void
    )
    
    /// Moderation Tools: Remove a ban on a user from a Comment Board
    /// - Parameters:
    ///   - commentBoardBanID: the unique identifier of the ban to be removed.
    ///   - completion: A callback indicating whether the user ban was removed or failed.
    func deleteCommentBoardBan(
        commentBoardBanID: String,
        completion: @escaping (Result<Bool, Error>) -> Void
    )
    
    /// Moderation Tools: Get a list of bans for a particular user or comment board.
    /// - Parameters:
    ///   - page: a `Pagination` typed parameter passed to allow paginated responses
    ///   - options: An object of GetCommentBoardBansListRequestOptions which contains filters for the paginated response.
    ///   - completion: A callback consisting of a list of `CommentBoardBanDetails` representing the details of the ban.
    func getCommentBoardBans(
        page: Pagination,
        options: GetCommentBoardBansListRequestOptions?,
        completion: @escaping (Result<[CommentBoardBanDetails], Error>) -> Void
    )
    
    /// Moderation Tools: Get details of a specific ban that was created.
    /// - Parameters:
    ///   - commentBoardBanID: the unique identifier of the ban for which the details are requested.
    ///   - completion: A callback containing`CommentBoardBanDetails` which represents the details of the ban.
    func getCommentBoardBanDetails(
        commentBoardBanID: String,
        completion: @escaping (Result<CommentBoardBanDetails, Error>) -> Void
    )
}

// MARK: - Internal
final class InternalCommentBoardClient: CommentBoardClient {
    
    private let whenAccessToken: Promise<AccessToken>
    private let commentsAPI: LiveLikeCommentAPIProtocol
    private let whenApplicationConfig: Promise<ApplicationConfiguration>
    private let networking: LLNetworking
    private let paginatedRequestController = PaginatedRequestController()
    
    struct CommentBoardClientPaginationProgress {
        var next: URL?
        var previous: URL?
        var total: Int = 0
    }
    
    private var commentBoardClientPagination: CommentBoardClientPaginationProgress
    private var clientPageURLRepo = [String: PaginatedResultURL]()
    
    init(
        whenApplicationConfig: Promise<ApplicationConfiguration>,
        whenAccessToken: Promise<AccessToken>,
        commentsAPI: LiveLikeCommentAPIProtocol,
        networking: LLNetworking
    ) {
        self.commentBoardClientPagination = CommentBoardClientPaginationProgress()
        self.whenAccessToken = whenAccessToken
        self.commentsAPI = commentsAPI
        self.whenApplicationConfig = whenApplicationConfig
        self.networking = networking
    }
    
    func createCommentBoard(
        options: CreateCommentBoardRequestOptions,
        completion: @escaping (Result<CommentBoard, Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                whenAccessToken,
                whenApplicationConfig
            )
        }.then { accessToken, applicationConfig -> Promise<CommentBoard> in
            self.commentsAPI.createCommentBoard(
                commentBoardURL: applicationConfig.commentBoardsUrl,
                options: options,
                accessToken: accessToken
            )
        }.then { commentBoard in
            completion(.success(commentBoard))
        }.catch { error in
            completion(.failure(error))
        }
    }
    
    func updateCommentBoard(
        commentBoardID: String,
        options: UpdateCommentBoardRequestOptions,
        completion: @escaping (Result<CommentBoard, Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                whenAccessToken,
                self.whenApplicationConfig
            )
        }.then { accessToken, app -> Promise<CommentBoard> in
            self.commentsAPI.updateCommentBoard(
                commentBoardsURL: try app.getCommentBoardDetailURL(
                    commentBoardID: commentBoardID
                ),
                options: options,
                accessToken: accessToken
            )
        }.then { commentBoard in
            completion(.success(commentBoard))
        }.catch { error in
            log.error("Error Updating Comment Board for: \(commentBoardID): \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func deleteCommentBoard(
        commentBoardID: String,
        completion: @escaping (Result<Bool, Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                whenAccessToken,
                whenApplicationConfig
            )
        }.then { accessToken, applicationConfig -> Promise<Bool> in
            self.commentsAPI.deleteCommentBoard(
                commentBoardURL: applicationConfig.commentBoardsUrl.appendingPathComponent(
                    commentBoardID,
                    isDirectory: true
                ),
                accessToken: accessToken
            )
        }.then { result in
            completion(.success(result))
        }.catch { error in
            log.error("Error deleting Comment Board: \(commentBoardID): \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func getCommentBoardDetails(
        commentBoardID: String,
        completion: @escaping (Result<CommentBoard, Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                self.whenApplicationConfig,
                whenAccessToken
            )
        }.then { app, accessToken in
            self.commentsAPI.getCommentBoardInfo(
                commentBoardURL: try app.getCommentBoardDetailURL(
                    commentBoardID: commentBoardID
                ),
                accessToken: accessToken
            )
        }.then { commentBoardInfo in
            completion(.success(commentBoardInfo))
        }.catch { error in
            log.error("Failed getting information of comment board: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func getCommentBoards(
        page: Pagination,
        completion: @escaping (Result<[CommentBoard], Error>) -> Void
    ) {
        var queryItems: [URLQueryItem] = []
        
        let pageResultURLID = "getCommentBoards"
        
        let commentBoardsListURL: Promise<URL> = Promise { fulfill, reject in
            firstly {
                self.whenApplicationConfig
            }.then { applicationConfig in
                switch page {
                case .first:
                    // Reset result repo cache if the user is calling `.first` page
                    self.clientPageURLRepo[pageResultURLID] = nil
                    queryItems = [
                        URLQueryItem(
                            name: CommentBoardQueryKeys.clientID.rawValue,
                            value: applicationConfig.clientId
                        )
                    ]
                    
                    if let url = applicationConfig.commentBoardsUrl.appending(queryItems: queryItems) {
                        fulfill(url)
                    }
                case .next:
                    if let url = self.clientPageURLRepo[pageResultURLID]?.nextURL {
                        fulfill(url)
                    } else {
                        reject(PaginationErrors.nextPageUnavailable)
                    }
                case .previous:
                    if let url = self.clientPageURLRepo[pageResultURLID]?.previousURL {
                        fulfill(url)
                    } else {
                        reject(PaginationErrors.previousPageUnavailable)
                    }
                }
            }
        }
        
        firstly {
            Promises.zip(
                commentBoardsListURL,
                whenAccessToken
            )
        }.then { url, accessToken in
            self.commentsAPI.getCommentBoards(
                url: url,
                accessToken: accessToken
            )
        }.then { paginatedCommentBoards in
            self.clientPageURLRepo[pageResultURLID] = PaginatedResultURL(
                nextURL: paginatedCommentBoards.next,
                previousURL: paginatedCommentBoards.previous
            )
            completion(.success(paginatedCommentBoards.items))
        }.catch { error in
            log.error("Failed getting list of comment boards: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func createCommentBoardBan(
        profileID: String,
        options: CommentBoardBanRequestOptions?,
        completion: @escaping (Result<CommentBoardBanDetails, Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                whenApplicationConfig,
                whenAccessToken
            )
        }.then(on: DispatchQueue.global()) { appConfig, accessToken in
            return CreateCommentBoardBanRequest(
                profileID: profileID,
                options: options,
                accessToken: accessToken.asString,
                networking: self.networking
            ).execute(url: appConfig.commentBoardBanUrl)
        }.then { commentBoardBan in
            completion(.success(commentBoardBan))
        }.catch { error in
            log.error(error.localizedDescription)
            completion(.failure(error))
        }
    }
    
    func deleteCommentBoardBan(
        commentBoardBanID: String,
        completion: @escaping (Result<Bool, Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                whenApplicationConfig,
                whenAccessToken
            )
        }.then(on: DispatchQueue.global()) { appConfig, accessToken in
            return DeleteCommentBoardBanRequest(
                accessToken: accessToken.asString,
                networking: self.networking
            ).execute(url: appConfig.commentBoardBanUrl.appendingPathComponent(
                commentBoardBanID,
                isDirectory: true
            ))
        }.then { result in
            completion(.success(result))
        }.catch { error in
            log.error("Error deleting Comment Board Ban: \(commentBoardBanID): \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    func getCommentBoardBans(
        page: Pagination,
        options: GetCommentBoardBansListRequestOptions? = nil,
        completion: @escaping (Result<[CommentBoardBanDetails], Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                whenApplicationConfig,
                whenAccessToken
            )
        }.then(on: DispatchQueue.global()) { appConfig, accessToken in
            self.paginatedRequestController.performExecute(
                paginatedRequest: GetCommentBoardBansRequest(
                    options: options,
                    accessToken: accessToken.asString,
                    networking: self.networking
                ),
                rootURL: appConfig.commentBoardBanUrl,
                page: page,
                completion: completion
            )
        }
    }
    
    func getCommentBoardBanDetails(
        commentBoardBanID: String,
        completion: @escaping (Result<CommentBoardBanDetails, Error>) -> Void
    ) {
        firstly {
            Promises.zip(
                self.whenApplicationConfig,
                whenAccessToken
            )
        }.then { applicationConfig, accessToken in
            return GetCommentBoardBanDetailsRequest(
                accessToken: accessToken.asString,
                networking: self.networking
            ).execute(url: try applicationConfig.getCommentBoardBanDetailURL(
                commentBoardBanID: commentBoardBanID
            ))
        }.then { commentBoardBanDetail in
            completion(.success(commentBoardBanDetail))
        }.catch { error in
            log.error("Failed to get detail of comment board ban: \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
}
