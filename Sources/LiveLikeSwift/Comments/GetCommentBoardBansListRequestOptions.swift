//
//  GetCommentBoardBansListRequestOptions.swift
//  
//
//  Created by Keval Shah on 09/03/23.
//

import Foundation

/// A configurable request object used to query for Comment Board Bans List
public struct GetCommentBoardBansListRequestOptions {

    let commentBoardID: String?
    let profileID: String?

    /// Initialize options to be used in the Comments Board Ban List queries
    public init(profileID: String?, commentBoardID: String?) {
        self.commentBoardID = commentBoardID
        self.profileID = profileID
    }
}

