//
//  CommentBoardBanDetails.swift
//  
//
//  Created by Keval Shah on 09/03/23.
//

import Foundation

// MARK: - Models

/// The `CommentBoardBanDetails` struct represents the  object when a user is banned from a Comment Board
public struct CommentBoardBanDetails {
    // MARK: - Public Properties
    /// The unique identifer of the Ban
    public let id: String
    /// The profile ID of the user banned from commenting
    public let profileID: String
    /// The unique identifier of the Comment Board the ban applies to.
    public let commentBoardID: String?
    /// The unique identifier of the user who issued the ban.
    public let bannedByID: String
    /// The String object describing the reason for the ban
    public let description: String?

    // MARK: - Internal Properties
    let url: URL
    let clientID: String
    let createdAt: Date

    init(
        id: String,
        url: URL,
        profileID: String,
        commentBoardID: String?,
        clientID: String,
        bannedByID: String,
        createdAt: Date,
        description: String?
    ) {
        self.id = id
        self.url = url
        self.profileID = profileID
        self.commentBoardID = commentBoardID
        self.clientID = clientID
        self.bannedByID = bannedByID
        self.createdAt = createdAt
        self.description = description
    }
}

extension CommentBoardBanDetails: Decodable {

    enum CodingKeys: String, CodingKey {
        case id
        case url
        case profileId
        case commentBoardId
        case clientId
        case bannedById
        case createdAt
        case description
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.url = try container.decode(URL.self, forKey: .url)
        self.profileID = try container.decode(String.self, forKey: .profileId)
        self.commentBoardID = try container.decodeIfPresent(String.self, forKey: .commentBoardId)
        self.clientID = try container.decode(String.self, forKey: .clientId)
        self.bannedByID = try container.decode(String.self, forKey: .bannedById)
        self.createdAt = try container.decode(Date.self, forKey: .createdAt)
        self.description = try container.decodeIfPresent(String.self, forKey: .description)
    }
}
