//
//  GetCommentsRequestOptions.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 12/28/22.
//

import Foundation

/// A configurable request object used to query for Comments
public struct GetCommentsRequestOptions {

    // Is a unique identifier used for pagination functionality
    let id: String

    // Is an array of `URLQueryItem` used to compile URL parameters
    let urlQueries: [URLQueryItem]

    private let orderBy: CommentsOrderByRequest
    private let repliedSince: Date?
    private let repliedUntil: Date?
    private let since: Date?
    private let until: Date?
    private let topLevel: Bool?
    private let withoutDeletedThread: Bool?
    private let reactionID: String?


    /// - Parameters:
    ///   - ordering: The ordering of comments
    ///   - topLevel: Set to true when you only want the top level comments in results
    ///   - repliedSince: Set the date from when you want replies since
    ///   - repliedUntil: Set the date from when you want replies until
    ///   - since: Set the date from when you want comments since
    ///   - until: Set the date from when you want comments until
    ///   - withoutDeletedThread: Set to true to exclude deleted threads
    ///   - reactionID: The reaction id to sort when using ordering `reactionCountAcsending` or `reactionCountDescending`
    public init(
        ordering: CommentsOrderByRequest,
        topLevel: Bool? = nil,
        repliedSince: Date? = nil,
        repliedUntil: Date? = nil,
        since: Date? = nil,
        until: Date? = nil,
        withoutDeletedThread: Bool? = nil,
        reactionID: String? = nil
    ) {
        self.orderBy = ordering
        self.repliedUntil = repliedUntil
        self.repliedSince = repliedSince
        self.since = since
        self.until = until
        self.topLevel = topLevel
        self.withoutDeletedThread = withoutDeletedThread
        self.reactionID = reactionID

        self.id = {

            var id: String = ""
            id += ordering.rawValue

            if let topLevel = topLevel {
                id += topLevel.description
            }

            if let repliedSince = repliedSince {
                id += DateFormatter.iso8601FullWithPeriod.string(from: repliedSince)
            }

            if let repliedUntil = repliedUntil {
                id += DateFormatter.iso8601FullWithPeriod.string(from: repliedUntil)
            }

            if let since = since {
                id += DateFormatter.iso8601FullWithPeriod.string(from: since)
            }

            if let until = until {
                id += DateFormatter.iso8601FullWithPeriod.string(from: until)
            }

            if let withoutDeletedThread = withoutDeletedThread {
                id += withoutDeletedThread.description
            }

            if let reactionID = reactionID {
                id += reactionID
            }

            return id
        }()

        self.urlQueries = {
            var urlQueries = [URLQueryItem]()

            urlQueries.append(URLQueryItem(name: "ordering", value: ordering.rawValue))

            if let topLevel = topLevel {
                urlQueries.append(URLQueryItem(name: "top_level", value: topLevel.description))
            }

            if let repliedSince = repliedSince {
                urlQueries.append(URLQueryItem(name: "replied_since", value: DateFormatter.iso8601FullWithPeriod.string(from: repliedSince)))
            }

            if let repliedUntil = repliedUntil {
                urlQueries.append(URLQueryItem(name: "replied_until", value: DateFormatter.iso8601FullWithPeriod.string(from: repliedUntil)))
            }

            if let since = since {
                urlQueries.append(URLQueryItem(name: "since", value: DateFormatter.iso8601FullWithPeriod.string(from: since)))
            }

            if let until = until {
                urlQueries.append(URLQueryItem(name: "until", value: DateFormatter.iso8601FullWithPeriod.string(from: until)))
            }

            if let withoutDeletedThread = withoutDeletedThread {
                urlQueries.append(
                    URLQueryItem(
                        name: "wihtout_deleted_thread",
                        value: withoutDeletedThread.description
                    )
                )
            }

            if let reactionID = reactionID {
                urlQueries.append(
                    URLQueryItem(
                        name: "reaction_id",
                        value: reactionID
                    )
                )
            }


            return urlQueries
        }()
    }

    @available(*, deprecated, message: "topLevel is now optional. Use init(ordering:topLevel:repliedSince:repliedUntil:since:until:) instead.")
    public init(
        orderBy: CommentsOrderByRequest,
        topLevel: Bool = false,
        repliedSince: Date? = nil,
        repliedUntil: Date? = nil,
        since: Date? = nil,
        until: Date? = nil
    ) {
        self.init(
            ordering: orderBy,
            topLevel: topLevel,
            repliedSince: repliedSince,
            repliedUntil: repliedUntil,
            since: since,
            until: until
        )
    }
}

/// Enables ordering of comments on the `getComments` interface
public enum CommentsOrderByRequest: String, Decodable, CaseIterable {
    /// Orders comments in descending order of created_at date
    case newest = "-created_at"
    /// Orders comments in ascending order of created_at date
    case oldest = "created_at"
    /// Orders comments in descending order of created_at date of replies
    case newestReplies = "-reply"
    /// Orders comments in ascending order of created_at date of replies
    case oldestReplies = "reply"
    /// Orders comments in ascending order of reaction counts. To use this, it is mandatory to set a `reactionID` in request options
    case reactionCountAscending = "reaction_count"
    /// Orders comments in descending order of reaction counts. To use this, it is mandatory to set a `reactionID` in request options
    case reactionCountDescending = "-reaction_count"
}
