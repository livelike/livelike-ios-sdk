//
//  File.swift
//  
//
//  Created by Keval Shah on 23/12/22.
//

import Foundation

/// A configurable request object used to create a Comment Board
/// - Parameters:
///     - `title`: the title of a `CommentBoard`
///     - `customID`: the custom id corresponding to the `CommentBoard`
///     - `allowComments`: Flag to toggle allowing comments on the `CommentBoard`
///     - `repliesDepth`: The limit of the nested replies to comments on the `CommentBoard`
public struct CreateCommentBoardRequestOptions {

    public let title: String?
    public let customIdentifier: String? // Stores an optional customID to support customID no longer being required by this request
    public let allowComments: Bool
    public let repliesDepth: Int
    public let customData: String?

    @available(*, deprecated, message: "`customID` is no longer required and will return an empty string if nil. Use `customIdentifier` instead.")
    public var customID: String {
        return customIdentifier ?? ""
    }

    @available(*, deprecated, message: "`customID` is not longer required for this request. Use init(allowComments:repliesDepth:) with optional customID instead.")
    public init(
        title: String?,
        customID: String,
        allowComments: Bool,
        repliesDepth: Int,
        customData: String?
    ) {
        self.title = title
        self.customIdentifier = nil
        self.allowComments = allowComments
        self.repliesDepth = repliesDepth
        self.customData = customData
    }

    public init(
        allowComments: Bool,
        repliesDepth: Int,
        title: String? = nil,
        customIdentifier: String? = nil,
        customData: String? = nil
    ) {
        self.title = title
        self.customIdentifier = customIdentifier
        self.allowComments = allowComments
        self.repliesDepth = repliesDepth
        self.customData = customData
    }
}
