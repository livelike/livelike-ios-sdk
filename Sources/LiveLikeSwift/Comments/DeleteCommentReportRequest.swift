//
//  DeleteCommentReportRequest.swift
//  
//
//  Created by Keval Shah on 12/04/23.
//

import Foundation
import LiveLikeCore

struct DeleteCommentReportRequest {
    
    private let accessToken: String
    private let networking: LLNetworking
    
    init(
        accessToken: String,
        networking: LLNetworking
    ) {
        self.accessToken = accessToken
        self.networking = networking
    }
    
}

extension DeleteCommentReportRequest: LLRequest {
    typealias ResponseType = Bool
    
    func execute(
        url: URL,
        completion: @escaping (Result<Bool, Error>) -> Void
    ) {
        
        let resource = Resource<Bool>.init(
            url: url,
            method: .delete(EmptyBody()),
            accessToken: accessToken
        )
        networking.task(resource, completion: completion)
    }
    
}
