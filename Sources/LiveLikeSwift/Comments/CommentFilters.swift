//
//  File.swift
//  
//
//  Created by Keval Shah on 29/08/23.
//

import Foundation

/// The filter type that has been applied to the Comment Board
public enum CommentFilter: String {
    /// Catch-all type for any kind of filtering
    case filtered

    /// Comment Text has been filtered for profanity
    case profanity

    /// No filters set
    case none
}

extension CommentFilter: Codable {
    enum CodingKeys: String, CodingKey {
        case filtered
        case profanity
        case none
    }
}
