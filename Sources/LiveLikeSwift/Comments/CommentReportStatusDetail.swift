//
//  CommentReportStatusDetail.swift
//  
//
//  Created by Keval Shah on 12/04/23.
//

import Foundation

/// The `CommentReportStatusDetail` struct represents the status of a Reported Comment
public struct CommentReportStatusDetail {

    /// The status of the report for the comment. Can be either pending or dismissed.
    public let reportStatus: CommentReportStatus?

    public let detail: String?

    init(
        reportStatus: CommentReportStatus?,
        detail: String?
    ) {
        self.reportStatus = reportStatus
        self.detail = detail
    }
}

extension CommentReportStatusDetail: Decodable {

    enum CodingKeys: String, CodingKey {
        case reportStatus
        case detail
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.reportStatus = try container.decodeIfPresent(CommentReportStatus.self, forKey: .reportStatus)
        self.detail = try container.decodeIfPresent(String.self, forKey: .detail)
    }
}

public enum CommentReportStatus: String, Codable, CaseIterable {
    case pending
    case dismissed
}
