//
//  GetCommentBoardBansRequest.swift
//  
//
//  Created by Keval Shah on 27/03/23.
//

import Foundation
import LiveLikeCore

struct GetCommentBoardBansRequest {

    private let options: GetCommentBoardBansListRequestOptions?
    private let accessToken: String
    private let networking: LLNetworking

    init(
        options: GetCommentBoardBansListRequestOptions?,
        accessToken: String,
        networking: LLNetworking
    ) {
        self.options = options
        self.accessToken = accessToken
        self.networking = networking
    }

    private func buildURL(baseURL: URL) throws -> URL {
        guard var components = URLComponents(url: baseURL, resolvingAgainstBaseURL: false) else {
            throw URLComponentError.failedToInitURLComponents(fromURL: baseURL)
        }

        var urlQueries = [URLQueryItem]()

        if let commentBoardID = options?.commentBoardID {
            urlQueries.append(URLQueryItem(name: "comment_board_id", value: commentBoardID))
        }

        if let profileID = options?.profileID {
            urlQueries.append(URLQueryItem(name: "profile_id", value: profileID))
        }

        components.queryItems = urlQueries

        guard let url = components.url else {
            throw URLComponentError.urlIsNil
        }

        return url
    }
}

extension GetCommentBoardBansRequest: LLRequest {
    typealias ResponseType = PaginatedResult<CommentBoardBanDetails>

    func execute(
        url: URL,
        completion: @escaping (Result<PaginatedResult<CommentBoardBanDetails>, Error>) -> Void
    ) {
        do {
            let resource = try Resource<PaginatedResult<CommentBoardBanDetails>>(
                get: buildURL(baseURL: url),
                accessToken: accessToken
            )
            networking.task(resource, completion: completion)
        } catch {
            completion(.failure(error))
        }
    }
}

extension GetCommentBoardBansRequest: LLPaginatedRequest {
    typealias ElementType = CommentBoardBanDetails

    var requestID: String {
        var id = "GetCommentBoardBansRequest"
        id += options?.profileID ?? ""
        id += options?.commentBoardID ?? ""
        return id
    }
}
