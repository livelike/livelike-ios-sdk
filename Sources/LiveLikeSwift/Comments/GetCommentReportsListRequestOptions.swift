//
//  GetCommentReportsListRequestOptions.swift
//  
//
//  Created by Keval Shah on 12/04/23.
//

import Foundation

/// A configurable request object used to query for Comment Reports List
public struct GetCommentReportsListRequestOptions {

    let commentBoardID: String?
    let commentID: String?
    let reportStatus: CommentReportStatus?


    /// Initialize options to be used in the Comments Reports List queries
    public init(commentBoardID: String?, commentID: String?, reportStatus: CommentReportStatus?) {
        self.commentBoardID = commentBoardID
        self.commentID = commentID
        self.reportStatus = reportStatus
    }
}
