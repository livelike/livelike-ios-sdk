//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//
	

import Foundation
import LiveLikeCore

struct GetCommentsRequest: LLRequest {
    
    private let options: GetCommentsRequestOptions?
    private let commentBoardID: String
    private let accessToken: String
    private let networking: LLNetworking
    
    typealias ResponseType = PaginatedResult<Comment>
    
    init(
        options: GetCommentsRequestOptions?,
        commentBoardID: String,
        accessToken: String,
        networking: LLNetworking
    ) {
        self.options = options
        self.commentBoardID = commentBoardID
        self.accessToken = accessToken
        self.networking = networking
    }
    
    func execute(
        url: URL,
        completion: @escaping (Result<ResponseType, Error>) -> Void
    ) {
        var queryItems: [URLQueryItem] = [
            URLQueryItem(
                name: CommentQueryKeys.commentBoardID.rawValue,
                value: self.commentBoardID
            )
        ]
        
        if let options = options {
            queryItems.append(contentsOf: options.urlQueries)
        }
        
        do {
            let url = try url.appendingQueryItems(queryItems)
            let resource = Resource<PaginatedResult<Comment>>.init(
                get: url,
                accessToken: accessToken
            )
            networking.task(resource, completion: completion)
        } catch {
            completion(.failure(error))
        }
    }
}

extension GetCommentsRequest: LLPaginatedRequest {
    typealias ElementType = Comment
    
    var requestID: String {
        var pageResultURLID = "getComments_\(self.commentBoardID)"

        // add params to the ID to uniqely identify a paginated call that has params
        if let options = options {
            pageResultURLID += "_\(options.id)"
        }
        
        return pageResultURLID
    }
}
