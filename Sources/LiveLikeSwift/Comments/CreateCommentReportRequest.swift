//
//  CreateCommentReportRequest.swift
//  
//
//  Created by Keval Shah on 12/04/23.
//

import Foundation
import LiveLikeCore

struct CreateCommentReportRequest {
    
    private let accessToken: String
    private let commentID: String
    private let networking: LLNetworking
    private let options: CreateCommentReportRequestOptions?
    
    init(
        commentID: String,
        options: CreateCommentReportRequestOptions?,
        accessToken: String,
        networking: LLNetworking
    ) {
        self.commentID = commentID
        self.accessToken = accessToken
        self.networking = networking
        self.options = options
    }
    
}

extension CreateCommentReportRequest: LLRequest {
    typealias ResponseType = CommentReport
    
    func execute(
        url: URL,
        completion: @escaping (Result<CommentReport, Error>) -> Void
    ) {
        struct Payload: Encodable {
            let commentId: String
            let description: String?
        }
        let payload = Payload(
            commentId: self.commentID,
            description: options?.description
        )
        
        let resource = Resource<CommentReport>.init(
            url: url,
            method: .post(payload),
            accessToken: accessToken
        )
        networking.task(resource, completion: completion)
    }
    
}
