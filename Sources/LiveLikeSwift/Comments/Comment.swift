//
//  Comment.swift
//  EngagementSDK
//
//  Created by Keval Shah on 04/01/23.
//

import Foundation

/// The `Comment` struct represents the Comment created by users in a `CommentBoard`
public struct Comment {
    // MARK: - Public Properties
    /// The unique identifer of the Comment
    public let id: String
    /// The unique identifier of the Parent Comment for a reply.
    public let parentCommentID: String?
    @available(*, deprecated, message: "The value of threadCommentID has changed and is now optional and will return an empty string if nil. Use getThreadCommentID() instead.")
    /// The unique identifier for the top level comment of the thread
    public var threadCommentID: String {
        return _threadCommentID ?? ""
    }
    private let _threadCommentID: String?
    /// The text of the comment.
    public let text: String
    /// The limit for the nested comment depth of the Comment Board
    public let commentDepth: Int
    ///  The custom data that can be attached to a comment
    public let customData: String?
    /// The unique identifier of the author of the comment.
    public let authorID: String
    /// The user profile of the author of the comment.
    public let author: ProfileResource
    /// The profile Image URl of the author
    public let authorImageURL: URL?

    public let createdAt: Date
    /// Boolean value to determine if the comment has been deleted from the Comment Board.
    public let isDeleted: Bool
    /// Boolean value to determine if the comment has been reported by a user
    public let isReported: Bool
    /// Integer value that represents the count of the number of times the comment has been reported.
    public let commentReportsCount: Int
    /// Integer value that represents the count of the number of replies for the comment.
    public let commentRepliesCount: Int
    /// The comment Text after it has been filtered.
    public var filteredText: String?
    /// The reason(s) why the comment text was filtered.
    public var contentFilter: Set<CommentFilter>

    public func getThreadCommentID() -> String? {
        return _threadCommentID
    }

    // MARK: Internal Properties
    internal let repliesURL: URL
    internal let dismissReportedCommentURL: URL
    internal let url: URL

    init(
        id: String,
        url: URL,
        parentCommentID: String?,
        threadCommentID: String,
        text: String,
        commentDepth: Int,
        customData: String?,
        authorID: String,
        author: ProfileResource,
        authorImageURL: URL?,
        createdAt: Date,
        repliesURL: URL,
        commentRepliesCount: Int,
        isDeleted: Bool,
        isReported: Bool,
        commentReportsCount: Int,
        dismissReportedCommentURL: URL,
        filteredText: String?,
        contentFilter: Set<CommentFilter>
    ) {
        self.id = id
        self.url = url
        self.parentCommentID = parentCommentID
        self._threadCommentID = threadCommentID
        self.text = text
        self.commentDepth = commentDepth
        self.customData = customData
        self.authorID = authorID
        self.createdAt = createdAt
        self.repliesURL = repliesURL
        self.commentRepliesCount = commentRepliesCount
        self.isDeleted = isDeleted
        self.isReported = isReported
        self.commentReportsCount = commentReportsCount
        self.dismissReportedCommentURL = dismissReportedCommentURL
        self.author = author
        self.authorImageURL = authorImageURL
        self.filteredText = filteredText
        self.contentFilter = contentFilter
    }
}

extension Comment: Decodable {

    enum CodingKeys: String, CodingKey {
        case id
        case url
        case parentCommentId
        case threadCommentId
        case text
        case commentDepth
        case customData
        case authorId
        case author
        case authorImageUrl
        case createdAt
        case repliesUrl
        case repliesCount
        case isDeleted
        case isReported
        case commentReportsCount
        case dismissReportedCommentUrl
        case filteredText
        case contentFilter
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.url = try container.decode(URL.self, forKey: .url)
        self.parentCommentID = try container.decodeIfPresent(String.self, forKey: .parentCommentId)
        self._threadCommentID = try container.decodeIfPresent(String.self, forKey: .threadCommentId)
        self.text = try container.decode(String.self, forKey: .text)
        self.commentDepth = try container.decode(Int.self, forKey: .commentDepth)
        self.customData = try container.decodeIfPresent(String.self, forKey: .customData)
        self.createdAt = try container.decode(Date.self, forKey: .createdAt)
        self.repliesURL = try container.decode(URL.self, forKey: .repliesUrl)
        self.commentRepliesCount = try container.decode(Int.self, forKey: .repliesCount)
        self.isDeleted = try container.decode(Bool.self, forKey: .isDeleted)
        self.isReported = try container.decode(Bool.self, forKey: .isReported)
        self.commentReportsCount = try container.decode(Int.self, forKey: .commentReportsCount)
        self.dismissReportedCommentURL = try container.decode(URL.self, forKey: .dismissReportedCommentUrl)
        self.authorImageURL = try container.decodeIfPresent(URL.self, forKey: .authorImageUrl)
        self.author = try container.decode(ProfileResource.self, forKey: .author)
        self.authorID = self.author.id
        self.filteredText = try container.decodeIfPresent(String.self, forKey: .filteredText)
        let filters = try container.decodeIfPresent([CommentFilter].self, forKey: .contentFilter) ?? []
        self.contentFilter = Set(filters)
    }
}
