//
//  GetCommentReportsRequest.swift
//  
//
//  Created by Keval Shah on 13/04/23.
//

import Foundation
import LiveLikeCore

struct GetCommentReportsRequest {

    private let options: GetCommentReportsListRequestOptions?
    private let accessToken: String
    private let networking: LLNetworking
    private let clientID: String

    init(
        options: GetCommentReportsListRequestOptions?,
        accessToken: String,
        networking: LLNetworking,
        clientID: String
    ) {
        self.options = options
        self.accessToken = accessToken
        self.networking = networking
        self.clientID = clientID
    }

    private func buildURL(baseURL: URL) throws -> URL {
        guard var components = URLComponents(url: baseURL, resolvingAgainstBaseURL: false) else {
            throw URLComponentError.failedToInitURLComponents(fromURL: baseURL)
        }

        var urlQueries = [URLQueryItem]()

        urlQueries.append(URLQueryItem(name: "client_id", value: self.clientID))

        if let commentBoardID = options?.commentBoardID {
            urlQueries.append(URLQueryItem(name: "comment_board_id", value: commentBoardID))
        }

        if let commentID = options?.commentID {
            urlQueries.append(URLQueryItem(name: "comment_id", value: commentID))
        }

        if let reportStatus = options?.reportStatus {
            urlQueries.append(URLQueryItem(name: "report_status", value: reportStatus.rawValue))
        }

        components.queryItems = urlQueries

        guard let url = components.url else {
            throw URLComponentError.urlIsNil
        }

        return url
    }
}

extension GetCommentReportsRequest: LLRequest {
    typealias ResponseType = PaginatedResult<CommentReport>

    func execute(
        url: URL,
        completion: @escaping (Result<PaginatedResult<CommentReport>, Error>) -> Void
    ) {
        do {
            let resource = try Resource<PaginatedResult<CommentReport>>(
                get: buildURL(baseURL: url),
                accessToken: accessToken
            )
            networking.task(resource, completion: completion)
        } catch {
            completion(.failure(error))
        }
    }
}

extension GetCommentReportsRequest: LLPaginatedRequest {
    typealias ElementType = CommentReport

    var requestID: String {
        var id = "GetCommentReportsRequest"
        id += self.clientID
        id += options?.commentID ?? ""
        id += options?.commentBoardID ?? ""
        id += options?.reportStatus?.rawValue ?? ""
        return id
    }
}

