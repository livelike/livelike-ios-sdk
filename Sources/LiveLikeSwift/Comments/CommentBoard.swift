//
//  CommentBoard.swift
//  EngagementSDK
//
//  Created by Keval Shah on 04/12/22.
//

import Foundation

// MARK: - Models

/// The `CommentBoard` struct represents the Comment Board where users can create comments and threads
public struct CommentBoard {
    // MARK: - Public Properties
    /// The unique identifer of the Board
    public let id: String
    /// The title of the Comment Board
    public let title: String?
    /// The custom unique identifier that can be attached to a Comment Board.
    public let customIdentifier: String?
    /// The boolean flag to determine if users can comment on the Comment Board
    public let allowComments: Bool
    /// The limit for the nested comment depth of the Comment Board
    public let repliesDepth: Int
    /// The String type parameter that can be used to attach any type of custom data to a Comment Board
    public let customData: String?

    /// The custom unique identifier that can be attached to a Comment Board.
    @available(*, deprecated, message: "`customID` is not longer required and will return an empty string if nil. Use `customIdentifier` instead.")
    public var customID: String {
        return customIdentifier ?? ""
    }

    // MARK: - Internal Properties
    let url: URL
    let clientID: String
    let createdBy: String
    let createdAt: Date
    let commentsURL: URL
    let commentDetailUrlTemplate: String
    let commentReportURL: URL
    let commentReportDetailUrlTemplate: String
    let dismissReportedCommentUrlTemplate: String

    init(
        id: String,
        url: URL,
        title: String?,
        customID: String?,
        clientID: String,
        allowComments: Bool,
        repliesDepth: Int,
        createdBy: String,
        createdAt: Date,
        commentsURL: URL,
        commentDetailUrlTemplate: String,
        customData: String?,
        commentReportURL: URL,
        commentReportDetailUrlTemplate: String,
        dismissReportedCommentUrlTemplate: String
    ) {
        self.id = id
        self.url = url
        self.title = title
        self.customIdentifier = customID
        self.clientID = clientID
        self.allowComments = allowComments
        self.repliesDepth = repliesDepth
        self.createdBy = createdBy
        self.createdAt = createdAt
        self.commentsURL = commentsURL
        self.commentDetailUrlTemplate = commentDetailUrlTemplate
        self.customData = customData
        self.commentReportURL = commentReportURL
        self.commentReportDetailUrlTemplate = commentReportDetailUrlTemplate
        self.dismissReportedCommentUrlTemplate = dismissReportedCommentUrlTemplate
    }
}

extension CommentBoard: Decodable {

    enum CodingKeys: String, CodingKey {
        case id
        case url
        case title
        case customId
        case clientId
        case allowComments
        case repliesDepth
        case createdById
        case createdAt
        case commentsUrl
        case commentDetailUrlTemplate
        case customData
        case commentReportUrl
        case commentReportDetailUrlTemplate
        case dismissReportedCommentUrlTemplate
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.url = try container.decode(URL.self, forKey: .url)
        self.title = try container.decodeIfPresent(String.self, forKey: .title)
        self.customIdentifier = try container.decodeIfPresent(String.self, forKey: .customId)
        self.clientID = try container.decode(String.self, forKey: .clientId)
        self.allowComments = try container.decode(Bool.self, forKey: .allowComments)
        self.repliesDepth = try container.decode(Int.self, forKey: .repliesDepth)
        self.createdBy = try container.decode(String.self, forKey: .createdById)
        self.createdAt = try container.decode(Date.self, forKey: .createdAt)
        self.commentsURL = try container.decode(URL.self, forKey: .commentsUrl)
        self.commentDetailUrlTemplate = try container.decode(String.self, forKey: .commentDetailUrlTemplate)
        self.customData = try container.decodeIfPresent(String.self, forKey: .customData)
        self.commentReportURL = try container.decode(URL.self, forKey: .commentReportUrl)
        self.commentReportDetailUrlTemplate = try container.decode(String.self, forKey: .commentReportDetailUrlTemplate)
        self.dismissReportedCommentUrlTemplate = try container.decode(String.self, forKey: .dismissReportedCommentUrlTemplate)
    }
}

extension CommentBoard {
    func getDismissReportedCommentURL(commentID: String) throws -> URL {
        return try TemplatedURLHelper.buildURL(
            templatedURLString: self.dismissReportedCommentUrlTemplate,
            templateKey: "{comment_id}",
            replacementString: commentID
        )
    }

    func getCommentReportDetailURL(commentReportID: String) throws -> URL {
        return try TemplatedURLHelper.buildURL(
            templatedURLString: self.commentReportDetailUrlTemplate,
            templateKey: "{comment_report_id}",
            replacementString: commentReportID
        )
    }

    func getCommentDetailURL(commentID: String) throws -> URL {
        return try TemplatedURLHelper.buildURL(
            templatedURLString: self.commentDetailUrlTemplate,
            templateKey: "{comment_id}",
            replacementString: commentID
        )
    }
}
