//
//  GetCommentReportDetailsRequest.swift
//  
//
//  Created by Keval Shah on 13/04/23.
//

import Foundation
import LiveLikeCore

struct GetCommentReportDetailsRequest {

    private let accessToken: String
    private let networking: LLNetworking

    init(
        accessToken: String,
        networking: LLNetworking
    ) {
        self.accessToken = accessToken
        self.networking = networking
    }
}

extension GetCommentReportDetailsRequest: LLRequest {
    typealias ResponseType = CommentReport

    func execute(
        url: URL,
        completion: @escaping (Result<CommentReport, Error>) -> Void
    ) {
        let resource = Resource<CommentReport>(
            get: url,
            accessToken: accessToken
        )
        networking.task(resource, completion: completion)
    }
}
