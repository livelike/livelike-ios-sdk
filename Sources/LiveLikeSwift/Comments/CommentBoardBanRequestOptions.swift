//
//  CommentBoardBanRequestOptions.swift
//  
//
//  Created by Keval Shah on 09/03/23.
//

import Foundation

/// A configurable request object used to create a Comment Board Ban
public struct CommentBoardBanRequestOptions {

    /// The unique identifier corresponding to the `CommentBoard`
    public let commentBoardID: String?
    /// String parameter representing the reason for the ban
    public let description: String?

    public init(
        commentBoardID: String?,
        description: String?
    ) {
        self.commentBoardID = commentBoardID
        self.description = description
    }
}
