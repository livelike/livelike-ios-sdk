//
//  DismissCommentReportRequest.swift
//  
//
//  Created by Keval Shah on 13/04/23.
//

import Foundation
import LiveLikeCore

struct DismissCommentReportRequest {
    
    private let accessToken: String
    private let networking: LLNetworking
    
    init(
        accessToken: String,
        networking: LLNetworking
    ) {
        self.accessToken = accessToken
        self.networking = networking
    }
    
}

extension DismissCommentReportRequest: LLRequest {
    typealias ResponseType = CommentReportStatusDetail
    
    func execute(
        url: URL,
        completion: @escaping (Result<CommentReportStatusDetail, Error>) -> Void
    ) {
        struct Payload: Encodable {
            var reportStatus: CommentReportStatus = .dismissed
        }
        
        let resource = Resource<CommentReportStatusDetail>.init(
            url: url,
            method: .patch(Payload()),
            accessToken: accessToken
        )
        networking.task(resource, completion: completion)
    }
    
}
