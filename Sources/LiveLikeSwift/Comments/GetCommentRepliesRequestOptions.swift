//
//  GetCommentRepliesRequestOptions.swift
//  EngagementSDK
//
//  Created by Keval Shah on 23/12/22.
//

import Foundation

/// A configurable request object used to query for Comment Replies List
public struct GetCommentRepliesRequestOptions {

    // Is an array of `URLQueryItem` used to compile URL parameters
    let urlQueries: [URLQueryItem]

    let commentBoardID: String
    let parentCommentID: String

    let ordering: CommentsOrderByRequest?
    let reactionID: String?

    /// Initialize options to be used in the Comments List queries
    /// - Parameters:
    ///   - commentBoardID: The associated comment board id
    ///   - parentCommentID: The parent comments id of which you want replies to
    ///   - ordering: The ordering of replies
    ///   - reactionID: The reaction id to order by when using ordering `reactionCountAscending` or `reactionCountDescending`
    public init(
        commentBoardID: String,
        parentCommentID: String,
        ordering: CommentsOrderByRequest? = nil,
        reactionID: String? = nil
    ) {
        self.commentBoardID = commentBoardID
        self.parentCommentID = parentCommentID
        self.ordering = ordering
        self.reactionID = reactionID

        self.urlQueries = {
            var urlQueries = [URLQueryItem]()

            urlQueries.append(
                URLQueryItem(
                    name: CommentQueryKeys.commentBoardID.rawValue,
                    value: commentBoardID
                )
            )

            urlQueries.append(
                URLQueryItem(
                    name: CommentQueryKeys.parentCommentID.rawValue,
                    value: parentCommentID
                )
            )

            if let ordering = ordering {
                urlQueries.append(
                    URLQueryItem(
                        name: "ordering", value: ordering.rawValue
                    )
                )
            }

            if let reactionID = reactionID {
                urlQueries.append(
                    URLQueryItem(
                        name: "reaction_id",
                        value: reactionID
                    )
                )
            }


            return urlQueries
        }()
    }
}
