//
//  DeleteCommentBoardBanRequest.swift
//  
//
//  Created by Keval Shah on 27/03/23.
//

import Foundation
import LiveLikeCore

struct DeleteCommentBoardBanRequest {
    
    private let accessToken: String
    private let networking: LLNetworking
    
    init(
        accessToken: String,
        networking: LLNetworking
    ) {
        self.accessToken = accessToken
        self.networking = networking
    }
    
}

extension DeleteCommentBoardBanRequest: LLRequest {
    typealias ResponseType = Bool
    
    func execute(
        url: URL,
        completion: @escaping (Result<Bool, Error>) -> Void
    ) {
        
        let resource = Resource<Bool>.init(
            url: url,
            method: .delete(EmptyBody()),
            accessToken: accessToken
        )
        networking.task(resource, completion: completion)
    }
    
}
