//
//  GetCommentDetailsRequest.swift
//  
//
//  Created by Keval Shah on 13/04/23.
//

import Foundation
import LiveLikeCore

struct GetCommentDetailsRequest {

    private let accessToken: String
    private let networking: LLNetworking

    init(
        accessToken: String,
        networking: LLNetworking
    ) {
        self.accessToken = accessToken
        self.networking = networking
    }
}

extension GetCommentDetailsRequest: LLRequest {
    typealias ResponseType = Comment

    func execute(
        url: URL,
        completion: @escaping (Result<Comment, Error>) -> Void
    ) {
        let resource = Resource<Comment>(
            get: url,
            accessToken: accessToken
        )
        networking.task(resource, completion: completion)
    }
}
