//
//  CreateCommentReportRequestOptions.swift
//  
//
//  Created by Keval Shah on 12/04/23.
//

import Foundation

/// A configurable request object used to create a Comment Board Ban
public struct CreateCommentReportRequestOptions {

    /// String parameter representing the reason for the ban
    public let description: String?

    public init(
        description: String?
    ) {
        self.description = description
    }
}
