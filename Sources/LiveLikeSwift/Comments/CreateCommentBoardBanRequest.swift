//
//  CreateCommentBoardBanRequest.swift
//  
//
//  Created by Keval Shah on 27/03/23.
//

import Foundation
import LiveLikeCore

struct CreateCommentBoardBanRequest {

    private let accessToken: String
    private let profileID: String
    private let networking: LLNetworking
    private let options: CommentBoardBanRequestOptions?

    init(
        profileID: String,
        options: CommentBoardBanRequestOptions?,
        accessToken: String,
        networking: LLNetworking
    ) {
        self.profileID = profileID
        self.accessToken = accessToken
        self.networking = networking
        self.options = options
    }
}

extension CreateCommentBoardBanRequest: LLRequest {
    typealias ResponseType = CommentBoardBanDetails

    func execute(
        url: URL,
        completion: @escaping (Result<CommentBoardBanDetails, Error>) -> Void
    ) {
        struct Payload: Encodable {
            let profileId: String
            let commentBoardId: String?
            let description: String?
        }
        let payload = Payload(
            profileId: self.profileID,
            commentBoardId: options?.commentBoardID,
            description: options?.description
        )

        let resource = Resource<CommentBoardBanDetails>.init(
            url: url,
            method: .post(payload),
            accessToken: accessToken
        )
        networking.task(resource, completion: completion)
    }
}
