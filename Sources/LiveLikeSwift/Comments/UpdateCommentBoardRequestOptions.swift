//
//  File.swift
//  
//
//  Created by Keval Shah on 23/12/22.
//

import Foundation

/// A configurable request object used to update a Comment Board
/// - Parameters:
///     - `customID`: the custom id corresponding to the `CommentBoard`
///     - `allowComments`: Flag to toggle allowing comments on the `CommentBoard`
///     - `repliesDepth`: The limit of the nested replies to comments on the `CommentBoard`
///     - `customData`: A string value used for custom data associated to the `CommentBoard`
///     - `title`: The title given to the `CommentBoard`
public struct UpdateCommentBoardRequestOptions {

    public let title: String?
    public let customID: String?
    public let allowComments: Bool?
    public let repliesDepth: Int?
    public let customData: String?

    public init(
        title: String? = nil,
        customID: String? = nil,
        allowComments: Bool? = nil,
        repliesDepth: Int? = nil,
        customData: String? = nil
    ) {
        self.title = title
        self.customID = customID
        self.allowComments = allowComments
        self.repliesDepth = repliesDepth
        self.customData = customData
    }
}
