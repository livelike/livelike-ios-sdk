//
//  PresenceService.swift
//  
//
//  Created by Mike Moloksher on 3/1/23.
//

import Foundation
import LiveLikeCore
import PubNub

private class ChannelOccupantsCache {

    private var cachedOccupants: [String: Set<String>]

    init(occupants: [String: Set<String>] = [:]) {
        self.cachedOccupants = occupants
    }

    func set(occupants: Set<String>, in channel: String) {
        self.cachedOccupants[channel] = occupants
    }

    func getOccupants(in channel: String) -> Set<String> {
        return cachedOccupants[channel, default: Set()]
    }

}

struct PresenceServiceConfig {
    let subscribeKey: String
    let userID: String
    let origin: String?
    let pubnubHeartbeatInterval: Int
    let pubnubPresenceTimeout: Int
}

enum PresenceServiceError: Error {
    case pubnubNotAvailable

    var errorDescription: String? {
        switch self {
        case .pubnubNotAvailable:
            return "PubNub is not initialized for this application.\nShould you require Presence as a Service functionality please contact LiveLike customer support"
        }
    }
}

class PubNubPresenceService {

    private let pubnub: PubNub
    private let listener = SubscriptionListener(
        queue: DispatchQueue(label: "com.livelike.pubnub.presence")
    )
    private let occupantsCache = ChannelOccupantsCache()

    weak var delegate: PresenceServiceDelegate?

    init(configuration: PresenceServiceConfig) {
        var config = PubNubConfiguration(
            publishKey: nil,
            subscribeKey: configuration.subscribeKey,
            userId: configuration.userID
        )

        if let origin = configuration.origin {
            config.origin = origin
        }

        // Configure Presence heartbeat
        config.heartbeatInterval = UInt(configuration.pubnubHeartbeatInterval)
        config.durationUntilTimeout = UInt(configuration.pubnubPresenceTimeout)

        config.automaticRetry = AutomaticRetry(
            retryLimit: 9,
            policy: .exponential(minDelay: 3, maxDelay: 60)
        )

        config.filterExpression = "!(content_filter LIKE '*filtered*') || sender_id == '\(configuration.userID)'"

        pubnub = PubNub(configuration: config)

        listener.didReceivePresence = { [weak self] event in
            guard let self = self else { return }
            if event.refreshHereNow {
                self.handleHereNowRefresh(event: event)
            } else {
                self.handleAnnouncementModeEvent(event: event)
            }
        }

        pubnub.add(listener)
    }

    deinit {
        pubnub.disconnect()
    }

    private func getPresenceChannels(from channels: [String]) -> [String] {
        return channels.map { "\($0)-pnpres" }
    }

    private func handleHereNowRefresh(
        event: PubNubPresenceChange
    ) {
        self.pubnub.hereNow(on: [event.channel]) { result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let presenceByChannel):
                guard let hereNowOccupants = presenceByChannel[event.channel]?.occupants else {
                    return
                }
                // calculate presence events
                let events: [PresenceEvent] = {
                    let cachedOccupants = self.occupantsCache.getOccupants(in: event.channel)
                    let diff = cachedOccupants.symmetricDifference(hereNowOccupants)
                    let leaves = diff.intersection(cachedOccupants).map { userID in
                        PresenceEvent(
                            userID: userID,
                            channel: event.channel,
                            attributes: nil,
                            action: .leave
                        )
                    }
                    let joins = diff.intersection(hereNowOccupants).map { userID in
                        PresenceEvent(
                            userID: userID,
                            channel: event.channel,
                            attributes: nil,
                            action: .join
                        )
                    }
                    return Set(leaves + joins).allObjects
                }()

                // update cache
                self.occupantsCache.set(
                    occupants: Set(hereNowOccupants),
                    in: event.channel
                )

                // notify delegate
                self.delegate?.presenceService(
                    self,
                    didReceivePresenceEvents: events
                )
            }
        }
    }

    private func handleAnnouncementModeEvent(
        event: PubNubPresenceChange
    ) {
        // calculate presence events
        var presenceEvents = [PresenceEvent]()
        event.actions.forEach { action in
            switch action {

            case .join(let uuids):
                uuids.forEach { uuid in
                    presenceEvents.append(
                        PresenceEvent(
                            userID: uuid,
                            channel: event.channel,
                            attributes: nil,
                            action: .join
                        )
                    )
                }
            case .leave(let uuids):
                uuids.forEach { uuid in
                    presenceEvents.append(
                        PresenceEvent(
                            userID: uuid,
                            channel: event.channel,
                            attributes: nil,
                            action: .leave
                        )
                    )
                }
            case .timeout(let uuids):
                uuids.forEach { uuid in
                    presenceEvents.append(
                        PresenceEvent(
                            userID: uuid,
                            channel: event.channel,
                            attributes: nil,
                            action: .timeout
                        )
                    )
                }
            case .stateChange(let uuid, let state):
                do {
                    let state = try state.decode([String: JSONCodableScalarType].self)
                    presenceEvents.append(
                        PresenceEvent(
                            userID: uuid,
                            channel: event.channel,
                            attributes: state.mapValues {
                                return $0.stringOptional
                            },
                            action: .attributesUpdate
                        )
                    )
                } catch {
                    return
                }
            }
        }

        // Apply joins and leaves to current cached occupants and update cache
        let currentUserIDs = self.occupantsCache.getOccupants(in: event.channel)
        let joins = presenceEvents.filter { $0.action == .join }.map { $0.userID }
        let leaves = presenceEvents.filter { $0.action == .leave }.map { $0.userID }
        self.occupantsCache.set(
            occupants: currentUserIDs.union(joins).subtracting(leaves),
            in: event.channel
        )

        // Notify delegate
        self.delegate?.presenceService(
            self,
            didReceivePresenceEvents: presenceEvents
        )
    }
}

extension PubNubPresenceService: PresenceService {

    func setDelegate(_ delegate: PresenceServiceDelegate?) {
        self.delegate = delegate
    }

    func subscribe(to channels: [String]) {
        pubnub.subscribe(to: getPresenceChannels(from: channels))
        // call hereNow to initialize cache
        hereNow(in: channels) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                print(error)
            case .success(let occupantsByChannel):
                occupantsByChannel.forEach { kvp in
                    self.occupantsCache.set(
                        occupants: Set(kvp.value),
                        in: kvp.key
                    )
                }

            }
        }
    }

    func unsubscribe(from channels: [String]) {
        pubnub.unsubscribe(from: getPresenceChannels(from: channels))
    }

    func joinChannels(_ channels: [String]) {
        pubnub.subscribe(to: channels)
    }

    func leaveChannels(_ channels: [String]) {
        pubnub.unsubscribe(from: channels)
    }

    func setAttributes(
        for channels: [String],
        with attributes: PresenceClientAttributes,
        completion: @escaping (Result<PresenceClientAttributes, Error>) -> Void
    ) {
        let state = attributes.mapValues { attribute in
            return JSONCodableScalarType(stringValue: attribute)
        }
        pubnub.setPresence(
            state: state,
            on: channels
        ) { result in
            switch result {
            case let .success(response):
                do {
                    let state = try response.decode([String: JSONCodableScalarType].self)
                    let attributes: PresenceClientAttributes = state.mapValues {
                        return $0.stringOptional
                    }
                    completion(.success(attributes))
                } catch {
                    completion(.failure(error))
                }
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }

    func getAttributes(
        for profileID: String,
        on channels: [String],
        completion: @escaping (Result<[String: PresenceClientAttributes], Error>) -> Void
    ) {
        pubnub.getPresenceState(
            for: profileID,
            on: channels
        ) { result in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let (_, stateByChannel)):
                let attributesByChannel: [String: PresenceClientAttributes] = stateByChannel.compactMapValues { state in
                    return try? state.decode([String: JSONCodableScalarType].self).mapValues {
                        return $0.stringOptional
                    }
                }
                completion(.success(attributesByChannel))
            }
        }
    }

    func whereNow(
        for profileID: String,
        completion: @escaping (Result<[String], Error>) -> Void
    ) {
        pubnub.whereNow(
            for: profileID
        ) { result in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let channelsByUUID):
                let channels = channelsByUUID[profileID] ?? []
                completion(.success(channels))
            }

        }
    }

    func hereNow(
        in channels: [String],
        completion: @escaping (Result<[String: [String]], Error>) -> Void
    ) {
        self.pubnub.hereNow(on: channels) { result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let presenceByChannel):
                completion(.success(presenceByChannel.mapValues { $0.occupants }))
            }
        }
    }
}
