//
//  InternalPresenceClient.swift
//  
//
//  Created by Mike Moloksher on 3/1/23.
//

import Foundation
import LiveLikeCore

class InternalPresenceClient {

    private let clientID: String
    private let whenPresenceService: Promise<PresenceService>
    private weak var delegate: PresenceClientDelegate?

    init(
        presenceService: Promise<PresenceService>,
        clientID: String
    ) {
        self.clientID = clientID
        self.whenPresenceService = presenceService

        firstly {
            self.whenPresenceService
        }.then { internalPresenceService in
            internalPresenceService.setDelegate(self)
        }
    }

    // Appending client ID to the channel names will help make channel names
    // unique and relative to a client, as a result avoiding
    // two integrators from unintentionally subscribing
    // to each other's channels.
    private func appendClientID(to channels: [String]) -> [String] {
        return channels.map { "\(clientID)_\($0)" }
    }
}

extension InternalPresenceClient: PresenceServiceDelegate {
    func presenceService(
        _ presenceService: PresenceService,
        didReceivePresenceEvents presenceEvents: [PresenceEvent]
    ) {
        self.delegate?.presenceClient(
            self,
            didReceivePresenceEvents: presenceEvents
        )
    }
}

extension InternalPresenceClient: PresenceClient {

    func setDelegate(_ delegate: PresenceClientDelegate?) {
        self.delegate = delegate
    }

    func unsubscribe(from channels: [String]) {
        firstly {
            self.whenPresenceService
        }.then { presenceService in
            presenceService.unsubscribe(from: self.appendClientID(to: channels))
        }
    }

    func setAttributes(
        for channels: [String],
        with attributes: PresenceClientAttributes,
        completion: @escaping (Result<PresenceClientAttributes, Error>) -> Void
    ) {
        firstly {
            self.whenPresenceService
        }.then { presenceService in
            presenceService.setAttributes(
                for: self.appendClientID(to: channels),
                with: attributes,
                completion: completion
            )
        }
    }

    func getAttributes(
        for profileID: String,
        on channels: [String],
        completion: @escaping (Result<[String: PresenceClientAttributes], Error>) -> Void
    ) {
        firstly {
            self.whenPresenceService
        }.then { presenceService in
            presenceService.getAttributes(
                for: profileID,
                on: self.appendClientID(to: channels),
                completion: completion
            )
        }
    }

    func subscribe(to channels: [String]) {
        firstly {
            self.whenPresenceService
        }.then { presenceService in
            presenceService.subscribe(to: self.appendClientID(to: channels))
        }
    }

    func leaveChannels(_ channels: [String]) {
        firstly {
            self.whenPresenceService
        }.then { presenceService in
            presenceService.leaveChannels(self.appendClientID(to: channels))
        }
    }

    func joinChannels(_ channels: [String]) {
        firstly {
            self.whenPresenceService
        }.then { presenceService in
            presenceService.joinChannels(self.appendClientID(to: channels))
        }
    }

    func whereNow(
        for profileID: String,
        completion: @escaping (Result<[String], Error>) -> Void
    ) {
        firstly {
            self.whenPresenceService
        }.then { presenceService in
            presenceService.whereNow(
                for: profileID,
                completion: completion
            )
        }
    }

    func hereNow(
        in channels: [String],
        completion: @escaping (Result<[String: [String]], Error>) -> Void
    ) {
        firstly {
            self.whenPresenceService
        }.then { presenceService in
            presenceService.hereNow(
                in: self.appendClientID(to: channels)
            ) { result in
                switch result {
                case .failure(let error):
                    completion(.failure(error))
                case .success(let occupantsByChannel):
                    let occupantsByChannel: [String: [String]] = occupantsByChannel.reduce(into: [:]) { dict, kvp in
                        let key = String(kvp.key.dropFirst("\(self.clientID)_".count))
                        dict[key] = kvp.value
                    }
                    completion(.success(occupantsByChannel))
                }

            }
        }
    }

}
