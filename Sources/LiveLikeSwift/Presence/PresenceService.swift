//
//  PresenceService.swift
//  
//
//  Created by Jelzon Monzon on 3/29/23.
//

import Foundation

protocol PresenceServiceDelegate: AnyObject {
    func presenceService(
        _ presenceService: PresenceService,
        didReceivePresenceEvents presenceEvents: [PresenceEvent]
    )
}

protocol PresenceService {

    func setDelegate(_ delegate: PresenceServiceDelegate?)

    func subscribe(to channels: [String])

    func unsubscribe(from channels: [String])

    func joinChannels(_ channels: [String])

    func leaveChannels(_ channels: [String])

    func setAttributes(
        for channels: [String],
        with attributes: PresenceClientAttributes,
        completion: @escaping (Result<PresenceClientAttributes, Error>) -> Void
    )

    func getAttributes(
        for profileID: String,
        on channels: [String],
        completion: @escaping (Result<[String: PresenceClientAttributes], Error>) -> Void
    )

    func whereNow(
        for profileID: String,
        completion: @escaping (Result<[String], Error>) -> Void
    )

    func hereNow(
        in channels: [String],
        completion: @escaping (Result<[String: [String]], Error>) -> Void
    )
}
