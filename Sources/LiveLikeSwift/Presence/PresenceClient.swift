//
//  PresenceClient.swift
//  
//
//  Created by Mike Moloksher on 3/6/23.
//

import Foundation

/// Delegate methods for listening to presence changes
public protocol PresenceClientDelegate: AnyObject {
    func presenceClient(
        _ presenceClient: PresenceClient,
        didReceivePresenceEvents presenceEvents: [PresenceEvent]
    )
}

/// Methods to manage a user's presence
public protocol PresenceClient {

    /// Set delegate to be able to listen to presence changes
    func setDelegate(_ delegate: PresenceClientDelegate?)

    /// Join channels to appear present
    /// - Parameter channels: Channel Ids
    func joinChannels(_ channels: [String])

    /// Remove your presence from channels
    /// - Parameter channels: Channel Ids
    func leaveChannels(_ channels: [String])

    /// Subscribe to channels to recieve presence events
    /// - Parameter channels: Channel Ids
    func subscribe(to channels: [String])

    /// Unsubscribe from channels to stop receiving presence events
    /// - Parameter channels: Channel Ids
    func unsubscribe(from channels: [String])

    /// Set attributes associated with the current user's presence
    /// - Parameters:
    ///   - channels: Channel Ids
    ///   - attributes: set attributes associated with the current user's presence
    ///   - completion:
    ///     - success: the presence attributes after the update
    ///     - failure: an error describing a failure
    func setAttributes(
        for channels: [String],
        with attributes: PresenceClientAttributes,
        completion: @escaping (Result<PresenceClientAttributes, Error>) -> Void
    )


    /// Gets attributes associated with a user's presence
    /// - Parameters:
    ///   - profileID: The id of the user's profile
    ///   - channels: The channel ids
    ///   - completion:
    ///     - success: The a dictionary of presence attributes keyed by channel
    ///     - failure: an error describing a failure
    func getAttributes(
        for profileID: String,
        on channels: [String],
        completion: @escaping (Result<[String: PresenceClientAttributes], Error>) -> Void
    )

    /// Fetch the channels where a user is present in now
    /// - Parameters:
    ///   - profileID: a user's profile id
    ///   - completion:
    ///     - success: the channels where a user is present now
    ///     - failure: an error describing a failure
    func whereNow(
        for profileID: String,
        completion: @escaping (Result<[String], Error>) -> Void
    )

    /// Fetch the occupant list of a channel(s)
    /// - Parameters:
    ///   - channels: The channel id(s)
    ///   - completion:
    ///     - success: the occupant list by channel
    ///     - failure: an error describing a failure
    func hereNow(
        in channels: [String],
        completion: @escaping (Result<[String: [String]], Error>) -> Void
    )

}

public typealias PresenceClientAttributes = [String: String?]

public struct PresenceEvent {
    public let userID: String
    public let channel: String
    public let attributes: PresenceClientAttributes?
    public let action: PresenceEventAction
}

extension PresenceEvent: Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(userID)
        hasher.combine(channel)
        hasher.combine(attributes)
        hasher.combine(action)
    }
}

public enum PresenceEventAction {
    case join
    case leave
    case timeout
    case attributesUpdate
}
