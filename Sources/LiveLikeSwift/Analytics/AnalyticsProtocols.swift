//
//  AnalyticsProtocols.swift
//  LiveLikeSDK
//
//  Created by jelzon on 2/13/19.
//

import Foundation

protocol EventRecorder {
    func record(_ event: AnalyticsEvent)
}

/**
 Delegate for receiving analytic events.
 */
@objc(LLAnalyticsDelegate)
public protocol EngagementAnalyticsDelegate {
    /**
     Called when an analytic event is recorded by the Engagement SDK.

     - parameter name: Name of event that has been recorded.
     - parameter data: Dictionary of data related to the event. The key will always be a string. The value can be either String, Int, UInt, Double, Float, Bool, Date, URL, Array.
     */
    func engagementAnalyticsEvent(name: String, withData data: [String: Any])
}
