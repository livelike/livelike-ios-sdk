//
//  AnalyticsEvent.swift
//  LiveLikeSDK
//
//  Created by jelzon on 2/13/19.
//

import Foundation

struct AnalyticsEvent {
    let metadata: [Attribute: Any]
    let name: Name
    let isClientReadable: Bool

    init(name: Name, data: [Attribute: Any], isClientReadable: Bool = true) {
        self.name = name
        metadata = data
        self.isClientReadable = isClientReadable
    }
}

extension AnalyticsEvent {
    struct Name: ExpressibleByStringLiteral, Equatable, CustomStringConvertible {
        let stringValue: String
        init(stringLiteral value: String) {
            stringValue = value
        }

        var description: String { return stringValue }
    }
}

extension AnalyticsEvent.Name {
    typealias Name = AnalyticsEvent.Name
    static let widgetDisplayed: Name = "Widget Displayed"

    static let widgetEngaged: Name = "Widget Engaged"
    static let widgetBecameInteractive: Name = "Widget Became Interactive"

    static let chatMessageSent: Name = "Chat Message Sent"
    static let orientationChanged: Name = "Orientation Changed"
    static let widgetPauseStatusChanged: Name = "Widget Pause Status Changed"
    static let chatPauseStatusChanged: Name = "Chat Pause Status Changed"
    static let alertWidgetLinkOpened: Name = "Alert Link Opened"
    static let videoAlertPlayStarted: Name = "Video Alert Play Started"
}

extension AnalyticsEvent {

    static func widgetBecameInteractive(programID: String, kind: WidgetKind, widgetID: String) -> AnalyticsEvent {
        return AnalyticsEvent(
            name: .widgetBecameInteractive,
            data: [
                .programId: programID,
                .widgetType: kind.analyticsName,
                .widgetId: widgetID
            ]
        )
    }

    static func widgetDisplayed(programID: String, kind: String, widgetId: String, widgetLink: URL?) -> AnalyticsEvent {
        var data: [Attribute: Any] = [
            .programId: programID,
            .widgetType: kind,
            .widgetId: widgetId
        ]
        if let widgetLink = widgetLink {
            data[.widgetLinkUrl] = widgetLink.absoluteString
        }
        return AnalyticsEvent(
            name: .widgetDisplayed,
            data: data
        )
    }

    static func widgetEngaged(
        widgetModel: WidgetModel
    ) -> AnalyticsEvent {
        let baseWidgetModel = widgetModel.baseWidgetModel
        return AnalyticsEvent(
            name: .widgetEngaged,
            data: {
                let props: [Attribute: Any?] = [
                    .programId: baseWidgetModel.programID,
                    .widgetType: baseWidgetModel.kind.analyticsName,
                    .widgetId: baseWidgetModel.id,
                    .widgetPrompt: widgetModel.prompt
                ]
                return props.compactMapValues { $0 }
            }()
        )
    }

    static func videoAlertPlayStarted(
        widgetID: String,
        programID: String,
        widgetKind: WidgetKind,
        videoURL: URL
    ) -> AnalyticsEvent {
        return .init(
            name: .videoAlertPlayStarted,
            data: [
                .widgetId: widgetID,
                .programId: programID,
                .widgetType: widgetKind.analyticsName,
                .videoURL: videoURL
            ]
        )
    }

    static func alertWidgetLinkOpened(
        alertId: String,
        programId: String,
        linkUrl: String,
        kind: WidgetKind
    ) -> AnalyticsEvent {
        return .init(
            name: .alertWidgetLinkOpened,
            data: [
                .alertId: alertId,
                .programId: programId,
                .widgetLinkUrl: linkUrl,
                .widgetType: kind.analyticsName
            ]
        )
    }

    static func chatMessageSent(properties: ChatSentMessageProperties) -> AnalyticsEvent {
        return AnalyticsEvent(
            name: .chatMessageSent,
            data: [.chatCharacterLength: properties.characterCount,
                   .chatMessageId: properties.messageId,
                   .stickerShortcodes: properties.stickerShortcodes,
                   .stickerCount: properties.stickerCount,
                   .stickerKeyboardIndices: properties.stickerIndices,
                   .chatMessageHasExternalImage: properties.hasExternalImage,
                   .chatRoomId: properties.chatRoomId]
        )
    }

    static func widgetPauseStatusChanged(previousStatus: PauseStatus, newStatus: PauseStatus, secondsInPreviousStatus: Double) -> AnalyticsEvent {
        return AnalyticsEvent(
            name: .widgetPauseStatusChanged,
            data: [
                .previousPauseStatus: previousStatus.analyticsName,
                .newPauseStatus: newStatus.analyticsName,
                .secondsInPreviousPauseStatus: secondsInPreviousStatus
            ]
        )
    }
}

private extension WidgetModel {
    var prompt: String? {
        switch self {
        case .alert(let model):
            return model.title
        case .cheerMeter(let model):
            return model.title
        case .quiz(let model):
            return model.question
        case .prediction(let model):
            return model.question
        case .predictionFollowUp(let model):
            return model.question
        case .poll(let model):
            return model.question
        case .imageSlider(let model):
            return model.question
        case .socialEmbed:
            return nil
        case .videoAlert(let model):
            return model.title
        case .textAsk(let model):
            return model.title
        case .numberPrediction(let model):
            return model.question
        case .numberPredictionFollowUp(let model):
            return model.question
        case .richPost(let model):
            return model.title
        }
    }
}
