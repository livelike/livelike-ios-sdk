//
//  Analytics.swift
//  LiveLikeSDK
//
//  Created by jelzon on 2/13/19.
//

import Foundation
import LiveLikeCore

class Analytics: EventRecorder {
    weak var delegate: EngagementAnalyticsDelegate?

    init() {
        CoreAnalytics.shared.didRecordEvent = { [weak self] name, data in
            guard let self = self else { return }
            let data = data.reduce(into: [AnalyticsEvent.Attribute: Any]()) { (partialResult, arg1) in
                partialResult[AnalyticsEvent.Attribute(stringLiteral: arg1.key)] = arg1.value
            }
            self.record(
                AnalyticsEvent(name: AnalyticsEvent.Name(stringLiteral: name), data: data)
            )
        }
    }

    func record(_ event: AnalyticsEvent) {
        log.info("\(event.name)\n\(event.metadata as AnyObject)")

        if let delegate = delegate, event.isClientReadable {
            // Repeat event to client
            let data = event.metadata.reduce(into: [String: Any]()) { result, pair in
                result[pair.key.stringValue] = pair.value
            }
            delegate.engagementAnalyticsEvent(
                name: event.name.stringValue,
                withData: data
            )
        }
    }
}
