//
//  AnalyticsEvent.swift
//  LiveLikeSDK
//
//  Created by jelzon on 2/13/19.
//

import Foundation

extension AnalyticsEvent {
    struct Attribute: ExpressibleByStringLiteral, Equatable, Hashable, CustomStringConvertible {
        let stringValue: String
        init(stringLiteral value: String) {
            stringValue = value
        }

        var description: String { return stringValue }
    }
}

extension AnalyticsEvent.Attribute {
    typealias Attribute = AnalyticsEvent.Attribute
    static let widgetType: Attribute = "Widget Type"
    static let widgetId: Attribute = "Widget ID"
    static let programId: Attribute = "Program ID"
    static let widgetPrompt: Attribute = "Widget Prompt"
    static let widgetLinkUrl: Attribute = "Link URL"
    static let totalSecondsInPreviousOrientation: Attribute = "Total Seconds In Previous Orientation"
    static let chatCharacterLength: Attribute = "Character Length"
    static let chatMessageId: Attribute = "Chat Message ID"
    static let chatRoomId: Attribute = "Chat Room ID"
    static let stickerShortcodes: Attribute = "Sticker Shortcodes"
    static let stickerCount: Attribute = "Sticker Count"
    static let stickerKeyboardIndices: Attribute = "Sticker Keyboard Indices"
    static let keyboardType: Attribute = "Keyboard Type"
    static let keyboardHideMethod: Attribute = "Keyboard Hide Method"
    static let previousPauseStatus: Attribute = "Previous Pause Status"
    static let newPauseStatus: Attribute = "New Pause Status"
    static let secondsInPreviousPauseStatus: Attribute = "Seconds In Previous Pause Status"
    static let completionType: Attribute = "Completion Type"
    static let chatMessageHasExternalImage: Attribute = "Has External Image"
    static let alertId: Attribute = "Alert Id"
    static let videoURL: Attribute = "Video URL"
}
