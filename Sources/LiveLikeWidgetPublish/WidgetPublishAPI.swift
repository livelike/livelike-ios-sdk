//
//  WidgetPublishAPI.swift
//  
//
//  Created by Jelzon Monzon on 5/15/23.
//

import Foundation

public class WidgetPublishAPI {
    public static func createTextNumberPrediction(
        baseURL: URL,
        accessToken: String,
        payload: CreateTextNumberPrediction,
        completion: @escaping (Result<WidgetCreatedResponse, Error>) -> Void
    ) {
        let url = baseURL.appendingPathComponent("text-number-predictions/")
        createAndScheduleWidget(
            url: url,
            apiToken: accessToken,
            payload: payload,
            completion: completion
        )
    }

    public static func createImageNumberPrediction(
        baseURL: URL,
        accessToken: String,
        payload: CreateImageNumberPrediction,
        completion: @escaping (Result<WidgetCreatedResponse, Error>) -> Void
    ) {
        let url = baseURL.appendingPathComponent("image-number-predictions/")
        createAndScheduleWidget(
            url: url,
            apiToken: accessToken,
            payload: payload,
            completion: completion
        )
    }

    public static func createTextAsk(
        baseURL: URL,
        accessToken: String,
        payload: CreateTextAsk,
        completion: @escaping (Result<WidgetCreatedResponse, Error>) -> Void
    ) {
        let url = baseURL.appendingPathComponent("text-asks/")
        createAndScheduleWidget(
            url: url,
            apiToken: accessToken,
            payload: payload,
            completion: completion
        )
    }

    public static func createVideoAlert(
        baseURL: URL,
        accessToken: String,
        payload: CreateVideoAlert,
        completion: @escaping (Result<WidgetCreatedResponse, Error>) -> Void
    ) {
        let url = baseURL.appendingPathComponent("video-alerts/")
        createAndScheduleWidget(
            url: url,
            apiToken: accessToken,
            payload: payload,
            completion: completion
        )
    }

    public static func createTextQuiz(
        baseURL: URL,
        accessToken: String,
        payload: CreateTextQuiz,
        completion: @escaping (Result<WidgetCreatedResponse, Error>) -> Void
    ) {
        let url = baseURL.appendingPathComponent("text-quizzes/")
        createAndScheduleWidget(
            url: url,
            apiToken: accessToken,
            payload: payload,
            completion: completion
        )
    }

    public static func createImageQuiz(
        baseURL: URL,
        accessToken: String,
        payload: CreateImageQuiz,
        completion: @escaping (Result<WidgetCreatedResponse, Error>) -> Void
    ) {
        let url = baseURL.appendingPathComponent("image-quizzes/")
        createAndScheduleWidget(
            url: url,
            apiToken: accessToken,
            payload: payload,
            completion: completion
        )
    }

    public static func createTextPoll(
        baseURL: URL,
        accessToken: String,
        payload: CreateTextPoll,
        completion: @escaping (Result<WidgetCreatedResponse, Error>) -> Void
    ) {
        let url = baseURL.appendingPathComponent("text-polls/")
        createAndScheduleWidget(
            url: url,
            apiToken: accessToken,
            payload: payload,
            completion: completion
        )
    }

    public static func createImagePoll(
        baseURL: URL,
        accessToken: String,
        payload: CreateImagePoll,
        completion: @escaping (Result<WidgetCreatedResponse, Error>) -> Void
    ) {
        let url = baseURL.appendingPathComponent("image-polls/")
        createAndScheduleWidget(
            url: url,
            apiToken: accessToken,
            payload: payload,
            completion: completion
        )
    }

    public static func createTextPrediction(
        baseURL: URL,
        accessToken: String,
        payload: CreateTextPrediction,
        completion: @escaping (Result<PredictionResult, Error>) -> Void
    ) {
        let url = baseURL.appendingPathComponent("text-predictions/")
        createAndSchedulePrediction(
            url: url,
            apiToken: accessToken,
            payload: payload,
            completion: completion
        )
    }

    public static func createImagePrediction(
        baseURL: URL,
        accessToken: String,
        payload: CreateImagePrediction,
        completion: @escaping (Result<PredictionResult, Error>) -> Void
    ) {
        let url = baseURL.appendingPathComponent("image-predictions/")
        createAndSchedulePrediction(
            url: url,
            apiToken: accessToken,
            payload: payload,
            completion: completion
        )
    }

    public static func createImageSlider(
        baseURL: URL,
        accessToken: String,
        payload: CreateImageSlider,
        completion: @escaping (Result<WidgetCreatedResponse, Error>) -> Void
    ) {
        let url = baseURL.appendingPathComponent("emoji-sliders/")
        createAndScheduleWidget(
            url: url,
            apiToken: accessToken,
            payload: payload,
            completion: completion
        )
    }

    public static func createAlert(
        baseURL: URL,
        accessToken: String,
        payload: CreateAlert,
        completion: @escaping (Result<WidgetCreatedResponse, Error>) -> Void
    ) {
        let url = baseURL.appendingPathComponent("alerts/")
        createAndScheduleWidget(
            url: url,
            apiToken: accessToken,
            payload: payload,
            completion: completion
        )
    }

    public static func createCheerMeter(
        baseURL: URL,
        accessToken: String,
        payload: CreateCheerMeter,
        completion: @escaping (Result<WidgetCreatedResponse, Error>) -> Void
    ) {
        let url = baseURL.appendingPathComponent("cheer-meters/")
        createAndScheduleWidget(
            url: url,
            apiToken: accessToken,
            payload: payload,
            completion: completion
        )
    }

    public static func createSocialEmbed(
        baseURL: URL,
        accessToken: String,
        payload: CreateSocialEmbed,
        completion: @escaping (Result<WidgetCreatedResponse, Error>) -> Void
    ) {
        let url = baseURL.appendingPathComponent("social-embeds/")
        createAndScheduleWidget(
            url: url,
            apiToken: accessToken,
            payload: payload,
            completion: completion
        )
    }

    private static func createAndScheduleWidget<T: Encodable>(
        url: URL,
        apiToken: String,
        payload: T,
        completion: @escaping (Result<WidgetCreatedResponse, Error>) -> Void
    ) {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("Bearer \(apiToken)", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .iso8601
        request.httpBody = try! encoder.encode(payload)

        URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                return completion(.failure(error))
            }
            guard let response = response as? HTTPURLResponse else {
                return completion(.failure(MyError("Response is not HTTPURLResponse")))
            }
            guard response.statusCode >= 200, response.statusCode < 300 else {
                if response.statusCode == 401 {
                    return completion(.failure(MyError("API Token was invalid.")))
                } else {
                    return completion(.failure(MyError("Request failed with status code: \(response.statusCode)")))
                }
            }
            guard let data = data else {
                return completion(.failure(MyError("Server returned no data.")))
            }
            guard let widgetCreatedResponse = try? JSONDecoder().decode(WidgetCreatedResponse.self, from: data) else {
                return completion(.failure(MyError("Failed to decode data as WidgetCreatedResponse")))
            }
            schedule(url: widgetCreatedResponse.schedule_url, apiToken: apiToken) { _ in
                completion(.success(widgetCreatedResponse))
            }
        }.resume()
    }

    private static func createAndSchedulePrediction<T: Encodable>(
        url: URL,
        apiToken: String,
        payload: T,
        completion: @escaping (Result<PredictionResult, Error>) -> Void
    ) {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("Bearer \(apiToken)", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let encoder = JSONEncoder()
        request.httpBody = try! encoder.encode(payload)

        URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                return completion(.failure(error))
            }
            guard let response = response as? HTTPURLResponse else {
                return completion(.failure(MyError("Response is not HTTPURLResponse")))
            }
            guard response.statusCode >= 200, response.statusCode < 300 else {
                if response.statusCode == 401 {
                    return completion(.failure(MyError("API Token was invalid.")))
                } else {
                    return completion(.failure(MyError("Request failed with status code: \(response.statusCode)")))
                }
            }
            guard let data = data else {
                return completion(.failure(MyError("Server returned no data.")))
            }
            guard let dataString = String(data: data, encoding: .utf8) else {
                return completion(.failure(MyError("Failed to encode data with utf8")))
            }
            guard let widgetCreatedResponse = try? JSONDecoder().decode(PredictionCreatedResponse.self, from: data) else {
                return completion(.failure(MyError("Failed to decode data as PredictionCreatedResponse")))
            }
            schedule(url: widgetCreatedResponse.schedule_url, apiToken: apiToken) { _ in
                let predictionResult = PredictionResult(
                    json: dataString,
                    payload: widgetCreatedResponse
                )
                completion(.success(predictionResult))
            }
        }.resume()
    }

    public static func setCorrectPredictionOptionAndScheduleFollowUp(
        _ option: PredictionCreatedResponse.Option,
        scheduleURL: URL,
        apiToken: String,
        completion: @escaping (Result<String, Error>) -> Void
    ) {
        var request = URLRequest(url: option.url)
        request.httpMethod = "PATCH"
        request.setValue("Bearer \(apiToken)", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let payload = PredictionOptionPatch(is_correct: true)

        let encoder = JSONEncoder()
        request.httpBody = try! encoder.encode(payload)

        URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                return completion(.failure(error))
            }
            guard let response = response as? HTTPURLResponse else {
                return completion(.failure(MyError("Response is not HTTPURLResponse")))
            }
            guard response.statusCode >= 200, response.statusCode < 300 else {
                if response.statusCode == 401 {
                    return completion(.failure(MyError("API Token was invalid.")))
                } else {
                    return completion(.failure(MyError("Request failed with status code: \(response.statusCode)")))
                }
            }
            guard let data = data else {
                return completion(.failure(MyError("Server returned no data.")))
            }
            guard let dataString = String(data: data, encoding: .utf8) else {
                return completion(.failure(MyError("Failed to encode data with utf8")))
            }
            schedule(url: scheduleURL, apiToken: apiToken) { _ in
                completion(.success(dataString))
            }
        }.resume()
    }

    private static func schedule(
        url: URL,
        apiToken: String,
        completion: @escaping (Result<String, Error>) -> Void
    ) {
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        request.setValue("Bearer \(apiToken)", forHTTPHeaderField: "Authorization")
        URLSession.shared.dataTask(with: request) { data, _, error in
            if let error = error {
                return completion(.failure(error))
            }
            guard let data = data else {
                return completion(.failure(MyError("Server returned no data.")))
            }
            guard let dataString = String(data: data, encoding: .utf8) else {
                return completion(.failure(MyError("Failed to encode data with utf8")))
            }
            completion(.success(dataString))
        }.resume()
    }

    public static func delete(
        url: URL,
        apiToken: String,
        completion: @escaping (Result<String, Error>) -> Void
    ) {
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        request.setValue("Bearer \(apiToken)", forHTTPHeaderField: "Authorization")
        URLSession.shared.dataTask(with: request) { data, _, error in
            if let error = error {
                return completion(.failure(error))
            }
            guard let data = data else {
                return completion(.failure(MyError("Server returned no data.")))
            }
            guard let dataString = String(data: data, encoding: .utf8) else {
                return completion(.failure(MyError("Failed to encode data with utf8")))
            }
            completion(.success(dataString))
        }.resume()
    }
}
