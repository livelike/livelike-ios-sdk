//
//  Models.swift
//  
//
//  Created by Jelzon Monzon on 5/15/23.
//

import Foundation

public struct MyError: LocalizedError {
    private let string: String

    init(_ string: String) {
        self.string = string
    }

    public var errorDescription: String? {
        return string
    }
}

public struct PredictionResult {
    public let json: String
    public let payload: PredictionCreatedResponse
}

public struct CreateTextNumberPrediction: Encodable {
    public init(program_id: String, question: String, options: [CreateTextNumberPrediction.Choice], timeout: String, custom_data: String? = nil) {
        self.program_id = program_id
        self.question = question
        self.options = options
        self.timeout = timeout
        self.custom_data = custom_data
    }

    public let program_id: String
    public let question: String
    public let options: [Choice]
    public let timeout: String
    public let custom_data: String?

    public struct Choice: Encodable {
        public init(description: String) {
            self.description = description
        }

        public let description: String
    }
}

public struct CreateImageNumberPrediction: Encodable {
    public init(program_id: String, question: String, options: [CreateImageNumberPrediction.Choice], timeout: String, custom_data: String? = nil) {
        self.program_id = program_id
        self.question = question
        self.options = options
        self.timeout = timeout
        self.custom_data = custom_data
    }

    public let program_id: String
    public let question: String
    public let options: [Choice]
    public let timeout: String
    public let custom_data: String?

    public struct Choice: Encodable {
        public init(description: String, image_url: URL) {
            self.description = description
            self.image_url = image_url
        }

        public let description: String
        public let image_url: URL
    }
}

public struct CreateTextAsk: Encodable {
    public init(program_id: String, title: String, prompt: String, confirmation_message: String? = nil) {
        self.program_id = program_id
        self.title = title
        self.prompt = prompt
        self.confirmation_message = confirmation_message
    }

    public let program_id: String
    public let title: String
    public let prompt: String
    public let confirmation_message: String?
}

public struct CreateVideoAlert: Encodable {
    public init(program_id: String, timeout: String, title: String? = nil, text: String? = nil, video_url: URL, link_label: String? = nil, link_url: URL? = nil, custom_data: String? = nil) {
        self.program_id = program_id
        self.timeout = timeout
        self.title = title
        self.text = text
        self.video_url = video_url
        self.link_label = link_label
        self.link_url = link_url
        self.custom_data = custom_data
    }

    public let program_id: String
    public let timeout: String
    public let title: String?
    public let text: String?
    public let video_url: URL
    public let link_label: String?
    public let link_url: URL?
    public let custom_data: String?
}

public struct CreateTextQuiz: Encodable {
    public init(
        program_id: String,
        question: String,
        choices: [CreateTextQuiz.Choice],
        timeout: String,
        custom_data: String? = nil,
        interactive_until: Date? = nil,
        playback_time_ms: Int? = nil
    ) {
        self.program_id = program_id
        self.question = question
        self.choices = choices
        self.timeout = timeout
        self.custom_data = custom_data
        self.interactive_until = interactive_until
        self.playback_time_ms = playback_time_ms
    }

    public let program_id: String
    public let question: String
    public let choices: [Choice]
    public let timeout: String
    public let custom_data: String?
    public let interactive_until: Date?
    public let playback_time_ms: Int?

    public struct Choice: Encodable {
        public init(description: String, is_correct: Bool) {
            self.description = description
            self.is_correct = is_correct
        }

        public let description: String
        public let is_correct: Bool
    }
}

public struct CreateImageQuiz: Encodable {
    public init(program_id: String, question: String, choices: [CreateImageQuiz.Choice], timeout: String, custom_data: String? = nil) {
        self.program_id = program_id
        self.question = question
        self.choices = choices
        self.timeout = timeout
        self.custom_data = custom_data
    }

    public let program_id: String
    public let question: String
    public let choices: [Choice]
    public let timeout: String
    public let custom_data: String?

    public struct Choice: Encodable {
        public init(description: String, is_correct: Bool, image_url: URL) {
            self.description = description
            self.is_correct = is_correct
            self.image_url = image_url
        }

        public let description: String
        public let is_correct: Bool
        public let image_url: URL
    }
}

public struct CreateTextPoll: Encodable {
    public init(program_id: String, question: String, options: [CreateTextPoll.Choice], timeout: String, custom_data: String? = nil) {
        self.program_id = program_id
        self.question = question
        self.options = options
        self.timeout = timeout
        self.custom_data = custom_data
    }

    public let program_id: String
    public let question: String
    public let options: [Choice]
    public let timeout: String
    public let custom_data: String?

    public struct Choice: Encodable {
        public init(description: String) {
            self.description = description
        }

        public let description: String
    }
}

public struct CreateImagePoll: Encodable {
    public init(program_id: String, question: String, options: [CreateImagePoll.Choice], timeout: String, custom_data: String? = nil) {
        self.program_id = program_id
        self.question = question
        self.options = options
        self.timeout = timeout
        self.custom_data = custom_data
    }

    public let program_id: String
    public let question: String
    public let options: [Choice]
    public let timeout: String
    public let custom_data: String?

    public struct Choice: Encodable {
        public init(description: String, image_url: URL) {
            self.description = description
            self.image_url = image_url
        }

        public let description: String
        public let image_url: URL
    }
}

public struct CreateTextPrediction: Encodable {
    public init(program_id: String, question: String, options: [CreateTextPrediction.Choice], timeout: String, custom_data: String? = nil, interactive_until: String? = nil) {
        self.program_id = program_id
        self.question = question
        self.options = options
        self.timeout = timeout
        self.custom_data = custom_data
        self.interactive_until = interactive_until
    }

    public let program_id: String
    public let question: String
    public let options: [Choice]
    public let timeout: String
    public let custom_data: String?
    public let interactive_until: String?

    public struct Choice: Encodable {
        public init(description: String) {
            self.description = description
        }

        public let description: String
    }
}

public struct CreateImagePrediction: Encodable {
    public init(program_id: String, question: String, options: [CreateImagePrediction.Choice], timeout: String, custom_data: String? = nil) {
        self.program_id = program_id
        self.question = question
        self.options = options
        self.timeout = timeout
        self.custom_data = custom_data
    }

    public let program_id: String
    public let question: String
    public let options: [Choice]
    public let timeout: String
    public let custom_data: String?

    public struct Choice: Encodable {
        public init(description: String, image_url: URL) {
            self.description = description
            self.image_url = image_url
        }

        public let description: String
        public let image_url: URL
    }
}

public struct CreateImageSlider: Encodable {
    public init(program_id: String, question: String, options: [CreateImageSlider.Option], timeout: String, custom_data: String? = nil) {
        self.program_id = program_id
        self.question = question
        self.options = options
        self.timeout = timeout
        self.custom_data = custom_data
    }

    public let program_id: String
    public let question: String
    public let options: [Option]
    public let timeout: String
    public let custom_data: String?

    public struct Option: Encodable {
        public init(image_url: URL) {
            self.image_url = image_url
        }

        public let image_url: URL
    }
}

public struct CreateAlert: Encodable {
    public init(program_id: String, timeout: String, title: String? = nil, text: String? = nil, image_url: URL? = nil, link_label: String? = nil, link_url: URL? = nil, custom_data: String? = nil) {
        self.program_id = program_id
        self.timeout = timeout
        self.title = title
        self.text = text
        self.image_url = image_url
        self.link_label = link_label
        self.link_url = link_url
        self.custom_data = custom_data
    }

    public let program_id: String
    public let timeout: String
    public let title: String?
    public let text: String?
    public let image_url: URL?
    public let link_label: String?
    public let link_url: URL?
    public let custom_data: String?
}

public struct CreateCheerMeter: Encodable {
    public init(question: String, cheer_type: String, program_id: String, timeout: String, options: [CreateCheerMeter.Option], custom_data: String? = nil) {
        self.question = question
        self.cheer_type = cheer_type
        self.program_id = program_id
        self.timeout = timeout
        self.options = options
        self.custom_data = custom_data
    }

    public let question: String
    public let cheer_type: String
    public let program_id: String
    public let timeout: String
    public let options: [Option]
    public let custom_data: String?

    public struct Option: Encodable {
        public init(description: String, image_url: URL) {
            self.description = description
            self.image_url = image_url
        }

        public let description: String
        public let image_url: URL
    }
}

public struct CreateSocialEmbed: Encodable {
    public init(program_id: String, comment: String, items: [CreateSocialEmbed.item], timeout: String, custom_data: String? = nil) {
        self.program_id = program_id
        self.comment = comment
        self.items = items
        self.timeout = timeout
        self.custom_data = custom_data
    }

    public let program_id: String
    public let comment: String
    public let items: [item]
    public let timeout: String
    public let custom_data: String?

    public struct item: Encodable {
        public init(url: String) {
            self.url = url
        }

        public let url: String
    }
}

public struct WidgetCreatedResponse: Decodable {
    public let url: URL
    public let schedule_url: URL
    public let id: String
    public let kind: String
}

public struct PredictionCreatedResponse: Decodable {
    public init(id: String, schedule_url: URL, follow_ups: [WidgetCreatedResponse], options: [PredictionCreatedResponse.Option], custom_data: String? = nil) {
        self.id = id
        self.schedule_url = schedule_url
        self.follow_ups = follow_ups
        self.options = options
        self.custom_data = custom_data
    }

    public let id: String
    public let schedule_url: URL
    public let follow_ups: [WidgetCreatedResponse]
    public let options: [Option]
    public let custom_data: String?

    public struct Option: Decodable {
        public let id: String
        public let url: URL
    }
}

public struct PredictionOptionPatch: Encodable {
    public init(is_correct: Bool) {
        self.is_correct = is_correct
    }

    public let is_correct: Bool
}
