//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//
	

import Foundation
import LiveLikeCore

public class LiveLikeRBACClient: RBACClient {
    
    private let clientID: String
    private let networking: LLNetworking
    private let accessToken: String
    private let application: Application
    private let paginationController = PaginatedRequestController()
    
    public static func make(
        config: RBACClientConfig,
        completion: @escaping (Result<RBACClient, Error>) -> Void
    ) {
        let url = config.apiBaseURL
            .appendingPathComponent("applications")
            .appendingPathComponent(config.clientID)
        let resource = LiveLikeCore.Resource<Application>(get: url)
        LLCore.networking.task(resource) { result in
            completion(result.map { application in
                LiveLikeRBACClient(
                    application: application,
                    networking: LLCore.networking,
                    accessToken: config.accessToken,
                    clientID: config.clientID
                )
            })
        }
    }
    
    init(
        application: Application,
        networking: LLNetworking,
        accessToken: String,
        clientID: String
    ) {
        self.application = application
        self.networking = networking
        self.accessToken = accessToken
        self.clientID = clientID
    }
    
    public func createRoleAssignment(
        options: CreateRoleAssignmentOptions,
        completion: @escaping (Result<RBACRoleAssignment, Error>) -> Void
    ) {
        CreateRoleAssignmentRequest(
            options: options,
            networking: networking,
            accessToken: accessToken
        ).execute(
            url: application.roleAssignmentsUrl,
            completion: completion
        )
    }
    
    public func deleteRoleAssignment(
        options: DeleteRoleAssignmentOptions,
        completion: @escaping (Result<Bool, Error>) -> Void
    ) {
        DeleteRoleAssignmentRequest(
            clientID: clientID,
            networking: networking,
            accessToken: accessToken
        ).execute(
            tryURL: try application.getProfileRoleAssignmentDetailURL(
                roleAssignmentID: options.roleAssignmentID
            ),
            completion: completion
        )
    }
    
    public func getAssignedRoles(
        page: Pagination,
        options: GetAssignedRolesOptions,
        completion: @escaping (Result<PaginatedResult<RBACRoleAssignment>, Error>) -> Void
    ) {
        paginationController.performExecuteFull(
            paginatedRequest: GetAssignedRolesRequest(
                options: options,
                networking: networking,
                accessToken: accessToken
            ), 
            rootURL: application.roleAssignmentsUrl,
            page: page,
            completion: completion
        )
    }
    
    public func getPermissions(
        page: Pagination,
        options: GetPermissionsOptions,
        completion: @escaping (Result<PaginatedResult<RBACPermission>, Error>) -> Void
    ) {
        paginationController.performExecuteFull(
            paginatedRequest: GetPermissionsRequest(
                clientID: clientID,
                options: options,
                networking: networking,
                accessToken: accessToken
            ),
            rootURL: application.permissionsUrl,
            page: page,
            completion: completion
        )
    }
    
    public func getRoles(
        page: Pagination,
        options: ListRolesOptions,
        completion: @escaping (Result<PaginatedResult<RBACRole>, Error>) -> Void
    ) {
        paginationController.performExecuteFull(
            paginatedRequest: ListRolesRequest(
                clientID: clientID,
                options: options,
                networking: networking,
                accessToken: accessToken
            ),
            rootURL: application.rolesUrl,
            page: page,
            completion: completion
        )
    }
    
    public func getResources(
        page: Pagination,
        options: GetResourceOptions,
        completion: @escaping (Result<PaginatedResult<RBACResource>, Error>) -> Void
    ) {
        paginationController.performExecuteFull(
            paginatedRequest: GetResourcesRequest(
                networking: networking,
                accessToken: accessToken
            ),
            rootURL: application.resourcesUrl,
            page: page,
            completion: completion
        )
    }
    
    public func getScopes(
        page: Pagination,
        options: GetScopesOptions,
        completion: @escaping (Result<PaginatedResult<RBACScope>, Error>) -> Void
    ) {
        paginationController.performExecuteFull(
            paginatedRequest: GetScopesRequest(
                clientID: clientID,
                options: options,
                networking: networking,
                accessToken: accessToken
            ),
            rootURL: application.scopesUrl,
            page: page,
            completion: completion
        )
    }
    
}
