//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation
import LiveLikeCore

public struct GetAssignedRolesOptions {

    public let profileID: String?
    public let scopeKey: String?

    public init(
        profileID: String? = nil,
        scopeKey: String? = nil
    ) {
        self.profileID = profileID
        self.scopeKey = scopeKey
    }
}

struct GetAssignedRolesRequest {
    init(
        options: GetAssignedRolesOptions,
        networking: LLNetworking,
        accessToken: String
    ) {
        self.options = options
        self.networking = networking
        self.accessToken = accessToken
    }

    private let options: GetAssignedRolesOptions
    private let networking: LLNetworking
    private let accessToken: String
}

extension GetAssignedRolesRequest: LLRequest {
    func execute(
        url: URL,
        completion: @escaping (Result<PaginatedResult<RBACRoleAssignment>, Error>) -> Void
    ) {
        do {
            let url = try url.with(
                queryItems: {
                    var queryItems = [URLQueryItem]()
                    if let profileID = options.profileID {
                        queryItems.append(
                            URLQueryItem(
                                name: "profile_id",
                                value: profileID
                            )
                        )
                    }

                    if let scopeKey = options.scopeKey {
                        queryItems.append(
                            URLQueryItem(
                                name: "scope_key",
                                value: scopeKey
                            )
                        )
                    }

                    return queryItems
                }()
            )
            let resource = LiveLikeCore.Resource<PaginatedResult<RBACRoleAssignment>>(
                get: url,
                accessToken: accessToken
            )
            networking.task(resource, completion: completion)
        } catch {
            completion(.failure(error))
        }

    }
}

extension GetAssignedRolesRequest: LLPaginatedRequest {
    typealias ElementType = RBACRoleAssignment

    var requestID: String {
        return "listRBACAssignedRolesRequest"
    }
}
