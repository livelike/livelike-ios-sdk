//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//
	

import Foundation
import LiveLikeCore

public struct CreateRoleAssignmentOptions {
    
    let roleKey: String
    let profileID: String
    let resourceKind: String
    let resourceKey: String
    
    public init(
        roleKey: String,
        profileID: String,
        resourceKind: String,
        resourceKey: String
    ) {
        self.roleKey = roleKey
        self.profileID = profileID
        self.resourceKey = resourceKey
        self.resourceKind = resourceKind
    }
}

struct CreateRoleAssignmentRequest {
    init(
        options: CreateRoleAssignmentOptions,
        networking: LLNetworking,
        accessToken: String
    ) {
        self.networking = networking
        self.accessToken = accessToken
        self.options = options
    }
    
    private let options: CreateRoleAssignmentOptions
    private let networking: LLNetworking
    private let accessToken: String
    
}

extension CreateRoleAssignmentRequest: LLRequest {
    func execute(
        url: URL,
        completion: @escaping (Result<RBACRoleAssignment, Error>) -> Void
    ) {
        struct Payload: Encodable {
            let roleKey: String
            let profileId: String
            let resourceKind: String
            let resourceKey: String
        }
        
        let resource = LiveLikeCore.Resource<RBACRoleAssignment>(
            url: url,
            method: .post(
                Payload(
                    roleKey: options.roleKey,
                    profileId: options.profileID,
                    resourceKind: options.resourceKind,
                    resourceKey: options.resourceKey
                )
            ),
            accessToken: self.accessToken
        )
        networking.task(resource, completion: completion)
    }
    
}
