//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//
	

import Foundation
import LiveLikeCore

public struct GetPermissionsOptions {
    
    public let profileID: String?
    
    public init(
        profileID: String? = nil
    ) {
        self.profileID = profileID
    }
}

struct GetPermissionsRequest {
    
    init(
        clientID: String,
        options: GetPermissionsOptions,
        networking: LLNetworking,
        accessToken: String
    ) {
        self.clientID = clientID
        self.options = options
        self.networking = networking
        self.accessToken = accessToken
    }
    
    private let clientID: String
    private let options: GetPermissionsOptions
    private let networking: LLNetworking
    private let accessToken: String
}

extension GetPermissionsRequest: LLRequest {
    func execute(
        url: URL,
        completion: @escaping (Result<PaginatedResult<RBACPermission>, Error>) -> Void
    ) {
        do {
            let url = try url.with(
                queryItems: {
                    
                    var queryItems = [URLQueryItem]()
                    
                    queryItems.append(URLQueryItem(name: "client_id", value: clientID))
                    
                    if let profileID = options.profileID {
                        queryItems.append(
                            URLQueryItem(name: "profile_id", value: profileID)
                        )
                    }
                
                    return queryItems
                }()
            )
                
            let resource = LiveLikeCore.Resource<PaginatedResult<RBACPermission>>(
                get: url,
                accessToken: accessToken
            )
            networking.task(resource, completion: completion)
        } catch {
            
        }
    }
}

extension GetPermissionsRequest: LLPaginatedRequest {
    typealias ElementType = RBACPermission
    
    var requestID: String {
        return "listPermissions"
    }
}
