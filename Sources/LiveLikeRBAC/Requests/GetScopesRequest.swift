//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//

import Foundation
import LiveLikeCore

public struct GetScopesOptions {
    
    public let resourceID: String?
    
    public init(
        resourceID: String?
    ) {
        self.resourceID = resourceID
    }
}

struct GetScopesRequest {
    init(
        clientID: String,
        options: GetScopesOptions,
        networking: LLNetworking,
        accessToken: String
    ) {
        self.clientID = clientID
        self.options = options
        self.networking = networking
        self.accessToken = accessToken
    }
    
    private let clientID: String
    private let options: GetScopesOptions
    private let networking: LLNetworking
    private let accessToken: String
}

extension GetScopesRequest: LLRequest {
    func execute(
        url: URL,
        completion: @escaping (Result<PaginatedResult<RBACScope>, Error>) -> Void
    ) {
        do {
            let url = try url.with(
                queryItems: {
                    var queryItems = [URLQueryItem]()
                    queryItems.append(
                        URLQueryItem(
                            name: "client_id",
                            value: clientID
                        )
                    )
                    
                    if let resourceID = options.resourceID {
                        queryItems.append(
                            URLQueryItem(
                                name: "resource_id",
                                value: resourceID
                            )
                        )
                    }
                    
                    return queryItems
                }()
            )
            
            let resource = LiveLikeCore.Resource<PaginatedResult<RBACScope>>(
                get: url,
                accessToken: accessToken
            )
            networking.task(resource, completion: completion)
        } catch {
            completion(.failure(error))
        }
    }
}

extension GetScopesRequest: LLPaginatedRequest {
    typealias ElementType = RBACScope
    
    var requestID: String {
        return "listScopesRequest"
    }
}
