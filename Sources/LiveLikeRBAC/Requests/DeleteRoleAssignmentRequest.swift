//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//
	

import Foundation
import LiveLikeCore

public struct DeleteRoleAssignmentOptions {
    let roleAssignmentID: String
    
    public init(roleAssignmentID: String) {
        self.roleAssignmentID = roleAssignmentID
    }
}

struct DeleteRoleAssignmentRequest {
    
    init(
        clientID: String,
        networking: LLNetworking,
        accessToken: String
    ) {
        self.clientID = clientID
        self.networking = networking
        self.accessToken = accessToken
    }
    
    private let clientID: String
    private let networking: LLNetworking
    private let accessToken: String
    
}

extension DeleteRoleAssignmentRequest: LLRequest {
    
    func execute(
        url: URL,
        completion: @escaping (Result<Bool, Error>) -> Void
    ) {
        struct EmptyBody: Encodable {}
        let resource = LiveLikeCore.Resource<Bool>(
            url: url,
            method: .delete(EmptyBody()),
            accessToken: accessToken
        )
        networking.task(resource, completion: completion)
    }
    
}
