//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation
import LiveLikeCore

public struct ListRolesOptions {
    public init() { }
}

struct ListRolesRequest {
    init(
        clientID: String,
        options: ListRolesOptions,
        networking: LLNetworking,
        accessToken: String
    ) {
        self.clientID = clientID
        self.options = options
        self.networking = networking
        self.accessToken = accessToken
    }

    private let clientID: String
    private let options: ListRolesOptions
    private let networking: LLNetworking
    private let accessToken: String
}

extension ListRolesRequest: LLRequest {
    func execute(
        url: URL,
        completion: @escaping (Result<PaginatedResult<RBACRole>, Error>) -> Void
    ) {
        do {
            let url = try url.with(
                queryItems: [
                    URLQueryItem(name: "client_id", value: clientID)
                ]
            )

            let resource = LiveLikeCore.Resource<PaginatedResult<RBACRole>>(
                get: url,
                accessToken: accessToken
            )
            networking.task(resource, completion: completion)
        } catch {
            completion(.failure(error))
        }
    }
}

extension ListRolesRequest: LLPaginatedRequest {
    typealias ElementType = RBACRole

    var requestID: String {
        return "listRBACRolesRequest"
    }
}
