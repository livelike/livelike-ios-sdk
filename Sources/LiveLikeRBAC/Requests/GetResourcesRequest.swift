//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation
import LiveLikeCore

public struct GetResourceOptions {
    public init() { }
}

struct GetResourcesRequest {
    init(
        networking: LLNetworking,
        accessToken: String
    ) {
        self.networking = networking
        self.accessToken = accessToken
    }

    private let networking: LLNetworking
    private let accessToken: String
}

extension GetResourcesRequest: LLRequest {
    func execute(
        url: URL,
        completion: @escaping (Result<PaginatedResult<RBACResource>, Error>) -> Void
    ) {
        let resource = LiveLikeCore.Resource<PaginatedResult<RBACResource>>(
            get: url,
            accessToken: accessToken
        )
        networking.task(resource, completion: completion)
    }
}

extension GetResourcesRequest: LLPaginatedRequest {
    typealias ElementType = RBACResource

    var requestID: String {
        return "listResourcesRequest"
    }
}
