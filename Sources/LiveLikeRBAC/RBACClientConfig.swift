//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation
import LiveLikeCore

public struct RBACClientConfig {

    public let clientID: String
    public let accessToken: String
    public let apiBaseURL: URL

    public init(
        clientID: String,
        accessToken: String,
        apiBaseURL: URL = LLAPIHelper.defaultAPIOrigin
    ) {
        self.clientID = clientID
        self.accessToken = accessToken
        self.apiBaseURL = apiBaseURL
    }
}
