//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//
	

import Foundation
import LiveLikeCore

public protocol RBACClient {
    
    func createRoleAssignment(
        options: CreateRoleAssignmentOptions,
        completion: @escaping (Result<RBACRoleAssignment, Error>) -> Void
    )
    
    func deleteRoleAssignment(
        options: DeleteRoleAssignmentOptions,
        completion: @escaping (Result<Bool, Error>) -> Void
    )
    
    func getAssignedRoles(
        page: Pagination,
        options: GetAssignedRolesOptions,
        completion: @escaping (Result<PaginatedResult<RBACRoleAssignment>, Error>) -> Void
    )
    
    func getPermissions(
        page: Pagination,
        options: GetPermissionsOptions,
        completion: @escaping (Result<PaginatedResult<RBACPermission>, Error>) -> Void
    )
    
    func getRoles(
        page: Pagination,
        options: ListRolesOptions,
        completion: @escaping (Result<PaginatedResult<RBACRole>, Error>) -> Void
    )
    
    func getResources(
        page: Pagination,
        options: GetResourceOptions,
        completion: @escaping (Result<PaginatedResult<RBACResource>, Error>) -> Void
    )
    
    func getScopes(
        page: Pagination,
        options: GetScopesOptions,
        completion: @escaping (Result<PaginatedResult<RBACScope>, Error>) -> Void
    )
}
