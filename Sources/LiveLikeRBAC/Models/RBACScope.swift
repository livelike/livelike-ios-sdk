//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

public struct RBACScope {
    public let id: String
    public let url: String
    public let clientId: String
    public let resourceKey: String
    public let createdAt: Date
    public let resource: RBACResource
}

extension RBACScope: Decodable { }
