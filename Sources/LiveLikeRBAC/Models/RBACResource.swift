//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

public struct RBACResource {
    public let id: String
    public let url: URL
    public let name: String
    public let description: String
    public let kind: String
    public let createdAt: Date
    public let scopesUrl: URL
}

extension RBACResource: Decodable { }
