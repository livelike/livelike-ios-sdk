//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation
import LiveLikeCore

struct Application: Decodable {

    static var defaultAPIOrigin: URL = URL(string: "https://cf-blast.livelikecdn.com/api/v1")!

    let rolesUrl: URL
    let scopesUrl: URL
    let permissionsUrl: URL
    let resourcesUrl: URL
    let roleAssignmentsUrl: URL
    let roleAssignmentsUrlTemplate: String

    func getProfileRoleAssignmentDetailURL(roleAssignmentID: String) throws -> URL {
        return try TemplatedURLHelper.buildURL(
            templatedURLString: roleAssignmentsUrlTemplate,
            templateKey: "{role_assignment_id}",
            replacementString: roleAssignmentID
        )
    }
}
