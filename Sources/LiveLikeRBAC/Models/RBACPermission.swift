//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

public struct RBACPermission {
    public let id: String
    public let url: String
    public let key: String
    public let name: String
    public let description: String
    public let createdAt: Date
}

extension RBACPermission: Decodable {}
