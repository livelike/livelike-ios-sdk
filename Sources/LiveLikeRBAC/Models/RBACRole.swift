//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

public struct RBACRole {
    public let id: String
    public let url: String
    public let clientId: String
    public let name: String
    public let description: String
    public let key: String
    public let createdAt: Date
    public let isActive: Bool
    public let permissions: [RBACPermission]
}

extension RBACRole: Decodable {}
