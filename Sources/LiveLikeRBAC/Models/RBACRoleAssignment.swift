//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

public struct RBACRoleAssignment {
    public let id: String
    public let url: URL
    public let profile: RBACProfile
    public let role: RBACRole
    public let scope: RBACScope
    public let createdAt: Date
}

extension RBACRoleAssignment: Decodable { }
