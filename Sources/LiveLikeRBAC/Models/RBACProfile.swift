//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

public struct RBACProfile {
    public let id: String
    public let url: String
    public let customId: String?
    public let nickname: String
    public let createdAt: Date
}

extension RBACProfile: Decodable { }
