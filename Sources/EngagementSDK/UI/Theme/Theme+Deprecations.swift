//
//  Theme+Deprecations.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 7/1/20.
//

import Foundation
import UIKit

/// Customizable properties of a Poll, Quiz, or Prediction Widget option
@objc public class ChoiceWidgetOptionColors: NSObject {
    /// Changes the border color of the option
    public var borderColor: UIColor
    /// Changes the progress bar gradient left color
    public var barGradientLeft: UIColor
    /// Changes the progress bar gradient right color
    public var barGradientRight: UIColor

    init(borderColor: UIColor, barGradientLeft: UIColor, barGradientRight: UIColor) {
        self.borderColor = borderColor
        self.barGradientLeft = barGradientLeft
        self.barGradientRight = barGradientRight
    }
}

/// Customizable properties of the Poll Widget
@objc public class PollWidgetTheme: NSObject {
    /// Changes the gradient left color
    public var titleGradientLeft: UIColor
    /// Changes the gradien right color
    public var titleGradientRight: UIColor
    /// Changes the user's selection colors
    public var selectedColors: ChoiceWidgetOptionColors
    ///
    public init(titleGradientLeft: UIColor, titleGradientRight: UIColor, selectedColors: ChoiceWidgetOptionColors) {
        self.titleGradientLeft = titleGradientLeft
        self.titleGradientRight = titleGradientRight
        self.selectedColors = selectedColors
    }
}

/// Customizable properties of the Prediction Widget
public struct PredictionWidgetTheme {
    /// Changes the gradient left color
    public var titleGradientLeft: UIColor
    /// Changes the gradient right color
    public var titleGradientRight: UIColor
    /// Changes the user's selection border color

    public var optionSelectBorderColor: UIColor
    /// Changes the color of the option results gradient
    public var optionGradientColors: ChoiceWidgetOptionColors
    /// Changes the lottie animation that plays when the prediction widget timer completes
    public var lottieAnimationOnTimerCompleteFilepaths: [String]

    ///
    public init(
        titleGradientLeft: UIColor,
        titleGradientRight: UIColor,
        optionSelectBorderColor: UIColor,
        optionGradientColors: ChoiceWidgetOptionColors,
        lottieAnimationOnTimerCompleteFilepath: [String]
    ) {
        self.titleGradientLeft = titleGradientLeft
        self.titleGradientRight = titleGradientRight
        self.optionSelectBorderColor = optionSelectBorderColor
        self.optionGradientColors = optionGradientColors
        self.lottieAnimationOnTimerCompleteFilepaths = lottieAnimationOnTimerCompleteFilepath
    }
}

/// Customizable properties of the Quiz Widget
@objc public class QuizWidgetTheme: NSObject {
    /// Changes the gradient left color
    public var titleGradientLeft: UIColor
    /// Changes the gradient right color
    public var titleGradientRight: UIColor
    /// Changes the user's selection border color
    public var optionSelectBorderColor: UIColor
    ///
    public init(titleGradientLeft: UIColor, titleGradientRight: UIColor, optionSelectBorderColor: UIColor) {
        self.titleGradientLeft = titleGradientLeft
        self.titleGradientRight = titleGradientRight
        self.optionSelectBorderColor = optionSelectBorderColor
    }
}

/// Customizable properties of the Alert Widget
@objc public class AlertWidgetTheme: NSObject {
    /// Changes the gradient left color
    public var titleGradientLeft: UIColor
    /// Changes the gradient right color
    public var titleGradientRight: UIColor
    /// Changes the background color of the link area
    public var linkBackgroundColor: UIColor

    ///
    public init(titleGradientLeft: UIColor, titleGradientRight: UIColor, linkBackgroundColor: UIColor) {
        self.titleGradientLeft = titleGradientLeft
        self.titleGradientRight = titleGradientRight
        self.linkBackgroundColor = linkBackgroundColor
    }
}

@objc public class ImageSliderTheme: NSObject {
    /// Changes the title background color
    public var titleBackgroundColor: UIColor
    /// Changes the track gradient left color
    public var trackGradientLeft: UIColor
    /// Changes the track gradient right color
    public var trackGradientRight: UIColor
    /// Changes the track minimum tint color
    public var trackMinimumTint: UIColor
    /// Changes the track maximum tint color
    public var trackMaximumTint: UIColor
    /// Changes the results hot color
    public var resultsHotColor: UIColor
    /// Changes the results cold color
    public var resultsColdColor: UIColor
    /// Changes the margins of the title
    public var titleMargins: UIEdgeInsets

    public var submitButton: Theme.SubmitButton

    public var footer: Theme.Container

    /// Defaults
    public override init() {
        titleBackgroundColor = UIColor(rInt: 0, gInt: 0, bInt: 0, alpha: 1)
        trackGradientLeft = UIColor(rInt: 255, gInt: 240, bInt: 0)
        trackGradientRight = UIColor(rInt: 160, gInt: 255, bInt: 40)
        trackMinimumTint = .clear
        trackMaximumTint = .white
        resultsHotColor = UIColor(rInt: 255, gInt: 5, bInt: 45)
        resultsColdColor = UIColor(rInt: 60, gInt: 30, bInt: 255)
        titleMargins = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: -2)
        submitButton = Theme.SubmitButton()
        footer = Theme.Container(
            background: .fill(color: .black),
            borderColor: .clear,
            borderWidth: 0,
            cornerRadii: Theme.CornerRadii(all: 0)
        )
    }
}
