//
//  AskWidgetTheme.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 7/29/21.
//

import UIKit

extension Theme {
    /// Customizable properties of the Text Ask Widget
    public struct TextAskWidget {
        public init(
            main: Theme.Container,
            header: Theme.Container,
            body: Theme.Container,
            title: Theme.Text,
            prompt: Theme.Text,
            confirmation: Theme.Text,
            submitButtonEnabled: Theme.Container,
            submitTextEnabled: Theme.Text,
            submitButtonDisabled: Theme.Container,
            submitTextDisabled: Theme.Text,
            placeholder: Text,
            response: Text,
            responseTextDisabled: Text,
            textView: Container,
            characterCount: Text
        ) {
            self.main = main
            self.header = header
            self.body = body
            self.title = title
            self.prompt = prompt
            self.confirmation = confirmation
            self.submitButtonEnabled = submitButtonEnabled
            self.submitTextEnabled = submitTextEnabled
            self.submitButtonDisabled = submitButtonDisabled
            self.submitTextDisabled = submitTextDisabled
            self.placeholder = placeholder
            self.reply = response
            self.replyTextDisabled = responseTextDisabled
            self.replyTextView = textView
            self.characterCount = characterCount
        }

        /// Default init
        public init() {
            let widgetTextColor: UIColor = .white
            let widgetTitleFont = UIFont.preferredFont(forTextStyle: .caption1).livelike_bold()
            let widgetOptionTextFont = UIFont.preferredFont(forTextStyle: .subheadline)
            let widgetCornerRadius: CGFloat = 6

            self.init(
                main: Container(
                    background: .fill(color: .black),
                    borderColor: .clear,
                    borderWidth: 0,
                    cornerRadii: CornerRadii(all: widgetCornerRadius)
                ),
                header: Container(
                    background: .gradient(
                        gradient: Background.Gradient(
                            colors: [#colorLiteral(red: 0.6235294118, green: 0.01568627451, blue: 0.1058823529, alpha: 1), #colorLiteral(red: 0.9607843137, green: 0.3176470588, blue: 0.3725490196, alpha: 1)],
                            start: CGPoint(x: 0, y: 0.5),
                            end: CGPoint(x: 1, y: 0.5)
                        )
                    ),
                    borderColor: .clear,
                    borderWidth: 0,
                    cornerRadii: .zero
                ),
                body: Container(
                    background: .fill(color: .black),
                    borderColor: .clear,
                    borderWidth: 0,
                    cornerRadii: .zero
                ),
                title: Text(
                    color: widgetTextColor,
                    font: widgetTitleFont
                ),
                prompt: Text(
                    color: widgetTextColor,
                    font: widgetOptionTextFont
                ),
                confirmation: Text(
                    color: UIColor(red: 0, green: 240 / 255, blue: 120 / 255, alpha: 1.0),
                    font: .systemFont(ofSize: 12, weight: .regular)
                ),
                submitButtonEnabled: Container(
                    background: .fill(color: .white),
                    borderColor: .clear,
                    borderWidth: 0,
                    cornerRadii: CornerRadii(all: 4)
                ),
                submitTextEnabled: Text(
                    color: .black,
                    font: .systemFont(ofSize: 10, weight: .bold)
                ),
                submitButtonDisabled: Container(
                    background: .fill(color: UIColor(red: 84 / 255, green: 84 / 255, blue: 84 / 255, alpha: 1.0)),
                    borderColor: .clear,
                    borderWidth: 0,
                    cornerRadii: CornerRadii(all: 4)
                ),
                submitTextDisabled: Text(
                    color: UIColor(red: 171 / 255, green: 171 / 255, blue: 171 / 255, alpha: 1.0),
                    font: .systemFont(ofSize: 10, weight: .bold)
                ),
                placeholder: Text(
                    color: UIColor(red: 171 / 255, green: 171 / 255, blue: 171 / 255, alpha: 1.0),
                    font: .systemFont(ofSize: 12, weight: .regular)
                ),
                response: Text(
                    color: .white,
                    font: .systemFont(ofSize: 12, weight: .regular)
                ),
                responseTextDisabled: Text(
                    color: UIColor(red: 171 / 255, green: 171 / 255, blue: 171 / 255, alpha: 1.0),
                    font: .systemFont(ofSize: 12, weight: .regular)
                ),
                textView: Container(
                    background: .fill(color: UIColor(red: 40 / 255, green: 40 / 255, blue: 40 / 255, alpha: 1.0)),
                    borderColor: UIColor(red: 63 / 255, green: 63 / 255, blue: 63 / 255, alpha: 1.0),
                    borderWidth: 1,
                    cornerRadii: CornerRadii(all: 4)
                ),
                characterCount: Text(
                    color: UIColor(red: 171 / 255, green: 171 / 255, blue: 171 / 255, alpha: 1.0),
                    font: .systemFont(ofSize: 10, weight: .regular)
                )
            )
        }

        public var main: Container
        public var header: Container
        public var body: Container

        public var title: Text
        public var prompt: Text
        public var confirmation: Text

        public var submitButtonEnabled: Container
        public var submitTextEnabled: Text

        public var submitButtonDisabled: Container
        public var submitTextDisabled: Text

        // The theme used for the placeholder text
        public var placeholder: Text
        // The theme used for the user's reply text
        public var reply: Text
        // The theme used for the user's reply text after submission
        public var replyTextDisabled: Text
        // The theme used for the reply text view
        public var replyTextView: Container

        public var characterCount: Text
    }
}
