//
//  Theme+JSON.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 4/28/20.
//

import Foundation
import UIKit
import LiveLikeCore

enum ThemeErrors: Error {
    case unsupportedBackgroundProperty
    case invalidColorValue
    case invalidCornerRadii
}

// MARK: Theme Extensions

extension Theme {

    /// Creates a `Theme` from a theme json
    /// - Parameter jsonObject: A theme json object compatable with `JSONSerialization.data(withJSONObject:options:)`
    public static func create(fromJSONObject jsonObject: Any) throws -> Theme {
        let decoder = LLJSONDecoder()
        let data = try JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
        let resource = try decoder.decode(ThemeResource.self, from: data)

        let theme = Theme()

        if let quiz = resource.widgets.quiz {
            try theme.widgets.quiz.apply(quiz)
        }

        if let poll = resource.widgets.poll {
            try theme.widgets.poll.apply(poll)
        }

        if let prediction = resource.widgets.prediction {
            try theme.widgets.prediction.apply(prediction)
        }

        if let alert = resource.widgets.alert {
            try theme.widgets.alert.apply(alert)
        }

        if let videoAlert = resource.widgets.videoAlert {
            try theme.widgets.videoAlert.apply(videoAlert)
        }

        if let textAsk = resource.widgets.textAsk {
            theme.widgets.textAsk = textAsk
        }

        if let numberPrediction = resource.widgets.numberPrediction {
            theme.widgets.numberPrediction = numberPrediction
        }

        if let imageSlider = resource.widgets.imageSlider {
            try theme.widgets.imageSlider.apply(imageSlider)
        }

        if let cheerMeter = resource.widgets.cheerMeter {
            theme.widgets.cheerMeter = cheerMeter
        }

        return theme
    }

    private static func background(from backgroundProperty: BackgroundProperty) throws -> Background {
        switch backgroundProperty {
        case .fill(let fill):
            return .fill(color: try Theme.uiColor(from: fill.color))
        case .uniformGradient(let gradient):
            return .gradient(
                gradient: Background.Gradient(
                    colors: try gradient.colors.map { try Theme.uiColor(from: $0) },
                    start: CGPoint(x: 0, y: 0.5),
                    end: CGPoint(x: 1, y: 0.5)
                )
            )
        case .unsupported:
            throw ThemeErrors.unsupportedBackgroundProperty
        }
    }

    private static func uiColor(from colorValue: ColorValue) throws -> UIColor {
        guard let color = UIColor(hexaRGBA: colorValue) else {
            throw ThemeErrors.invalidColorValue
        }
        return color
    }

    private static func uiFont(
        fontNames: [String]?,
        fontWeight: FontWeight?,
        fontSize: Number?,
        fallbackFont: Theme.TextFont
    ) -> UIFont {

        if
            fontWeight == nil,
            fontNames == nil,
            fontSize == nil
        {
            return fallbackFont
        }

        let validFontSize: CGFloat = {
            if let fontSize = fontSize {
                return CGFloat(fontSize)
            } else {
                return fallbackFont.pointSize
            }
        }()

        let validFontWeight: FontWeight = {
            if let fontWeight = fontWeight {
                return fontWeight
            } else if let face = fallbackFont.fontDescriptor.object(forKey: .face) as? String,
                      let validFontWeight = FontWeight(rawValue: face)
            {
                return validFontWeight
            } else {
                return .normal
            }
        }()

        if let fontNames = fontNames {
            var fontNamesSet: Set<String> = Set()
            var familyNames = UIFont.familyNames
            if #available(iOS 15.0, *) {
                // Filter out System Font to avoid console spam for missing fonts
                // https://developer.apple.com/forums/thread/685638
                familyNames.removeAll { $0 == "System Font" }
            }
            familyNames.forEach { fontFamily in
                let fontNames = UIFont.fontNames(forFamilyName: fontFamily)
                fontNames.forEach { fontName in
                    fontNamesSet.insert(fontName)
                }
            }

            for fontName in fontNames {
                if fontNamesSet.contains(fontName) {
                    return Theme.uiFont(
                        fontName: fontName,
                        size: CGFloat(validFontSize),
                        weight: validFontWeight.uiFontWeight
                    )
                }
            }
        }
        let fallbackFontDescriptor = fallbackFont.fontDescriptor.withWeight(validFontWeight.uiFontWeight)
        return UIFont(descriptor: fallbackFontDescriptor, size: CGFloat(validFontSize))
    }

    private static func uiFont(fontName: String, size: CGFloat, weight: UIFont.Weight) -> UIFont {
        let fontDescriptor = UIFontDescriptor(name: fontName, size: size)
            .withWeight(weight)
        return UIFont(descriptor: fontDescriptor, size: CGFloat(size))
    }

    private static func cornerRadii(borderRadius: [Number]) throws -> CornerRadii {
        guard
            let topLeft = borderRadius[safe: 0],
            let topRight = borderRadius[safe: 1],
            let bottomLeft = borderRadius[safe: 2],
            let bottomRight = borderRadius[safe: 3]
        else {
            throw ThemeErrors.invalidCornerRadii
        }

        return CornerRadii(
            topLeft: CGFloat(topLeft),
            topRight: CGFloat(topRight),
            bottomLeft: CGFloat(bottomLeft),
            bottomRight: CGFloat(bottomRight)
        )
    }
}

private extension UIFontDescriptor {
    func withWeight(_ weight: UIFont.Weight) -> UIFontDescriptor {
        var attributes = fontAttributes
        var traits = (attributes[.traits] as? [UIFontDescriptor.TraitKey: Any]) ?? [:]
        traits[.weight] = weight
        attributes[.traits] = traits
        return UIFontDescriptor(fontAttributes: attributes)
    }
}

private enum PickerComponentState {
    case unselected
    case selected
    case correct
    case incorrect
}

private struct PickerComponentGroup {
    var option: Component?
    var description: Component?
    var image: Component?
    var percentage: Component?
    var bar: Component?

    init(
        pickerComponents: PickerComponentsDecodable,
        state: PickerComponentState
    ) {
        switch state {
        case .correct:
            option = pickerComponents.correctOption
            description = pickerComponents.correctOptionDescription
            image = pickerComponents.correctOptionImage
            percentage = pickerComponents.correctOptionPercentage
            bar = pickerComponents.correctOptionBar
        case .incorrect:
            option = pickerComponents.incorrectOption
            description = pickerComponents.incorrectOptionDescription
            image = pickerComponents.incorrectOptionImage
            percentage = pickerComponents.incorrectOptionPercentage
            bar = pickerComponents.incorrectOptionBar
        case .selected:
            option = pickerComponents.selectedOption
            description = pickerComponents.selectedOptionDescription
            image = pickerComponents.selectedOptionImage
            percentage = pickerComponents.selectedOptionPercentage
            bar = pickerComponents.selectedOptionBar
        case .unselected:
            option = pickerComponents.unselectedOption
            description = pickerComponents.unselectedOptionDescription
            image = pickerComponents.unselectedOptionImage
            percentage = pickerComponents.unselectedOptionPercentage
            bar = pickerComponents.unselectedOptionBar
        }
    }
}

private extension Theme.Container {
    mutating func apply(_ component: Component?) throws {
        // do nothing if nil
        guard let component = component else { return }
        if let background = component.background {
            self.background = try Theme.background(from: background)
        }

        if let borderColor = component.borderColor {
            self.borderColor = try Theme.uiColor(from: borderColor)
        }

        if let borderWidth = component.borderWidth {
            self.borderWidth = CGFloat(borderWidth)
        }

        if let cornerRadii = component.borderRadius {
            self.cornerRadii = try Theme.cornerRadii(borderRadius: cornerRadii)
        }
    }
}

private extension Theme.ChoiceWidget {
    mutating func apply(_ widgetComponents: LayoutAndPickerComponents) throws {
        if let main = widgetComponents.root {
            try self.main.apply(main)
        }

        if let header = widgetComponents.header {
            try self.header.apply(header)
        }

        if let body = widgetComponents.body {
            try self.body.apply(body)
        }

        if let title = widgetComponents.title {
            try self.title.apply(title)
        }

        try self.selectedOption.apply(PickerComponentGroup(
            pickerComponents: widgetComponents,
            state: .selected
        )
        )

        try self.unselectedOption.apply(PickerComponentGroup(
            pickerComponents: widgetComponents,
            state: .unselected
        )
        )

        try self.correctOption?.apply(PickerComponentGroup(
            pickerComponents: widgetComponents,
            state: .correct
        )
        )

        try self.incorrectOption?.apply(PickerComponentGroup(
            pickerComponents: widgetComponents,
            state: .incorrect
        )
        )

        try self.submitButton?.apply(widgetComponents)

    }
}

private extension Theme.AlertWidget {
    mutating func apply(_ alertWidget: LayoutComponents) throws {
        if let main = alertWidget.root {
            try self.main.apply(main)
        }

        if let header = alertWidget.header {
            try self.header.apply(header)
        }

        if let title = alertWidget.title {
            try self.title.apply(title)
        }

        if let body = alertWidget.body {
            try self.body.apply(body)
            try self.description.apply(body)
        }

        if let footer = alertWidget.footer {
            try self.footer.apply(footer)
            try self.link.apply(footer)
        }
    }
}

private extension Theme.VideoAlertWidget {
    mutating func apply(_ videoAlertWidget: LayoutComponents) throws {
        if let main = videoAlertWidget.root {
            try self.main.apply(main)
        }

        if let header = videoAlertWidget.header {
            try self.header.apply(header)
        }

        if let title = videoAlertWidget.title {
            try self.title.apply(title)
        }

        if let body = videoAlertWidget.body {
            try self.body.apply(body)
            try self.description.apply(body)
        }

        if let footer = videoAlertWidget.footer {
            try self.footer.apply(footer)
            try self.link.apply(footer)
        }
    }
}

private extension Theme.ChoiceWidget.Option {
    mutating func apply(_ option: PickerComponentGroup) throws {
        if let container = option.option {
            try self.container.apply(container)
        }

        if let description = option.description {
            try self.description.apply(description)
        }

        if let percentage = option.percentage {
            try self.percentage.apply(percentage)
        }

        if let progressBar = option.bar {
            try self.progressBar.apply(progressBar)
        }
    }
}

private extension Theme.SubmitButton {
    mutating func apply(_ component: LayoutAndPickerComponents) throws {
        try self.apply(
            submitButtonEnabled: component.submitButtonEnabled,
            submitButtonDisabled: component.submitButtonDisabled,
            submitConfirmation: component.confirmation
        )
    }

    mutating func apply(
        submitButtonEnabled: Component?,
        submitButtonDisabled: Component?,
        submitConfirmation: Component?
    ) throws {
        if let submitButtonEnabled = submitButtonEnabled {
            try self.buttonEnabled.apply(submitButtonEnabled)
            try self.textEnabled.apply(submitButtonEnabled)
        }

        if let submitButtonDisabled = submitButtonDisabled {
            try self.buttonDisabled.apply(submitButtonDisabled)
            try self.textDisabled.apply(submitButtonDisabled)
        }

        if let submitConfirmation = submitConfirmation {
            try self.confirmation.apply(submitConfirmation)
        }
    }
}

private extension Theme.ProgressBar {
    mutating func apply(_ component: Component) throws {
        if let background = component.background {
            self.background = try Theme.background(from: background)
        }

        if let borderColor = component.borderColor {
            self.borderColor = try Theme.uiColor(from: borderColor)
        }

        if let borderWidth = component.borderWidth {
            self.borderWidth = CGFloat(borderWidth)
        }

        if let cornerRadii = component.borderRadius {
            self.cornerRadii = try Theme.cornerRadii(borderRadius: cornerRadii)
        }
    }
}

extension Theme.TextAskWidget: Decodable {
    public init(from decoder: Decoder) throws {
        enum CodingKeys: CodingKey {
            case root
            case header
            case body
            case title
            case prompt
            case confirmation
            case submitButtonEnabled
            case submitButtonDisabled
            case replyPlaceholder
            case replyEnabled
            case replyDisabled
            case characterCount
        }

        // Initialize with defaults
        self.init()

        // Apply json properties
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let root = try? container.decode(Component.self, forKey: .root) {
            try self.main.apply(root)
        }

        if let header = try? container.decode(Component.self, forKey: .header) {
            try self.header.apply(header)
        }

        if let body = try? container.decode(Component.self, forKey: .body) {
            try self.body.apply(body)
        }

        if let title = try? container.decode(Component.self, forKey: .title) {
            try self.title.apply(title)
        }

        if let prompt = try? container.decode(Component.self, forKey: .prompt) {
            try self.prompt.apply(prompt)
        }

        if let confirmation = try? container.decode(Component.self, forKey: .confirmation) {
            try self.confirmation.apply(confirmation)
        }

        if let submitButtonEnabled = try? container.decode(Component.self, forKey: .submitButtonEnabled) {
            try self.submitButtonEnabled.apply(submitButtonEnabled)
            try self.submitTextEnabled.apply(submitButtonEnabled)
        }

        if let submitButtonDisabled = try? container.decode(Component.self, forKey: .submitButtonDisabled) {
            try self.submitButtonDisabled.apply(submitButtonDisabled)
            try self.submitTextDisabled.apply(submitButtonDisabled)
        }

        if let replyPlaceholder = try? container.decode(Component.self, forKey: .replyPlaceholder) {
            try self.placeholder.apply(replyPlaceholder)
        }

        if let replyEnabled = try? container.decode(Component.self, forKey: .replyEnabled) {
            try self.reply.apply(replyEnabled)
            try self.replyTextView.apply(replyEnabled)
        }

        if let replyDisabled = try? container.decode(Component.self, forKey: .replyDisabled) {
            try self.replyTextDisabled.apply(replyDisabled)
        }

        if let characterCount = try container.decodeIfPresent(Component.self, forKey: .characterCount) {
            try self.characterCount.apply(characterCount)
        }
    }
}

extension Theme.NumberPredictionWidget: Decodable {
    enum CodingKeys: CodingKey {
        case root
        case header
        case title
        case body
        case option
        case optionInputFieldEnabled
        case optionInputFieldDisabled
        case optionInputFieldPlaceholder
        case optionInputFieldCorrect
        case optionInputFieldIncorrect
        case submitButtonEnabled
        case submitButtonDisabled
        case submitConfirmation
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.init()

        if let main = try container.decodeIfPresent(Component.self, forKey: .root) {
            try self.main.apply(main)
        }
        if let header = try container.decodeIfPresent(Component.self, forKey: .header) {
            try self.header.apply(header)
        }
        if let title = try container.decodeIfPresent(Component.self, forKey: .title) {
            try self.title.apply(title)
        }
        if let body = try container.decodeIfPresent(Component.self, forKey: .body) {
            try self.body.apply(body)
        }
        if let component = try container.decodeIfPresent(Component.self, forKey: .option) {
            try self.optionContainer.apply(component)
            try self.optionText.apply(component)
        }
        if let component = try container.decodeIfPresent(Component.self, forKey: .optionInputFieldEnabled) {
            try self.optionInputFieldContainerEnabled.apply(component)
            try self.optionInputFieldTextEnabled.apply(component)
        }
        if let component = try container.decodeIfPresent(Component.self, forKey: .optionInputFieldDisabled) {
            try self.optionInputFieldContainerDisabled.apply(component)
            try self.optionInputFieldTextDisabled.apply(component)
        }
        if let component = try container.decodeIfPresent(Component.self, forKey: .optionInputFieldPlaceholder) {
            try self.optionInputFieldPlaceholder.apply(component)
        }
        if let component = try container.decodeIfPresent(Component.self, forKey: .optionInputFieldCorrect) {
            try self.optionInputFieldCorrectContainer.apply(component)
            try self.optionInputFieldCorrectText.apply(component)
        }
        if let component = try container.decodeIfPresent(Component.self, forKey: .optionInputFieldIncorrect) {
            try self.optionInputFieldIncorrectContainer.apply(component)
            try self.optionInputFieldIncorrectText.apply(component)
        }

        if let component = try container.decodeIfPresent(Component.self, forKey: .submitButtonEnabled) {
            try self.submitButton.buttonEnabled.apply(component)
            try self.submitButton.textEnabled.apply(component)
        }

        if let component = try container.decodeIfPresent(Component.self, forKey: .submitButtonDisabled) {
            try self.submitButton.textDisabled.apply(component)
            try self.submitButton.buttonDisabled.apply(component)
        }

        if let component = try container.decodeIfPresent(Component.self, forKey: .submitConfirmation) {
            try self.submitButton.confirmation.apply(component)
        }
    }
}

private extension Theme.Text {
    mutating func apply(_ component: Component) throws {
        if let textColor = component.fontColor {
            self.color = try Theme.uiColor(from: textColor)
        }

        self.font = Theme.uiFont(
            fontNames: component.fontFamily,
            fontWeight: component.fontWeight,
            fontSize: component.fontSize,
            fallbackFont: self.font
        )
    }
}

internal extension UIColor {
    convenience init?(hexaRGBA: String) {
        var chars = Array(hexaRGBA.hasPrefix("#") ? hexaRGBA.dropFirst() : hexaRGBA[...])
        switch chars.count {
        case 3: chars = chars.flatMap { [$0, $0] }; fallthrough
        case 6: chars.append(contentsOf: ["F", "F"])
        case 8: break
        default: return nil
        }
        self.init(
            red: .init(strtoul(String(chars[0 ... 1]), nil, 16)) / 255,
            green: .init(strtoul(String(chars[2 ... 3]), nil, 16)) / 255,
            blue: .init(strtoul(String(chars[4 ... 5]), nil, 16)) / 255,
            alpha: .init(strtoul(String(chars[6 ... 7]), nil, 16)) / 255
        )
    }

    func toHexString() -> String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb: Int = (Int)(r * 255) << 16 | (Int)(g * 255) << 8 | (Int)(b * 255) << 0
        return String(format: "#%06x", rgb)
    }
}

// MARK: Type Definitions

private typealias Number = Double
private typealias ColorValue = String

private struct ThemeResource: Decodable {
    var version: Number
    var widgets: Widgets
}

/// Common layout components between most widgets
private protocol LayoutComponentsDecodable: Decodable {
    var root: Component? { get }
    var header: Component? { get }
    var title: Component? { get }
    var timer: Component? { get }
    var dismiss: Component? { get }
    var body: Component? { get }
    var footer: Component? { get }
}

/// Common picker components between some widgets
private protocol PickerComponentsDecodable: Decodable {
    // Unselected
    var unselectedOption: Component? { get }
    var unselectedOptionDescription: Component? { get }
    var unselectedOptionImage: Component? { get }
    var unselectedOptionPercentage: Component? { get }
    var unselectedOptionBar: Component? { get }

    // Selected
    var selectedOption: Component? { get }
    var selectedOptionDescription: Component? { get }
    var selectedOptionImage: Component? { get }
    var selectedOptionPercentage: Component? { get }
    var selectedOptionBar: Component? { get }

    // Correct
    var correctOption: Component? { get }
    var correctOptionDescription: Component? { get }
    var correctOptionImage: Component? { get }
    var correctOptionPercentage: Component? { get }
    var correctOptionBar: Component? { get }

    // Incorrect
    var incorrectOption: Component? { get }
    var incorrectOptionDescription: Component? { get }
    var incorrectOptionImage: Component? { get }
    var incorrectOptionPercentage: Component? { get }
    var incorrectOptionBar: Component? { get }
}

private struct LayoutComponents: LayoutComponentsDecodable {
    var root: Component?
    var header: Component?
    var title: Component?
    var timer: Component?
    var dismiss: Component?
    var body: Component?
    var footer: Component?
}

private struct LayoutAndPickerComponents: LayoutComponentsDecodable, PickerComponentsDecodable {

    // MARK: Layout Components
    var root: Component?
    var header: Component?
    var title: Component?
    var timer: Component?
    var dismiss: Component?
    var body: Component?
    var footer: Component?
    var submitButtonEnabled: Component?
    var submitButtonDisabled: Component?
    var confirmation: Component?

    // MARK: Picker Components

    var unselectedOption: Component?
    var unselectedOptionDescription: Component?
    var unselectedOptionImage: Component?
    var unselectedOptionPercentage: Component?
    var unselectedOptionBar: Component?

    var selectedOption: Component?
    var selectedOptionDescription: Component?
    var selectedOptionImage: Component?
    var selectedOptionPercentage: Component?
    var selectedOptionBar: Component?

    var correctOption: Component?
    var correctOptionDescription: Component?
    var correctOptionImage: Component?
    var correctOptionPercentage: Component?
    var correctOptionBar: Component?

    var incorrectOption: Component?
    var incorrectOptionDescription: Component?
    var incorrectOptionImage: Component?
    var incorrectOptionPercentage: Component?
    var incorrectOptionBar: Component?
}

private struct Component: Decodable {
    var background: BackgroundProperty?
    var borderColor: ColorValue?
    var borderRadius: [Number]?
    var borderWidth: Number?
    var fontColor: ColorValue?
    var fontFamily: [String]?
    var fontWeight: FontWeight?
    var fontSize: Number?
}

private enum BackgroundProperty: Decodable {
    case fill(FillBackground)
    case uniformGradient(UniformGradientBackground)
    case unsupported

    private enum CodingKeys: String, CodingKey {
        case format
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let format = try container.decode(String.self, forKey: .format)

        switch format {
        case "fill":
            self = try .fill(FillBackground(from: decoder))
        case "uniformGradient":
            self = try .uniformGradient(UniformGradientBackground(from: decoder))
        default:
            self = .unsupported
        }
    }

    struct FillBackground: Decodable {
        var color: ColorValue
    }

    struct UniformGradientBackground: Decodable {
        var colors: [ColorValue]
        var direction: Number
    }
}

private enum FontWeight: String, Decodable {
    case light
    case normal
    case bold

    var uiFontWeight: UIFont.Weight {
        switch self {
        case .light:
            return .light
        case .normal:
            return .regular
        case .bold:
            return .bold
        }
    }
}

private struct Widgets: Decodable {
    var alert: LayoutComponents?
    var videoAlert: LayoutComponents?
    var poll: LayoutAndPickerComponents?
    var quiz: LayoutAndPickerComponents?
    var prediction: LayoutAndPickerComponents?
    var imageSlider: ImageSlider?
    var cheerMeter: Theme.CheerMeter?
    var textAsk: Theme.TextAskWidget?
    var numberPrediction: Theme.NumberPredictionWidget?
}

private struct ImageSlider: LayoutComponentsDecodable {
    // MARK: Layout Components
    var root: Component?
    var header: Component?
    var title: Component?
    var timer: Component?
    var dismiss: Component?
    var body: Component?
    var footer: Component?

    // MARK: Image Slider Components
    var interactiveTrackLeft: Component?
    var interactiveTrackRight: Component?
    var resultsTrackLeft: Component?
    var resultsTrackRight: Component?
    var marker: Component?
    var submitButtonEnabled: Component?
    var submitButtonDisabled: Component?
    var confirmation: Component?
}

private extension Theme.ImageSlider {
    mutating func apply(_ imageSlider: ImageSlider) throws {
        if let root = imageSlider.root {
            try self.main.apply(root)
        }

        if let header = imageSlider.header {
            try self.header.apply(header)
        }

        if let title = imageSlider.title {
            try self.title.apply(title)
        }

        if let body = imageSlider.body {
            try self.body.apply(body)
        }

        if let footer = imageSlider.footer {
            try self.footer.apply(footer)
        }

        if let component = imageSlider.interactiveTrackLeft {
            try self.interactiveTrackLeft.apply(component)
        }

        if let component = imageSlider.interactiveTrackRight {
            try self.interactiveTrackRight.apply(component)
        }

        if let component = imageSlider.resultsTrackLeft {
            try self.resultsTrackLeft.apply(component)
        }

        if let component = imageSlider.resultsTrackRight {
            try self.resultsTrackRight.apply(component)
        }

        try self.submitButton.apply(
            submitButtonEnabled: imageSlider.submitButtonEnabled,
            submitButtonDisabled: imageSlider.submitButtonDisabled,
            submitConfirmation: imageSlider.confirmation
        )
    }
}

extension Theme.CheerMeter: Decodable {

    enum CodingKeys: CodingKey {
        case root
        case header
        case title
        case body
        case versus
        case sideABar
        case sideAButton
        case sideBBar
        case sideBButton
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.init()

        if let main = try container.decodeIfPresent(Component.self, forKey: .root) {
            try self.main.apply(main)
        }

        if let header = try container.decodeIfPresent(Component.self, forKey: .header) {
            try self.header.apply(header)
        }

        if let component = try container.decodeIfPresent(Component.self, forKey: .title) {
            try self.title.apply(component)
        }

        if let component = try container.decodeIfPresent(Component.self, forKey: .body) {
            try self.body.apply(component)
        }

        if let component = try container.decodeIfPresent(Component.self, forKey: .versus) {
            try self.versus.apply(component)
        }

        if let component = try container.decodeIfPresent(Component.self, forKey: .sideABar) {
            try self.sideABar.apply(component)
        }

        if let component = try container.decodeIfPresent(Component.self, forKey: .sideAButton) {
            try self.sideAButton.apply(component)
        }

        if let component = try container.decodeIfPresent(Component.self, forKey: .sideBBar) {
            try self.sideBBar.apply(component)
        }

        if let component = try container.decodeIfPresent(Component.self, forKey: .sideBButton) {
            try self.sideBButton.apply(component)
        }
    }
}
