//
//  CheerMeterJSONTheme.swift
//  
//
//  Created by Jelzon Monzon on 4/6/23.
//

import Foundation
import UIKit

extension Theme {
    public struct CheerMeter {
        public init(
            main: Theme.Container,
            header: Theme.Container,
            title: Theme.Text,
            body: Theme.Container,
            score: Theme.Text,
            versus: Container,
            sideABar: Container,
            sideAText: Theme.Text,
            sideAButton: Container,
            sideBBar: Container,
            sideBText: Theme.Text,
            sideBButton: Container
        ) {
            self.main = main
            self.header = header
            self.title = title
            self.body = body
            self.score = score
            self.versus = versus
            self.sideABar = sideABar
            self.sideAText = sideAText
            self.sideAButton = sideAButton
            self.sideBBar = sideBBar
            self.sideBText = sideBText
            self.sideBButton = sideBButton
        }

        public var main: Container

        public var header: Container
        public var title: Text

        public var body: Container
        public var score: Text
        
        public var versus: Container
        
        public var sideABar: Container
        public var sideAText: Text
        public var sideAButton: Container
        
        public var sideBBar: Container
        public var sideBText: Text
        public var sideBButton: Container
        
        // Default theme
        public init() {
            self.main = Container(
                background: .fill(color: .black),
                borderColor: .clear,
                borderWidth: 0,
                cornerRadii: .init(all: 6)
            )
            
            self.header = Container(
                background: .fill(color: .black),
                borderColor: .clear,
                borderWidth: 0,
                cornerRadii: .zero
            )
            
            self.body = Container(
                background: .fill(color: .black),
                borderColor: .clear,
                borderWidth: 0,
                cornerRadii: .zero
            )
            
            self.score = Text(
                color: .white,
                font: UIFont.systemFont(ofSize: 32, weight: .bold)
            )
            
            self.title = Text(
                color: .white,
                font: UIFont.preferredFont(forTextStyle: .caption1).livelike_bold()
            )
            
            self.versus = Container(
                background: .fill(color: .black),
                borderColor: .clear,
                borderWidth: 0,
                cornerRadii: .zero
            )
        
            self.sideABar = Container(
                background: .gradient(
                    gradient: Background.Gradient(
                        colors: [
                            UIColor(rInt: 80, gInt: 160, bInt: 250),
                            UIColor(rInt: 40, gInt: 40, bInt: 180),
                        ],
                        start: CGPoint(x: 0, y: 0.5),
                        end: CGPoint(x: 1, y: 0.5)
                    )
                ),
                borderColor: .clear,
                borderWidth: 0,
                cornerRadii: .zero
            )
            
            self.sideAText = Text(
                color: .white,
                font: UIFont.systemFont(ofSize: 14)
            )
            
            self.sideAButton = Container(
                background: .fill(color: .black),
                borderColor: .clear,
                borderWidth: 0,
                cornerRadii: .zero
            )
            
            self.sideBBar = Container(
                background: .gradient(
                    gradient: Background.Gradient(
                        colors: [
                            UIColor(rInt: 160, gInt: 0, bInt: 40),
                            UIColor(rInt: 250, gInt: 80, bInt: 100)
                        ],
                        start: CGPoint(x: 0, y: 0.5),
                        end: CGPoint(x: 1, y: 0.5)
                    )
                ),
                borderColor: .clear,
                borderWidth: 0,
                cornerRadii: .zero
            )
            
            self.sideBText = Text(
                color: .white,
                font: UIFont.systemFont(ofSize: 14)
            )
            
            self.sideBButton = Container(
                background: .fill(color: .black),
                borderColor: .clear,
                borderWidth: 0,
                cornerRadii: .zero
            )
            
        }

    }
}
