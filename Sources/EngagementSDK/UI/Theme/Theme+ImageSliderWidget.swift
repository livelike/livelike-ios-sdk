//
//  Theme+ImageSliderWidget.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 11/15/21.
//

import UIKit

public extension Theme {
    struct ImageSlider {
        
        /// Changes the properties of the main container
        public var main: Theme.Container
        /// Changes the properties of the header container
        public var header: Theme.Container
        /// Changes the properties of the title text
        public var title: Theme.Text
        /// Changes the properties of the body container
        public var body: Theme.Container
        /// Changes the properties of the body container
        public var footer: Theme.Container
        
        /// Changes the left side of the interactive track container
        public var interactiveTrackLeft: Theme.Container
        /// Changes the right side of the interactive track container
        public var interactiveTrackRight: Theme.Container
        /// Changes the left side of the results track container
        public var resultsTrackLeft: Theme.Container
        /// Changes the right side of the results track container
        public var resultsTrackRight: Theme.Container
        
        /// Changes the properties of the submit button
        public var submitButton: SubmitButton
        
        /// Default init
        public init() {
            main = Theme.Container(
                background: .fill(color: .clear),
                borderColor: .clear,
                borderWidth: 0,
                cornerRadii: .init(all: 6)
            )
            header = Theme.Container(
                background: .fill(color: .black),
                borderColor: .clear,
                borderWidth: 0,
                cornerRadii: .zero
            )
            title = Theme.Text(
                color: .white,
                font: UIFont.preferredFont(forTextStyle: .caption1).livelike_bold()
            )
            body = Theme.Container(
                background: .fill(color: .black),
                borderColor: .clear,
                borderWidth: 0,
                cornerRadii: .zero
            )
            footer = Theme.Container(
                background: .fill(color: .black),
                borderColor: .clear,
                borderWidth: 0,
                cornerRadii: .zero
            )
            interactiveTrackLeft = Theme.Container(
                background: .fill(color: UIColor(rInt: 255, gInt: 240, bInt: 0)),
                borderColor: .clear,
                borderWidth: 0,
                cornerRadii: .zero
            )
            interactiveTrackRight = Theme.Container(
                background: .fill(color: UIColor(rInt: 160, gInt: 255, bInt: 40)),
                borderColor: .clear,
                borderWidth: 0,
                cornerRadii: .zero
            )
            resultsTrackLeft = Theme.Container(
                background: .gradient(
                    gradient: .init(colors: [
                        UIColor(rInt: 60, gInt: 30, bInt: 255),
                        UIColor(rInt: 255, gInt: 5, bInt: 45)
                    ])
                ),
                borderColor: .clear,
                borderWidth: 0,
                cornerRadii: .zero
            )
            resultsTrackRight = Theme.Container(
                background: .gradient(
                    gradient: .init(colors: [
                        UIColor(rInt: 255, gInt: 5, bInt: 45),
                        UIColor(rInt: 60, gInt: 30, bInt: 255)
                    ])
                ),
                borderColor: .clear,
                borderWidth: 0,
                cornerRadii: .zero
            )
            
            submitButton = SubmitButton()
        }
    }
}
