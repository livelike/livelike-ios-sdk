//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//

import UIKit

// swiftlint:disable nesting
extension Theme {
    public typealias BorderWidth = CGFloat
    public typealias BorderColor = UIColor

    public struct CornerRadii {
        public init(
            topLeft: CornerRadius,
            topRight: CornerRadius,
            bottomLeft: CornerRadius,
            bottomRight: CornerRadius
        ) {
            self.topLeft = topLeft
            self.topRight = topRight
            self.bottomLeft = bottomLeft
            self.bottomRight = bottomRight
        }

        public init(all: CornerRadius) {
            self.init(topLeft: all, topRight: all, bottomLeft: all, bottomRight: all)
        }

        public typealias CornerRadius = CGFloat

        public var topLeft: CornerRadius
        public var topRight: CornerRadius
        public var bottomLeft: CornerRadius
        public var bottomRight: CornerRadius

        public static var zero = CornerRadii(all: 0)
        public var average: CornerRadius {
            return [topLeft, topRight, bottomLeft, bottomRight].reduce(0, +) / 4
        }
    }
    public typealias TextColor = UIColor
    public typealias TextFont = UIFont

    public struct Text {
        public init(
            color: Theme.TextColor,
            font: Theme.TextFont
        ) {
            self.color = color
            self.font = font
        }

        public var color: TextColor
        public var font: TextFont
    }

    public struct Container {
        public init(
            background: Theme.Background,
            borderColor: Theme.BorderColor,
            borderWidth: Theme.BorderWidth,
            cornerRadii: Theme.CornerRadii
        ) {
            self.background = background
            self.borderColor = borderColor
            self.borderWidth = borderWidth
            self.cornerRadii = cornerRadii
        }

        public var background: Background
        public var borderColor: BorderColor
        public var borderWidth: BorderWidth
        public var cornerRadii: CornerRadii
    }

    public struct ProgressBar {
        public init(
            background: Theme.Background,
            borderColor: Theme.BorderColor,
            borderWidth: Theme.BorderWidth,
            cornerRadii: Theme.CornerRadii
        ) {
            self.background = background
            self.borderColor = borderColor
            self.borderWidth = borderWidth
            self.cornerRadii = cornerRadii
        }

        public var background: Background
        public var borderColor: BorderColor
        public var borderWidth: BorderWidth
        public var cornerRadii: CornerRadii
    }

    /// Theme options for submit buttons on Widgets
    public struct SubmitButton {
        public init() {
            self.init(
                buttonEnabled: Theme.Container(
                    background: .fill(color: UIColor(red: 0.5, green: 0, blue: 0.5, alpha: 1.0)),
                    borderColor: .clear,
                    borderWidth: 0,
                    cornerRadii: .init(all: 6)
                ),
                textEnabled: Theme.Text(
                    color: .white,
                    font: UIFont.preferredFont(forTextStyle: .subheadline)
                ),
                buttonDisabled: Theme.Container(
                    background: .fill(color: UIColor(red: 0.5, green: 0, blue: 0.5, alpha: 0.5)),
                    borderColor: .clear,
                    borderWidth: 0,
                    cornerRadii: .init(all: 6)
                ),
                textDisabled: Theme.Text(
                    color: UIColor(white: 1.0, alpha: 0.5),
                    font: UIFont.preferredFont(forTextStyle: .subheadline)
                ),
                confirmation: Theme.Text(
                    color: .white,
                    font: .systemFont(ofSize: 14, weight: .regular)
                )
            )
        }

        init(
            buttonEnabled: Theme.Container,
            textEnabled: Theme.Text,
            buttonDisabled: Theme.Container,
            textDisabled: Theme.Text,
            confirmation: Theme.Text
        ) {
            self.buttonEnabled = buttonEnabled
            self.textEnabled = textEnabled
            self.buttonDisabled = buttonDisabled
            self.textDisabled = textDisabled
            self.confirmation = confirmation
        }

        /// The button container when enabled
        public var buttonEnabled: Theme.Container
        /// The text when enabled
        public var textEnabled: Theme.Text
        /// The button container when disabled
        public var buttonDisabled: Theme.Container
        /// The text when disabled
        public var textDisabled: Theme.Text
        /// The theme of the submit confirmation text
        public var confirmation: Theme.Text
    }

    public struct ChoiceWidget {
        public init(
            main: Theme.Container,
            header: Theme.Container,
            body: Theme.Container,
            title: Theme.Text,
            footer: Theme.Container,
            correctOption: Theme.ChoiceWidget.Option? = nil,
            incorrectOption: Theme.ChoiceWidget.Option? = nil,
            selectedOption: Theme.ChoiceWidget.Option,
            unselectedOption: Theme.ChoiceWidget.Option,
            submitButton: SubmitButton?
        ) {
            self.main = main
            self.header = header
            self.body = body
            self.title = title
            self.footer = footer
            self.correctOption = correctOption
            self.incorrectOption = incorrectOption
            self.selectedOption = selectedOption
            self.unselectedOption = unselectedOption
            self.submitButton = submitButton
        }

        public var main: Container
        public var header: Container
        public var body: Container
        public var title: Text
        public var footer: Container

        public var correctOption: Option?
        public var incorrectOption: Option?
        public var selectedOption: Option
        public var unselectedOption: Option

        public var submitButton: SubmitButton?

        public struct Option {
            public init(
                container: Theme.Container,
                description: Theme.Text,
                percentage: Theme.Text,
                progressBar: Theme.ProgressBar,
                imageHorizontalOffset: CGFloat = .zero,
                imageVerticalOffset: CGFloat = .zero,
                imageContentMode: UIImageView.ContentMode = .scaleAspectFit
            ) {
                self.container = container
                self.description = description
                self.percentage = percentage
                self.progressBar = progressBar
                self.imageHorizontalOffset = imageHorizontalOffset
                self.imageVerticalOffset = imageVerticalOffset
                self.imageContentMode = imageContentMode
            }

            public var container: Container
            public var description: Text
            public var percentage: Text
            public var progressBar: ProgressBar
            /// Allows modifying the horizontal position of the image
            public var imageHorizontalOffset: CGFloat
            /// Allows modifying the vertical position of the image in the widget choice option
            public var imageVerticalOffset: CGFloat
            /// Allows modifying the content mode of the image view in the widget choice option
            public var imageContentMode: UIImageView.ContentMode
        }

        /// Sets a horizontal offset to all states of the option
        public mutating func setImageHorizontalOffset(horizontalOffset: CGFloat) {
            self.unselectedOption.imageHorizontalOffset = horizontalOffset
            self.selectedOption.imageHorizontalOffset = horizontalOffset
            self.incorrectOption?.imageHorizontalOffset = horizontalOffset
            self.correctOption?.imageHorizontalOffset = horizontalOffset
        }

        /// Sets a vertical offset to all states of the option
        public mutating func setImageVerticalOffset(verticalOffset: CGFloat) {
            self.unselectedOption.imageVerticalOffset = verticalOffset
            self.selectedOption.imageVerticalOffset = verticalOffset
            self.incorrectOption?.imageVerticalOffset = verticalOffset
            self.correctOption?.imageVerticalOffset = verticalOffset
        }

        /// Sets a content mode to images for all states of the option
        public mutating func setImageContentMode(contentMode: UIImageView.ContentMode) {
            self.unselectedOption.imageContentMode = contentMode
            self.selectedOption.imageContentMode = contentMode
            self.incorrectOption?.imageContentMode = contentMode
            self.correctOption?.imageContentMode = contentMode
        }
    }
}
