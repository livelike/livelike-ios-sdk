//
//  Theme+NumberPredictionWidget.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 9/30/21.
//

import UIKit

extension Theme {
    /// Customizable properties of the Number Prediction Widget
    public struct NumberPredictionWidget {
        public init(
            main: Theme.Container,
            header: Theme.Container,
            title: Theme.Text,
            body: Theme.Container,
            footer: Theme.Container,
            optionContainer: Container,
            optionText: Text,
            optionInputFieldContainerEnabled: Container,
            optionInputFieldContainerDisabled: Container,
            optionInputFieldTextEnabled: Text,
            optionInputFieldTextDisabled: Text,
            optionInputFieldPlaceholder: Text,
            submitButton: SubmitButton,
            correctOptionContainer: Container,
            incorrectOptionContainer: Container,
            correctOptionText: Text,
            incorrectOptionText: Text
        ) {
            self.main = main
            self.header = header
            self.title = title
            self.body = body
            self.footer = footer
            self.optionContainer = optionContainer
            self.optionText = optionText
            self.optionInputFieldContainerEnabled = optionInputFieldContainerEnabled
            self.optionInputFieldContainerDisabled = optionInputFieldContainerDisabled
            self.optionInputFieldTextEnabled = optionInputFieldTextEnabled
            self.optionInputFieldTextDisabled = optionInputFieldTextDisabled
            self.optionInputFieldPlaceholder = optionInputFieldPlaceholder
            self.optionInputFieldCorrectContainer = correctOptionContainer
            self.optionInputFieldIncorrectContainer = incorrectOptionContainer
            self.optionInputFieldCorrectText = correctOptionText
            self.optionInputFieldIncorrectText = incorrectOptionText
            self.submitButton = submitButton
        }

        public init() {
            let widgetTextColor = UIColor.white
            let widgetTitleFont = UIFont.preferredFont(forTextStyle: .caption1).livelike_bold()
            let widgetOptionTextFont = UIFont.systemFont(ofSize: 16, weight: .bold)
            let inputFieldFont = UIFont.systemFont(ofSize: 20, weight: .bold)
            let optionTextColor = UIColor(red: 27 / 255, green: 198 / 255, blue: 1.0, alpha: 1.0)

            self.init(
                main: Container(
                    background: .fill(color: .black),
                    borderColor: .clear,
                    borderWidth: 0,
                    cornerRadii: CornerRadii(all: 6)
                ),
                header: Container(
                    background: .gradient(
                        gradient: Background.Gradient(
                            colors: [#colorLiteral(red: 0, green: 0.1960784314, blue: 0.3921568627, alpha: 1), #colorLiteral(red: 0, green: 0.5882352941, blue: 0.7843137255, alpha: 1)],
                            start: CGPoint(x: 0, y: 0.5),
                            end: CGPoint(x: 1, y: 0.5)
                        )
                    ),
                    borderColor: .clear,
                    borderWidth: 0,
                    cornerRadii: CornerRadii(all: 0)
                ),
                title: Text(
                    color: .white,
                    font: widgetTitleFont
                ),
                body: Container(
                    background: .fill(color: .black),
                    borderColor: .clear,
                    borderWidth: 0,
                    cornerRadii: CornerRadii(all: 0)
                ),
                footer: Container(
                    background: .fill(color: .black),
                    borderColor: .clear,
                    borderWidth: 0,
                    cornerRadii: CornerRadii(all: 0)
                ),
                optionContainer: Container(
                    background: .fill(color: .clear),
                    borderColor: .clear,
                    borderWidth: 0,
                    cornerRadii: .zero
                ),
                optionText: Text(
                    color: widgetTextColor,
                    font: widgetOptionTextFont
                ),
                optionInputFieldContainerEnabled: Container(
                    background: .fill(color: UIColor(white: 51 / 255, alpha: 1.0)),
                    borderColor: UIColor(white: 92 / 255, alpha: 1.0),
                    borderWidth: 1,
                    cornerRadii: CornerRadii(all: 4)
                ),
                optionInputFieldContainerDisabled: Container(
                    background: .fill(color: UIColor(white: 51 / 255, alpha: 1.0)),
                    borderColor: UIColor(white: 92 / 255, alpha: 1.0),
                    borderWidth: 1,
                    cornerRadii: CornerRadii(all: 4)
                ),
                optionInputFieldTextEnabled: Text(
                    color: optionTextColor,
                    font: inputFieldFont
                ),
                optionInputFieldTextDisabled: Text(
                    color: optionTextColor,
                    font: inputFieldFont
                ),
                optionInputFieldPlaceholder: Text(
                    color: UIColor(white: 95 / 255, alpha: 1.0),
                    font: inputFieldFont
                ),
                submitButton: SubmitButton(
                    buttonEnabled: Container(
                        background: .fill(color: .white),
                        borderColor: .clear,
                        borderWidth: 0,
                        cornerRadii: CornerRadii(all: 4)
                    ),
                    textEnabled: Text(
                        color: .black,
                        font: .systemFont(ofSize: 14, weight: .bold)
                    ),
                    buttonDisabled: Container(
                        background: .fill(color: UIColor(white: 84 / 255, alpha: 1.0)),
                        borderColor: .clear,
                        borderWidth: 0,
                        cornerRadii: CornerRadii(all: 4)
                    ),
                    textDisabled: Text(
                        color: UIColor(white: 171 / 255, alpha: 1.0),
                        font: .systemFont(ofSize: 14, weight: .bold)
                    ),
                    confirmation: Theme.Text(
                        color: .white,
                        font: .systemFont(ofSize: 14, weight: .regular)
                    )
                ),
                correctOptionContainer: Container(
                    background: .fill(color: UIColor(red: 0, green: 240 / 255, blue: 120 / 255, alpha: 0.2)),
                    borderColor: .clear,
                    borderWidth: 0,
                    cornerRadii: CornerRadii(all: 6)
                ),
                incorrectOptionContainer: Container(
                    background: .fill(color: UIColor(red: 240 / 255, green: 0, blue: 60 / 255, alpha: 0.2)),
                    borderColor: .clear,
                    borderWidth: 0,
                    cornerRadii: CornerRadii(all: 6)
                ),
                correctOptionText: Text(
                    color: UIColor(red: 0, green: 240 / 255, blue: 120 / 255, alpha: 1.0),
                    font: inputFieldFont
                ),
                incorrectOptionText: Text(
                    color: UIColor(red: 240 / 255, green: 0, blue: 60 / 255, alpha: 1.0),
                    font: inputFieldFont
                )
            )
        }

        public var main: Container

        public var header: Container
        public var title: Text

        public var body: Container
        public var footer: Container

        public var optionContainer: Container
        public var optionText: Text
        public var optionInputFieldContainerEnabled: Container
        public var optionInputFieldContainerDisabled: Container
        public var optionInputFieldTextEnabled: Text
        public var optionInputFieldTextDisabled: Text
        public var optionInputFieldPlaceholder: Text

        public var submitButton: SubmitButton

        public var optionInputFieldCorrectContainer: Container
        public var optionInputFieldIncorrectContainer: Container
        public var optionInputFieldCorrectText: Text
        public var optionInputFieldIncorrectText: Text
    }
}
