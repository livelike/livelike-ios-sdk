//
//  ChatViewHandlerConfig.swift
//  EngagementSDK
//
//  Created by Heinrich Dahms on 2019-04-08.
//

import Foundation
import UIKit
import LiveLikeSwift

/**
 A `ChatViewHandlerConfig` instance represents a chat cell at the given index.
 */
struct ChatViewHandlerConfig {
    /// An object giving more information related to the chat message.
    var messageViewModel: MessageViewModel

    /// An index path locating a row in tableView.
    var indexPath: IndexPath

    /// An instance of `Theme`.
    var theme: Theme

    var timestampFormatter: TimestampFormatter?

    var shouldDisplayDebugVideoTime: Bool = false

    var shouldDisplayAvatar: Bool = false

    var enableChatMessageURLs: Bool = true

    var chatMessageUrlPatterns: String?

    init(
        messageViewModel: MessageViewModel,
        indexPath: IndexPath,
        theme: Theme,
        timestampFormatter: TimestampFormatter?,
        shouldDisplayDebugVideoTime: Bool,
        shouldDisplayAvatar: Bool,
        enableChatMessageURLs: Bool,
        chatMessageUrlPatterns: String?
    ) {

        self.messageViewModel = messageViewModel
        self.indexPath = indexPath
        self.theme = theme
        self.shouldDisplayDebugVideoTime = shouldDisplayDebugVideoTime
        self.shouldDisplayAvatar = shouldDisplayAvatar
        self.enableChatMessageURLs = enableChatMessageURLs
        self.chatMessageUrlPatterns = chatMessageUrlPatterns

        if shouldDisplayDebugVideoTime {
            self.timestampFormatter = { date in
                let dateFormatter = DateFormatter()
                dateFormatter.amSymbol = "am"
                dateFormatter.pmSymbol = "pm"
                dateFormatter.setLocalizedDateFormatFromTemplate("MMM d hh:mm:ss:SSSS")
                return dateFormatter.string(from: date)
            }
        } else {
            self.timestampFormatter = timestampFormatter
        }
    }
}
