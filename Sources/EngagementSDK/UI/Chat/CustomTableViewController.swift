//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//

import UIKit

class CustomTableViewController: UIViewController {

    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()

    lazy var tableViewLeading = tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
    lazy var tableViewTrailing = tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor)

    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(tableView)

        let constraints = [
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableViewLeading,
            tableViewTrailing
        ]

        NSLayoutConstraint.activate(constraints)

        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none

    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
            tableView.scrollToRow(at: lastVisibleIndexPath, at: .bottom, animated: false)
        }
    }
}
