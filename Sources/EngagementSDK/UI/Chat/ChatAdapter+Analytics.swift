//
//  ChatAdapter+Analytics.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 9/13/19.
//

import Foundation
import LiveLikeCore
import LiveLikeSwift

extension ChatAdapter {
    func senderIDAttributeValue(for message: MessageViewModel) -> String {
        return message.chatMessage.senderID
    }

    func recordChatFlagButtonPressed(for messageViewModel: MessageViewModel) {
        CoreAnalytics.shared.record(
            name: UIAnalyticsConsts.Name.chatFlagButtonPressed,
            properties: [
                UIAnalyticsConsts.Property.targetChatMessageID: messageViewModel.id.asString,
                UIAnalyticsConsts.Property.targetUserProfileID: senderIDAttributeValue(for: messageViewModel)
            ]
        )
    }

    func recordChatDeleteButtonPressed(for messageViewModel: MessageViewModel) {
        CoreAnalytics.shared.record(
            name: UIAnalyticsConsts.Name.chatDeleteActionSelected,
            properties: [
                UIAnalyticsConsts.Property.targetChatMessageID: messageViewModel.id.asString,
                UIAnalyticsConsts.Property.targetUserProfileID: senderIDAttributeValue(for: messageViewModel)
            ]
        )
    }

    func recordChatFlagActionSelected(for messageViewModel: MessageViewModel, result: ChatActionResult) {
        defer {
            CoreAnalytics.shared.record(
                name: UIAnalyticsConsts.Name.chatFlagActionSelected,
                properties: [
                    UIAnalyticsConsts.Property.targetChatMessageID: messageViewModel.id.asString,
                    UIAnalyticsConsts.Property.targetUserProfileID: senderIDAttributeValue(for: messageViewModel),
                    UIAnalyticsConsts.Property.selectedAction: result.analyticsValue
                ]
            )
        }

        switch result {
        case let .deleted(message: messageViewModel):
            self.chatSession.deleteMessage(message: messageViewModel.chatMessage) { result in
                switch result {
                case .success:
                    log.info("Successfult deleted Message with id: \(messageViewModel.id.asString)")
                case .failure(let error):
                    log.info("Failed to delete Message with id: \(messageViewModel.id.asString) due to error: \(error)")
                }
            }
        case let .blocked(userID: userID, dueTo: messageViewModel):
            self.chatSession.userClient.blockProfile(profileID: userID) { result in
                switch result {
                case .success:
                    log.info("Successfully blocked user with id: \(userID)")

                case .failure(let error):
                    log.error("Failed to block user with id: \(userID) due to error \(error)")
                }
            }
            fallthrough // The blocking action implies reporting as well per IOSSDK-408 definition

        case let .reported(message: messageViewModel):
            self.chatSession.reportMessage(
                messageID: messageViewModel.id.asString
            ) { result in
                switch result {
                case .success:
                    log.info("Message Reported for message with id: \(messageViewModel.id.asString)")
                case .failure(let error):
                    log.error("Failed to report message with id: \(messageViewModel.id.asString) due to error \(error)")
                }
            }
        case .cancelled:
            break
        }
    }

    func recordChatReactionPanelOpened(for messageViewModel: MessageViewModel) {
        CoreAnalytics.shared.record(
            name: UIAnalyticsConsts.Name.chatReactionPanelOpened,
            properties: [
                UIAnalyticsConsts.Property.chatMessageID: messageViewModel.id.asString
            ]
        )
    }

    func recordChatMessageURLOpened(for message: MessageViewModel, chatMessageURL: String) {
        CoreAnalytics.shared.record(
            name: UIAnalyticsConsts.Name.chatMessageURLClicked,
            properties: [
                UIAnalyticsConsts.Property.chatMessageID: message.id.asString,
                UIAnalyticsConsts.Property.chatRoomID: message.chatRoomId,
                UIAnalyticsConsts.Property.chatMessageURL: chatMessageURL
            ]
        )
    }
}

private extension ChatActionResult {
    var analyticsValue: String {
        switch self {
        case .cancelled:
            return "Cancel"
        case .blocked:
            return "Block"
        case .reported:
            return "Report"
        case .deleted:
            return "Delete"
        }
    }
}
