//
//  QuoteMessageView.swift
//  EngagementSDK
//
//  Created by Keval Shah on 15/02/22.
//

import Foundation
import UIKit

protocol QuoteMessageViewDelegate: AnyObject {
    func quoteMessageViewClosed()
}

class QuoteMessageView: UIView {
    var containerView: UIView = {
        let containerView: UIView = UIView(frame: .zero)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.livelike_cornerRadius = 12.0
        containerView.alpha = 1.0
        return containerView
    }()
    
    var closeViewButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "chat_close", in: EngagementResources.resourceBundle, compatibleWith: nil), for: .normal)
        return button
    }()
    
    var messageContainerView: UIView = {
        let messageContainerView: UIView = UIView(frame: .zero)
        messageContainerView.translatesAutoresizingMaskIntoConstraints = false
        messageContainerView.livelike_cornerRadius = 12.0
        messageContainerView.alpha = 1.0
        return messageContainerView
    }()
    
    var profileImageView: GIFImageView = {
        let profileImageView: GIFImageView = GIFImageView(frame: .zero)
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.contentMode = .scaleAspectFit
        profileImageView.backgroundColor = .clear
        profileImageView.layer.masksToBounds = true
        return profileImageView
    }()
    
    var usernameLabel: UILabel = {
        let usernameLabel = UILabel(frame: .zero)
        usernameLabel.translatesAutoresizingMaskIntoConstraints = false
        usernameLabel.font = .systemFont(ofSize: 12)
        return usernameLabel
    }()
    
    var messageLabel: AnimatedLabel = {
        let animatedLabel = AnimatedLabel(frame: .zero)
        animatedLabel.translatesAutoresizingMaskIntoConstraints = false
        animatedLabel.font = .systemFont(ofSize: 12)
        animatedLabel.numberOfLines = 0
        return animatedLabel
    }()
    
    var quoteMessage: MessageViewModel?
    var shouldDisplayAvatar: Bool = false
    var theme: Theme = Theme()
    
    weak var delegate: QuoteMessageViewDelegate?
    
    init() {
        super.init(frame: .zero)
        self.setupViews()
    }
    
    func configure(quoteMessage: MessageViewModel?, shouldDisplayAvatar: Bool, theme: Theme) {
        self.quoteMessage = quoteMessage
        self.shouldDisplayAvatar = shouldDisplayAvatar
        self.theme = theme
        self.messageLabel.message = quoteMessage
        self.usernameLabel.text = quoteMessage?.username
        self.messageLabel.attributedText = quoteMessage?.message
        if self.shouldDisplayAvatar {
            profileImageView.isHidden = false
            if let imageUrl = quoteMessage?.profileImageUrl {
                if theme.chatImageWidth > 0 {
                    NSLayoutConstraint.activate([
                        profileImageView.widthAnchor.constraint(equalToConstant: theme.chatImageWidth),
                        profileImageView.heightAnchor.constraint(equalToConstant: theme.chatImageHeight)
                    ])
                    profileImageView.livelike_cornerRadius = theme.chatImageCornerRadius
                }
                profileImageView.setImage(url: imageUrl)
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        addSubview(containerView)
        containerView.addSubview(messageContainerView)
        messageContainerView.addSubview(profileImageView)
        messageContainerView.addSubview(usernameLabel)
        messageContainerView.addSubview(messageLabel)
        containerView.addSubview(closeViewButton)
        containerView.backgroundColor = .red
        messageContainerView.backgroundColor = .blue
        closeViewButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
        profileImageView.isHidden = !shouldDisplayAvatar

        NSLayoutConstraint.activate([
            containerView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            containerView.topAnchor.constraint(equalTo: self.topAnchor),
            containerView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            messageContainerView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8),
            messageContainerView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 8),
            messageContainerView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -8),
            messageContainerView.trailingAnchor.constraint(equalTo: closeViewButton.leadingAnchor, constant: -10),
            
            closeViewButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8),
            closeViewButton.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            closeViewButton.widthAnchor.constraint(equalToConstant: 15.0),
            closeViewButton.heightAnchor.constraint(equalToConstant: 15.0),
            
            profileImageView.widthAnchor.constraint(equalToConstant: 50.0),
            profileImageView.heightAnchor.constraint(equalToConstant: 50.0),
            profileImageView.leadingAnchor.constraint(equalTo: messageContainerView.leadingAnchor, constant: 10),
            profileImageView.topAnchor.constraint(equalTo: messageContainerView.topAnchor, constant: 10),
            
            usernameLabel.topAnchor.constraint(equalTo: messageContainerView.topAnchor, constant: 10),
            usernameLabel.leadingAnchor.constraint(equalTo: (self.shouldDisplayAvatar ? profileImageView.trailingAnchor : messageContainerView.leadingAnchor), constant: 8),
            usernameLabel.bottomAnchor.constraint(equalTo: messageLabel.topAnchor, constant: -5),
            usernameLabel.trailingAnchor.constraint(equalTo: messageContainerView.trailingAnchor, constant: -10),
            
            messageLabel.leadingAnchor.constraint(equalTo: usernameLabel.leadingAnchor),
            messageLabel.bottomAnchor.constraint(equalTo: messageContainerView.bottomAnchor, constant: -10),
            messageLabel.trailingAnchor.constraint(equalTo: messageContainerView.trailingAnchor, constant: -10)

        ])
        applyTheme(theme: self.theme)
    }
    
    func applyTheme(theme: Theme) {
        messageContainerView.layer.cornerRadius = theme.messageCornerRadius
        messageContainerView.backgroundColor = theme.quoteMessageBackgroundColor
        containerView.backgroundColor = theme.messageBackgroundColor
        containerView.layer.cornerRadius = theme.messageCornerRadius
        messageLabel.textColor = theme.messageTextColor
        usernameLabel.textColor = theme.quoteUsernameTextColor
        usernameLabel.font = theme.quoteUsernameTextFont
        usernameLabel.text = theme.usernameTextUppercased ? usernameLabel.text?.uppercased() : usernameLabel.text
    }
    
    @objc func closeButtonPressed() {
        delegate?.quoteMessageViewClosed()
    }
    
    func reset() {
        quoteMessage = nil
        messageLabel.prepareForReuse()
    }
}
