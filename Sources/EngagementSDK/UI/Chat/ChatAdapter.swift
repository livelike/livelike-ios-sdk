//
//  ChatAdapter.swift
//  LiveLikeSDK
//
//  Created by Heinrich Dahms on 2019-01-19.
//

import UIKit
import LiveLikeCore
import LiveLikeSwift

final class ChatAdapter: NSObject {
    let chatCellIdentifier = "chatMessageCellID"

    weak var chatMessageViewDelegate: ChatMessageViewDelegate?

    weak var tableView: UITableView? {
        didSet {
            guard let tableView = tableView else { return }

            tableView.delegate = self
            let cellNib = UINib(nibName: "ChatMessageTableViewCell", bundle: EngagementResources.resourceBundle)
            tableView.register(cellNib, forCellReuseIdentifier: chatCellIdentifier)
            tableView.rowHeight = UITableView.automaticDimension
            tableView.estimatedRowHeight = UITableView.automaticDimension
            tableView.reloadData()
        }
    }

    var hideSnapToLive: ((Bool) -> Void)?
    var didScrollToTop: (() -> Void)?
    var timestampFormatter: TimestampFormatter?
    var shouldDisplayDebugVideoTime: Bool = false
    var shouldShowIncomingMessages: Bool
    var enableQuoteMessage: Bool

    // Private
    internal var shouldHideSnapToLive = true {
        didSet {
            if shouldHideSnapToLive != oldValue {
                hideSnapToLive?(shouldHideSnapToLive)
            }
        }
    }

    internal var theme: Theme = Theme()
    internal var messagesDisplayed = [MessageViewModel]() {
        didSet {
            self.chatMessageViewDelegate?.chatAdapter(
                self,
                messageCountDidChange: messagesDisplayed.count
            )
        }
    }

    /// The messages waiting to be appended to the bottom of the table (new messages)
    internal var messagesToAppend = [ChatMessage]()
    /// The messages waiting to be inserted to the top of the table (older messages from history)
    var messagesToInsert = [MessageViewModel]()
    internal var updatingTable = false
    internal let messageViewModelFactory: MessageViewModelFactory
    internal var isDragging = false {
        didSet {
            if isDragging == false, scrollingState == .tracking {
                scrollingState = .inactive
            }
        }
    }

    internal var lastMessageIsVisible: Bool {
        if let lastVisibleRow = tableView?.indexPathsForVisibleRows?.last?.row, lastVisibleRow == messagesDisplayed.count - 1 {
            return true
        }
        return false
    }

    internal var lastRowWasVisible = false
    // Analytics

    internal var oldestMessageIndex: Int = Int.max
    internal var viewedMessages: Int = 0
    internal var scrollingState: ScrollingState = .inactive
    internal var scrollingReturnMethod: ChatScrollingReturnMethod = .scroll
    var hideDeletedMessage: Bool

    // keeps last bottom visible row before orientation change in case we need to scroll to it
    private var lastBottomVisibleRow: Int?
    private var isReactionsPanelOpen: Bool = false

    /// The amount of newest messages a user needs to scroll past
    /// in order for Snap To Live button to appear
    internal let snapToLiveAfterMessageAmount: Int = 3

    var scrollToMostRecentCompletion: (() -> Void)?

    /// A flag that determines whether we should scroll to display the newest message when it is received
    var shouldScrollToNewestMessageOnArrival: Bool = true

    var updateTimer: DispatchSourceTimer?

    var chatSession: ChatSession

    init(
        messageViewModelFactory: MessageViewModelFactory,
        chatSession: ChatSession,
        shouldShowIncomingMessages: Bool,
        enableQuoteMessage: Bool,
        shouldHideDeletedMessage: Bool
    ) {
        self.messageViewModelFactory = messageViewModelFactory
        self.chatSession = chatSession

        self.shouldShowIncomingMessages = shouldShowIncomingMessages
        self.enableQuoteMessage = enableQuoteMessage
        self.hideDeletedMessage = shouldHideDeletedMessage
        super.init()

        updateTimer = DispatchSource.makeTimerSource(flags: [], queue: .main)
        updateTimer?.schedule(deadline: .now(), repeating: .milliseconds(200))
        updateTimer?.setEventHandler { [weak self] in
            guard let self = self else { return }
            if self.shouldShowIncomingMessages == true {
                self.updateTable()
            }
        }
        updateTimer?.resume()
        chatSession.chatClient.delegate = self
        chatSession.userClient.delegate = self
    }

    deinit {
        updateTimer?.cancel()
        updateTimer = nil
    }
}

extension ChatAdapter {
    func setTheme(_ theme: Theme) {
        self.theme = theme
    }

    /// Attempts to scroll to the most recent message
    ///
    /// - Parameters:
    ///   - force: When set to `true`, will always scroll to last item including during a dragging session
    ///   - returnMethod: Describes the method that initiated the call (scroll, snapToLive or keyboard)
    func scrollToMostRecent(force: Bool = false, returnMethod: ChatScrollingReturnMethod) {
        scrollingReturnMethod = returnMethod
        if isDragging, !force { return }
        guard let tableView = tableView else { return }
        guard tableView.contentSize.height > tableView.bounds.height else {
            self.updatingTable = false
            return
        }
        if messagesDisplayed.count > 1 {
            tableView.scrollToRow(at: IndexPath(row: messagesDisplayed.count - 1, section: 0), at: .bottom, animated: false)
            shouldHideSnapToLive = true
            self.scrollToMostRecentCompletion?()
        }

        deselectSelectedMessage()
    }

    func orientationWillChange() {
        // Check if the last row is displayed prior to rotation
        lastRowWasVisible = lastMessageIsVisible
        lastBottomVisibleRow = tableView?.indexPathsForVisibleRows?.last?.row
        deselectSelectedMessage()
    }

    func orientationDidChange() {
        // If the last row was visible as the orientation change started, scroll to the said last
        // row when the orientation change has completed
        if lastRowWasVisible {
            scrollToMostRecent(force: true, returnMethod: .scroll)
        } else {
            if let lastBottomVisibleRow = lastBottomVisibleRow {
                tableView?.scrollToRow(
                    at: IndexPath(row: lastBottomVisibleRow, section: 0),
                    at: .bottom,
                    animated: false
                )
            }
        }
        lastRowWasVisible = false
    }

    func getMessage(withID id: ChatMessageID) -> MessageViewModel? {
        return messagesDisplayed.first(where: { $0.id == id })
    }
}

// MARK: - ChatProxyInput

extension ChatAdapter {
    func publish(
        messagesFromHistory messages: [ChatMessage]
    ) {
        firstly {
            Promises.zip(
                Promises.all(messages.map({ messageViewModelFactory.create(from: $0)})),
                chatSession.userClient.getBlockedProfileIDListComplete()
            )
        }.then { messageViewModels, blockList in
            let newMessages = messageViewModels.filter({ messageViewModel -> Bool in
                self.messagesDisplayed.contains(messageViewModel) == false &&
                    self.messagesToInsert.contains(messageViewModel) == false &&
                    self.blockListContains(userID: messageViewModel.chatMessage.senderID, blockList: blockList) == false
            })

            self.messagesToInsert.insert(contentsOf: newMessages, at: 0)
            if self.messagesToInsert.isEmpty, self.messagesDisplayed.isEmpty {
                self.chatMessageViewDelegate?.chatAdapter(self, messageCountDidChange: 0)
            }
        }.catch {
            log.error($0.localizedDescription)
        }
    }

    func publish(
        newestMessages messages: [ChatMessage]
    ) {
        firstly {
            self.chatSession.userClient.getBlockedProfileIDListComplete()
        }.then { blockList -> Promise<[MessageViewModel]> in
            let newMessages = messages.filter({ messageViewModel -> Bool in
                self.messagesDisplayed.contains(where: { $0.id == messageViewModel.id }) == false &&
                    self.messagesToAppend.contains(messageViewModel) == false &&
                    self.blockListContains(userID: messageViewModel.senderID, blockList: blockList) == false
            })
            self.messagesToAppend.append(contentsOf: newMessages)
            return Promises.all(messages.map({ self.messageViewModelFactory.create(from: $0)}))
        }.catch {
            log.error($0.localizedDescription)
        }
    }

    func publish(newMessage message: ChatMessage) {
        firstly {
            self.chatSession.userClient.getBlockedProfileIDListComplete()
        }.then { blockList in
            guard
                self.messagesListContains(message: message) == false,
                self.blockListContains(userID: message.senderID, blockList: blockList) == false
            else {
                return
            }
            self.messagesToAppend.append(message)
        }
    }

    private func blockListContains(userID: String, blockList: [String]) -> Bool {
        return blockList.contains(userID)
    }

    private func messagesListContains(message: ChatMessage) -> Bool {
        let messagesDisplayedContains = self.messagesDisplayed.contains(
            where: { $0.id == message.id }
        )
        let messagesToAppendContains = self.messagesToAppend.contains(
            where: { $0.id == message.id }
        )
        return messagesDisplayedContains != messagesToAppendContains
    }

    func deleteMessage(messageId: ChatMessageID) {
        if let indexOfDeletedMessage = self.messagesDisplayed.firstIndex(where: { $0.id == messageId }) {
            // Dismiss reactions panel if it was open
            if let messageViewDelegate = self.chatMessageViewDelegate {
                messageViewDelegate.dismissChatMessageActionPanel()
            }

            // mark messageViewModel as deleted
            self.messagesDisplayed[indexOfDeletedMessage].redact(theme: self.theme, quoteMessage: false)

            // only reload the tableView if the deleted cell is currently visbile in the tableView
            updateTableViewIfCellVisible(indexOfCell: indexOfDeletedMessage)
        } else {
            log.error("Chat Message Id: \(messageId) cannot be deleted")
        }

        var indicesOfQuoteMessage: [Int] = []
        for (indice, message) in self.messagesDisplayed.enumerated() {
            if let quoteMessage = message.quoteMessage {
                if quoteMessage.id == messageId.asString {
                    indicesOfQuoteMessage.append(indice)
                }
            }
        }

        if let messageViewDelegate = self.chatMessageViewDelegate {
            messageViewDelegate.dismissChatMessageActionPanel()
        }

        // mark messageViewModel as deleted
        for i in indicesOfQuoteMessage {
            self.messagesDisplayed[i].redact(theme: self.theme, quoteMessage: true)

            // only reload the tableView if the deleted cell is currently visbile in the tableView
            updateTableViewIfCellVisible(indexOfCell: i)
        }
    }

    private func updateTableViewIfCellVisible(indexOfCell index: Int) {
        if (self.tableView?.indexPathsForVisibleRows?.first(where: { $0.row == index })) != nil {
            self.tableView?.beginUpdates()
            self.tableView?.reloadRows(at: [IndexPath(row: index, section: 0)], with: .middle)
            self.tableView?.endUpdates()
        }
    }
}

// MARK: - Private

internal extension ChatAdapter {
    func selectSelectedMessage(cell: ChatMessageTableViewCell) {
        guard let tableView = self.tableView else { return }
        guard let messageViewDelegate = self.chatMessageViewDelegate else { return }

        if let indexPath = tableView.indexPath(for: cell) {
            let rect = tableView.rectForRow(at: indexPath)
            let rectInScreen = tableView.convert(rect, to: tableView.superview)
            cell.selectableView.isSelected = true

            var animationDirection: ChatMessageActionPanelAnimationDirection = .up
            if tableView.bounds.minY > rect.minY - messageViewDelegate.actionPanelHeight {
                animationDirection = .down
            }

            isReactionsPanelOpen = true
            self.shouldScrollToNewestMessageOnArrival = false
            messageViewDelegate.showChatMessageActionPanel(
                for: messagesDisplayed[indexPath.row],
                cellRect: rectInScreen,
                direction: animationDirection
            )
        }
    }

    func deselectSelectedMessage() {
        if
            let tableView = tableView,
            let indexPath = tableView.indexPathForSelectedRow
        {
            tableView.deselectRow(at: indexPath, animated: true)
            guard let cell = tableView.cellForRow(at: indexPath) as? ChatMessageTableViewCell else {
                return
            }

            cellDeselected(cell: cell, tableView: tableView)
        }
    }

    /// Used to toggle UI elements/flags on message deselection which happens from multiple places
    func cellDeselected(cell: ChatMessageTableViewCell, tableView: UITableView) {
        cell.selectableView.isSelected = false
        isReactionsPanelOpen = false

        // Re-enable autoscroll if we are still at bottom of table when closing reaction panel
        if self.lastMessageIsVisible {
            self.shouldScrollToNewestMessageOnArrival = true
        }
        chatMessageViewDelegate?.dismissChatMessageActionPanel()
    }
}

extension ChatAdapter: ChatClientDelegate {
    func chatClient(_ chatClient: LiveLikeSwift.ChatClient, userDidGetBlocked blockInfo: LiveLikeSwift.BlockInfo) {
        var indicesOfQuoteMessage: [Int] = []
        for (indice, message) in self.messagesDisplayed.enumerated() {
            if let quoteMessage = message.quoteMessage {
                if quoteMessage.senderID == blockInfo.blockedProfileID {
                    indicesOfQuoteMessage.append(indice)
                }
            }
        }

        if let messageViewDelegate = self.chatMessageViewDelegate {
            messageViewDelegate.dismissChatMessageActionPanel()
        }

        // mark messageViewModel as deleted
        for i in indicesOfQuoteMessage {
            self.messagesDisplayed[i].redactForBlockProfile(theme: self.theme, quoteMessage: true)

            // only reload the tableView if the deleted cell is currently visbile in the tableView
            updateTableViewIfCellVisible(indexOfCell: i)
        }
    }

    func chatClient(_ chatClient: LiveLikeSwift.ChatClient, userDidGetUnblocked unblockInfo: LiveLikeSwift.UnblockInfo) { }

    func chatClient(_ chatClient: ChatClient, userDidBecomeMemberOfChatRoom newChatMembershipInfo: NewChatMembershipInfo) { }

    func chatClient(_ chatClient: ChatClient, userDidReceiveInvitation chatRoomInvitation: ChatRoomInvitation) { }
}

extension ChatAdapter: UserClientDelegate {
    func userClient(_ userClient: UserClient, userDidGetBlocked blockInfo: BlockInfo) {

        var indicesOfQuoteMessage: [Int] = []
        for (indice, message) in self.messagesDisplayed.enumerated() {
            if let quoteMessage = message.quoteMessage {
                if quoteMessage.senderID == blockInfo.blockedProfileID {
                    indicesOfQuoteMessage.append(indice)
                }
            }
        }

        if let messageViewDelegate = self.chatMessageViewDelegate {
            messageViewDelegate.dismissChatMessageActionPanel()
        }

        // mark messageViewModel as deleted
        for i in indicesOfQuoteMessage {
            self.messagesDisplayed[i].redactForBlockProfile(theme: self.theme, quoteMessage: true)

            // only reload the tableView if the deleted cell is currently visbile in the tableView
            updateTableViewIfCellVisible(indexOfCell: i)
        }
    }

    func userClient(_ userClient: UserClient, userDidGetUnblocked unblockInfo: UnblockInfo) { }
}

/// The `ScrollingState` is used to help trigger the proper analytic events while a user scrolls.
///
/// - inactive: Is the default state, the last message is visible and the user is not actively dragging
/// - tracking: Is the state while the user starts dragging, but the last message is still in the view
/// - active: Is when the last message is no longer visible
enum ScrollingState {
    case inactive
    case tracking
    case active
}

typealias SelectableView = UIView & Selectable
protocol Selectable {
    var isSelected: Bool { get set }
}

enum ChatActionResult {
    case cancelled
    case blocked(userID: String, dueTo: MessageViewModel)
    case reported(message: MessageViewModel)
    case deleted(message: MessageViewModel)
}

typealias FlagTapCompletion = (ChatActionResult) -> Void
typealias DeleteTapCompletion = (ChatActionResult) -> Void

protocol ChatMessageViewDelegate: AnyObject {
    func flagTapped(for message: MessageViewModel, completion: FlagTapCompletion?)
    func deleteTapped(for message: MessageViewModel, completion: DeleteTapCompletion?)
    func showChatMessageActionPanel(for messageViewModel: MessageViewModel, cellRect: CGRect, direction: ChatMessageActionPanelAnimationDirection)
    func dismissChatMessageActionPanel()
    func chatAdapter(_ chatAdapter: ChatAdapter, messageCountDidChange count: Int)
    func actionPanelPrepareToBeShown(messageViewModel: MessageViewModel)
    func chatMessageURLTapped(messageViewModel: MessageViewModel, urlValue: String)
    func getCustomViewForCustomMessage(_ customMessage: String) -> UIView?
    func quoteMessageSelected(quoteMessage: MessageViewModel)
    func getCustomViewBetweenMessages(beforeMessageID: String?, afterMessageID: String?) -> UIView?
    var actionPanelHeight: CGFloat { get }
}

extension ChatMessageViewDelegate {
    func flagTapped(for message: MessageViewModel) {
        flagTapped(for: message, completion: nil)
    }
}

protocol ChatActionsDelegateContainer {
    var chatMessageViewDelegate: ChatMessageViewDelegate? { get set }
}
