//
//  AnimatedLabel.swift
//  EngagementSDK
//
//  Created by Heinrich Dahms on 2019-06-05.
//

import UIKit

class AnimatedLabel: UILabel {
    
    var message: MessageViewModel?
    var onURLTap: ((String) -> Void)?
    
    override var attributedText: NSAttributedString? {
        didSet {
            guard let attributedText = self.attributedText else {
                return
            }
            
            attributedText.enumerateAttribute(NSAttributedString.Key.attachment, in: NSRange(location: 0, length: attributedText.length), options: .reverse) { [weak self] value, _, _ in
                guard let self = self else { return }
                if let attachment = value as? StickerAttachment {
                    attachment.containerView = self
                }
            }
        }
    }
    
    // Function to detect tap on link and open in browser
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
       
        let returnVal = super.point(inside: point, with: event)
                
        // NSTextContainer Configuration
        let messageContainer = NSTextContainer(size: bounds.size)
        messageContainer.lineFragmentPadding = 0.0
        messageContainer.lineBreakMode = lineBreakMode
        messageContainer.maximumNumberOfLines = numberOfLines
        
        // NSLayoutManager Configuration for messageContainer
        let layoutManager = NSLayoutManager()
        layoutManager.addTextContainer(messageContainer)
        
        guard let attributedText = attributedText else { return false }
        
        let messageStorage = NSTextStorage(attributedString: attributedText)
        messageStorage.addAttribute(NSAttributedString.Key.font, value: font!, range: NSRange(location: 0, length: attributedText.length))
        messageStorage.addLayoutManager(layoutManager)
        
        // Location of tapped part of message
        let locationOfTouchInLabel = point
        
        // Text Alignment and Insets for formatting
        let textBoundingBox = layoutManager.usedRect(for: messageContainer)
        var alignmentOffset: CGFloat!
        switch textAlignment {
        case .left, .natural, .justified:
            alignmentOffset = 0.0
        case .center:
            alignmentOffset = 0.5
        case .right:
            alignmentOffset = 1.0
        @unknown default:
            alignmentOffset = 0.0
        }
        
        let xOffset = ((bounds.size.width - textBoundingBox.size.width) * alignmentOffset) - textBoundingBox.origin.x
        let yOffset = ((bounds.size.height - textBoundingBox.size.height) * alignmentOffset) - textBoundingBox.origin.y
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - xOffset, y: locationOfTouchInLabel.y - yOffset)
        
        // Point out the characted tapped
        let characterIndex = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: messageContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        let lineTapped = Int(ceil(locationOfTouchInLabel.y / font.lineHeight)) - 1
        let rightMostPointInLineTapped = CGPoint(x: bounds.size.width, y: font.lineHeight * CGFloat(lineTapped))
        let charsInLineTapped = layoutManager.characterIndex(for: rightMostPointInLineTapped, in: messageContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        guard characterIndex < charsInLineTapped else { return false }
        
        let attributeName = NSAttributedString.Key.link
        
        let attributeValue = self.attributedText?.attribute(attributeName, at: characterIndex, effectiveRange: nil)
        
        if let value = attributeValue as? String {
            if let urlToOpen = URL(string: ((value).hasPrefix("http") ? value : "http://\(value)")) {
                if UIApplication.shared.canOpenURL(urlToOpen) {
                    self.onURLTap?(value)
                    UIApplication.shared.open(urlToOpen)
                }
            }
        }
        
        return returnVal
    }

    func prepareForReuse() {
        guard let attributedText = self.attributedText else {
            return
        }
        
        onURLTap = nil
        attributedText.enumerateAttribute(NSAttributedString.Key.attachment, in: NSRange(location: 0, length: attributedText.length), options: .reverse) { value, _, _ in
            if let attachment = value as? StickerAttachment {
                attachment.prepareForReuse()
            }
        }
    }
}
