//
//  ChatViewController+Input.swift
//  EngagementSDK
//
//  Created by Heinrich Dahms on 2019-06-27.
//

import UIKit
import MobileCoreServices
import UniformTypeIdentifiers
import LiveLikeSwift

extension ChatViewController: ChatInputViewDelegate {
    func chatInputError(title: String, message: String) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
            self.present(alert, animated: true, completion: nil)
        }
    }

    func chatInputBeginEditing(with textField: UITextField) { }
    func chatInputEndEditing(with textField: UITextField) {}
    func chatInputSendPressed(message: NewChatMessage) {
        if !self.quoteMessageContainerView.isHidden {
            self.closeQuoteMessageContainerView()
        }
    }
    func chatInputKeyboardToggled() { }

    func chatSuccessfulSend(message: ChatMessage) {
        self.didSendMessage(message)
        if !self.quoteMessageContainerView.isHidden {
            self.closeQuoteMessageContainerView()
        }
    }

    func imagePickerButtonPressed() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if #available(iOS 14.0, *) {
            actionSheet.addAction(UIAlertAction(title: "Documents", style: .default, handler: { [weak self] _ in
                let documentPicker = UIDocumentPickerViewController(forOpeningContentTypes: [UTType.image])
                documentPicker.delegate = self
                self?.present(documentPicker, animated: true)
            }))
        }
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { [weak self] _ in
            let picker = UIImagePickerController()
            picker.allowsEditing = true
            picker.sourceType = .photoLibrary
            picker.delegate = self
            self?.present(picker, animated: true)
        }))
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { [weak self] _ in
            let picker = UIImagePickerController()
            picker.allowsEditing = true
            picker.sourceType = .camera
            picker.delegate = self
            self?.present(picker, animated: true)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        self.present(actionSheet, animated: true, completion: nil)
    }
}

extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIDocumentPickerDelegate {
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        for image in info {
            if image.key == UIImagePickerController.InfoKey.originalImage {
                if let selectedImage: UIImage = image.value as? UIImage {
                    self.chatInputView.textField.imageAttachmentData = selectedImage.jpegData(compressionQuality: 1.0)
                    self.chatInputView.updateInputViewButtonVisibility()
                    picker.dismiss(animated: true)
                }
            }
        }
    }

    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        if let imageURL = urls.first {
            let imageData = try? Data(contentsOf: imageURL)
            if let selectedImageData = imageData {
                let selectedImage = UIImage(data: selectedImageData)
                self.chatInputView.textField.imageAttachmentData = selectedImage?.jpegData(compressionQuality: 1.0)
                self.chatInputView.updateInputViewButtonVisibility()
                controller.dismiss(animated: true)
            }
        }
    }
}
