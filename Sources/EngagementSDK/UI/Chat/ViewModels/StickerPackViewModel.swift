//
//  StickerPackViewModel.swift
//  
//
//  Created by Jelzon Monzon on 11/10/22.
//

import UIKit
import LiveLikeCore
import LiveLikeSwift

struct StickerPackViewModel {

    init(stickerPack: StickerPack) {
        self.id = stickerPack.id
        self.name = stickerPack.name
        self.file = stickerPack.file
        self.stickers = stickerPack.stickers
    }

    init(id: String, name: String, file: URL, stickers: [Sticker]) {
        self.id = id
        self.name = name
        self.file = file
        self.stickers = stickers
    }

    let id: String
    let name: String
    let file: URL
    let stickers: [Sticker]
}

extension StickerPackViewModel {
    static var identifier: String {
        return "chat_recent_stickers_tab"
    }

    static func recentStickerPacks(from stickers: [Sticker]) -> [StickerPackViewModel] {
        var stickerPack = [StickerPackViewModel]()

        let fileURL = "file://chat_recent_stickers_tab.local"

        if
            let image = UIImage(
                named: identifier,
                in: EngagementResources.resourceBundle,
                compatibleWith: nil
            ),
            let imageData = image.pngData(),
            let url = URL(string: fileURL)
        {
            Cache.shared.set(object: imageData, key: fileURL, completion: nil)
            stickerPack.append(
                StickerPackViewModel(
                    id: identifier,
                    name: identifier,
                    file: url,
                    stickers: stickers
                )
            )

        }
        return stickerPack
    }
}
