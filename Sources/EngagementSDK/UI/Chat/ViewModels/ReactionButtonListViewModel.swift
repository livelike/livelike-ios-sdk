//
//  ReactionButtonListViewModel.swift
//  
//
//  Created by Jelzon Monzon on 11/10/22.
//

import LiveLikeSwift
import LiveLikeCore
import UIKit

/// Events for ReactionButtonListViewModel
protocol ReactionButtonListViewModelListener {
    func didReceiveAddMessageReaction(reactionButtonViewModel: ReactionButtonViewModel)

    func didReceiveRemoveMessageReaction(reactionButtonViewModel: ReactionButtonViewModel)
}

/// Represents all of a chat message's reaction buttons
class ReactionButtonListViewModel {

    let reactionAssets: [Reaction]

    private let profileID: String
    private let chatSession: ChatSession
    private let chatMessageID: ChatMessageID
    private let chatRoomID: String
    private var listeners = Listener<ReactionButtonListViewModelListener>()

    // Confirmed reaction votes
    @Atomic private var reactionVotes: [ReactionVote]
    // Locally managed list of votes. Some votes may not have been confirmed yet.
    @Atomic private var reactionButtonViewModels: [ReactionButtonViewModel]

    /// Used to prevent the user from spamming reactions before receiving and update from the server
    private var canReact: Bool = true

    internal init(
        reactionAssets: [Reaction],
        reactionVotes: [ReactionVote],
        profileID: String,
        chatSession: ChatSession,
        chatMessageID: ChatMessageID,
        chatRoomID: String,
        canReact: Bool = true
    ) {
        self.reactionAssets = reactionAssets
        self.reactionVotes = reactionVotes
        self.reactionButtonViewModels = reactionVotes.map {
            ReactionButtonViewModel(
                isMine: $0.senderID == profileID,
                reactionID: $0.reactionID.asString
            )
        }
        self.profileID = profileID
        self.chatSession = chatSession
        self.chatMessageID = chatMessageID
        self.chatRoomID = chatRoomID
        self.canReact = canReact
    }

    func addListener(_ listener: ReactionButtonListViewModelListener) {
        if listeners.isEmpty() {
            self.chatSession.addDelegate(self)
        }
        self.listeners.addListener(listener)
    }

    func removeListener(_ listener: ReactionButtonListViewModelListener) {
        self.listeners.removeListener(listener)
        if listeners.isEmpty() {
            self.chatSession.removeDelegate(self)
        }
    }

    func isReactionsAvailable() -> Bool {
        return reactionAssets.count > 0
    }

    func image(forID id: String) -> URL? {
        return reactionAssets.first(where: { $0.id == id })?.file
    }

    func isMine(forReactionID id: String) -> Bool {
        return reactionButtonViewModels
            .filter { $0.reactionID == id }
            .contains(where: { $0.isMine })
    }

    func voteCount(forID id: String) -> Int {
        return reactionButtonViewModels.filter { $0.reactionID == id }.count
    }

    private func myVote(forReactionID reactionID: String) -> ReactionVote? {
        return reactionVotes
            .filter { $0.reactionID.asString == reactionID }
            .filter { $0.senderID == self.profileID }
            .first
    }

    func hasVotes(forReactionID reactionID: String) -> Bool {
        return voteCount(forID: reactionID) > 0
    }

    var hasVotes: Bool {
        return reactionButtonViewModels.count > 0
    }

    var totalReactionsCount: Int {
        return reactionButtonViewModels.count
    }

    private func remove(where whereBlock: (ReactionButtonViewModel) -> Bool) {
        if let removeIndex = self.reactionButtonViewModels.firstIndex(where: whereBlock) {
            self.reactionButtonViewModels.remove(at: removeIndex)
        }
    }

    /// User may have one vote on each reaction
    func reactionSelected(reactionID: String) {
        guard canReact else { return }

        if isMine(forReactionID: reactionID) {
            self.removeReaction(reactionID: reactionID)
        } else {
            self.addReaction(reactionID: reactionID)
        }
    }

    private func addReaction(reactionID: String) {
        guard let messageID = self.chatMessageID.id else { return }
        recordChatReactionAdded(reactionId: reactionID)

        chatSession.addMessageReaction(
            messageID: messageID,
            reactionID: reactionID
        ) { [weak self] _ in
            guard let self = self else { return }
            self.canReact = true
        }
    }

    private func removeReaction(reactionID: String) {
        guard let messageID = self.chatMessageID.id else { return }
        guard let reactionVoteID = myVote(forReactionID: reactionID)?.voteID else {
            return
        }

        chatSession.removeMessageReaction(
            reactionVoteID: reactionVoteID,
            messageID: messageID
        ) { [weak self] _ in
            guard let self = self else { return }
            self.canReact = true
        }
    }
}

extension ReactionButtonListViewModel: ChatSessionDelegate {

    func chatSession(
        _ chatSession: ChatSession,
        didRecieveAddMessageReaction reactionVote: ReactionVote
    ) {
        guard reactionVote.chatMessageID == self.chatMessageID.id else { return }
        self.reactionVotes.append(reactionVote)
        let reactionButtonViewModel = ReactionButtonViewModel(
            isMine: reactionVote.senderID == self.profileID,
            reactionID: reactionVote.reactionID.asString
        )
        self.reactionButtonViewModels.append(reactionButtonViewModel)
        self.listeners.publish {
            $0.didReceiveAddMessageReaction(
                reactionButtonViewModel: reactionButtonViewModel
            )
        }
    }

    func chatSession(
        _ chatSession: ChatSession,
        diRecieveRemoveMessageReaction reactionVote: ReactionVote
    ) {
        let isMine = reactionVote.senderID == self.profileID
        guard reactionVote.chatMessageID == self.chatMessageID.id else { return }

        self.remove(where: { reactionButtonViewModel in
            reactionButtonViewModel.reactionID == reactionVote.reactionID.asString &&
                reactionButtonViewModel.isMine == isMine
        })

        // Update reactionVotes
        self.reactionVotes.removeAll(where: { $0.voteID == reactionVote.voteID })

        self.listeners.publish {
            $0.didReceiveRemoveMessageReaction(
                reactionButtonViewModel: ReactionButtonViewModel(
                    isMine: reactionVote.senderID == self.profileID,
                    reactionID: reactionVote.reactionID.asString
                )
            )
        }

    }
}

// Analytics
extension ReactionButtonListViewModel {
    private func recordChatReactionAdded(reactionId: String) {
        CoreAnalytics.shared.record(
            name: UIAnalyticsConsts.Name.chatReactionAdded,
            properties: [
                UIAnalyticsConsts.Property.chatMessageID: chatMessageID,
                UIAnalyticsConsts.Property.chatReactionID: reactionId,
                UIAnalyticsConsts.Property.chatRoomID: chatRoomID
            ]
        )
    }

    private func recordChatReactionRemoved(reactionId: String) {
        CoreAnalytics.shared.record(
            name: UIAnalyticsConsts.Name.chatReactionRemoved,
            properties: [
                UIAnalyticsConsts.Property.chatMessageID: chatMessageID,
                UIAnalyticsConsts.Property.chatReactionID: reactionId,
                UIAnalyticsConsts.Property.chatRoomID: chatRoomID
            ]
        )
    }
}
