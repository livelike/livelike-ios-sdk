//
//  MessageViewModel.swift
//  EngagementSDK
//
//  Created by Heinrich Dahms on 2019-05-22.
//

import UIKit
import LiveLikeSwift
import LiveLikeCore

class MessageViewModel: Equatable {
    var chatMessage: ChatMessage
    
    var id: ChatMessageID
    var message: NSAttributedString
    let username: String
    let localProfileID: String
    
    var isLocalClient: Bool {
        return localProfileID == chatMessage.senderID
    }
    
    let syncPublishTimecode: String?
    let chatRoomId: String
    private(set) var isDeleted: Bool = false
    let createdAt: Date
    
    var chatReactions: ReactionButtonListViewModel
    var profileImageUrl: URL?
    var bodyImageUrl: URL?
    
    var bodyImageSize: CGSize?
    
    /// Used for debuging video player time
    var videoPlayerDebugTime: Date?
    
    var accessibilityLabel: String?
    
    let stickerShortcodesInMessage: [String]
    
    /// Used for custom message data that can be in JSON Format
    var customData: String?
    
    var quoteMessage: ChatMessagePayload?
    
    var quoteMessageText: NSAttributedString
    var quoteMessageImageUrl: URL?
    var quoteMessageImageSize: CGSize?
    var quoteMessageCustomData: String?
    var quoteMessageAccessibilityLabel: String
    var stickerShortcodesInQuoteMessage: [String]
    
    private let chatSession: ChatSession
    
    init(
        chatMessage: ChatMessage,
        id: ChatMessageID,
        message: NSAttributedString,
        username: String,
        localProfileID: String,
        syncPublishTimecode: String?,
        chatRoomId: String,
        chatReactions: ReactionButtonListViewModel,
        profileImageUrl: URL?,
        createdAt: Date,
        bodyImageUrl: URL?,
        bodyImageSize: CGSize?,
        accessibilityLabel: String,
        stickerShortcodesInMessage: [String],
        customData: String?,
        quoteMessage: ChatMessagePayload?,
        quoteMessageText: NSAttributedString,
        quoteMessageImageUrl: URL?,
        quoteMessageImageSize: CGSize?,
        quoteMessageCustomData: String?,
        quoteMessageAccessibilityLabel: String,
        stickerShortcodesInQuoteMessage: [String],
        chatSession: ChatSession
    ) {
        
        self.chatSession = chatSession
        self.chatMessage = chatMessage
        self.id = id
        self.message = message
        self.username = username
        self.localProfileID = localProfileID
        self.syncPublishTimecode = syncPublishTimecode
        self.chatReactions = chatReactions
        self.profileImageUrl = profileImageUrl
        self.createdAt = createdAt
        self.bodyImageUrl = bodyImageUrl
        self.bodyImageSize = bodyImageSize
        self.stickerShortcodesInMessage = stickerShortcodesInMessage
        self.chatRoomId = chatRoomId
        self.customData = customData
        self.quoteMessage = quoteMessage
        self.quoteMessageText = quoteMessageText
        self.quoteMessageImageUrl = quoteMessageImageUrl
        self.quoteMessageImageSize = quoteMessageImageSize
        self.quoteMessageCustomData = quoteMessageCustomData
        self.quoteMessageAccessibilityLabel = quoteMessageAccessibilityLabel
        self.stickerShortcodesInQuoteMessage = stickerShortcodesInQuoteMessage
        
        if let videoTimestamp = syncPublishTimecode,
           let videoTimestampInterval = TimeInterval(videoTimestamp)
        {
            self.videoPlayerDebugTime = Date(timeIntervalSince1970: videoTimestampInterval)
        } else {
            self.videoPlayerDebugTime = nil
        }
    }
    
    /// Replaces the message body with 'Redacted'
    func redact(theme: Theme, quoteMessage: Bool) {
        let attributes = [
            NSAttributedString.Key.font: UIFont.italicSystemFont(ofSize: 14.0),
            NSAttributedString.Key.foregroundColor: theme.messageTextColor
        ]
        let attributedString = NSMutableAttributedString(
            string: "EngagementSDK.chat.messageCell.msgDeleted".localized(),
            attributes: attributes
        )
        
        if quoteMessage {
            self.quoteMessageText = attributedString
        } else {
            self.message = attributedString
        }
        
        self.accessibilityLabel = ("\(username) \(attributedString.mutableString)")
        self.isDeleted = true
    }
    
    func redactForBlockProfile(theme: Theme, quoteMessage: Bool) {
        let attributes = [
            NSAttributedString.Key.font: UIFont.italicSystemFont(ofSize: 14.0),
            NSAttributedString.Key.foregroundColor: theme.messageTextColor
        ]
        let attributedString = NSMutableAttributedString(
            string: "EngagementSDK.chat.messageCell.userBlocked".localized(),
            attributes: attributes
        )
        
        if quoteMessage {
            self.quoteMessageText = attributedString
        } else {
            self.message = attributedString
        }
        
        self.accessibilityLabel = ("\(username) \(attributedString.mutableString)")
        self.isDeleted = true
    }

    static func == (lhs: MessageViewModel, rhs: MessageViewModel) -> Bool {
        // In the case that both ids are 0 for local messages, this is because the user is muted, so the messages should only be considered equal if the send dates are equal
        if lhs.isLocalClient, rhs.isLocalClient, lhs.id == rhs.id {
            return lhs.syncPublishTimecode == rhs.syncPublishTimecode
        }

        return lhs.id == rhs.id
    }
}

