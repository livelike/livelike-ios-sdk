//
//  ReactionButtonViewModel.swift
//  
//
//  Created by Jelzon Monzon on 7/10/23.
//

import Foundation

struct ReactionButtonViewModel {
    let isMine: Bool
    let reactionID: String
}
