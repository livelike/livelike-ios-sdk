//
//  ChatViewController+Pause.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 7/22/19.
//

import Foundation
import LiveLikeCore
import LiveLikeSwift

public extension ChatViewController {

    @available(*, deprecated, message: "Please use `shouldShowIncomingMessages` and `isChatInputVisible` to achieve this")
    func pause() {
        CoreAnalytics.shared.record(
            name: UIAnalyticsConsts.Name.chatPauseStatusChanged,
            properties: [
                UIAnalyticsConsts.Property.previousPauseStatus: "Unpaused",
                UIAnalyticsConsts.Property.newPauseStatus: "Paused",
                UIAnalyticsConsts.Property.secondsInPreviousPauseStatus: Date().timeIntervalSince(timeChatPauseStatusChanged)
            ]
        )
        messageViewController.shouldShowIncomingMessages = false
        timeChatPauseStatusChanged = Date()
        log.info("Chat was paused.")
    }

    @available(*, deprecated, message: "Please use `shouldShowIncomingMessages` and `isChatInputVisible` to achieve this")
    func resume() {
        CoreAnalytics.shared.record(
            name: UIAnalyticsConsts.Name.chatPauseStatusChanged,
            properties: [
                UIAnalyticsConsts.Property.previousPauseStatus: "Paused",
                UIAnalyticsConsts.Property.newPauseStatus: "Unpaused",
                UIAnalyticsConsts.Property.secondsInPreviousPauseStatus: Date().timeIntervalSince(timeChatPauseStatusChanged)
            ]
        )
        messageViewController.shouldShowIncomingMessages = true
        log.info("Chat has resumed from pause.")
    }
}
