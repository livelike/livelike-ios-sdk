//
//  ChatMessageCell.swift
//  LiveLikeSDK
//
//  Created by Heinrich Dahms on 2019-01-19.
//

import UIKit
import LiveLikeSwift

class ChatMessageView: UIView {
    // MARK: - Outlets

    @IBOutlet weak var customMessageView: UIView!
    @IBOutlet weak var messageLabel: AnimatedLabel!
    
    @IBOutlet weak var quoteMessageLabel: AnimatedLabel!
    @IBOutlet weak var quoteMessageView: UIView!
    @IBOutlet weak var quoteMessageUsernameLabel: UILabel!
    @IBOutlet weak private var topBorder: UIView!
    @IBOutlet weak private var topBorderHeight: NSLayoutConstraint!
    @IBOutlet weak private var bottomBorder: UIView!
    @IBOutlet weak private var bottomBorderHeight: NSLayoutConstraint!
    
    @IBOutlet weak private var messageViewHolderWidth: NSLayoutConstraint!
    @IBOutlet weak private var messageViewHolderLeading: NSLayoutConstraint!
    @IBOutlet weak private var messageViewHolderTrailing: NSLayoutConstraint!
    @IBOutlet weak private var usernameLabel: UILabel!
    @IBOutlet weak private var messageViewHolder: UIView!
    @IBOutlet weak private var messageBackground: UIView!
    
    @IBOutlet weak private var badgeImageView: GIFImageView!
    @IBOutlet weak var timestampLabel: UILabel!
    @IBOutlet weak var alternateTimestampLabel: UILabel!
    @IBOutlet weak private var lhsImageView: GIFImageView!
    @IBOutlet weak private var lhsImageWidth: NSLayoutConstraint!
    @IBOutlet weak private var lhsImageHeight: NSLayoutConstraint!
    @IBOutlet weak private var lhsImageCenterAlignment: NSLayoutConstraint!
    @IBOutlet weak private var lhsImageTopAlignment: NSLayoutConstraint!
    @IBOutlet weak private var lhsImageBottomAlignment: NSLayoutConstraint!
    @IBOutlet weak private var lhsImageLeadingMargin: NSLayoutConstraint!
    
    // padding
    @IBOutlet weak var timestampLabelTrailingPaddingConstraint: NSLayoutConstraint!
    @IBOutlet weak var timestampLabelToBadgePaddingConstraint: NSLayoutConstraint!
    @IBOutlet weak var alternateTimestampLeadingPaddingConstraint: NSLayoutConstraint!
    @IBOutlet weak var alternateTimestampTopPaddingConstraint: NSLayoutConstraint!
    @IBOutlet weak var alternateTimestampBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak private var messageLeadPaddingConstraint: NSLayoutConstraint!
    @IBOutlet weak private var messageTrailingPaddingConstraint: NSLayoutConstraint!
    @IBOutlet weak private var usernameLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak private var usernameTrailingConstraint: NSLayoutConstraint?
    @IBOutlet weak private var messageBodyTrailingToSafeArea: NSLayoutConstraint!
    @IBOutlet weak private var messageBodyBottomMargin: NSLayoutConstraint!
    @IBOutlet weak private var messageBodyTopMargin: NSLayoutConstraint!
    @IBOutlet weak var messageLabelTopPadding: NSLayoutConstraint!
    @IBOutlet weak var quoteMessageViewTopPadding: NSLayoutConstraint!
    @IBOutlet weak var topSeparatorView: UIView!
    @IBOutlet weak var bottomSeparatorView: UIView!
    @IBOutlet weak var topSeparatorHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomSeparatorHeightConstraint: NSLayoutConstraint!
    
    lazy var reactionsDisplayView = constraintBased { ReactionsDisplayView() }

    // MARK: - Internal Properties

    weak var chatMessageViewDelegate: ChatMessageViewDelegate?

    var state: State = .normal {
        didSet {
            switch (oldValue, state) {
            case (.normal, .showingActionsPanel):
                showActionsPanel()
            case (.showingActionsPanel, .normal):
                hideActionsPanel()
            default:
                break
            }
        }
    }
    
    var theme: Theme = Theme()

    // MARK: - Private Properties
    private var isLocalClientMessage: Bool = false
    private var message: MessageViewModel?
    private let badgePadding: CGFloat = 16.0 // 14pt for badge + 2pt for leading
    private let timestampPadding: CGFloat = 2.0
    private var cellImageUrl: URL?
    private var timestampExists: Bool = false
    private var shouldDisplayAvatar: Bool = false
    private var enableChatMessageURLs: Bool = true
    private var chatMessageUrlPatterns: String?
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        if reactionsDisplayView.superview == nil {
            addSubview(reactionsDisplayView)
            
            let xInset = theme.chatCornerRadius + 3
            
            var reactionsVerticalAlignment: NSLayoutYAxisAnchor
            switch theme.messageReactionsVerticalAlignment {
            case .top:
                reactionsVerticalAlignment = messageViewHolder.topAnchor
            case .center:
                reactionsVerticalAlignment = messageViewHolder.centerYAnchor
            case .bottom:
                reactionsVerticalAlignment = messageViewHolder.bottomAnchor
            }
            
            NSLayoutConstraint.activate([
                reactionsDisplayView.centerYAnchor
                    .constraint(equalTo: reactionsVerticalAlignment, constant: theme.messageReactionsVerticalOffset),
                reactionsDisplayView.rightAnchor
                    .constraint(equalTo: messageViewHolder.rightAnchor, constant: -(xInset)),
                reactionsDisplayView.leftAnchor
                    .constraint(greaterThanOrEqualTo: messageViewHolder.leftAnchor, constant: xInset),
            ])
        }
    }

    // MARK: - Configuration Functions

    func configure(
        for message: MessageViewModel,
        indexPath: IndexPath,
        timestampFormatter: TimestampFormatter?,
        shouldDisplayDebugVideoTime: Bool,
        shouldDisplayAvatar: Bool,
        enableChatMessageURLs: Bool,
        chatMessageUrlPatterns: String?,
        theme: Theme
    ) {
        self.theme = theme
        self.message = message
        
        self.isLocalClientMessage = message.isLocalClient
        self.shouldDisplayAvatar = shouldDisplayAvatar
        self.enableChatMessageURLs = enableChatMessageURLs
        self.chatMessageUrlPatterns = chatMessageUrlPatterns
        self.topSeparatorView.backgroundColor = .clear
        self.bottomSeparatorView.backgroundColor = .clear
        self.hideActionsPanel()
        
        if let quoteMessage = message.quoteMessage {
            self.quoteMessageView.isHidden = false
            self.quoteMessageLabel.isHidden = false
            self.quoteMessageUsernameLabel.isHidden = false
            self.quoteMessageLabel.attributedText = message.quoteMessageText
            self.quoteMessageLabel.message = message
            self.quoteMessageUsernameLabel.text = quoteMessage.senderNickname
            messageLabelTopPadding.isActive = false
            self.messageLabel.message = message
            self.messageLabel.attributedText = message.message
        } else {
            self.quoteMessageView.isHidden = true
            self.quoteMessageLabel.isHidden = true
            self.quoteMessageUsernameLabel.isHidden = true
            messageLabelTopPadding.isActive = true
        }
        
        if let customData = message.customData, message.chatMessage.isCustomMessage {
            if let customView = chatMessageViewDelegate?.getCustomViewForCustomMessage(customData) {
                self.customMessageView.addSubview(customView)
                setupCustomView(customView: customView)
                self.messageLabel.isHidden = true
                self.customMessageView.isHidden = false
            } else {
                self.messageLabel.attributedText = NSAttributedString(string: customData)
                self.messageLabel.isHidden = false
                self.customMessageView.isHidden = true
            }
        } else {
            self.messageLabel.isHidden = false
            self.customMessageView.isHidden = true
            self.messageLabel.message = message
            self.messageLabel.attributedText = message.message
        }
        
        self.messageLabel.isUserInteractionEnabled = true
        self.timestampExists = timestampFormatter != nil
        self.usernameLabel.text = message.username
        // timestampLabel.text = timestampFormatter?(message.createdAt)
        self.timestampLabel.text = nil // using alternateTimestamp label only for now
        
        self.alternateTimestampLabel.text = {
            
            let defaultTimestamp = timestampFormatter?(message.createdAt)
            if shouldDisplayDebugVideoTime {
                var debugTimestamp = ""
                if let defaultTimestamp = defaultTimestamp {
                    debugTimestamp = "Created: \(defaultTimestamp)"
                }
                
                if let videoTime = message.videoPlayerDebugTime {
                    if let videoTimeCode = timestampFormatter?(videoTime) {
                        debugTimestamp.append(" | Sync Time: \(videoTimeCode)")
                    }
                }
                return debugTimestamp
            }
            return defaultTimestamp
            
        }()
                
        self.reactionsDisplayView.set(chatReactions: message.chatReactions, theme: self.theme)
        
        self.cellImageUrl = message.profileImageUrl
        self.accessibilityLabel = message.accessibilityLabel
        self.quoteMessageView.accessibilityLabel = message.quoteMessageAccessibilityLabel
        
        // Handle Chat Avatar
        if self.shouldDisplayAvatar {
            lhsImageView.isHidden = false
            if let imageUrl = cellImageUrl {
                lhsImageView.setImage(url: imageUrl)
            }
        } else {
            lhsImageView.isHidden = true
        }
        
        self.applyTheme()
        
        if let customSeparator = chatMessageViewDelegate?.getCustomViewBetweenMessages(beforeMessageID: message.id.asString, afterMessageID: nil) {
            customSeparator.translatesAutoresizingMaskIntoConstraints = true
            self.topSeparatorView.addSubview(customSeparator)
            self.topSeparatorHeightConstraint.constant = customSeparator.frame.height
            NSLayoutConstraint.activate([
                customSeparator.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
                customSeparator.leadingAnchor.constraint(equalTo: self.topSeparatorView.leadingAnchor),
                customSeparator.centerXAnchor.constraint(equalTo: self.topSeparatorView.centerXAnchor),
                customSeparator.centerYAnchor.constraint(equalTo: self.topSeparatorView.centerYAnchor),
                
                self.topSeparatorHeightConstraint
            ])
            self.bottomSeparatorHeightConstraint.isActive = false
            
        } else if let customSeparator = chatMessageViewDelegate?.getCustomViewBetweenMessages(beforeMessageID: nil, afterMessageID: message.id.asString) {
            customSeparator.translatesAutoresizingMaskIntoConstraints = true
            self.bottomSeparatorView.addSubview(customSeparator)
            self.bottomSeparatorHeightConstraint.constant = customSeparator.frame.height
            NSLayoutConstraint.activate([
                customSeparator.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
                customSeparator.leadingAnchor.constraint(equalTo: self.bottomSeparatorView.leadingAnchor),
                customSeparator.centerXAnchor.constraint(equalTo: self.bottomSeparatorView.centerXAnchor),
                customSeparator.centerYAnchor.constraint(equalTo: self.bottomSeparatorView.centerYAnchor),
                self.bottomSeparatorHeightConstraint
            ])
            self.topSeparatorHeightConstraint.isActive = false
        } else {
            self.topSeparatorHeightConstraint.isActive = false
            self.bottomSeparatorHeightConstraint.isActive = false
        }
    }
    
    func applyTheme() {
        timestampLabel.font = theme.chatMessageTimestampFont
        timestampLabel.textColor = theme.chatMessageTimestampTextColor
        alternateTimestampLabel.font = theme.chatMessageTimestampFont
        alternateTimestampLabel.textColor = theme.chatMessageTimestampTextColor
        alternateTimestampLabel.text = theme.chatMessageTimestampUppercased ? alternateTimestampLabel.text?.uppercased() : alternateTimestampLabel.text
        messageViewHolder.layer.cornerRadius = theme.messageCornerRadius
        quoteMessageView.layer.cornerRadius = theme.messageCornerRadius
        quoteMessageView.backgroundColor = theme.quoteMessageBackgroundColor
        messageBackground.backgroundColor = theme.messageBackgroundColor
        messageBackground.layer.cornerRadius = theme.messageCornerRadius
        messageLabel.textColor = theme.messageTextColor
        quoteMessageLabel.textColor = theme.quoteMessageTextColor
        if enableChatMessageURLs {
            if let validationRegEx = chatMessageUrlPatterns {
                configureForUrls(in: messageLabel.attributedText ?? NSAttributedString(), validationRegEx: validationRegEx)
                configureForUrls(in: quoteMessageLabel.attributedText ?? NSAttributedString(), validationRegEx: validationRegEx)
            } else {
                configureForUrls(in: messageLabel.attributedText ?? NSAttributedString(), validationRegEx: nil)
                configureForUrls(in: quoteMessageLabel.attributedText ?? NSAttributedString(), validationRegEx: nil)
            }
        }
        messageLabel.tintColor = theme.linkTextColor
        quoteMessageLabel.tintColor = theme.quoteMessageLinkTextColor
        usernameLabel.textColor = isLocalClientMessage ? theme.myUsernameTextColor : theme.usernameTextColor
        quoteMessageUsernameLabel.textColor = theme.quoteUsernameTextColor
        usernameLabel.font = theme.usernameTextFont
        usernameLabel.text = theme.usernameTextUppercased ? usernameLabel.text?.uppercased() : usernameLabel.text
        quoteMessageUsernameLabel.font = theme.quoteUsernameTextFont
        quoteMessageUsernameLabel.text = theme.usernameTextUppercased ? quoteMessageUsernameLabel.text?.uppercased() : quoteMessageUsernameLabel.text
        timestampLabelTrailingPaddingConstraint?.constant = theme.messageMargins.right
        timestampLabelToBadgePaddingConstraint?.constant = theme.messagePadding - badgePadding
        alternateTimestampLeadingPaddingConstraint?.constant = theme.messagePadding
        alternateTimestampTopPaddingConstraint?.constant = theme.chatMessageTimestampTopPadding
        messageLeadPaddingConstraint.constant = theme.messagePadding
        messageTrailingPaddingConstraint.constant = theme.messagePadding
        usernameLeadingConstraint?.constant = theme.messagePadding
        usernameTrailingConstraint?.constant = theme.messagePadding
        messageLabelTopPadding?.constant = theme.chatUsernameLabelBottomPadding
        quoteMessageViewTopPadding?.constant = theme.chatUsernameLabelBottomPadding
        
        timestampLabelToBadgePaddingConstraint?.isActive = timestampExists
        timestampLabelTrailingPaddingConstraint?.isActive = timestampExists
        alternateTimestampLeadingPaddingConstraint?.isActive = timestampExists
        alternateTimestampBottomConstraint?.isActive = timestampExists

        let usernameRowWidth: CGFloat = {
            var width = usernameLabel.intrinsicContentSize.width
            width += timestampLabel.intrinsicContentSize.width
            return width
        }()
        if usernameRowWidth > messageLabel.intrinsicContentSize.width {

            usernameTrailingConstraint?.constant += timestampLabel.intrinsicContentSize.width
            usernameTrailingConstraint?.isActive = true
            messageTrailingPaddingConstraint.isActive = false
        } else {
            usernameTrailingConstraint?.isActive = false
            messageTrailingPaddingConstraint.isActive = true
        }
        
        // Handle Chat Avatar
        if self.shouldDisplayAvatar {
            lhsImageWidth.constant = theme.chatImageWidth
            
            if theme.chatImageWidth > 0 {
                lhsImageHeight.constant = theme.chatImageHeight
                lhsImageView.livelike_cornerRadius = theme.chatImageCornerRadius
                lhsImageLeadingMargin.constant = -theme.messageMargins.left
            }
            
            messageViewHolderLeading.constant = theme.chatImageWidth + theme.chatImageTrailingMargin + theme.messageMargins.left
        } else {
            messageViewHolderLeading.constant = theme.messageMargins.left
        }
        messageViewHolderTrailing.constant = theme.messageMargins.right
        let currentMessageWidth = messageViewHolderWidth.constant
        messageViewHolderWidth.constant = currentMessageWidth - (messageViewHolderLeading.constant + messageViewHolderTrailing.constant)
        
        switch theme.chatImageVerticalAlignment {
        case .top:
            lhsImageTopAlignment.isActive = true
            lhsImageTopAlignment.constant = -(theme.messageTopBorderHeight + theme.messageMargins.top)
            lhsImageCenterAlignment.isActive = false
            lhsImageBottomAlignment.isActive = false
            
        case .center:
            lhsImageTopAlignment.isActive = false
            lhsImageCenterAlignment.isActive = true
            lhsImageBottomAlignment.isActive = false
        case .bottom:
            lhsImageTopAlignment.isActive = false
            lhsImageCenterAlignment.isActive = false
            lhsImageBottomAlignment.isActive = true
        }
        
        reactionsDisplayView.setTheme(theme)
        
        if theme.messageDynamicWidth {
            messageBodyTrailingToSafeArea.isActive = false
            usernameTrailingConstraint?.isActive = true
        } else {
            messageBodyTrailingToSafeArea.isActive = true
            usernameTrailingConstraint?.isActive = false
        }
        
        messageBodyTopMargin.constant = theme.messageMargins.top + theme.messageTopBorderHeight
        messageBodyBottomMargin.constant = theme.messageMargins.bottom + theme.messageBottomBorderHeight
        
        topBorder.backgroundColor = theme.messageTopBorderColor
        topBorderHeight.constant = theme.messageTopBorderHeight
        bottomBorder.backgroundColor = theme.messageBottomBorderColor
        bottomBorderHeight.constant = theme.messageBottomBorderHeight

    }

    override func layoutSubviews() {
        super.layoutSubviews()
        messageViewHolder.layer.cornerRadius = theme.messageCornerRadius
        
        // Handle Chat Avatar
        if self.shouldDisplayAvatar {
            /*  This hack sets a forced width to the `messageLabel` in order to handle issues with inline stickers.
             If the width is not set, the sticker images are not aligned correctly and go over
             the margins of the screen after a few orientation changes. (ES-1579) */
            messageLabel.frame.size.width = messageViewHolder.frame.size.width - (theme.messagePadding * 2)
        }
    }
    
    func prepareForReuse() {
        message = nil
        messageLabel.prepareForReuse()
    }
    
    private func setupCustomView(customView: UIView) {
        NSLayoutConstraint.activate([
            customView.centerYAnchor
                .constraint(equalTo: self.customMessageView.centerYAnchor),
            customView.rightAnchor
                .constraint(equalTo: self.customMessageView.rightAnchor),
            customView.leftAnchor
                .constraint(equalTo: self.customMessageView.leftAnchor),
            customView.bottomAnchor
                .constraint(equalTo: self.customMessageView.bottomAnchor)
        ])
    }
}

// MARK: - Actions Panel show/hide

internal extension ChatMessageView {
    enum State {
        case normal
        case showingActionsPanel
    }
    
    func configureForUrls(in messageText: NSAttributedString, validationRegEx: String?) {
        let returnMessage = NSMutableAttributedString(attributedString: messageText)
        var regExPattern = #"((https?):\/\/|www\.)?([-A-Z0-9._\+~#=]*)\.[a-z]{2,}\b([@%_\+.~#?&:\/\/=]+[-A-Z0-9\/]+)*"#
        if let regExString = validationRegEx {
            regExPattern = regExString
        }
        do {
            let regEx = try NSRegularExpression(pattern: regExPattern, options: NSRegularExpression.Options.caseInsensitive)
            let strText = messageText.string as NSString
            regEx.enumerateMatches(in: messageText.string, options: NSRegularExpression.MatchingOptions.init(rawValue: 0), range: NSRange(location: 0, length: strText.length)) { (result, flags, _) in
                let urlValue = strText.substring(with: result!.range)
                returnMessage.setAsLink(textString: urlValue)
                messageLabel.attributedText = returnMessage.attributedSubstring(from: NSRange(location: 0, length: returnMessage.length))
            }
        } catch {
            messageLabel.attributedText = messageText
        }
    }

    func showActionsPanel() {
        messageBackground.backgroundColor = theme.messageSelectedColor
    }

    func hideActionsPanel() {
        messageBackground.backgroundColor = theme.messageBackgroundColor
    }
}

// MARK: - Protocol conformances

extension ChatMessageView: Selectable {
    var isSelected: Bool {
        get { return state == .showingActionsPanel }
        set {
            state = newValue ? .showingActionsPanel : .normal
        }
    }
}

extension ChatMessageView: ChatActionsDelegateContainer {}
