//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//

import UIKit
import LiveLikeCore
import LiveLikeSwift

class QuizWidgetViewController: Widget {
    // MARK: - Internal Properties

    override var theme: Theme {
        didSet {
            applyTheme(theme)
            self.quizWidget.options.forEach { optionView in
                switch optionView.optionThemeStyle {
                case .unselected:
                    self.applyOptionTheme(
                        optionView: optionView,
                        optionTheme: theme.widgets.quiz.unselectedOption
                    )
                case .selected:
                    self.applyOptionTheme(
                        optionView: optionView,
                        optionTheme: theme.widgets.quiz.selectedOption
                    )
                case .correct:
                    self.applyOptionTheme(
                        optionView: optionView,
                        optionTheme: theme.widgets.quiz.correctOption
                    )
                case .incorrect:
                    self.applyOptionTheme(
                        optionView: optionView,
                        optionTheme: theme.widgets.quiz.incorrectOption
                    )
                }
            }
        }
    }
    override var currentState: WidgetState {
        willSet {
            previousState = self.currentState
        }
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.delegate?.widgetDidEnterState(widget: self, state: self.currentState)
                switch self.currentState {
                case .ready:
                    break
                case .interacting:
                    self.enterInteractingState()
                case .results:
                    self.enterResultsState()
                case .finished:
                    self.enterFinishedState()
                }
            }
        }
    }

    var closeButtonCompletion: ((WidgetViewModel) -> Void)?

    // MARK: - Private Stored Properties

    private let optionType: ChoiceWidgetOptionButton.Type
    private lazy var quizWidget: ChoiceWidget = {
        let view = VerticalChoiceWidget(optionType: self.optionType)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let lockVoteFooter: WidgetLockVoteFooterView = {
        let view = WidgetLockVoteFooterView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private let widgetFooterView: WidgetFooterView = {
        let view = WidgetFooterView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private var myQuizSelection: WidgetOption?
    private let mediaRepository: MediaRepository = MediaRepository.shared
    private var firstTapTime: Date?
    private let model: QuizWidgetModel

    // MARK: - Initializers

    override init(model: QuizWidgetModel) {
        self.model = model
        self.optionType = model.containsImages ? WideTextImageChoice.self : TextChoiceWidgetOptionButton.self
        super.init(model: model)
    }

    required init?(coder aDecoder: NSCoder) {
        assertionFailure("init(coder:) has not been implemented")
        return nil
    }

    override func moveToNextState() {
        switch self.currentState {
        case .ready:
            self.currentState = .interacting
        case .interacting:
            self.currentState = .results
        case .results:
            self.currentState = .finished
        case .finished:
            break
        }
    }

    override func addCloseButton(_ completion: @escaping (WidgetViewModel) -> Void) {
        self.closeButtonCompletion = completion
        quizWidget.titleView.showCloseButton()
    }

    override func addTimer(seconds: TimeInterval, completion: @escaping (WidgetViewModel) -> Void) {
        quizWidget.titleView.beginTimer(
            duration: seconds,
            animationFilepath: theme.lottieFilepaths.timer
        ) { [weak self] in
            guard let self = self else { return }
            completion(self)
        }
    }

    override func cancelTimer() {
        quizWidget.titleView.cancelTimer()
    }
}

// MARK: - View Lifecycle

extension QuizWidgetViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(quizWidget)
        quizWidget.constraintsFill(to: view)
        quizWidget.isUserInteractionEnabled = false

        quizWidget.titleView.closeButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)

        quizWidget.titleView.titleLabel.text = widgetTitle
        quizWidget.titleView.widgetTag.text = "EngagementSDK.widget.quiz.tag".localized()
        optionsArray?.forEach { option in
            quizWidget.addOption(withID: option.id) { optionView in
                optionView.text = option.text
                if let imageUrl = option.imageUrl {
                    mediaRepository.getImage(url: imageUrl) { result in
                        switch result {
                        case .success(let imageResult):
                            optionView.image = imageResult.image
                        case .failure(let error):
                            log.error(error)
                        }
                    }
                }
                optionView.delegate = self
                self.applyOptionTheme(
                    optionView: optionView,
                    optionTheme: theme.widgets.quiz.unselectedOption
                )
                if model.mostRecentAnswer?.choiceID == option.id {
                    self.applyOptionTheme(
                        optionView: optionView,
                        optionTheme: theme.widgets.quiz.selectedOption
                    )
                }
            }
        }

        // Configure Lock buttons in the footer
        lockVoteFooter.lockButton.setTitle("EngagementSDK.widget.quiz.answerButton.title".localized(), for: .normal)
        lockVoteFooter.confirmationLabel.text = "EngagementSDK.widget.quiz.answerConfirmationLabel.text".localized()
        lockVoteFooter.lockButtonHandler = { [weak self] view in
            guard let self = self else { return }
            guard self.myQuizSelection != nil else { return }
            view.confirmationLabel.isHidden = false
            view.disableLockButton()
            self.quizWidget.options.forEach {
                $0.isUserInteractionEnabled = false
            }
            self.delegate?.widgetStateCanComplete(widget: self, state: .interacting)
        }
        lockVoteFooter.disableLockButton()

        // Configure Sponsors in the Footer
        if let sponsor = model.sponsors.first {
            widgetFooterView.configureSponsor(sponsor)
        }

        widgetFooterView.configureWidgetLockView(lockVoteFooter)
        widgetFooterView.heightAnchor.constraint(equalToConstant: widgetFooterView.height).isActive = true
        quizWidget.coreWidgetView.footerView = widgetFooterView

        if model.mostRecentAnswer != nil {
            lockVoteFooter.confirmationLabel.isHidden = false
        }

        if model.isInteractivityExpired() {
            self.disableInteractions()
        }

        self.beginInteractiveUntilTimer { [weak self] in
            guard let self = self else { return }
            self.disableInteractions()
        }

        self.applyTheme(theme)
        self.model.delegate = self

        self.model.registerImpression()
    }
}

// MARK: - WidgetViewModel

extension QuizWidgetViewController {

    private func applyTheme(_ theme: Theme) {
        quizWidget.titleView.titleMargins = theme.choiceWidgetTitleMargins
        quizWidget.titleView.widgetTagMargins = theme.widgetTagMargins
        quizWidget.titleView.widgetTag.font = theme.widgetTagFont
        quizWidget.titleView.widgetTag.textColor = theme.widgetTagTextColor
        quizWidget.bodyBackground = theme.widgets.quiz.body.background
        quizWidget.optionSpacing = theme.interOptionSpacing
        quizWidget.headerBodySpacing = theme.titleBodySpacing
        quizWidget.titleView.titleLabel.textColor = theme.widgets.quiz.title.color
        quizWidget.titleView.titleLabel.font = theme.widgets.quiz.title.font
        quizWidget.applyContainerProperties(theme.widgets.quiz.main)
        quizWidget.titleView.applyContainerProperties(theme.widgets.quiz.header)

        widgetFooterView.applyTheme(theme, submitButton: theme.widgets.quiz.submitButton ?? .init())
        widgetFooterView.applyContainerProperties(theme.widgets.quiz.body)
    }

    private func applyOptionTheme(
        optionView: ChoiceWidgetOption,
        optionTheme: Theme.ChoiceWidget.Option?
    ) {
        guard let optionTheme = optionTheme else { return }

        optionView.descriptionFont = optionTheme.description.font
        optionView.descriptionTextColor = optionTheme.description.color
        optionView.percentageFont = optionTheme.percentage.font
        optionView.percentageTextColor = optionTheme.percentage.color
        optionView.barBackground = optionTheme.progressBar.background
        optionView.barCornerRadii = optionTheme.progressBar.cornerRadii
        optionView.applyContainerProperties(optionTheme.container)
        optionView.imageVerticalOffset = optionTheme.imageVerticalOffset
        optionView.imageHorizontalOffset = optionTheme.imageHorizontalOffset
        optionView.imageContentMode = optionTheme.imageContentMode
    }

    /// Shows results from `WidgetOption` data that already exists
    private func showResultsFromWidgetOptions() {
        guard let quizResults = options else { return }
        let totalVoteCount = quizResults.map { $0.voteCount ?? 0 }.reduce(0, +)

        self.quizWidget.options.forEach { option in
            guard let optionData = self.options?.first(where: { $0.id == option.id }) else {
                return
            }

            let answerCount = quizResults.first(where: { $0.id == option.id })?.voteCount ?? 0
            let votePercentage: CGFloat = totalVoteCount > 0 ? CGFloat(answerCount) / CGFloat(totalVoteCount) : 0
            let isCorrect = optionData.isCorrect ?? false

            if isCorrect {
                option.optionThemeStyle = .correct
                self.applyOptionTheme(
                    optionView: option,
                    optionTheme: self.theme.widgets.quiz.correctOption
                )
            } else if let answer = model.mostRecentAnswer, !isCorrect, answer.choiceID == optionData.id {
                option.optionThemeStyle = .incorrect
                self.applyOptionTheme(
                    optionView: option,
                    optionTheme: self.theme.widgets.quiz.incorrectOption
                )
            } else {
                if votePercentage > 0.0 {
                    self.applyOptionTheme(
                        optionView: option,
                        optionTheme: self.theme.widgets.quiz.incorrectOption
                    )
                    option.borderWidth = 0.0
                }
            }

            option.setProgress(votePercentage)
            option.isProgressLabelHidden = false
        }

    }

    @objc private func closeButtonPressed() {
        self.closeButtonCompletion?(self)
    }

    private func disableInteractions() {
        quizWidget.isUserInteractionEnabled = false
        quizWidget.options.forEach {
            $0.isUserInteractionEnabled = false
        }
    }

}

// MARK: - Private APIs

private extension QuizWidgetViewController {

    // MARK: Handle States

    func enterInteractingState() {
        if model.mostRecentAnswer == nil {
            quizWidget.isUserInteractionEnabled = true
            self.interactableState = .openToInteraction
            self.model.markAsInteractive()
        } else {
            self.disableInteractions()
            self.interactableState = .closedToInteraction
            self.delegate?.widgetStateCanComplete(widget: self, state: .interacting)
        }
    }

    func enterResultsState() {
        if self.firstTapTime != nil, self.timeOfLastInteraction != nil {
            CoreAnalytics.shared.recordWidgetInteracted(
                widgetModel: .quiz(self.model)
            )
        }

        self.disableInteractions()
        lockVoteFooter.disableLockButton()
        self.interactableState = .closedToInteraction

        guard let myQuizSelection = self.myQuizSelection else {
            showResultsFromWidgetOptions()
            self.delegate?.widgetStateCanComplete(widget: self, state: .results)
            return
        }

        self.lockVoteFooter.confirmationLabel.isHidden = false

        // Update local option vote data from selection
        if let localOption = options?.first(where: { $0.id == myQuizSelection.id }) {
            if let voteCount = localOption.voteCount {
                localOption.voteCount = voteCount + 1
            } else {
                localOption.voteCount = 1
            }
        }

        self.model.lockInAnswer(choiceID: myQuizSelection.id) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success:
                self.quizWidget.options.forEach { option in
                    guard let optionData = self.options?.first(where: { $0.id == option.id }) else {
                        return
                    }

                    guard let isCorrect = optionData.isCorrect else {
                        return
                    }

                    if isCorrect {
                        option.optionThemeStyle = .correct
                        self.applyOptionTheme(
                            optionView: option,
                            optionTheme: self.theme.widgets.quiz.correctOption
                        )
                    } else if !isCorrect, myQuizSelection.id == optionData.id {
                        option.optionThemeStyle = .incorrect
                        self.applyOptionTheme(
                            optionView: option,
                            optionTheme: self.theme.widgets.quiz.incorrectOption
                        )
                    }
                }

                // Optimistically start showing result graph
                // from local data prior to the delegate data
                self.showResultsFromWidgetOptions()

                let animationFilepath: String? = {
                    if myQuizSelection.isCorrect ?? false {
                        return self.theme.lottieFilepaths.randomWin()
                    } else {
                        return self.theme.lottieFilepaths.randomLose()
                    }
                }()

                self.quizWidget.playOverlayAnimation(animationFilepath: animationFilepath) {
                    self.delegate?.widgetStateCanComplete(widget: self, state: .results)
                }
            case .failure(let error):
                log.error(error)
            }
        }
    }

    func enterFinishedState() {
        // Display results from `WidgetOptions` if entering `finished` state without casting a vote
        if self.myQuizSelection == nil {
            showResultsFromWidgetOptions()
        }
        lockVoteFooter.disableLockButton()

        self.quizWidget.stopOverlayAnimation()
        self.delegate?.widgetStateCanComplete(widget: self, state: .finished)
    }
}

extension QuizWidgetViewController: QuizWidgetModelDelegate {
    func quizWidgetModel(_ model: QuizWidgetModel, answerCountDidChange voteCount: Int, forChoice optionID: String) {
        guard currentState == .results || currentState == .finished else { return } // Only update progress while in results state
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            guard let optionView = self.quizWidget.options.first(where: { $0.id == optionID }) else { return }
            guard model.totalAnswerCount > 0 else { return }
            optionView.setProgress((CGFloat(voteCount) / CGFloat(model.totalAnswerCount)))
        }
    }
}

extension QuizWidgetViewController: ChoiceWidgetOptionDelegate {
    func wasSelected(_ option: ChoiceWidgetOption) {

        // Ignore repeated selections
        guard myQuizSelection?.id != option.id else { return }

        guard let opt = options?.first(where: { $0.id == option.id }) else {
            return
        }
        self.userDidInteract = true
        self.lockVoteFooter.enableLockbutton()

        self.myQuizSelection = opt

        quizWidget.options.forEach {
            $0.optionThemeStyle = .unselected
            self.applyOptionTheme(
                optionView: $0,
                optionTheme: theme.widgets.quiz.unselectedOption
            )
        }
        option.optionThemeStyle = .selected
        self.applyOptionTheme(
            optionView: option,
            optionTheme: theme.widgets.quiz.selectedOption
        )

        let now = Date()
        if firstTapTime == nil {
            firstTapTime = now
        }
        timeOfLastInteraction = now
        interactionCount += 1
        let selectedVote = ChoiceWidgetVote(from: option)
        self.delegate?.userDidSubmitVote(self, selectedVote: selectedVote)
        self.delegate?.userDidInteract(self)
    }

    func wasDeselected(_ option: ChoiceWidgetOption) {

    }
}
