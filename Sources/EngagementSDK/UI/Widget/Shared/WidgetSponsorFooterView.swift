//
//  WidgetSponsorFooterView.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 8/19/21.
//

import UIKit
import LiveLikeCore
import LiveLikeSwift

class WidgetSponsorFooterView: ThemeableView {

    let height: CGFloat = 50.0

    private let stack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    private let sponsoredByLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let sponsorLogo: UIImageView = {
        let logo = UIImageView()
        logo.contentMode = .scaleAspectFit
        logo.translatesAutoresizingMaskIntoConstraints = false
        return logo
    }()

    private let mediaRepository: MediaRepository = MediaRepository.shared

    init(sponsor: Sponsor) {
        super.init()

        mediaRepository.getImage(url: sponsor.logoURL) { [weak self] result in
            switch result {
            case .success(let imageResult):
                guard let self = self else { return }

                self.stack.addArrangedSubview(self.sponsoredByLabel)

                let ratio = imageResult.image.size.width / imageResult.image.size.height
                self.sponsorLogo.image = imageResult.image
                self.stack.addArrangedSubview(self.sponsorLogo)
                self.stack.osAdaptive_setCustomSpacing(8.0, after: self.sponsoredByLabel)

                self.addSubview(self.stack)

                NSLayoutConstraint.activate([
                    self.stack.centerXAnchor.constraint(equalTo: self.centerXAnchor),
                    self.stack.centerYAnchor.constraint(equalTo: self.centerYAnchor),
                    self.sponsorLogo.heightAnchor.constraint(equalToConstant: 20.0),
                    self.sponsorLogo.widthAnchor.constraint(equalTo: self.sponsorLogo.heightAnchor, multiplier: ratio),
                ])
            case .failure(let error):
                log.error(error)
            }
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func applyTheme(_ theme: Theme) {
        sponsoredByLabel.attributedText = NSMutableAttributedString(
            "EngagementSDK.widget.footer.sponsor.sponsoredByLabel".localized(),
            font: theme.widgetSponsorFont,
            color: theme.widgetSponsorFontColor,
            lineSpacing: 0.0
        )
        self.backgroundColor = theme.widgetSponsorBackgroundColor
    }
}
