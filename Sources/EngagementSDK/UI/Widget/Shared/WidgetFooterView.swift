//
//  WidgetFooterView.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 8/19/21.
//

import UIKit
import LiveLikeSwift

class WidgetFooterView: ThemeableView {

    var widgetLockVoteFooterView: WidgetLockVoteFooterView?
    var widgetSponsorFooterView: WidgetSponsorFooterView?

    var height: CGFloat {
        var footerHeight: CGFloat = 0.0

        if widgetLockVoteFooterView != nil {
            footerHeight = widgetLockVoteFooterView?.height ?? 0.0
        }

        if widgetSponsorFooterView != nil {
            footerHeight += widgetSponsorFooterView?.height ?? 0.0
        }

        return footerHeight
    }

    private let footerStack: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        return stackView
    }()

    override init() {
        super.init()

        addSubview(footerStack)

        NSLayoutConstraint.activate([
            footerStack.topAnchor.constraint(equalTo: self.topAnchor, constant: 0.0),
            footerStack.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            footerStack.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            footerStack.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0)
        ])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configureSponsor(_ sponsor: Sponsor) {
        widgetSponsorFooterView = WidgetSponsorFooterView(sponsor: sponsor)
        widgetSponsorFooterView?.translatesAutoresizingMaskIntoConstraints = false
    }

    func configureLockVoteBtn(_ handler: @escaping ((WidgetLockVoteFooterView) -> Void)) {
        widgetLockVoteFooterView = WidgetLockVoteFooterView()
        widgetLockVoteFooterView?.lockButtonHandler = handler
        widgetLockVoteFooterView?.translatesAutoresizingMaskIntoConstraints = false
    }

    func configureWidgetLockView(_ widgetLockView: WidgetLockVoteFooterView) {
        widgetLockVoteFooterView = widgetLockView
        widgetLockVoteFooterView?.translatesAutoresizingMaskIntoConstraints = false
    }

    override func didMoveToSuperview() {
        if let widgetLockVoteFooterView = widgetLockVoteFooterView {
            footerStack.addArrangedSubview(widgetLockVoteFooterView)
        }

        if let widgetSponsorFooterView = widgetSponsorFooterView {
            footerStack.addArrangedSubview(widgetSponsorFooterView)
        }

        self.layoutIfNeeded()
    }

    func applyTheme(_ theme: Theme, submitButton: Theme.SubmitButton?) {
        widgetLockVoteFooterView?.applyTheme(
            theme,
            submitButtonTheme: submitButton ?? .init()
        )
        widgetSponsorFooterView?.applyTheme(theme)
    }
}
