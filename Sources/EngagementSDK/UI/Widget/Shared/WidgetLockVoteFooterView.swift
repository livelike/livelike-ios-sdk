//
//  WidgetLockVoteFooterView.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 5/17/21.
//

import UIKit

class WidgetLockVoteFooterView: ThemeableView {

    let height: CGFloat = 50.0
    var lockButtonHandler: (WidgetLockVoteFooterView) -> Void = { _ in }

    var theme: Theme = Theme()
    var submitButton: Theme.SubmitButton = Theme.SubmitButton()

    let lockButtonBackground: ThemeableView = {
        let view = ThemeableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    let lockButton: UIButton = {
        let button = UIButton()
        let title = "EngagementSDK.widget.voteButton.title".localized()
        button.setTitle(title, for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    let confirmationLabel: UILabel = {
        let label = UILabel()
        label.text = "EngagementSDK.widget.voteConfirmationLabel.text".localized()
        label.isHidden = true
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.adjustsFontSizeToFitWidth = true
        return label
    }()

    let activityIndicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(style: .white)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.hidesWhenStopped = true
        return view
    }()

    override init() {
        super.init()

        addSubview(lockButtonBackground)
        addSubview(lockButton)
        addSubview(confirmationLabel)
        addSubview(activityIndicator)

        NSLayoutConstraint.activate([
            lockButtonBackground.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            lockButtonBackground.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            lockButtonBackground.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1 / 4),
            lockButtonBackground.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),

            lockButton.topAnchor.constraint(equalTo: lockButtonBackground.topAnchor),
            lockButton.leadingAnchor.constraint(equalTo: lockButtonBackground.leadingAnchor),
            lockButton.trailingAnchor.constraint(equalTo: lockButtonBackground.trailingAnchor),
            lockButton.bottomAnchor.constraint(equalTo: lockButtonBackground.bottomAnchor),

            confirmationLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            confirmationLabel.leadingAnchor.constraint(equalTo: lockButton.trailingAnchor, constant: 10),
            confirmationLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            confirmationLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),

            activityIndicator.leadingAnchor.constraint(equalTo: lockButton.trailingAnchor, constant: 10),
            activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor),
            activityIndicator.heightAnchor.constraint(equalToConstant: 40),
            activityIndicator.widthAnchor.constraint(equalToConstant: 40),
        ])

        lockButton.addTarget(self, action: #selector(buttonSelected), for: .touchUpInside)

        lockButton.layer.cornerRadius = 6

        self.applyTheme(theme, submitButtonTheme: self.submitButton)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc private func buttonSelected() {
        lockButtonHandler(self)
    }

    func disableLockButton() {
        self.lockButton.isEnabled = false
        self.applySubmitButtonTheme()
    }

    func enableLockbutton() {
        self.lockButton.isEnabled = true
        self.applySubmitButtonTheme()
    }

    func applyTheme(_ theme: Theme, submitButtonTheme: Theme.SubmitButton) {
        self.theme = theme
        self.submitButton = submitButtonTheme
        self.confirmationLabel.theme(submitButtonTheme.confirmation)
        applySubmitButtonTheme()
    }

    private func applySubmitButtonTheme() {
        if self.lockButton.isEnabled {
            self.lockButtonBackground.applyContainerProperties(submitButton.buttonEnabled)
            self.lockButton.setTitleColor(submitButton.textEnabled.color, for: .normal)
            self.lockButton.titleLabel?.font = submitButton.textEnabled.font
        } else {
            self.lockButtonBackground.applyContainerProperties(submitButton.buttonDisabled)
            self.lockButton.setTitleColor(submitButton.textDisabled.color, for: .normal)
            self.lockButton.titleLabel?.font = submitButton.textDisabled.font
        }
    }
}
