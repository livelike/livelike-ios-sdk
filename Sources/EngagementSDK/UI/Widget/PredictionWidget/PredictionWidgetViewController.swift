//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//

import Lottie
import UIKit
import LiveLikeCore
import LiveLikeSwift

/// Game logic for prediction widgets
class PredictionWidgetViewController: Widget {

    // MARK: Properties

    override var theme: Theme {
        didSet {
            self.applyTheme(theme)
            self.predictionWidgetView.options.forEach { optionView in
                switch optionView.optionThemeStyle {
                case .selected:
                    self.applyOptionTheme(
                        optionView: optionView,
                        optionTheme: theme.widgets.prediction.selectedOption
                    )
                case .unselected:
                    self.applyOptionTheme(
                        optionView: optionView,
                        optionTheme: theme.widgets.prediction.unselectedOption
                    )
                default:
                    break
                }
            }
        }
    }

    override var currentState: WidgetState {
        willSet {
            previousState = self.currentState
        }
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.delegate?.widgetDidEnterState(widget: self, state: self.currentState)
                switch self.currentState {
                case .ready:
                    break
                case .interacting:
                    self.enterInteractingState()
                case .results:
                    self.enterResultsState()
                case .finished:
                    self.enterFinishedState()
                }
            }
        }
    }

    // MARK: Private Properties

    private var closeButtonAction: (() -> Void)?
    private let mediaRepository: MediaRepository = MediaRepository.shared
    private let model: PredictionWidgetModel
    private let viewModel: PredictionWidgetVoteCountViewModel
    private var firstTapTime: Date?

    // MARK: View Properties

    private let optionType: ChoiceWidgetOptionButton.Type
    private(set) lazy var predictionWidgetView: ChoiceWidget = {
        let textChoiceWidget = VerticalChoiceWidget(optionType: self.optionType)
        textChoiceWidget.translatesAutoresizingMaskIntoConstraints = false
        return textChoiceWidget
    }()

    private let widgetFooterView: WidgetFooterView = {
        let view = WidgetFooterView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    override init(model: PredictionWidgetModel) {
        self.model = model
        self.viewModel = PredictionWidgetVoteCountViewModel(model: model)
        if model.containsImages {
            self.optionType = WideTextImageChoice.self
        } else {
            self.optionType = TextChoiceWidgetOptionButton.self
        }
        super.init(model: model)

        self.viewModel.voteCountsDidChange = { [weak self] in
            guard let self = self else { return }
            DispatchQueue.main.async {
                zip(self.predictionWidgetView.options, self.model.options)
                    .filter { view, data in view.id == data.id }
                    .forEach { view, data in
                        let votePercentage: CGFloat = self.viewModel.getVoteCountTotal() > 0
                            ? CGFloat(self.viewModel.getVoteCount(optionID: data.id)) / CGFloat(self.viewModel.getVoteCountTotal())
                            : 0
                        view.setProgress(votePercentage)
                    }
            }
        }

        self.viewModel.didRecieveFollowUp = { [weak self] in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.delegate?.widgetStateCanComplete(widget: self, state: .results)
            }
        }

        self.predictionWidgetView.isUserInteractionEnabled = false

        if let sponsor = model.sponsors.first {
            widgetFooterView.configureSponsor(sponsor)
        }

        predictionWidgetView.coreWidgetView.footerView = widgetFooterView
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        super.loadView()
        view.addSubview(predictionWidgetView)
        predictionWidgetView.constraintsFill(to: view)

        predictionWidgetView.titleView.closeButton.addTarget(self, action: #selector(onCloseButtonPressed), for: .touchUpInside)
        predictionWidgetView.titleView.titleLabel.text = widgetTitle
        predictionWidgetView.titleView.widgetTag.text = "EngagementSDK.widget.prediction.tag".localized()
        widgetFooterView.heightAnchor.constraint(equalToConstant: widgetFooterView.height).isActive = true

        self.model.options.forEach { optionData in
            predictionWidgetView.addOption(withID: optionData.id) { optionView in
                optionView.text = optionData.text
                if let imageURL = optionData.imageURL {
                    mediaRepository.getImage(url: imageURL) { result in
                        switch result {
                        case .success(let imageResult):
                            optionView.image = imageResult.image
                        case .failure(let error):
                            log.error(error)
                        }
                    }
                }
                optionView.delegate = self
                optionView.isProgressLabelHidden = true
                optionView.isProgressBarHidden = true
            }
        }

        self.userDidInteract = model.mostRecentVote != nil
        self.applyTheme(theme)

        DispatchQueue.main.async {
            zip(self.predictionWidgetView.options, self.model.options)
                .filter { view, data in view.id == data.id }
                .forEach { view, data in
                    if let mostRecentVote = self.model.mostRecentVote, mostRecentVote.optionID == data.id {
                        self.applyOptionTheme(
                            optionView: view,
                            optionTheme: self.theme.widgets.prediction.selectedOption
                        )
                    } else {
                        self.applyOptionTheme(
                            optionView: view,
                            optionTheme: self.theme.widgets.prediction.unselectedOption
                        )
                    }

                    let votePercentage: CGFloat = self.viewModel.getVoteCountTotal() > 0
                        ? CGFloat(self.viewModel.getVoteCount(optionID: data.id)) / CGFloat(self.viewModel.getVoteCountTotal())
                        : 0
                    view.setProgress(votePercentage)
                }
        }

        if model.isInteractivityExpired() {
            self.disableInteractions()
        }

        self.beginInteractiveUntilTimer { [weak self] in
            guard let self = self else { return }
            self.disableInteractions()
        }

        self.model.registerImpression()
    }

    override func moveToNextState() {
        switch self.currentState {
        case .ready:
            self.currentState = .interacting
        case .interacting:
            self.currentState = .results
        case .results:
            self.currentState = .finished
        case .finished:
            break
        }
    }

    override func addCloseButton(_ completion: @escaping (WidgetViewModel) -> Void) {
        self.closeButtonAction = { [weak self] in
            guard let self = self else { return }
            completion(self)
        }
        predictionWidgetView.titleView.showCloseButton()
    }

    override func addTimer(seconds: TimeInterval, completion: @escaping (WidgetViewModel) -> Void) {
        predictionWidgetView.titleView.beginTimer(
            duration: model.interactionTimeInterval,
            animationFilepath: theme.lottieFilepaths.timer
        ) { [weak self] in
            guard let self = self else { return }
            completion(self)
        }
    }

    private func enterInteractingState() {
        self.predictionWidgetView.isUserInteractionEnabled = true
        self.model.markAsInteractive()
        self.delegate?.widgetStateCanComplete(widget: self, state: .interacting)
    }

    private func enterResultsState() {
        if self.firstTapTime != nil, self.timeOfLastInteraction != nil {
            CoreAnalytics.shared.recordWidgetInteracted(
                widgetModel: .prediction(self.model)
            )
        }
        self.disableInteractions()
        self.interactableState = .closedToInteraction

        if userDidInteract {
            self.predictionWidgetView.options.forEach {
                $0.isProgressLabelHidden = false
                $0.isProgressBarHidden = false
            }

            if let animationFilepath = self.theme.lottieFilepaths.predictionTimerComplete.randomElement() {
                self.predictionWidgetView.playOverlayAnimation(animationFilepath: animationFilepath) {
                    self.delegate?.widgetStateCanComplete(widget: self, state: .results)
                }
            }
        } else {
            self.delegate?.widgetStateCanComplete(widget: self, state: .results)
        }
    }

    private func enterFinishedState() {
        if userDidInteract {
            self.predictionWidgetView.options.forEach {
                $0.isProgressLabelHidden = false
                $0.isProgressBarHidden = false
            }
        }
        predictionWidgetView.stopOverlayAnimation()
        self.delegate?.widgetStateCanComplete(widget: self, state: .finished)
    }

    @objc private func onCloseButtonPressed() {
        self.closeButtonAction?()
    }

    private func applyTheme(_ theme: Theme) {
        predictionWidgetView.titleView.titleMargins = theme.choiceWidgetTitleMargins
        predictionWidgetView.titleView.widgetTagMargins = theme.widgetTagMargins
        predictionWidgetView.titleView.widgetTag.font = theme.widgetTagFont
        predictionWidgetView.titleView.widgetTag.textColor = theme.widgetTagTextColor
        predictionWidgetView.applyContainerProperties(theme.widgets.prediction.main)
        predictionWidgetView.titleView.applyContainerProperties(theme.widgets.prediction.header)
        predictionWidgetView.bodyBackground = theme.widgets.prediction.body.background
        predictionWidgetView.titleView.titleLabel.textColor = theme.widgets.prediction.title.color
        predictionWidgetView.titleView.titleLabel.font = theme.widgets.prediction.title.font
        widgetFooterView.applyTheme(theme, submitButton: nil)
    }

    private func applyOptionTheme(
        optionView: ChoiceWidgetOption,
        optionTheme: Theme.ChoiceWidget.Option?
    ) {
        guard let optionTheme = optionTheme else { return }
        optionView.applyContainerProperties(optionTheme.container)
        optionView.descriptionTextColor = optionTheme.description.color
        optionView.descriptionFont = optionTheme.description.font
        optionView.barBackground = optionTheme.progressBar.background
        optionView.barCornerRadii = optionTheme.progressBar.cornerRadii
        optionView.percentageFont = optionTheme.percentage.font
        optionView.percentageTextColor = optionTheme.percentage.color
        optionView.imageVerticalOffset = optionTheme.imageVerticalOffset
        optionView.imageHorizontalOffset = optionTheme.imageHorizontalOffset
        optionView.imageContentMode = optionTheme.imageContentMode
    }

    private func disableInteractions() {
        self.predictionWidgetView.coreWidgetView.contentView?.isUserInteractionEnabled = false
    }
}

extension PredictionWidgetViewController: ChoiceWidgetOptionDelegate {
    func wasSelected(_ option: ChoiceWidgetOption) {
        self.userDidInteract = true

        for optionButton in predictionWidgetView.options {
            optionButton.optionThemeStyle = .unselected
            self.applyOptionTheme(
                optionView: optionButton,
                optionTheme: theme.widgets.prediction.unselectedOption
            )
        }
        option.optionThemeStyle = .selected
        self.applyOptionTheme(
            optionView: option,
            optionTheme: theme.widgets.prediction.selectedOption
        )

        let now = Date()
        if firstTapTime == nil {
            firstTapTime = now
        }
        timeOfLastInteraction = now
        interactionCount += 1
        let selectedVote = ChoiceWidgetVote(from: option)
        self.delegate?.userDidInteract(self)
        self.delegate?.userDidSubmitVote(self, selectedVote: selectedVote)
        self.viewModel.submitVote(optionID: option.id)
    }

    func wasDeselected(_ option: ChoiceWidgetOption) {

    }
}
