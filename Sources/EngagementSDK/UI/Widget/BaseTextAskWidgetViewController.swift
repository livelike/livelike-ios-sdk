//
//  BaseTextAskWidgetViewController.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 7/28/21.
//

import UIKit

class BaseTextAskWidgetViewController: Widget {
    
    let widgetView: TextAskWidgetView = {
        let view = TextAskWidgetView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    var textViewPlaceholder: String = "EngagementSDK.widget.textAsk.textViewPlaceholder".localized() {
        didSet {
            self.setPlaceholderIfNeeded()
        }
    }
    
    override var theme: Theme {
        didSet {
            self.applyTheme(theme)
        }
    }
    
    var askTheme: Theme.TextAskWidget {
        theme.widgets.textAsk
    }
    
    let characterLimit: Int = 240

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(widgetView)
        
        NSLayoutConstraint.activate([
            widgetView.topAnchor.constraint(equalTo: view.topAnchor),
            widgetView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            widgetView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            widgetView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        widgetView.characterCountLabel.text = "\(characterLimit)"
        widgetView.userReplyTextView.returnKeyType = .done
        widgetView.userReplyTextView.delegate = self
        widgetView.submitReplyButton.addTarget(self, action: #selector(submitReplyButtonSelected(_:)), for: .touchUpInside)
        setPlaceholderIfNeeded()
        applyTheme(theme)
        setSubmitButtonIsEnabled(false)
    }
    
    @objc func submitReplyButtonSelected(_ sender: UIButton) {
        widgetView.userReplyTextView.isEditable = false
        setSubmitButtonIsEnabled(false)
        widgetView.userReplyTextView.theme(askTheme.replyTextDisabled)
    }
    
    private func setPlaceholderIfNeeded() {
        if
            !self.widgetView.userReplyTextView.hasText,
            self.widgetView.userReplyTextView.text != self.textViewPlaceholder
        {
            self.widgetView.userReplyTextView.text = textViewPlaceholder
            widgetView.userReplyTextView.theme(askTheme.placeholder)
        }
    }
    
    private func applyTheme(_ theme: Theme) {
        widgetView.titleView.titleMargins = theme.choiceWidgetTitleMargins
        widgetView.titleView.widgetTagMargins = theme.widgetTagMargins
        self.applyAskTheme(theme.widgets.textAsk)
    }
    
    private func applyAskTheme(_ theme: Theme.TextAskWidget) {
        widgetView.applyContainerProperties(theme.main)
        widgetView.titleView.applyContainerProperties(theme.header)
        widgetView.body.applyContainerProperties(theme.body)
        widgetView.titleView.titleLabel.theme(theme.title)
        widgetView.promptLabel.theme(theme.prompt)
        widgetView.confirmationLabel.theme(theme.confirmation)
        widgetView.submitReplyThemeableView.applyContainerProperties(theme.submitButtonDisabled)
        widgetView.submitReplyButton.titleLabel?.font = theme.submitTextEnabled.font
        widgetView.submitReplyButton.setTitleColor(theme.submitTextEnabled.color, for: .normal)
        widgetView.submitReplyButton.setTitleColor(theme.submitTextDisabled.color, for: .disabled)
        widgetView.userReplyThemableView.applyContainerProperties(theme.replyTextView)
        widgetView.userReplyTextView.theme(theme.placeholder)
        widgetView.characterCountLabel.theme(theme.characterCount)
    }
    
    func setSubmitButtonIsEnabled(_ isEnabled: Bool) {
        widgetView.submitReplyButton.isEnabled = isEnabled
        if isEnabled {
            widgetView.submitReplyThemeableView.applyContainerProperties(askTheme.submitButtonEnabled)
            widgetView.submitReplyButton.titleLabel?.font = askTheme.submitTextEnabled.font
        } else {
            widgetView.submitReplyThemeableView.applyContainerProperties(askTheme.submitButtonDisabled)
            widgetView.submitReplyButton.titleLabel?.font = askTheme.submitTextDisabled.font
        }
    }
}

extension BaseTextAskWidgetViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if self.widgetView.userReplyTextView.text == self.textViewPlaceholder {
            self.widgetView.userReplyTextView.text = nil
            self.widgetView.userReplyTextView.theme(askTheme.reply)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        setPlaceholderIfNeeded()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        // Dismiss keyboard when user hits return key
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        
        // Prevent user from typing after server enforced character limit reached
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        if newText == " " {
            return false
        }
        
        if newText.utf8.count > characterLimit {
            return false
        }
        
        widgetView.characterCountLabel.text = "\(characterLimit - newText.count)"
        
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.setSubmitButtonIsEnabled(!textView.text.isEmpty)
    }
}
