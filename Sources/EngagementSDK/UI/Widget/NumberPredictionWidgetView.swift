//
//  NumberPredictionWidgetView.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 9/24/21.
//

import UIKit

class NumberPredictionWidgetView: ThemeableView {
    
    let mainStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        return stackView
    }()
    
    let body: ThemeableView = {
        let view = ThemeableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let optionsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 24
        return stackView
    }()
    
    let widgetTitleView: WidgetTitleView = {
        let view = WidgetTitleView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let header: ThemeableView = {
        let view = ThemeableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let questionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let widgetFooterView: WidgetFooterView = {
        let view = WidgetFooterView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init() {
        super.init()
        
        backgroundColor = .white
        
        addSubview(mainStackView)
        mainStackView.addArrangedSubview(widgetTitleView)
        mainStackView.addArrangedSubview(body)
        body.addSubview(optionsStackView)
        header.addSubview(questionLabel)
        
        NSLayoutConstraint.activate([
            mainStackView.topAnchor.constraint(equalTo: topAnchor),
            mainStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            mainStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            mainStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            questionLabel.topAnchor.constraint(equalTo: header.topAnchor),
            questionLabel.leadingAnchor.constraint(equalTo: header.leadingAnchor),
            questionLabel.trailingAnchor.constraint(equalTo: header.trailingAnchor),
            questionLabel.bottomAnchor.constraint(equalTo: header.bottomAnchor),
            
            optionsStackView.topAnchor.constraint(equalTo: body.topAnchor, constant: 16),
            optionsStackView.leadingAnchor.constraint(equalTo: body.leadingAnchor, constant: 16),
            optionsStackView.trailingAnchor.constraint(equalTo: body.trailingAnchor, constant: -16),
            optionsStackView.bottomAnchor.constraint(equalTo: body.bottomAnchor, constant: -16),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    class TextOptionView: ThemeableView {
        let imageView: UIImageView = {
            let imageView = UIImageView()
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            return imageView
        }()
        
        let textLabel: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.numberOfLines = 0
            label.adjustsFontSizeToFitWidth = true
            return label
        }()
        
        let numberTextFieldBackground: ThemeableView = {
            let view = ThemeableView()
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let numberTextField: UITextField = {
            let textField = UITextField()
            textField.translatesAutoresizingMaskIntoConstraints = false
            textField.keyboardType = .numberPad
            textField.textAlignment = .center
            textField.adjustsFontSizeToFitWidth = true
            textField.minimumFontSize = 5
            return textField
        }()
        
        override init() {
            super.init()
            
            let numberTextFieldContainer = UIView()
            numberTextFieldContainer.translatesAutoresizingMaskIntoConstraints = false
            numberTextFieldContainer.addSubview(numberTextFieldBackground)
            numberTextFieldContainer.addSubview(numberTextField)
            
            let stackView = UIStackView(arrangedSubviews: [
                imageView,
                textLabel,
                numberTextFieldContainer
            ])
            stackView.axis = .horizontal
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.osAdaptive_setCustomSpacing(10, after: imageView)
            stackView.osAdaptive_setCustomSpacing(10, after: textLabel)
            
            addSubview(stackView)
            
            NSLayoutConstraint.activate([
                imageView.widthAnchor.constraint(equalToConstant: 64),
                imageView.heightAnchor.constraint(equalToConstant: 64),
                
                textLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 0),
                
                numberTextFieldContainer.widthAnchor.constraint(equalToConstant: 53),
                numberTextFieldBackground.centerYAnchor.constraint(equalTo: centerYAnchor),
                numberTextFieldBackground.heightAnchor.constraint(equalToConstant: 36),
                numberTextFieldBackground.widthAnchor.constraint(equalToConstant: 53),
                numberTextField.topAnchor.constraint(equalTo: numberTextFieldBackground.topAnchor),
                numberTextField.leadingAnchor.constraint(equalTo: numberTextFieldBackground.leadingAnchor),
                numberTextField.trailingAnchor.constraint(equalTo: numberTextFieldBackground.trailingAnchor),
                numberTextField.bottomAnchor.constraint(equalTo: numberTextFieldBackground.bottomAnchor),
                
                stackView.topAnchor.constraint(equalTo: topAnchor),
                stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
                stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
                stackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            ])
            
            addDoneButtonOnKeyboard()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private func addDoneButtonOnKeyboard() {
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
            doneToolbar.barStyle = .default

            let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))

            let items = [flexSpace, done]
            doneToolbar.items = items
            doneToolbar.sizeToFit()

            numberTextField.inputAccessoryView = doneToolbar
        }

        @objc private func doneButtonAction() {
            numberTextField.resignFirstResponder()
        }
    }
}
