//
//  WidgetTimelineViewController.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 2/18/21.
//

import UIKit
import LiveLikeSwift

/// A presentation mode for Widgets
///
/// Widgets in this mode will appear in a vertical list sorted by the time the Widget is created (descending).
///
/// On load, published widgets will be loaded from history and, by default, display results, and be non-interactive.
/// New widgets will appear at the top of the list and be interactive until the timer expires.
///
/// You can configure which and how widgets are displayed by overriding:
/// * didLoadInitialWidgets
/// * didLoadMoreWidgets
/// * didReceiveNewWidget
/// * makeWidget
open class WidgetTimelineViewController: InteractiveWidgetTimelineViewController {

    // Determines if the displayed widget should be interactable or not
    private var widgetIsInteractableByID: [String: Bool] = [:]

    public override init(contentSession: ContentSession) {
        super.init(contentSession: contentSession)
        self.widgetStateController = TimelineWidgetViewDelegate()
    }

    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    open override func didLoadMoreWidgets(_ widgetModels: [WidgetModel]) -> [WidgetModel] {
        widgetModels.forEach {
            self.widgetIsInteractableByID[$0.id] = false
        }
        return widgetModels
    }

    /// A factory method that is called when a widget is ready to be displayed.
    /// - Parameter widgetModel: The `WidgetModel` to be displayed
    /// - Returns: A `UIViewController` that represents the Widget. Return `nil` to ignore this widget.
    open override func makeWidget(_ widgetModel: WidgetModel) -> UIViewController? {
        var widget: Widget?
        switch widgetModel {
        case .socialEmbed(let model):
            let socialEmbedWidget = SocialEmbedWidgetViewController(model: model)
            socialEmbedWidget.didFinishLoadingContent = { [weak self] in
                self?.tableView.beginUpdates()
                self?.tableView.endUpdates()
            }
            widget = socialEmbedWidget
        default:
            widget = DefaultWidgetFactory.makeWidget(from: widgetModel)
        }

        /// Set interactivity
        widget?.delegate = widgetStateController
        if widgetIsInteractableByID[widgetModel.id] ?? false {
            widget?.moveToNextState()
        } else {
            widget?.currentState = .finished
        }

        widgetIsInteractableByID[widgetModel.id] = false
        return widget
    }

    open override func contentSession(_ session: ContentSession, didReceiveWidget widget: WidgetModel) {
        DispatchQueue.main.async {
            guard let widgetModel = self.didReceiveNewWidget(widget) else { return }
            self.widgetModels.insert(widgetModel, at: 0)
            self.widgetIsInteractableByID[widgetModel.id] = true
            self.tableView.insertSections(IndexSet(integer: 0), with: .none)
        }
    }
}
