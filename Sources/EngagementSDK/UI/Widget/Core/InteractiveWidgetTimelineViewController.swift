//
//  InteractiveWidgetTimelineViewController.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 5/18/21.
//

import UIKit
import LiveLikeCore
import LiveLikeSwift

/// A presentation mode for Widgets
///
/// Widgets in this mode will appear in a vertical list sorted by the time the Widget is created (descending).
///
/// Default widgets in this mode are indefinitely interactive with a few exceptions.
/// Quiz and Image Slider widgets will become non-interactive after the user has locked in their vote.
/// Prediction Widgets will become non-interactive after the Prediction Follow Up has been published.
///
/// You can configure which and how widgets are displayed by overriding:
/// * didLoadInitialWidgets
/// * didLoadMoreWidgets
/// * didReceiveNewWidget
/// * makeWidget
open class InteractiveWidgetTimelineViewController: UIViewController, ContentSessionDelegate {

    public let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.allowsSelection = false
        tableView.separatorStyle = .none
        tableView.isHidden = true
        return tableView
    }()

    public let snapToLiveButton: UIImageView = {
        let snapToLiveButton = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        snapToLiveButton.image = UIImage(named: "chatIcLive", in: EngagementResources.resourceBundle, compatibleWith: nil)
        snapToLiveButton.translatesAutoresizingMaskIntoConstraints = false
        snapToLiveButton.alpha = 0.0
        snapToLiveButton.isUserInteractionEnabled = true
        return snapToLiveButton
    }()

    private var snapToLiveBottomConstraint: NSLayoutConstraint!

    public private(set) weak var session: ContentSession?

    /// The list of loaded widgets sorted in descending order by the creation date
    public internal(set) var widgetModels: [WidgetModel] = []

    /// Change to modify the behavior of the Default Widgets
    public var widgetStateController: WidgetViewDelegate = InteractiveTimelineWidgetViewDelegate()

    private let emptyTableLoadingIndicator: UIView = {
        let spinner = UIActivityIndicatorView()
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.startAnimating()
        return spinner
    }()

    /// Determines whether fetching data from backend is in progress
    var isLoading: Bool = true

    public init(contentSession: ContentSession) {
        self.session = contentSession
        super.init(nibName: nil, bundle: nil)
    }

    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(emptyTableLoadingIndicator)
        view.addSubview(tableView)
        view.addSubview(snapToLiveButton)

        snapToLiveBottomConstraint = snapToLiveButton.bottomAnchor.constraint(
            equalTo: view.bottomAnchor,
            constant: -16
        )
        NSLayoutConstraint.activate([
            emptyTableLoadingIndicator.topAnchor.constraint(equalTo: view.topAnchor),
            emptyTableLoadingIndicator.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            emptyTableLoadingIndicator.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            emptyTableLoadingIndicator.bottomAnchor.constraint(equalTo: view.bottomAnchor),

            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),

            snapToLiveButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            snapToLiveBottomConstraint
        ])

        tableView.dataSource = self
        tableView.delegate = self

        snapToLiveButton.addGestureRecognizer({
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(snapToLive))
            tapGestureRecognizer.numberOfTapsRequired = 1
            return tapGestureRecognizer
        }())

        session?.getPostedWidgetModels(page: .first) { [weak self] result in
            guard let self = self else { return }
            DispatchQueue.main.async {
                switch result {
                case let .success(widgetModels):
                    guard let models = widgetModels else {
                        self.isLoading = false
                        self.tableView.isHidden = false
                        return
                    }
                    let filteredModels = self.didLoadInitialWidgets(models)
                    self.widgetModels.append(contentsOf: filteredModels)

                    self.tableView.reloadData()
                    self.session?.delegate = self
                case let .failure(error):
                    log.error(error)
                }
                self.isLoading = false
                self.tableView.isHidden = false
            }
        }
    }

    private func snapToLiveIsHidden(_ isHidden: Bool) {
        view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
            self.snapToLiveBottomConstraint?.constant = isHidden ? self.snapToLiveButton.bounds.height : -16
            self.view.layoutIfNeeded()
            self.snapToLiveButton.alpha = isHidden ? 0 : 1
        }, completion: nil)
    }

    @objc func snapToLive() {
        tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }

    /// Called when the first page of widget history is successfully loaded.
    /// - Parameter widgetModels: The `WidgetModel`s that were loaded
    /// - Returns:The `WidgetModels` to be added to the timeline
    open func didLoadInitialWidgets(_ widgetModels: [WidgetModel]) -> [WidgetModel] {
        return widgetModels
    }

    /// Called when the next page of widget history is successfully loaded.
    /// - Parameter widgetModels: The `WidgetModels` that were loaded
    /// - Returns:The `WidgetModels` to be added to the timeline
    open func didLoadMoreWidgets(_ widgetModels: [WidgetModel]) -> [WidgetModel] {
        return widgetModels
    }

    /// Called when a new widget is published.
    /// - Parameter widgetModel: The `WidgetModel` that was received
    /// - Returns: The `WidgetModel` to be added to the timeline. Return `nil` to ignore this `WidgetModel`
    open func didReceiveNewWidget(_ widgetModel: WidgetModel) -> WidgetModel? {
        return widgetModel
    }

    /// A factory method that is called when a widget is ready to be displayed.
    /// - Parameter widgetModel: The `WidgetModel` to be displayed
    /// - Returns: A `UIViewController` that represents the Widget. Return `nil` to ignore this widget.
    open func makeWidget(_ widgetModel: WidgetModel) -> UIViewController? {
        var widget: Widget?
        switch widgetModel {
        case .socialEmbed(let model):
            let socialEmbedWidget = SocialEmbedWidgetViewController(model: model)
            socialEmbedWidget.didFinishLoadingContent = { [weak self] in
                self?.tableView.beginUpdates()
                self?.tableView.endUpdates()
            }
            widget = socialEmbedWidget
        default:
            widget = DefaultWidgetFactory.makeWidget(from: widgetModel)
        }

        /// Set interactivity
        widget?.delegate = widgetStateController
        widget?.moveToNextState()
        return widget
    }

    func createLoadingFooterView() -> UIView {
        let loadingView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 44))
        let spinner = UIActivityIndicatorView()
        spinner.center = loadingView.center
        loadingView.addSubview(spinner)
        spinner.startAnimating()

        return loadingView
    }

    // MARK: - Helpers
    private class WidgetTableViewCell: UITableViewCell {
        var widget: UIViewController?

        deinit {
            widget?.willMove(toParent: nil)
            widget?.removeFromParent()
            widget = nil
        }
    }

    // MARK: - ContentSessionDelegate

    public func playheadTimeSource(_ session: ContentSession) -> Date? { return nil }
    public func session(_ session: ContentSession, didChangeStatus status: SessionStatus) {}
    public func session(_ session: ContentSession, didReceiveError error: Error) {}
    public func chat(session: ContentSession, roomID: String, newMessage message: ChatMessage) {}
    public func widget(_ session: ContentSession, didBecomeReady widget: Widget) {}
    open func contentSession(_ session: ContentSession, didReceiveWidget widget: WidgetModel) {
        DispatchQueue.main.async {
            guard let widgetModel = self.didReceiveNewWidget(widget) else { return }
            self.widgetModels.insert(widgetModel, at: 0)
            self.tableView.insertSections(IndexSet(integer: 0), with: .none)
        }
    }
}

// MARK: - UITableViewDelegate

/// :nodoc:
extension InteractiveWidgetTimelineViewController: UITableViewDelegate {
    open func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if
            let firstVisibleRow = self.tableView.indexPathsForVisibleRows?.first?.section,
            firstVisibleRow > 0
        {
            self.snapToLiveIsHidden(false)
        } else {
            self.snapToLiveIsHidden(true)
        }

        /// Handle infinite scroll
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height

        if offsetY > contentHeight - scrollView.frame.height, !isLoading {
            self.isLoading = true
            self.tableView.tableFooterView = createLoadingFooterView()
            session?.getPostedWidgetModels(page: .next) { [weak self] result in
                guard let self = self else { return }
                DispatchQueue.main.async {
                    switch result {
                    case let .success(models):
                        guard let models = models else {
                            self.isLoading = false
                            self.tableView.tableFooterView = nil
                            return
                        }

                        let insertionRange = Range(
                            uncheckedBounds: (
                                self.widgetModels.count,
                                self.widgetModels.count + models.count
                            )
                        )
                        let indexesOfNewSections = IndexSet(insertionRange)

                        let widgetModels = self.didLoadMoreWidgets(models)
                        self.widgetModels.append(contentsOf: widgetModels)

                        // Capture offset before update to prevent cells from jumping
                        let offsetBeforeUpdate = self.tableView.contentOffset

                        UIView.setAnimationsEnabled(false)
                        self.tableView.beginUpdates()
                        self.tableView.insertSections(indexesOfNewSections, with: .none)
                        self.tableView.endUpdates()
                        UIView.setAnimationsEnabled(true)

                        self.tableView.setContentOffset(offsetBeforeUpdate, animated: false)
                    case let .failure(error):
                        log.error(error)
                    }
                    self.isLoading = false
                    self.tableView.tableFooterView = nil
                }

            }
        }
    }
}

// MARK: - UITableViewDataSource

/// :nodoc:
extension InteractiveWidgetTimelineViewController: UITableViewDataSource {
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    open func numberOfSections(in tableView: UITableView) -> Int {
        return widgetModels.count
    }

    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = WidgetTableViewCell()

        let widgetModel = widgetModels[indexPath.section]

        if let widget = self.makeWidget(widgetModel) {
            addChild(widget)
            widget.didMove(toParent: self)
            widget.view.translatesAutoresizingMaskIntoConstraints = false
            cell.contentView.addSubview(widget.view)

            NSLayoutConstraint.activate([
                widget.view.topAnchor.constraint(equalTo: cell.contentView.topAnchor),
                widget.view.leadingAnchor.constraint(equalTo: cell.contentView.leadingAnchor),
                widget.view.trailingAnchor.constraint(equalTo: cell.contentView.trailingAnchor),
                widget.view.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor)
            ])
            cell.widget = widget
        }

        return cell
    }

    /// Overrride the following functions to add spacing between cells or a custom `UIView` separator
    open func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }

    open func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }

    open func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }

    open func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }

    open func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

}
