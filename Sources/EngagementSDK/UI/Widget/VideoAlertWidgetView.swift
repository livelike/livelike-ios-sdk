//
//  VideoAlertWidgetView.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 7/12/21.
//

import UIKit
import AVKit

/// A View that describes the default Video Alert Widget
class VideoAlertWidgetView: ThemeableView {

    var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        return stackView
    }()

    var header: ThemeableView = {
        let gradientView = ThemeableView()
        gradientView.translatesAutoresizingMaskIntoConstraints = false
        return gradientView
    }()

    var body: ThemeableView = {
        let view = ThemeableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    var videoContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    var footer: ThemeableView = {
        let view = ThemeableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 12, weight: .bold)
        label.preservesSuperviewLayoutMargins = true
        return label
    }()

    var bodyLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 14, weight: .regular)
        return label
    }()

    var playButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(
            UIImage(named: "icPlayButton", in: EngagementResources.resourceBundle, compatibleWith: nil),
            for: .normal
        )
        return button
    }()

    var brokenMediaContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = true
        return view
    }()

    var brokenMediaImageView: UIImageView = {
        let image = UIImage(
            named: "icBrokenMedia",
            in: EngagementResources.resourceBundle,
            compatibleWith: nil
        )
        let imageView = UIImageView(image: image)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = false
        return imageView
    }()

    var brokenMediaLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 14, weight: .medium)
        label.text = "EngagementSDK.widget.videoAlert.cantPlayError".localized()
        return label
    }()

    var muteToggle: UIButton = {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = .systemFont(ofSize: 14, weight: .bold)
        button.setTitle("EngagementSDK.widget.videoAlert.muteButton".localized(), for: .normal)
        button.setImage(
            UIImage(named: "icVolumeOff", in: EngagementResources.resourceBundle, compatibleWith: nil),
            for: .normal
        )
        button.setTitle("EngagementSDK.widget.videoAlert.unmuteButton".localized(), for: .selected)
        button.setImage(
            UIImage(named: "icVolumeOn", in: EngagementResources.resourceBundle, compatibleWith: nil),
            for: .selected
        )
        button.layer.cornerRadius = 6
        button.backgroundColor = UIColor(white: 1, alpha: 0.35)
        button.contentEdgeInsets = UIEdgeInsets(top: 6, left: 6, bottom: 6, right: 6)
        return button
    }()

    var linkButton: UIButton = {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.contentHorizontalAlignment = .left
        button.titleLabel?.font = .systemFont(ofSize: 12, weight: .bold)
        button.contentEdgeInsets = UIEdgeInsets(top: 10, left: 16, bottom: 10, right: 16)
        return button
    }()

    var linkImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(
            named: "widget_alert_right_arrow",
            in: EngagementResources.resourceBundle,
            compatibleWith: nil
        )
        imageView.isUserInteractionEnabled = false
        return imageView
    }()

    var activityIndicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(style: .white)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.hidesWhenStopped = true
        return view
    }()

    override init() {
        super.init()

        addSubviews()
        activateLayoutConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func addSubviews() {
        addSubview(stackView)
        stackView.addArrangedSubview(header)
        stackView.addArrangedSubview(body)
        stackView.addArrangedSubview(videoContainer)
        stackView.addArrangedSubview(footer)
        addSubview(titleLabel)
        addSubview(playButton)
        addSubview(bodyLabel)
        addSubview(muteToggle)
        addSubview(linkButton)
        addSubview(linkImageView)
        addSubview(activityIndicator)
        addSubview(brokenMediaContainer)
        brokenMediaContainer.addSubview(brokenMediaImageView)
        brokenMediaContainer.addSubview(brokenMediaLabel)
    }

    func activateLayoutConstraints() {
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor),

            titleLabel.topAnchor.constraint(equalTo: header.topAnchor, constant: 10),
            titleLabel.leadingAnchor.constraint(equalTo: header.leadingAnchor, constant: 16),
            titleLabel.trailingAnchor.constraint(equalTo: header.trailingAnchor, constant: -16),
            titleLabel.bottomAnchor.constraint(equalTo: header.bottomAnchor, constant: -10),

            bodyLabel.topAnchor.constraint(equalTo: body.topAnchor, constant: 10),
            bodyLabel.leadingAnchor.constraint(equalTo: body.leadingAnchor, constant: 16),
            bodyLabel.trailingAnchor.constraint(equalTo: body.trailingAnchor, constant: -16),
            bodyLabel.bottomAnchor.constraint(equalTo: body.bottomAnchor, constant: -10),

            videoContainer.heightAnchor.constraint(equalTo: videoContainer.widthAnchor, multiplier: 9 / 16),

            playButton.centerXAnchor.constraint(equalTo: videoContainer.centerXAnchor),
            playButton.centerYAnchor.constraint(equalTo: videoContainer.centerYAnchor),
            playButton.heightAnchor.constraint(equalTo: videoContainer.heightAnchor, multiplier: 0.5),
            playButton.widthAnchor.constraint(equalTo: playButton.heightAnchor),

            brokenMediaImageView.topAnchor.constraint(equalTo: brokenMediaContainer.topAnchor),
            brokenMediaImageView.leadingAnchor.constraint(equalTo: brokenMediaContainer.leadingAnchor),
            brokenMediaImageView.trailingAnchor.constraint(equalTo: brokenMediaLabel.leadingAnchor, constant: -3),
            brokenMediaImageView.bottomAnchor.constraint(equalTo: brokenMediaContainer.bottomAnchor),

            brokenMediaLabel.topAnchor.constraint(equalTo: brokenMediaContainer.topAnchor),
            brokenMediaLabel.leadingAnchor.constraint(equalTo: brokenMediaImageView.trailingAnchor),
            brokenMediaLabel.trailingAnchor.constraint(equalTo: brokenMediaContainer.trailingAnchor),
            brokenMediaLabel.bottomAnchor.constraint(equalTo: brokenMediaContainer.bottomAnchor),

            brokenMediaContainer.centerYAnchor.constraint(equalTo: videoContainer.centerYAnchor),
            brokenMediaContainer.centerXAnchor.constraint(equalTo: videoContainer.centerXAnchor),
            brokenMediaContainer.widthAnchor.constraint(lessThanOrEqualTo: videoContainer.widthAnchor),

            muteToggle.centerXAnchor.constraint(equalTo: videoContainer.centerXAnchor),
            muteToggle.bottomAnchor.constraint(equalTo: videoContainer.bottomAnchor, constant: -12),

            linkButton.topAnchor.constraint(equalTo: footer.topAnchor),
            linkButton.leadingAnchor.constraint(equalTo: footer.leadingAnchor),
            linkButton.trailingAnchor.constraint(equalTo: footer.trailingAnchor),
            linkButton.bottomAnchor.constraint(equalTo: footer.bottomAnchor),

            linkImageView.heightAnchor.constraint(equalToConstant: 10),
            linkImageView.widthAnchor.constraint(equalToConstant: 10),
            linkImageView.trailingAnchor.constraint(equalTo: footer.trailingAnchor, constant: -16),
            linkImageView.centerYAnchor.constraint(equalTo: footer.centerYAnchor),

            activityIndicator.topAnchor.constraint(equalTo: videoContainer.topAnchor),
            activityIndicator.leadingAnchor.constraint(equalTo: videoContainer.leadingAnchor),
            activityIndicator.trailingAnchor.constraint(equalTo: videoContainer.trailingAnchor),
            activityIndicator.bottomAnchor.constraint(equalTo: videoContainer.bottomAnchor)
        ])
    }
}
