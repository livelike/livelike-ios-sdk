//
//  ImageSliderViewController.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 5/13/19.
//

import UIKit
import LiveLikeCore
import LiveLikeSwift

class ImageSliderViewController: Widget {
    override var theme: Theme {
        didSet {
            configureView()
        }
    }

    override var currentState: WidgetState {
        willSet {
            previousState = self.currentState
        }
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.delegate?.widgetDidEnterState(widget: self, state: self.currentState)
                switch self.currentState {
                case .ready:
                    break
                case .interacting:
                    self.enterInteractingState()
                case .results:
                    self.enterResultsState()
                case .finished:
                    self.enterFinishedState()
                }
            }
        }
    }

    override var dismissSwipeableView: UIView {
        return self.imageSliderView.titleView
    }

    private let averageAnimationSeconds: CGFloat = 2
    private let additionalResultsSeconds: Double = 5

    private var closeButtonAction: (() -> Void)?
    private let model: ImageSliderWidgetModel
    private var sliderChangedCount: Int = 0
    private var firstTimeSliderChanged: Date?

    private lazy var imageSliderView: ImageSliderView = {
        var imageUrls = self.model.options.map({ $0.imageURL })

        let initialSliderValue = self.model.initialMagnitude
        let imageSliderView = ImageSliderView(
            thumbImageUrls: imageUrls,
            initialSliderValue: Float(initialSliderValue),
            timerAnimationFilepath: self.theme.lottieFilepaths.timer
        )
        imageSliderView.translatesAutoresizingMaskIntoConstraints = false
        imageSliderView.sliderView.addTarget(self, action: #selector(imageSliderViewValueChanged), for: .touchUpInside)

        return imageSliderView
    }()

    private let widgetFooterView: WidgetFooterView = {
        let view = WidgetFooterView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private var didPressLockButton: Bool = false

    private var timerDuration: TimeInterval?
    private var interactionTimer: Timer?

    // MARK: - Init

    override init(model: ImageSliderWidgetModel) {
        self.model = model
        super.init(model: model)

        if let sponsor = model.sponsors.first {
            widgetFooterView.configureSponsor(sponsor)
        }

        widgetFooterView.configureLockVoteBtn { [weak self] view in
            guard let self = self else { return }

            self.didPressLockButton = true
            view.confirmationLabel.isHidden = false
            view.disableLockButton()
            self.lockSlider()
            self.delegate?.widgetStateCanComplete(widget: self, state: .interacting)
        }
        imageSliderView.coreWidgetView.footerView = widgetFooterView

        NotificationCenter.default.addObserver(self, selector: #selector(didMoveToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    @objc private func didMoveToForeground() {
        // Restart the timer animation from continuous time since background
        // We need to do this because when Lottie goes into background it pauses the animation
        if
            let interactionTimer = interactionTimer,
            let lottieAnimation = imageSliderView.timerView.animation,
            let duration = timerDuration
        {
            let timeRemaining = interactionTimer.fireDate.timeIntervalSince(Date())
            let timeScalar = lottieAnimation.duration / duration

            imageSliderView.timerView.currentTime = (duration - timeRemaining) * timeScalar
            imageSliderView.timerView.play()
        }
    }

    required init?(coder aDecoder: NSCoder) {
        assertionFailure("init(coder:) has not been implemented")
        return nil
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        enterReadyState()
        configureView()
        self.model.delegate = self
        self.model.registerImpression()
    }

    override func moveToNextState() {
        switch self.currentState {
        case .ready:
            self.currentState = .interacting
        case .interacting:
            self.currentState = .results
        case .results:
            self.currentState = .finished
        case .finished:
            break
        }
    }

    override func addCloseButton(_ completion: @escaping (WidgetViewModel) -> Void) {
        self.closeButtonAction = { [weak self] in
            guard let self = self else { return }
            completion(self)
        }
        self.imageSliderView.closeButton.isHidden = false
    }

    override func addTimer(seconds: TimeInterval, completion: @escaping (WidgetViewModel) -> Void) {
        timerDuration = seconds
        interactionTimer = Timer.scheduledTimer(withTimeInterval: seconds, repeats: false, block: { [weak self] _ in
            guard let self = self else { return }
            completion(self)
        })

        imageSliderView.timerView.animationSpeed = CGFloat(imageSliderView.timerView.animation?.duration ?? 0) / CGFloat(seconds)
        imageSliderView.timerView.isHidden = false
        imageSliderView.timerView.play { [weak self] finished in
            if finished {
                self?.imageSliderView.timerView.isHidden = true
            }
        }
    }

    override func cancelTimer() {
        interactionTimer?.invalidate()
        imageSliderView.timerView.stop()
        imageSliderView.timerView.isHidden = true
    }

    // MARK: - Private Method

    private func configureView() {
        imageSliderView.avgIndicatorView.animationSpeed = CGFloat(imageSliderView.timerView.animation?.duration ?? 0) / CGFloat(averageAnimationSeconds)
        imageSliderView.applyContainerProperties(theme.widgets.imageSlider.main)
        imageSliderView.bodyView.applyContainerProperties(theme.widgets.imageSlider.body)

        let title: String = {
            var title = model.question
            if theme.uppercaseTitleText {
                title = title.uppercased()
            }
            return title
        }()
        imageSliderView.titleLabel.text = title
        imageSliderView.titleLabel.theme(theme.widgets.imageSlider.title)

        imageSliderView.sliderView.minimumTrackTintColor = .clear
        imageSliderView.sliderView.maximumTrackTintColor = .white

        imageSliderView.resultsTrackLeft = theme.widgets.imageSlider.resultsTrackLeft.background
        imageSliderView.resultsTrackRight = theme.widgets.imageSlider.resultsTrackRight.background

        imageSliderView.titleView.applyContainerProperties(theme.widgets.imageSlider.header)

        imageSliderView.customSliderTrack.livelike_startColor = theme.widgets.imageSlider.interactiveTrackLeft.background
        imageSliderView.customSliderTrack.livelike_endColor = theme.widgets.imageSlider.interactiveTrackRight.background

        imageSliderView.titleMargins = theme.choiceWidgetTitleMargins

        imageSliderView.closeButton.addTarget(self, action: #selector(closeButtonSelected), for: .touchUpInside)

        view.addSubview(imageSliderView)
        imageSliderView.constraintsFill(to: view)

        widgetFooterView.applyTheme(theme, submitButton: theme.widgets.imageSlider.submitButton)
        widgetFooterView.applyContainerProperties(theme.widgets.imageSlider.footer)
        widgetFooterView.heightAnchor.constraint(equalToConstant: widgetFooterView.height).isActive = true

        if model.mostRecentVote != nil {
            widgetFooterView.widgetLockVoteFooterView?.disableLockButton()
            widgetFooterView.widgetLockVoteFooterView?.confirmationLabel.isHidden = false
        }

        if model.isInteractivityExpired() {
            self.disableInteractions()
        }

        self.beginInteractiveUntilTimer { [weak self] in
            guard let self = self else { return }
            self.disableInteractions()
        }
    }

    private func disableInteractions() {
        imageSliderView.isUserInteractionEnabled = false
    }

    @objc private func closeButtonSelected() {
        closeButtonAction?()
    }

    @objc private func imageSliderViewValueChanged() {
        self.userDidInteract = true

        let now = Date()
        if firstTimeSliderChanged == nil {
            firstTimeSliderChanged = now
        }
        timeOfLastInteraction = now
        sliderChangedCount += 1
        self.delegate?.userDidInteract(self)
    }

    private func lockSlider() {
        imageSliderView.sliderView.isUserInteractionEnabled = false
    }

    private func playAverageAnimation(completion: @escaping () -> Void) {
        imageSliderView.avgIndicatorView.play { finished in
            if finished {
                completion()
            }
        }
    }

    // MARK: Handle States

    private func enterReadyState() {
        self.disableInteractions()
        imageSliderView.timerView.isHidden = true
    }

    private func enterInteractingState() {
        if model.mostRecentVote == nil {
            imageSliderView.isUserInteractionEnabled = true
            self.interactableState = .openToInteraction
            self.model.markAsInteractive()
        } else {
            self.disableInteractions()
            self.interactableState = .closedToInteraction
            self.delegate?.widgetStateCanComplete(widget: self, state: .interacting)
        }
    }

    private func enterResultsState() {
        if
            self.firstTimeSliderChanged != nil,
            self.timeOfLastInteraction != nil
        {
            CoreAnalytics.shared.recordWidgetInteracted(
                widgetModel: .imageSlider(self.model)
            )
        }

        let magnitude = self.imageSliderView.sliderView.value
        log.info("Submitting vote with magnitude: \(magnitude)")
        self.imageSliderView.timerView.isHidden = true
        self.lockSlider()
        self.widgetFooterView.widgetLockVoteFooterView?.disableLockButton()
        self.interactableState = .closedToInteraction

        // can complete results if user did not interact
        guard self.sliderChangedCount > 0 || self.didPressLockButton else {
            showPreviouslyVotedResults()
            self.delegate?.widgetStateCanComplete(widget: self, state: .results)
            return
        }

        self.widgetFooterView.widgetLockVoteFooterView?.confirmationLabel.isHidden = false

        self.model.lockInVote(magnitude: Double(magnitude))

        self.imageSliderView.averageVote = Float(calculateMagnitude(myMagnitude: Double(magnitude)))

        self.playAverageAnimation { [weak self] in
            guard let self = self else { return }
            self.imageSliderView.showResultsTrack()
            DispatchQueue.main.asyncAfter(deadline: .now() + self.additionalResultsSeconds) { [weak self] in
                guard let self = self else { return }
                self.delegate?.widgetStateCanComplete(widget: self, state: .results)
            }
        }
    }

    private func calculateMagnitude(myMagnitude: Double) -> Double {
        let engagementCount = Double(self.model.engagementCount)
        let total: Double = engagementCount * self.model.averageMagnitude
        let newAverage = (total + myMagnitude) / (engagementCount + 1)
        return newAverage
    }

    private func enterFinishedState() {
        if sliderChangedCount == 0 {
            showPreviouslyVotedResults()
        }

        self.widgetFooterView.widgetLockVoteFooterView?.disableLockButton()

        self.delegate?.widgetStateCanComplete(widget: self, state: .finished)
    }

    override func willTransition(
        to newCollection: UITraitCollection,
        with coordinator: UIViewControllerTransitionCoordinator
    ) {
        coordinator.animate(alongsideTransition: { _ in }, completion: { [weak self] _ in
            guard let self = self else { return }
            self.imageSliderView.refreshAverageAnimationLeadingConstraint()
        })
        super.willTransition(to: newCollection, with: coordinator)
    }

    /// Shows average magnitude from already existing votes
    private func showPreviouslyVotedResults() {
        self.imageSliderView.averageVote = Float(model.averageMagnitude)
        self.imageSliderView.showResultsTrack()
        self.playAverageAnimation { }
        if let userVote = self.model.mostRecentVote {
            self.imageSliderView.moveSliderThumb(to: Float(userVote.magnitude))
        }
        self.widgetFooterView.widgetLockVoteFooterView?.disableLockButton()
    }
}

extension ImageSliderViewController: ImageSliderWidgetModelDelegate {
    func imageSliderWidgetModel(
        _ model: ImageSliderWidgetModel,
        averageMagnitudeDidChange averageMagnitude: Double
    ) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.imageSliderView.averageVote = Float(averageMagnitude)
        }
    }
}
