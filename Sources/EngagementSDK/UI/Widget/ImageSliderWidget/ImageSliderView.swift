//
//  ImageSliderView.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 5/13/19.
//

import Lottie
import UIKit
import LiveLikeCore
import LiveLikeSwift

class ImageSliderView: ThemeableView {

    private let mediaRepository: MediaRepository = MediaRepository.shared

    private let minimumSize: Float = 36
    private let maximumSize: Float = 54
    private var thumbImages = [UIImage]()
    private let thumbImageUrls: [URL]
    private let initialSliderValue: Float
    private let timerAnimationFilepath: String

    let coreWidgetView = CoreWidgetView()

    var titleView: ThemeableView = {
        let titleView = ThemeableView()
        titleView.translatesAutoresizingMaskIntoConstraints = false
        return titleView
    }()

    var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.textColor = .white
        label.numberOfLines = 0
        return label
    }()

    var bodyView: ThemeableView = {
        let background = ThemeableView()
        background.translatesAutoresizingMaskIntoConstraints = false
        return background
    }()

    lazy var timerView: LottieAnimationView = {
        let lottieView = LottieAnimationView(filePath: self.timerAnimationFilepath)
        lottieView.translatesAutoresizingMaskIntoConstraints = false
        lottieView.contentMode = .scaleAspectFit
        return lottieView
    }()

    var customSliderTrack: GradientView = {
        let gradient = GradientView(orientation: .horizontal)
        gradient.translatesAutoresizingMaskIntoConstraints = false
        gradient.isUserInteractionEnabled = false
        gradient.livelike_cornerRadius = 9
        return gradient
    }()

    var closeButton: UIButton = {
        let image = UIImage(named: "widget_close", in: EngagementResources.resourceBundle, compatibleWith: nil)
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.imageView?.contentMode = .scaleAspectFit
        button.setImage(image, for: .normal)
        button.isHidden = true
        return button
    }()

    private var titleLeadingConstraint: NSLayoutConstraint!
    private var titleTrailingConstraint: NSLayoutConstraint!
    private var titleTopConstraint: NSLayoutConstraint!
    private var titleBottomConstraint: NSLayoutConstraint!

    var titleMargins: UIEdgeInsets = .zero {
        didSet {
            titleLeadingConstraint.constant = titleMargins.left
            titleTrailingConstraint.constant = titleMargins.right
            titleTopConstraint.constant = titleMargins.top
            titleBottomConstraint.constant = titleMargins.bottom
        }
    }

    var resultsTrackLeft: Theme.Background = .fill(color: .red) {
        didSet {
            updateResultsTrackColors(
                resultsTrackLeft: resultsTrackRight,
                resultsTrackRight: resultsTrackLeft
            )
        }
    }

    var resultsTrackRight: Theme.Background = .fill(color: .blue) {
        didSet {
            updateResultsTrackColors(
                resultsTrackLeft: resultsTrackRight,
                resultsTrackRight: resultsTrackLeft
            )
        }
    }

    private func updateResultsTrackColors(resultsTrackLeft: Theme.Background, resultsTrackRight: Theme.Background) {
        var colors: [CGColor] = []
        switch resultsTrackLeft {
        case .fill(let color):
            colors.append(color.cgColor)
        case .gradient(let gradient):
            colors.append(contentsOf: gradient.colors.map { $0.cgColor })
        }

        switch resultsTrackRight {
        case .fill(let color):
            colors.append(color.cgColor)
        case .gradient(let gradient):
            colors.append(contentsOf: gradient.colors.map { $0.cgColor })
        }

        resultsSliderTrack.colors = colors
    }

    private var resultsTrackZeroWidthConstraint: NSLayoutConstraint!
    private var resultsTrackFinalWidthConstraint: NSLayoutConstraint!

    var resultsSliderTrack: MultiGradientView = {
        let view = MultiGradientView()
        view.startPoint = CGPoint(x: 0, y: 0.5)
        view.endPoint = CGPoint(x: 1, y: 0.5)
        view.livelike_cornerRadius = 9
        view.isHidden = true
        return view
    }()

    private var averageAnimationLeadingConstraint: NSLayoutConstraint?

    var avgIndicatorView: LottieAnimationView = {
        let lottie = LottieAnimationView(name: "image-slider-avg", bundle: EngagementResources.resourceBundle)
        lottie.translatesAutoresizingMaskIntoConstraints = false
        lottie.contentMode = .scaleAspectFit
        return lottie
    }()

    var sliderView: UISlider = {
        let slider = CustomSlider()
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.maximumTrackTintColor = .white
        slider.minimumTrackTintColor = .clear
        slider.minimumValue = 0
        slider.maximumValue = 1
        return slider
    }()

    var averageVote: Float = 0.5 {
        didSet {
            resultsSliderTrack.locations = [averageVote - 0.3, averageVote, averageVote + 0.3] as [NSNumber]
            refreshAverageAnimationLeadingConstraint()
        }
    }

    // MARK: - Init

    init(thumbImageUrls: [URL], initialSliderValue: Float, timerAnimationFilepath: String) {
        self.thumbImageUrls = thumbImageUrls
        self.initialSliderValue = initialSliderValue
        self.timerAnimationFilepath = timerAnimationFilepath
        super.init()
        self.configureLayout()
        self.sliderView.addTarget(self, action: #selector(self.sliderValueChanged), for: .valueChanged)
        self.sliderView.setValue(self.initialSliderValue, animated: true)

        mediaRepository.getImages(urls: thumbImageUrls) { [weak self] in
            guard let self = self else { return }
            switch $0 {
            case .success(let imageResults):
                self.thumbImages = imageResults.map { $0.image }
                self.configureSlider()
            case .failure(let error):
                log.error(error)
            }
        }
    }

    required init?(coder aDecoder: NSCoder) {
        assertionFailure("init(coder:) has not been implemented")
        return nil
    }

    // MARK: Public methods

    func showResultsTrack() {
        resultsSliderTrack.isHidden = false
        sliderView.minimumTrackTintColor = .clear
        sliderView.maximumTrackTintColor = .clear

        resultsTrackZeroWidthConstraint.priority = .defaultLow
        resultsTrackFinalWidthConstraint.priority = .defaultHigh
    }

    /// Move thumb to a position
    func moveSliderThumb(to position: Float) {
        sliderView.value = position
        sliderValueChanged()
    }

    // MARK: Private methods

    func refreshAverageAnimationLeadingConstraint() {
        averageAnimationLeadingConstraint?.isActive = false

        let averageXPosition = CGFloat(averageVote) * customSliderTrack.bounds.width
        averageAnimationLeadingConstraint = avgIndicatorView.centerXAnchor.constraint(
            equalTo: customSliderTrack.leadingAnchor,
            constant: averageXPosition
        )
        averageAnimationLeadingConstraint!.isActive = true
    }

    @objc private func sliderValueChanged() {
        setThumbsOnSlider()
    }

    private func setThumbsOnSlider() {
        if thumbImages.count > 0 {
            let newThumbSize = getThumbSize()
            let newThumbImage = getThumbImage().scaleToSize(newSize: newThumbSize)
            sliderView.setThumbImage(newThumbImage, for: .normal)
        }
    }

    private func configureLayout() {
        coreWidgetView.headerView = titleView
        coreWidgetView.contentView = bodyView
        addSubview(coreWidgetView)
        coreWidgetView.constraintsFill(to: self)

        titleView.addSubview(timerView)
        titleView.addSubview(titleLabel)
        titleView.addSubview(closeButton)

        bodyView.addSubview(customSliderTrack)
        bodyView.addSubview(resultsSliderTrack)
        bodyView.addSubview(sliderView)
        bodyView.addSubview(avgIndicatorView)

        resultsTrackZeroWidthConstraint = resultsSliderTrack.widthAnchor.constraint(equalToConstant: 0)
        resultsTrackFinalWidthConstraint = resultsSliderTrack.widthAnchor.constraint(equalTo: customSliderTrack.widthAnchor)

        titleLeadingConstraint = titleLabel.leadingAnchor.constraint(equalTo: titleView.leadingAnchor)
        titleTrailingConstraint = titleLabel.trailingAnchor.constraint(equalTo: timerView.leadingAnchor)
        titleTopConstraint = titleLabel.topAnchor.constraint(equalTo: titleView.topAnchor)
        titleBottomConstraint = titleLabel.bottomAnchor.constraint(equalTo: titleView.bottomAnchor)

        resultsTrackZeroWidthConstraint.priority = .defaultHigh
        resultsTrackFinalWidthConstraint.priority = .defaultLow

        // set slider thumb to be empty by default
        sliderView.setThumbImage(UIImage(), for: .normal)

        NSLayoutConstraint.activate([
            titleView.heightAnchor.constraint(greaterThanOrEqualToConstant: 35),

            titleLeadingConstraint,
            titleTrailingConstraint,
            titleTopConstraint,
            titleBottomConstraint,

            timerView.trailingAnchor.constraint(equalTo: titleView.trailingAnchor, constant: -10),
            timerView.heightAnchor.constraint(equalToConstant: 18),
            timerView.widthAnchor.constraint(equalToConstant: 18),
            timerView.centerYAnchor.constraint(equalTo: titleView.centerYAnchor),

            bodyView.heightAnchor.constraint(equalToConstant: 60),

            customSliderTrack.centerYAnchor.constraint(equalTo: bodyView.centerYAnchor),
            customSliderTrack.centerXAnchor.constraint(equalTo: bodyView.centerXAnchor),
            customSliderTrack.leadingAnchor.constraint(equalTo: bodyView.leadingAnchor, constant: 40),
            customSliderTrack.trailingAnchor.constraint(equalTo: bodyView.trailingAnchor, constant: -40),
            customSliderTrack.heightAnchor.constraint(equalToConstant: 18),

            resultsSliderTrack.centerYAnchor.constraint(equalTo: customSliderTrack.centerYAnchor),
            resultsSliderTrack.centerXAnchor.constraint(equalTo: customSliderTrack.centerXAnchor),
            resultsSliderTrack.heightAnchor.constraint(equalToConstant: 18),
            resultsTrackZeroWidthConstraint,
            resultsTrackFinalWidthConstraint,

            avgIndicatorView.centerYAnchor.constraint(equalTo: bodyView.centerYAnchor),
            avgIndicatorView.widthAnchor.constraint(equalToConstant: 20),
            avgIndicatorView.heightAnchor.constraint(equalToConstant: 60),

            sliderView.centerYAnchor.constraint(equalTo: bodyView.centerYAnchor),
            sliderView.centerXAnchor.constraint(equalTo: bodyView.centerXAnchor),
            sliderView.heightAnchor.constraint(equalToConstant: 18),
            sliderView.leadingAnchor.constraint(equalTo: bodyView.leadingAnchor, constant: 40),
            sliderView.trailingAnchor.constraint(equalTo: bodyView.trailingAnchor, constant: -40)
        ])

        refreshAverageAnimationLeadingConstraint()

        closeButton.constraintsFill(to: timerView)
    }

    private func configureSlider() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.setThumbsOnSlider()
        }
    }

    private func getThumbSize() -> CGSize {
        if thumbImages.count <= 2 {
            let length = CGFloat(Math.lerp(start: minimumSize, end: maximumSize, t: sliderView.value))
            return CGSize(width: length, height: length)
        } else {
            let length = CGFloat(vCurve(minimum: minimumSize, maximum: maximumSize, t: sliderView.value, tMax: 1))
            return CGSize(width: length, height: length)
        }
    }

    private func getThumbImage() -> UIImage {
        if thumbImages.count == 1 {
            return thumbImages[0]
        } else {
            let imageIndex: Int = Int(round(sliderView.value * Float(thumbImages.count - 1)))
            return thumbImages[imageIndex]
        }
    }

    private func vCurve(minimum: Float, maximum: Float, t: Float, tMax: Float) -> Float {
        if t < tMax / 2 {
            return Math.lerp(start: maximum, end: minimum, t: t)
        } else {
            return Math.lerp(start: minimum, end: maximum, t: t)
        }
    }
}

class CustomSlider: UISlider {
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        var newBounds = super.trackRect(forBounds: bounds)
        newBounds.size.height = 18
        newBounds.origin.y = 0
        return newBounds
    }
}

extension UIImage {
    /// Maintains original image's aspect ratio
    func scaleToSize(newSize: CGSize) -> UIImage {
        let oldWidth = size.width
        let oldHeight = size.height

        // Scale to max width or max height
        let scaleFactor = (oldWidth > oldHeight) ? newSize.width / oldWidth : newSize.height / oldHeight

        let newHeight = oldHeight * scaleFactor
        let newWidth = oldWidth * scaleFactor

        // Center new image vertically in rect
        let newY = (newSize.height - newHeight) / 2

        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        draw(in: CGRect(x: 0, y: newY, width: newWidth, height: newHeight))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}

class Math {
    static func lerp(start: Float, end: Float, t: Float) -> Float {
        return start + t * (end - start)
    }
}
