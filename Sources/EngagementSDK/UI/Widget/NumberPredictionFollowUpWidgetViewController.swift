//
//  NumberPredictionFollowUpWidgetViewController.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 10/13/21.
//

import UIKit
import LiveLikeCore
import LiveLikeSwift

class NumberPredictionFollowUpWidgetViewController: Widget {
    
    override var theme: Theme {
        didSet {
            applyTheme(theme)
        }
    }
    
    private let model: NumberPredictionFollowUpWidgetModel
    
    private let widgetView: NumberPredictionWidgetView = {
        let view = NumberPredictionWidgetView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var optionViews: [OptionView] = []
    
    override var currentState: WidgetState {
        willSet {
            previousState = self.currentState
        }
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.delegate?.widgetDidEnterState(widget: self, state: self.currentState)
                switch self.currentState {
                case .ready:
                    break
                case .interacting:
                    break
                case .results:
                    self.enterResultsState()
                case .finished:
                    self.enterFinishedState()
                }
            }
        }
    }
    
    init(model: NumberPredictionFollowUpWidgetModel) {
        self.model = model
        super.init(
            id: model.id,
            kind: model.kind,
            widgetTitle: model.question,
            createdAt: model.createdAt,
            publishedAt: model.publishedAt,
            interactionTimeInterval: model.interactionTimeInterval,
            options: []
        )
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(widgetView)
        NSLayoutConstraint.activate([
            widgetView.topAnchor.constraint(equalTo: view.topAnchor),
            widgetView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            widgetView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            widgetView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        widgetView.widgetTitleView.titleLabel.text = model.question
        widgetView.widgetTitleView.widgetTag.text = "EngagementSDK.widget.numberPrediction.tag".localized()
        
        model.options.enumerated().forEach { index, option in
            let view = OptionView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textLabel.text = option.text
            view.tag = index
            if let imageURL = option.imageURL {
                MediaRepository.shared.getImage(url: imageURL) { result in
                    switch result {
                    case .failure(let error):
                        log.error(error)
                    case .success(let imageResult):
                        DispatchQueue.main.async {
                            view.imageView.image = imageResult.image
                        }
                    }
                }
            }
            
            if let correctAnswer = option.correctNumber {
                view.correctAnswerLabel.text = correctAnswer.description
                
                if let userOptionVote = model.userVotes.first?.votes.first(where: { $0.optionID == option.id })?.number {
                    view.userAnswerLabel.text = userOptionVote.description
                    if correctAnswer == userOptionVote {
                        view.userAnswerLabel.isHidden = true
                        view.userAnswerBackground.isHidden = true
                    }
                    view.userAnswerBackground.applyContainerProperties(theme.widgets.numberPrediction.optionInputFieldIncorrectContainer)
                    view.userAnswerLabel.theme(theme.widgets.numberPrediction.optionInputFieldIncorrectText)
                } else {
                    view.userAnswerBackground.applyContainerProperties(theme.widgets.numberPrediction.optionInputFieldContainerDisabled)
                    view.userAnswerLabel.theme(theme.widgets.numberPrediction.optionInputFieldPlaceholder)
                    view.userAnswerLabel.text = "-"
                }
            }
            widgetView.optionsStackView.addArrangedSubview(view)
            optionViews.append(view)
        }
        
        applyTheme(theme)
        model.registerImpression { _ in }
    }
    
    func applyTheme(_ theme: Theme) {
        let numberPredictionTheme: Theme.NumberPredictionWidget = theme.widgets.numberPrediction
        widgetView.applyContainerProperties(numberPredictionTheme.main)
        widgetView.body.applyContainerProperties(numberPredictionTheme.body)
        widgetView.widgetTitleView.applyContainerProperties(numberPredictionTheme.header)
        widgetView.widgetTitleView.titleLabel.theme(numberPredictionTheme.title)
        widgetView.widgetTitleView.titleMargins = theme.choiceWidgetTitleMargins
        widgetView.widgetTitleView.widgetTagMargins = theme.widgetTagMargins
        widgetView.widgetTitleView.widgetTag.font = theme.widgetTagFont
        widgetView.widgetTitleView.widgetTag.textColor = theme.widgetTagTextColor
        self.optionViews.forEach { optionView in
            optionView.applyContainerProperties(numberPredictionTheme.optionContainer)
            optionView.textLabel.theme(numberPredictionTheme.optionText)
            optionView.correctAnswerBackground.applyContainerProperties(theme.widgets.numberPrediction.optionInputFieldCorrectContainer)
            optionView.correctAnswerLabel.theme(theme.widgets.numberPrediction.optionInputFieldCorrectText)
        }
    }
    
    private func enterResultsState() {
        self.delegate?.widgetStateCanComplete(widget: self, state: .results)
    }
    
    private func enterFinishedState() {
        self.delegate?.widgetStateCanComplete(widget: self, state: .finished)
    }
    
    override func moveToNextState() {
        switch self.currentState {
        case .ready:
            self.currentState = .results
        case .interacting:
            break
        case .results:
            self.currentState = .finished
        case .finished:
            break
        }
    }
    
    override func addTimer(seconds: TimeInterval, completion: @escaping (WidgetViewModel) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) { [weak self] in
            guard let self = self else { return }
            completion(self)
        }
    }
    
    override func addCloseButton(_ completion: @escaping (WidgetViewModel) -> Void) {
        self.widgetView.widgetTitleView.closeButtonAction = { [weak self] in
            guard let self = self else { return }
            completion(self)
        }
        self.widgetView.widgetTitleView.showCloseButton()
    }
}

private class OptionView: ThemeableView {
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let textLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    private let userAnswerContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let userAnswerBackground: ThemeableView = {
        let view = ThemeableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let userAnswerLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.adjustsFontSizeToFitWidth = true
        view.textAlignment = .center
        return view
    }()
    
    private let correctAnswerContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let correctAnswerBackground: ThemeableView = {
        let view = ThemeableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let correctAnswerLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.adjustsFontSizeToFitWidth = true
        view.textAlignment = .center
        return view
    }()
    
    override init() {
        super.init()
        
        userAnswerContainer.addSubview(userAnswerBackground)
        userAnswerContainer.addSubview(userAnswerLabel)
        
        correctAnswerContainer.addSubview(correctAnswerBackground)
        correctAnswerContainer.addSubview(correctAnswerLabel)
        
        let stackView = UIStackView(arrangedSubviews: [
            imageView,
            textLabel,
            userAnswerContainer,
            correctAnswerContainer
        ])
        stackView.axis = .horizontal
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.osAdaptive_setCustomSpacing(10, after: imageView)
        stackView.osAdaptive_setCustomSpacing(10, after: textLabel)
        stackView.osAdaptive_setCustomSpacing(10, after: userAnswerContainer)
        
        addSubview(stackView)
        
        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(equalToConstant: 64),
            imageView.heightAnchor.constraint(equalToConstant: 64),
            
            textLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 0),
            
            correctAnswerContainer.widthAnchor.constraint(equalToConstant: 50),
            userAnswerContainer.widthAnchor.constraint(equalToConstant: 50),
            
            userAnswerBackground.centerYAnchor.constraint(equalTo: centerYAnchor),
            userAnswerBackground.heightAnchor.constraint(equalToConstant: 36),
            userAnswerBackground.widthAnchor.constraint(equalToConstant: 53),
            userAnswerLabel.topAnchor.constraint(equalTo: userAnswerBackground.topAnchor),
            userAnswerLabel.leadingAnchor.constraint(equalTo: userAnswerBackground.leadingAnchor),
            userAnswerLabel.trailingAnchor.constraint(equalTo: userAnswerBackground.trailingAnchor),
            userAnswerLabel.bottomAnchor.constraint(equalTo: userAnswerBackground.bottomAnchor),
            
            correctAnswerBackground.centerYAnchor.constraint(equalTo: centerYAnchor),
            correctAnswerBackground.heightAnchor.constraint(equalToConstant: 36),
            correctAnswerBackground.widthAnchor.constraint(equalToConstant: 53),
            correctAnswerLabel.topAnchor.constraint(equalTo: correctAnswerBackground.topAnchor),
            correctAnswerLabel.leadingAnchor.constraint(equalTo: correctAnswerBackground.leadingAnchor),
            correctAnswerLabel.trailingAnchor.constraint(equalTo: correctAnswerBackground.trailingAnchor),
            correctAnswerLabel.bottomAnchor.constraint(equalTo: correctAnswerBackground.bottomAnchor),
            
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
