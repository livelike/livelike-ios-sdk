//
//  VideoAlertWidgetViewController.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 7/14/21.
//

import UIKit
import LiveLikeSwift
import LiveLikeCore

/// A Video Alert ViewController that connects with the VideoWidgetModel and initializes the UI with the data and calls methods on the model
class VideoAlertWidgetViewController: BaseVideoAlertWidgetViewController {

    public let model: VideoAlertWidgetModel

    override var currentState: WidgetState {
        willSet {
            previousState = self.currentState
        }
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.delegate?.widgetDidEnterState(widget: self, state: self.currentState)
                switch self.currentState {
                case .ready:
                    break
                case .interacting:
                    self.enterInteractingState()
                case .results:
                    break
                case .finished:
                    self.enterFinishedState()
                }
            }
        }
    }

    public override init(model: VideoAlertWidgetModel) {
        self.model = model
        super.init(model: model)
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public override func viewDidLoad() {
        super.viewDidLoad()

        setTitle(model.title)
        setText(model.text)
        setLink(url: model.linkURL, text: model.linkText)
        setVideoURL(model.videoURL)
        model.registerImpression()
        if model.linkURL != nil {
            model.markAsInteractive()
        }
    }

    public override func linkButtonSelected(_ sender: UIButton) {
        super.linkButtonSelected(sender)
        CoreAnalytics.shared.recordWidgetInteracted(
            widgetModel: .videoAlert(self.model)
        )
        model.registerLinkOpened()
    }

    public override func playVideo() {
        super.playVideo()
        model.registerPlayStarted()
    }

    override func moveToNextState() {
        switch self.currentState {
        case .ready:
            self.currentState = .interacting
        case .interacting:
            self.currentState = .finished
        case .results:
            break
        case .finished:
            break
        }
    }

    override func addTimer(seconds: TimeInterval, completion: @escaping (WidgetViewModel) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) { [weak self] in
            guard let self = self else { return }
            completion(self)
        }
    }

    private func enterInteractingState() {
        self.delegate?.widgetStateCanComplete(widget: self, state: .interacting)
    }

    private func enterFinishedState() {
        self.delegate?.widgetStateCanComplete(widget: self, state: .finished)
    }
}
