//
//  BaseVideoAlertWidgetViewController.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 7/12/21.
//

import UIKit
import AVKit
import LiveLikeCore

/// A Video Alert ViewController that applies the core functionality of the Video Alert Widget to the ViewAlertWidgetView
/// Such as button handling and video playback
class BaseVideoAlertWidgetViewController: Widget {

    override var theme: Theme {
        didSet {
            self.applyTheme(theme.widgets.videoAlert)
        }
    }

    public var avPlayerViewController: AVPlayerViewController = {
        let vc = AVPlayerViewController()
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        vc.showsPlaybackControls = false
        vc.videoGravity = .resizeAspectFill
        return vc
    }()

    /// A tap gesture recognizer over the video to toggle play/pause
    public var playerTapGestureRecognizer: UITapGestureRecognizer = {
        let tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer.numberOfTapsRequired = 1
        return tapGestureRecognizer
    }()

    public let videoWidgetView: VideoAlertWidgetView = {
        let view = VideoAlertWidgetView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    /// The url to open when linkButton is selected
    public var linkURL: URL?

    private var playerItemObservation: NSKeyValueObservation?

    public override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(videoWidgetView)
        addChild(avPlayerViewController)
        avPlayerViewController.didMove(toParent: self)
        videoWidgetView.videoContainer.addSubview(avPlayerViewController.view)

        NSLayoutConstraint.activate([
            videoWidgetView.topAnchor.constraint(equalTo: view.topAnchor),
            videoWidgetView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            videoWidgetView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            videoWidgetView.bottomAnchor.constraint(equalTo: view.bottomAnchor),

            avPlayerViewController.view.topAnchor.constraint(equalTo: videoWidgetView.videoContainer.topAnchor),
            avPlayerViewController.view.leadingAnchor.constraint(equalTo: videoWidgetView.videoContainer.leadingAnchor),
            avPlayerViewController.view.trailingAnchor.constraint(equalTo: videoWidgetView.videoContainer.trailingAnchor),
            avPlayerViewController.view.bottomAnchor.constraint(equalTo: videoWidgetView.videoContainer.bottomAnchor)
        ])

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(playerDidFinishPlaying),
            name: .AVPlayerItemDidPlayToEndTime,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(appWillEnterForegroundNotification),
            name: UIApplication.willEnterForegroundNotification,
            object: nil
        )

        videoWidgetView.muteToggle.addTarget(self, action: #selector(muteToggleSelected(_:)), for: .touchUpInside)
        videoWidgetView.playButton.addTarget(self, action: #selector(playButtonSelected(_:)), for: .touchUpInside)
        playerTapGestureRecognizer.addTarget(self, action: #selector(playerTapGestureRecognized))
        videoWidgetView.linkButton.addTarget(self, action: #selector(linkButtonSelected(_:)), for: .touchUpInside)
        avPlayerViewController.view.addGestureRecognizer(playerTapGestureRecognizer)

        videoWidgetView.muteToggle.isHidden = true
        videoWidgetView.playButton.isHidden = true
        videoWidgetView.activityIndicator.startAnimating()

        applyTheme(theme.widgets.videoAlert)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)    
        self.pauseVideo()
    }

    @objc func muteToggleSelected(_ sender: UIButton) {
        sender.isSelected.toggle()
        avPlayerViewController.player?.isMuted = sender.isSelected
    }

    @objc func playButtonSelected(_ sender: UIButton) {
        self.playVideo()
    }

    @objc func playerTapGestureRecognized() {
        if self.avPlayerViewController.player?.timeControlStatus == .playing {
            self.pauseVideo()
        } else {
            self.playVideo()
        }
    }

    @objc func linkButtonSelected(_ sender: UIButton) {
        if let linkURL = linkURL {
            UIApplication.shared.open(linkURL, options: [:], completionHandler: nil)
        }
    }

    @objc func playerDidFinishPlaying(_ sender: Notification) {
        guard
            let senderPlayerItem = sender.object as? AVPlayerItem,
            let currentPlayerItem = self.avPlayerViewController.player?.currentItem,
            senderPlayerItem == currentPlayerItem
        else {
            return
        }
        self.pauseVideo()
        self.avPlayerViewController.player?.currentItem?.seek(
            to: CMTimeMakeWithSeconds(0, preferredTimescale: 1),
            toleranceBefore: .zero,
            toleranceAfter: .zero
        )
    }

    @objc func appWillEnterForegroundNotification(_ sender: Notification) {
        self.pauseVideo()
    }

    func playVideo() {
        if self.avPlayerViewController.player?.currentItem?.status == .failed {
            self.showBrokenMediaError(true)
            self.videoWidgetView.playButton.isHidden = true
        } else {
            self.avPlayerViewController.player?.play()
            self.videoWidgetView.playButton.isHidden = true
            videoWidgetView.muteToggle.isHidden = false
        }
    }

    func pauseVideo() {
        self.avPlayerViewController.player?.pause()
        self.videoWidgetView.playButton.isHidden = false
        videoWidgetView.muteToggle.isHidden = true
    }

    func setTitle(_ text: String?) {
        videoWidgetView.titleLabel.text = text
        videoWidgetView.header.isHidden = text == nil
    }

    func setText(_ text: String?) {
        videoWidgetView.bodyLabel.text = text
        videoWidgetView.body.isHidden = text == nil
    }

    func setLink(url: URL?, text: String?) {
        videoWidgetView.linkButton.setTitle(text, for: .normal)
        videoWidgetView.footer.isHidden = text == nil
        videoWidgetView.linkImageView.isHidden = text == nil
        linkURL = url
    }

    func setVideoURL(_ url: URL) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let avPlayerItem = AVPlayerItem(url: url)

            self.playerItemObservation?.invalidate()
            self.playerItemObservation = avPlayerItem.observe(\.status, options: .new) { [weak self] playerItem, change in
                guard let self = self else { return }
                self.videoWidgetView.activityIndicator.stopAnimating()
                if playerItem.status == .readyToPlay {
                    self.videoWidgetView.playButton.isHidden = false
                } else if playerItem.status == .failed {
                    log.error("Failed to player the video \(url.absoluteString) with error: \(playerItem.error?.localizedDescription ?? "unknown")")
                    self.showBrokenMediaError(true)
                }
            }
            self.avPlayerViewController.player = AVPlayer(playerItem: avPlayerItem)
        }
    }

    func showBrokenMediaError(_ show: Bool) {
        self.videoWidgetView.brokenMediaContainer.isHidden = !show
    }

    private func applyTheme(_ theme: Theme.VideoAlertWidget) {
        videoWidgetView.header.applyContainerProperties(theme.header)
        videoWidgetView.body.applyContainerProperties(theme.body)
        videoWidgetView.footer.applyContainerProperties(theme.footer)
        videoWidgetView.applyContainerProperties(theme.main)
        videoWidgetView.titleLabel.theme(theme.title)
        videoWidgetView.bodyLabel.theme(theme.description)
        videoWidgetView.linkButton.titleLabel?.font = theme.link.font
        videoWidgetView.linkButton.setTitleColor(theme.link.color, for: .normal)
    }
}
