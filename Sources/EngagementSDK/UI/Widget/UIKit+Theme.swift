//
//  UIKit+Theme.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 7/29/21.
//

import UIKit

extension UILabel {
    func theme(_ theme: Theme.Text) {
        self.font = theme.font
        self.textColor = theme.color
    }
}

extension UITextView {
    func theme(_ text: Theme.Text) {
        font = text.font
        textColor = text.color
    }
}
