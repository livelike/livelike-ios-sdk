//
//  NumberPredictionWidgetViewController.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 9/24/21.
//

import UIKit
import LiveLikeCore
import LiveLikeSwift

class NumberPredictionWidgetViewController: Widget {
    
    override var theme: Theme {
        didSet {
            applyTheme(theme)
        }
    }
    
    let widgetView: NumberPredictionWidgetView = {
        let view = NumberPredictionWidgetView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override var currentState: WidgetState {
        willSet {
            previousState = self.currentState
        }
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.delegate?.widgetDidEnterState(widget: self, state: self.currentState)
                switch self.currentState {
                case .ready:
                    break
                case .interacting:
                    self.enterInteractingState()
                case .results:
                    self.enterResultsState()
                case .finished:
                    self.enterFinishedState()
                }
            }
        }
    }
    
    private var optionViews: [NumberPredictionWidgetView.TextOptionView] = []
    
    private let model: NumberPredictionWidgetModel
    
    init(model: NumberPredictionWidgetModel) {
        self.model = model
        super.init(
            id: model.id,
            kind: model.kind,
            widgetTitle: model.question,
            createdAt: model.createdAt,
            publishedAt: model.publishedAt,
            interactionTimeInterval: model.interactionTimeInterval,
            options: []
        )
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(widgetView)
        NSLayoutConstraint.activate([
            widgetView.topAnchor.constraint(equalTo: view.topAnchor),
            widgetView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            widgetView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            widgetView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
        
        widgetView.widgetTitleView.titleLabel.text = model.question
        widgetView.widgetTitleView.widgetTag.text = "EngagementSDK.widget.numberPrediction.tag".localized()
        model.options.enumerated().forEach { index, option in
            let view = NumberPredictionWidgetView.TextOptionView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textLabel.text = option.text
            view.tag = index
            if let imageURL = option.imageURL {
                MediaRepository.shared.getImage(url: imageURL) { result in
                    switch result {
                    case .failure(let error):
                        log.error(error)
                    case .success(let imageResult):
                        DispatchQueue.main.async {
                            view.imageView.image = imageResult.image
                        }
                    }
                }
            }
            view.numberTextField.attributedPlaceholder = NSAttributedString(
                string: "-",
                attributes: [.foregroundColor: theme.widgets.numberPrediction.optionInputFieldPlaceholder.color]
            )
            if let userOptionVote = model.userVotes.first?.votes.first(where: { $0.optionID == option.id })?.number {
                view.numberTextField.text = userOptionVote.description
            }
            view.numberTextField.addTarget(
                self,
                action: #selector(textFieldDidChange(_:)),
                for: .editingChanged
            )
            view.numberTextField.delegate = self
            widgetView.optionsStackView.addArrangedSubview(view)
            optionViews.append(view)
        }
        
        widgetView.widgetFooterView.configureLockVoteBtn { [weak self] lockVoteButton in
            guard let self = self else { return }
            self.lockVoteButtonSelected()
        }
        widgetView.widgetFooterView.widgetLockVoteFooterView?.disableLockButton()
        widgetView.widgetFooterView.widgetLockVoteFooterView?.confirmationLabel.isHidden = true
        widgetView.widgetFooterView.widgetLockVoteFooterView?.confirmationLabel.text =
            "EngagementSDK.widget.numberPrediction.predictConfirmationLabel.text".localized()
        widgetView.widgetFooterView.widgetLockVoteFooterView?.lockButton.setTitle(
            "EngagementSDK.widget.numberPrediction.predictButton.title".localized(),
            for: .normal
        )
        
        widgetView.widgetFooterView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        widgetView.mainStackView.addArrangedSubview(widgetView.widgetFooterView)
        
        if model.userVotes.first != nil {
            widgetView.widgetFooterView.widgetLockVoteFooterView?.confirmationLabel.isHidden = false
            optionViews.forEach { optionView in
                setOptionAsDisabled(optionView)
            }
        } else if model.isFollowUpPublished {
            optionViews.forEach { optionView in
                setOptionAsDisabled(optionView)
            }
        } else {
            model.markAsInteractive()
            optionViews.forEach { optionView in
                setOptionAsEnabled(optionView)
            }
        }
        
        if isInteractivityExpired() {
            self.disableInteractions()
        }

        self.beginInteractiveUntilTimer { [weak self] in
            guard let self = self else { return }
            self.disableInteractions()
        }
        
        applyTheme(theme)
        model.registerImpression { _ in }
        model.delegate = self
    }
    
    override func moveToNextState() {
        switch self.currentState {
        case .ready:
            self.currentState = .interacting
        case .interacting:
            self.currentState = .results
        case .results:
            self.currentState = .finished
        case .finished:
            break
        }
    }
    
    override func addTimer(seconds: TimeInterval, completion: @escaping (WidgetViewModel) -> Void) {
        self.widgetView.widgetTitleView.beginTimer(
            duration: seconds,
            animationFilepath: self.theme.lottieFilepaths.timer
        ) { [weak self] in
            guard let self = self else { return }
            completion(self)
        }
    }
    
    override func addCloseButton(_ completion: @escaping (WidgetViewModel) -> Void) {
        self.widgetView.widgetTitleView.closeButtonAction = { [weak self] in
            guard let self = self else { return }
            completion(self)
        }
        self.widgetView.widgetTitleView.showCloseButton()
    }
    
    private func lockVoteButtonSelected() {
        let submissions: [NumberPredictionVoteSubmission] = optionViews.enumerated().compactMap { index, optionView in
            guard
                let numberText = optionView.numberTextField.text,
                let number = Int(numberText)
            else {
                return nil
            }
            return NumberPredictionVoteSubmission(
                optionID: model.options[index].id,
                number: number
            )
        }
        // validate that all options are answered
        guard submissions.count == model.options.count else { return }
        
        self.widgetView.widgetFooterView.widgetLockVoteFooterView?.disableLockButton()
        self.disableInteractions()
        self.model.lockVotes(submissions: submissions) { [weak self] result in
            guard let self = self else { return }
            DispatchQueue.main.async {
                guard let lockVoteFooterView = self.widgetView.widgetFooterView.widgetLockVoteFooterView else { return }
                switch result {
                case .failure:
                    lockVoteFooterView.enableLockbutton()
                    self.optionViews.forEach { optionView in
                        self.setOptionAsEnabled(optionView)
                    }
                case .success:
                    lockVoteFooterView.confirmationLabel.isHidden = false
                }
            }
        }
    }
    
    private func enterInteractingState() {
        self.delegate?.widgetStateCanComplete(widget: self, state: .interacting)
    }
    
    private func enterResultsState() {
        self.disableInteractions()
        if self.model.userVotes.count == 0 {
            // attempt to automatically submit vote if user hasn't answered
            self.lockVoteButtonSelected()
        }
        self.delegate?.widgetStateCanComplete(widget: self, state: .results)
    }
    
    private func enterFinishedState() {
        self.delegate?.widgetStateCanComplete(widget: self, state: .finished)
    }
    
    private func disableInteractions() {
        optionViews.forEach {
            self.setOptionAsDisabled($0)
        }
    }
    
    private func isInteractivityExpired() -> Bool {
        guard let interactiveUntil = self.model.interactiveUntil else {
            return false
        }

        return interactiveUntil < Date()
    }
    
    private func setOptionAsDisabled(_ optionView: NumberPredictionWidgetView.TextOptionView) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            optionView.numberTextField.isEnabled = false
            optionView.numberTextFieldBackground.applyContainerProperties(
                self.theme.widgets.numberPrediction.optionInputFieldContainerDisabled
            )
            optionView.numberTextField.font = self.theme.widgets.numberPrediction.optionInputFieldTextDisabled.font
            optionView.numberTextField.textColor = self.theme.widgets.numberPrediction.optionInputFieldTextDisabled.color
        }
    }
    
    private func setOptionAsEnabled(_ optionView: NumberPredictionWidgetView.TextOptionView) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            optionView.numberTextField.isEnabled = true
            optionView.numberTextFieldBackground.applyContainerProperties(
                self.theme.widgets.numberPrediction.optionInputFieldContainerEnabled
            )
            optionView.numberTextField.font = self.theme.widgets.numberPrediction.optionInputFieldTextEnabled.font
            optionView.numberTextField.textColor = self.theme.widgets.numberPrediction.optionInputFieldTextEnabled.color
        }
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        let submissions: [NumberPredictionVoteSubmission] = optionViews.enumerated().compactMap { index, optionView in
            guard
                let numberText = optionView.numberTextField.text,
                let number = Int(numberText)
            else {
                return nil
            }
            return NumberPredictionVoteSubmission(
                optionID: model.options[index].id,
                number: number
            )
        }
        if submissions.count == model.options.count {
            self.widgetView.widgetFooterView.widgetLockVoteFooterView?.enableLockbutton()
        } else {
            self.widgetView.widgetFooterView.widgetLockVoteFooterView?.disableLockButton()
        }
    }
    
    func applyTheme(_ theme: Theme) {
        let numberPredictionTheme = theme.widgets.numberPrediction
        
        widgetView.applyContainerProperties(numberPredictionTheme.main)
        widgetView.body.applyContainerProperties(numberPredictionTheme.body)
        widgetView.widgetFooterView.applyTheme(theme, submitButton: numberPredictionTheme.submitButton)
        widgetView.widgetTitleView.applyContainerProperties(numberPredictionTheme.header)
        widgetView.widgetTitleView.titleLabel.theme(numberPredictionTheme.title)
        widgetView.widgetTitleView.titleMargins = theme.choiceWidgetTitleMargins
        widgetView.widgetTitleView.widgetTagMargins = theme.widgetTagMargins
        widgetView.widgetTitleView.widgetTag.font = theme.widgetTagFont
        widgetView.widgetTitleView.widgetTag.textColor = theme.widgetTagTextColor
        self.optionViews.forEach { optionView in
            optionView.applyContainerProperties(numberPredictionTheme.optionContainer)
            optionView.textLabel.theme(numberPredictionTheme.optionText)
        }
    }
}

extension NumberPredictionWidgetViewController: NumberPredictionWidgetModelDelegate {
    func numberPredictionModel(
        _ model: NumberPredictionWidgetModel,
        didReceiveFollowUp id: String,
        kind: WidgetKind
    ) {
        // End interacting phase if we receive follow up
        self.delegate?.widgetStateCanComplete(widget: self, state: .results)
    }
}

extension NumberPredictionWidgetViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" { return true }
        guard Int(string) != nil else { return false }
        return true
    }
}
