//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//

import UIKit
import LiveLikeCore
import LiveLikeSwift

class PollWidgetViewController: Widget {

    // MARK: Internal Properties
    override var theme: Theme {
        didSet {
            self.applyTheme(theme)
            
            self.widgetView.options.forEach { optionView in
                switch optionView.optionThemeStyle {
                case .selected:
                    self.applyOptionTheme(
                        optionView: optionView,
                        optionTheme: theme.widgets.poll.selectedOption
                    )
                case .unselected:
                    self.applyOptionTheme(
                        optionView: optionView,
                        optionTheme: theme.widgets.poll.unselectedOption
                    )
                default:
                    break
                }
            }
        }
    }
    
    override var currentState: WidgetState {
        willSet {
            previousState = self.currentState
        }
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.delegate?.widgetDidEnterState(widget: self, state: self.currentState)
                switch self.currentState {
                case .ready:
                    break
                case .interacting:
                    self.enterInteractingState()
                case .results:
                    self.enterResultsState()
                case .finished:
                    self.enterFinishedState()
                }
            }
        }
    }

    // MARK: Private Properties
    
    private var firstTapTime: Date?
    private let optionType: ChoiceWidgetOptionButton.Type
    private lazy var widgetView: ChoiceWidget = {
        let view = VerticalChoiceWidget(optionType: self.optionType)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let mediaRepository: MediaRepository = MediaRepository.shared
    private let model: PollWidgetModel
    private let viewModel: PollWidgetViewModel
    private var timer: Timer?
    
    private let widgetFooterView: WidgetFooterView = {
        let view = WidgetFooterView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(model: PollWidgetModel) {
        self.model = model
        self.viewModel = PollWidgetViewModel(model: model)
        self.optionType = model.containsImages ? WideTextImageChoice.self : TextChoiceWidgetOptionButton.self
    
        super.init(model: model)
    
        self.viewModel.voteCountsDidChange = { [weak self] in
            guard let self = self else { return }
            DispatchQueue.main.async {
                zip(self.widgetView.options, self.model.options)
                    .filter { view, data in view.id == data.id }
                    .forEach { view, data in
                        let votePercentage: CGFloat = self.viewModel.getVoteCountTotal() > 0
                            ? CGFloat(self.viewModel.getVoteCount(optionID: data.id)) / CGFloat(self.viewModel.getVoteCountTotal())
                            : 0
                        view.setProgress(votePercentage)
                    }
            }
        }
    
        if let sponsor = model.sponsors.first {
            widgetFooterView.configureSponsor(sponsor)
        }
    
        widgetView.coreWidgetView.footerView = widgetFooterView
    }
    
    required init?(coder aDecoder: NSCoder) {
        assertionFailure("init(coder:) has not been implemented")
        return nil
    }
    
    // MARK: Lifecycle Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        widgetView.isUserInteractionEnabled = false
        view.addSubview(widgetView)
        widgetView.constraintsFill(to: view)
        
        widgetFooterView.heightAnchor.constraint(equalToConstant: widgetFooterView.height).isActive = true

        widgetView.titleView.closeButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
        widgetView.titleView.titleLabel.text = widgetTitle
        widgetView.titleView.widgetTag.text = "EngagementSDK.widget.poll.tag".localized()
        self.optionsArray?.forEach { optionData in
            self.widgetView.addOption(withID: optionData.id) { optionView in
                optionView.text = optionData.text
                if let imageUrl = optionData.imageUrl {
                    mediaRepository.getImage(url: imageUrl) { result in
                        switch result {
                        case .success(let imageResult):
                            optionView.image = imageResult.image
                        case .failure(let error):
                            log.error(error)
                        }
                    }
                }
                optionView.delegate = self
                self.applyOptionTheme(
                    optionView: optionView,
                    optionTheme: theme.widgets.poll.unselectedOption
                )
                optionView.isProgressLabelHidden = true
            }
        }
        applyTheme(theme)
        if model.mostRecentVote != nil {
            DispatchQueue.main.async {
                self.showResultsFromWidgetOptions()
            }
        }
        
        if model.isInteractivityExpired() {
            self.disableInteractions()
            DispatchQueue.main.async {
                self.showResultsFromWidgetOptions()
            }
        }
        
        self.beginInteractiveUntilTimer { [weak self] in
            guard let self = self else { return }
            self.disableInteractions()
        }
        
        self.model.registerImpression()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    // MARK: UI Functionality

    private var closeButtonCompletion: ((WidgetViewModel) -> Void)?
    
    override func addCloseButton(_ completion: @escaping (WidgetViewModel) -> Void) {
        self.widgetView.titleView.showCloseButton()
        closeButtonCompletion = completion
    }
    
    @objc private func closeButtonPressed() {
        closeButtonCompletion?(self)
    }
    
    override func addTimer(seconds: TimeInterval, completion: @escaping (WidgetViewModel) -> Void) {
        self.widgetView.titleView.beginTimer(
            duration: seconds,
            animationFilepath: theme.lottieFilepaths.timer
        ) { [weak self] in
            guard let self = self else { return }
            completion(self)
        }
    }
    
    private func applyTheme(_ theme: Theme) {
        widgetView.applyContainerProperties(theme.widgets.poll.main)
        widgetView.titleView.titleMargins = theme.choiceWidgetTitleMargins
        widgetView.titleView.widgetTagMargins = theme.widgetTagMargins
        widgetView.titleView.widgetTag.font = theme.widgetTagFont
        widgetView.titleView.widgetTag.textColor = theme.widgetTagTextColor
        widgetView.bodyBackground = theme.widgets.poll.body.background
        widgetView.optionSpacing = theme.interOptionSpacing
        widgetView.headerBodySpacing = theme.titleBodySpacing
        widgetView.titleView.titleLabel.textColor = theme.widgets.poll.title.color
        widgetView.titleView.titleLabel.font = theme.widgets.poll.title.font
        widgetView.titleView.applyContainerProperties(theme.widgets.poll.header)
        
        widgetFooterView.applyTheme(theme, submitButton: nil)
    }

    private func applyOptionTheme(
        optionView: ChoiceWidgetOption,
        optionTheme: Theme.ChoiceWidget.Option
    ) {
        optionView.descriptionTextColor = optionTheme.description.color
        optionView.descriptionFont = optionTheme.description.font
        optionView.barBackground = optionTheme.progressBar.background
        optionView.barCornerRadii = optionTheme.progressBar.cornerRadii
        optionView.percentageFont = optionTheme.percentage.font
        optionView.percentageTextColor = optionTheme.percentage.color
        optionView.applyContainerProperties(optionTheme.container)
        optionView.imageVerticalOffset = optionTheme.imageVerticalOffset
        optionView.imageHorizontalOffset = optionTheme.imageHorizontalOffset
        optionView.imageContentMode = optionTheme.imageContentMode
    }
    
    // MARK: Results

    /// Shows results from `WidgetOption` data that already exists
    private func showResultsFromWidgetOptions() {
        zip(self.widgetView.options, self.model.options)
            .filter { view, data in view.id == data.id }
            .forEach { view, data in
                if let vote = model.mostRecentVote, data.id == vote.optionID {
                    view.optionThemeStyle = .selected
                    self.applyOptionTheme(
                        optionView: view,
                        optionTheme: theme.widgets.poll.selectedOption
                    )
                } else {
                    view.optionThemeStyle = .unselected
                    self.applyOptionTheme(
                        optionView: view,
                        optionTheme: theme.widgets.poll.unselectedOption
                    )
                }
                let votePercentage: CGFloat = self.viewModel.getVoteCountTotal() > 0
                    ? CGFloat(self.viewModel.getVoteCount(optionID: data.id)) / CGFloat(self.viewModel.getVoteCountTotal())
                    : 0
                view.setProgress(votePercentage)
                view.isProgressLabelHidden = false
            }
    }

    // MARK: Widget States
    override func moveToNextState() {
        switch self.currentState {
        case .ready:
            self.currentState = .interacting
        case .interacting:
            self.currentState = .results
        case .results:
            self.currentState = .finished
        case .finished:
            break
        }
    }
    
    private func enterInteractingState() {
        widgetView.isUserInteractionEnabled = true
        self.interactableState = .openToInteraction
        self.model.markAsInteractive()
        self.widgetView.options.forEach {
            $0.isUserInteractionEnabled = true
        }
        self.delegate?.widgetStateCanComplete(widget: self, state: .interacting)
    }
    
    private func enterResultsState() {
        self.disableInteractions()
        self.interactableState = .closedToInteraction
        if self.firstTapTime != nil, self.timeOfLastInteraction != nil {
            CoreAnalytics.shared.recordWidgetInteracted(
                widgetModel: .poll(self.model)
            )
        }
        self.delegate?.widgetStateCanComplete(widget: self, state: .results)
    }
    
    private func enterFinishedState() {
        self.disableInteractions()
        self.delegate?.widgetStateCanComplete(widget: self, state: .finished)
    }
    
    private func disableInteractions() {
        widgetView.isUserInteractionEnabled = false
        self.widgetView.options.forEach {
            $0.isUserInteractionEnabled = false
        }
    }
}

extension PollWidgetViewController: ChoiceWidgetOptionDelegate {
    func wasSelected(_ option: ChoiceWidgetOption) {
        self.userDidInteract = true
        
        // unselect all
        widgetView.options.forEach { optionView in
            optionView.isProgressLabelHidden = false
            optionView.optionThemeStyle = .unselected
            self.applyOptionTheme(
                optionView: optionView,
                optionTheme: theme.widgets.poll.unselectedOption
            )
        }
        
        // select selection
        option.optionThemeStyle = .selected
        self.applyOptionTheme(
            optionView: option,
            optionTheme: theme.widgets.poll.selectedOption
        )
        
        let now = Date()
        if firstTapTime == nil {
            firstTapTime = now
        }
        timeOfLastInteraction = now
        interactionCount += 1
        viewModel.submitVote(optionID: option.id)
        let selectedVote = ChoiceWidgetVote(from: option)
        self.delegate?.userDidSubmitVote(self, selectedVote: selectedVote)
        self.delegate?.userDidInteract(self)
    }
    
    func wasDeselected(_ option: ChoiceWidgetOption) {}
}
