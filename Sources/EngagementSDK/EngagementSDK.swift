//
//  EngagementSDK.swift
//  
//
//  Created by Jelzon Monzon on 1/3/23.
//

import UIKit
import LiveLikeSwift
import LiveLikeCore

/**
  The entry point for all interaction with the EngagementSDK.

 - Important: Concurrent instances of the EngagementSDK is not supported; Only one instance should exist at any time.
 */
public class EngagementSDK: LiveLike {

    public static let version: String = livelikeversion

    public override init(config: LiveLikeConfig) {
        LLCore.networking.userAgent += "[EngagementSDK/\(UIDevice.modelName)/\(UIDevice.current.systemVersion)]"
        super.init(config: config)
    }

    /// Loads a `Widget` using  the `DefaultWidgetFactory` by an id and kind
    public func getWidget(
        id: String,
        kind: WidgetKind,
        completion: @escaping (Result<Widget, Error>) -> Void
    ) {
        self.getWidgetModel(
            id: id,
            kind: kind
        ) { result in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let widgetModel):
                guard let widget = DefaultWidgetFactory.makeWidget(from: widgetModel) else {
                    return completion(.failure(DefaultWidgetFactory.Error.failedToBuildUI))
                }
                completion(.success(widget))
            }
        }
    }

    /// Loads a `Widget` using  the `DefaultWidgetFactory` by a widget json
    public func createWidget(
        withJSONObject jsonObject: Any,
        completion: @escaping (Result<Widget, Error>) -> Void
    ) {
        self.createWidgetModel(withJSONObject: jsonObject) { result in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let widgetModel):
                guard let widget = DefaultWidgetFactory.makeWidget(from: widgetModel) else {
                    return completion(.failure(DefaultWidgetFactory.Error.failedToBuildUI))
                }
                completion(.success(widget))
            }

        }
    }
}
