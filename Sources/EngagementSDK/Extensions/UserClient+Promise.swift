//
//  UserClient+Promise.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 12/1/22.
//

import Foundation
import LiveLikeCore

extension UserClient {
    func getBlockedProfileIDListComplete() -> Promise<[String]> {
        return Promise { fulfill, reject in
            self.getBlockedProfileIDListComplete { result in
                switch result {
                case .failure(let error):
                    reject(error)
                case .success(let blockedUsers):
                    fulfill(blockedUsers)
                }
            }
        }
    }
}
