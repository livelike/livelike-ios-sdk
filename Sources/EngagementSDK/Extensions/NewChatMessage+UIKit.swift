//
//  NewChatMessage+UIKit.swift
//  
//
//  Created by Jelzon Monzon on 11/15/22.
//

import UIKit
import LiveLikeCore

extension NewChatMessage {
    /// The suggested size of the image as a `CGSize`
    public var imageSize: CGSize? {
        guard let imageHeight = self.imageHeight else {
            return nil
        }
        guard let imageWidth = self.imageWidth else {
            return nil
        }
        return CGSize(width: imageWidth, height: imageHeight)

    }

    /// Init method for a chat message containing an image
    /// - Parameters:
    ///   - imageURL: a `URL` to the image
    ///   - imageSize: a preferable size of the image in the chat message
    public init(
        imageURL: URL,
        imageSize: CGSize,
        messageMetadata: MessageMetadata? = nil
    ) {
        self.init(
            imageURL: imageURL,
            imageHeight: Int(imageSize.height),
            imageWidth: Int(imageSize.width),
            messageMetadata: messageMetadata
        )
    }

    /// Init method for a chat message containing an image
    /// - Parameters:
    ///   - imageData: `UIImage` object represented in `Data` type
    ///   - imageSize: a preferable size of the image in the chat message
    public init(
        imageData: Data,
        imageSize: CGSize,
        messageMetadata: MessageMetadata? = nil
    ) {
        let imageName = "\(Int64(NSDate().timeIntervalSince1970 * 1000)).gif"
        let fileURL = "mock:\(imageName)"
        Cache.shared.set(object: imageData, key: fileURL, completion: nil)
        self.init(
            imageURL: URL(string: fileURL),
            imageHeight: Int(imageSize.height),
            imageWidth: Int(imageSize.width),
            messageMetadata: messageMetadata
        )
    }
}
