//
//  Sponsor+UIKit.swift
//  
//
//  Created by Jelzon Monzon on 11/14/22.
//

import UIKit
import LiveLikeSwift

extension Sponsor {
    public var brandColor: UIColor? {
        guard let brandColorHex = self.brandColorHex else { return nil }
        return UIColor(hexaRGBA: brandColorHex)
    }
}
