//
//  ChatMessage+UIKit.swift
//  
//
//  Created by Jelzon Monzon on 11/15/22.
//

import UIKit
import LiveLikeSwift

extension ChatMessage {
    /// A suggested image size of the image in the chat message as a CGSize
    public var imageSize: CGSize? {
        guard let imageHeight = self.imageHeight else {
            return nil
        }
        guard let imageWidth = self.imageWidth else {
            return nil
        }
        return CGSize(width: imageWidth, height: imageHeight)

    }
}
