//
//  ChatSession+Promise.swift
//  
//
//  Created by Jelzon Monzon on 11/21/22.
//

import Foundation
import LiveLikeCore
import LiveLikeSwift

extension ChatSession {
    func getReactions() -> Promise<[Reaction]> {
        return Promise { fulfill, reject in
            self.getReactions { result in
                switch result {
                case .failure(let error):
                    return reject(error)
                case .success(let reactions):
                    return fulfill(reactions)
                }
            }
        }
    }
}
