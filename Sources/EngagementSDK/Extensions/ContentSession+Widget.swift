//
//  ContentSession+Widget.swift
//  
//
//  Created by Jelzon Monzon on 11/14/22.
//

import Foundation
import LiveLikeSwift

extension ContentSession {
    /// Retrieves widgets that have already been posted. Each request returns a maximum of 20 posted widgets.
    /// - Parameters:
    ///   - page: Pass the `.next` page parameter to retrieve the next page of the posted widgets.
    ///   - completion: Use the `Result` value to parse an array of posted widgets
    public func getPostedWidgets(
        page: WidgetPagination,
        completion: @escaping (Result<[Widget]?, Error>) -> Void
    ) {
        self.getPostedWidgetModels(page: page) { result in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let widgetModels):
                let widgets: [Widget]? = widgetModels?.compactMap { widgetModel in
                    return DefaultWidgetFactory.makeWidget(from: widgetModel)
                }
                completion(.success(widgets))
            }
        }
    }
}
