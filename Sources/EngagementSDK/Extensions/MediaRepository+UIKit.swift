//
//  MediaRepository+UIKit.swift
//  
//
//  Created by Jelzon Monzon on 11/15/22.
//

import UIKit
import LiveLikeCore

extension MediaRepository.GetImageResult {
    var image: UIImage {
        return UIImage(data: self.imageData) ?? UIImage()
    }
}
