//
//  Typealias+LiveLikeSwift.swift
//  
//
//  Created by Jelzon Monzon on 11/14/22.
//

import Foundation
import LiveLikeSwift

public typealias EngagementSDKConfig = LiveLikeSwift.LiveLikeConfig
public typealias ContentSession = LiveLikeSwift.ContentSession
public typealias SessionConfiguration = LiveLikeSwift.SessionConfiguration
public typealias ContentSessionDelegate = LiveLikeSwift.ContentSessionDelegate
public typealias SessionStatus = LiveLikeSwift.SessionStatus
public typealias EngagementSDKDelegate = LiveLikeSwift.EngagementSDKDelegate
public typealias AccessTokenStorage = LiveLikeSwift.AccessTokenStorage

public typealias WidgetKind = LiveLikeSwift.WidgetKind
public typealias WidgetModel = LiveLikeSwift.WidgetModel
public typealias WidgetOrdering = LiveLikeSwift.WidgetOrdering
public typealias WidgetStatus = LiveLikeSwift.WidgetStatus
public typealias PredictionWidgetModel = LiveLikeSwift.PredictionWidgetModel
public typealias ImageSliderWidgetModel = LiveLikeSwift.ImageSliderWidgetModel
public typealias PredictionFollowUpWidgetModel = LiveLikeSwift.PredictionFollowUpWidgetModel
public typealias AlertWidgetModel = LiveLikeSwift.AlertWidgetModel
public typealias NumberPredictionWidgetModel = LiveLikeSwift.NumberPredictionWidgetModel
public typealias NumberPredictionFollowUpWidgetModel = LiveLikeSwift.NumberPredictionFollowUpWidgetModel
public typealias PollWidgetModelDelegate = LiveLikeSwift.PollWidgetModelDelegate
public typealias PollWidgetModel = LiveLikeSwift.PollWidgetModel
public typealias PredictionVote = LiveLikeSwift.PredictionVote
public typealias QuizWidgetModel = LiveLikeSwift.QuizWidgetModel
public typealias CheerMeterWidgetModel = LiveLikeSwift.CheerMeterWidgetModel
public typealias PredictionVoteRepository = LiveLikeSwift.PredictionVoteRepository
public typealias NumberPredictionVoteSubmission = LiveLikeSwift.NumberPredictionVoteSubmission
public typealias QuizWidgetModelDelegate = LiveLikeSwift.QuizWidgetModelDelegate
public typealias PredictionWidgetModelDelegate = LiveLikeSwift.PredictionWidgetModelDelegate
public typealias WidgetConfig = LiveLikeSwift.WidgetConfig
public typealias WidgetInteraction = LiveLikeSwift.WidgetInteraction
public typealias NumberPredictionWidgetModelDelegate = LiveLikeSwift.NumberPredictionWidgetModelDelegate
public typealias NumberPredictionOption = LiveLikeSwift.NumberPredictionOption
public typealias NumberPredictionVote = LiveLikeSwift.NumberPredictionVote
public typealias ImageSliderWidgetModelDelegate = LiveLikeSwift.ImageSliderWidgetModelDelegate
public typealias GetWidgetModelsRequestOptions = LiveLikeSwift.GetWidgetModelsRequestOptions
public typealias GetWidgetInteractionsRequestOptions = LiveLikeSwift.GetWidgetInteractionsRequestOptions
public typealias CheerMeterWidgetModelDelegate = LiveLikeSwift.CheerMeterWidgetModelDelegate

public typealias ChatClient = LiveLikeSwift.ChatClient
public typealias MessageMetadata = LiveLikeSwift.MessageMetadata
public typealias ChatFilter = LiveLikeSwift.ChatFilter
public typealias ChatMessageID = LiveLikeSwift.ChatMessageID
public typealias PinMessageInfo = LiveLikeSwift.PinMessageInfo
public typealias ChatRoomInfo = LiveLikeSwift.ChatRoomInfo
public typealias ChatSession = LiveLikeSwift.ChatSession
public typealias ChatRoomMember = LiveLikeSwift.ChatRoomMember
public typealias ChatSessionConfig = LiveLikeSwift.ChatSessionConfig
public typealias NewChatMessage = LiveLikeSwift.NewChatMessage
public typealias ChatMessage = LiveLikeSwift.ChatMessage
public typealias ChatRoomInvitation = LiveLikeSwift.ChatRoomInvitation
public typealias BlockInfo = LiveLikeSwift.BlockInfo
public typealias UnblockInfo = LiveLikeSwift.UnblockInfo
public typealias ChatRoomInvitationStatus = LiveLikeSwift.ChatRoomInvitationStatus
public typealias ChatClientDelegate = LiveLikeSwift.ChatClientDelegate
public typealias NewChatMembershipInfo = LiveLikeSwift.NewChatMembershipInfo
public typealias ChatRoomVisibilty = LiveLikeSwift.ChatRoomVisibilty
public typealias ChatSessionDelegate = LiveLikeSwift.ChatSessionDelegate
public typealias TokenGate = LiveLikeSwift.TokenGate
public typealias BlockChainNetworkType = LiveLikeSwift.BlockChainNetworkType
public typealias BlockChainTokenType = LiveLikeSwift.BlockChainTokenType
public typealias NFTAttribute = LiveLikeSwift.NFTAttribute

public typealias ReactionSession = LiveLikeSwift.ReactionSession
public typealias ReactionPack = LiveLikeSwift.ReactionPack
public typealias ReactionSpace = LiveLikeSwift.ReactionSpace
public typealias UserReactionCount = LiveLikeSwift.UserReactionCount

public typealias Attribute = LiveLikeSwift.Attribute


public typealias Reward = LiveLikeSwift.Reward
public typealias RewardsClientDelegate = LiveLikeSwift.RewardsClientDelegate
public typealias RewardItem = LiveLikeSwift.RewardItem
public typealias GetApplicationRewardItemsRequestOptions = LiveLikeSwift.GetApplicationRewardItemsRequestOptions
public typealias RewardItemAttribute = LiveLikeSwift.RewardItemAttribute
public typealias RewardsClient = LiveLikeSwift.RewardsClient
public typealias RewardItemTransfer = LiveLikeSwift.RewardItemTransfer
public typealias RewardTransaction = LiveLikeSwift.RewardTransaction
public typealias InvokedRewardAction = LiveLikeSwift.InvokedRewardAction
public typealias GetInvokedRewardActionsOptions = LiveLikeSwift.GetInvokedRewardActionsOptions
public typealias RewardActionInfo = LiveLikeSwift.RewardActionInfo
public typealias RewardItemTransferRequestOptions = LiveLikeSwift.RewardItemTransferRequestOptions
public typealias RewardTransactionsRequestOptions = LiveLikeSwift.RewardTransactionsRequestOptions

public typealias GetRedemptionKeysRequestOptions = LiveLikeSwift.GetRedemptionKeysRequestOptions
public typealias RedemptionKey = LiveLikeSwift.RedemptionKey
public typealias RedemptionKeyStatus = LiveLikeSwift.RedemptionKeyStatus

public typealias Leaderboard = LiveLikeSwift.Leaderboard
public typealias LeaderboardEntry = LiveLikeSwift.LeaderboardEntry
public typealias ProfileLeaderboard = LiveLikeSwift.ProfileLeaderboard
public typealias ProfileLeaderboardView = LiveLikeSwift.ProfileLeaderboardView
public typealias GetLeaderboardEntriesRequestOptions = LiveLikeSwift.GetLeaderboardEntriesRequestOptions
public typealias LeaderboardClient = LiveLikeSwift.LeaderboardClient
public typealias LeaderboardPosition = LiveLikeSwift.LeaderboardPosition
public typealias LeaderboardDelegate = LiveLikeSwift.LeaderboardDelegate

public typealias UserQuest = LiveLikeSwift.UserQuest
public typealias UserQuestTask = LiveLikeSwift.UserQuestTask
public typealias GetUserQuestsRequestOptions = LiveLikeSwift.GetUserQuestsRequestOptions
public typealias Quest = LiveLikeSwift.Quest
public typealias QuestTask = LiveLikeSwift.QuestTask
public typealias GetQuestsRequestOptions = LiveLikeSwift.GetQuestsRequestOptions

public typealias Badge = LiveLikeSwift.Badge
public typealias ProfileBadge = LiveLikeSwift.ProfileBadge
public typealias BadgeProfile = LiveLikeSwift.BadgeProfile

public typealias Sponsor = LiveLikeSwift.Sponsor

public typealias TimeToken = LiveLikeSwift.TimeToken

public typealias Comment = LiveLikeSwift.Comment
public typealias CommentBoard = LiveLikeSwift.CommentBoard
public typealias CommentClient = LiveLikeSwift.CommentClient
public typealias CommentReport = LiveLikeSwift.CommentReport
public typealias GetCommentRepliesRequestOptions = LiveLikeSwift.GetCommentRepliesRequestOptions
public typealias UpdateCommentBoardRequestOptions = LiveLikeSwift.UpdateCommentBoardRequestOptions
public typealias CreateCommentBoardRequestOptions = LiveLikeSwift.CreateCommentBoardRequestOptions
public typealias GetCommentsRequestOptions = LiveLikeSwift.GetCommentsRequestOptions
public typealias CommentBoardBanDetails = LiveLikeSwift.CommentBoardBanDetails
public typealias CommentBoardBanRequestOptions = LiveLikeSwift.CommentBoardBanRequestOptions
public typealias GetCommentBoardBansListRequestOptions = LiveLikeSwift.GetCommentBoardBansListRequestOptions
public typealias CommentReportStatusDetail = LiveLikeSwift.CommentReportStatusDetail
public typealias CreateCommentReportRequestOptions = LiveLikeSwift.CreateCommentReportRequestOptions
public typealias GetCommentReportsListRequestOptions = LiveLikeSwift.GetCommentReportsListRequestOptions


// MARK: - Social Graph
public typealias SocialGraphClient = LiveLikeSwift.SocialGraphClient
public typealias GetProfileRelationshipsOptions = LiveLikeSwift.GetProfileRelationshipsOptions
public typealias CreateProfileRelationshipOptions = LiveLikeSwift.CreateProfileRelationshipOptions
public typealias DeleteProfileRelationshipOptions = LiveLikeSwift.DeleteProfileRelationshipOptions
public typealias GetProfileRelationshipTypesOptions = LiveLikeSwift.GetProfileRelationshipTypesOptions
public typealias ProfileRelationship = LiveLikeSwift.ProfileRelationship
public typealias ProfileRelationshipType = LiveLikeSwift.ProfileRelationshipType
public typealias GetProfileRealtionshipDetailsOptions = LiveLikeSwift.GetProfileRealtionshipDetailsOptions

// MARK: - Presence Client
public typealias PresenceClientDelegate = LiveLikeSwift.PresenceClientDelegate
public typealias PresenceClient = LiveLikeSwift.PresenceClient
public typealias PresenceEvent = LiveLikeSwift.PresenceEvent


// MARK: - User Client
public typealias UserClient = LiveLikeSwift.UserClient
public typealias UserClientDelegate = LiveLikeSwift.UserClientDelegate

