//
//  TypeAlias+LiveLikeCore.swift
//  
//
//  Created by Jelzon Monzon on 12/7/22.
//

import LiveLikeCore

/// The level of detail to be logged to Apple's unified logging system.
public typealias LogLevel = LiveLikeCore.LogLevel
public typealias Pagination = LiveLikeCore.Pagination
