//
//  UIImageViewAligned.swift
//  EngagementSDK
//

import UIKit

struct UIImageViewAlignmentMask: OptionSet {
    let rawValue: Int
    init(rawValue: Int) { self.rawValue = rawValue }

    /// The option to align the content to the center.
    static let center = UIImageViewAlignmentMask([])
    /// The option to align the content to the left.
    static let left = UIImageViewAlignmentMask(rawValue: 1)
    /// The option to align the content to the right.
    static let right = UIImageViewAlignmentMask(rawValue: 2)
    /// The option to align the content to the top.
    static let top = UIImageViewAlignmentMask(rawValue: 4)
    /// The option to align the content to the bottom.
    static let bottom = UIImageViewAlignmentMask(rawValue: 8)
    /// The option to align the content to the top left.
    static let topLeft: UIImageViewAlignmentMask = [top, left]
    /// The option to align the content to the top right.
    static let topRight: UIImageViewAlignmentMask = [top, right]
    /// The option to align the content to the bottom left.
    static let bottomLeft: UIImageViewAlignmentMask = [bottom, left]
    /// The option to align the content to the bottom right.
    static let bottomRight: UIImageViewAlignmentMask = [bottom, right]
}

@IBDesignable
class UIImageViewAligned: GIFImageView {
    /**
     The technique to use for aligning the image.

     Changes to this property can be animated.
     */
    var alignment: UIImageViewAlignmentMask = .center {
        didSet {
            guard alignment != oldValue else { return }
            updateLayout()
        }
    }

    override var image: UIImage? {
        get {
            return realImageView?.image
        }
        set {
            runOnMainThread { [weak self] in
                guard let self = self else { return }
                self.realImageView?.image = newValue
                self.setNeedsLayout()
            }
        }
    }

    override var highlightedImage: UIImage? {
        get {
            return realImageView?.highlightedImage
        }
        set {
            runOnMainThread { [weak self] in
                guard let self = self else { return }
                self.realImageView?.highlightedImage = newValue
                self.setNeedsLayout()
            }
        }
    }

    /**
     The option to align the content to the top.

     It is available in Interface Builder and should not be set programmatically. Use `alignment` property if you want to set alignment outside Interface Builder.
     */
    @IBInspectable var livelike_alignTop: Bool {
        get {
            return getInspectableProperty(.top)
        }
        set {
            setInspectableProperty(newValue, alignment: .top)
        }
    }

    /**
     The option to align the content to the left.

     It is available in Interface Builder and should not be set programmatically. Use `alignment` property if you want to set alignment outside Interface Builder.
     */
    @IBInspectable var livelike_alignLeft: Bool {
        get {
            return getInspectableProperty(.left)
        }
        set {
            setInspectableProperty(newValue, alignment: .left)
        }
    }

    /**
     The option to align the content to the right.

     It is available in Interface Builder and should not be set programmatically. Use `alignment` property if you want to set alignment outside Interface Builder.
     */
    @IBInspectable var livelike_alignRight: Bool {
        get {
            return getInspectableProperty(.right)
        }
        set {
            setInspectableProperty(newValue, alignment: .right)
        }
    }

    /**
     The option to align the content to the bottom.

     It is available in Interface Builder and should not be set programmatically. Use `alignment` property if you want to set alignment outside Interface Builder.
     */
    @IBInspectable var livelike_alignBottom: Bool {
        get {
            return getInspectableProperty(.bottom)
        }
        set {
            setInspectableProperty(newValue, alignment: .bottom)
        }
    }

    override var isHighlighted: Bool {
        get {
            return super.isHighlighted
        }
        set {
            super.isHighlighted = newValue
            layer.contents = nil
        }
    }

    /**
     The inner image view.

     It should be used only when necessary.
     Accessible to keep compatibility with the original `UIImageViewAligned`.
     */
    private(set) var realImageView: UIImageView?

    private var realContentSize: CGSize {
        var size = bounds.size

        guard let image = image else { return size }

        let scaleX = size.width / image.size.width
        let scaleY = size.height / image.size.height

        switch contentMode {
        case .scaleAspectFill:
            let scale = max(scaleX, scaleY)
            size = CGSize(width: image.size.width * scale, height: image.size.height * scale)

        case .scaleAspectFit:
            let scale = min(scaleX, scaleY)
            size = CGSize(width: image.size.width * scale, height: image.size.height * scale)

        case .scaleToFill:
            size = CGSize(width: image.size.width * scaleX, height: image.size.height * scaleY)

        default:
            size = image.size
        }

        return size
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    override init(image: UIImage?) {
        super.init(image: image)
        setup(image: image)
    }

    override init(image: UIImage?, highlightedImage: UIImage?) {
        super.init(image: image, highlightedImage: highlightedImage)
        setup(image: image, highlightedImage: highlightedImage)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layoutIfNeeded()
        updateLayout()
    }

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        layer.contents = nil
    }

    override func didMoveToWindow() {
        super.didMoveToWindow()
        layer.contents = nil
        if #available(iOS 11, *) {
            let currentImage = realImageView?.image
            image = nil
            realImageView?.image = currentImage
        }
    }

    private func setup(image: UIImage? = nil, highlightedImage: UIImage? = nil) {
        realImageView = UIImageView(image: image ?? super.image, highlightedImage: highlightedImage ?? super.highlightedImage)
        realImageView?.frame = bounds
        realImageView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        realImageView?.contentMode = contentMode
        addSubview(realImageView!)
    }

    private func updateLayout() {
        let realSize = realContentSize
        var realFrame = CGRect(
            origin: CGPoint(
                x: (bounds.size.width - realSize.width) / 2.0,
                y: (bounds.size.height - realSize.height) / 2.0
            ),
            size: realSize
        )

        if alignment.contains(.left) {
            realFrame.origin.x = 0.0
        } else if alignment.contains(.right) {
            realFrame.origin.x = bounds.maxX - realFrame.size.width
        }

        if alignment.contains(.top) {
            realFrame.origin.y = 0.0
        } else if alignment.contains(.bottom) {
            realFrame.origin.y = bounds.maxY - realFrame.size.height
        }

        realImageView?.frame = realFrame.integral

        // Make sure we clear the contents of this container layer, since it refreshes from the image property once in a while.
        if #available(iOS 13, *) {
            self.layer.contents = CALayer()
        } else if #available(iOS 11, *) {
            super.image = nil
        } else {
            layer.contents = nil
        }
    }

    private func setInspectableProperty(_ newValue: Bool, alignment: UIImageViewAlignmentMask) {
        if newValue {
            self.alignment.insert(alignment)
        } else {
            self.alignment.remove(alignment)
        }
    }

    private func getInspectableProperty(_ alignment: UIImageViewAlignmentMask) -> Bool {
        return self.alignment.contains(alignment)
    }

    private func runOnMainThread(completion: @escaping () -> Void) {
        if Thread.isMainThread {
            completion()
        } else {
            DispatchQueue.main.async {
                completion()
            }
        }
    }
}
