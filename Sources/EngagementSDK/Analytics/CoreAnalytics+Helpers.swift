//
//  UIAnalytics+Helpers.swift
//  
//
//  Created by Jelzon Monzon on 11/21/22.
//

import Foundation
import LiveLikeCore

extension CoreAnalytics {
    func recordKeyboardHidden(properties: KeyboardHiddenProperties) {
        var data: [String: Any] = [
            UIAnalyticsConsts.Property.keyboardType: properties.keyboardType.name,
            UIAnalyticsConsts.Property.keyboardHideMethod: properties.keyboardHideMethod.name
        ]
        if let messageID = properties.messageID {
            data[UIAnalyticsConsts.Property.chatMessageId] = messageID
        }

        record(
            name: UIAnalyticsConsts.Name.keyboardHidden,
            properties: data
        )
    }

    func recordKeyboardSelected(keyboardType: KeyboardType) {
        record(
            name: UIAnalyticsConsts.Name.keyboardSelected,
            properties: [
                UIAnalyticsConsts.Property.keyboardType: keyboardType.name
            ]
        )
    }

    func recordWidgetInteracted(
        widgetModel: WidgetModel
    ) {
        record(
            name: UIAnalyticsConsts.Name.widgetInteracted,
            properties: {
                let props: [String: Any?] = [
                    UIAnalyticsConsts.Property.programId: widgetModel.programID,
                    UIAnalyticsConsts.Property.widgetType: widgetModel.kind.displayName,
                    UIAnalyticsConsts.Property.widgetId: widgetModel.id,
                    UIAnalyticsConsts.Property.widgetPrompt: widgetModel.prompt
                ]
                return props.compactMapValues { $0 }
            }()
        )
    }

    func recordWidgetUserDismissed(
        programID: String,
        properties: WidgetDismissedProperties
    ) {
        var data: [String: Any] = [
            UIAnalyticsConsts.Property.programId: programID,
            UIAnalyticsConsts.Property.widgetType: properties.widgetKind,
            UIAnalyticsConsts.Property.widgetId: properties.widgetId,
            UIAnalyticsConsts.Property.dismissAction: properties.dismissAction.rawValue,
            UIAnalyticsConsts.Property.dismissSecondsSinceStart: properties.dismissSecondsSinceStart
        ]
        if let interactableState = properties.interactableState {
            data[UIAnalyticsConsts.Property.interactableState] = interactableState.rawValue
        }
        if let seconds = properties.dismissSecondsSinceLastTap {
            data[UIAnalyticsConsts.Property.dismissSecondsSinceLastTap] = seconds
        }
        record(
            name: UIAnalyticsConsts.Name.widgetUserDismissed,
            properties: data
        )
    }

}

private extension WidgetModel {
    var prompt: String? {
        switch self {
        case .alert(let model):
            return model.title
        case .cheerMeter(let model):
            return model.title
        case .quiz(let model):
            return model.question
        case .prediction(let model):
            return model.question
        case .predictionFollowUp(let model):
            return model.question
        case .poll(let model):
            return model.question
        case .imageSlider(let model):
            return model.question
        case .socialEmbed:
            return nil
        case .videoAlert(let model):
            return model.title
        case .textAsk(let model):
            return model.title
        case .numberPrediction(let model):
            return model.question
        case .numberPredictionFollowUp(let model):
            return model.question
        case .richPost(let model):
            return model.title
        }
    }
}
