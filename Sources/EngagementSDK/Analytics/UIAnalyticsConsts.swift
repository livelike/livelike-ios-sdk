//
//  UIAnalyticsConsts.swift
//  
//
//  Created by Jelzon Monzon on 11/21/22.
//

import Foundation

struct UIAnalyticsConsts {
    struct Name {
        static let chatMessageDisplayed = "Chat Message Displayed"
        static let chatFlagButtonPressed = "Chat Flag Button Pressed"
        static let chatFlagActionSelected = "Chat Flag Action Selected"
        static let chatDeleteActionSelected = "Chat Delete Action Selected"
        static let chatReactionPanelOpened = "Chat Reaction Panel Opened"
        static let chatReactionSelected = "Chat Reaction Selected"
        static let chatReactionAdded = "Chat Reaction Added"
        static let chatReactionRemoved = "Chat Reaction Removed"
        static let chatMessageURLClicked = "Chat Message Link Clicked"
        static let chatScrollInitiated = "Chat Scroll Initiated"
        static let chatScrollCompleted = "Chat Scroll Completed"
        static let chatVisibilityStatusChanged = "Chat Visibility Status Changed"
        static let chatPauseStatusChanged = "Chat Pause Status Changed"
        
        static let keyboardSelected = "Keyboard Selected"
        static let keyboardHidden = "Keyboard Hidden"
        static let widgetInteracted = "Widget Interacted"
        static let widgetUserDismissed = "Widget Dismissed"
    }
    
    struct Property {
        static let targetChatMessageID = "Target Chat Message ID"
        static let targetUserProfileID = "Target User Profile ID"
        static let selectedAction = "Selected Action"
        static let chatMessageID = "Chat Message ID"
        static let chatReactionID = "Chat Reaction ID"
        static let chatReactionAction = "Reaction Action"
        static let chatRoomID = "Chat Room ID"
        static let chatMessageURL = "Chat Message Link"
        
        static let messagesScrolledThrough = "# of Messages Scrolled Through"
        static let maxReached = "Max Reached"
        static let returnMethod = "Return Method"
        static let stickerShortcodes = "Sticker Shortcodes"
        
        static let keyboardType = "Keyboard Type"
        static let keyboardHideMethod = "Keyboard Hide Method"
        static let chatMessageId = "Chat Message ID"
        static let widgetType = "Widget Type"
        static let widgetId = "Widget ID"
        static let widgetPrompt = "Widget Prompt"
        static let programId = "Program ID"
        static let dismissAction = "Dismiss Action"
        static let interactableState = "Interactable State"
        static let dismissSecondsSinceStart = "Dismiss Seconds Since Start"
        
        static let dismissSecondsSinceLastTap = "Dismiss Seconds Since Last Tap"
        static let previousPauseStatus = "Previous Pause Status"
        static let newPauseStatus = "New Pause Status"
        static let secondsInPreviousPauseStatus = "Seconds In Previous Pause Status"
        static let previousVisibilityStatus = "Previous Visibility Status"
        static let newVisibilityStatus = "New Visibility Status"
        static let secondsInPreviousVisibilityStatus = "Seconds In Previous Visibility Status"
    }

}
