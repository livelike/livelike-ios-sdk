//
//  TemplatedURLHelper.swift
//  EngagementSDK
//
//  Created by Keval Shah on 15/12/22.
//

import Foundation

public class TemplatedURLHelper {
    enum Error: LocalizedError {
        case failedToBuildURLFromTemplate
    }

    public static func buildURL(
        templatedURLString: String,
        templateKey: String,
        replacementString: String
    ) throws -> URL {
        let filledTemplate = templatedURLString.replacingOccurrences(
            of: templateKey,
            with: replacementString
        )
        guard let url = URL(string: filledTemplate) else {
            throw Error.failedToBuildURLFromTemplate
        }
        return url
    }
}
