//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//
	

import Foundation

/// Helpers for building request to LiveLike APIs
public class LLAPIHelper {
    /// The production origin url
    public static let defaultAPIOrigin: URL = URL(string: "https://cf-blast.livelikecdn.com/api/v1")!
    
    /// A JSONDecoder used to decode responses from LiveLike APIs
    public static let jsonDecoder: JSONDecoder = LLJSONDecoder()
    
    /// A JSONEncoder used to encode http body/payloads to LiveLikeAPIs
    public static let jsonEncoder: JSONEncoder = LLJSONEncoder()
    
    /// Attaches an access token to a URLRequest
    public static func attach(
        accessToken: String,
        to urlRequest: inout URLRequest
    ) {
        urlRequest.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
    }
    
    /// Attaches a personal API token to a URLRequest
    public static func attach(
        personalAPIToken: String,
        to urlRequest: inout URLRequest
    ) {
        urlRequest.setValue("Token \(personalAPIToken)", forHTTPHeaderField: "Authorization")
    }
}
