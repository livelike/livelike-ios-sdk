//
//  URL+QueryItems.swift
//  
//
//  Created by Jelzon Monzon on 7/27/23.
//

import Foundation

public enum URLComponentError: LocalizedError {
    case failedToInitURLComponents(fromURL: URL)
    case urlIsNil
}

extension URL {
    public func with(queryItems: [URLQueryItem]) throws -> URL {
        guard var components = URLComponents(
            url: self,
            resolvingAgainstBaseURL: false
        ) else {
            throw URLComponentError.failedToInitURLComponents(fromURL: self)
        }
        components.queryItems = queryItems
        guard let url = components.url else {
            throw URLComponentError.urlIsNil
        }
        return url
    }

    public func appendingQueryItems(_ newItems: [URLQueryItem]) throws -> URL {
        guard var components = URLComponents(
            url: self,
            resolvingAgainstBaseURL: false
        ) else {
            throw URLComponentError.failedToInitURLComponents(fromURL: self)
        }

        var queryItems = components.queryItems ?? []
        queryItems.append(contentsOf: newItems)
        components.queryItems = queryItems
        guard let url = components.url else {
            throw URLComponentError.urlIsNil
        }
        return url
    }
}
