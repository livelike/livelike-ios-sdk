//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//


import Foundation

public enum PaginationErrors: LocalizedError {
    case nextPageUnavailable
    case previousPageUnavailable

    public var errorDescription: String? {
        switch self {
        case .nextPageUnavailable:
            return "There is no next page available. Try calling with .first instead."
        case .previousPageUnavailable:
            return "There is no previous page available. Try calling with .first instead."
        }
    }
}
