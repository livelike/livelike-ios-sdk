//
//  OptionalObject.swift
//  
//
//  Created by Jelzon Monzon on 8/2/23.
//

import Foundation

/// Wraps a Decodable as an optional
/// Useful for decoding collections of dynamic objects which may fail and throw an error
public struct OptionalObject<Base: Decodable>: Decodable {
    public let value: Base?

    public init(from decoder: Decoder) throws {
        do {
            let container = try decoder.singleValueContainer()
            self.value = try container.decode(Base.self)
        } catch {
            log.error(error)
            self.value = nil
        }
    }
}
