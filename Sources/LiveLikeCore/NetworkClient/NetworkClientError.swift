//
//  File.swift
//  
//
//  Created by Jelzon Monzon on 7/29/22.
//

import Foundation

public enum NetworkClientError: LocalizedError {
    case invalidResponse(description: String)
    case internalServerError
    case badRequest(
        detail: String? = nil,
        rawResponse: String? = nil,
        httpResponse: HTTPURLResponse? = nil
    )
    case notFound404
    case noData
    case decodingError(Data, Error)
    case forbidden(
        detail: String? = nil,
        errorCode: String? = nil
    )
    case unauthorized(
        detail: String? = nil,
        errorCode: String? = nil
    )
    case badDeleteResponseType

    public var errorDescription: String? {
        switch self {
        case let .decodingError(data, error):
            return
                """
                Decoding Error: \(String(describing: error))
                Data:
                \(String(data: data, encoding: .utf8) ?? "")
                """
        case let .badRequest(detail, rawResponse, httpResponse):
            return """
            HTTP \(httpResponse?.statusCode.description ?? "n/a")
            rawResponse: \(rawResponse ?? "n/a")
            detail: \(detail ?? "n/a")
            """
        case let .unauthorized(detail, errorCode):
            return """
            Error Code: \(errorCode ?? "n/a")
            Detail: \(detail ?? "n/a")
            """
        case let .forbidden(detail, errorCode):
            return """
            Error Code: \(errorCode ?? "n/a")
            Detail: \(detail ?? "n/a")
            """
        default:
            return String(describing: self)
        }
    }
}

