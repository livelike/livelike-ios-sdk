//
//  LLPaginatedRequest.swift
//  
//
//  Created by Jelzon Monzon on 2/7/23.
//

import Foundation

/// A request for a paginated resource
public protocol LLPaginatedRequest: LLRequest where ResponseType == PaginatedResult<ElementType> {
    /// The type of items in the paginated result
    associatedtype ElementType: Decodable

    /// An identifier for the request
    var requestID: String { get }
}
