//
//  PaginatedResource.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 5/20/21.
//

import Foundation

/// A generic result type that can be used when parsing paginated results from backend
open class PaginatedResult<Element: Decodable>: Decodable {
    public init(previous: URL?, count: Int, next: URL?, items: [Element]) {
        self.previous = previous
        self.count = count
        self.next = next
        self.items = items
    }

    public let previous: URL?
    public let next: URL?
    public let count: Int

    public let items: [Element]
    public var hasNext: Bool {
        return next != nil ? true : false
    }
    public var hasPrevious: Bool {
        return previous != nil ? true : false
    }

    enum CodingKeys: CodingKey {
        case previous
        case count
        case next
        case results
    }

    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.previous = try container.decode(URL?.self, forKey: .previous)
        self.next = try container.decode(URL?.self, forKey: .next)
        self.count = try container.decode(Int.self, forKey: .count)
        // compactMaps unexpected or corrupt elements
        self.items = try container.decode([OptionalObject<Element>].self, forKey: .results).compactMap { $0.value }
    }
}

/// Used by the repos that keep track of the `nextURL` and `previousURL` from a paginated result
public struct PaginatedResultURL {
    public let nextURL: URL?
    public let previousURL: URL?

    public init(nextURL: URL?, previousURL: URL?) {
        self.nextURL = nextURL
        self.previousURL = previousURL
    }
}

/// Represents the different  pagination types that can be passed down to when working with paginated calls
public enum Pagination {
    case first
    case next
    case previous
}
