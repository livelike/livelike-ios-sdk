//
//  LLRequest.swift
//  
//
//  Created by Jelzon Monzon on 1/31/23.
//

import Foundation

/// A single API request where ResponseType is the resource in the response
public protocol LLRequest {
    associatedtype ResponseType: Decodable

    /// Execute the request at a given url
    func execute(
        url: URL,
        completion: @escaping (Result<ResponseType, Error>) -> Void
    )
}

extension LLRequest {
    public func execute(
        tryURL: @autoclosure () throws -> URL,
        completion: @escaping (Result<ResponseType, Error>) -> Void
    ) {
        do {
            execute(
                url: try tryURL(),
                completion: completion
            )
        } catch {
            completion(.failure(error))
        }
    }
}

extension LLRequest {
    /// Execute the request at a given url
    public func execute(url: URL) -> Promise<ResponseType> {
        return Promise { fulfill, reject in
            self.execute(url: url) { result in
                switch result {
                case .failure(let error):
                    reject(error)
                case .success(let resource):
                    fulfill(resource)
                }
            }
        }
    }
}


