//
//  SDKNetworking.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 7/22/19.
//

import Foundation

class SDKNetworking {

    // Set to `true` to print http requests
    private static let isLoggingEnabled: Bool = false

    let urlSession: URLSession

    init(userAgent: String) {
        let urlSessionConfig: URLSessionConfiguration = .default
        urlSessionConfig.httpAdditionalHeaders = ["User-Agent": userAgent]
        urlSession = URLSession(configuration: urlSessionConfig)
    }
}

private struct LiveLikeRestAPIError: Decodable {
    let detail: String
    let errorCode: String?
}

extension SDKNetworking: LLNetworking {

    public var userAgent: String {
        get {
            self.urlSession.configuration.httpAdditionalHeaders?["User-Agent"] as? String ?? ""
        }
        set {
            self.urlSession.configuration.httpAdditionalHeaders?["User-Agent"] = newValue
        }
    }

    public func task(request: URLRequest, completion: @escaping (Result<Data, Error>) -> Void) {
        self.urlSession.dataTask(with: request) { data, _, error in
            if let error = error {
                return completion(.failure(error))
            }
            guard let data = data else {
                return completion(.failure(NetworkClientError.noData))
            }
            return completion(.success(data))
        }.resume()
    }

    @discardableResult
    public func task<A>(
        _ resource: Resource<A>,
        completion: @escaping (Result<A, Error>) -> Void
    ) -> URLSessionDataTask {
        assert(Thread.isMainThread == false, "Should not be on main thread")
        let dataTask = self.urlSession.dataTask(with: resource.urlRequest) { data, response, error in
            if let error = error {
                return completion(.failure(error))
            }

            guard let httpResponse = response as? HTTPURLResponse else {
                return completion(.failure((NetworkClientError.invalidResponse(description: "No Response"))))
            }

            guard 200 ..< 300 ~= httpResponse.statusCode else {
                log.error("bad response: \(httpResponse.statusCode) for request to url: \(String(describing: httpResponse.url))")
                let rawResponse: String? = {
                    guard
                        let data = data,
                        let message = String(data: data, encoding: .utf8)
                    else {
                        return nil
                    }
                    return message
                }()
                if let responseString = rawResponse {
                    log.error(responseString)
                }
                if httpResponse.statusCode == 401 || httpResponse.statusCode == 403 {
                    // try to decode error details
                    let error: LiveLikeRestAPIError? = {
                        guard let data = data else { return nil }
                        return try? JSONDecoder().decode(LiveLikeRestAPIError.self, from: data)
                    }()

                    if httpResponse.statusCode == 401 {
                        let error = NetworkClientError.unauthorized(
                            detail: error?.detail,
                            errorCode: error?.errorCode
                        )
                        completion(.failure(error))
                    } else if httpResponse.statusCode == 403 {
                        let error = NetworkClientError.forbidden(
                            detail: error?.detail,
                            errorCode: error?.errorCode
                        )
                        completion(.failure(error))
                    }
                } else if httpResponse.statusCode == 404 {
                    completion(.failure(NetworkClientError.notFound404))
                } else if 400 ..< 500 ~= httpResponse.statusCode {
                    // try to decode error details
                    let errorDetail: String? = {
                        guard let data = data else { return nil }
                        return try? JSONDecoder().decode(LiveLikeRestAPIError.self, from: data).detail
                    }()
                    let error = NetworkClientError.badRequest(
                        detail: errorDetail,
                        rawResponse: rawResponse,
                        httpResponse: httpResponse
                    )
                    completion(.failure(error))
                } else if 500 ..< 600 ~= httpResponse.statusCode {
                    completion(.failure(NetworkClientError.internalServerError))
                } else {
                    completion(.failure(NetworkClientError.invalidResponse(description: "Invalid Status Code: \(httpResponse.statusCode)")))
                }
                return
            }

            guard let data = data else {
                completion(.failure(NetworkClientError.noData))
                return
            }

            // Handle httpMethod `DELETE` which does not return a valid JSON when success
            if let httpMethod = resource.urlRequest.httpMethod,
               httpMethod == "DELETE"
            {
                if let deleteResult = true as? A {
                    completion(.success(deleteResult))
                } else {
                    // reject when `DELETE` response type is not specified as `Bool` type
                    completion(.failure(NetworkClientError.badDeleteResponseType))
                }
                return
            }

            do {
                #if DEBUG
                    if
                        SDKNetworking.isLoggingEnabled,
                        let httpMethod = resource.urlRequest.httpMethod,
                        let url = resource.urlRequest.url,
                        let token = resource.urlRequest.value(forHTTPHeaderField: "Authorization")
                    {
                        if let jsonResponseString = data.prettyPrintedJSONString {
                            print("\n\(httpMethod) \(url)\nAuthorization \(token)\n\(jsonResponseString)")
                        } else if let dataString = String(data: data, encoding: .utf8) {
                            print("\n\(httpMethod) \(url)\nAuthorization \(token)\n\(dataString)")
                        }
                    }
                #endif
                let result = try resource.parse(data)
                completion(.success(result))
            } catch {
                completion(.failure(NetworkClientError.decodingError(data, error)))
                return
            }
        }
        dataTask.resume()
        return dataTask
    }
}

private extension Data {
    var prettyPrintedJSONString: NSString? { /// NSString gives us a nice sanitized debugDescription
        var options: JSONSerialization.WritingOptions = [.prettyPrinted]
        if #available(iOS 13.0, macOS 10.15, tvOS 13.0, watchOS 6.0, *) {
            options.insert(.withoutEscapingSlashes)
        }
        guard
            let object = try? JSONSerialization.jsonObject(with: self, options: []),

            let data = try? JSONSerialization.data(withJSONObject: object, options: options),
            let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        else { return nil }

        return prettyPrintedString
    }
}
