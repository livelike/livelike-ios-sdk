//
//  LLJSONDecoder.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 4/15/21.
//

import Foundation

public class LLJSONDecoder: JSONDecoder {

    private let skipPlatformCheck: Bool

    public init(skipPlatformCheck: Bool = false) {
        self.skipPlatformCheck = skipPlatformCheck

        super.init()
        self.keyDecodingStrategy = .convertFromSnakeCase
        self.dateDecodingStrategy = .custom {
            let container = try $0.singleValueContainer()
            let dateString = try container.decode(String.self)

            if
                !skipPlatformCheck,
                #available(iOS 11.0, macOS 13.0, tvOS 11.0, watchOS 4.0, *),
                let iso8601DateFormatter: ISO8601DateFormatter =
            {
                    let formatter = ISO8601DateFormatter()
                    formatter.formatOptions = [.withFractionalSeconds, .withInternetDateTime]
                    return formatter
                }(),
                let isoDate = iso8601DateFormatter.date(from: dateString) {
                return isoDate
            } else if let date = DateFormatter.iso8601Full.date(from: dateString) {
                return date
            } else if let date = DateFormatter.iso8601.date(from: dateString) {
                return date
            } else if let date = DateFormatter.iso8601FullWithPeriod.date(from: dateString) {
                return date
            }

            throw DecodingError.dataCorruptedError(in: container, debugDescription: "Invalid date: \(dateString)")
        }
    }
}
