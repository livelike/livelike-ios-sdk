//
//  LLJSONEncoder.swift
//  EngagementSDK
//
//  Created by Mike Moloksher on 4/15/21.
//

import Foundation

public class LLJSONEncoder: JSONEncoder {
    public override init() {
        super.init()
        self.keyEncodingStrategy = .convertToSnakeCase
        self.dateEncodingStrategy = .iso8601
        self.outputFormatting = .sortedKeys
    }
}
