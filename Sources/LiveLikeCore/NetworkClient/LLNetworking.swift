//
//  LLNetworking.swift
//  EngagementSDK
//
//  Created by Jelzon Monzon on 8/24/22.
//

import Foundation

public protocol LLNetworking: AnyObject {
    @discardableResult
    func task<A>(
        _ resource: Resource<A>,
        completion: @escaping (Result<A, Error>) -> Void
    ) -> URLSessionDataTask

    func task(
        request: URLRequest,
        completion: @escaping (Result<Data, Error>) -> Void
    )

    var userAgent: String { get set }
}

extension LLNetworking {
    public func load<A>(_ resource: Resource<A>) -> Promise<A> {
        return Promise<A>(work: { fulfilled, rejected in
            self.task(resource) { result in
                switch result {
                case .failure(let error):
                    rejected(error)
                case .success(let resource):
                    fulfilled(resource)
                }
            }
        })
    }
}
