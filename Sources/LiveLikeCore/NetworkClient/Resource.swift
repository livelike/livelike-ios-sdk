//
//  Resource.swift
//  LiveLikeSDK
//
//  Created by Cory Sullivan on 2019-02-02.
//

import Foundation

public struct Resource<A> {
    var urlRequest: URLRequest
    let parse: (Data) throws -> A
}

public extension Resource where A: Decodable {
    init(get url: URL) {
        urlRequest = URLRequest(url: url)
        parse = { data in
            let decoder = LLJSONDecoder()
            return try decoder.decode(A.self, from: data)
        }
    }

    init(get url: URL, accessToken: String) {
        urlRequest = URLRequest(url: url)
        urlRequest.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        parse = { data in
            let decoder = LLJSONDecoder()
            return try decoder.decode(A.self, from: data)
        }
    }

    init<Body: Encodable>(url: URL, method: HttpMethod<Body>) {
        self.urlRequest = URLRequest(
            url: url,
            method: method
        )
        parse = { data in
            let decoder = LLJSONDecoder()
            return try decoder.decode(A.self, from: data)
        }
    }

    init<Body: Encodable>(url: URL, method: HttpMethod<Body>, accessToken: String) {
        self.urlRequest = URLRequest(
            url: url,
            method: method,
            accessToken: accessToken
        )
        parse = { data in
            let decoder = LLJSONDecoder()
            return try decoder.decode(A.self, from: data)
        }
    }
}

extension URLRequest {

    public init<Body: Encodable>(
        url: URL,
        method: HttpMethod<Body>
    ) {
        self.init(url: url)
        self.httpMethod = method.method
        switch method {
        case .get: ()
        case let .post(body), let .patch(body), let .delete(body), let .put(body):
            self.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let encoder = LLJSONEncoder()
            self.httpBody = try! encoder.encode(body) // swiftlint:disable:this force_try
        }
    }

    public init<Body: Encodable>(
        url: URL,
        method: HttpMethod<Body>,
        accessToken: String
    ) {
        self.init(url: url, method: method)
        setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
    }
}
