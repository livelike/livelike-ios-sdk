//
//  PaginatedRequestController.swift
//  
//
//  Created by Jelzon Monzon on 2/7/23.
//

import Foundation

/// Performs execute on a LLPaginatedRequest and stores urls for next and previous page of the request
public class PaginatedRequestController {

    private var pageResultURLRepo = [String: PaginatedResultURL]()

    public init() { }

    // Performs the execute function of a LLPaginatedRequest
    // Returns the items of the page
    public func performExecute<T: LLPaginatedRequest>(
        paginatedRequest: T,
        rootURL: URL,
        page: Pagination,
        completion: @escaping (Result<[T.ElementType], Error>) -> Void
    ) {
        performExecuteFull(
            paginatedRequest: paginatedRequest,
            rootURL: rootURL,
            page: page
        ) {
            completion($0.map { $0.items })
        }
    }

    // Performs the execute function of a LLPaginatedRequest
    // Returns the full page payload
    public func performExecuteFull<T: LLPaginatedRequest>(
        paginatedRequest: T,
        rootURL: URL,
        page: Pagination,
        completion: @escaping (Result<PaginatedResult<T.ElementType>, Error>) -> Void
    ) {
        do {
            let baseURL: URL = try {
                switch page {
                case .first:
                    // Reset result repo cache if the user is calling `.first` page
                    pageResultURLRepo[paginatedRequest.requestID] = nil
                    return rootURL
                case .next:
                    if let url = pageResultURLRepo[paginatedRequest.requestID]?.nextURL {
                        return url
                    } else {
                        throw PaginationErrors.nextPageUnavailable
                    }
                case .previous:
                    if let url = pageResultURLRepo[paginatedRequest.requestID]?.previousURL {
                        return url
                    } else {
                        throw PaginationErrors.previousPageUnavailable
                    }
                }
            }()

            DispatchQueue.global().async { [weak self] in
                guard let self = self else { return }
                paginatedRequest.execute(url: baseURL) { result in
                    switch result {
                    case .failure(let error):
                        completion(.failure(error))
                    case .success(let page):
                        self.pageResultURLRepo[paginatedRequest.requestID] = PaginatedResultURL(
                            nextURL: page.next,
                            previousURL: page.previous
                        )
                        completion(.success(page))
                    }
                }
            }
        } catch {
            completion(.failure(error))
        }
    }
}
