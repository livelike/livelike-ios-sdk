//
//  CoreAnalytics.swift
//  
//
//  Created by Jelzon Monzon on 11/21/22.
//

import Foundation

public class CoreAnalytics {

    public static var shared: CoreAnalytics = CoreAnalytics()

    public var didRecordEvent: ((String, [String: Any]) -> Void)?

    public func record(name: String, properties: [String: Any]) {
        self.didRecordEvent?(name, properties)
    }
}
