//
//  LiveLikeCore.swift
//  
//
//  Created by Jelzon Monzon on 11/18/22.
//

import Foundation

public class LLCore {
    public static let networking: LLNetworking = SDKNetworking(
        userAgent: "[LiveLikeCore/\(livelikeversion)]"
    )
}
