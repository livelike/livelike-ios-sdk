//
//  Cachable.swift
//  LiveLikeSDK
//
//  Created by Heinrich Dahms on 2019-02-15.
//

import Foundation

public protocol Cachable {
    static func decode(_ data: Data) -> Self?
    func encode() -> Data?
}
