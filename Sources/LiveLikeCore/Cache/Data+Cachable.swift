//
//  Data+Cachable.swift
//  LiveLikeSDK
//
//  Created by Heinrich Dahms on 2019-03-25.
//

import Foundation

extension Data: Cachable {
    typealias CacheType = Data

    public static func decode(_ data: Data) -> Data? {
        return data
    }

    public func encode() -> Data? {
        return self
    }
}
