## Contributing
To contribute to the iOS SDK, please make sure you have a JIRA ticket associated with the task.
If none exist, feel free to create one in the [Mobile Backlog JIRA board](https://livelike.atlassian.net/jira/software/c/projects/MBLG/issues/)

### Please refer to the following documents:
- [Development Workflow](https://livelike.quip.com/VFCBAdnenbuQ/iOS-Development-Workflow)
- [SDK Coding Style Guide](https://livelike.quip.com/eKCRAmJbSYay/iOS-SDK-Style-Guide)
- [Pull Request Requirements / Best Practices](https://livelike.quip.com/w1usAKbvzhcz/iOS-PR-Requirements-Best-Practices)
