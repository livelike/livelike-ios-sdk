//
//  ProgramResult.swift
//  LiveLikeDemo
//
//  Created by Cory Sullivan on 2019-02-12.
//

import Foundation

struct ProgramResult: Decodable {
    let results: [Event]
}

struct Event: Codable, Equatable {
    let id: String
    let name: String
    let videoUrl: URL
    let videoThumbnailUrl: URL
    let livelikeProgramUrl: URL
}
