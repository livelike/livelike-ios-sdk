//
//  Resource.swift
//  LiveLikeSDK
//
//  Created by Cory Sullivan on 2019-02-02.
//

import Foundation

struct Resource<A> {
    var urlRequest: URLRequest
    let parse: (Data) -> A?
}

extension Resource where A: Decodable {
    init(get url: URL, decodingStrategy: JSONDecoder.KeyDecodingStrategy = .convertFromSnakeCase) {
        urlRequest = URLRequest(url: url)
        parse = { data in
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = decodingStrategy
            return try? decoder.decode(A.self, from: data)
        }
    }

    init<Body: Encodable>(url: URL, method: HttpMethod<Body>, decodingStrategy: JSONDecoder.KeyDecodingStrategy = .convertFromSnakeCase) {
        urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.method
        switch method {
        case .get: ()
        case let .post(body), let .patch(body):
            urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.httpBody = try! JSONEncoder().encode(body) // swiftlint:disable:this force_try
        }
        parse = { data in
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = decodingStrategy
            return try? decoder.decode(A.self, from: data)
        }
    }
}
