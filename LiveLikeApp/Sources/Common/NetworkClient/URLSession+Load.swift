//
//  URLSession+Load.swift
//  LiveLikeSDK
//
//  Created by Cory Sullivan on 2019-02-02.
//

import Foundation

enum NetworkClientError: Error {
    case invalidResponse(description: String)
}

extension URLSession {
    func load<A>(_ resource: Resource<A>, completion: @escaping (A) -> Void, failure: ((Error) -> Void)?) {
        dataTask(with: resource.urlRequest) { data, response, error in
            if let error = error {
                failure?(error)
                return
            }

            guard let httpResponse = response as? HTTPURLResponse else {
                failure?(NetworkClientError.invalidResponse(description: "No Response"))
                return
            }
            guard 200 ..< 300 ~= httpResponse.statusCode else {
                failure?(NetworkClientError.invalidResponse(description: "Invalid Status Code: \(httpResponse.statusCode)"))
                return
            }
            guard let result = data.flatMap(resource.parse) else {
                failure?(NetworkClientError.invalidResponse(description: "Parsing Failed"))
                return
            }
            completion(result)
        }.resume()
    }
}
