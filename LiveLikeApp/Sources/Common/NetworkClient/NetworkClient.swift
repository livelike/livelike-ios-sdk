//
//  NetworkClient.swift
//  LiveLikeSDK
//
//  Created by Cory Sullivan on 2019-01-30.
//

import Foundation

struct EventNetworkConfiguration {
    static let baseURL = URL(string: "https://livelike-webs.s3.amazonaws.com/mobile-pilot/video-backend-sdk-ios.json")!
}

class NetworkClient {
    static let shared = NetworkClient()

    private init() {}

    func getEvents(completion: @escaping (ProgramResult) -> Void, failure: ((Error) -> Void)? = nil) {
        let url = EventNetworkConfiguration.baseURL
        let resource = Resource<ProgramResult>(get: url)
        URLSession.shared.load(resource, completion: completion, failure: failure)
    }
}
