//
//  AppDelegate.swift
//  TestProject
//
//  Created by Cory Sullivan on 2019-01-14.
//  Copyright © 2019 Cory Sullivan. All rights reserved.
//

import EngagementSDK
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    public static var theme: Theme {
        return ThemeManager.shared.themeDefinition.theme
    }

    static var sdkDelegate = SDKDelegate()

    func application(_: UIApplication, didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        EngagementSDK.logLevel = .debug
        return true
    }

    func applicationWillResignActive(_: UIApplication) {}

    func applicationDidEnterBackground(_: UIApplication) {}

    func applicationWillEnterForeground(_: UIApplication) {}

    func applicationDidBecomeActive(_: UIApplication) {}

    func applicationWillTerminate(_: UIApplication) {}
}

class SDKDelegate: EngagementSDKDelegate {
    func sdk(_ sdk: EngagementSDK, setupFailedWithError error: Error) {
        print(error)
    }
}
