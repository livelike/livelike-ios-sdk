//
//  OrientationProtocol.swift
//  LiveLikeDemo
//
//  Created by Heinrich Dahms on 2019-01-25.
//

import UIKit

protocol OrientationProtocol {
    var orientation: UIInterfaceOrientationMask { get set }
}
