//
//  UIAlertController+Events.swift
//  LiveLikeTestApp
//
//  Created by Heinrich Dahms on 2019-03-03.
//

import EngagementSDK
import UIKit

extension UIAlertController {
    static func eventsAlertController(themes: [ThemeDefinitions], completion: @escaping () -> Void) -> UIAlertController {
        let alertController = UIAlertController(
            title: "Themes",
            message: "Select theme",
            preferredStyle: .actionSheet
        )

        themes.forEach { theme in
            let action = UIAlertAction(title: theme.name, style: .default, handler: { _ in
                ThemeManager.shared.themeDefinition = theme
                completion()
            })
            alertController.addAction(action)
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)

        return alertController
    }

    static func eventsAlertController(events: [Program], programNumber: Int, completion: @escaping () -> Void) -> UIAlertController {
        let alertController = UIAlertController(title: "Events", message: "Select event", preferredStyle: .actionSheet)

        let clearAction = UIAlertAction(title: "none - clear session", style: .default, handler: { _ in
            EngagementSDKConfigManager.shared.selectedProgram = nil
            completion()
        })
        alertController.addAction(clearAction)

        events.forEach { event in
            let action = UIAlertAction(title: event.title, style: .default, handler: { _ in
                if programNumber == 1 {
                    EngagementSDKConfigManager.shared.selectedProgram = event
                } else {
                    EngagementSDKConfigManager.shared.selectedProgram2 = event
                }
                completion()
            })
            alertController.addAction(action)
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)

        return alertController
    }

    static func apiEndpointsAlertController(apiEndpoints: [Environment], completion: @escaping () -> Void) -> UIAlertController {
        let alertController = UIAlertController(title: "API Endpoints", message: "Select API endpoint", preferredStyle: .actionSheet)

        apiEndpoints.forEach { apiEndpoint in
            let action = UIAlertAction(title: apiEndpoint.name, style: .default, handler: { _ in
                EngagementSDKConfigManager.shared.selectedEnvironment = apiEndpoint
                completion()
            })
            alertController.addAction(action)
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)

        return alertController
    }

    static func clientIdAlertController(clientIDs: [ClientApp], completion: @escaping () -> Void) -> UIAlertController {
        let alertController = UIAlertController(title: "Client IDs", message: "Select Client ID", preferredStyle: .actionSheet)

        clientIDs.forEach { clientID in
            let action = UIAlertAction(title: clientID.name, style: .default, handler: { _ in
                EngagementSDKConfigManager.shared.selectedClientApp = clientID
                completion()
            })
            alertController.addAction(action)
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)

        return alertController
    }

    static func chooseThemeAlert(completion: @escaping (Theme) -> Void) -> UIAlertController {
        let alertController = UIAlertController(
            title: "Themes",
            message: "Select theme",
            preferredStyle: .actionSheet
        )

        ThemeDefinitions.allCases.forEach { theme in
            let action = UIAlertAction(title: theme.name, style: .default, handler: { _ in
                completion(theme.theme)
            })
            alertController.addAction(action)
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        return alertController
    }
}
