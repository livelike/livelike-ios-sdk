//
//  LiveLikeAPI.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 1/28/20.
//

import Foundation

// swiftlint:disable force_try
class LiveLikeAPI {

    static func sendCustomMessage(
        baseURL: URL,
        chatRoomID: String,
        customData: String,
        accessToken: String,
        completion: @escaping (Result<Void, Error>) -> Void
    ) {
        let url = baseURL.appendingPathComponent("chat-rooms/\(chatRoomID)/custom-messages/")
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        request.httpBody = "{\"custom_data\": \"\(customData)\"}".data(using: .utf8)

        URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                return completion(.failure(error))
            }
            guard let response = response as? HTTPURLResponse else {
                return completion(.failure(MyError("Response is not HTTPURLResponse")))
            }
            guard response.statusCode >= 200, response.statusCode < 300 else {
                if response.statusCode == 401 {
                    return completion(.failure(MyError("API Token was invalid.")))
                } else {
                    return completion(.failure(MyError("Request failed with status code: \(response.statusCode)")))
                }
            }
            guard data != nil else {
                return completion(.failure(MyError("Server returned no data.")))
            }
//            guard let dataString = String(data: data, encoding: .utf8) else {
//                return completion(.failure(MyError("Failed to encode data with utf8")))
//            }
//            guard let widgetCreatedResponse = try? JSONDecoder().decode(WidgetCreatedResponse.self, from: data) else {
//                return completion(.failure(MyError("Failed to decode data as WidgetCreatedResponse")))
//            }
            completion(.success(()))
        }.resume()
    }

    static func getAllPrograms(apiEndpoint: URL, clientID: String, completion: @escaping ([Program]) -> Void, failure: @escaping (Error) -> Void) {
        getApplicationConfiguration(
            apiURL: apiEndpoint,
            clientID: clientID
        ) { result in
            switch result {
            case let .failure(error):
                failure(error)
            case let .success(appConfig):
                getAllPrograms(url: appConfig.programsUrl) { result in
                    switch result {
                    case let .failure(error):
                        failure(error)
                    case let .success(programs):
                        completion(programs.results.map {
                            Program(
                                id: $0.id,
                                title: $0.title,
                                streamURL: $0.streamUrl.flatMap(URL.init(string:))
                            )
                        })
                    }
                }
            }
        }
    }

    private static func getApplicationConfiguration(
        apiURL: URL,
        clientID: String,
        completion: @escaping (Result<ApplicationResource, Error>) -> Void
    ) {
        struct NilError: Error {}

        let url = apiURL.appendingPathComponent("applications").appendingPathComponent(clientID)
        URLSession.shared.dataTask(with: url) { data, _, error in
            if let error = error {
                completion(.failure(error))
            }

            guard let data = data else {
                return completion(.failure(NilError()))
            }
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase

            do {
                let configuration = try decoder.decode(ApplicationResource.self, from: data)
                completion(.success(configuration))
            } catch {
                completion(.failure(error))
            }
        }.resume()
    }

    private static func getAllPrograms(
        url: URL,
        completion: @escaping (Result<ProgramsResource, Error>) -> Void
    ) {
        struct NilError: Error {}

        URLSession.shared.dataTask(with: url) { data, _, error in
            if let error = error {
                completion(.failure(error))
            }

            guard let data = data else {
                return completion(.failure(NilError()))
            }
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase

            do {
                let programs = try decoder.decode(ProgramsResource.self, from: data)
                completion(.success(programs))
            } catch {
                completion(.failure(error))
            }
        }.resume()
    }



    static func getChatRoomsPage(
        baseURL: URL,
        completion: @escaping (Result<ChatRoomResourcePage, Error>) -> Void
    ) {
        guard let clientID = EngagementSDKConfigManager.shared.selectedClientApp?.id,
              let chatRoomsUrlComponents = NSURLComponents(url: baseURL, resolvingAgainstBaseURL: false) else { return }

        chatRoomsUrlComponents.path = "/api/v1/chat-rooms/"
        chatRoomsUrlComponents.queryItems = [
            URLQueryItem(name: "client_id", value: clientID)
        ]

        guard let url = chatRoomsUrlComponents.url else { return }

        getChatRoomsPage(url: url, completion: completion)
    }

    static func getChatRoomsPage(
        url: URL,
        completion: @escaping (Result<ChatRoomResourcePage, Error>) -> Void
    ) {
        var request = URLRequest(url: url)
        request.httpMethod = "GET"

        URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                return completion(.failure(error))
            }

            guard response != nil else {
                return
            }

            guard let data = data else {
                return completion(.failure(MyError("Server returned no data")))
            }

            guard let chatRoomResourcePage = try? JSONDecoder().decode(ChatRoomResourcePage.self, from: data) else {
                return completion(.failure(MyError("Data was not ChatRoomResourcePage")))
            }

            completion(.success(chatRoomResourcePage))
        }.resume()
    }

    struct MyError: LocalizedError {
        private let string: String

        init(_ string: String) {
            self.string = string
        }

        var errorDescription: String? {
            return string
        }
    }
}

struct ApplicationResource: Decodable {
    let name: String
    let clientId: String
    let programsUrl: URL
}

struct ProgramsResource: Decodable {
    let results: [ProgramResource]
}

struct ProgramResource: Decodable {
    let id: String
    let title: String
    let streamUrl: String?
}

struct ChatRoomResourcePage: Decodable {
    var results: [ChatRoomResource]
    var next: URL?
    var previous: URL?
}

struct ChatRoomResource: Decodable {
    var id: String
}
