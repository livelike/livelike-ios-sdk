//
//  HomeNavigationController.swift
//  LiveLikeTestApp
//
//  Created by Heinrich Dahms on 2019-03-05.
//

import UIKit

class PortraitNavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}
