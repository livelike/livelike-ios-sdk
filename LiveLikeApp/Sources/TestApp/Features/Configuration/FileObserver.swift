//
//  FileObserver.swift
//  LiveLikeDemoApp
//
//  Created by Cory Sullivan on 2019-03-05.
//

import UIKit

class FileObserver {
    func dispatchSoureForFile(at path: String) -> DispatchSourceFileSystemObject? {
        let fileHandle = open(path.cString(using: .utf8)!, O_RDONLY)

        // Cannot open file
        guard fileHandle != -1 else {
            return nil
        }

        return DispatchSource.makeFileSystemObjectSource(fileDescriptor: fileHandle, eventMask: [.delete, .write, .extend], queue: DispatchQueue.global())
    }

    func observeFile(at path: String, result: @escaping (String) -> Void) {
        guard let dispatchSource = dispatchSoureForFile(at: path) else {
            print("Cannot create dispatch source to monitor file at '\(path)'")
            return
        }

        let eventHandler = {
            let event = dispatchSource.data

            // Stop monitoring if file is deleted
            if event == .delete {
                dispatchSource.cancel()
                return
            }

            if let fileContent = try? String(contentsOfFile: path) {
                DispatchQueue.main.async {
                    result(fileContent)
                }
            }
        }

        // When we stop monitoring a vnode, close the file handle
        let cancelHandler = {
            let fileHandle = dispatchSource.handle
            close(Int32(fileHandle))
        }

        dispatchSource.setEventHandler(handler: eventHandler)
        dispatchSource.setRegistrationHandler(handler: eventHandler)
        dispatchSource.setCancelHandler(handler: cancelHandler)
        dispatchSource.resume()
    }
}
