//
//  APIEndpointManager.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 6/20/19.
//

import EngagementSDK
import Foundation

@objc protocol EngagementSDKConfigDelegate: AnyObject {
    func apiEndpointDidChange(apiEnvironment: Environment, clientApp: ClientApp?)
    func selectedProgramDidChange(program: Program?, programNumber: Int)
    func didBeginLoading()
    func didFinishLoading()
}

class EngagementSDKConfigManager {
    private struct UserDefaultKeys {
        static let selectedAPIEnvironment = "EngagementSDKConfigManager.selectedAPIEnvironment"
        static let selectedClientID = "EngagementSDKConfigManager.selectedClientID"
        static let selectedProgram = "EngagementSDKConfigManager.selectedProgram"
        static let selectedProgram2 = "EngagementSDKConfigManager.selectedProgram2"
    }

    static var shared: EngagementSDKConfigManager = EngagementSDKConfigManager()

    var delegates: [WeakBox<EngagementSDKConfigDelegate>] = [WeakBox<EngagementSDKConfigDelegate>]()

    private init() {
        if let environmentInt = UserDefaults.standard.object(forKey: UserDefaultKeys.selectedAPIEnvironment) as? Int, let endpoint = Environment(rawValue: environmentInt) {
            selectedEnvironment = endpoint
        } else {
            selectedEnvironment = .dev
        }

        selectedClientApp = defaultClientApp
        selectedProgram = defaultProgram
        selectedProgram2 = defaultProgram2
    }

    var selectedEnvironment: Environment {
        didSet {
            delegates.forEach { $0.unbox?.didBeginLoading() }
            if let clientApp = defaultClientApp, selectedEnvironment.availableClientApps.contains(where: { $0.id == clientApp.id }) {
                selectedClientApp = clientApp
            } else if let firstClientId = selectedEnvironment.availableClientApps.first {
                selectedClientApp = firstClientId
            } else {
                selectedClientApp = nil
            }
            defaultEnvironment = selectedEnvironment
        }
    }

    var selectedClientApp: ClientApp? {
        didSet {
            if let clientApp = selectedClientApp {
                // try to load the first program
                LiveLikeAPI.getAllPrograms(apiEndpoint: selectedEnvironment.url, clientID: clientApp.id, completion: { programs in
                    if let defaultProgram = self.defaultProgram, programs.contains(where: { $0.id == defaultProgram.id }) {
                        self.selectedProgram = defaultProgram
                    } else if let firstProgram = programs.first {
                        self.selectedProgram = firstProgram
                    } else {
                        self.selectedProgram = nil
                    }

                    if let defaultProgram2 = self.defaultProgram2, programs.contains(where: { $0.id == defaultProgram2.id }) {
                        self.selectedProgram2 = defaultProgram2
                    }

                }, failure: { error in
                    self.selectedProgram = nil
                    self.selectedProgram2 = nil
                    print(error)
                })
            } else {
                // no client app? no program!
                selectedProgram = nil
                selectedProgram2 = nil
            }

            defaultClientApp = selectedClientApp

            delegates.forEach {
                $0.unbox?.apiEndpointDidChange(
                    apiEnvironment: selectedEnvironment,
                    clientApp: selectedClientApp
                )
            }

            delegates.forEach { $0.unbox?.didBeginLoading() }
        }
    }

    var selectedProgram: Program? {
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.delegates.forEach {
                    $0.unbox?.selectedProgramDidChange(program: self.selectedProgram, programNumber: 1)
                }

                self.defaultProgram = self.selectedProgram

                self.delegates.forEach { $0.unbox?.didFinishLoading() }
            }
        }
    }

    var selectedProgram2: Program? {
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.delegates.forEach {
                    $0.unbox?.selectedProgramDidChange(program: self.selectedProgram2, programNumber: 2)
                }

                self.defaultProgram2 = self.selectedProgram2

                self.delegates.forEach { $0.unbox?.didFinishLoading() }
            }

        }
    }

    func getAPIToken(for environment: Environment) -> String? {
        return UserDefaults.standard.string(forKey: environment.apiTokenUserDefaultsKey)
    }

    func setAPIToken(_ token: String, environment: Environment) {
        UserDefaults.standard.set(token, forKey: environment.apiTokenUserDefaultsKey)
    }

    func clearAPIToken(for environment: Environment) {
        UserDefaults.standard.removeObject(forKey: environment.apiTokenUserDefaultsKey)
    }

    // MARK: - Default Selections

    private var defaultEnvironment: Environment {
        get {
            if let environmentInt = UserDefaults.standard.object(forKey: UserDefaultKeys.selectedAPIEnvironment) as? Int, let endpoint = Environment(rawValue: environmentInt) {
                return endpoint
            }
            return .dev
        }
        set(newValue) {
            UserDefaults.standard.set(newValue.rawValue, forKey: UserDefaultKeys.selectedAPIEnvironment)
        }
    }

    private var defaultClientApp: ClientApp? {
        get {
            if let data = UserDefaults.standard.data(forKey: UserDefaultKeys.selectedClientID) {
                return try? PropertyListDecoder().decode(ClientApp.self, from: data)
            }
            return nil
        }
        set(newValue) {
            UserDefaults.standard.set(try? PropertyListEncoder().encode(newValue), forKey: UserDefaultKeys.selectedClientID)
        }
    }

    private var defaultProgram: Program? {
        get {
            if let data = UserDefaults.standard.data(forKey: UserDefaultKeys.selectedProgram) {
                return try? PropertyListDecoder().decode(Program.self, from: data)
            }
            return nil
        }
        set(newValue) {
            UserDefaults.standard.set(try? PropertyListEncoder().encode(newValue), forKey: UserDefaultKeys.selectedProgram)
        }
    }

    private var defaultProgram2: Program? {
        get {
            if let data = UserDefaults.standard.data(forKey: UserDefaultKeys.selectedProgram2) {
                return try? PropertyListDecoder().decode(Program.self, from: data)
            }
            return nil
        }
        set(newValue) {
            UserDefaults.standard.set(try? PropertyListEncoder().encode(newValue), forKey: UserDefaultKeys.selectedProgram2)
        }
    }
}

@objc enum Environment: Int, Codable {
    case dev
    case qa
    case qaGameChangers
    case qaIconic
    case qaDig
    case staging
    case prod
    case fiftyfive

    var name: String {
        switch self {
        case .dev:
            return "DEV"
        case .qa:
            return "QA"
        case .qaGameChangers:
            return "QA Game Changers"
        case .qaIconic:
            return "QA Iconic"
        case .qaDig:
            return "QA Dig"
        case .staging:
            return "STAGING"
        case .prod:
            return "PROD"
        case .fiftyfive:
            return "FiftyFive tech"
        }
    }

    var userDefaultKeyForAccessToken: String {
        switch self {
        case .dev: return "EngagementSDKConfigManager.devAccessToken"
        case .qa: return "EngagementSDKConfigManager.qaAccessToken"
        case .qaGameChangers: return "EngagementSDKConfigManager.qaGameChangersAccessToken"
        case .qaIconic: return "EngagementSDKConfigManager.qaIconicAccessToken"
        case .qaDig: return "EngagementSDKConfigManager.qaDigAccessToken"
        case .staging: return "EngagementSDKConfigManager.stagingAccessToken"
        case .prod: return "EngagementSDKConfigManager.prodAccessToken"
        case .fiftyfive: return "EngagementSDKConfigManager.fiftyFiveAccessToken"
        }
    }

    var url: URL {
        switch self {
        case .dev: return URL(string: "https://cf-blast-dev.livelikecdn.com/api/v1/")!
        case .qa: return URL(string: "https://cf-blast-qa.livelikecdn.com/api/v1/")!
        case .qaGameChangers: return URL(string: "https://cf-blast-game-changers.livelikecdn.com/api/v1/")!
        case .qaIconic: return URL(string: "https://cf-blast-iconic.livelikecdn.com/api/v1/")!
        case .qaDig: return URL(string: "https://cf-blast-dig.livelikecdn.com/api/v1/")!
        case .staging: return URL(string: "https://cf-blast-staging.livelikecdn.com/api/v1/")!
        case .prod: return URL(string: "https://cf-blast.livelikecdn.com/api/v1/")!
        case .fiftyfive: return URL(string: "https://cf-blast-fifty-five.livelikecdn.com/api/v1/")!
        }
    }

    var availableClientApps: [ClientApp] {
        switch self {
        case .dev:
            return [ClientApp(id: "UEvoLS228uG6sazor9oeX29IJ3zPwVZDZ2bdCGW2", name: "PubNub Chat Dev")]
        case .qa:
            return [ClientApp(id: "pnODbVXg0UI80s0l2aH5Y7FOuGbftoAdSNqpdvo6", name: "PubNub Chat QA")]
        case .qaGameChangers: return [ClientApp(id: "GaEBcpVrCxiJOSNu4bvX6krEaguxHR9Hlp63tK6L", name: "Test App")]
        case .qaIconic: return [
                ClientApp(id: "LT9lUmrzSqXvAL66rSWSGK0weclpFNbHANUTxW9O", name: "Test App"),
                ClientApp(id: "ImtYYfpppCFzxAgFpGrV7vnm3l1SAf7QymNL69M8", name: "No PubNub")
            ]
        case .qaDig: return [
                ClientApp(id: "lom9db0XtQUhOZQq1vz8QPfSpiyyxppiUVGMcAje", name: "Test App"),
                ClientApp(id: "FCI8JDgn3Ej86lKJRIL0XTF891Da6pIDwiPz1nrF", name: "No Pubnub")
            ]
        case .staging:
            return [ClientApp(id: "vLgjH7dF0uX4J4FQJK3ncMkVmsCdLWhJ0qPtsbk7", name: "PubNub Chat")]
        case .prod:
            return [
                ClientApp(id: "8PqSNDgIVHnXuJuGte1HdvOjOqhCFE1ZCR3qhqaS", name: "PubNub Chat Prod"),
                ClientApp(id: "skoaC6EnpAJNws4HVVD2WrNcLECxQ2TSdgw3WpnM", name: "No PubNub")
            ]
        case .fiftyfive:
            return [ClientApp(id: "bw9BhiwyazS45d9Nt6XvXyE3rA5dumhSpeVpYJZc", name: "FiftyFive Test App")]
        }
    }

    var apiTokenUserDefaultsKey: String {
        switch self {
        case .dev:
            return "EngagementSDK.TestApp.devAPIToken"
        case .qa:
            return "EngagementSDK.TestApp.qaAPIToken"
        case .qaGameChangers:
            return "EngagementSDK.TestApp.qaGameChangersAPIToken"
        case .qaIconic:
            return "EngagementSDK.TestApp.qaIconicAPIToken"
        case .qaDig:
            return "EngagementSDK.TestApp.qaDigAPIToken"
        case .staging:
            return "EngagementSDK.TestApp.stagingAPIToken"
        case .prod:
            return "EngagementSDK.TestApp.prodAPIToken"
        case .fiftyfive:
            return "EngagementSDK.TestApp.fiftyFiveAccessToken"
        }
    }
}

final class WeakBox<T: AnyObject> {
    weak var unbox: T?
    init(_ unbox: T) {
        self.unbox = unbox
    }
}

extension EngagementSDKConfigManager: AccessTokenStorage {
    func fetchAccessToken() -> String? {
        guard let clientApp = selectedClientApp else { return nil }
        let environment = selectedEnvironment
        return UserDefaults.standard.string(forKey: "\(environment.userDefaultKeyForAccessToken).client-\(clientApp.id)")
    }

    func storeAccessToken(accessToken: String) {
        guard let clientApp = selectedClientApp else { return }
        let environment = selectedEnvironment
        UserDefaults.standard.set(accessToken, forKey: "\(environment.userDefaultKeyForAccessToken).client-\(clientApp.id)")
    }
}

@objc public class Program: NSObject, Codable {
    public let id: String
    public let title: String
    public let streamURL: URL?

    init(id: String, title: String, streamURL: URL?) {
        self.id = id
        self.title = title
        self.streamURL = streamURL
    }
}

@objc public class ClientApp: NSObject, Codable {
    public let id: String
    public let name: String

    public init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}
