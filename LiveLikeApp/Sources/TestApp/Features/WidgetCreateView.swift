//
//  WidgetCreateView.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 4/15/20.
//

import UIKit
import LiveLikeWidgetPublish

struct MediaURLs {
    static let sampleVideo: URL = URL(string: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4")!
    static let localVideo: URL = Bundle.main.url(forResource: "sample_ad", withExtension: "mp4")!
}

class WidgetCreateView: UIView {
    struct Section {
        let name: String
        let rows: [Row]
    }

    struct Row {
        let name: String
        let onClick: () -> Void
    }

    let tableView: UITableView = .init()

    let apiToken: String
    let programID: String
    private let apiOrigin: URL

    private let timeoutSeconds: Int
    private var timeoutSecondsISO8601: String {
        return "P0DT00H00M\(timeoutSeconds)S"
    }

    private var randomWidgetTimer: Timer?

    var timestamp: String {
        let dateFormatter = DateFormatter()
        dateFormatter.amSymbol = "am"
        dateFormatter.pmSymbol = "pm"
        dateFormatter.setLocalizedDateFormatFromTemplate("MMM d hh:mm")
        return dateFormatter.string(from: Date())
    }

    init(programID: String, apiToken: String, timeoutSeconds: Int, apiOrigin: URL) {
        self.programID = programID
        self.apiToken = apiToken
        self.timeoutSeconds = timeoutSeconds
        self.apiOrigin = apiOrigin
        super.init(frame: .zero)

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        tableView.delegate = self

        tableView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(tableView)

        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func widgetCreateCompletion<T>(_ result: Result<T, Error>) {
        DispatchQueue.main.sync {
            switch result {
            case let .failure(error):
                log.dev("Error: \(error.localizedDescription)")
                let alert = UIAlertController(
                    title: "Error",
                    message: error.localizedDescription,
                    preferredStyle: .alert
                )
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
            case let .success(string):
                print(string)
            }
        }
    }

    private func predictionCreatedCompletion(_ result: Result<PredictionResult, Error>) {
        DispatchQueue.main.sync {
            switch result {
            case let .failure(error):
                let alert = UIAlertController(
                    title: "Error",
                    message: error.localizedDescription,
                    preferredStyle: .alert
                )
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
            case let .success(predictionResult):
                DispatchQueue.global().asyncAfter(deadline: .now() + .seconds(timeoutSeconds + 10)) {
                    WidgetPublishAPI.setCorrectPredictionOptionAndScheduleFollowUp(
                        predictionResult.payload.options[0],
                        scheduleURL: predictionResult.payload.follow_ups[0].schedule_url,
                        apiToken: self.apiToken,
                        completion: self.widgetCreateCompletion(_:)
                    )
                }
            }
        }
    }

    lazy var sections: [Section] = [

        // MARK: Number Predictions

        Section(name: "Number Predictions", rows: [
            Row(name: "Image 2 Options", onClick: {
                WidgetPublishAPI.createImageNumberPrediction(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateImageNumberPrediction(
                        program_id: self.programID,
                        question: "Question",
                        options: [
                            .init(description: "Option 1", image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!),
                            .init(description: "Option 2 block of text block of text block of text", image_url: URL(string: "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")!),
                        ],
                        timeout: self.timeoutSecondsISO8601,
                        custom_data: nil
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            })
        ]),

        // MARK: Text Quiz

        Section(name: "Text Ask", rows: [
            Row(name: "w/ Long Confirmation Message", onClick: {
                WidgetPublishAPI.createTextAsk(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateTextAsk(
                        program_id: self.programID,
                        title: "AMA With Mike (\(self.timestamp))",
                        prompt: "Submit a question about his coffee empire.",
                        confirmation_message: "Stay Tuned to see if you question was selected! Stay Tuned to see if you question was selected! Stay Tuned to see if you question was selected! Stay Tuned to see if you question was selected! Stay Tuned to see if you question was selected!"
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }),
            Row(name: "w/ Confirmation Message", onClick: {
                WidgetPublishAPI.createTextAsk(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateTextAsk(
                        program_id: self.programID,
                        title: "AMA With Mike (\(self.timestamp))",
                        prompt: "Submit a question about his coffee empire.",
                        confirmation_message: "Stay Tuned to see if you question was selected!"
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }),
            Row(name: "w/o Confirmation Message", onClick: {
                WidgetPublishAPI.createTextAsk(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateTextAsk(
                        program_id: self.programID,
                        title: "AMA With Mike (\(self.timestamp))",
                        prompt: "Submit a question about his coffee empire. (no confirmation_message)",
                        confirmation_message: nil
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            })
        ]),

        Section(name: "Video Alert", rows: [
            Row(name: "Video Only", onClick: {
                WidgetPublishAPI.createVideoAlert(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateVideoAlert(
                        program_id: self.programID,
                        timeout: self.timeoutSecondsISO8601,
                        title: nil,
                        text: nil,
                        video_url: MediaURLs.sampleVideo,
                        link_label: nil,
                        link_url: nil,
                        custom_data: nil
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }),
            Row(name: "Title Only ", onClick: {
                WidgetPublishAPI.createVideoAlert(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateVideoAlert(
                        program_id: self.programID,
                        timeout: self.timeoutSecondsISO8601,
                        title: "Video Alert (\(self.timestamp))",
                        text: nil,
                        video_url: MediaURLs.sampleVideo,
                        link_label: nil,
                        link_url: nil,
                        custom_data: nil
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }),
            Row(name: "Body Only", onClick: {
                WidgetPublishAPI.createVideoAlert(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateVideoAlert(
                        program_id: self.programID,
                        timeout: self.timeoutSecondsISO8601,
                        title: nil,
                        text: "This is some body text. This is more body text.",
                        video_url: MediaURLs.sampleVideo,
                        link_label: nil,
                        link_url: nil,
                        custom_data: nil
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }),
            Row(name: "Link Only", onClick: {
                WidgetPublishAPI.createVideoAlert(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateVideoAlert(
                        program_id: self.programID,
                        timeout: self.timeoutSecondsISO8601,
                        title: nil,
                        text: nil,
                        video_url: MediaURLs.sampleVideo,
                        link_label: "Link Text",
                        link_url: URL(string: "https://livelike.com")!,
                        custom_data: nil
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }),
            Row(name: "Title + Body + URL", onClick: {
                WidgetPublishAPI.createVideoAlert(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateVideoAlert(
                        program_id: self.programID,
                        timeout: self.timeoutSecondsISO8601,
                        title: "Video Alert (\(self.timestamp))",
                        text: "This is some body text. This is more body text.",
                        video_url: MediaURLs.sampleVideo,
                        link_label: "Link Text",
                        link_url: URL(string: "https://livelike.com")!,
                        custom_data: nil
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            })
        ]),

        Section(name: "Text Quiz", rows: [
            Row(name: "2 Option Text Quiz") {
                WidgetPublishAPI.createTextQuiz(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateTextQuiz(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        choices: [
                            CreateTextQuiz.Choice(description: "Correct", is_correct: true),
                            CreateTextQuiz.Choice(description: "Incorrect", is_correct: false)
                        ],
                        timeout: self.timeoutSecondsISO8601,
                        custom_data: "Text Quiz Custom Data"
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            },
            Row(name: "4 Option Text Quiz") {
                WidgetPublishAPI.createTextQuiz(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateTextQuiz(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        choices: [
                            CreateTextQuiz.Choice(description: "Correct", is_correct: true),
                            CreateTextQuiz.Choice(description: "Incorrect", is_correct: false),
                            CreateTextQuiz.Choice(description: "Incorrect", is_correct: false),
                            CreateTextQuiz.Choice(description: "Incorrect", is_correct: false)
                        ],
                        timeout: self.timeoutSecondsISO8601,
                        custom_data: nil
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }
        ]),

        // MARK: Image Quiz

        Section(name: "Image Quiz", rows: [
            Row(name: "2 Options") {
                WidgetPublishAPI.createImageQuiz(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateImageQuiz(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        choices: [
                            .init(description: "Correct", is_correct: true, image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!),
                            .init(description: "Incorrect", is_correct: false, image_url: URL(string: "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")!)
                        ],
                        timeout: self.timeoutSecondsISO8601,
                        custom_data: "Image Quiz Custom Data"
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            },
            Row(name: "3 Options") {
                WidgetPublishAPI.createImageQuiz(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateImageQuiz(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        choices: [
                            .init(description: "Correct", is_correct: true, image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!),
                            .init(description: "Incorrect", is_correct: false, image_url: URL(string: "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")!),
                            .init(description: "Incorrect", is_correct: false, image_url: URL(string: "https://livelike.com/wp-content/uploads/2017/08/Vijay.jpg")!)
                        ],
                        timeout: self.timeoutSecondsISO8601,
                        custom_data: "Image Quiz Custom Data"
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            },
            Row(name: "4 Options") {
                WidgetPublishAPI.createImageQuiz(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateImageQuiz(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        choices: [
                            .init(description: "Correct", is_correct: true, image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!),
                            .init(description: "Incorrect", is_correct: false, image_url: URL(string: "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")!),
                            .init(description: "Incorrect", is_correct: false, image_url: URL(string: "https://livelike.com/wp-content/uploads/2017/08/Vijay.jpg")!),
                            .init(description: "Incorrect", is_correct: false, image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/12/Shivender-Singh.jpg")!)
                        ],
                        timeout: self.timeoutSecondsISO8601
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }
        ]),

        // MARK: Text Poll

        Section(name: "Text Poll", rows: [
            Row(name: "2 Options") {
                WidgetPublishAPI.createTextPoll(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateTextPoll(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        options: [
                            .init(description: "Choice A"),
                            .init(description: "Choice B")
                        ],
                        timeout: self.timeoutSecondsISO8601,
                        custom_data: "Text Poll Custom Data"
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            },
            Row(name: "4 Options") {
                WidgetPublishAPI.createTextPoll(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateTextPoll(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        options: [
                            .init(description: "Choice A"),
                            .init(description: "Choice B"),
                            .init(description: "Choice C"),
                            .init(description: "Choice D")
                        ],
                        timeout: self.timeoutSecondsISO8601
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }
        ]),

        // MARK: Image Poll

        Section(name: "Image Poll", rows: [
            Row(name: "2 Options") {
                WidgetPublishAPI.createImagePoll(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateImagePoll(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        options: [
                            .init(description: "Choice A", image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!),
                            .init(description: "Choice B", image_url: URL(string: "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")!)
                        ],
                        timeout: self.timeoutSecondsISO8601,
                        custom_data: "Image Poll Custom Data"
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            },
            Row(name: "4 Options") {
                WidgetPublishAPI.createImagePoll(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateImagePoll(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        options: [
                            .init(description: "Choice A", image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!),
                            .init(description: "Choice B", image_url: URL(string: "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")!),
                            .init(description: "Choice C", image_url: URL(string: "https://livelike.com/wp-content/uploads/2017/08/Vijay.jpg")!),
                            .init(description: "Choice D", image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/12/Shivender-Singh.jpg")!)
                        ],
                        timeout: self.timeoutSecondsISO8601
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }
        ]),

        // MARK: Text Prediction

        Section(name: "Text Prediction", rows: [
            Row(name: "2 Options", onClick: {
                WidgetPublishAPI.createTextPrediction(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateTextPrediction(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        options: [
                            .init(description: "Choice A"),
                            .init(description: "Choice B")
                        ],
                        timeout: self.timeoutSecondsISO8601,
                        custom_data: "Text Prediction Custom Data"
                    ),
                    completion: { _ in }
                )
            }),
            Row(name: "4 Options", onClick: {
                WidgetPublishAPI.createTextPrediction(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateTextPrediction(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        options: [
                            .init(description: "Choice A"),
                            .init(description: "Choice B"),
                            .init(description: "Choice C"),
                            .init(description: "Choice D"),
                        ],
                        timeout: self.timeoutSecondsISO8601,
                        custom_data: "Text Prediction Custom Data"
                    ),
                    completion: { _ in }
                )
            }),
            Row(name: "2 Options + Follow Up", onClick: {
                WidgetPublishAPI.createTextPrediction(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateTextPrediction(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        options: [
                            .init(description: "Choice A"),
                            .init(description: "Choice B")
                        ],
                        timeout: self.timeoutSecondsISO8601,
                        custom_data: "Text Prediction Custom Data"
                    ),
                    completion: self.predictionCreatedCompletion(_:)
                )
            }),
            Row(name: "4 Options + Follow Up", onClick: {
                WidgetPublishAPI.createTextPrediction(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateTextPrediction(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        options: [
                            .init(description: "Choice A"),
                            .init(description: "Choice B"),
                            .init(description: "Choice C"),
                            .init(description: "Choice D")
                        ],
                        timeout: self.timeoutSecondsISO8601
                    ),
                    completion: self.predictionCreatedCompletion(_:)
                )
            }),
            Row(name: "2 Options + Follow Up (10 minute interactive_until)", onClick: {
                WidgetPublishAPI.createTextPrediction(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateTextPrediction(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        options: [
                            .init(description: "Choice A"),
                            .init(description: "Choice B")
                        ],
                        timeout: self.timeoutSecondsISO8601,
                        custom_data: "Text Prediction Custom Data",
                        interactive_until: ISO8601DateFormatter().string(from: Date().addingTimeInterval(600))
                    ),
                    completion: self.predictionCreatedCompletion(_:)
                )
            }),
        ]),

        // MARK: Image Prediction

        Section(name: "Image Prediction", rows: [
            Row(name: "2 Options") {
                WidgetPublishAPI.createImagePrediction(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateImagePrediction(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        options: [
                            .init(description: "Choice A", image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!),
                            .init(description: "Choice B", image_url: URL(string: "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")!)
                        ],
                        timeout: self.timeoutSecondsISO8601,
                        custom_data: "Image Prediction Custom Data"
                    ),
                    completion: { _ in }
                )
            },
            Row(name: "2 Options + Follow Up") {
                WidgetPublishAPI.createImagePrediction(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateImagePrediction(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        options: [
                            .init(description: "Choice A", image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!),
                            .init(description: "Choice B", image_url: URL(string: "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")!)
                        ],
                        timeout: self.timeoutSecondsISO8601,
                        custom_data: "Image Prediction Custom Data"
                    ),
                    completion: self.predictionCreatedCompletion(_:)
                )
            },
            Row(name: "4 Options + Follow Up") {
                WidgetPublishAPI.createImagePrediction(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateImagePrediction(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        options: [
                            .init(description: "Choice A", image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!),
                            .init(description: "Choice B", image_url: URL(string: "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")!),
                            .init(description: "Choice C", image_url: URL(string: "https://livelike.com/wp-content/uploads/2017/08/Vijay.jpg")!),
                            .init(description: "Choice D", image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/12/Shivender-Singh.jpg")!)
                        ],
                        timeout: self.timeoutSecondsISO8601
                    ),
                    completion: self.predictionCreatedCompletion(_:)
                )
            }
        ]),

        // MARK: Image Slider

        Section(name: "Image Slider", rows: [
            Row(name: "1 Option", onClick: {
                WidgetPublishAPI.createImageSlider(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateImageSlider(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        options: [
                            .init(image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!)
                        ],
                        timeout: self.timeoutSecondsISO8601,
                        custom_data: "Image Slider Metadata"
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }),
            Row(name: "2 Option", onClick: {
                WidgetPublishAPI.createImageSlider(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateImageSlider(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        options: [
                            .init(image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!),
                            .init(image_url: URL(string: "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")!)
                        ],
                        timeout: self.timeoutSecondsISO8601
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }),
            Row(name: "3 Option", onClick: {
                WidgetPublishAPI.createImageSlider(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateImageSlider(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        options: [
                            .init(image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!),
                            .init(image_url: URL(string: "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")!),
                            .init(image_url: URL(string: "https://livelike.com/wp-content/uploads/2017/08/Vijay.jpg")!)
                        ],
                        timeout: self.timeoutSecondsISO8601
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }),
            Row(name: "4 Option", onClick: {
                WidgetPublishAPI.createImageSlider(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateImageSlider(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        options: [
                            .init(image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!),
                            .init(image_url: URL(string: "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")!),
                            .init(image_url: URL(string: "https://livelike.com/wp-content/uploads/2017/08/Vijay.jpg")!),
                            .init(image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/12/Shivender-Singh.jpg")!)
                        ],
                        timeout: self.timeoutSecondsISO8601
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }),
            Row(name: "5 Option", onClick: {
                WidgetPublishAPI.createImageSlider(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateImageSlider(
                        program_id: self.programID,
                        question: "This is a question? (\(self.timestamp))",
                        options: [
                            .init(image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!),
                            .init(image_url: URL(string: "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")!),
                            .init(image_url: URL(string: "https://livelike.com/wp-content/uploads/2017/08/Vijay.jpg")!),
                            .init(image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/12/Shivender-Singh.jpg")!),
                            .init(image_url: URL(string: "https://livelikevr.com/wp-content/uploads/2017/06/Willis.jpg")!)
                        ],
                        timeout: self.timeoutSecondsISO8601
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            })
        ]),

        // MARK: Alerts

        Section(name: "Alerts", rows: [
            Row(name: "Text Only", onClick: {
                WidgetPublishAPI.createAlert(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateAlert(
                        program_id: self.programID,
                        timeout: self.timeoutSecondsISO8601,
                        title: "This is a title. (\(self.timestamp))",
                        text: "Here is some text.",
                        image_url: nil,
                        link_label: nil,
                        link_url: nil,
                        custom_data: "Text Alert Custom Data"
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }),

            Row(name: "Image Only", onClick: {
                WidgetPublishAPI.createAlert(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateAlert(
                        program_id: self.programID,
                        timeout: self.timeoutSecondsISO8601,
                        title: "This is a title. (\(self.timestamp))",
                        text: nil,
                        image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!,
                        link_label: nil,
                        link_url: nil
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }),

            Row(name: "Text And Image", onClick: {
                WidgetPublishAPI.createAlert(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateAlert(
                        program_id: self.programID,
                        timeout: self.timeoutSecondsISO8601,
                        title: "This is a title. (\(self.timestamp))",
                        text: "Here is some text.",
                        image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!,
                        link_label: nil,
                        link_url: nil
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }),

            Row(name: "Text, Image, and URL", onClick: {
                WidgetPublishAPI.createAlert(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateAlert(
                        program_id: self.programID,
                        timeout: self.timeoutSecondsISO8601,
                        title: "This is a title. (\(self.timestamp))",
                        text: "Here is some text.",
                        image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!,
                        link_label: "Click here!",
                        link_url: URL(string: "https://livelike.com")
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }),

            Row(name: "Custom", onClick: {
                let alert = UIAlertController(
                    title: "Custom Alert Widget (\(self.timestamp))",
                    message: "Set a title, text, imageURL, and/or linkURL.",
                    preferredStyle: .alert
                )
                alert.addTextField(configurationHandler: { $0.placeholder = "Title" }) // Title
                alert.addTextField(configurationHandler: { $0.placeholder = "Text" }) // Text
                alert.addTextField(configurationHandler: { $0.placeholder = "Image URL" }) // ImageURL
                alert.addTextField(configurationHandler: { $0.placeholder = "Link URL" }) // LinkURL
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                    WidgetPublishAPI.createAlert(
                        baseURL: self.apiOrigin,
                        accessToken: self.apiToken,
                        payload: CreateAlert(
                            program_id: self.programID,
                            timeout: "P0DT00H00M05S",
                            title: alert.textFields?[0].text,
                            text: alert.textFields?[1].text,
                            image_url: URL(alert.textFields?[2].text),
                            link_label: alert.textFields?[3].text,
                            link_url: URL(alert.textFields?[3].text)
                        ),
                        completion: self.widgetCreateCompletion(_:)
                    )
                }))
                UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
            }),

            Row(name: "mailto: Scheme", onClick: {
                WidgetPublishAPI.createAlert(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateAlert(
                        program_id: self.programID,
                        timeout: self.timeoutSecondsISO8601,
                        title: "mailto:",
                        text: "This should open a mail app.",
                        image_url: nil,
                        link_label: "Do it!",
                        link_url: URL(string: "mailto:test@example.com")!
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }),

            Row(name: "Youtube https URL", onClick: {
                WidgetPublishAPI.createAlert(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateAlert(
                        program_id: self.programID,
                        timeout: self.timeoutSecondsISO8601,
                        title: "Youtube",
                        text: "This should open up the youtube app if installed or web page if not installed.",
                        image_url: nil,
                        link_label: "Do it!",
                        link_url: URL(string: "https://www.youtube.com/watch?v=dQw4w9WgXcQ")!
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }),

            Row(name: "Google maps custom URL Scheme", onClick: {
                WidgetPublishAPI.createAlert(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateAlert(
                        program_id: self.programID,
                        timeout: self.timeoutSecondsISO8601,
                        title: "Google Maps",
                        text: "This should open up the google maps app if installed. Does nothing if app is not installed.",
                        image_url: nil,
                        link_label: "Do it!",
                        link_url: URL(string: "comgooglemapsurl://www.google.com/maps/dir/24.848607,46.660478/24.748607,46.760478/24.778607,46.784478")!
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            })
        ]),

        // MARK: Cheer meters

        Section(name: "Cheer Meter", rows: [
            Row(name: "One Option", onClick: {
                WidgetPublishAPI.createCheerMeter(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateCheerMeter(
                        question: "Who is the most handsome iOS engineer? (\(self.timestamp))",
                        cheer_type: "tap",
                        program_id: self.programID,
                        timeout: self.timeoutSecondsISO8601,
                        options: [
                            .init(description: "Mike", image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!)
                        ],
                        custom_data: "Cheer Meter Custom Data"
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }),
            Row(name: "Two Option", onClick: {
                WidgetPublishAPI.createCheerMeter(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateCheerMeter(
                        question: "Who is the most handsome iOS engineer? (\(self.timestamp))",
                        cheer_type: "tap",
                        program_id: self.programID,
                        timeout: self.timeoutSecondsISO8601,
                        options: [
                            .init(description: "Mike", image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!),
                            .init(description: "Jelzon", image_url: URL(string: "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")!)
                        ],
                        custom_data: "Cheer Meter Custom Data"
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }),
            Row(name: "Three Option", onClick: {
                WidgetPublishAPI.createCheerMeter(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateCheerMeter(
                        question: "Who is the most handsome iOS engineer? (\(self.timestamp))",
                        cheer_type: "tap",
                        program_id: self.programID,
                        timeout: self.timeoutSecondsISO8601,
                        options: [
                            .init(description: "Mike", image_url: URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!),
                            .init(description: "Jelzon", image_url: URL(string: "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")!),
                            .init(description: "Victor", image_url: URL(string: "https://ca.slack-edge.com/T04G26FHU-U012MNVGMRQ-8d7434ded7ff-512")!)

                        ],
                        custom_data: "Cheer Meter Custom Data"
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            })
        ]),

        Section(name: "Social Embed", rows: [
            Row(name: "Simple Tweet", onClick: {
                let simpleTweet = URL(string: "https://twitter.com/Dadsaysjokes/status/1358166308370735112")!
                WidgetPublishAPI.createSocialEmbed(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateSocialEmbed(
                        program_id: self.programID,
                        comment: "Simple Tweet at (\(self.timestamp))",
                        items: [CreateSocialEmbed.item(url: simpleTweet.absoluteString)],
                        timeout: self.timeoutSecondsISO8601,
                        custom_data: "Social Embed Custom Data"
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }),
            Row(name: "Picture Tweet", onClick: {
                let simpleTweet = URL(string: "https://twitter.com/KaticCox/status/1358631117532897281")!
                WidgetPublishAPI.createSocialEmbed(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateSocialEmbed(
                        program_id: self.programID,
                        comment: "Picture Tweet at (\(self.timestamp))",
                        items: [CreateSocialEmbed.item(url: simpleTweet.absoluteString)],
                        timeout: self.timeoutSecondsISO8601,
                        custom_data: "Social Embed Custom Data"
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            }),
            Row(name: "YouTube", onClick: {
                let simpleTweet = URL(string: "https://www.youtube.com/watch?v=rQAbkVF0kXM")!
                WidgetPublishAPI.createSocialEmbed(
                    baseURL: self.apiOrigin,
                    accessToken: self.apiToken,
                    payload: CreateSocialEmbed(
                        program_id: self.programID,
                        comment: "Picture Tweet at (\(self.timestamp))",
                        items: [CreateSocialEmbed.item(url: simpleTweet.absoluteString)],
                        timeout: self.timeoutSecondsISO8601,
                        custom_data: "Social Embed Custom Data"
                    ),
                    completion: self.widgetCreateCompletion(_:)
                )
            })
        ])
    ]

    func randomizeWidgets(timeInterval: TimeInterval) {
        let randomSection = Int.random(in: 0 ..< sections.count)
        sections[randomSection].rows[1].onClick()
        randomWidgetTimer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: true) { [weak self] _ in
            guard let self = self else { return }
            let randomSection = Int.random(in: 0 ..< self.sections.count)
            self.sections[randomSection].rows[1].onClick()
        }
    }

    func stopRandomizedWidgets() {
        if let randomWidgetTimer = randomWidgetTimer {
            randomWidgetTimer.invalidate()
        }
    }
}

extension WidgetCreateView: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].rows.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = sections[indexPath.section].rows[indexPath.row].name
        return cell
    }
}

extension WidgetCreateView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        sections[indexPath.section].rows[indexPath.row].onClick()
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].name
    }
}

private extension URL {
    init?(_ optionalString: String?) {
        if let string = optionalString {
            self.init(string: string)
        } else {
            return nil
        }
    }
}
