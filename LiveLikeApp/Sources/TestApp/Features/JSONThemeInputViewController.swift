//
//  JSONThemeInputViewController.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 4/30/20.
//

import EngagementSDK
import UIKit

class JSONThemeInputViewController: UIViewController {
    private let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Paste theme JSON below."
        label.textAlignment = .center
        return label
    }()

    private let textView: UITextView = {
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.backgroundColor = .lightGray
        return textView
    }()

    private let button: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Apply Theme", for: .normal)
        return button
    }()

    var didParseJSONIntoTheme: (Theme) -> Void = { _ in }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white

        button.addTarget(self, action: #selector(applyButtonPressed), for: .touchUpInside)

        view.addSubview(label)
        view.addSubview(textView)
        view.addSubview(button)

        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: view.topAnchor),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            label.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.1),

            button.topAnchor.constraint(equalTo: label.bottomAnchor),
            button.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            button.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            button.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.1),

            textView.topAnchor.constraint(equalTo: button.bottomAnchor),
            textView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            textView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            textView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.8)
        ])
    }

    @objc private func applyButtonPressed() {
        do {
            guard
                textView.text != "",
                let jsonData = textView.text.data(using: .utf8)
            else {
                showAlert("Found no text.")
                return
            }
            let jsonObject = try JSONSerialization.jsonObject(with: jsonData, options: [])
            let theme = try Theme.create(fromJSONObject: jsonObject)
            didParseJSONIntoTheme(theme)
        } catch {
            showAlert(String(describing: error))
        }
    }

    private func showAlert(_ message: String) {
        let alert = UIAlertController(
            title: "Error",
            message: message,
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
