//
//  GroupChatManager.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 11/7/19.
//

import Foundation

let log = Logger.self
class Logger {
    static func dev(_ message: String, function: String = #function, line: Int = #line) {
        #if DEBUG
            print("TESTAPP DEV - \(function):\(line) | \(message)")
        #endif
    }
}

class GroupChatManager {
    static let shared = GroupChatManager()
    private init() {}

    static func getChatRoomIDs(for environment: Environment) -> [String] {
        switch environment {
        case .dev:
            return shared.devChatRoomIDs
        case .qa:
            return shared.qaChatRoomIDs
        case .qaGameChangers:
            return shared.qaGameChangersChatRoomIDs
        case .qaIconic:
            return shared.qaIconicChatRoomIDs
        case .qaDig:
            return shared.qaDigChatRoomIDs
        case .staging:
            return shared.stagingChatRoomIDs
        case .prod:
            return shared.productionChatRoomIDs
        case .fiftyfive:
            return shared.fiftyfiveChatRoomIDs
        }
    }

    var chatRoomIds: [String] {
        return GroupChatManager.getChatRoomIDs(for: EngagementSDKConfigManager.shared.selectedEnvironment)
    }

    private var productionChatRoomIDs = [
        "fba60fb2-bcf2-4d4b-b611-9d95616b2e2e",
        "f05ee348-b8e5-4107-8019-c66fad7054a8",
        "a8410fc1-c00d-4886-b70a-604cf581b1c3",
        "99a3d6c2-ed1b-4893-bfda-5649f2b37ca8",
        "54bdc808-0269-412f-b260-d6393e40c127"
    ]

    private var stagingChatRoomIDs = [
        "e50ee571-7679-4efd-ad0b-e5fa00e38384",
        "a676725e-5824-4464-8056-e65bd358fde7",
        "8c8e7086-09ba-40f9-834f-cddc6d905ee2",
        "f482e9ba-1cd7-49b2-b82a-88e1cbd84d1d",
        "6a516e12-1e07-429c-b8ad-822669d5dc44"
    ]

    private var devChatRoomIDs = [
        "ab7bcdc7-5cc2-4e05-8852-e30c4b5b6e7f",
        "b13697af-7697-4d5f-a9ce-b3aa22c09a21",
        "7d37dd92-3e73-4e48-977a-4494c39fe148",
        "7cbbd3b1-a13b-480e-bfab-a47827ba79dd"
    ]

    private var qaChatRoomIDs = [
        "20679243-9338-4d1c-bdd3-4223bb401aaa",
        "5c15e95c-3903-4771-a6b6-1eaaebd801b8",
        "1a52cf3d-39b5-4257-8691-2f639bfdc89a",
        "23791f45-d0ed-46fe-b7e9-1438493993dd",
        "54c75168-e69a-45b9-bdbe-08a9d7e2a077"
    ]

    private var qaGameChangersChatRoomIDs: [String] = []
    private var qaIconicChatRoomIDs: [String] = []
    private var qaDigChatRoomIDs: [String] = [
        "fc5feb36-6272-4e4b-8daa-5fe987bec9fc",
        "969a3d4d-feb2-4826-a3d4-dd74a8c85cf4",
        "ee831ec4-7d77-424a-aeb1-4a39ac436731",
    ]
    private var fiftyfiveChatRoomIDs: [String] = ["a4a910e8-d3f0-4523-9c71-4daa089453a9"]
}
