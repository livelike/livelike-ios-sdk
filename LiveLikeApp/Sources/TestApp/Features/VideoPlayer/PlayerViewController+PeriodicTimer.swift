//
//  PlayerViewController+PeriodicTimer.swift
//  LiveLikeTestApp
//
//  Created by Heinrich Dahms on 2019-03-05.
//

import AVKit
import UIKit

extension PlayerViewController {
    func addPDTObserver() {
        let time = CMTimeMake(value: 1, timescale: 4)
        timeObserver = avPlayerViewController.player?.addPeriodicTimeObserver(forInterval: time, queue: nil, using: { [weak self] _ in
            guard let date = self?.avPlayerViewController.player?.currentItem?.currentDate() else {
                return
            }
            self?.pdtLabel.text = self?.dateFormatter.string(from: date)
        })
    }

    func removePDTObserver() {
        if let timeObserver = timeObserver {
            avPlayerViewController.player?.removeTimeObserver(timeObserver)
        }
    }
}
