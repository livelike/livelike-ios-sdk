//
//  VideoPlayerViewController.swift
//  LiveLikeDemo
//
//  Created by Heinrich Dahms on 2019-01-20.
//

import AVKit
import EngagementSDK
import UIKit

class PlayerViewController: UIViewController {
    // MARK: Properties

    let adPlayerViewController = AdsPlayer()
    lazy var avPlayerViewController: AVPlayerViewController = {
        let playerViewController = AVPlayerViewController()
        playerViewController.videoGravity = .resizeAspect
        playerViewController.updatesNowPlayingInfoCenter = false
        return playerViewController
    }()

    lazy var pdtLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = UIColor(white: 0, alpha: 0.6)
        label.textColor = UIColor(red: 255, green: 240, blue: 180)
        label.text = "[UNAVAILABLE]"
        label.font = UIFont.systemFont(ofSize: 13)
        return label
    }()

    lazy var versionLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = UIColor(white: 0, alpha: 0.6)
        label.textColor = UIColor(red: 255, green: 240, blue: 180)
        label.text = "SDK Version: \(EngagementSDK.version)"
        label.font = UIFont.systemFont(ofSize: 13)
        return label
    }()

    lazy var dateFormatter: DateFormatter = {
        DateFormatter.currentTimeZoneTime
    }()

    var isAdPlaying: Bool = false
    var timeObserver: Any?

    // MARK: Lifecycle

    deinit {
        removePDTObserver()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupVideo()
        addLabels()
        playVideo()
        addPDTObserver()
    }

    // MARK: View Functions

    private func setupVideo() {
        addChild(viewController: adPlayerViewController, into: view)
        addChild(viewController: avPlayerViewController, into: view)
        if let url = EngagementSDKConfigManager.shared.selectedProgram?.streamURL {
            updatePlayer(with: url)
        }
    }

    private func addLabels() {
        view.addSubview(pdtLabel)
        view.addSubview(versionLabel)
        let constraints = [
            pdtLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            pdtLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            versionLabel.bottomAnchor.constraint(equalTo: pdtLabel.topAnchor),
            versionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ]

        NSLayoutConstraint.activate(constraints)
    }

    func updatePlayer(with url: URL?) {
        removePDTObserver()

        if let url = url {
            avPlayerViewController.player = AVPlayer(url: url)
        } else {
            avPlayerViewController.player = nil
        }

        addPDTObserver()
        avPlayerViewController.player?.play()
    }

    // MARK: Actions

    func playAd() {
        adPlayerViewController.playAd()
    }
}
