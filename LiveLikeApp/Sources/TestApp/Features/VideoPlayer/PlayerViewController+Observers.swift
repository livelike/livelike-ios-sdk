//
//  PlayerViewController+Observers.swift
//  LiveLikeTestApp
//
//  Created by Heinrich Dahms on 2019-03-05.
//

import AVKit
import UIKit

extension PlayerViewController {
    @objc func adStarted() {
        avPlayerViewController.view.isHidden = true
        avPlayerViewController.player?.volume = 0
        isAdPlaying = true
    }

    @objc func adFinished() {
        avPlayerViewController.view.isHidden = false
        avPlayerViewController.player?.volume = 1
        isAdPlaying = false
    }

    @objc func playVideo() {
        if isAdPlaying {
            adPlayerViewController.playAd()
        } else {
            if let livePosition = avPlayerViewController.player?.currentItem?.seekableTimeRanges.last as? CMTimeRange {
                avPlayerViewController.player?.seek(to: CMTimeRangeGetEnd(livePosition))
            }
            avPlayerViewController.player?.play()
        }
    }

    @objc func pauseVideo() {
        if isAdPlaying {
            adPlayerViewController.pauseAd()
        } else {
            avPlayerViewController.player?.pause()
        }
    }
}
