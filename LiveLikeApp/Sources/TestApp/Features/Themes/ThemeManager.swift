//
// Copyright © 2023 LiveLike Inc. All Rights Reserved.
//

import EngagementSDK
import UIKit

class ThemeManager {
    static var shared = ThemeManager()

    private let selectedThemeUserDefaultsKey: String = "LiveLike.TestApp.selectedTheme"

    var themeDefinition: ThemeDefinitions {
        didSet {
            UserDefaults.standard.set(themeDefinition.rawValue, forKey: selectedThemeUserDefaultsKey)
        }
    }

    private init() {
        themeDefinition = ThemeDefinitions(rawValue: UserDefaults.standard.integer(forKey: selectedThemeUserDefaultsKey)) ?? .default
    }

    func shuffleTheme() -> ThemeDefinitions {
        switch themeDefinition {
        case .default:
            themeDefinition = .default
        case .fox:
            themeDefinition = .fox
        case .turner:
            themeDefinition = .turner
        case .uefa:
            themeDefinition = .uefa
        }

        return themeDefinition
    }
}

enum ThemeDefinitions: Int, CaseIterable {
    case `default` = 0
    case fox = 1
    case turner = 2
    case uefa = 3

    var name: String {
        switch self {
        case .default:
            return "Default"
        case .fox:
            return "Fox"
        case .turner:
            return "Turner"
        case .uefa:
            return "UEFA"
        }
    }

    var theme: Theme {
        switch self {
        case .default:
            let defaultTheme = Theme()
            defaultTheme.chatBodyColor = UIColor(red: 36.0 / 256.0, green: 40.0 / 256.0, blue: 44.0 / 256.0, alpha: 1.0)
            defaultTheme.widgetBodyColor = UIColor(red: 0.0 / 256.0, green: 0.0 / 256.0, blue: 0.0 / 256.0, alpha: 0.6)
            defaultTheme.chatInputSendButtonTint = .white
            return defaultTheme
        case .fox:
            let foxTheme: Theme = Theme()
            foxTheme.chatBodyColor = .darkGray
            foxTheme.lottieFilepaths.win = [
                Bundle.main.path(forResource: "confetti_test", ofType: "json")!
            ]
            foxTheme.lottieFilepaths.lose = [
                Bundle.main.path(forResource: "wrong_test", ofType: "json")!
            ]

            foxTheme.quoteMenuBackgroundColor = .orange
            foxTheme.uppercaseTitleText = false
            foxTheme.uppercaseOptionText = true
            foxTheme.selectedOptionBorderWidth = 5.0
            foxTheme.unselectedOptionBorderWidth = 3.0
            foxTheme.selectedOptionTextColor = .orange
            foxTheme.interOptionSpacing = 5.0
            foxTheme.titleBodySpacing = 5.0
            foxTheme.chatMessageTimestampFont = .systemFont(ofSize: 20, weight: .bold)
            foxTheme.chatMessageTimestampTextColor = .red
            foxTheme.reactionsImageHint = nil
            foxTheme.chatStickerKeyboardIconTint = .red
            foxTheme.chatStickerKeyboardIconSelectedTint = .blue
            foxTheme.chatAutocapitalizationType = .sentences
            foxTheme.messageMargins = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 40)
            return foxTheme
        case .turner:
            let theme = Theme()
            theme.chatBodyColor = .white
            theme.chatImageWidth = 40.0
            theme.chatImageHeight = 40.0
            theme.chatImageCornerRadius = 40 / 2

            theme.usernameTextFont = UIFont(name: "RingsideExtraWide-Book", size: 12) ?? theme.usernameTextFont
            theme.usernameTextUppercased = true
            theme.messageTextFont = UIFont(name: "RingsideRegular-Book", size: 14) ?? theme.messageTextFont
            theme.messageTextColor = UIColor(red: 24.0 / 256.0, green: 24.0 / 256.0, blue: 24.0 / 256.0, alpha: 1.0)

            theme.messageDynamicWidth = false
            theme.messageBackgroundColor = .white
            theme.messageSelectedColor = .white

            theme.usernameTextColor = .black
            theme.messageCornerRadius = 0.0
            theme.messagePadding = 4.0
            theme.messageMargins = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
            theme.chatLeadingMargin = 0.0
            theme.chatTrailingMargin = 0.0
            theme.messageTopBorderHeight = 1.0
            theme.messageTopBorderColor = UIColor(red: 240.0 / 256.0, green: 240.0 / 256.0, blue: 240.0 / 256.0, alpha: 1.0)
            theme.messageBottomBorderHeight = 0.0
            theme.messageBottomBorderColor = .clear
            theme.chatReactions.displayCountsColor = UIColor(red: 152.0 / 256.0, green: 152.0 / 256.0, blue: 152.0 / 256.0, alpha: 1.0)

            theme.messageReactionsVerticalOffset = -16.0
            theme.messageReactionsVerticalAlignment = .bottom
            theme.messageReactionsSpaceBetweenIcons = 2.0
            theme.messageReactionsCountLeadingMargin = 3.0
            theme.messageReactionsCountFont = UIFont(name: "RingsideExtraWide-Black", size: 10) ?? theme.messageTextFont

            theme.chatMessageTimestampFont = UIFont(name: "RingsideRegular-Book", size: 10) ?? theme.chatMessageTimestampFont
            theme.chatMessageTimestampTextColor = .black

            theme.reactionsPopupHorizontalAlignment = .right
            theme.reactionsPopupHorizontalOffset = -16.0
            theme.reactionsPopupVerticalAlignment = .bottom
            theme.reactionsPopupBackground = .white
            theme.reactionsPopupSelectedBackground = UIColor(red: 223.0 / 256.0, green: 223.0 / 256.0, blue: 231.0 / 256.0, alpha: 1.0)
            theme.reactionsPopupVerticalOffset = -10.0
            theme.reactionsPopupCountFont = UIFont(name: "RingsideExtraWide-Black", size: 10) ?? theme.messageTextFont
            theme.reactionsImageHint = UIImage(named: "chat_ic_default")

            theme.chatLoadingIndicatorColor = .darkGray
            theme.chatMessageTimestampUppercased = true

            let tetColor: UIColor = .white
            theme.widgetBodyColor = .yellow
            theme.widgets.quiz.body.background = .fill(color: tetColor)
            theme.widgets.poll.body.background = .fill(color: tetColor)
            theme.widgets.prediction.body.background = .fill(color: tetColor)
            theme.widgets.alert.body.background = .fill(color: tetColor)

            let horizontalOffset = -20.0
            let verticalOffset = 10.0

            // using convenience method
            theme.widgets.poll.setImageHorizontalOffset(horizontalOffset: horizontalOffset)
            theme.widgets.poll.setImageVerticalOffset(verticalOffset: verticalOffset)

            theme.widgets.quiz.unselectedOption.imageHorizontalOffset = horizontalOffset
            theme.widgets.quiz.selectedOption.imageHorizontalOffset = horizontalOffset
            theme.widgets.quiz.incorrectOption?.imageHorizontalOffset = horizontalOffset
            theme.widgets.quiz.correctOption?.imageHorizontalOffset = horizontalOffset

            theme.widgets.quiz.unselectedOption.imageVerticalOffset = verticalOffset
            theme.widgets.quiz.selectedOption.imageVerticalOffset = verticalOffset
            theme.widgets.quiz.incorrectOption?.imageVerticalOffset = verticalOffset
            theme.widgets.quiz.correctOption?.imageVerticalOffset = verticalOffset

            theme.widgets.poll.setImageContentMode(contentMode: .scaleAspectFill)
            theme.widgets.quiz.unselectedOption.imageContentMode = .scaleAspectFit

            return theme
        case .uefa:
            let theme = Theme()
            theme.widgetBodyColor = .clear
            theme.widgetCornerRadius = 6
            theme.unselectedOptionCornerRadius = 6
            theme.widgetFontPrimaryColor = .white
            theme.widgetFontSecondaryColor = .white
            theme.widgetFontTertiaryColor = .white

            theme.titleBodySpacing = 5
            theme.interOptionSpacing = 5
            theme.uppercaseTitleText = false
            theme.uppercaseOptionText = true
            theme.selectedOptionBorderWidth = 3
            theme.unselectedOptionBorderWidth = 3

            theme.fontSecondary = UIFont.systemFont(ofSize: 25)
            theme.fontTertiary = UIFont.systemFont(ofSize: 20)

            theme.widgets.poll.header.background = .fill(color: .clear)
            theme.widgets.poll.selectedOption.container.borderColor = .white
            theme.widgets.poll.selectedOption.progressBar.background = .fill(color: .clear)
            theme.widgets.poll.correctOption?.container.borderColor = UIColor(red: 0, green: 200, blue: 40, alpha: 1)
            theme.widgets.poll.correctOption?.progressBar.background = .fill(color: .clear)
            theme.widgets.poll.incorrectOption?.container.borderColor = UIColor(red: 240, green: 0, blue: 40, alpha: 1)
            theme.widgets.poll.incorrectOption?.progressBar.background = .fill(color: .clear)

            theme.widgets.quiz.header.background = .fill(color: .clear)
            theme.widgets.quiz.selectedOption.container.borderColor = .white
            theme.widgets.quiz.selectedOption.progressBar.background = .fill(color: .clear)
            theme.widgets.quiz.correctOption?.container.borderColor = UIColor(red: 0, green: 200, blue: 40, alpha: 1)
            theme.widgets.quiz.correctOption?.progressBar.background = .fill(color: .clear)
            theme.widgets.quiz.incorrectOption?.container.borderColor = UIColor(red: 240, green: 0, blue: 40, alpha: 1)
            theme.widgets.quiz.incorrectOption?.progressBar.background = .fill(color: .clear)

            theme.widgets.prediction.header.background = .fill(color: .clear)
            theme.widgets.prediction.selectedOption.container.borderColor = .white
            theme.widgets.prediction.selectedOption.progressBar.background = .fill(color: .clear)
            theme.widgets.prediction.correctOption?.container.borderColor = UIColor(red: 0, green: 200, blue: 40, alpha: 1)
            theme.widgets.prediction.correctOption?.progressBar.background = .fill(color: .clear)
            theme.widgets.prediction.incorrectOption?.container.borderColor = UIColor(red: 240, green: 0, blue: 40, alpha: 1)
            theme.widgets.prediction.incorrectOption?.progressBar.background = .fill(color: .clear)

            theme.widgets.alert.header.background = .fill(color: .clear)

            theme.cheerMeter.titleBackgroundColor = .clear
            theme.cheerMeter.tutorialBackgroundColor = .clear

            theme.choiceWidgetTitleMargins.left = 0
            theme.cheerMeter.titleMargins.left = 0

            return theme
        }
    }
}
