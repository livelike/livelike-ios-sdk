//
//  WidgetConfigManager.swift
//  LiveLikeApp
//
//  Created by Jelzon Monzon on 11/5/19.
//

import EngagementSDK
import Foundation

class WidgetConfigManager {
    public static let shared: WidgetConfigManager = WidgetConfigManager()

    private var usingDefault = true

    private let config1: WidgetConfig = WidgetConfig()
    private let config2: WidgetConfig = {
        let config = WidgetConfig()
        config.isAutoDismissEnabled = false
        config.isManualDismissButtonEnabled = false
        config.isSwipeGestureEnabled = false
        config.isWidgetAnimationsEnabled = false
        return config
    }()

    private(set) var currentConfig: WidgetConfig

    init() {
        currentConfig = config1
    }

    public func toggleWidgetConfig() {
        if usingDefault {
            currentConfig = config2
            usingDefault = false
            print("Using config2")
        } else {
            currentConfig = config1
            usingDefault = true
            print("Using config1")
        }
    }
}
