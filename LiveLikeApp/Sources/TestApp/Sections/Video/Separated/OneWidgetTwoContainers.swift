//
//  OneWidgetTwoContainers.swift
//  LiveLikeDemoApp
//
//  Created by Mike Moloksher on 11/20/19.
//

import AVKit
import EngagementSDK
import UIKit

class OneWidgetTwoContainers: UIViewController {
    // MARK: EngagementSDK Properties

    private var videoAndWidgetsSession: ContentSession?
    private let widgetViewController = WidgetPopupViewController()

    private var currentTheme: Theme = Theme()

    // MARK: ViewController Properties

    private var isLandscapeMode: Bool {
        if UIScreen.main.bounds.size.width > UIScreen.main.bounds.size.height {
            return true
        }
        return false
    }

    // MARK: - UI Elements

    private var widgetViewPortrait: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        return view
    }()

    private var widgetViewLandscape: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .green
        return view
    }()

    let backButton: UIButton = {
        let backButton = UIButton(frame: .zero)
        backButton.setTitle("<-: \(EngagementSDKConfigManager.shared.selectedProgram?.title ?? "[NONE]")/\(EngagementSDKConfigManager.shared.selectedProgram2?.title ?? "[NONE]")", for: .normal)
        backButton.translatesAutoresizingMaskIntoConstraints = false
        backButton.addTarget(self, action: #selector(backButtonAction(sender:)), for: .touchUpInside)
        backButton.setTitleColor(.green, for: .normal)
        return backButton
    }()

    var widgetSessionLabel: UILabel = {
        let widgetSessionLabel = UILabel(frame: .zero)
        widgetSessionLabel.text = "Widgets: \(EngagementSDKConfigManager.shared.selectedProgram?.title ?? "[NONE]")"
        widgetSessionLabel.textColor = .green
        return widgetSessionLabel
    }()

    var chatSessionLabel: UILabel = {
        let chatSessionLabel = UILabel(frame: .zero)
        chatSessionLabel.text = "Room ID: program_\(EngagementSDKConfigManager.shared.selectedProgram2?.id ?? "[NONE]")"
        chatSessionLabel.textColor = .green
        chatSessionLabel.adjustsFontSizeToFitWidth = true
        return chatSessionLabel
    }()

    let buttonsStack: UIStackView = {
        let buttonsStack = UIStackView(frame: .zero)
        buttonsStack.translatesAutoresizingMaskIntoConstraints = false
        buttonsStack.axis = .vertical
        buttonsStack.alignment = .leading
        buttonsStack.distribution = .fillEqually
        return buttonsStack
    }()

    // MARK: - UI Setup

    private func setUpViews() {
        view.addSubview(widgetViewPortrait)
        view.addSubview(widgetViewLandscape)

        buttonsStack.addArrangedSubview(backButton)
        buttonsStack.addArrangedSubview(widgetSessionLabel)
        buttonsStack.addArrangedSubview(chatSessionLabel)

        view.addSubview(buttonsStack)
    }

    private func setUpLayout() {
        buttonsStack.topAnchor.constraint(equalTo: view.safeTopAnchor).isActive = true
        buttonsStack.trailingAnchor.constraint(equalTo: view.safeTrailingAnchor).isActive = true
        buttonsStack.leadingAnchor.constraint(equalTo: view.safeLeadingAnchor).isActive = true
        buttonsStack.heightAnchor.constraint(equalToConstant: 100.0).isActive = true

        widgetViewPortrait.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        widgetViewPortrait.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        widgetViewPortrait.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        widgetViewPortrait.heightAnchor.constraint(equalToConstant: view.frame.size.height - 10).isActive = true

        widgetViewLandscape.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        widgetViewLandscape.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        widgetViewLandscape.widthAnchor.constraint(equalToConstant: view.frame.size.height - 10).isActive = true
        widgetViewLandscape.heightAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
    }

    // MARK: - UIViewController Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpEngagementSDK()

        setUpViews()
        setUpLayout()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshOrientationMode(isLandscape: isLandscapeMode)
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        refreshOrientationMode(isLandscape: UIDevice.current.orientation.isLandscape)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    private func refreshOrientationMode(isLandscape: Bool) {
        widgetViewController.view.removeFromSuperview()

        if isLandscape {
            buttonsStack.isHidden = true

            widgetViewLandscape.addSubview(widgetViewController.view)
            widgetViewLandscape.backgroundColor = .gray
            widgetViewLandscape.translatesAutoresizingMaskIntoConstraints = false

            widgetViewLandscape.isHidden = false
            widgetViewPortrait.isHidden = true

            widgetViewController.didMove(toParent: self)

            widgetViewController.view.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                widgetViewController.view.bottomAnchor.constraint(equalTo: widgetViewLandscape.bottomAnchor),
                widgetViewController.view.topAnchor.constraint(equalTo: widgetViewLandscape.topAnchor),
                widgetViewController.view.trailingAnchor.constraint(equalTo: widgetViewLandscape.trailingAnchor),
                widgetViewController.view.leadingAnchor.constraint(equalTo: widgetViewLandscape.leadingAnchor)
            ])

        } else {
            buttonsStack.isHidden = false

            widgetViewPortrait.addSubview(widgetViewController.view)
            widgetViewPortrait.backgroundColor = .lightGray
            widgetViewPortrait.translatesAutoresizingMaskIntoConstraints = false

            widgetViewLandscape.isHidden = true
            widgetViewPortrait.isHidden = false

            widgetViewController.didMove(toParent: self)

            widgetViewController.view.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                widgetViewController.view.bottomAnchor.constraint(equalTo: widgetViewPortrait.bottomAnchor),
                widgetViewController.view.topAnchor.constraint(equalTo: widgetViewPortrait.topAnchor),
                widgetViewController.view.trailingAnchor.constraint(equalTo: widgetViewPortrait.trailingAnchor),
                widgetViewController.view.leadingAnchor.constraint(equalTo: widgetViewPortrait.leadingAnchor)
            ])
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: - UIButton Actions

    @objc func backButtonAction(sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    private func presentErrorAlert(for errorMessage: String) {
        let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))

        present(alert, animated: true, completion: nil)
    }
}

// MARK: - Engagement SDK Functionality

extension OneWidgetTwoContainers {
    private func setUpEngagementSDKThemes() {
        currentTheme = ThemeManager.shared.themeDefinition.theme
        widgetViewController.setTheme(currentTheme)
        view.backgroundColor = currentTheme.chatBodyColor
    }

    private func setUpEngagementSDK() {
        setUpEngagementSDKThemes()

        // Add widgetViewController as child view controller
        addChild(widgetViewController)

        if let clientApp = EngagementSDKConfigManager.shared.selectedClientApp {
            let configManager = EngagementSDKConfigManager.shared
            let environment = configManager.selectedEnvironment
            var config = EngagementSDKConfig(clientID: clientApp.id)
            config.accessTokenStorage = configManager
            config.apiOrigin = environment.url
            let sdk = EngagementSDK(config: config)

            if let program1 = EngagementSDKConfigManager.shared.selectedProgram {
                let dismissOnViewDidDisappear: WidgetConfig = {
                    let dismissOnViewDidDisappear = WidgetConfig()
                    dismissOnViewDidDisappear.isWidgetDismissedOnViewDisappear = false
                    return dismissOnViewDidDisappear
                }()
                let config = SessionConfiguration(
                    programID: program1.id,
                    chatHistoryLimit: 50,
                    widgetConfig: dismissOnViewDidDisappear
                )
                videoAndWidgetsSession = sdk.contentSession(config: config)
                widgetViewController.session = videoAndWidgetsSession
                videoAndWidgetsSession?.delegate = self
            }
        }
    }

    /// Per design landscape chatview needs to be transparent and when returning to portrait
    /// mode, we need to resume the previous background color
    private func enableLandscapeChatView(isLandscape: Bool) {
        if isLandscape {
            currentTheme = .overlay
        } else {
            currentTheme = ThemeManager.shared.themeDefinition.theme
        }
    }

    /// Used for demo purposes to toggle themes and show case theme switching
    func toggleThemes() {
        currentTheme = ThemeManager.shared.shuffleTheme().theme
        widgetViewController.setTheme(currentTheme)
        view.backgroundColor = currentTheme.chatBodyColor
        print("Switch to color: \(currentTheme.chatBodyColor)")
    }
}

// MARK: - ContentSessionDelegate

extension OneWidgetTwoContainers: ContentSessionDelegate {
    func playheadTimeSource(_ session: ContentSession) -> Date? {
        return Date()
    }

    func chat(session: ContentSession, roomID: String, newMessage message: ChatMessage) {}

    func widget(_ session: ContentSession, didBecomeReady jsonObject: Any) {}

    func widget(_ session: ContentSession, didBecomeReady widget: Widget) {}

    func session(_ session: ContentSession, didChangeStatus status: SessionStatus) {
        print("DEV - Session status did change \(status)")
    }

    func session(_ session: ContentSession, didReceiveError error: Error) {
        print("DEV - Did receive error: \(error)")
    }

    func contentSession(_ session: ContentSession, didReceiveWidget widget: WidgetModel) {}
}
