//
//  PresentationDelegateDemoViewController.swift
//  LiveLikeTestApp
//

import EngagementSDKDemo
import UIKit

class PresentationDelegateDemoViewController: CoreVideoViewController {
    @IBOutlet private(set) var controlsStack: UIStackView!
    @IBOutlet private(set) var chatViewController: ChatViewController!
    @IBOutlet private(set) var widgetViewController: WidgetViewController! {
        didSet {
            widgetViewController?.widgetPresentationDelegate = self
        }
    }

    @IBOutlet private(set) var widgetSlidingConstraint: NSLayoutConstraint!

    @IBAction func dismissWidgetButtonPressed(_ sender: Any) {
        widgetViewController?.dismissWidget(direction: .up)
    }

    private var shouldDeferWidgets = false

    private var deferredWidgetToast: UIView?

    override func setProgram(programID: String) {
        super.setProgram(programID: programID)
        session?.install(plugin: WidgetToggle(toggleParentView: chatViewController.view))
        chatViewController.session = session
        widgetViewController.session = session
    }
}

// MARK: - View Lifecycle

extension PresentationDelegateDemoViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        updateLayout()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let widgetViewController = segue.destination as? WidgetViewController {
            self.widgetViewController = widgetViewController
        }

        if let chatViewController = segue.destination as? ChatViewController {
            self.chatViewController = chatViewController
        }
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        updateLayout()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        updateLayout(for: size)
    }
}

// MARK: - WidgetPresentationDelegate

extension PresentationDelegateDemoViewController: WidgetPresentationDelegate {
    func widgetDidChangeState(widget: WidgetViewModel, state: WidgetState) {}

    func didPresent(widget: WidgetViewModel, in view: UIView) {}
    func didBeginInteraction(widget: WidgetViewModel) {}
    func didEndInteraction(widget: WidgetViewModel) {}

    func shouldPresent(widget: WidgetViewModel) -> WidgetPresentationDecision {
        if shouldDeferWidgets {
            showToast(for: widget)
            return .defer
        } else {
            return .present
        }
    }

    func willPresent(widget: WidgetViewModel, in view: UIView) {
        UIView.animate(withDuration: 0.33) { [weak self] in
            guard let self = self else { return }
            self.widgetSlidingConstraint.constant = widget.height
            self.view.layoutIfNeeded()
        }
    }

    func willDismiss(widget: WidgetViewModel, in view: UIView, reason: WidgetDismissReason) {
        UIView.animate(withDuration: 0.33) { [weak self] in
            guard let self = self else { return }
            self.widgetSlidingConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }

    func didDismiss(widget: WidgetViewModel, reason: WidgetDismissReason) {
        if let deferredWidget = widgetViewController.nextDeferredWidget {
            showToast(for: deferredWidget)
        }
    }
}

// MARK: - Private APIs

private extension PresentationDelegateDemoViewController {
    func showToast(for widget: WidgetViewModel) {
        guard
            deferredWidgetToast == nil,
            let parent = widgetViewController.view
        else {
            return
        }

        let present = constraintBased(UIButton(type: .system))
        present.setTitle(widget.kind.displayName, for: .normal)
        present.addTarget(self, action: #selector(presentDeferredWidget(gesture:)), for: .touchUpInside)

        let discard = constraintBased(UIButton(type: .system))
        discard.setTitle("X", for: .normal)
        discard.addTarget(self, action: #selector(discardDeferredWidget), for: .touchUpInside)

        let stack = constraintBased(UIStackView(arrangedSubviews: [present, discard]))
        stack.axis = .horizontal
        stack.spacing = 5
        stack.layer.cornerRadius = 5
        stack.backgroundColor = .blue

        parent.addSubview(stack)

        let topConstraint = stack.topAnchor.constraint(equalTo: parent.safeTopAnchor)
        NSLayoutConstraint.activate([
            topConstraint,
            stack.centerXAnchor.constraint(equalTo: parent.centerXAnchor)
        ])

        topConstraint.constant = -present.frame.height

        deferredWidgetToast = stack

        UIView.animate(withDuration: 1.0) {
            topConstraint.constant = 0
            parent.layoutIfNeeded()
        }
    }

    private func dismissToast(duration: TimeInterval = 0.33, completion: @escaping () -> Void) {
        guard let toastView = deferredWidgetToast else {
            fatalError("Do not call dismissToast when no toast is presented")
        }

        let animations = {
            toastView.alpha = 0.0
        }
        let completion: (Bool) -> Void = { [weak self] _ in
            if let self = self {
                self.deferredWidgetToast = nil
            }
            toastView.removeFromSuperview()
            completion()
        }
        UIView.animate(
            withDuration: duration,
            animations: animations,
            completion: completion
        )
    }

    @objc private func presentDeferredWidget(gesture: UIGestureRecognizer) {
        dismissToast { [weak self] in
            guard let self = self else { return }
            self.widgetViewController.presentDeferredWidget()
        }
    }

    @objc private func discardDeferredWidget() {
        dismissToast { [weak self] in
            guard let self = self else { return }

            self.widgetViewController.discardDeferredWidget()

            if let widget = self.widgetViewController.nextDeferredWidget {
                self.showToast(for: widget)
            }
        }
    }

    private func updateLayout(for size: CGSize? = nil) {
        let size = size ?? view.bounds.size

        let isLandscape = size.width > size.height
        shouldDeferWidgets = isLandscape

        if isLandscape {
            chatViewController.view!.isHidden = true
            view.bringSubviewToFront(widgetViewController.view.superview!)
            NSLayoutConstraint.deactivate(portraitConstraints)
            NSLayoutConstraint.activate(landscapeConstraints)
        } else {
            chatViewController.view!.isHidden = false
            view.sendSubviewToBack(widgetViewController!.view.superview!)
            NSLayoutConstraint.deactivate(landscapeConstraints)
            NSLayoutConstraint.activate(portraitConstraints)

            if case .some = deferredWidgetToast {
                dismissToast(duration: 0) { [weak self] in
                    guard let self = self else { return }
                    if case .some = self.widgetViewController.nextDeferredWidget {
                        self.widgetViewController.presentDeferredWidget()
                    }
                }
            }
        }
    }
}

// MARK: - Private helper

private func constraintBased<View>(_ factory: @autoclosure () -> View) -> View where View: UIView {
    let view = factory()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
}
