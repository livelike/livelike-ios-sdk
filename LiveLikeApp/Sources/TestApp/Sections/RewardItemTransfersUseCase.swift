//
//  RewardItemTransfersUseCase.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 9/30/21.
//

import UIKit
import EngagementSDK

class RewardItemTransfersUseCase: TestCaseViewController {

    private var contentSession: ContentSession!
    private var sdk: EngagementSDK
    private var items: [RewardItemTransfer] = []

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.allowsSelection = true
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "myCell")
        return tableView
    }()

    private let loadMoreButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Load More", for: .normal)
        return button
    }()

    private let buttonStack: UIStackView = {
        let buttonStack = UIStackView()
        buttonStack.translatesAutoresizingMaskIntoConstraints = false
        buttonStack.axis = .horizontal
        buttonStack.distribution = .fillEqually
        return buttonStack
    }()

    private let receivedButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Received Filter", for: .normal)
        return button
    }()

    private let sentButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Sent Filter", for: .normal)
        return button
    }()

    private let noFilterButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("No Filter", for: .normal)
        return button
    }()

    init(sdk: EngagementSDK, programID: String) {
        self.sdk = sdk
        self.contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "Reward Item Transfers"

        super.viewDidLoad()

        buttonStack.addArrangedSubview(noFilterButton)
        buttonStack.addArrangedSubview(receivedButton)
        buttonStack.addArrangedSubview(sentButton)

        view.addSubview(tableView)
        view.addSubview(loadMoreButton)
        view.addSubview(buttonStack)

        NSLayoutConstraint.activate([
            buttonStack.topAnchor.constraint(equalTo: contentView.topAnchor),
            buttonStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            buttonStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            buttonStack.heightAnchor.constraint(equalToConstant: 50),

            tableView.topAnchor.constraint(equalTo: buttonStack.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: loadMoreButton.topAnchor),

            loadMoreButton.topAnchor.constraint(equalTo: tableView.bottomAnchor),
            loadMoreButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            loadMoreButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            loadMoreButton.heightAnchor.constraint(equalToConstant: 60.0),
            loadMoreButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])

        loadMoreButton.addTarget(self, action: #selector(loadMoreButtonSelected), for: .touchUpInside)
        noFilterButton.addTarget(self, action: #selector(resetButtonSelected), for: .touchUpInside)
        sentButton.addTarget(self, action: #selector(sentButtonSelected), for: .touchUpInside)
        receivedButton.addTarget(self, action: #selector(receivedButtonSelected), for: .touchUpInside)

        tableView.delegate = self
        tableView.dataSource = self

        noFilterButton.isSelected = true 
        sdk.rewards.getRewardItemTransfers(
            page: .first
        ) { [weak self] result in

            guard let self = self else { return }

            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(let rewardItems):
                self.items.append(contentsOf: rewardItems)

                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }

            }
        }
    }

    private func resetAllButtons() {
        noFilterButton.isSelected = false
        sentButton.isSelected = false
        receivedButton.isSelected = false
    }

    @objc private func loadMoreButtonSelected() {
        sdk.rewards.getRewardItemTransfers(
            page: .next
        ) { [weak self] result in

            guard let self = self else { return }

            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(let rewardItems):
                self.items.append(contentsOf: rewardItems)

                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }

            }
        }
    }

    @objc private func resetButtonSelected() {
        resetAllButtons()
        noFilterButton.isSelected = true

        sdk.rewards.getRewardItemTransfers(
            page: .first
        ) { [weak self] result in

            guard let self = self else { return }

            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(let rewardItems):
                self.items.append(contentsOf: rewardItems)

                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }

            }
        }
    }

    @objc private func receivedButtonSelected() {
        resetAllButtons()
        receivedButton.isSelected = true

        sdk.rewards.getRewardItemTransfers(
            page: .first,
            options: RewardItemTransferRequestOptions(transferType: .received)
        ) { [weak self] result in

            guard let self = self else { return }

            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(let rewardItems):
                self.items.removeAll()
                self.items.append(contentsOf: rewardItems)

                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }

            }
        }
    }

    @objc private func sentButtonSelected() {
        resetAllButtons()
        sentButton.isSelected = true

        sdk.rewards.getRewardItemTransfers(
            page: .first,
            options: RewardItemTransferRequestOptions(transferType: .sent)
        ) { [weak self] result in

            guard let self = self else { return }

            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(let rewardItems):
                self.items.removeAll()
                self.items.append(contentsOf: rewardItems)

                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }

            }
        }
    }
}

extension RewardItemTransfersUseCase: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "myCell")
        else {
            return UITableViewCell()
        }

        let rewardItem = self.items[indexPath.row]
        cell.textLabel?.text = "id: \(rewardItem.id)"

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
