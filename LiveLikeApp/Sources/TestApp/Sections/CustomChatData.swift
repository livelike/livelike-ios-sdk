//
//  CustomChatData.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 3/17/21.
//

import AVKit
import EngagementSDK
import UIKit

class CustomChatDataUseCaseVC: TestCaseViewController {
    private let sdk: EngagementSDK
    private let envBaseURL: URL
    private var chatSession: ChatSession?
    private let chatViewController = ChatViewController()
    private let avPlayer: AVPlayerViewController = .init()
    private let changeButton = UIButton(type: .system)
    private let sendMessageBTN: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Send Custom Message", for: .normal)
        btn.backgroundColor = .blue
        return btn
    }()

    init(envBaseURL: URL, sdk: EngagementSDK) {
        self.envBaseURL = envBaseURL
        self.sdk = sdk
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        chatViewController.view.translatesAutoresizingMaskIntoConstraints = false
        avPlayer.view.translatesAutoresizingMaskIntoConstraints = false
        changeButton.translatesAutoresizingMaskIntoConstraints = false

        addChild(chatViewController)
        addChild(avPlayer)

        contentView.addSubview(chatViewController.view)
        contentView.addSubview(avPlayer.view)
        contentView.addSubview(changeButton)
        contentView.addSubview(sendMessageBTN)

        NSLayoutConstraint.activate([
            avPlayer.view.topAnchor.constraint(equalTo: contentView.topAnchor),
            avPlayer.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            avPlayer.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            avPlayer.view.heightAnchor.constraint(equalToConstant: 200),

            chatViewController.view.topAnchor.constraint(equalTo: avPlayer.view.bottomAnchor),
            chatViewController.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            chatViewController.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            chatViewController.view.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.5),

            changeButton.topAnchor.constraint(equalTo: chatViewController.view.bottomAnchor),
            changeButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            changeButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            changeButton.heightAnchor.constraint(equalToConstant: 50),

            sendMessageBTN.topAnchor.constraint(equalTo: changeButton.bottomAnchor, constant: 0.0),
            sendMessageBTN.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            sendMessageBTN.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            sendMessageBTN.heightAnchor.constraint(equalToConstant: 50)

        ])

        changeButton.backgroundColor = .lightGray
        changeButton.setTitle("Set Room", for: .normal)
        changeButton.addTarget(self, action: #selector(changeButtonSelected), for: .touchUpInside)

        // Play stream
        avPlayer.player = AVPlayer(url: URL(string: "https://cf-streams.livelikecdn.com/live/colorbars-angle1/index.m3u8")!)
        avPlayer.player?.play()

        sendMessageBTN.addTarget(self, action: #selector(sendCustomMessage), for: .touchUpInside)

        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc private func applicationWillEnterForeground() {
        avPlayer.player?.play()
    }

    @objc private func changeButtonSelected() {
        LiveLikeAPI.getChatRoomsPage(baseURL: envBaseURL) { result in
            self.handleChatRoomsPageResult(result)
        }
    }

    private func handleChatRoomsPageResult(_ result: Result<ChatRoomResourcePage, Error>) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let alert = UIAlertController(
                title: "Select Chat Room",
                message: "Select a chat room to enter.",
                preferredStyle: .actionSheet
            )

            GroupChatManager.shared.chatRoomIds.forEach { chatRoomId in
                alert.addAction(
                    UIAlertAction(
                        title: chatRoomId,
                        style: .default,
                        handler: { _ in self.handleChatRoomSelected(chatRoomId) }
                    )
                )
            }

            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    private func handleChatRoomSelected(_ chatRoomId: String) {
        var config = ChatSessionConfig(roomID: chatRoomId)
        config.syncTimeSource = { [weak self] in
            self?.avPlayer.player?.programDateAndTime?.timeIntervalSince1970
        }

        sdk.connectChatRoom(config: config, completion: { [weak self] result in
            DispatchQueue.main.async {
                guard let self = self else { return }
                switch result {
                case let .success(chatSession):
                    self.chatSession = chatSession
                    self.chatViewController.setChatSession(chatSession)
                    self.chatViewController.shouldDisplayDebugVideoTime = true
                case let .failure(error):
                    print(error)
                }
            }
        })
    }

    @objc func sendCustomMessage() {
        let alert = UIAlertController(
            title: "Pick a message to send",
            message: "",
            preferredStyle: .actionSheet
        )

        alert.addAction(UIAlertAction(title: "Simple Text Message", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            guard let currentChatSession = self.chatSession else { return }

            let chatMessage = NewChatMessage(text: "\(Date()) I love iOS Developers!")

            currentChatSession.sendMessage(chatMessage) { result in

                switch result {
                case let .success(messageID):
                    log.dev("new msg: \(messageID)")
                case let .failure(error):
                    log.dev("\(error)")
                }
            }
        }))

        alert.addAction(UIAlertAction(title: "Image URL Message", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }

            guard let currentChatSession = self.chatSession else { return }
            guard let imageURL = URL(string: "https://media.giphy.com/media/l1KVboXQeiaX7FHgI/giphy.gif") else { return }
            let chatMessage = NewChatMessage(
                imageURL: imageURL,
                imageSize: CGSize(width: 100, height: 100)
            )

            currentChatSession.sendMessage(chatMessage) { result in

                switch result {
                case let .success(messageID):
                    log.dev("new msg: \(messageID)")
                case let .failure(error):
                    log.dev("\(error)")
                }
            }
        }))

        alert.addAction(UIAlertAction(title: "Image Data Message", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }

            guard let currentChatSession = self.chatSession else { return }
            guard let imageData = UIImage(named: "logo_small")?.pngData() else { return }
            let chatMessage = NewChatMessage(
                imageData: imageData,
                imageSize: CGSize(width: 100, height: 100)
            )

            currentChatSession.sendMessage(chatMessage) { result in

                switch result {
                case let .success(messageID):
                    log.dev("new msg: \(messageID)")
                case let .failure(error):
                    log.dev("\(error)")
                }
            }
        }))

        alert.addAction(UIAlertAction(title: "Custom Image Message", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }

            guard let currentChatSession = self.chatSession else { return }

            let alert = UIAlertController(
                title: "Enter Image URL",
                message: "",
                preferredStyle: .alert
            )
            alert.addTextField(configurationHandler: nil)
            alert.addAction(
                UIAlertAction(title: "Set", style: .default, handler: { _ in
                    guard let imgString = alert.textFields?[0].text,
                          let imgURL = URL(string: imgString) else { return }

                    let chatMessage = NewChatMessage(
                        imageURL: imgURL,
                        imageSize: CGSize(width: 50, height: 50)
                    )

                    currentChatSession.sendMessage(chatMessage) { result in

                        switch result {
                        case let .success(messageID):
                            log.dev("new msg: \(messageID)")
                        case let .failure(error):
                            log.dev("\(error)")
                        }
                    }
                })
            )

            self.present(alert, animated: true, completion: nil)

        }))

        alert.addAction(UIAlertAction(title: "Text Message w/ Stickers", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }

            guard let currentChatSession = self.chatSession else { return }
            let chatMessage = NewChatMessage(
                text: "\(Date()) :raptors2::raptors4:"
            )

            currentChatSession.sendMessage(chatMessage) { result in

                switch result {
                case let .success(messageID):
                    log.dev("new msg: \(messageID)")
                case let .failure(error):
                    log.dev("\(error)")
                }
            }
        }))

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
