//
//  ChatMessageMetadataTests.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 10/11/22.
//

import EngagementSDK
import UIKit

class ChatMessageMetadataTests: TestCaseViewController {
    
    private let chatSession: ChatSession
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private let textMessageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Send Text Message w/ Metadata"
        return label
    }()
    
    private let imageMessageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Send Image Message w/ Metadata"
        return label
    }()
    
    init(chatSession: ChatSession) {
        self.chatSession = chatSession
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentView.addSubview(stackView)
        stackView.addArrangedSubview(textMessageLabel)
        stackView.addArrangedSubview(imageMessageLabel)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
        ])
        
        let testMessageMetadata: MessageMetadata = [
            "key1": "some-string",
            "key2": 1
        ]
        
        // Send text message
        chatSession.sendMessage(
            NewChatMessage(
                text: "test",
                messageMetadata: testMessageMetadata
            )
        ) { result in
            switch result {
            case .success(let chatMessage):
                guard
                    let messageMetadata = chatMessage.messageMetadata,
                    messageMetadata["key1"] as? String == "some-string",
                    messageMetadata["key2"] as? Int == 1
                else {
                    self.textMessageLabel.backgroundColor = .red
                    return
                }
                self.textMessageLabel.backgroundColor = .green
            case .failure(let error):
                self.textMessageLabel.backgroundColor = .red
                log.dev(error.localizedDescription)
                return
            }
        }
        
        // Send image message
        chatSession.sendMessage(
            NewChatMessage(
                imageURL: URL(string: "https://ca.slack-edge.com/T04G26FHU-U21G96WDA-3a6d4e1e84e0-512")!,
                imageSize: CGSize(width: 100, height: 100),
                messageMetadata: testMessageMetadata
            )
        ) { result in
            switch result {
            case .success(let chatMessage):
                guard
                    let messageMetadata = chatMessage.messageMetadata,
                    messageMetadata["key1"] as? String == "some-string",
                    messageMetadata["key2"] as? Int == 1
                else {
                    self.imageMessageLabel.backgroundColor = .red
                    return
                }
                self.imageMessageLabel.backgroundColor = .green
            case .failure(let error):
                self.imageMessageLabel.backgroundColor = .red
                log.dev(error.localizedDescription)
                return
            }
        }
    }
}
