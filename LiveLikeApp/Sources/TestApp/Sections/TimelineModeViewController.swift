//
//  TimelineModeViewController.swift
//  LiveLikeApp
//
//  Created by Mike Moloksher on 4/16/20.
//

import EngagementSDK
import UIKit

final class TimelineModeViewController: TestCaseViewController {
    private var sdk: EngagementSDK!
    private var session: ContentSession!

    private let clientID: String
    private let programID: String

    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.isHidden = true
        return tableView
    }()

    private let widgetView: UIView = {
        let widgetView = UIView()
        widgetView.translatesAutoresizingMaskIntoConstraints = false
        widgetView.isHidden = true
        return widgetView
    }()

    private var widgets = [Widget]()
    private var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .gray)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.hidesWhenStopped = true
        return indicator
    }()

    private let nextBtn: UIButton = {
        let next = UIButton()
        next.translatesAutoresizingMaskIntoConstraints = false
        next.setTitle("More Widgets", for: .normal)
        next.backgroundColor = .gray
        next.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        next.addTarget(self, action: #selector(nextWidgetPage), for: .touchUpInside)
        return next
    }()

    private let applyWidgetStateBTN: UIButton = {
        let next = UIButton()
        next.translatesAutoresizingMaskIntoConstraints = false
        next.setTitle("Set State", for: .normal)
        next.backgroundColor = .gray
        next.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        next.addTarget(self, action: #selector(applyWidgetState), for: .touchUpInside)
        return next
    }()

    private var widgetStateController = DefaultWidgetStateController(
        closeButtonAction: {},
        widgetFinishedCompletion: { _ in }
    )

    private let widgetStatePicker: UIPickerView = {
        let picker = UIPickerView()
        picker.translatesAutoresizingMaskIntoConstraints = false
        return picker
    }()

    // MARK: - Init

    init(clientID: String, programID: String) {
        self.clientID = clientID
        self.programID = programID
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        vcTitle = "Timeline Mode"
        super.viewDidLoad()

        var config = EngagementSDKConfig(clientID: clientID)
        config.apiOrigin = EngagementSDKConfigManager.shared.selectedEnvironment.url
        config.accessTokenStorage = EngagementSDKConfigManager.shared
        sdk = EngagementSDK(config: config)
        session = sdk.contentSession(config: SessionConfiguration(programID: programID))
        session.delegate = self

        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(WidgetCell.self, forCellReuseIdentifier: "widgetCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension

        view.addSubview(tableView)
        view.addSubview(activityIndicator)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 50),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            activityIndicator.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)
        ])

        view.addSubview(nextBtn)
        NSLayoutConstraint.activate([
            nextBtn.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10.0),
            nextBtn.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 10.0),
            nextBtn.widthAnchor.constraint(equalToConstant: 150.0),
            nextBtn.heightAnchor.constraint(equalToConstant: 44.0)
        ])

        widgetStatePicker.delegate = self
        widgetStatePicker.dataSource = self
        view.addSubview(widgetStatePicker)
        NSLayoutConstraint.activate([
            widgetStatePicker.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0),
            widgetStatePicker.widthAnchor.constraint(equalToConstant: 125.0),
            widgetStatePicker.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0.0),
            widgetStatePicker.heightAnchor.constraint(equalToConstant: 44)
        ])

        view.addSubview(applyWidgetStateBTN)
        NSLayoutConstraint.activate([
            applyWidgetStateBTN.topAnchor.constraint(equalTo: widgetStatePicker.topAnchor, constant: 10.0),
            applyWidgetStateBTN.leadingAnchor.constraint(equalTo: widgetStatePicker.trailingAnchor, constant: 5.0),
            applyWidgetStateBTN.widthAnchor.constraint(equalToConstant: 85.0),
            applyWidgetStateBTN.heightAnchor.constraint(equalToConstant: 44.0)
        ])

        view.addSubview(widgetView)
        NSLayoutConstraint.activate([
            widgetView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 50),
            widgetView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            widgetView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10)
        ])

        activityIndicator.startAnimating()

        session.getPostedWidgets(page: .first) { [weak self] result in
            switch result {
            case let .success(widgets):
                DispatchQueue.main.async {
                    if let widgets = widgets {
                        self?.widgets.append(contentsOf: widgets)
                        self?.tableView.reloadData()
                        self?.tableView.isHidden = false
                    }
                    self?.activityIndicator.stopAnimating()
                }
            case let .failure(error):
                log.dev("Error: \(error)")
            }
        }

        widgetStateController = DefaultWidgetStateController(
            closeButtonAction: {},
            widgetFinishedCompletion: { [weak self] _ in
                self?.widgetView.isHidden = true
                self?.tableView.reloadData()
            }
        )
    }

    @objc private func nextWidgetPage() {
        activityIndicator.startAnimating()
        session.getPostedWidgets(page: .next) { [weak self] result in
            switch result {
            case let .success(widgets):
                if let widgets = widgets {
                    DispatchQueue.main.async {
                        self?.widgets.append(contentsOf: widgets)
                        self?.tableView.reloadData()
                    }
                }

                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                }
            case let .failure(error):
                log.dev("Error: \(error)")
            }
        }
    }

    @objc private func applyWidgetState() {
        tableView.reloadData()
    }
}

extension TimelineModeViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return widgets.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "widgetCell", for: indexPath) as? WidgetCell else { return UITableViewCell() }

        let widget = widgets[indexPath.row]
        let selectedRow = widgetStatePicker.selectedRow(inComponent: 0)
        widget.currentState = WidgetState.allCases[selectedRow]
        cell.configureCell(widget: widget, index: indexPath)
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let widget = widgets[indexPath.row]

        if widget.currentState != .interacting {
            let dateFormatter = DateFormatter()
            dateFormatter.amSymbol = "am"
            dateFormatter.pmSymbol = "pm"
            dateFormatter.setLocalizedDateFormatFromTemplate("MMM d hh:mm:ss")
            let formattedDate = dateFormatter.string(from: widget.publishedAt!)
            let created = dateFormatter.string(from: widget.createdAt)

            let alert = UIAlertController(
                title: "Widget Info",
                message: "Published: \(formattedDate)\nCreated: \(created)",
                preferredStyle: .alert
            )

            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }
}

extension TimelineModeViewController: ContentSessionDelegate {
    func widget(_ session: ContentSession, didBecomeReady jsonObject: Any) {}

    func widget(_ session: ContentSession, didBecomeReady widget: Widget) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }

            self.widgetView.isHidden = false
            self.widgets.insert(widget, at: 0)

            self.addChild(widget)

            widget.view.translatesAutoresizingMaskIntoConstraints = false
            self.widgetView.addSubview(widget.view)
            NSLayoutConstraint.activate([
                widget.view.topAnchor.constraint(equalTo: self.widgetView.topAnchor),
                widget.view.leadingAnchor.constraint(equalTo: self.widgetView.leadingAnchor),
                widget.view.trailingAnchor.constraint(equalTo: self.widgetView.trailingAnchor),
                widget.view.bottomAnchor.constraint(equalTo: self.widgetView.bottomAnchor, constant: 50)
            ])

            widget.didMove(toParent: self)
            widget.delegate = self.widgetStateController
            widget.moveToNextState()
        }
    }

    func playheadTimeSource(_ session: ContentSession) -> Date? {
        return nil
    }

    func session(_ session: ContentSession, didChangeStatus status: SessionStatus) {}

    func session(_ session: ContentSession, didReceiveError error: Error) {}

    func chat(session: ContentSession, roomID: String, newMessage message: ChatMessage) {}

    func contentSession(_ session: ContentSession, didReceiveWidget widget: WidgetModel) {}
}

// MARK: - WidgetCell

class WidgetCell: UITableViewCell {
    private var cellWidget: Widget?
    private var widgetNumber: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none

        addSubview(widgetNumber)
        NSLayoutConstraint.activate([
            widgetNumber.centerYAnchor.constraint(equalTo: centerYAnchor),
            widgetNumber.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            widgetNumber.widthAnchor.constraint(equalToConstant: 40)
        ])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        cellWidget?.view.removeFromSuperview()
        cellWidget = nil
    }

    func configureCell(widget: Widget, index: IndexPath) {
        cellWidget = widget
        widgetNumber.text = "\(index.row)"
        if let cellWidget = cellWidget {
            cellWidget.view.translatesAutoresizingMaskIntoConstraints = false
            addSubview(cellWidget.view)

            NSLayoutConstraint.activate([
                cellWidget.view.topAnchor.constraint(equalTo: topAnchor, constant: 10),
                cellWidget.view.leadingAnchor.constraint(equalTo: widgetNumber.trailingAnchor, constant: 5),
                cellWidget.view.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
                cellWidget.view.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)
            ])
            layoutIfNeeded()
        }
    }
}

extension TimelineModeViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return WidgetState.allCases.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "Visibility: \(WidgetState.allCases[row])"
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var title = UILabel()
        if let view = view as? UILabel {
            title = view
        }
        title.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold)
        title.textColor = UIColor.blue
        title.text = "State: \(WidgetState.allCases[row])"
        title.textAlignment = .center

        return title
    }
}
