//
//  NoProfileStatusBarChat.swift
//  LiveLikeDemoApp
//
//  Created by Mike Moloksher on 1/23/20.
//

import EngagementSDK
import UIKit

/// Tests the use-case of shouldDisplayProfileStatusBar being disabled
final class NoProfileStatusBarChat: ChatTestCaseViewController {
    override func viewDidLoad() {
        vcTitle = "No Username Bar in Chat"
        super.viewDidLoad()
        chatController.didSendMessage = { [weak self] _ in
            self?.chatController.dismissKeyboard()
        }
    }
}
