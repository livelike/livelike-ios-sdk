//
//  WidgetRewardClaimCenterViewController.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 6/29/21.
//

import UIKit
import EngagementSDK

class WidgetRewardClaimCenterViewController: TestCaseViewController {

    private var contentSession: ContentSession!

    private var claimableWidgetModels: [WidgetInteraction] = []

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.allowsSelection = false
        tableView.register(ClaimWidgetRewardCell.self, forCellReuseIdentifier: "myCell")
        return tableView
    }()

    private let loadMoreButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Load More", for: .normal)
        return button
    }()

    init(sdk: EngagementSDK, programID: String) {
        self.contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        vcTitle = "Widget Reward Claim Center"

        contentSession.getWidgetInteractionWithUnclaimedRewards(page: .first) { result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let widgetInterations):
                log.dev("Found \(widgetInterations.count) widgets with unclaimed rewards.")
                self.claimableWidgetModels.append(contentsOf: widgetInterations)
                self.tableView.reloadData()
            }
        }

        view.addSubview(tableView)
        view.addSubview(loadMoreButton)

        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: contentView.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            tableView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.95),

            loadMoreButton.topAnchor.constraint(equalTo: tableView.bottomAnchor),
            loadMoreButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            loadMoreButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            loadMoreButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])

        loadMoreButton.addTarget(self, action: #selector(loadMoreButtonSelected), for: .touchUpInside)
        tableView.delegate = self
        tableView.dataSource = self
    }

    @objc private func loadMoreButtonSelected() {
        contentSession.getWidgetInteractionWithUnclaimedRewards(page: .next) { result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let widgetInterations):
                log.dev("Found \(widgetInterations.count) widgets with unclaimed rewards.")
                self.claimableWidgetModels.append(contentsOf: widgetInterations)
                self.tableView.reloadData()
            }
        }
    }
}

extension WidgetRewardClaimCenterViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return claimableWidgetModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "myCell") as? ClaimWidgetRewardCell
        else {
            return UITableViewCell()
        }

        let interaction = self.claimableWidgetModels[indexPath.row]
        switch interaction {
        case .predictionVote(let vote):
            contentSession.getWidgetModel(
                byID: interaction.widgetID,
                kind: interaction.widgetKind
            ) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .failure(let error):
                    print(error.localizedDescription)
                case .success(let widgetModel):
                    switch widgetModel {
                    case .prediction(let model):
                        guard let followUp = model.followUpWidgetModels.first else { return }

                        cell.titleLabel.text =
                            """
                            Follow Up \(followUp.id)
                            Question: \(followUp.question)
                            You Answered: \(model.options.first(where: { $0.id == vote.optionID})?.text ?? "")
                            """
                        cell.claimButtonCompletion = {
                            followUp.claimRewards(vote: vote) { result in
                                switch result {
                                case .failure(let error):
                                    log.dev(error.localizedDescription)
                                case .success(let rewards):
                                    DispatchQueue.main.async {
                                        let message: String = rewards.reduce("") { string, reward in
                                            string + "- \(reward.amount) \(reward.item.name)"
                                        }
                                        let alert = UIAlertController(
                                            title: "Rewards Claimed!",
                                            message: message,
                                            preferredStyle: .alert
                                        )
                                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                                        self.present(alert, animated: true, completion: nil)

                                        self.claimableWidgetModels.remove(at: indexPath.row)
                                        self.tableView.reloadData()
                                    }
                                }

                            }
                        }
                    default:
                        break
                    }
                }
            }
        case .numberPredictionVote(let vote):
            contentSession.getWidgetModel(
                byID: interaction.widgetID,
                kind: interaction.widgetKind
            ) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .failure(let error):
                    print(error.localizedDescription)
                case .success(let widgetModel):
                    switch widgetModel {
                    case .numberPrediction(let model):
                        guard let followUp = model.followUpWidgetModels.first else { return }

                        cell.titleLabel.text =
                            """
                            Follow Up \(followUp.id)
                            Question: \(followUp.question)
                            Your Answers: \(vote.votes.map { "\($0.number)" })
                            """
                        cell.claimButtonCompletion = {
                            followUp.claimRewards { result in
                                switch result {
                                case .failure(let error):
                                    log.dev(error.localizedDescription)
                                case .success(let rewards):
                                    DispatchQueue.main.async {
                                        let message: String = rewards.reduce("") { string, reward in
                                            string + "- \(reward.amount) \(reward.item.name)"
                                        }
                                        let alert = UIAlertController(
                                            title: "Rewards Claimed!",
                                            message: message,
                                            preferredStyle: .alert
                                        )
                                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                                        self.present(alert, animated: true, completion: nil)

                                        self.claimableWidgetModels.remove(at: indexPath.row)
                                        self.tableView.reloadData()
                                    }
                                }

                            }

                        }
                    default:
                        break
                    }
                }
            }
        default:
            cell.titleLabel.text = interaction.widgetKind.displayName
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

class ClaimWidgetRewardCell: UITableViewCell {

    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        return stackView
    }()

    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()

    private let claimButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Claim Reward", for: .normal)
        return button
    }()

    var claimButtonCompletion: () -> Void = { }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        claimButton.addTarget(self, action: #selector(claimButtonSelected), for: .touchUpInside)

        contentView.addSubview(stackView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(claimButton)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc private func claimButtonSelected() {
        claimButtonCompletion()
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        self.titleLabel.text = ""
        self.claimButtonCompletion = { }
    }
}
