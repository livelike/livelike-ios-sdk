//
//  PresenceUseCase.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 3/8/23.
//

import Foundation
import EngagementSDK
import UIKit
import LiveLikeSwift

class PresenceUseCase: TestCaseViewController {
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 30
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let buttonStack1: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 5
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()
    
    private let currentProfileIDLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "Current Profile ID"
        return label
    }()
    
    private let currentProfileID: UITextView = {
        let textView = UITextView()
        textView.textColor = .black
        return textView
    }()
    
    private let channelsTextField = UseCaseTextField(placeholder: "channel ids, separated by comma")
    private let attributeKeyTextField = UseCaseTextField(placeholder: "Attribute Key")
    private let attributeValueTextField = UseCaseTextField(placeholder: "Attribute Value")
    private let profileIDTextField = UseCaseTextField(placeholder: "Enter Profile ID for whereNow()")
    
    private let joinChannelsButton = UseCaseButton(label: "Join Channels")
    private let leaveChannelsButton = UseCaseButton(label: "Leave Channels")
    private let subscribeToChannelsButton = UseCaseButton(label: "Subscribe")
    private let unsubscribeToChannelsButton = UseCaseButton(label: "Unsubscribe")
    private let submitAttributesButton = UseCaseButton(label: "Submit Attributes")
    private let whereNowButton = UseCaseButton(label: "Find user's presence in channels")
    private let getAttributesButton = UseCaseButton(label: "Get Attributes")
   
    private let sdk: EngagementSDK
    private var commentClient: CommentClient?
    
    private var profileRelationships = [String]()

    init(sdk: EngagementSDK) {
        self.sdk = sdk
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vcTitle = "Comments Use Case"
        
        tableView.delegate = self
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        
        self.joinChannelsButton.addTarget(self, action: #selector(self.joinChannels), for: .touchUpInside)
        self.leaveChannelsButton.addTarget(self, action: #selector(self.leaveChannels), for: .touchUpInside)
        self.subscribeToChannelsButton.addTarget(self, action: #selector(self.subscribe), for: .touchUpInside)
        self.unsubscribeToChannelsButton.addTarget(self, action: #selector(self.unsubscribe), for: .touchUpInside)
        self.submitAttributesButton.addTarget(self, action: #selector(self.submitAttributes), for: .touchUpInside)
        self.whereNowButton.addTarget(self, action: #selector(self.whereNow), for: .touchUpInside)
        self.getAttributesButton.addTarget(self, action: #selector(self.getAttributes), for: .touchUpInside)
        
        contentView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            stackView.bottomAnchor.constraint(equalTo: contentView.safeBottomAnchor, constant: -20),
        ])
        
        buttonStack1.addArrangedSubview(currentProfileIDLabel)
        buttonStack1.addArrangedSubview(currentProfileID)
        buttonStack1.addArrangedSubview(channelsTextField)
        buttonStack1.addArrangedSubview(joinChannelsButton)
        buttonStack1.addArrangedSubview(leaveChannelsButton)
        buttonStack1.addArrangedSubview(subscribeToChannelsButton)
        buttonStack1.addArrangedSubview(unsubscribeToChannelsButton)
        buttonStack1.addArrangedSubview(attributeKeyTextField)
        buttonStack1.addArrangedSubview(attributeValueTextField)
        buttonStack1.addArrangedSubview(submitAttributesButton)
        buttonStack1.addArrangedSubview(profileIDTextField)
        buttonStack1.addArrangedSubview(profileIDTextField)
        buttonStack1.addArrangedSubview(whereNowButton)
        buttonStack1.addArrangedSubview(getAttributesButton)
        
        stackView.addArrangedSubview(buttonStack1)
        stackView.addArrangedSubview(tableView)
        
        sdk.presenceClient.setDelegate(self)
        
        sdk.getCurrentUserProfileID { result in
            switch result {
            case .success(let id):
                self.currentProfileID.text = id
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func joinChannels() {
        
        guard let channels = channelsTextField.getChannels() else {
            self.showAlert(
                title: "Hey!",
                message: "This requires channels to be entered"
            )
            
            return
        }
        
        sdk.presenceClient.joinChannels(channels)
        self.showAlert(
            title: "Hey!",
            message: "Joined channels"
        )
    }
    
    @objc private func leaveChannels() {
        
        guard let channels = channelsTextField.getChannels() else {
            self.showAlert(
                title: "Hey!",
                message: "This requires channels to be entered"
            )
            
            return
        }
        
        sdk.presenceClient.leaveChannels(channels)
        
        self.showAlert(
            title: "Hey!",
            message: "Left channels"
        )
    }
    
    @objc private func subscribe() {
        
        guard let channels = channelsTextField.getChannels() else {
            self.showAlert(
                title: "Hey!",
                message: "This requires channels to be entered"
            )
            
            return
        }
        
        sdk.presenceClient.subscribe(to: channels)
        
        self.showAlert(
            title: "Hey!",
            message: "Subscribed to channels"
        )
    }
    
    @objc private func unsubscribe() {
        
        guard let channels = channelsTextField.getChannels() else {
            self.showAlert(
                title: "Hey!",
                message: "This requires channels to be entered"
            )
            
            return
        }
        
        sdk.presenceClient.unsubscribe(from: channels)
        
        self.showAlert(
            title: "Hey!",
            message: "Unsubscribed from channels"
        )
    }
    
    @objc private func submitAttributes() {
        
        guard let channels = channelsTextField.getChannels() else {
            self.showAlert(
                title: "Hey!",
                message: "This requires channels to be entered"
            )
            
            return
        }
        
        guard let key = attributeKeyTextField.getText(),
              let value = attributeValueTextField.getText()
        else {
            
            self.showAlert(
                title: "Hey!",
                message: "Key and Value fields are required"
            )
            
            return
        }
        
        sdk.presenceClient.setAttributes(
            for: channels,
            with: [key: value]
        ) { result in
            switch result {
            case .success(let attributes):
                self.showAlert(
                    title: "Hey!",
                    message: attributes.description
                )
            case .failure(let error):
                self.showAlert(
                    title: "Error!",
                    message: error.localizedDescription
                )
            }
        }
    }
    
    @objc private func whereNow() {

        guard let profileID = profileIDTextField.getText() else {
            
            self.showAlert(
                title: "Hey!",
                message: "Profile ID is required"
            )
            
            return
        }
        
        sdk.presenceClient.whereNow(
            for: profileID
        ) { result in
            switch result {
            case .success(let channels):
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    self.profileRelationships.removeAll()
                    channels.forEach { channel in
                        self.profileRelationships.insert(channel, at: 0)
                    }
                    self.tableView.reloadData()
                }
            case .failure(let error):
                self.showAlert(
                    title: "Error!",
                    message: error.localizedDescription
                )
            }
        }
    }
    
    @objc private func getAttributes() {
           
        guard let channels = channelsTextField.getChannels() else {
            self.showAlert(
                title: "Hey!",
                message: "This requires channels to be entered"
            )
               
            return
        }
           
        guard let profileID = profileIDTextField.getText() else {
            self.showAlert(
                title: "Hey!",
                message: "profileID field is required"
            )
               
            return
        }
           
        sdk.presenceClient.getAttributes(
            for: profileID,
            on: channels
        ) { result in
            switch result {
            case .success(let attributes):
                self.showAlert(
                    title: "Hey!",
                    message: attributes.description
                )
            case .failure(let error):
                self.showAlert(
                    title: "Error!",
                    message: error.localizedDescription
                )
            }
        }
    }
    
    func showAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: title,
                message: message,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension PresenceUseCase: PresenceClientDelegate {
    func presenceClient(
        _ presenceClient: PresenceClient,
        didReceivePresenceEvents presenceEvents: [PresenceEvent]
    ) {
        presenceEvents.forEach { event in
            var rowString = "ACTION: \(event.action)\nUSERID: \(event.userID)\nCHAN: \(event.channel)"
            if let attributes = event.attributes {
                attributes.forEach { key, val in
                    rowString += "\nATTS: \(key): \(val ?? "no value")"
                }
            }
            log.dev(rowString)
            profileRelationships.insert(rowString, at: 0)
        }
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

extension PresenceUseCase: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profileRelationships.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        var cellText: String = ""
        let rowData = profileRelationships[indexPath.row]
        cellText = rowData

        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = cellText
        cell.textLabel?.font = UIFont.systemFont(ofSize: 12.0)
        return cell
    }
}

extension PresenceUseCase: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {}
}

extension PresenceUseCase: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}

private extension UITextField {
    func getChannels() -> [String]? {
        
        guard let channels = self.text?.lowercased(),
              channels.count != 0
        else {
            return nil
        }
        
        let nospace = channels.replacingOccurrences(of: " ", with: "")
        return nospace.split(separator: Character(",")).map { String($0) }
    }
    
    func getText() -> String? {
        
        guard let text = self.text?.lowercased(),
              text.count != 0
        else {
            return nil
        }
        
        return text
    }
}
