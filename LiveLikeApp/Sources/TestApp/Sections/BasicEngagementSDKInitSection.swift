//
//  BasicEngagementSDKInitSection.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 7/5/22.
//

import Foundation
import EngagementSDK

class BasicEngagementSDKInitSection: TestCaseViewController {
    
    private let clientID: String
    private let programID: String
    
    init(clientID: String, programID: String) {
        self.clientID = clientID
        self.programID = programID
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.vcTitle = "Basic EngagementSDK Init"
        self.vcInfo = "This section is used to test EngagementSDK initialization. Useful for checking if PubNub is being initialied on init (ES-3497)"
        
        let sdk = EngagementSDK(config: EngagementSDKConfig(clientID: clientID))
    }
    
}
