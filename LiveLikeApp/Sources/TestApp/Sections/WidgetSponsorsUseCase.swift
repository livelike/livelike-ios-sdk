//
//  WidgetSponsorsUseCase.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 8/5/21.
//

import EngagementSDK
import UIKit

class WidgetSponsorsUseCase: TestCaseViewController {

    let widgetViewController = WidgetPopupViewController()
    let widgetView = UIView()
    let sponsorView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        return view
    }()
    lazy var widgetCreateView = WidgetCreateView(
        programID: self.programID,
        apiToken: self.apiToken,
        timeoutSeconds: self.timeoutSeconds,
        apiOrigin: self.apiOrigin
    )
    var session: ContentSession?

    let sdk: EngagementSDK
    let apiToken: String
    let programID: String
    let theme: Theme
    let apiOrigin: URL
    private let timeoutSeconds: Int
    private var timeoutSecondsISO8601: String {
        return "P0DT00H00M\(timeoutSeconds)S"
    }

    private var widgetsTimeInterval: TimeInterval?

    init(
        sdk: EngagementSDK,
        apiToken: String,
        programID: String,
        theme: Theme,
        timeoutSeconds: Int,
        apiOrigin: URL,
        widgetsInterval: TimeInterval? = nil
    ) {
        self.sdk = sdk
        self.apiToken = apiToken
        self.programID = programID
        self.theme = theme
        self.timeoutSeconds = timeoutSeconds
        self.apiOrigin = apiOrigin
        widgetsTimeInterval = widgetsInterval

        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "Widget Sponsors"
        vcInfo = "Use this view to test Widgets sponsors, when a widget comes in you will see the sponsor status below"

        super.viewDidLoad()

        view.backgroundColor = .darkGray

        widgetView.translatesAutoresizingMaskIntoConstraints = false
        widgetCreateView.translatesAutoresizingMaskIntoConstraints = false
        sponsorView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(widgetView)
        contentView.addSubview(widgetCreateView)
        contentView.addSubview(sponsorView)
        addChild(viewController: widgetViewController, into: widgetView)
        applyLayoutConstraints()
        let config = SessionConfiguration(programID: programID)
        session = sdk.contentSession(config: config)
        widgetViewController.session = session
        widgetViewController.setTheme(theme)

        widgetView.backgroundColor = .gray
        widgetViewController.delegate = self
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        widgetCreateView.stopRandomizedWidgets()
    }

    func applyLayoutConstraints() {
        NSLayoutConstraint.activate([
            widgetView.topAnchor.constraint(equalTo: contentView.topAnchor),
            widgetView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.5),
            widgetView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            widgetCreateView.topAnchor.constraint(equalTo: widgetView.bottomAnchor),
            widgetCreateView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetCreateView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            widgetCreateView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

            sponsorView.bottomAnchor.constraint(equalTo: widgetCreateView.topAnchor),
            sponsorView.leadingAnchor.constraint(equalTo: widgetView.leadingAnchor),
        ])
    }

    func displaySponsor(sponsors: [Sponsor]) {

        if sponsors.isEmpty {
            let label = UILabel()
            label.text = "No Sponsors found"
            sponsorView.addArrangedSubview(label)
            return
        }

        sponsors.forEach { sponsor in
            let label = UILabel()
            label.numberOfLines = 4
            label.text = "Name: \(sponsor.name)\nID:\(sponsor.id) \nlogo: \(sponsor.logoURL)\n"
            sponsorView.addArrangedSubview(label)
        }
    }

    deinit {
        log.dev("deinit")
    }
}

extension WidgetSponsorsUseCase: WidgetPopupViewControllerDelegate {
    func widgetViewController(_ widgetViewController: WidgetPopupViewController, willDisplay widget: Widget) {
        guard let widgetModel = widget.widgetModel else { return }

        switch widgetModel {
        case .alert(let model):
            displaySponsor(sponsors: model.sponsors)
        case .cheerMeter(let model):
            displaySponsor(sponsors: model.sponsors)
        case .quiz(let model):
            displaySponsor(sponsors: model.sponsors)
        case .prediction(let model):
            displaySponsor(sponsors: model.sponsors)
        case .predictionFollowUp(let model):
            displaySponsor(sponsors: model.sponsors)
        case .poll(let model):
            displaySponsor(sponsors: model.sponsors)
        case .imageSlider(let model):
            displaySponsor(sponsors: model.sponsors)
        case .socialEmbed(let model):
            displaySponsor(sponsors: model.sponsors)
        case .videoAlert(let model):
            displaySponsor(sponsors: model.sponsors)
        default:
            log.dev("Sponsors not supported for \(widgetModel.kind)")
        }
    }

    func widgetViewController(_ widgetViewController: WidgetPopupViewController, didDisplay widget: Widget) {

    }

    func widgetViewController(_ widgetViewController: WidgetPopupViewController, willDismiss widget: Widget) {

    }

    func widgetViewController(_ widgetViewController: WidgetPopupViewController, didDismiss widget: Widget) {
        sponsorView.arrangedSubviews.forEach {
            sponsorView.removeArrangedSubview($0)
            $0.removeFromSuperview()
        }
    }

    func widgetViewController(_ widgetViewController: WidgetPopupViewController, willEnqueueWidget widgetModel: WidgetModel) -> Widget? {
        return DefaultWidgetFactory.makeWidget(from: widgetModel)
    }

}
