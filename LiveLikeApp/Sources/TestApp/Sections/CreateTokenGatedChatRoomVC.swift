//
//  CreateTokenGatedChatRoomVC.swift
//  LiveLikeTestApp
//
//  Created by apple on 02/05/23.
//

import EngagementSDK
import UIKit

class CreateTokenGatedChatRoomVC: TestCaseViewController {

    private let sdk: EngagementSDK
    private let envBaseURL: URL
    private var arrSmartContract = [TokenGate]()
    private var arrNFTAttributes = [NFTAttribute]()
    private var selectedNetworkType: BlockChainNetworkType?
    private var selectedTokenType: BlockChainTokenType?
    private var selectedTextField: UITextField?
    private var isDropdownVisible = false
    private var smartContractNames = [String]()
    private var smartContractSelected = ""
    private var arrContractAddress = [String]()
    private let smartContractTableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()
    
    private let attributesTableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()
    
    private let createRoomButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Create Chat Room", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let addSmartContractButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Add Smart Contract", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let tokenGateSwitch: UISwitch = {
        let switchOnOff = UISwitch(frame: CGRect(origin: CGPoint(x: 20.0, y: 100.0), size: .zero))
        switchOnOff.setOn(true, animated: false)
        return switchOnOff
    }()
    
    private let contractAddressField: UITextField = {
        let roomTitle: UITextField = UITextField()
        roomTitle.placeholder = "Enter Smart Contract Address"
        roomTitle.translatesAutoresizingMaskIntoConstraints = false
        roomTitle.borderStyle = .line
        roomTitle.font = UIFont.systemFont(ofSize: 10.0)
        return roomTitle
    }()
    
    private let networkTypeField: UITextField = {
        let roomTitle: UITextField = UITextField()
        roomTitle.placeholder = "Network Type"
        roomTitle.translatesAutoresizingMaskIntoConstraints = false
        roomTitle.borderStyle = .line
        roomTitle.font = UIFont.systemFont(ofSize: 10.0)
        return roomTitle
    }()
    
    private let tokenTypeField: UITextField = {
        let roomTitle: UITextField = UITextField()
        roomTitle.placeholder = "Token Type"
        roomTitle.translatesAutoresizingMaskIntoConstraints = false
        roomTitle.borderStyle = .line
        roomTitle.font = UIFont.systemFont(ofSize: 10.0)
        return roomTitle
    }()
    
    private let traitKeyField: UITextField = {
        let roomTitle: UITextField = UITextField()
        roomTitle.placeholder = "Trait Key"
        roomTitle.translatesAutoresizingMaskIntoConstraints = false
        roomTitle.borderStyle = .line
        roomTitle.font = UIFont.systemFont(ofSize: 10.0)
        return roomTitle
    }()
    
    private let valueField: UITextField = {
        let roomTitle: UITextField = UITextField()
        roomTitle.placeholder = "Value"
        roomTitle.translatesAutoresizingMaskIntoConstraints = false
        roomTitle.borderStyle = .line
        roomTitle.font = UIFont.systemFont(ofSize: 10.0)
        return roomTitle
    }()
    
    private let addTraitButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Add Trait", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let roomTitle: UITextField = {
        let roomTitle: UITextField = UITextField()
        roomTitle.placeholder = "Chat Room Title"
        roomTitle.translatesAutoresizingMaskIntoConstraints = false
        roomTitle.borderStyle = .line
        roomTitle.font = UIFont.systemFont(ofSize: 12.0)
        return roomTitle
    }()
    
    private let smartContractAddressesLabel: UILabel = {
        let smartContractLabel = UILabel()
        smartContractLabel.textColor = .black
        smartContractLabel.translatesAutoresizingMaskIntoConstraints = false
        smartContractLabel.font = UIFont.systemFont(ofSize: 12.0)
        smartContractLabel.textAlignment = .center
        smartContractLabel.text = "Smart Contract Addresses"
        return smartContractLabel
    }()
    
    private let tokenGateAccessLabel: UILabel = {
        let tokenGateLabel = UILabel()
        tokenGateLabel.textColor = .black
        tokenGateLabel.translatesAutoresizingMaskIntoConstraints = false
        tokenGateLabel.font = UIFont.systemFont(ofSize: 12.0)
        tokenGateLabel.textAlignment = .left
        tokenGateLabel.text = "Token Gate Access"
        return tokenGateLabel
    }()
    
    private let attributesListLabel: UILabel = {
        let attributesLabel = UILabel()
        attributesLabel.textColor = .black
        attributesLabel.translatesAutoresizingMaskIntoConstraints = false
        attributesLabel.font = UIFont.systemFont(ofSize: 15.0)
        attributesLabel.textAlignment = .center
        attributesLabel.text = "Attributes List"
        return attributesLabel
    }()
    
    private let networkTypePicker: UIPickerView = {
        let picker = UIPickerView()
        picker.translatesAutoresizingMaskIntoConstraints = false
        return picker
    }()
    
    private let dropDownButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        if #available(iOS 13.0, *) {
            button.setImage(UIImage(systemName: "chevron.down.circle.fill"), for: .normal)
        } else {
            // Fallback on earlier versions
            button.setImage(UIImage(named: ""), for: .normal)
        }
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let dropDownTableView: UITableView = {
        let tableView = UITableView()
        tableView.frame = CGRect(x: 190, y: 193, width: 154, height: 0)
        tableView.layer.borderWidth = 1.0
        tableView.layer.borderColor = UIColor.lightGray.cgColor
        tableView.isHidden = true
        return tableView
    }()

    init(envBaseURL: URL, sdk: EngagementSDK) {
        self.envBaseURL = envBaseURL
        self.sdk = sdk
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        smartContractTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        smartContractTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        smartContractTableView.dataSource = self
        smartContractTableView.delegate = self
        
        attributesTableView.register(UITableViewCell.self, forCellReuseIdentifier: "attributesCell")
        attributesTableView.dataSource = self
        attributesTableView.delegate = self
        
        dropDownTableView.register(UITableViewCell.self, forCellReuseIdentifier: "dropDownCell")
        dropDownTableView.dataSource = self
        dropDownTableView.delegate = self
        
        networkTypeField.delegate = self
        tokenTypeField.delegate = self
        
        contentView.addSubview(roomTitle)
        contentView.addSubview(tokenGateAccessLabel)
        contentView.addSubview(tokenGateSwitch)
        contentView.addSubview(networkTypeField)
        contentView.addSubview(smartContractAddressesLabel)
        contentView.addSubview(tokenTypeField)
        contentView.addSubview(contractAddressField)
        contentView.addSubview(dropDownButton)
        contentView.addSubview(addSmartContractButton)
        contentView.addSubview(createRoomButton)
        contentView.addSubview(addTraitButton)
        contentView.addSubview(traitKeyField)
        contentView.addSubview(valueField)
        contentView.addSubview(smartContractTableView)
        contentView.addSubview(attributesListLabel)
        contentView.addSubview(attributesTableView)
        contentView.addSubview(dropDownTableView)
        
        NSLayoutConstraint.activate([
            roomTitle.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20.0),
            roomTitle.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20.0),
            roomTitle.heightAnchor.constraint(equalToConstant: 30.0),
            roomTitle.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -40),
            
            createRoomButton.topAnchor.constraint(equalTo: roomTitle.bottomAnchor, constant: 10.0),
            createRoomButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            createRoomButton.heightAnchor.constraint(equalToConstant: 30),
            createRoomButton.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -20),
            
            tokenGateSwitch.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20.0),
            tokenGateSwitch.topAnchor.constraint(equalTo: createRoomButton.bottomAnchor, constant: 10.0),
            
            tokenGateAccessLabel.leadingAnchor.constraint(equalTo: tokenGateSwitch.trailingAnchor, constant: 10.0),
            tokenGateAccessLabel.widthAnchor.constraint(equalToConstant: 150.0),
            tokenGateAccessLabel.centerYAnchor.constraint(equalTo: tokenGateSwitch.centerYAnchor),
            
            smartContractAddressesLabel.topAnchor.constraint(equalTo: tokenGateSwitch.bottomAnchor, constant: 5.0),
            smartContractAddressesLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor),
            smartContractAddressesLabel.heightAnchor.constraint(equalToConstant: 15),
            smartContractAddressesLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            
            networkTypeField.topAnchor.constraint(equalTo: smartContractAddressesLabel.bottomAnchor, constant: 10.0),
            networkTypeField.widthAnchor.constraint(equalToConstant: 75.0),
            networkTypeField.heightAnchor.constraint(equalToConstant: 30.0),
            networkTypeField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20.0),
            
            tokenTypeField.topAnchor.constraint(equalTo: smartContractAddressesLabel.bottomAnchor, constant: 10.0),
            tokenTypeField.widthAnchor.constraint(equalToConstant: 75.0),
            tokenTypeField.heightAnchor.constraint(equalToConstant: 30.0),
            tokenTypeField.leadingAnchor.constraint(equalTo: networkTypeField.trailingAnchor, constant: 10.0),
            
            contractAddressField.topAnchor.constraint(equalTo: smartContractAddressesLabel.bottomAnchor, constant: 10.0),
            contractAddressField.leadingAnchor.constraint(equalTo: tokenTypeField.trailingAnchor, constant: 10.0),
            contractAddressField.heightAnchor.constraint(equalToConstant: 30),
            contractAddressField.trailingAnchor.constraint(equalTo: dropDownButton.leadingAnchor, constant: -10),
            
            dropDownButton.topAnchor.constraint(equalTo: smartContractAddressesLabel.bottomAnchor, constant: 10.0),
            dropDownButton.leadingAnchor.constraint(equalTo: contractAddressField.trailingAnchor, constant: 10.0),
            dropDownButton.heightAnchor.constraint(equalToConstant: 30),
            dropDownButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            dropDownButton.widthAnchor.constraint(equalToConstant: 20.0),
            
            addTraitButton.topAnchor.constraint(equalTo: networkTypeField.bottomAnchor, constant: 10.0),
            addTraitButton.widthAnchor.constraint(equalToConstant: 75.0),
            addTraitButton.heightAnchor.constraint(equalToConstant: 30.0),
            addTraitButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20.0),
            
            traitKeyField.topAnchor.constraint(equalTo: tokenTypeField.bottomAnchor, constant: 10.0),
            traitKeyField.widthAnchor.constraint(equalTo: valueField.widthAnchor, multiplier: 1),
            traitKeyField.heightAnchor.constraint(equalToConstant: 30.0),
            traitKeyField.leadingAnchor.constraint(equalTo: addTraitButton.trailingAnchor, constant: 10.0),
            
            valueField.topAnchor.constraint(equalTo: contractAddressField.bottomAnchor, constant: 10.0),
            valueField.leadingAnchor.constraint(equalTo: traitKeyField.trailingAnchor, constant: 10.0),
            valueField.heightAnchor.constraint(equalToConstant: 30),
            valueField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            
            addSmartContractButton.topAnchor.constraint(equalTo: addTraitButton.bottomAnchor, constant: 10.0),
            addSmartContractButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            addSmartContractButton.heightAnchor.constraint(equalToConstant: 30),
            addSmartContractButton.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -20),
            
            smartContractTableView.topAnchor.constraint(equalTo: addSmartContractButton.bottomAnchor, constant: 10),
            smartContractTableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            smartContractTableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            
            attributesListLabel.topAnchor.constraint(equalTo: smartContractTableView.bottomAnchor, constant: 5.0),
            attributesListLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor),
            attributesListLabel.heightAnchor.constraint(equalToConstant: 15),
            attributesListLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            
            attributesTableView.topAnchor.constraint(equalTo: attributesListLabel.bottomAnchor, constant: 10),
            attributesTableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            attributesTableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            attributesTableView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            attributesTableView.heightAnchor.constraint(equalToConstant: 150.0)
        ])
        
        createRoomButton.addTarget(self, action: #selector(createChatRoom), for: .touchUpInside)
        addSmartContractButton.addTarget(self, action: #selector(addSmartContract), for: .touchUpInside)
        dropDownButton.addTarget(self, action: #selector(toggleDropdown), for: .touchUpInside)
        addTraitButton.addTarget(self, action: #selector(addTraits), for: .touchUpInside)
        tokenGateSwitch.addTarget(self, action: #selector(self.switchStateDidChange(_:)), for: .valueChanged)
        contractAddressField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        
        getSmartContractAliases(for: .first)
        addTraitButton.isHidden = true
        traitKeyField.isHidden = true
        valueField.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func applicationWillEnterForeground() {}
    
    @objc private func createChatRoom() {
        guard let chatRoomTitle = roomTitle.text, chatRoomTitle.count > 0 else {
            showAlert(title: "Enter Room Title", msg: nil)
            return
        }
        if tokenGateSwitch.isOn == true, arrSmartContract.count > 0 {
            sdk.createChatRoom(
                title: chatRoomTitle,
                visibility: .members,
                tokenGates: arrSmartContract
            ) { result in
                switch result {
                case let .success(chatRoomID):
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
                        // self.joinRoomField.text = chatRoomID
                        let alert = UIAlertController(
                            title: "Room Created",
                            message: "ID: \(chatRoomID)",
                            preferredStyle: .alert
                        )
                        
                        alert.addAction(UIAlertAction(title: "Copy Id To Clipboard", style: .default, handler: { _ in
                            UIPasteboard.general.string = chatRoomID
                        }))
                        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                case let .failure(error):
                    log.dev("Error: \(error.localizedDescription)")
                }
            }
        } else {
            sdk.createChatRoom(
                title: chatRoomTitle,
                visibility: .members
            ) { result in
                switch result {
                case let .success(chatRoomID):
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
                        let alert = UIAlertController(
                            title: "Room Created",
                            message: "ID: \(chatRoomID)",
                            preferredStyle: .alert
                        )
                        
                        alert.addAction(UIAlertAction(title: "Copy Id To Clipboard", style: .default, handler: { _ in
                            UIPasteboard.general.string = chatRoomID
                        }))
                        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                case let .failure(error):
                    log.dev("Error: \(error.localizedDescription)")
                }
            }
        }
    }
    
    @objc func switchStateDidChange(_ sender: UISwitch!) {
        if sender.isOn == true {
            print("UISwitch state is now ON")
            networkTypeField.isHidden = false
            contractAddressField.isHidden = false
            smartContractAddressesLabel.isHidden = false
            addSmartContractButton.isHidden = false
            tokenTypeField.isHidden = false
            addTraitButton.isHidden = true
            traitKeyField.isHidden = true
            valueField.isHidden = true
        } else {
            print("UISwitch state is now Off")
            arrSmartContract.removeAll()
            networkTypeField.text = ""
            contractAddressField.text = ""
            tokenTypeField.text = ""
            networkTypeField.isHidden = true
            contractAddressField.isHidden = true
            smartContractAddressesLabel.isHidden = true
            addSmartContractButton.isHidden = true
            tokenTypeField.isHidden = true
            addTraitButton.isHidden = true
            traitKeyField.isHidden = true
            valueField.isHidden = true
            self.smartContractTableView.reloadData()
        }
    }
    
    @objc func toggleDropdown() {
        if isDropdownVisible {
            dropDownTableView.isHidden = true
            dropDownTableView.frame.size.height = 0
        } else {
            dropDownTableView.isHidden = false
            dropDownTableView.frame.size.height = CGFloat(100) // Adjust the row height if needed options.count * 44
        }
        isDropdownVisible.toggle()
    }
    
    @objc func addTraits() {
        if let trait = traitKeyField.text, let value = valueField.text, trait.count > 0, value.count > 0 {
            arrNFTAttributes.append(NFTAttribute(traitType: trait, value: value))
            traitKeyField.text = ""
            valueField.text = ""
            attributesTableView.reloadData()
        } else {
            showAlert(title: "Enter Trait Details", msg: nil)
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        smartContractSelected = ""
    }
    
    @objc func addSmartContract() {
        if let contractAddress = contractAddressField.text, let networkType = selectedNetworkType, let tokenType = selectedTokenType, contractAddress.count > 0 {
            if tokenType == BlockChainTokenType.nonfungible, arrNFTAttributes.count > 0 {
                arrSmartContract.append(TokenGate(contractAddress: contractAddress, networkType: networkType, tokenType: tokenType, attributes: arrNFTAttributes))
            } else {
                arrSmartContract.append(TokenGate(contractAddress: contractAddress, networkType: networkType, tokenType: tokenType, attributes: []))
            }
            print(arrSmartContract)
            networkTypeField.text = ""
            contractAddressField.text = ""
            tokenTypeField.text = ""
            self.smartContractTableView.reloadData()
            self.arrNFTAttributes.removeAll()
            self.attributesTableView.reloadData()
        } else {
            showAlert(title: "Enter Token Gate Access Details", msg: nil)
        }
    }
    
    private func getSmartContractAliases(for page: Pagination) {
        sdk.getSmartContracts(page: page) { result in
            switch result {
            case let .success(contractNames):
                contractNames.forEach { contractAddressName in
                    self.smartContractNames.append(contractAddressName.contractName)
                    self.arrContractAddress.append(contractAddressName.contractAddress)
                }
                if contractNames.count >= 0 {
                    self.smartContractNames += contractNames.count == 0 ? [] : ["Load More"]
                    DispatchQueue.main.async {
                        self.dropDownTableView.reloadData()
                    }
                } else {
                    self.smartContractNames.removeLast()
                    DispatchQueue.main.async {
                        self.dropDownTableView.reloadData()
                    }
                }
            case let .failure(error):
                log.dev("Error: \(error.localizedDescription)")
            }
        }
    }
    
    private func loadMoreSmartContractNames() {
        getSmartContractAliases(for: .next)
    }
    
    private func showAlert(title: String, msg: String?) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let alert = UIAlertController(
                title: title,
                message: msg ?? "",
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension CreateTokenGatedChatRoomVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == smartContractTableView {
            return arrSmartContract.count
        } else {
            return arrNFTAttributes.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case smartContractTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            let cellText: String = "Network Type: \(arrSmartContract[indexPath.row].networkType) \nSmart Contract Address: \(arrSmartContract[indexPath.row].contractAddress)"
            cell.textLabel?.text = cellText
            cell.textLabel?.numberOfLines = 2
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12)
            return cell
        case dropDownTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: "dropDownCell", for: indexPath)
            let cellText: String = smartContractNames[indexPath.row]
            cell.textLabel?.text = cellText
            cell.textLabel?.numberOfLines = 2
            cell.textLabel?.font = UIFont.systemFont(ofSize: 10)
            // Load more options if the "Load More" cell is visible
            if indexPath.row == smartContractNames.count - 1, smartContractNames[indexPath.row] == "Load More" {
                loadMoreSmartContractNames()
            }
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "attributesCell", for: indexPath)
            var cellText: String = "Trait Type: \(arrNFTAttributes[indexPath.row].traitType) \nValue: \(arrNFTAttributes[indexPath.row].value)"
            cell.textLabel?.text = cellText
            cell.textLabel?.numberOfLines = 2
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            switch tableView {
            case smartContractTableView:
                self.arrSmartContract.remove(at: indexPath.row)
                self.smartContractTableView.deleteRows(at: [indexPath], with: .automatic)
            default:
                self.arrNFTAttributes.remove(at: indexPath.row)
                self.attributesTableView.deleteRows(at: [indexPath], with: .automatic)
            }
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}

extension CreateTokenGatedChatRoomVC: UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTextField = textField
        switch selectedTextField {
        case networkTypeField:
            pickUp(networkTypeField)
        case tokenTypeField:
            pickUp(tokenTypeField)
        default:
            break
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tokenTypeField {
            switch selectedTokenType {
            case .fungible:
                addTraitButton.isHidden = true
                traitKeyField.isHidden = true
                valueField.isHidden = true
            case .nonfungible:
                addTraitButton.isHidden = false
                traitKeyField.isHidden = false
                valueField.isHidden = false
            default:
                break
            }
        }
    }

    func pickUp(_ textField: UITextField) {
        // UIPickerView
        self.networkTypePicker.delegate = self
        self.networkTypePicker.dataSource = self
        self.networkTypePicker.backgroundColor = UIColor.white
        textField.inputView = self.networkTypePicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92 / 255, green: 216 / 255, blue: 255 / 255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(CreateTokenGatedChatRoomVC.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(CreateTokenGatedChatRoomVC.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    @objc func doneClick() {
        if selectedTextField == networkTypeField {
            networkTypeField.resignFirstResponder()
        } else {
            tokenTypeField.resignFirstResponder()
        }
    }
    @objc func cancelClick() {
        if selectedTextField == networkTypeField {
            networkTypeField.resignFirstResponder()
        } else {
            tokenTypeField.resignFirstResponder()
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if selectedTextField == networkTypeField {
            return BlockChainNetworkType.allCases.count
        } else {
            return BlockChainTokenType.allCases.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if selectedTextField == networkTypeField {
            return BlockChainNetworkType.allCases[row].rawValue // dropdown item
        } else {
            return BlockChainTokenType.allCases[row].rawValue // dropdown item
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if selectedTextField == networkTypeField {
            networkTypeField.text = BlockChainNetworkType.allCases[row].rawValue
            selectedNetworkType = BlockChainNetworkType.allCases[row]
        } else {
            tokenTypeField.text = BlockChainTokenType.allCases[row].rawValue
            selectedTokenType = BlockChainTokenType.allCases[row]
        }
    }
}

