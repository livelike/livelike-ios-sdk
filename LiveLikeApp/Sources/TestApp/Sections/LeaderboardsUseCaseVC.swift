//
//  LeaderboardsUseCaseVC.swift
//  LiveLikeDemoApp
//
//  Created by Mike Moloksher on 7/27/20.
//
import EngagementSDK
import UIKit

class LeaderboardsUseCaseVC: TestCaseViewController {
    private let sdk: EngagementSDK
    private let programID: String
    private var leaderboards = [Leaderboard]()

    private let topStack: UIStackView = {
        let stack = UIStackView()
        stack.alignment = .fill
        stack.distribution = .fill
        stack.axis = .vertical
        stack.backgroundColor = .red
        stack.spacing = 10.0
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    private let getLeaderBoardsBTN: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Get Leaderboards", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private let programIDField: UITextField = {
        let roomTitle: UITextField = UITextField()
        roomTitle.placeholder = "Enter Program ID"
        roomTitle.translatesAutoresizingMaskIntoConstraints = false
        roomTitle.borderStyle = .line
        roomTitle.font = UIFont.systemFont(ofSize: 10.0)
        return roomTitle
    }()

    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .lightGray
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()

    init(sdk: EngagementSDK, programID: String) {
        self.sdk = sdk
        self.programID = programID
        programIDField.text = programID
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        tableView.delegate = self

        contentView.addSubview(topStack)

        NSLayoutConstraint.activate([
            topStack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0.0),
            topStack.widthAnchor.constraint(equalTo: contentView.widthAnchor),
            topStack.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            topStack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0.0)
        ])

        let row1 = UIStackView(arrangedSubviews: [getLeaderBoardsBTN, programIDField])
        row1.distribution = .fillProportionally
        topStack.addArrangedSubview(row1)
        topStack.addArrangedSubview(tableView)

        getLeaderBoardsBTN.addTarget(self, action: #selector(getLeaderBoards), for: .touchUpInside)
    }

    @objc private func getLeaderBoards() {
        guard let program = programIDField.text,
              program.count > 0
        else {
            return
        }
        sdk.getLeaderboards(programID: program) { result in
            switch result {
            case let .success(leads):
                self.leaderboards = leads
                self.tableView.reloadData()
            case let .failure(error):
                print(error)
            }
        }
    }
}

extension LeaderboardsUseCaseVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        log.dev("\(leaderboards.count)")
        return leaderboards.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        var cellText: String = ""
        cellText = "\(leaderboards[indexPath.row].name)"

        cell.textLabel?.text = cellText
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        sdk.getLeaderboard(leaderboardID: leaderboards[indexPath.row].id) { result in
            switch result {
            case let .success(leaderboard):
                self.navigationController?.pushViewController(
                    LeaderBoardDetailVC(sdk: self.sdk, leaderboard: leaderboard),
                    animated: true
                )
            case let .failure(error):
                print(error)
            }
        }
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Leaderboards"
    }
}

class LeaderBoardDetailVC: UIViewController {
    private let sdk: EngagementSDK
    private let leaderboard: Leaderboard
    private var entries = [LeaderboardEntry]()

    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.backgroundColor = .lightGray
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()

    private let mainStack: UIStackView = {
        let stack = UIStackView()
        stack.alignment = .fill
        stack.distribution = .fill
        stack.axis = .vertical
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    private let getMoreEntriesBTN: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Get More Entries", for: .normal)
        return button
    }()

    private let getManyEntriesBTN: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Get All Entries at Once", for: .normal)
        return button
    }()

    private let sortUpBTN: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Sort Up", for: .normal)
        return button
    }()

    private let sortDownBTN: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Sort Down", for: .normal)
        return button
    }()

    private let getCurrentUserProfileBTN: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Get Current User Board Profile", for: .normal)
        return button
    }()

    init(sdk: EngagementSDK, leaderboard: Leaderboard) {
        self.sdk = sdk
        self.leaderboard = leaderboard
        super.init(nibName: nil, bundle: nil)
        title = leaderboard.name
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        getMoreEntriesBTN.addTarget(self, action: #selector(getMoreEntries), for: .touchUpInside)
        sortUpBTN.addTarget(self, action: #selector(sortUp), for: .touchUpInside)
        sortDownBTN.addTarget(self, action: #selector(sortDown), for: .touchUpInside)
        getCurrentUserProfileBTN.addTarget(self, action: #selector(getCurrentUserProfile), for: .touchUpInside)
        getManyEntriesBTN.addTarget(self, action: #selector(getManyEntries), for: .touchUpInside)

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        tableView.delegate = self

        view.addSubview(mainStack)

        NSLayoutConstraint.activate([
            mainStack.topAnchor.constraint(equalTo: view.topAnchor, constant: 70.0),
            mainStack.widthAnchor.constraint(equalTo: view.widthAnchor),
            mainStack.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            mainStack.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0.0)
        ])

        let buttons = UIStackView(arrangedSubviews: [getManyEntriesBTN, getMoreEntriesBTN, sortUpBTN, sortDownBTN, getCurrentUserProfileBTN])
        buttons.axis = .vertical
        buttons.spacing = 1.0
        buttons.distribution = .equalSpacing

        mainStack.spacing = 10.0
        mainStack.addArrangedSubview(buttons)
        mainStack.addArrangedSubview(tableView)

        getEntries(page: .first)

        // Test setting custom user data
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YY, MMM d, HH:mm:ss"

        sdk.setUserCustomData(data: "\(dateFormatter.string(from: Date()))") { result in
            switch result {
            case .success(()):
                log.dev("success on custom data")
            case .failure(let error):
                let alert = UIAlertController(
                    title: "Error setting Custom Data!",
                    message: "Contact Jelzon immedietly! \(error)",
                    preferredStyle: .alert
                )

                alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    @objc private func getManyEntries() {
        for _ in 1 ... 20 {
            getEntries(page: .next)
        }
    }

    @objc private func getMoreEntries() {
        getEntries(page: .next)
    }

    @objc private func sortUp() {
        entries.sort { $0.rank < $1.rank }
        tableView.reloadData()
    }

    @objc private func sortDown() {
        entries.sort { $0.rank > $1.rank }
        tableView.reloadData()
    }

    @objc private func getCurrentUserProfile() {
        sdk.getLeaderboardEntryForCurrentProfile(leaderboardID: leaderboard.id) { result in
            switch result {
            case let .success(leaderboardEntry):
                let alert = UIAlertController(
                    title: "Profile for \(leaderboardEntry.profileNickname)",
                    message: "Custom Data: \(leaderboardEntry.profile.customData ?? "no data") \nRank: \(leaderboardEntry.rank)",
                    preferredStyle: .alert
                )

                alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case let .failure(error):
                print(error)
            }
        }
    }

    func getEntries(page: Pagination) {
        sdk.getLeaderboardEntries(leaderboardID: leaderboard.id, page: page) { result in
            switch result {
            case let .success(leaderboardEntries):
                self.entries.append(contentsOf: leaderboardEntries.entries)
                self.entries.sort { $0.rank < $1.rank }
                self.getMoreEntriesBTN.setTitle("Get More Entries \(self.entries.count)/\(leaderboardEntries.total)", for: .normal)
                self.tableView.reloadData()
                if leaderboardEntries.hasNext == false {
                    self.getMoreEntriesBTN.isUserInteractionEnabled = false
                    self.getMoreEntriesBTN.alpha = 0.5
                }
            case let .failure(error):
                print(error)
            }
        }
    }
}

extension LeaderBoardDetailVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        log.dev("\(entries.count)")
        return entries.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        var cellText: String = ""
        cellText = "\(entries[indexPath.row].rank) - \(entries[indexPath.row].profileNickname) Score: \(entries[indexPath.row].score)"

        cell.textLabel?.text = cellText
        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Entries for \(leaderboard.rewardItem.name)"
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        sdk.getLeaderboardEntry(
            profileID: entries[indexPath.row].profileId,
            leaderboardID: leaderboard.id
        ) { result in
            switch result {
            case let .success(profile):
                let alert = UIAlertController(
                    title: "Profile Results",
                    message: "Name: \(profile.profileNickname) \nRank: \(profile.rank)",
                    preferredStyle: .alert
                )

                alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case let .failure(error):
                print(error)
            }
        }
    }
}
