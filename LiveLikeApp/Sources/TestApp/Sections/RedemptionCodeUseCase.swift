//
//  RedemptionCodeUseCase.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 1/26/22.
//

import UIKit
import EngagementSDK

class CodeRedemptionUseCase: TestCaseViewController {

    struct RewardItemsData {
        var rewardItem: RewardItem
        var rewardBalance: Int
    }

    private var contentSession: ContentSession!
    private var sdk: EngagementSDK
    private var items: [RedemptionKey] = []
    private var requestOptions = GetRedemptionKeysRequestOptions()

    private let submitAttributesBTN: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Redeem Key", for: .normal)
        return button
    }()

    private let redeemByCodeInput: UITextField = {
        let attributesInput: UITextField = UITextField()
        attributesInput.placeholder = "Enter Redeem Code to Redeem"
        attributesInput.translatesAutoresizingMaskIntoConstraints = false
        attributesInput.borderStyle = .line
        attributesInput.font = UIFont.systemFont(ofSize: 10.0)
        return attributesInput
    }()

    private let redeemByIDInput: UITextField = {
        let attributesInput: UITextField = UITextField()
        attributesInput.placeholder = "Enter Key ID to Redeem"
        attributesInput.translatesAutoresizingMaskIntoConstraints = false
        attributesInput.borderStyle = .line
        attributesInput.font = UIFont.systemFont(ofSize: 10.0)
        return attributesInput
    }()

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.allowsSelection = true
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "myCell")
        return tableView
    }()

    private let loadMoreButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Load More", for: .normal)
        return button
    }()

    init(sdk: EngagementSDK, programID: String) {
        self.sdk = sdk
        self.contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "Redeemed Keys"

        super.viewDidLoad()

        view.addSubview(redeemByCodeInput)
        view.addSubview(redeemByIDInput)
        view.addSubview(submitAttributesBTN)
        view.addSubview(tableView)
        view.addSubview(loadMoreButton)

        NSLayoutConstraint.activate([

            redeemByCodeInput.topAnchor.constraint(equalTo: contentView.topAnchor),
            redeemByCodeInput.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0),
            redeemByCodeInput.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            redeemByCodeInput.heightAnchor.constraint(equalToConstant: 50.0),

            redeemByIDInput.topAnchor.constraint(equalTo: redeemByCodeInput.bottomAnchor, constant: 5.0),
            redeemByIDInput.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0),
            redeemByIDInput.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            redeemByIDInput.heightAnchor.constraint(equalToConstant: 50.0),

            submitAttributesBTN.topAnchor.constraint(equalTo: redeemByIDInput.bottomAnchor),
            submitAttributesBTN.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            submitAttributesBTN.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            submitAttributesBTN.heightAnchor.constraint(equalToConstant: 60.0),

            tableView.topAnchor.constraint(equalTo: submitAttributesBTN.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: loadMoreButton.topAnchor),

            loadMoreButton.topAnchor.constraint(equalTo: tableView.bottomAnchor),
            loadMoreButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            loadMoreButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            loadMoreButton.heightAnchor.constraint(equalToConstant: 60.0),
            loadMoreButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])

        submitAttributesBTN.addTarget(self, action: #selector(submitAttributesBTNSelected), for: .touchUpInside)
        loadMoreButton.addTarget(self, action: #selector(loadMoreButtonSelected), for: .touchUpInside)
        redeemByIDInput.addTarget(
            self,
            action: #selector(textFieldDidChange(_:)),
            for: .editingChanged
        )

        redeemByCodeInput.addTarget(
            self,
            action: #selector(textFieldDidChange(_:)),
            for: .editingChanged
        )

        tableView.delegate = self
        tableView.dataSource = self

        loadInitialData()
    }

    @objc private func textFieldDidChange(_ textField: UITextField) {
        if redeemByIDInput == textField {
            redeemByCodeInput.text = ""
        } else {
            redeemByIDInput.text = ""
        }
    }

    private func loadInitialData() {
        self.items.removeAll()

        sdk.getRedemptionKeys(
            page: .first,
            options: requestOptions
        ) { [weak self] result in

            guard let self = self else { return }

            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(let redeemedCodes):
                self.items.removeAll()
                self.items.append(contentsOf: redeemedCodes)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }

            }
        }
    }

    @objc private func submitAttributesBTNSelected() {

        if !redeemByIDInput.text!.isEmpty {
            sdk.redeemKeyBy(
                redemptionKeyID: redeemByIDInput.text!
            ) { [weak self] result in

                guard let self = self else { return }

                switch result {
                case .failure(let error):
                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                    self.present(alert, animated: true, completion: nil)
                case .success(_):
                    self.loadInitialData()
                }
            }
        } else {
            sdk.redeemKeyBy(
                redemptionCode: redeemByCodeInput.text!
            ) { [weak self] result in

                guard let self = self else { return }

                switch result {
                case .failure(let error):
                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                    self.present(alert, animated: true, completion: nil)
                case .success(_):
                    self.loadInitialData()
                }
            }

        }
    }

    @objc private func loadMoreButtonSelected() {
        sdk.rewards.getApplicationRewardItems(
            page: .next
        ) { result in

            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(_):
                self.items.removeAll()
                // self.items.append(contentsOf: receivedProfileBadges)
                self.tableView.reloadData()
            }
        }
    }
}

extension CodeRedemptionUseCase: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            var cell = tableView.dequeueReusableCell(withIdentifier: "myCell")
        else {
            return UITableViewCell()
        }

        cell = UITableViewCell(style: .subtitle, reuseIdentifier: "myCell")
        let redeemedCode = self.items[indexPath.row]
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        cell.textLabel?.text = "Code: \(redeemedCode.code) | Name: \(redeemedCode.name)"

        cell.detailTextLabel?.text = "ID: \(redeemedCode.id)"
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
