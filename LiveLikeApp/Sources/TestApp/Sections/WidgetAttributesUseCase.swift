//
//  WidgetAttributesUseCase.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 2/10/22.
//

import EngagementSDK
import UIKit

class WidgetAttributesUseCase: TestCaseViewController {

    let widgetViewController = WidgetPopupViewController()
    let widgetView = UIView()
    let attributesView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        return view
    }()
    lazy var widgetCreateView = WidgetCreateView(
        programID: self.programID,
        apiToken: self.apiToken,
        timeoutSeconds: self.timeoutSeconds,
        apiOrigin: self.apiOrigin
    )
    var session: ContentSession?

    let sdk: EngagementSDK
    let apiToken: String
    let programID: String
    let theme: Theme
    let apiOrigin: URL
    private let timeoutSeconds: Int
    private var timeoutSecondsISO8601: String {
        return "P0DT00H00M\(timeoutSeconds)S"
    }

    private var widgetsTimeInterval: TimeInterval?

    init(
        sdk: EngagementSDK,
        apiToken: String,
        programID: String,
        theme: Theme,
        timeoutSeconds: Int,
        apiOrigin: URL,
        widgetsInterval: TimeInterval? = nil
    ) {
        self.sdk = sdk
        self.apiToken = apiToken
        self.programID = programID
        self.theme = theme
        self.timeoutSeconds = timeoutSeconds
        self.apiOrigin = apiOrigin
        widgetsTimeInterval = widgetsInterval

        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "Widget Attributes"
        vcInfo = "Use this view to test Widgets attributes, when a widget comes in you will see the associated with the widget"

        super.viewDidLoad()

        view.backgroundColor = .darkGray

        widgetView.translatesAutoresizingMaskIntoConstraints = false
        widgetCreateView.translatesAutoresizingMaskIntoConstraints = false
        attributesView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(widgetView)
        contentView.addSubview(widgetCreateView)
        contentView.addSubview(attributesView)
        addChild(viewController: widgetViewController, into: widgetView)
        applyLayoutConstraints()
        let config = SessionConfiguration(programID: programID)
        session = sdk.contentSession(config: config)
        widgetViewController.session = session
        widgetViewController.setTheme(theme)

        widgetView.backgroundColor = .gray
        widgetViewController.delegate = self
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        widgetCreateView.stopRandomizedWidgets()
    }

    func applyLayoutConstraints() {
        NSLayoutConstraint.activate([
            widgetView.topAnchor.constraint(equalTo: contentView.topAnchor),
            widgetView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.5),
            widgetView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            widgetCreateView.topAnchor.constraint(equalTo: widgetView.bottomAnchor),
            widgetCreateView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetCreateView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            widgetCreateView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

            attributesView.bottomAnchor.constraint(equalTo: widgetCreateView.topAnchor),
            attributesView.leadingAnchor.constraint(equalTo: widgetView.leadingAnchor),
        ])
    }

    func displayAttributes(attributes: [Attribute]) {

        if attributes.isEmpty {
            let label = UILabel()
            label.text = "No Attributes found"
            attributesView.addArrangedSubview(label)
            return
        }

        attributes.forEach { attribute in
            let label = UILabel()
            label.numberOfLines = 4
            label.text = "Key: \(attribute.key)\nValue:\(attribute.value)"
            attributesView.addArrangedSubview(label)
        }
    }

    deinit {
        log.dev("deinit")
    }
}

extension WidgetAttributesUseCase: WidgetPopupViewControllerDelegate {
    func widgetViewController(_ widgetViewController: WidgetPopupViewController, willDisplay widget: Widget) {
        guard let widgetModel = widget.widgetModel else { return }

        switch widgetModel {
        case .alert(let model):
            displayAttributes(attributes: model.widgetAttributes)
        case .cheerMeter(let model):
            displayAttributes(attributes: model.widgetAttributes)
        case .quiz(let model):
            displayAttributes(attributes: model.widgetAttributes)
        case .prediction(let model):
            displayAttributes(attributes: model.widgetAttributes)
        case .predictionFollowUp(let model):
            displayAttributes(attributes: model.widgetAttributes)
        case .poll(let model):
            displayAttributes(attributes: model.widgetAttributes)
        case .imageSlider(let model):
            displayAttributes(attributes: model.widgetAttributes)
        case .socialEmbed(let model):
            displayAttributes(attributes: model.widgetAttributes)
        case .videoAlert(let model):
            displayAttributes(attributes: model.widgetAttributes)
        default:
            log.dev("Sponsors not supported for \(widgetModel.kind)")
        }
    }

    func widgetViewController(_ widgetViewController: WidgetPopupViewController, didDisplay widget: Widget) {

    }

    func widgetViewController(_ widgetViewController: WidgetPopupViewController, willDismiss widget: Widget) {

    }

    func widgetViewController(_ widgetViewController: WidgetPopupViewController, didDismiss widget: Widget) {
        attributesView.arrangedSubviews.forEach {
            attributesView.removeArrangedSubview($0)
            $0.removeFromSuperview()
        }
    }

    func widgetViewController(_ widgetViewController: WidgetPopupViewController, willEnqueueWidget widgetModel: WidgetModel) -> Widget? {
        return DefaultWidgetFactory.makeWidget(from: widgetModel)
    }

}
