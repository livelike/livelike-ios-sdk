//
//  ChatReactionSimulatorSection.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 8/4/20.
//

import EngagementSDK
import UIKit

class ChatReactionSimulatorSection: ChatTestCaseViewController {
    var timer: DispatchSourceTimer?

    private let startDelay: TimeInterval
    private let messageFrequency: DispatchTimeInterval

    init(
        sdk: EngagementSDK,
        programID: String,
        startDelay: TimeInterval,
        messageFrequency: DispatchTimeInterval
    ) {
        self.startDelay = startDelay
        self.messageFrequency = messageFrequency
        super.init(sdk: sdk, programID: programID, theme: Theme())
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "Automatic Chat Reactions"
        vcInfo = "A random chat reaction will be sent on the newest message automatically."

        super.viewDidLoad()

        // Start simulator
        DispatchQueue.global().asyncAfter(deadline: .now() + startDelay) { [weak self] in
            guard let self = self else { return }
            self.timer = DispatchSource.makeTimerSource(flags: [], queue: .main)
            self.timer?.schedule(deadline: .now(), repeating: self.messageFrequency)
            self.timer?.setEventHandler {
                DispatchQueue.main.async {
//                    let randomReactionIndex = Int.random(in: 0 ... 3)
//                    self?.chatController.sendReaction(index: randomReactionIndex)
                }
            }
            self.timer?.resume()
        }
    }
}
