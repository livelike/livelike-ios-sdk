//
//  GetWidgetInteractionHistoryTestSection.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 8/24/21.
//

import EngagementSDK
import UIKit

class GetWidgetInteractionHistoryTestSection: TestCaseViewController {
    
    private let session: ContentSession
    
    private let widgetIDTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = "Enter Widget ID"
        return textField
    }()
    
    private let widgetKindTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = "Enter Widget Kind"
        return textField
    }()
    
    private let loadInteractionHistoryButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Load Interaction History", for: .normal)
        return button
    }()
    
    private let interactionHistoryTextView: UITextView = {
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    init(session: ContentSession) {
        self.session = session
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vcTitle = "Get Widget Interactions History"
        vcInfo = "Displays the widget's interaction history"
        
        let stackView = UIStackView(arrangedSubviews: [
            widgetIDTextField,
            widgetKindTextField,
            loadInteractionHistoryButton
        ])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        contentView.addSubview(stackView)
        contentView.addSubview(interactionHistoryTextView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            stackView.heightAnchor.constraint(greaterThanOrEqualToConstant: 0),
            
            interactionHistoryTextView.topAnchor.constraint(equalTo: stackView.bottomAnchor),
            interactionHistoryTextView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            interactionHistoryTextView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            interactionHistoryTextView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
        ])
        
        loadInteractionHistoryButton.addTarget(self, action: #selector(loadInteractionHistoryButtonSelected), for: .touchUpInside)
        
    }
    
    @objc private func loadInteractionHistoryButtonSelected() {
        guard
            let widgetID = widgetIDTextField.text
        else {
            alert(message: "Missing widget id")
            return
        }
        
        guard
            let kindString = widgetKindTextField.text,
            let kind = WidgetKind(stringValue: kindString)
        else {
            alert(message: "missing or invalid widget kind")
            return
        }
        
        session.getWidgetModel(byID: widgetID, kind: kind) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let model):
                switch model {
                case .cheerMeter(let model):
                    model.loadInteractionHistory { result in
                        switch result {
                        case .failure(let error):
                            self.alert(message: error.localizedDescription)
                        case .success(let interations):
                            self.interactionHistoryTextView.text = interations.map { $0.optionID }.joined(separator: "\n")
                        }
                    }
                case .quiz(let model):
                    model.loadInteractionHistory { result in
                        switch result {
                        case .failure(let error):
                            self.alert(message: error.localizedDescription)
                        case .success(let interations):
                            self.interactionHistoryTextView.text = interations.map { $0.choiceID }.joined(separator: "\n")
                        }
                    }
                case .prediction(let model):
                    model.loadInteractionHistory { result in
                        switch result {
                        case .failure(let error):
                            self.alert(message: error.localizedDescription)
                        case .success(let interations):
                            self.interactionHistoryTextView.text = interations.map { $0.optionID }.joined(separator: "\n")
                        }
                    }
                case .predictionFollowUp(let model):
                    model.loadInteractionHistory { result in
                        switch result {
                        case .failure(let error):
                            self.alert(message: error.localizedDescription)
                        case .success(let interations):
                            self.interactionHistoryTextView.text = interations.map { $0.optionID }.joined(separator: "\n")
                        }
                    }
                case .textAsk(let model):
                    model.loadInteractionHistory { result in
                        switch result {
                        case .failure(let error):
                            self.alert(message: error.localizedDescription)
                        case .success(let interations):
                            self.interactionHistoryTextView.text = interations.map { $0.id }.joined(separator: "\n")
                        }
                    }
                case .poll(let model):
                    model.loadInteractionHistory { result in
                        switch result {
                        case .failure(let error):
                            self.alert(message: error.localizedDescription)
                        case .success(let interations):
                            self.interactionHistoryTextView.text = interations.map { $0.optionID }.joined(separator: "\n")
                        }
                    }
                case .imageSlider(let model):
                    model.loadInteractionHistory { result in
                        switch result {
                        case .failure(let error):
                            self.alert(message: error.localizedDescription)
                        case .success(let interations):
                            self.interactionHistoryTextView.text = interations.map { $0.id }.joined(separator: "\n")
                        }
                    }
                default:
                    self.interactionHistoryTextView.text = ""
                    print("no interactions for \(model.kind)")
                }
            case .failure(let error):
                self.alert(message: "\(error.localizedDescription)\n\(String(describing: error))")
            }
        }
    }
    
    func alert(message: String) {
        self.interactionHistoryTextView.text = ""
        let alert = UIAlertController(
            title: "Failed",
            message: message,
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
