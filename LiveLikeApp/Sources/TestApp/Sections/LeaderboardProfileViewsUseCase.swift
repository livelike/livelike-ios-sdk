//
//  LeaderboardProfileViewsUseCase.swift
//  LiveLikeTestApp
//
//  Created by Keval Shah on 27/09/22.
//

import EngagementSDK
import UIKit

class LeaderboardProfileViewsUseCase: TestCaseViewController {
    private let sdk: EngagementSDK
    private let programID: String
    private var entries = [ProfileLeaderboard]()
    private var views = [ProfileLeaderboardView]()
    private var isViews: Bool = false

    private let topStack: UIStackView = {
        let stack = UIStackView()
        stack.alignment = .fill
        stack.distribution = .fill
        stack.axis = .vertical
        stack.spacing = 10.0
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    private let getLeaderBoardsBTN: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Get Leaderboards For Profile", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private let profileIDField: UITextField = {
        let roomTitle: UITextField = UITextField()
        roomTitle.placeholder = "Enter Profile ID"
        roomTitle.translatesAutoresizingMaskIntoConstraints = false
        roomTitle.borderStyle = .line
        roomTitle.font = UIFont.systemFont(ofSize: 10.0)
        return roomTitle
    }()

    private let getLeaderBoardViewsBTN: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Get Leaderboard Views For Profile", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .lightGray
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()

    init(sdk: EngagementSDK, programID: String) {
        self.sdk = sdk
        self.programID = programID
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        tableView.delegate = self

        contentView.addSubview(topStack)

        NSLayoutConstraint.activate([
            topStack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0.0),
            topStack.widthAnchor.constraint(equalTo: contentView.widthAnchor),
            topStack.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            topStack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0.0)
        ])

        let row1 = UIStackView(arrangedSubviews: [profileIDField, getLeaderBoardsBTN, getLeaderBoardViewsBTN])
        row1.axis = .vertical
        row1.spacing = 5.0
        topStack.addArrangedSubview(row1)
        topStack.addArrangedSubview(tableView)

        getLeaderBoardsBTN.addTarget(self, action: #selector(getLeaderBoardProfiles), for: .touchUpInside)
        getLeaderBoardViewsBTN.addTarget(self, action: #selector(getLeaderBoardViews), for: .touchUpInside)
    }

    @objc private func getLeaderBoardProfiles() {
        entries.removeAll()
        isViews = false
        guard let profileIDText = profileIDField.text else {
            self.showAlert(title: "Error", message: "Missing Profile ID")
            return
        }

        sdk.getProfileLeaderboards(profileID: profileIDText, page: .first) { result in
            switch result {
            case .success(let leaderboardEntries):
                self.entries.append(contentsOf: leaderboardEntries.entries)
                self.tableView.reloadData()
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }

    @objc private func getLeaderBoardViews() {
        views.removeAll()
        isViews = true
        guard let profileIDText = profileIDField.text else {
            self.showAlert(title: "Error", message: "Missing Profile ID")
            return
        }

        sdk.getProfileLeaderboardViews(profileID: profileIDText, page: .first) { result in
            switch result {
            case .success(let leaderboardEntries):
                self.views.append(contentsOf: leaderboardEntries.entries)
                self.tableView.reloadData()
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }

    private func showAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: title,
                message: message,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension LeaderboardProfileViewsUseCase: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        isViews ? log.dev("\(views.count)") : log.dev("\(entries.count)")
        return isViews ? views.count : entries.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        var cellText: String = ""
        cellText = isViews ? "\(views[indexPath.row].entry?.rank) - \(views[indexPath.row].profileNickname)" : "\(entries[indexPath.row].entry.rank) - \(entries[indexPath.row].profileNickname) Score: \(entries[indexPath.row].entry.score)"

        cell.textLabel?.text = cellText
        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Leaderboard Entries Result"
    }
}
