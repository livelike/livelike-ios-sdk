//
//  ChatRoomMembershipUseCase.swift
//  LiveLikeDemoApp
//
//  Created by Mike Moloksher on 6/17/20.
//

import EngagementSDK
import UIKit

class ChatRoomMembershipUseCase: TestCaseViewController {

    private let sdk: EngagementSDK
    private let envBaseURL: URL

    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()

    private let createRoomButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Create Chat Room", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private let checkMembershipBtn: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Show Memberships For This Room", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private let becomeMemberBtn: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Become Member of This Room", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private let deleteMemberBtn: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Delete Membership of This Room", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private let getUserMemberChatRoomsBtn: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Show all rooms you are a member of", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private let joinRoomField: UITextField = {
        let roomTitle: UITextField = UITextField()
        roomTitle.placeholder = "Enter Chat Room ID"
        roomTitle.translatesAutoresizingMaskIntoConstraints = false
        roomTitle.borderStyle = .line
        roomTitle.font = UIFont.systemFont(ofSize: 10.0)
        return roomTitle
    }()

    private let roomTitle: UITextField = {
        let roomTitle: UITextField = UITextField()
        roomTitle.placeholder = "Chat Room Title"
        roomTitle.translatesAutoresizingMaskIntoConstraints = false
        roomTitle.borderStyle = .line
        roomTitle.font = UIFont.systemFont(ofSize: 12.0)
        return roomTitle
    }()

    private let roomIdLabel: UILabel = {
        let roomIdLabel = UILabel()
        roomIdLabel.textColor = .white
        roomIdLabel.translatesAutoresizingMaskIntoConstraints = false
        roomIdLabel.font = UIFont.systemFont(ofSize: 8.0)
        return roomIdLabel
    }()

    private let roomTitleLabel: UILabel = {
        let roomTitleLabel = UILabel()
        roomTitleLabel.textColor = .white
        roomTitleLabel.font = UIFont.systemFont(ofSize: 8.0)
        roomTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        return roomTitleLabel
    }()

    private let roomInFocusLabel: UILabel = {
        let roomInFocusLabel = UILabel()
        roomInFocusLabel.textColor = .black
        roomInFocusLabel.translatesAutoresizingMaskIntoConstraints = false
        roomInFocusLabel.font = UIFont.systemFont(ofSize: 12.0)
        roomInFocusLabel.textAlignment = .center
        roomInFocusLabel.text = "Room In Focus"
        return roomInFocusLabel
    }()

    private var memberships = [Any]()

    init(envBaseURL: URL, sdk: EngagementSDK) {
        self.envBaseURL = envBaseURL
        self.sdk = sdk
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        sdk.chat.delegate = self

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self

        contentView.addSubview(createRoomButton)
        contentView.addSubview(roomTitle)
        contentView.addSubview(roomInFocusLabel)
        contentView.addSubview(checkMembershipBtn)
        contentView.addSubview(becomeMemberBtn)
        contentView.addSubview(deleteMemberBtn)
        contentView.addSubview(getUserMemberChatRoomsBtn)
        contentView.addSubview(joinRoomField)
        contentView.addSubview(tableView)

        NSLayoutConstraint.activate([
            createRoomButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20.0),
            createRoomButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20.0),
            createRoomButton.heightAnchor.constraint(equalToConstant: 50.0),
            createRoomButton.widthAnchor.constraint(equalToConstant: 150.0),
            roomTitle.leadingAnchor.constraint(equalTo: createRoomButton.trailingAnchor, constant: 10.0),
            roomTitle.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            roomTitle.centerYAnchor.constraint(equalTo: createRoomButton.centerYAnchor),

            roomInFocusLabel.topAnchor.constraint(equalTo: createRoomButton.bottomAnchor, constant: 10.0),
            roomInFocusLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor),
            roomInFocusLabel.heightAnchor.constraint(equalToConstant: 30),
            roomInFocusLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),

            joinRoomField.topAnchor.constraint(equalTo: roomInFocusLabel.bottomAnchor, constant: 5.0),
            joinRoomField.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -40),
            joinRoomField.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),

            becomeMemberBtn.topAnchor.constraint(equalTo: joinRoomField.bottomAnchor, constant: 5.0),
            becomeMemberBtn.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            becomeMemberBtn.heightAnchor.constraint(equalToConstant: 30),
            becomeMemberBtn.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -20),

            deleteMemberBtn.topAnchor.constraint(equalTo: becomeMemberBtn.bottomAnchor, constant: 5.0),
            deleteMemberBtn.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            deleteMemberBtn.heightAnchor.constraint(equalToConstant: 30),
            deleteMemberBtn.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -20),

            checkMembershipBtn.topAnchor.constraint(equalTo: deleteMemberBtn.bottomAnchor, constant: 5.0),
            checkMembershipBtn.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            checkMembershipBtn.heightAnchor.constraint(equalToConstant: 30),
            checkMembershipBtn.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -20),

            getUserMemberChatRoomsBtn.topAnchor.constraint(equalTo: checkMembershipBtn.bottomAnchor, constant: 5.0),
            getUserMemberChatRoomsBtn.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            getUserMemberChatRoomsBtn.heightAnchor.constraint(equalToConstant: 30),
            getUserMemberChatRoomsBtn.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -20),

            tableView.topAnchor.constraint(equalTo: getUserMemberChatRoomsBtn.bottomAnchor, constant: 10),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])

        createRoomButton.addTarget(self, action: #selector(createChatRoom), for: .touchUpInside)
        checkMembershipBtn.addTarget(self, action: #selector(getChatRoomMemberships), for: .touchUpInside)
        becomeMemberBtn.addTarget(self, action: #selector(becomeMemberOfTheRoom), for: .touchUpInside)
        deleteMemberBtn.addTarget(self, action: #selector(deleteMemebershipOfRoom), for: .touchUpInside)
        getUserMemberChatRoomsBtn.addTarget(self, action: #selector(getUserChatRoomMemberships), for: .touchUpInside)

        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc private func applicationWillEnterForeground() {}

    @objc private func createChatRoom() {
        sdk.createChatRoom(
            title: roomTitle.text!.count > 0 ? roomTitle.text : nil,
            visibility: .members
        ) { result in
            switch result {
            case let .success(chatRoomID):
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    self.joinRoomField.text = chatRoomID
                    let alert = UIAlertController(
                        title: "Room Created",
                        message: "ID: \(chatRoomID)",
                        preferredStyle: .alert
                    )

                    alert.addAction(UIAlertAction(title: "Become member of new room", style: .default, handler: { _ in
                        self.becomeMemberOfTheRoom()
                    }))

                    alert.addAction(UIAlertAction(title: "Copy Id To Clipboard", style: .default, handler: { _ in
                        UIPasteboard.general.string = chatRoomID
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            case let .failure(error):
                log.dev("Error: \(error.localizedDescription)")
            }
        }
    }

    @objc private func getChatRoomMemberships() {
        sdk.getChatRoomMemberships(roomID: joinRoomField.text!, page: .first) { result in
            switch result {
            case let .success(chatRoomMembers):
                self.showAlert(title: "Members in Chat Room", msg: "Amount: \(chatRoomMembers.count)")
                self.memberships = chatRoomMembers
                self.tableView.reloadData()
            case let .failure(error):
                log.dev("Error: \(error.localizedDescription)")
            }
        }
    }

    @objc private func getUserChatRoomMemberships() {
        sdk.getUserChatRoomMemberships(page: .first) { result in
            switch result {
            case let .success(chatRoomMembers):
                self.showAlert(title: "User Member of # Rooms", msg: "Amount: \(chatRoomMembers.count)")
                self.memberships = chatRoomMembers
                self.tableView.reloadData()
            case let .failure(error):
                log.dev("Error: \(error.localizedDescription)")
            }
        }
    }

    @objc private func becomeMemberOfTheRoom() {
        guard let chatRoomID = joinRoomField.text,
              chatRoomID.count > 0
        else {
            showAlert(title: "Enter Room ID", msg: nil)
            return
        }
        sdk.createUserChatRoomMembership(roomID: chatRoomID) { [weak self] result in
            DispatchQueue.main.async {
                guard let self = self else { return }
                switch result {
                case .success:
                    self.showAlert(title: "Now Member", msg: "")
                case let .failure(error):
                    self.showAlert(title: "Error", msg: error.localizedDescription)
                }
            }
        }
    }

    @objc private func deleteMemebershipOfRoom() {
        guard let chatRoomID = joinRoomField.text,
              chatRoomID.count > 0
        else {
            showAlert(title: "Enter Room ID", msg: nil)
            return
        }
        sdk.deleteUserChatRoomMembership(roomID: chatRoomID) { [weak self] result in
            DispatchQueue.main.async {
                guard let self = self else { return }
                switch result {
                case .success:
                    self.showAlert(title: "Deleted Membership", msg: "")
                case let .failure(error):
                    self.showAlert(title: "Error", msg: error.localizedDescription)
                }
            }
        }
    }

    private func showAlert(title: String, msg: String?) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let alert = UIAlertController(
                title: title,
                message: msg ?? "",
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    private func showInviteAlert(title: String, msg: String?, invitation: ChatRoomInvitation) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let alert = UIAlertController(
                title: title,
                message: msg ?? "",
                preferredStyle: .actionSheet
            )
            alert.addAction(UIAlertAction(title: "Accept", style: .default, handler: { _ in
                self.sdk.chat.updateChatRoomInviteStatus(chatRoomInvitation: invitation, invitationStatus: .accepted) { result in
                    switch result {
                    case .success:
                        self.showAlert(title: "Invitation Accepted", msg: "")
                    case .failure(let error):
                        self.showAlert(title: "Failed to Accept", msg: error.localizedDescription)
                    }
                }
            }))
            alert.addAction(UIAlertAction(title: "Reject", style: .destructive, handler: { _ in
                self.sdk.chat.updateChatRoomInviteStatus(chatRoomInvitation: invitation, invitationStatus: .rejected) { result in
                    switch result {
                    case .success:
                        self.showAlert(title: "Invitation Rejected", msg: "")
                    case .failure(let error):
                        self.showAlert(title: "Failed to Reject", msg: error.localizedDescription)
                    }
                }
            }))
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension ChatRoomMembershipUseCase: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return memberships.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        var cellText: String = ""
        if let rowData = memberships[indexPath.row] as? ChatRoomInfo {
            cellText = "Room Id: \(rowData.id)"
        } else if let rowData = memberships[indexPath.row] as? ChatRoomMember {
            cellText = "User: \(rowData.profile.nickname)"
        } else if let rowData = memberships[indexPath.row] as? BlockInfo {
            cellText = "Blocked User: \(rowData.blockedProfile.nickname)"
        }

        cell.textLabel?.text = cellText
        return cell
    }
}

extension ChatRoomMembershipUseCase: ChatClientDelegate {
    func chatClient(_ chatClient: ChatClient, userDidGetBlocked blockInfo: BlockInfo) {

    }

    func chatClient(_ chatClient: ChatClient, userDidGetUnblocked unblockInfo: UnblockInfo) {

    }

    func chatClient(_ chatClient: ChatClient, userDidBecomeMemberOfChatRoom newChatMembershipInfo: NewChatMembershipInfo) {
        self.showAlert(title: "Added to Chatroom", msg: "You've been added to room: \(String(describing: newChatMembershipInfo.chatRoomTitle)) by \(newChatMembershipInfo.senderNickName)")
    }

    func chatClient(_ chatClient: ChatClient, userDidReceiveInvitation chatRoomInvitation: ChatRoomInvitation) {
        self.showInviteAlert(title: "Invitation Received", msg: "You've been invited to room: \(String(describing: chatRoomInvitation.chatRoom.title)) by \(chatRoomInvitation.invitedBy.nickname)", invitation: chatRoomInvitation)
    }
}
