//
//  GetWidgetsUseCase.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 11/24/21.
//

import UIKit
import EngagementSDK

class GetWidgetsUseCase: TestCaseViewController {

    private var contentSession: ContentSession!
    private var sdk: EngagementSDK
    private var widgetModels: [WidgetModel] = []
    private var reqOptions: GetWidgetModelsRequestOptions = GetWidgetModelsRequestOptions()

    private static let anyKindString = "anyKind"

    private let widgetKindOptions: [String] = {
        var options = [GetWidgetsUseCase.anyKindString]
        options.append(contentsOf: WidgetKind.allCases.map { $0.displayName })
        return options
    }()

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.allowsSelection = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "myCell")
        return tableView
    }()

    private let loadMoreButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Load More", for: .normal)
        return button
    }()

    private let mainStack: UIStackView = {
        let stack = UIStackView()
        stack.alignment = .center
        stack.distribution = .fillEqually
        stack.axis = .horizontal
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    private let getWidgetsBTN: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Submit", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private let cellReuseIdentifer: String = "com.livelike.timelineCell"

    private let statusPicker: UIPickerView = {
        let picker = UIPickerView()
        picker.translatesAutoresizingMaskIntoConstraints = false
        return picker
    }()

    private let widgetKindPicker: UIPickerView = {
        let picker = UIPickerView()
        picker.translatesAutoresizingMaskIntoConstraints = false
        return picker
    }()

    private let widgetOrderingPicker: UIPickerView = {
        let picker = UIPickerView()
        picker.translatesAutoresizingMaskIntoConstraints = false
        return picker
    }()

    private let interactionOptions: [String] = ["true", "false", "none"]

    private let sinceDateTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.adjustsFontSizeToFitWidth = true
        textField.placeholder = "since"
        return textField
    }()

    private let interactivePicker: UIPickerView = {
        let picker = UIPickerView()
        picker.translatesAutoresizingMaskIntoConstraints = false
        return picker
    }()

    init(sdk: EngagementSDK, programID: String) {
        self.sdk = sdk
        self.contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "Get Widgets Use Case"

        super.viewDidLoad()

        mainStack.addArrangedSubview(statusPicker)
        mainStack.addArrangedSubview(widgetKindPicker)
        mainStack.addArrangedSubview(widgetOrderingPicker)
        mainStack.addArrangedSubview(interactivePicker)
        mainStack.addArrangedSubview(sinceDateTextField)
        mainStack.addArrangedSubview(getWidgetsBTN)

        statusPicker.delegate = self
        statusPicker.dataSource = self
        widgetKindPicker.delegate = self
        widgetKindPicker.dataSource = self
        widgetOrderingPicker.delegate = self
        widgetOrderingPicker.dataSource = self
        interactivePicker.delegate = self
        interactivePicker.dataSource = self

        view.addSubview(mainStack)
        view.addSubview(tableView)
        view.addSubview(loadMoreButton)

        NSLayoutConstraint.activate([
            mainStack.topAnchor.constraint(equalTo: contentView.topAnchor),
            mainStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0),
            mainStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            mainStack.heightAnchor.constraint(equalToConstant: 60),

            tableView.topAnchor.constraint(equalTo: mainStack.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: loadMoreButton.topAnchor),

            loadMoreButton.topAnchor.constraint(equalTo: tableView.bottomAnchor),
            loadMoreButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            loadMoreButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            loadMoreButton.heightAnchor.constraint(equalToConstant: 60.0),
            loadMoreButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])

        loadMoreButton.addTarget(self, action: #selector(loadMoreButtonSelected), for: .touchUpInside)
        getWidgetsBTN.addTarget(self, action: #selector(getWidgets), for: .touchUpInside)

        tableView.register(WidgetTableViewCell.self, forCellReuseIdentifier: cellReuseIdentifer)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = true

        getWidgets()
    }

    @objc private func getWidgets() {

        reqOptions = {
            let status = WidgetStatus.allCases[statusPicker.selectedRow(inComponent: 0)]
            let kind: Set<WidgetKind>? = {
                if self.widgetKindOptions[self.widgetKindPicker.selectedRow(inComponent: 0)] == GetWidgetsUseCase.anyKindString {
                    return nil
                } else {
                    let pickedKind = WidgetKind.allCases[widgetKindPicker.selectedRow(inComponent: 0)]
                    return [pickedKind]
                }
            }()
            let ordering = WidgetOrdering.allCases[widgetOrderingPicker.selectedRow(inComponent: 0)]
            let interactive: Bool? = {
                let selectedRow = interactivePicker.selectedRow(inComponent: 0)
                switch selectedRow {
                case 0:
                    return true
                case 1:
                    return false
                default:
                    return nil
                }
            }()
            let since: Date? = {
                guard let sinceText = sinceDateTextField.text else {
                    return nil
                }
                return DateFormatter.iso8601Full.date(from: sinceText)
            }()
            return GetWidgetModelsRequestOptions(
                widgetStatus: status,
                widgetKind: kind,
                widgetOrdering: ordering,
                interactive: interactive,
                since: since
            )
        }()
        contentSession.getWidgetModels(
            page: .first,
            options: reqOptions
        ) { [weak self] result in

            guard let self = self else { return }
            DispatchQueue.main.async {
                switch result {
                case let .success(widgets):
                    if widgets.count > 0 {
                        self.widgetModels.removeAll()
                        self.widgetModels.append(contentsOf: widgets)
                        self.tableView.reloadData()
                    } else {
                        let alert = UIAlertController(title: "No Results", message: "No Widgets Found", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Thanks for Nothing!", style: .cancel))
                        self.present(alert, animated: true, completion: nil)
                    }
                case let .failure(error):
                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }

    @objc private func loadMoreButtonSelected() {
        contentSession.getWidgetModels(
            page: .next,
            options: reqOptions
        ) { [weak self] result in

            guard let self = self else { return }
            DispatchQueue.main.async {
                switch result {
                case let .success(widgets):
                    if widgets.count > 0 {
                        self.widgetModels.append(contentsOf: widgets)
                        self.tableView.reloadData()
                    } else {
                        let alert = UIAlertController(title: "No Results", message: "No Widgets Found", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Thanks for Nothing!", style: .cancel))
                        self.present(alert, animated: true, completion: nil)
                    }
                case let .failure(error):
                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                    self.present(alert, animated: true, completion: nil)
                }
            }

        }
    }
}

extension GetWidgetsUseCase: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return widgetModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifer) as? WidgetTableViewCell else {
            return UITableViewCell()
        }
        // prepare for reuse
        cell.widget?.removeFromParent()
        cell.widget?.view.removeFromSuperview()
        cell.widget = nil

        let widgetModel = widgetModels[indexPath.row]

        if let widget = DefaultWidgetFactory.makeWidget(from: widgetModel) {
            addChild(widget)
            widget.didMove(toParent: self)
            widget.view.translatesAutoresizingMaskIntoConstraints = false
            cell.contentView.addSubview(widget.view)

            NSLayoutConstraint.activate([
                widget.view.topAnchor.constraint(equalTo: cell.contentView.topAnchor),
                widget.view.leadingAnchor.constraint(equalTo: cell.contentView.leadingAnchor),
                widget.view.trailingAnchor.constraint(equalTo: cell.contentView.trailingAnchor),
                widget.view.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor)
            ])
            cell.widget = widget
        }

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let widgetModel = widgetModels[indexPath.row]
        let infoDetailVC = InfoDetailVC(model: widgetModel)
        self.navigationController?.pushViewController(infoDetailVC, animated: true)
    }
}

class InfoDetailVC: UIViewController {

    private var model: WidgetModel
    private let mainStack: UIStackView = {
        let stack = UIStackView()
        stack.alignment = .leading
        stack.distribution = .fillProportionally
        stack.axis = .vertical
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    init(model: WidgetModel) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white

        switch model {
        case .prediction(let model):
            self.title = model.question

            model.earnableRewards.forEach { reward in

                let title = UILabel()
                title.text = "Earn-able Reward for \(model.question)"
                mainStack.addArrangedSubview(title)

                let name = UILabel()
                name.text = "\tName: \(reward.rewardItemName)"
                mainStack.addArrangedSubview(name)

                let id = UILabel()
                id.text = "\tID: \(reward.rewardItemID)"
                mainStack.addArrangedSubview(id)

                let action = UILabel()
                action.text = "\tReward Action: \(reward.rewardActionKey)"
                mainStack.addArrangedSubview(action)

                let amount = UILabel()
                amount.text = "\tAmount \(String(reward.rewardItemAmount))"
                mainStack.addArrangedSubview(amount)

                let breakline = UILabel()
                breakline.text = "---"
                mainStack.addArrangedSubview(breakline)
            }

            model.options.forEach { option in
                let title = UILabel()
                title.text = "\tOption \(option.text)"
                mainStack.addArrangedSubview(title)

                option.earnableRewards.forEach { reward in
                    let name = UILabel()
                    name.text = "\t\tName: \(reward.rewardItemName)"
                    mainStack.addArrangedSubview(name)

                    let id = UILabel()
                    id.text = "\t\tID: \(reward.rewardItemID)"
                    mainStack.addArrangedSubview(id)

                    let action = UILabel()
                    action.text = "\t\tReward Action: \(reward.rewardActionKey)"
                    mainStack.addArrangedSubview(action)

                    let amount = UILabel()
                    amount.text = "\t\tAmount \(String(reward.rewardItemAmount))"
                    mainStack.addArrangedSubview(amount)
                }

                if option.earnableRewards.count == 0 {
                    let title = UILabel()
                    title.text = "\t\tNo Earnable Reward found for Option"
                    mainStack.addArrangedSubview(title)
                }

                let breakline = UILabel()
                breakline.text = "---"
                mainStack.addArrangedSubview(breakline)
            }

        default:
            break
        }

        view.addSubview(mainStack)

        NSLayoutConstraint.activate([
            mainStack.topAnchor.constraint(equalTo: view.safeTopAnchor),
            mainStack.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10.0),
            mainStack.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10.0),
            mainStack.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}

extension GetWidgetsUseCase: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == statusPicker {
            return WidgetStatus.allCases.count
        } else if pickerView == widgetOrderingPicker {
            return WidgetOrdering.allCases.count
        } else if pickerView == interactivePicker {
            return 3
        }
        return self.widgetKindOptions.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        if pickerView == statusPicker {
            return "\(WidgetStatus.allCases[row])"
        } else if pickerView == widgetOrderingPicker {
            return "\(WidgetOrdering.allCases[row])"
        } else if pickerView == interactivePicker {
            return interactionOptions[row]
        }
        return "\(self.widgetKindOptions[row])"
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var title = UILabel()
        if let view = view as? UILabel {
            title = view
        }
        title.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold)
        title.textColor = UIColor.blue
        title.textAlignment = .center

        if pickerView == statusPicker {
            title.text = "\(WidgetStatus.allCases[row])"
        } else if pickerView == widgetOrderingPicker {
            title.text = "\(WidgetOrdering.allCases[row])"
        } else if pickerView == interactivePicker {
            title.text = interactionOptions[row]
        } else {
            title.text = "\(self.widgetKindOptions[row])"
        }

        return title
    }
}
