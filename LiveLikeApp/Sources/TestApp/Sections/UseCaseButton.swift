//
//  File.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 3/10/23.
//

import UIKit

class UseCaseButton: UIButton {
    init(label: String) {
        super.init(frame: .zero)
        self.backgroundColor = .lightGray
        self.translatesAutoresizingMaskIntoConstraints = true
        self.setTitle("\(label) ->", for: .normal)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
