//
//  OnDemandWidgetEndlessListView.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 4/10/20.
//

import EngagementSDK
import UIKit

class OnDemandWidgetDynamicListView: TestCaseViewController {
    private let clientID: String
    private let programID: String
    private let apiToken: String
    private let apiOrigin: URL
    private let widgetStateController = DefaultWidgetStateController(
        closeButtonAction: {},
        widgetFinishedCompletion: { _ in }
    )

    private var sdk: EngagementSDK!
    private var session: ContentSession!

    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()

    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    private lazy var widgetCreateView: WidgetCreateView = {
        let widgetCreateView = WidgetCreateView(
            programID: self.programID,
            apiToken: self.apiToken,
            timeoutSeconds: 20,
            apiOrigin: self.apiOrigin
        )
        widgetCreateView.translatesAutoresizingMaskIntoConstraints = false
        return widgetCreateView
    }()

    init(clientID: String, programID: String, apiToken: String, apiOrigin: URL) {
        self.clientID = clientID
        self.programID = programID
        self.apiToken = apiToken
        self.apiOrigin = apiOrigin
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        vcInfo = """
        This section is to test the behavior of on demand widgets in a never ending list view with dynamic height.
        """

        setupUI()
        setupEngagementSDK()
    }

    private func setupUI() {
        view.backgroundColor = .white

        contentView.addSubview(scrollView)
        scrollView.addSubview(stackView)
        contentView.addSubview(widgetCreateView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: contentView.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            scrollView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.6),

            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),

            widgetCreateView.topAnchor.constraint(equalTo: scrollView.bottomAnchor),
            widgetCreateView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetCreateView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            widgetCreateView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.4)
        ])
    }

    private func setupEngagementSDK() {
        sdk = EngagementSDK(config: EngagementSDKConfig(clientID: clientID))
        session = sdk.contentSession(config: SessionConfiguration(programID: programID))
        session.delegate = self
    }
}

extension OnDemandWidgetDynamicListView: ContentSessionDelegate {
    func playheadTimeSource(_ session: ContentSession) -> Date? {
        return nil
    }

    func session(_ session: ContentSession, didChangeStatus status: SessionStatus) {}

    func session(_ session: ContentSession, didReceiveError error: Error) {}

    func chat(session: ContentSession, roomID: String, newMessage message: ChatMessage) {}

    func widget(_ session: ContentSession, didBecomeReady jsonObject: Any) {}

    func widget(_ session: ContentSession, didBecomeReady widget: Widget) {
        DispatchQueue.main.async {
            self.addChild(widget)
            self.stackView.addArrangedSubview(widget.view)
            widget.didMove(toParent: self)
            widget.delegate = self.widgetStateController
            widget.moveToNextState()
        }
    }

    func contentSession(_ session: ContentSession, didReceiveWidget widget: WidgetModel) {}
}
