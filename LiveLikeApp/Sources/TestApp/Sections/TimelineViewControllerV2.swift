//
//  TimelineViewControllerV2.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 2/16/21.
//

import EngagementSDK
import UIKit

final class TimelineViewControllerV2: TestCaseViewController {
    private let cellReuseIdentifer: String = "myCell"

    private var sdk: EngagementSDK!
    private var session: ContentSession!

    private let clientID: String
    private let programID: String

    /// Keeps track of loaded model id's
    /// Used mostly for social embed refresh its height once loaded
    private var loadedIndexPaths = Set<String>()

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.allowsSelection = false
        tableView.separatorStyle = .none
        tableView.isHidden = true
        return tableView
    }()

    // Determines if the displayed widget should be interactable or not
    private var widgetIsInteractableByID: [String: Bool] = [:]
    private var widgetModels: [WidgetModel] = []

    init(clientID: String, programID: String) {
        self.clientID = clientID
        self.programID = programID
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        contentView.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: contentView.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])

        var config = EngagementSDKConfig(clientID: clientID)
        config.apiOrigin = EngagementSDKConfigManager.shared.selectedEnvironment.url
        config.accessTokenStorage = EngagementSDKConfigManager.shared
        sdk = EngagementSDK(config: config)
        session = sdk.contentSession(config: SessionConfiguration(programID: programID))
        session.delegate = self

        tableView.register(WidgetTableViewCell.self, forCellReuseIdentifier: cellReuseIdentifer)
        tableView.dataSource = self

        session.getPostedWidgetModels(page: .first) { result in
            switch result {
            case let .success(widgetModels):
                guard let widgetModels = widgetModels else { return }
                self.widgetModels.append(contentsOf: widgetModels)
                widgetModels.forEach {
                    self.widgetIsInteractableByID[$0.id] = false
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.tableView.isHidden = false
                }
            case let .failure(error):
                print(error)
            }
            self.session.delegate = self
        }
    }

    func makeInteractableWidget(widgetModel: WidgetModel) -> UIViewController? {
        let widget = DefaultWidgetFactory.makeWidget(from: widgetModel)
        widget?.currentState = .interacting

        let timeInterval: Double = {
            switch widgetModel {
            case let .socialEmbed(model):
                return model.interactionTimeInterval + 2.0
            default:
                return (widget?.interactionTimeInterval)!
            }
        }()

        widget?.addTimer(seconds: timeInterval, completion: { [weak self] _ in
            guard let self = self else { return }
            self.widgetIsInteractableByID[widgetModel.id] = false
            widget?.currentState = .finished

            switch widgetModel {
            case .socialEmbed:
                break
            default:
                self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
            }

        })
        return widget
    }

    func makeResultsWidget(widgetModel: WidgetModel) -> UIViewController? {
        let widget = DefaultWidgetFactory.makeWidget(from: widgetModel)
        widget?.currentState = .finished
        return widget
    }
}

extension TimelineViewControllerV2: ContentSessionDelegate {
    func playheadTimeSource(_ session: ContentSession) -> Date? { return nil }
    func session(_ session: ContentSession, didChangeStatus status: SessionStatus) {}
    func session(_ session: ContentSession, didReceiveError error: Error) {}
    func chat(session: ContentSession, roomID: String, newMessage message: ChatMessage) {}
    func widget(_ session: ContentSession, didBecomeReady widget: Widget) {}

    func contentSession(_ session: ContentSession, didReceiveWidget widget: WidgetModel) {
        widgetModels.insert(widget, at: 0)
        widgetIsInteractableByID[widget.id] = true
        DispatchQueue.main.async {
            self.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .top)
        }
    }
}

extension TimelineViewControllerV2: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return widgetModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifer) as? WidgetTableViewCell else {
            return UITableViewCell()
        }

        // prepare for reuse
        cell.widget?.removeFromParent()
        cell.widget?.view.removeFromSuperview()
        cell.widget = nil

        let widgetModel = widgetModels[indexPath.row]
        log.dev("cell loaded: row: \(indexPath.row) \(widgetModel.id)")

        if indexPath.row == 2 {
            log.dev("catch")
        }

        var widget: UIViewController?
        if widgetIsInteractableByID[widgetModel.id] ?? false {
            widget = makeInteractableWidget(widgetModel: widgetModel)
        } else {
            widget = makeResultsWidget(widgetModel: widgetModel)
        }

        if let widget = widget {
            addChild(widget)
            widget.didMove(toParent: self)
            widget.view.translatesAutoresizingMaskIntoConstraints = false
            cell.contentView.addSubview(widget.view)

            NSLayoutConstraint.activate([
                widget.view.topAnchor.constraint(equalTo: cell.contentView.topAnchor),
                widget.view.leadingAnchor.constraint(equalTo: cell.contentView.leadingAnchor),
                widget.view.trailingAnchor.constraint(equalTo: cell.contentView.trailingAnchor),
                widget.view.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor, constant: -10)
            ])

            widgetIsInteractableByID[widgetModel.id] = false
        }

        cell.widget = widget

        return cell
    }
}

class WidgetTableViewCell: UITableViewCell {
    var widget: UIViewController?
}
