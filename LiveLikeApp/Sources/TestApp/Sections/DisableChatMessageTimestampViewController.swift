//
//  DisableChatMessageTimestampViewController.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 1/7/20.
//

import EngagementSDK
import UIKit

/// Tests the use-case of disabling the chat message timestamps
final class DisableChatMessageTimestampViewController: ChatTestCaseViewController {
    override func viewDidLoad() {
        vcTitle = "No Timestamp in Chat"
        super.viewDidLoad()
        chatController.messageTimestampFormatter = nil
    }
}
