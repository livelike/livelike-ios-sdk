//
//  WidgetsTestingViewController.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 1/27/20.
//

import EngagementSDK
import UIKit

class WidgetsTestingViewController: TestCaseViewController {
    let widgetViewController = WidgetPopupViewController()
    let widgetView = UIView()
    lazy var widgetCreateView = WidgetCreateView(
        programID: self.programID,
        apiToken: self.apiToken,
        timeoutSeconds: self.timeoutSeconds,
        apiOrigin: self.apiOrigin
    )
    var session: ContentSession?

    let sdk: EngagementSDK
    let apiToken: String
    let programID: String
    let theme: Theme
    let apiOrigin: URL
    private let timeoutSeconds: Int
    private var timeoutSecondsISO8601: String {
        return "P0DT00H00M\(timeoutSeconds)S"
    }

    private var widgetsTimeInterval: TimeInterval?

    init(
        sdk: EngagementSDK,
        apiToken: String,
        programID: String,
        theme: Theme,
        timeoutSeconds: Int,
        apiOrigin: URL,
        widgetsInterval: TimeInterval? = nil
    ) {
        self.sdk = sdk
        self.apiToken = apiToken
        self.programID = programID
        self.theme = theme
        self.timeoutSeconds = timeoutSeconds
        self.apiOrigin = apiOrigin
        widgetsTimeInterval = widgetsInterval
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "Widgets Test"
        vcInfo = "Use this view to test Widgets without having to use the produce site. This view replicates API calls that the produce site would be doing"

        if let widgetsTimeInterval = widgetsTimeInterval {
            vcTitle = "Randomized Widgets"
            vcInfo = "Use this view to test Widgets without having to use the produce site. This view replicates API calls that the produce site would be doing"
            widgetCreateView.randomizeWidgets(timeInterval: widgetsTimeInterval)
        }

        super.viewDidLoad()

        view.backgroundColor = .darkGray

        widgetView.translatesAutoresizingMaskIntoConstraints = false
        widgetCreateView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(widgetView)
        contentView.addSubview(widgetCreateView)
        addChild(viewController: widgetViewController, into: widgetView)
        applyLayoutConstraints()
        let config = SessionConfiguration(programID: programID)
        session = sdk.contentSession(config: config)
        widgetViewController.session = session
        widgetViewController.setTheme(theme)

        widgetView.backgroundColor = .gray
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        widgetCreateView.stopRandomizedWidgets()
    }

    func applyLayoutConstraints() {
        NSLayoutConstraint.activate([
            widgetView.topAnchor.constraint(equalTo: contentView.topAnchor),
            widgetView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.5),
            widgetView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            widgetCreateView.topAnchor.constraint(equalTo: widgetView.bottomAnchor),
            widgetCreateView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetCreateView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            widgetCreateView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }

    deinit {
        log.dev("deinit")
    }
}
