//
//  UserQuestsUseCase.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 6/29/22.
//

import UIKit
import EngagementSDK

class UserQuestsUseCase: TestCaseViewController {

    private var contentSession: ContentSession!
    private var sdk: EngagementSDK
    private var items: [UserQuest] = []
    private var questRequest = GetUserQuestsRequestOptions()

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.allowsSelection = true
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "myCell")
        return tableView
    }()

    private let loadMoreButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Load More", for: .normal)
        return button
    }()

    private let buttonStack: UIStackView = {
        let buttonStack = UIStackView()
        buttonStack.translatesAutoresizingMaskIntoConstraints = false
        buttonStack.axis = .vertical
        buttonStack.distribution = .fillEqually
        buttonStack.spacing = 10.0
        return buttonStack
    }()

    private let questIDInput: UITextField = {
        let questIDInput: UITextField = UITextField()
        questIDInput.placeholder = "Filter User Quest IDs separated by comma"
        questIDInput.translatesAutoresizingMaskIntoConstraints = false
        questIDInput.borderStyle = .line
        questIDInput.font = UIFont.systemFont(ofSize: 10.0)
        return questIDInput
    }()

    private let profileIDInput: UITextField = {
        let profileInput: UITextField = UITextField()
        profileInput.placeholder = "Enter Profile ID, if left empty will use current user"
        profileInput.translatesAutoresizingMaskIntoConstraints = false
        profileInput.borderStyle = .line
        profileInput.font = UIFont.systemFont(ofSize: 10.0)
        return profileInput
    }()

    private let statusStack: UIStackView = {
        let statusStack = UIStackView()
        statusStack.translatesAutoresizingMaskIntoConstraints = false
        statusStack.axis = .horizontal
        statusStack.distribution = .fillProportionally
        statusStack.spacing = 10.0
        return statusStack
    }()

    private let switchLabel: UILabel = {
        let switchLabel = UILabel()
        switchLabel.translatesAutoresizingMaskIntoConstraints = false
        switchLabel.text = "Set Status: GREEN = Compelted"
        return switchLabel
    }()

    private let statusSwitch: UISwitch = {
        let statusSwitch = UISwitch()
        statusSwitch.translatesAutoresizingMaskIntoConstraints = false
        return statusSwitch
    }()

    private let sendButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Submit Filter", for: .normal)
        button.backgroundColor = .green
        return button
    }()

    init(sdk: EngagementSDK, programID: String) {
        self.sdk = sdk
        self.contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "User Quests"

        super.viewDidLoad()

        buttonStack.addArrangedSubview(questIDInput)
        buttonStack.addArrangedSubview(profileIDInput)

        statusStack.addArrangedSubview(switchLabel)
        statusStack.addArrangedSubview(statusSwitch)

        buttonStack.addArrangedSubview(statusStack)
        buttonStack.addArrangedSubview(sendButton)

        view.addSubview(tableView)
        view.addSubview(loadMoreButton)
        view.addSubview(buttonStack)

        NSLayoutConstraint.activate([
            buttonStack.topAnchor.constraint(equalTo: contentView.topAnchor),
            buttonStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            buttonStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            buttonStack.heightAnchor.constraint(equalToConstant: 200),

            tableView.topAnchor.constraint(equalTo: buttonStack.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: loadMoreButton.topAnchor),

            loadMoreButton.topAnchor.constraint(equalTo: tableView.bottomAnchor),
            loadMoreButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            loadMoreButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            loadMoreButton.heightAnchor.constraint(equalToConstant: 60.0),
            loadMoreButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])

        loadMoreButton.addTarget(self, action: #selector(loadMoreButtonSelected), for: .touchUpInside)
        sendButton.addTarget(self, action: #selector(sentButtonSelected), for: .touchUpInside)

        tableView.delegate = self
        tableView.dataSource = self

        sdk.quests.getUserQuests(
            page: .first,
            options: questRequest
        ) { [weak self] result in

            guard let self = self else { return }

            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(let quests):
                self.items.append(contentsOf: quests)

                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }

    }

    private func resetAllButtons() {
    }

    @objc private func loadMoreButtonSelected() {
        sdk.quests.getUserQuests(
            page: .next,
            options: questRequest
        ) { [weak self] result in

            guard let self = self else { return }

            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(let quests):
                self.items.append(contentsOf: quests)

                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }

    @objc private func sentButtonSelected() {
        resetAllButtons()

        let stringQuestIDs: [String] = {
            if let text = questIDInput.text, text.count > 0 {

                let attrInput = text.replacingOccurrences(of: " ", with: "").lowercased()
                return attrInput.components(separatedBy: ",")
            } else {

                return []
            }
        }()

        let profileID: String? = {
            if let profileID = profileIDInput.text, profileID.count > 0 {

                return profileID
            } else {

                return nil
            }
        }()

        questRequest = GetUserQuestsRequestOptions(
            profileID: profileID,
            userQuestIDs: stringQuestIDs,
            status: statusSwitch.isOn ? .completed : .incomplete
        )

        sdk.quests.getUserQuests(
            page: .first,
            options: questRequest
        ) { [weak self] result in

            guard let self = self else { return }

            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(let quests):
                self.items.removeAll()
                self.items.append(contentsOf: quests)

                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
}

extension UserQuestsUseCase: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            var cell = tableView.dequeueReusableCell(withIdentifier: "myCell")
        else {
            return UITableViewCell()
        }

        cell = UITableViewCell(style: .subtitle, reuseIdentifier: "myCell")
        let userQuest = self.items[indexPath.row]
        cell.textLabel?.text = "\(userQuest.quest.name)"
        cell.detailTextLabel?.text = "Status: \(userQuest.status.rawValue)"

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let quest = items[indexPath.row]
        let questDetailVC = UserQuestDetailUseCase(sdk: sdk, userQuest: quest)
        self.navigationController?.pushViewController(questDetailVC, animated: true)
    }
}

class UserQuestDetailUseCase: TestCaseViewController {

    private var sdk: EngagementSDK
    private var items: [UserQuestTask] = []
    private let userQuest: UserQuest

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.allowsSelection = true
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "myCell")
        return tableView
    }()

    private let startQuestButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Complete User Quest", for: .normal)
        button.backgroundColor = .green
        return button
    }()

    init(sdk: EngagementSDK, userQuest: UserQuest) {
        self.sdk = sdk
        self.userQuest = userQuest
        self.items = userQuest.userQuestTasks
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "\(userQuest.id) Quest Tasks"

        super.viewDidLoad()

        view.addSubview(tableView)
        view.addSubview(startQuestButton)

        NSLayoutConstraint.activate([

            startQuestButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10.0),
            startQuestButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            startQuestButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            startQuestButton.heightAnchor.constraint(equalToConstant: 50.0),

            tableView.topAnchor.constraint(equalTo: startQuestButton.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
        ])

        startQuestButton.addTarget(self, action: #selector(completeUserQuestAction), for: .touchUpInside)

        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }

    @objc private func completeUserQuestAction() {

        let alert = UIAlertController(
            title: "Complete User Quest?",
            message: "Would you like to complete User Quest: \(userQuest.quest.name)",
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "No", style: .destructive))
        alert.addAction(UIAlertAction(title: "Yes", style: .default) { [weak self] _ in

            guard let self = self else { return }

            let userTaskIDs = self.userQuest.userQuestTasks.map({ $0.id })

            self.sdk.quests.updateUserQuestTasks(
                userQuestID: self.userQuest.id,
                userQuestTaskIDs: userTaskIDs,
                status: .completed
            ) { result in

                switch result {
                case .success(_):

                    let alert = UIAlertController(
                        title: "Success!",
                        message: "Quest [\(self.userQuest.quest.name)] Successfuly Completed",
                        preferredStyle: .alert
                    )
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                    self.present(alert, animated: true, completion: nil)

                case .failure(let error):

                    let alert = UIAlertController(
                        title: "Error!",
                        message: "\(error.localizedDescription)",
                        preferredStyle: .alert
                    )
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                    self.present(alert, animated: true, completion: nil)
                }

            }
        })

        self.present(alert, animated: true, completion: nil)

    }
}

extension UserQuestDetailUseCase: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            var cell = tableView.dequeueReusableCell(withIdentifier: "myCell")
        else {
            return UITableViewCell()
        }

        cell = UITableViewCell(style: .subtitle, reuseIdentifier: "myCell")
        let questTask = self.items[indexPath.row]
        cell.textLabel?.text = "\(questTask.questTask.name)"
        cell.detailTextLabel?.text = "Progress: \(questTask.progress) | Status: \(questTask.status.rawValue)"

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userTask = items[indexPath.row]

        let alert = UIAlertController(
            title: "Set User Quest Task Status",
            message: "",
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive))
        alert.addAction(UIAlertAction(title: "Set Progress", style: .default) { [weak self] _ in

            guard let self = self else { return }

            let alert = UIAlertController(
                title: "Set Progress",
                message: "Set Progress for task [\(userTask.questTask.name)]",
                preferredStyle: .alert
            )
            alert.addTextField()
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
            alert.addAction(UIAlertAction(title: "Set", style: .default) { [weak self] _ in

                guard let self = self else { return }

                if let progressTextField = alert.textFields?[0],
                   let progressString = progressTextField.text,
                   let progress = Float(progressString)
                {

                    self.sdk.quests.setUserQuestTaskProgress(
                        userQuestTaskID: userTask.id,
                        progress: progress
                    ) { result in

                        switch result {
                        case .success(_):

                            let alert = UIAlertController(
                                title: "Success!",
                                message: "User Task [\(userTask.questTask.name)] Progress Set",
                                preferredStyle: .alert
                            )
                            alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                            self.present(alert, animated: true, completion: nil)

                        case .failure(let error):

                            let alert = UIAlertController(
                                title: "Error!",
                                message: "\(error.localizedDescription)",
                                preferredStyle: .alert
                            )
                            alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                            self.present(alert, animated: true, completion: nil)
                        }

                    }
                }

            })
            self.present(alert, animated: true, completion: nil)

        })

        alert.addAction(UIAlertAction(title: "Increment by Default", style: .default) { [weak self] _ in

            guard let self = self else { return }

            self.sdk.quests.incrementUserQuestTaskProgress(
                userQuestTaskID: userTask.id,
                customIncrement: nil
            ) { result in

                switch result {
                case .success(_):

                    let alert = UIAlertController(
                        title: "Success!",
                        message: "User Task [\(userTask.questTask.name)] Incremented",
                        preferredStyle: .alert
                    )
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                    self.present(alert, animated: true, completion: nil)

                case .failure(let error):

                    let alert = UIAlertController(
                        title: "Error!",
                        message: "\(error.localizedDescription)",
                        preferredStyle: .alert
                    )
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        })

        alert.addAction(UIAlertAction(title: "Increment by Custom", style: .default) { [weak self] _ in

            guard let self = self else { return }

            let alert = UIAlertController(
                title: "Increment User Quest Task",
                message: "Increment by Custom Increment (please use float)",
                preferredStyle: .alert
            )
            alert.addTextField()
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
            alert.addAction(UIAlertAction(title: "Increment", style: .default) { [weak self] _ in

                guard let self = self else { return }

                if let incrementTextField = alert.textFields?[0],
                   let incrementString = incrementTextField.text,
                   let customIncrement = Float(incrementString)
                {

                    self.sdk.quests.incrementUserQuestTaskProgress(
                        userQuestTaskID: userTask.id,
                        customIncrement: customIncrement
                    ) { result in

                        switch result {
                        case .success(_):

                            let alert = UIAlertController(
                                title: "Success!",
                                message: "User Task [\(userTask.questTask.name)] Incremented",
                                preferredStyle: .alert
                            )
                            alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                            self.present(alert, animated: true, completion: nil)

                        case .failure(let error):

                            let alert = UIAlertController(
                                title: "Error!",
                                message: "\(error.localizedDescription)",
                                preferredStyle: .alert
                            )
                            alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }

            })
            self.present(alert, animated: true, completion: nil)
        })

        alert.addAction(UIAlertAction(title: "Incomplete", style: .default) { [weak self] _ in

            guard let self = self else { return }

            self.sdk.quests.updateUserQuestTasks(
                userQuestID: userTask.userQuestID,
                userQuestTaskIDs: [userTask.id],
                status: .incomplete
            ) { result in

                switch result {
                case .success(_):

                    let alert = UIAlertController(
                        title: "Success!",
                        message: "User Task [\(userTask.questTask.name)] marked Incomplete",
                        preferredStyle: .alert
                    )
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                    self.present(alert, animated: true, completion: nil)

                case .failure(let error):

                    let alert = UIAlertController(
                        title: "Error!",
                        message: "\(error.localizedDescription)",
                        preferredStyle: .alert
                    )
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                    self.present(alert, animated: true, completion: nil)
                }
            }

        })

        alert.addAction(UIAlertAction(title: "Completed", style: .default) { [weak self] _ in

            guard let self = self else { return }

            self.sdk.quests.updateUserQuestTasks(
                userQuestID: userTask.userQuestID,
                userQuestTaskIDs: [userTask.id],
                status: .completed
            ) { result in

                switch result {
                case .success(_):

                    let alert = UIAlertController(
                        title: "Success!",
                        message: "User Task [\(userTask.questTask.name)] marked Completed",
                        preferredStyle: .alert
                    )
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                    self.present(alert, animated: true, completion: nil)

                case .failure(let error):

                    let alert = UIAlertController(
                        title: "Error!",
                        message: "\(error.localizedDescription)",
                        preferredStyle: .alert
                    )
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                    self.present(alert, animated: true, completion: nil)
                }
            }

        })

        self.present(alert, animated: true, completion: nil)
    }
}
