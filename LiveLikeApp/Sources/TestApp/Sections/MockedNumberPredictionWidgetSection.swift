//
//  MockedNumberPredictionWidgetSection.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 9/29/21.
//

import UIKit
import EngagementSDK

class MockedNumberPredictionWidgetSection: TestCaseViewController {
    
    struct Row {
        var description: String
        var widget: NumberPredictionWidgetModel
    }
    
    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .equalSpacing
        return stackView
    }()
    
    let defaultStateController = DefaultWidgetStateController(closeButtonAction: {}, widgetFinishedCompletion: { _ in })
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentView.addSubview(scrollView)
        scrollView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: contentView.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        ])
        
        stackView.addArrangedSubview(label(text: "Always successful vote"))
        stackView.addArrangedSubview({
            let model = MockNumberPredictionWidgetModel()
            model.lockVotesResult = .success(
                NumberPredictionVote(
                    widgetID: "",
                    widgetKind: .imageNumberPrediction,
                    votes: [],
                    claimToken: ""
                )
            )
            guard let widget = DefaultWidgetFactory.makeWidget(from: .numberPrediction(model)) else {
                return UIView()
            }
            addChild(widget)
            return widget.view
        }())
        
        stackView.addArrangedSubview(label(text: "Always fail vote"))
        stackView.addArrangedSubview({
            let model = MockNumberPredictionWidgetModel()
            guard let widget = DefaultWidgetFactory.makeWidget(from: .numberPrediction(model)) else {
                return UIView()
            }
            addChild(widget)
            return widget.view
        }())
        
        stackView.addArrangedSubview(label(text: "number prediction w/ DefaultWidgetStateController"))
        stackView.addArrangedSubview({
            let model = MockNumberPredictionWidgetModel()
            model.lockVotesResult = .success(
                NumberPredictionVote(
                    widgetID: "",
                    widgetKind: .imageNumberPrediction,
                    votes: [],
                    claimToken: ""
                )
            )
            guard let widget = DefaultWidgetFactory.makeWidget(from: .numberPrediction(model)) else {
                return UIView()
            }
            widget.delegate = defaultStateController
            widget.moveToNextState()
            addChild(widget)
            return widget.view
        }())
        
        stackView.addArrangedSubview(label(text: "UI locked if already answered"))
        stackView.addArrangedSubview({
            let model = MockNumberPredictionWidgetModel()
            model.userVotes.append(
                NumberPredictionVote(
                    widgetID: model.id,
                    widgetKind: model.kind,
                    votes: model.options.map { option in
                        .init(
                            id: "vote\(option.id)",
                            optionID: option.id,
                            number: .random(in: 0 ... 10),
                            createdAt: Date()
                        )
                    },
                    claimToken: ""
                )
            )
            guard let widget = DefaultWidgetFactory.makeWidget(from: .numberPrediction(model)) else {
                return UIView()
            }
            
            addChild(widget)
            return widget.view
        }())
        
        stackView.addArrangedSubview(label(text: "UI locked if follow up published"))
        stackView.addArrangedSubview({
            let model = MockNumberPredictionWidgetModel()
            model.isFollowUpPublished = true
            guard let widget = DefaultWidgetFactory.makeWidget(from: .numberPrediction(model)) else {
                return UIView()
            }
            
            addChild(widget)
            return widget.view
        }())
        
        stackView.addArrangedSubview(label(text: "Follow up with user answer"))
        stackView.addArrangedSubview({
            let model = MockNumberPredictionFollowUpWidgetModel()
            let image = URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!
            model.options = [
                .init(id: "1", imageURL: image, text: "answered correctly", correctNumber: 1, url: image),
                .init(id: "2", imageURL: image, text: "answered incorrectly", correctNumber: 2, url: image),
            ]
            model.userVotes = [
                .init(
                    widgetID: model.id,
                    widgetKind: model.kind,
                    votes: [
                        .init(id: "vote-1", optionID: "1", number: 1, createdAt: Date()),
                        .init(id: "vote-2", optionID: "2", number: 3, createdAt: Date())
                    ],
                    claimToken: "claimToken"
                )
            ]
            guard let widget = DefaultWidgetFactory.makeWidget(from: .numberPredictionFollowUp(model)) else {
                return UIView()
            }
            addChild(widget)
            return widget.view
        }())
        
        stackView.addArrangedSubview(label(text: "Follow up without user answer"))
        stackView.addArrangedSubview({
            let model = MockNumberPredictionFollowUpWidgetModel()
            let image = URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!
            model.options = [
                .init(id: "1", imageURL: image, text: "answered correctly", correctNumber: 1, url: image),
                .init(id: "2", imageURL: image, text: "answered incorrectly", correctNumber: 2, url: image),
            ]
            model.userVotes = []
            guard let widget = DefaultWidgetFactory.makeWidget(from: .numberPredictionFollowUp(model)) else {
                return UIView()
            }
            addChild(widget)
            return widget.view
        }())
        
        stackView.addArrangedSubview(label(text: "Follow up w/ DefaultWidgetStateController"))
        stackView.addArrangedSubview({
            let model = MockNumberPredictionFollowUpWidgetModel()
            let image = URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!
            model.options = [
                .init(id: "1", imageURL: image, text: "answered correctly", correctNumber: 1, url: image),
                .init(id: "2", imageURL: image, text: "answered incorrectly", correctNumber: 2, url: image),
            ]
            model.userVotes = []
            guard let widget = DefaultWidgetFactory.makeWidget(from: .numberPredictionFollowUp(model)) else {
                return UIView()
            }
            widget.delegate = defaultStateController
            widget.moveToNextState()
            addChild(widget)
            return widget.view
        }())
    }
    
    func label(text: String) -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = text
        return label
    }
}

class MockNumberPredictionWidgetModel: NumberPredictionWidgetModel {
    func loadInteractionHistory(completion: @escaping (Result<[NumberPredictionVote], Error>) -> Void) { }
    
    struct MockError: Error { }
    static let image = URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!
    
    weak var delegate: NumberPredictionWidgetModelDelegate?
    var question: String = "question"
    var options: [NumberPredictionOption] = [
        .init(id: "1", imageURL: image, text: "option 1", correctNumber: nil, url: image),
        .init(id: "2", imageURL: image, text: "option 2", correctNumber: nil, url: image),
    ]
    var confirmationMessage: String = "confirmationMessage"
    var containsImages: Bool = true
    var userVotes: [NumberPredictionVote] = []
    var isFollowUpPublished: Bool = false
    var followUpWidgetModels: [NumberPredictionFollowUpWidgetModel] = []
    var id: String = "id"
    var kind: WidgetKind = .imageNumberPrediction
    var createdAt: Date = Date()
    var publishedAt: Date?
    var interactionTimeInterval: TimeInterval = 10
    var customData: String?
    var programID: String = "programID"
    var interactiveUntil: Date?
    var widgetAttributes: [Attribute] = []
    
    var lockVotesResult: Result<NumberPredictionVote, Error> = .failure(MockError())
    func lockVotes(
        submissions: [NumberPredictionVoteSubmission],
        completion: @escaping (Result<NumberPredictionVote, Error>) -> Void
    ) {
        completion(lockVotesResult)
    }
    
    func registerImpression(completion: @escaping (Result<Void, Error>) -> Void) { }
    func markAsInteractive() { }
    func getSecondsUntilInteractivityExpiration() -> TimeInterval? { return nil }

}

class MockNumberPredictionFollowUpWidgetModel: NumberPredictionFollowUpWidgetModel {
    
    struct MockError: Error { }
    static let image = URL(string: "https://livelike.com/wp-content/uploads/2019/07/Mike-Moloksher.jpg")!
    
    weak var delegate: NumberPredictionWidgetModelDelegate?
    var question: String = "question"
    var options: [NumberPredictionOption] = [
        .init(id: "1", imageURL: image, text: "option 1", correctNumber: 1, url: image),
        .init(id: "2", imageURL: image, text: "option 2", correctNumber: 2, url: image),
    ]
    var confirmationMessage: String = "confirmationMessage"
    var containsImages: Bool = true
    var userVotes: [NumberPredictionVote] = []
    var isFollowUpPublished: Bool = false
    var followUpWidgetModels: [NumberPredictionFollowUpWidgetModel] = []
    var id: String = "id"
    var kind: WidgetKind = .imageNumberPrediction
    var createdAt: Date = Date()
    var publishedAt: Date?
    var interactionTimeInterval: TimeInterval = 10
    var customData: String?
    var programID: String = "programID"
    var associatedPredictionID: String = "associatedPredictionID"
    var associatedPredictionKind: WidgetKind = .imageNumberPredictionFollowUp
    var interactiveUntil: Date?
    var widgetAttributes: [Attribute] = []
    
    var lockVotesResult: Result<NumberPredictionVote, Error> = .failure(MockError())
    func lockVotes(
        submissions: [NumberPredictionVoteSubmission],
        completion: @escaping (Result<NumberPredictionVote, Error>) -> Void
    ) {
        completion(lockVotesResult)
    }
    
    func registerImpression(completion: @escaping (Result<Void, Error>) -> Void) { }
    func markAsInteractive() { }
    func loadInteractionHistory(completion: @escaping (Result<[NumberPredictionVote], Error>) -> Void) { }
    func claimRewards(completion: @escaping (Result<[Reward], Error>) -> Void) { }
    func getSecondsUntilInteractivityExpiration() -> TimeInterval? { return nil }

}
