//
//  GetListOfInvitationsSection.swift
//  LiveLikeTestApp
//
//  Created by Keval Shah on 14/10/21.
//

import Foundation
import EngagementSDK
import UIKit

class GetListOfInvitationsSection: TestCaseViewController {
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 30
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textColor = .black
        return label
    }()
    
    private let invitationStatusTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Invitation Status"
        return textField
    }()
    
    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()
    
    private let getListOfInvitesForUserButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get List of Invitations Received", for: .normal)
        return button
    }()
    
    private let getListOfInvitesByUserButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get List of Invitations Sent", for: .normal)
        return button
    }()

    private var chatSession: ChatSession!
    private let contentSession: ContentSession
    private let baseURL: URL
    private let accessToken: String
    private let sdk: EngagementSDK
    
    private var invitations = [ChatRoomInvitation]()

    init(sdk: EngagementSDK, contentSession: ContentSession, baseURL: URL, accessToken: String) {
        self.sdk = sdk
        self.contentSession = contentSession
        self.baseURL = baseURL
        self.accessToken = accessToken
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vcTitle = "Add User to Chat Room"
        vcInfo = "Add another user to chatroom using member id and chatroom id"
        
        sdk.chat.delegate = self
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        
        self.getListOfInvitesForUserButton.addTarget(self, action: #selector(self.getListOfInvitationsForUserPressed), for: .touchUpInside)
        self.getListOfInvitesByUserButton.addTarget(self, action: #selector(self.getListOfInvitationsByUserPressed), for: .touchUpInside)
        
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .dark:
                label.textColor = .white
                invitationStatusTextField.textColor = .white
            case .light:
                label.textColor = .black
                invitationStatusTextField.textColor = .black
            case .unspecified:
                label.textColor = .black
                invitationStatusTextField.textColor = .black
            @unknown default:
                label.textColor = .black
                invitationStatusTextField.textColor = .black
            }
        }
        
        contentView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -(UIScreen.main.bounds.height / 2)),
        ])
        
        stackView.addArrangedSubview(label)
        stackView.addArrangedSubview(invitationStatusTextField)
        stackView.addArrangedSubview(getListOfInvitesForUserButton)
        stackView.addArrangedSubview(getListOfInvitesByUserButton)
        stackView.addArrangedSubview(tableView)
    }
    
    @objc private func getListOfInvitationsForUserPressed() {
        var inviteStatus: ChatRoomInvitationStatus = .pending
        if let status = invitationStatusTextField.text {
            if let invStatus = ChatRoomInvitationStatus(rawValue: status) {
                inviteStatus = invStatus
            }
        }
        
        sdk.chat.getInvitationsForUserWithInvitationStatus(
            invitationStatus: inviteStatus,
            page: .first
        ) { result in
            switch result {
            case .success(let chatRoomInvitations):
                if chatRoomInvitations.count < 1 {
                    self.showAlert(title: "No. of Invitations:", message: "\(chatRoomInvitations.count)")
                }
                self.invitations = chatRoomInvitations
                self.tableView.reloadData()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    @objc private func getListOfInvitationsByUserPressed() {
        var inviteStatus: ChatRoomInvitationStatus = .pending
        if let status = invitationStatusTextField.text {
            if let invStatus = ChatRoomInvitationStatus(rawValue: status) {
                inviteStatus = invStatus
            }
        }
        sdk.chat.getInvitationsByUserWithInvitationStatus(
            invitationStatus: inviteStatus,
            page: .first
        ) { result in
            switch result {
            case .success(let chatRoomInvitations):
                if chatRoomInvitations.count < 1 {
                    self.showAlert(title: "No. of Invitations:", message: "\(chatRoomInvitations.count)")
                }
                self.invitations = chatRoomInvitations
                self.tableView.reloadData()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    private func showAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: title,
                message: message,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func showInviteAlert(title: String, msg: String?, invitation: ChatRoomInvitation) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let alert = UIAlertController(
                title: title,
                message: msg ?? "",
                preferredStyle: .actionSheet
            )
            alert.addAction(UIAlertAction(title: "Accept", style: .default, handler: { _ in
                self.sdk.chat.updateChatRoomInviteStatus(chatRoomInvitation: invitation, invitationStatus: .accepted) { result in
                    switch result {
                    case .success(let invitation):
                        self.showAlert(
                            title: "Invitation Accepted",
                            message: "\(String(describing: invitation.chatRoom.title))"
                        )
                    case .failure(let error):
                        self.showAlert(title: "Failed to Accept", message: error.localizedDescription)
                    }
                }
            }))
            alert.addAction(UIAlertAction(title: "Reject", style: .destructive, handler: { _ in
                self.sdk.chat.updateChatRoomInviteStatus(chatRoomInvitation: invitation, invitationStatus: .rejected) { result in
                    switch result {
                    case .success(let invitation):
                        self.showAlert(
                            title: "Invitation Rejected",
                            message: "\(String(describing: invitation.chatRoom.title))"
                        )
                    case .failure(let error):
                        self.showAlert(title: "Failed to Accept", message: error.localizedDescription)
                    }
                }
            }))
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension GetListOfInvitationsSection: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invitations.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        var cellText: String = ""
        let rowData = invitations[indexPath.row]
        cellText = "Chat Room = \(String(describing: rowData.chatRoom.title))\nInvited By: \(rowData.invitedBy.nickname)"

        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = cellText
        return cell
    }
}

extension GetListOfInvitationsSection: ChatClientDelegate {
    func chatClient(_ chatClient: ChatClient, userDidBecomeMemberOfChatRoom newChatMembershipInfo: NewChatMembershipInfo) {
        self.showAlert(title: "Added to Chatroom", message: "You've been added to room: \(String(describing: newChatMembershipInfo.chatRoomTitle)) by \(newChatMembershipInfo.senderNickName)")
    }
    
    func chatClient(_ chatClient: ChatClient, userDidReceiveInvitation chatRoomInvitation: ChatRoomInvitation) {
        self.showInviteAlert(title: "Invitation Received", msg: "You've been invited to room: \(String(describing: chatRoomInvitation.chatRoom.title)) by \(chatRoomInvitation.invitedBy.nickname)", invitation: chatRoomInvitation)
    }
    
    func chatClient(_ chatClient: ChatClient, userDidGetBlocked blockInfo: BlockInfo) { }
    
    func chatClient(_ chatClient: ChatClient, userDidGetUnblocked unblockInfo: UnblockInfo) { }
}
