//
//  StandaloneChatViewController.swift
//  LiveLikeTestApp
//
//  Created by Heinrich Dahms on 2019-03-03.
//

import EngagementSDK
import UIKit

class ChatStandaloneViewController: UIViewController {
    let chatController = ChatViewController()
    var timer: DispatchSourceTimer?

    private var autoChatState: AutoChatState = .active {
        didSet {
            switch autoChatState {
            case .paused:
                timer?.cancel()
                timer = nil

            case .active:
                recreateTimer()
            }

            refreshNavigationItem()
        }
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        refreshNavigationItem()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        refreshNavigationItem()
    }

    deinit {
        timer = nil
    }
}

// MARK: - View Lifecycle

extension ChatStandaloneViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        chatController.setTheme(AppDelegate.theme)
        navigationController?.setNavigationBarHidden(false, animated: true)
        addChild(viewController: chatController, into: view)
        // chatController.session = Samples.mockChatSession

        recreateTimer()
    }
}

// MARK: - Private

private extension ChatStandaloneViewController {
    enum AutoChatState {
        case paused, active
    }

    @objc func toggleAutoChatState() {
        switch autoChatState {
        case .active:
            autoChatState = .paused

        case .paused:
            autoChatState = .active
        }
    }

    @objc func generateMessage() {
        // Samples.publishRandomMessage(on: chatController)
    }

    func refreshNavigationItem() {
        navigationItem.rightBarButtonItems = [
            UIBarButtonItem(
                barButtonSystemItem: .compose,
                target: self,
                action: #selector(generateMessage)
            ),

            autoChatState == .active
                ? UIBarButtonItem(
                    barButtonSystemItem: .stop,
                    target: self,
                    action: #selector(toggleAutoChatState)
                )
                : UIBarButtonItem(
                    barButtonSystemItem: .play,
                    target: self,
                    action: #selector(toggleAutoChatState)
                )
        ]
    }

    private func recreateTimer() {
        timer?.cancel()
        timer = DispatchSource.makeTimerSource()
        timer?.schedule(deadline: .now(), repeating: .seconds(5))
        timer?.setEventHandler { [weak self] in
            guard let self = self else { return }
            self.generateMessage()
        }
        timer?.resume()
    }
}
