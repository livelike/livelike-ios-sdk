//
//  AddToChatroomSection.swift
//  LiveLikeTestApp
//
//  Created by Keval Shah on 9/16/21.
//

import Foundation
import EngagementSDK
import UIKit

class AddToChatroomSection: TestCaseViewController {
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textColor = .black
        return label
    }()
    
    private let profileIdTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Profile Id"
        return textField
    }()
    
    private let chatRoomIdTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Chatroom Id"
        return textField
    }()
    
    private let addUserToChatroomButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Add User", for: .normal)
        return button
    }()
    
    private let inviteUserToChatroomButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Invite User", for: .normal)
        return button
    }()
    
    private let getListOfInvitesButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get Invites List", for: .normal)
        return button
    }()

    private var chatSession: ChatSession!
    private let contentSession: ContentSession
    private let baseURL: URL
    private let accessToken: String
    private let sdk: EngagementSDK

    init(sdk: EngagementSDK, contentSession: ContentSession, baseURL: URL, accessToken: String) {
        self.sdk = sdk
        self.contentSession = contentSession
        self.baseURL = baseURL
        self.accessToken = accessToken
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vcTitle = "Add User to Chat Room"
        vcInfo = "Add another user to chatroom using member id and chatroom id"
        
        profileIdTextField.delegate = self
        chatRoomIdTextField.delegate = self
        sdk.chat.delegate = self
        
        self.addUserToChatroomButton.addTarget(self, action: #selector(self.addUserToChatroomPressed), for: .touchUpInside)
        self.inviteUserToChatroomButton.addTarget(self, action: #selector(self.inviteUserToChatroomPressed), for: .touchUpInside)
        self.getListOfInvitesButton.addTarget(self, action: #selector(self.getListOfInvitationsPressed), for: .touchUpInside)
        
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .dark:
                label.textColor = .white
                profileIdTextField.textColor = .white
                chatRoomIdTextField.textColor = .white
            case .light:
                label.textColor = .black
                profileIdTextField.textColor = .black
                chatRoomIdTextField.textColor = .black
            case .unspecified:
                label.textColor = .black
                profileIdTextField.textColor = .black
                chatRoomIdTextField.textColor = .black
            @unknown default:
                label.textColor = .black
                profileIdTextField.textColor = .black
                chatRoomIdTextField.textColor = .black
            }
        }
        
        contentView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -(UIScreen.main.bounds.height / 2)),
        ])
        
        stackView.addArrangedSubview(label)
        stackView.addArrangedSubview(profileIdTextField)
        stackView.addArrangedSubview(chatRoomIdTextField)
        stackView.addArrangedSubview(addUserToChatroomButton)
        stackView.addArrangedSubview(inviteUserToChatroomButton)
        stackView.addArrangedSubview(getListOfInvitesButton)
    }
    
    @objc private func addUserToChatroomPressed() {
        if let roomID = self.chatRoomIdTextField.text, let profileID = profileIdTextField.text {
            sdk.chat.addNewMemberToChatRoom(roomID: roomID, profileID: profileID) { [weak self] result in
                DispatchQueue.main.async {
                    guard let self = self else { return }
                    switch result {
                    case .success(let member):
                        self.showAlert(title: "Now Member", message: member.url.absoluteString)
                    case let .failure(error):
                        self.showAlert(title: "Error", message: error.localizedDescription)
                    }
                }
            }
        }
    }
    
    @objc private func inviteUserToChatroomPressed() {
        if let roomID = self.chatRoomIdTextField.text, let profileID = profileIdTextField.text {
            sdk.chat.sendChatRoomInviteToUser(roomID: roomID, profileID: profileID) { [weak self] result in
                DispatchQueue.main.async {
                    guard let self = self else { return }
                    switch result {
                    case .success(let invitation):
                        self.showAlert(title: "Invitation Sent", message: invitation.chatRoom.title ?? "")
                    case let .failure(error):
                        self.showAlert(title: "Error", message: error.localizedDescription)
                    }
                }
            }
        }
    }
    
    @objc private func getListOfInvitationsPressed() {
        sdk.chat.getInvitationsForUserWithInvitationStatus(
            invitationStatus: .pending,
            page: .first
        ) { result in
            switch result {
            case .success(let chatRoomInvitations):
                self.showAlert(title: "Chat Room Invitations Recieved", message: "No: \(chatRoomInvitations.count)")
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    private func showAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: title,
                message: message,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func showInviteAlert(title: String, msg: String?, invitation: ChatRoomInvitation) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let alert = UIAlertController(
                title: title,
                message: msg ?? "",
                preferredStyle: .actionSheet
            )
            alert.addAction(UIAlertAction(title: "Accept", style: .default, handler: { _ in
                self.sdk.chat.updateChatRoomInviteStatus(chatRoomInvitation: invitation, invitationStatus: .accepted) { result in
                    switch result {
                    case .success(let invitation):
                        self.showAlert(
                            title: "Invitation Accepted",
                            message: "\(String(describing: invitation.chatRoom.title))"
                        )
                    case .failure(let error):
                        self.showAlert(
                            title: "Failed to Accept",
                            message: error.localizedDescription
                        )
                    }
                }
            }))
            alert.addAction(UIAlertAction(title: "Reject", style: .destructive, handler: { _ in
                self.sdk.chat.updateChatRoomInviteStatus(chatRoomInvitation: invitation, invitationStatus: .rejected) { result in
                    switch result {
                    case .success(let invitation):
                        self.showAlert(
                            title: "Invitation Rejected",
                            message: "\(String(describing: invitation.chatRoom.title))"
                        )
                    case .failure(let error):
                        self.showAlert(
                            title: "Failed to Accept",
                            message: error.localizedDescription
                        )
                    }
                }
            }))
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension AddToChatroomSection: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension AddToChatroomSection: ChatClientDelegate {
    func chatClient(_ chatClient: ChatClient, userDidBecomeMemberOfChatRoom newChatMembershipInfo: NewChatMembershipInfo) {
        self.showAlert(title: "Added to Chatroom", message: "You've been added to room: \(String(describing: newChatMembershipInfo.chatRoomTitle)) by \(newChatMembershipInfo.senderNickName)")
    }
    
    func chatClient(_ chatClient: ChatClient, userDidReceiveInvitation chatRoomInvitation: ChatRoomInvitation) {
        self.showInviteAlert(title: "Invitation Received", msg: "You've been invited to room: \(String(describing: chatRoomInvitation.chatRoom.title)) by \(chatRoomInvitation.invitedBy.nickname)", invitation: chatRoomInvitation)
    }
    
    func chatClient(_ chatClient: ChatClient, userDidGetBlocked blockInfo: BlockInfo) {
        
    }
    
    func chatClient(_ chatClient: ChatClient, userDidGetUnblocked unblockInfo: UnblockInfo) {
        
    }
}
