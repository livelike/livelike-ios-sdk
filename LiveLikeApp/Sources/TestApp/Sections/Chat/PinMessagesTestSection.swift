//
//  PinMessagesTestSection.swift
//  LiveLikeTestApp
//
//  Created by Keval Shah on 07/12/21.
//

import EngagementSDK
import UIKit

class PinMessagesTestSection: TestCaseViewController {
    private let sdk: EngagementSDK
    private let envBaseURL: URL
    private var chatSession: ChatSession? {
        didSet {
            if let chatSession = chatSession {
                roomIdLabel.text = "id: \(chatSession.roomID)"
                roomTitleLabel.text = "title: \(chatSession.title ?? "No Title")"
                chatSession.addDelegate(self)
            }
        }
    }

    private let chatViewController = ChatViewController()
    private let createRoomButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Create Chat Room", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private let connectRoomButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Connect Room", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private let connectRoomField: UITextField = {
        let roomTitle: UITextField = UITextField()
        roomTitle.placeholder = "Enter Chat Room ID"
        roomTitle.translatesAutoresizingMaskIntoConstraints = false
        roomTitle.borderStyle = .line
        roomTitle.font = UIFont.systemFont(ofSize: 10.0)
        return roomTitle
    }()

    private let roomTitle: UITextField = {
        let roomTitle: UITextField = UITextField()
        roomTitle.placeholder = "Chat Room Title"
        roomTitle.translatesAutoresizingMaskIntoConstraints = false
        roomTitle.borderStyle = .line
        roomTitle.font = UIFont.systemFont(ofSize: 12.0)
        return roomTitle
    }()

    private let roomIdLabel: UILabel = {
        let roomIdLabel = UILabel()
        roomIdLabel.textColor = .white
        roomIdLabel.translatesAutoresizingMaskIntoConstraints = false
        roomIdLabel.font = UIFont.systemFont(ofSize: 8.0)
        return roomIdLabel
    }()

    private let roomTitleLabel: UILabel = {
        let roomTitleLabel = UILabel()
        roomTitleLabel.textColor = .white
        roomTitleLabel.font = UIFont.systemFont(ofSize: 8.0)
        roomTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        return roomTitleLabel
    }()

    private let visiblityPicker: UIPickerView = {
        let picker = UIPickerView()
        picker.translatesAutoresizingMaskIntoConstraints = false
        return picker
    }()

    private let pinMessagesButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Pin Message", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private let messageField: UITextField = {
        let roomTitle: UITextField = UITextField()
        roomTitle.placeholder = "Enter message to pin"
        roomTitle.translatesAutoresizingMaskIntoConstraints = false
        roomTitle.borderStyle = .line
        roomTitle.font = UIFont.systemFont(ofSize: 10.0)
        return roomTitle
    }()

    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()

    private let getListOfPinMessagesButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get Pin Messages", for: .normal)
        return button
    }()

    private var pinMessages = [PinMessageInfo]()

    init(envBaseURL: URL, sdk: EngagementSDK) {
        self.envBaseURL = envBaseURL
        self.sdk = sdk
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        visiblityPicker.delegate = self
        visiblityPicker.dataSource = self

        chatViewController.view.translatesAutoresizingMaskIntoConstraints = false

        addChild(chatViewController)

        contentView.addSubview(chatViewController.view)
        contentView.addSubview(createRoomButton)
        contentView.addSubview(roomTitle)
        contentView.addSubview(connectRoomButton)
        contentView.addSubview(connectRoomField)
        contentView.addSubview(roomIdLabel)
        contentView.addSubview(roomTitleLabel)
        contentView.addSubview(visiblityPicker)
        contentView.addSubview(pinMessagesButton)
        contentView.addSubview(messageField)
        contentView.addSubview(getListOfPinMessagesButton)
        contentView.addSubview(tableView)

        NSLayoutConstraint.activate([
            chatViewController.view.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 350.0),
            chatViewController.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            chatViewController.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            chatViewController.view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

            tableView.topAnchor.constraint(equalTo: pinMessagesButton.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: chatViewController.view.topAnchor),

            getListOfPinMessagesButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            getListOfPinMessagesButton.heightAnchor.constraint(equalToConstant: 60.0),
            getListOfPinMessagesButton.widthAnchor.constraint(equalToConstant: 150.0),
            getListOfPinMessagesButton.topAnchor.constraint(equalTo: tableView.topAnchor, constant: 20),

            createRoomButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20.0),
            createRoomButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20.0),
            createRoomButton.heightAnchor.constraint(equalToConstant: 60.0),
            createRoomButton.widthAnchor.constraint(equalToConstant: 150.0),
            roomTitle.leadingAnchor.constraint(equalTo: createRoomButton.trailingAnchor, constant: 10.0),
            roomTitle.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            roomTitle.topAnchor.constraint(equalTo: createRoomButton.topAnchor),

            visiblityPicker.leadingAnchor.constraint(equalTo: createRoomButton.trailingAnchor, constant: 10.0),
            visiblityPicker.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            visiblityPicker.topAnchor.constraint(equalTo: roomTitle.bottomAnchor, constant: 0.0),
            visiblityPicker.heightAnchor.constraint(equalToConstant: 40.0),

            connectRoomButton.topAnchor.constraint(equalTo: createRoomButton.bottomAnchor, constant: 5.0),
            connectRoomButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20.0),
            connectRoomButton.heightAnchor.constraint(equalToConstant: 50.0),
            connectRoomButton.widthAnchor.constraint(equalToConstant: 100.0),
            connectRoomField.leadingAnchor.constraint(equalTo: connectRoomButton.trailingAnchor, constant: 10.0),
            connectRoomField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            connectRoomField.centerYAnchor.constraint(equalTo: connectRoomButton.centerYAnchor),

            pinMessagesButton.topAnchor.constraint(equalTo: connectRoomButton.bottomAnchor, constant: 5.0),
            pinMessagesButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20.0),
            pinMessagesButton.heightAnchor.constraint(equalToConstant: 50.0),
            pinMessagesButton.widthAnchor.constraint(equalToConstant: 100.0),
            messageField.leadingAnchor.constraint(equalTo: pinMessagesButton.trailingAnchor, constant: 10.0),
            messageField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            messageField.centerYAnchor.constraint(equalTo: pinMessagesButton.centerYAnchor),

            roomIdLabel.topAnchor.constraint(equalTo: chatViewController.view.topAnchor, constant: 10.0),
            roomIdLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            roomTitleLabel.topAnchor.constraint(equalTo: roomIdLabel.bottomAnchor, constant: 5.0),
            roomTitleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0)

        ])

        createRoomButton.addTarget(self, action: #selector(createChatRoom), for: .touchUpInside)
        connectRoomButton.addTarget(self, action: #selector(connectRoom), for: .touchUpInside)
        pinMessagesButton.addTarget(self, action: #selector(pinMessage), for: .touchUpInside)
        getListOfPinMessagesButton.addTarget(self, action: #selector(getPinMessageList), for: .touchUpInside)

        messageField.delegate = self
        connectRoomField.delegate = self
        roomTitle.delegate = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self

        contentView.bringSubviewToFront(getListOfPinMessagesButton)

        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc private func applicationWillEnterForeground() {}

    @objc private func createChatRoom() {
        let selectedRow = visiblityPicker.selectedRow(inComponent: 0)
        let visibility: ChatRoomVisibilty = ChatRoomVisibilty.allCases[selectedRow]
        sdk.createChatRoom(
            title: roomTitle.text!.count > 0 ? roomTitle.text : nil,
            visibility: visibility
        ) { result in
            switch result {
            case let .success(chatRoomID):
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    let alert = UIAlertController(
                        title: "Room Created",
                        message: "ID: \(chatRoomID)",
                        preferredStyle: .alert
                    )

                    alert.addAction(UIAlertAction(title: "Connect Created Room", style: .default, handler: { _ in
                        self.connectRoomField.text = chatRoomID
                        self.connectRoom()
                    }))
                    alert.addAction(UIAlertAction(title: "Test Get Info API", style: .default, handler: { _ in
                        self.connectRoomField.text = chatRoomID
                        self.getInfoOnChatRoom()
                    }))
                    alert.addAction(UIAlertAction(title: "Copy Id To Clipboard", style: .default, handler: { _ in
                        UIPasteboard.general.string = chatRoomID
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            case let .failure(error):
                log.dev("Error: \(error.localizedDescription)")
            }
        }
    }

    private func getInfoOnChatRoom() {
        sdk.getChatRoomInfo(roomID: connectRoomField.text!) { result in
            switch result {
            case let .success(chatInfo):
                self.showAlert(title: "Chat Room Info", msg: "title: \(chatInfo.title ?? "No Title") \nid:\(chatInfo.id) \nVisibility: \(chatInfo.visibility)")
            case let .failure(error):
                log.dev("Error: \(error.localizedDescription)")
            }
        }
    }

    @objc private func connectRoom() {
        guard let chatRoomID = connectRoomField.text,
              chatRoomID.count > 0
        else {
            showAlert(title: "Enter Room ID", msg: nil)
            return
        }
        let config = ChatSessionConfig(roomID: chatRoomID)
        sdk.connectChatRoom(config: config, completion: { [weak self] result in
            DispatchQueue.main.async {
                guard let self = self else { return }
                switch result {
                case let .success(chatSession):
                    self.chatSession = chatSession
                    self.chatViewController.setChatSession(chatSession)
                    self.showAlert(title: "Connected Room ID", msg: chatSession.roomID)
                case let .failure(error):
                    self.showAlert(title: "Error", msg: error.localizedDescription)
                }
            }
        })
    }

    @objc private func pinMessage() {
        let messageString: String = self.messageField.text!.isEmpty ? "iOS Test Pin Chat Message" : messageField.text!

        guard let session = chatSession else {
            showAlert(title: "Connect Chat Room", msg: nil)
            return
        }

        session.sendCustomMessage(messageString) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                self.showAlert(title: "Error!", msg: "Failed to send custom message with error: \(error.localizedDescription)")
            case .success(let message):
                session.pinMessage(message) { [weak self] result in
                    guard let self = self else { return }
                    switch result {
                    case .failure(let error):
                        self.showAlert(title: "Error", msg: "Failed to send custom message with error: \(error.localizedDescription)")
                    case .success:
                        DispatchQueue.main.async {
                            let alert = UIAlertController(
                                title: "Success!",
                                message: "Pinned message successfully",
                                preferredStyle: .alert
                            )
                            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }

    private func unpinMessage(withMessageID messageID: String) {
        guard let session = chatSession else {
            showAlert(title: "Connect Chat Room", msg: nil)
            return
        }

        session.unpinMessage(pinMessageInfoID: messageID) { result in
            switch result {
            case .success:
                self.showAlert(title: "Unpinned Message", msg: messageID)
            case .failure(let error):
                self.showAlert(title: "Error", msg: "Error Unpinning Message \(messageID): \(error.localizedDescription)")
            }
        }
    }

    @objc private func getPinMessageList() {
        guard let session = chatSession else {
            showAlert(title: "Connect Chat Room", msg: nil)
            return
        }

        session.getPinMessageInfoList(orderBy: .ascending, page: .first) { result in
            switch result {
            case .success(let items):
                self.pinMessages = items
                self.tableView.reloadData()
            case .failure(let error):
                self.showAlert(title: "Error", msg: "Failed to get pinned messages with error: \(error.localizedDescription)")
            }
        }
    }

    private func showAlert(title: String, msg: String?) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let alert = UIAlertController(
                title: title,
                message: msg ?? "",
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension PinMessagesTestSection: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return ChatRoomVisibilty.allCases.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "Visibility: \(ChatRoomVisibilty.allCases[row].rawValue)"
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var title = UILabel()
        if let view = view as? UILabel {
            title = view
        }
        title.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold)
        title.textColor = UIColor.blue
        title.text = "Visibility: \(ChatRoomVisibilty.allCases[row].rawValue)"
        title.textAlignment = .center

        return title
    }
}

extension PinMessagesTestSection: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pinMessages.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        var cellText: String = ""
        let rowData = pinMessages[indexPath.row]
        cellText = "Pinned Message:\(String(describing: rowData.messagePayload.message))\nBlock Id: \(rowData.id)"

        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = cellText
        return cell
    }
}

extension PinMessagesTestSection: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowData = pinMessages[indexPath.row]
        self.unpinMessage(withMessageID: rowData.id)
    }
}

extension PinMessagesTestSection: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}

extension PinMessagesTestSection: ChatSessionDelegate {
    func chatSession(_ chatSession: ChatSession, didRecieveNewMessage message: ChatMessage) { }

    func chatSession(_ chatSession: ChatSession, didRecieveRoomUpdate chatRoom: ChatRoomInfo) { }

    func chatSession(_ chatSession: ChatSession, didPinMessage message: PinMessageInfo) {
        self.showAlert(title: "Message Pinned", msg: message.messagePayload.id)
    }

    func chatSession(_ chatSession: ChatSession, didUnpinMessage pinMessageInfoID: String) {
        self.showAlert(title: "Message Unpinned", msg: pinMessageInfoID)
    }

    func chatSession(_ chatSession: ChatSession, didDeleteMessage messageID: ChatMessageID) {
        self.showAlert(title: "Chat Message Deleted", msg: messageID.asString)
    }
}
