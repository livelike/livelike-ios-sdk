//
//  BlockProfilesSection.swift
//  LiveLikeTestApp
//
//  Created by Keval Shah on 27/10/21.
//

import Foundation
import EngagementSDK
import UIKit

class BlockProfilesSection: TestCaseViewController {
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 30
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textColor = .black
        return label
    }()
    
    private let profileIdTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Profile Id to Block"
        return textField
    }()
    
    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()
    
    private let blockProfileButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Block Profile", for: .normal)
        return button
    }()
    
    private let getListOfBlockedProfilesButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get List of Blocked Profiles", for: .normal)
        return button
    }()
    
    private let getBlockedProfileInfoButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get Block Profile Info", for: .normal)
        return button
    }()

    private var chatSession: ChatSession!
    private let contentSession: ContentSession
    private let baseURL: URL
    private let accessToken: String
    private let sdk: EngagementSDK
    
    private var profiles = [BlockInfo]()

    init(sdk: EngagementSDK, contentSession: ContentSession, baseURL: URL, accessToken: String) {
        self.sdk = sdk
        self.contentSession = contentSession
        self.baseURL = baseURL
        self.accessToken = accessToken
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vcTitle = "Add User to Chat Room"
        vcInfo = "Add another user to chatroom using member id and chatroom id"
        
        sdk.chat.delegate = self
        tableView.delegate = self
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        
        self.blockProfileButton.addTarget(self, action: #selector(self.blockProfileButtonPressed), for: .touchUpInside)
        self.getListOfBlockedProfilesButton.addTarget(self, action: #selector(self.getListOfBlockedProfilesButtonPressed), for: .touchUpInside)
        self.getBlockedProfileInfoButton.addTarget(self, action: #selector(self.getBlockedProfileInfoButtonPressed), for: .touchUpInside)
        
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .dark:
                label.textColor = .white
                profileIdTextField.textColor = .white
            case .light:
                label.textColor = .black
                profileIdTextField.textColor = .black
            case .unspecified:
                label.textColor = .black
                profileIdTextField.textColor = .black
            @unknown default:
                label.textColor = .black
                profileIdTextField.textColor = .black
            }
        }
        
        contentView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 20),
        ])
        
        stackView.addArrangedSubview(label)
        stackView.addArrangedSubview(profileIdTextField)
        stackView.addArrangedSubview(blockProfileButton)
        stackView.addArrangedSubview(getListOfBlockedProfilesButton)
        stackView.addArrangedSubview(getBlockedProfileInfoButton)
        stackView.addArrangedSubview(tableView)
    }
    
    @objc private func blockProfileButtonPressed() {
        guard let profileID = profileIdTextField.text else {
            self.showAlert(title: "Profile ID Missing", message: "Please Enter Profile ID")
            return
        }
        sdk.user.blockProfile(
            profileID: profileID
        ) { result in
            switch result {
            case .success(let blockedProfile):
                self.showAlert(
                    title: "Profile Blocked",
                    message: "User: \(blockedProfile.blockedProfile.nickname)\nId:\(blockedProfile.blockedProfileID)"
                )
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    private func unblockProfile(withRequestID requestID: String) {
        sdk.user.unblockProfile(blockRequestID: requestID) { result in
            switch result {
            case .success:
                break
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func getListOfBlockedProfilesButtonPressed() {
        let profileID = profileIdTextField.text
        sdk.user.getBlockedProfileList(blockedProfileID: profileID, page: .first) { result in
            switch result {
            case .success(let blockedProfiles):
                if blockedProfiles.count > 1 {
                    self.showAlert(
                        title: "No. of Profiles:",
                        message: "\(blockedProfiles.count)"
                    )
                }
                self.profiles = blockedProfiles
                self.tableView.reloadData()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    @objc private func getBlockedProfileInfoButtonPressed() {
        let profileID = profileIdTextField.text ?? ""
        sdk.user.getProfileBlockInfo(profileID: profileID) { result in
            switch result {
            case .success(let blockInfo):
                self.profiles.append(blockInfo)
                self.tableView.reloadData()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    private func showAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: title,
                message: message,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func showInviteAlert(title: String, msg: String?, invitation: ChatRoomInvitation) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let alert = UIAlertController(
                title: title,
                message: msg ?? "",
                preferredStyle: .actionSheet
            )
            alert.addAction(UIAlertAction(title: "Accept", style: .default, handler: { _ in
                self.sdk.chat.updateChatRoomInviteStatus(chatRoomInvitation: invitation, invitationStatus: .accepted) { result in
                    switch result {
                    case .success(let invitation):
                        self.showAlert(
                            title: "Invitation Accepted",
                            message: "\(String(describing: invitation.chatRoom.title))"
                        )
                    case .failure(let error):
                        self.showAlert(title: "Failed to Accept", message: error.localizedDescription)
                    }
                }
            }))
            alert.addAction(UIAlertAction(title: "Reject", style: .destructive, handler: { _ in
                self.sdk.chat.updateChatRoomInviteStatus(chatRoomInvitation: invitation, invitationStatus: .rejected) { result in
                    switch result {
                    case .success(let invitation):
                        self.showAlert(
                            title: "Invitation Rejected",
                            message: "\(String(describing: invitation.chatRoom.title))"
                        )
                    case .failure(let error):
                        self.showAlert(title: "Failed to Accept", message: error.localizedDescription)
                    }
                }
            }))
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension BlockProfilesSection: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profiles.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        var cellText: String = ""
        let rowData = profiles[indexPath.row]
        cellText = "Blocked User:\(String(describing: rowData.blockedProfile.nickname))\nBlock Id: \(rowData.id)"

        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = cellText
        return cell
    }
}

extension BlockProfilesSection: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowData = profiles[indexPath.row]
        self.unblockProfile(withRequestID: rowData.id)
    }
}

extension BlockProfilesSection: ChatClientDelegate {
    func chatClient(_ chatClient: ChatClient, userDidBecomeMemberOfChatRoom newChatMembershipInfo: NewChatMembershipInfo) {
        self.showAlert(title: "Added to Chatroom", message: "You've been added to room: \(String(describing: newChatMembershipInfo.chatRoomTitle)) by \(newChatMembershipInfo.senderNickName)")
    }
    
    func chatClient(_ chatClient: ChatClient, userDidReceiveInvitation chatRoomInvitation: ChatRoomInvitation) {
        self.showInviteAlert(title: "Invitation Received", msg: "You've been invited to room: \(String(describing: chatRoomInvitation.chatRoom.title)) by \(chatRoomInvitation.invitedBy.nickname)", invitation: chatRoomInvitation)
    }
    
    func chatClient(_ chatClient: ChatClient, userDidGetBlocked blockInfo: BlockInfo) {
        self.showAlert(title: "User Blocked", message: "User Name: \(blockInfo.blockedProfile.nickname)")
    }
    
    func chatClient(_ chatClient: ChatClient, userDidGetUnblocked unblockInfo: UnblockInfo) {
        self.showAlert(title: "User Unblocked", message: "User Name: \(unblockInfo.blockedProfileID)")
    }
}
