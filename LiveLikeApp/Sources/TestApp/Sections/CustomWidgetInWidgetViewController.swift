//
//  CustomWidgetInWidgetViewController.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 10/29/20.
//

import EngagementSDK
import UIKit

class CustomWidgetInWidgetViewController: TestCaseViewController {
    private let widgetVC = WidgetPopupViewController()
    private let alertContainer = UIView()
    private var currentAlertInContainer: CustomAlertWidgetVC?
    private let widgetRemote: WidgetCreateView

    private let sdk: EngagementSDK
    private let session: ContentSession

    init(sdk: EngagementSDK, programID: String, apiToken: String) {
        self.sdk = sdk
        widgetRemote = WidgetCreateView(
            programID: programID,
            apiToken: apiToken,
            timeoutSeconds: 15,
            apiOrigin: sdk.currentConfiguration.apiOrigin
        )
        session = sdk.contentSession(config: SessionConfiguration(programID: programID))
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        alertContainer.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(alertContainer)

        addChild(widgetVC)
        widgetVC.didMove(toParent: self)
        widgetVC.view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(widgetVC.view)

        widgetRemote.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(widgetRemote)

        NSLayoutConstraint.activate([
            alertContainer.topAnchor.constraint(equalTo: contentView.topAnchor),
            alertContainer.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            alertContainer.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            alertContainer.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.2),

            widgetVC.view.topAnchor.constraint(equalTo: alertContainer.bottomAnchor),
            widgetVC.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetVC.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            widgetVC.view.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.4),

            widgetRemote.topAnchor.constraint(equalTo: widgetVC.view.bottomAnchor),
            widgetRemote.leadingAnchor.constraint(equalTo: widgetVC.view.leadingAnchor),
            widgetRemote.trailingAnchor.constraint(equalTo: widgetVC.view.trailingAnchor),
            widgetRemote.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.4)
        ])

        widgetVC.delegate = self
        widgetVC.session = session
    }
}

extension CustomWidgetInWidgetViewController: WidgetPopupViewControllerDelegate {
    func widgetViewController(_ widgetViewController: WidgetPopupViewController, willDisplay widget: Widget) {}

    func widgetViewController(_ widgetViewController: WidgetPopupViewController, didDisplay widget: Widget) {}

    func widgetViewController(_ widgetViewController: WidgetPopupViewController, willDismiss widget: Widget) {}

    func widgetViewController(_ widgetViewController: WidgetPopupViewController, didDismiss widget: Widget) {}

    func widgetViewController(_ widgetViewController: WidgetPopupViewController, willEnqueueWidget widgetModel: WidgetModel) -> Widget? {
        switch widgetModel {
        case let .alert(model):
            currentAlertInContainer?.removeFromParent()
            currentAlertInContainer?.view.removeFromSuperview()
            currentAlertInContainer = nil

            let customAlert = CustomAlertWidgetVC(alertWidgetViewModel: model)
            addChild(customAlert)
            customAlert.didMove(toParent: self)
            customAlert.view.translatesAutoresizingMaskIntoConstraints = false
            alertContainer.addSubview(customAlert.view)

            NSLayoutConstraint.activate([
                customAlert.view.topAnchor.constraint(equalTo: alertContainer.topAnchor),
                customAlert.view.leadingAnchor.constraint(equalTo: alertContainer.leadingAnchor),
                customAlert.view.trailingAnchor.constraint(equalTo: alertContainer.trailingAnchor),
                customAlert.view.bottomAnchor.constraint(equalTo: alertContainer.bottomAnchor)
            ])

            currentAlertInContainer = customAlert
            return nil
        default:
            return DefaultWidgetFactory.makeWidget(from: widgetModel)
        }
    }
}
