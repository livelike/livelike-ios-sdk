//
//  IntegratorTriggersWidgetViewController.swift
//  LiveLikeApp
//
//  Created by Mike Moloksher on 7/13/20.
//

import EngagementSDK
import UIKit

class IntegratorTriggersWidgetViewController: TestCaseViewController {
    private var sdk: EngagementSDK!
    private var session: ContentSession!

    private let clientID: String
    private let programID: String

    private var currentWidget: Widget?
    private var shownWidgets: [Widget] = []

    private let localDBKey = "livelikeTempWidgetDB"

    let getWidgetButton: UIButton = {
        let button = UIButton()
        button.setTitle("Trigger Widget", for: .normal)
        button.backgroundColor = .lightGray
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    let widgetIDTextField: UITextField = {
        let textField: UITextField = UITextField()
        textField.placeholder = "Enter Widget ID"
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.borderStyle = .line
        textField.font = UIFont.systemFont(ofSize: 10.0)
        return textField
    }()

    let widgetKindSelect: UIPickerView = {
        let picker = UIPickerView()
        picker.translatesAutoresizingMaskIntoConstraints = false
        return picker
    }()

    private let widgetView: UIView = {
        let widgetView = UIView()
        widgetView.translatesAutoresizingMaskIntoConstraints = false
        widgetView.isHidden = true
        return widgetView
    }()

    private var currentStateLabel: UILabel = {
        let label = UILabel()
        label.text = "Current State: "
        label.isHidden = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private var widgetKindSelectLabel: UILabel = {
        let label = UILabel()
        label.text = "Select Widget Kind: "
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let moveToNextStateBtn: UIButton = {
        let button = UIButton()
        button.setTitle("Move To Next State", for: .normal)
        button.backgroundColor = .lightGray
        button.isHidden = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    let manualStateLabel: UILabel = {
        let label = UILabel()
        label.text = "Manual State Management"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let stateToggle: UISwitch = {
        let toggle = UISwitch()
        toggle.translatesAutoresizingMaskIntoConstraints = false
        return toggle
    }()

    let tableView: UITableView = .init()

    private var widgetStateController = DefaultWidgetStateController(
        closeButtonAction: {},
        widgetFinishedCompletion: { _ in }
    )

    init(clientID: String, programID: String) {
        self.clientID = clientID
        self.programID = programID
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "Integrator Triggers Widget"
        super.viewDidLoad()

        // Connect data:
        widgetKindSelect.delegate = self
        widgetKindSelect.dataSource = self

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.isHidden = true // hide it for now until we implement deserialization/serialization for widgets

        contentView.addSubview(widgetIDTextField)
        contentView.addSubview(getWidgetButton)
        contentView.addSubview(widgetView)
        contentView.addSubview(currentStateLabel)
        contentView.addSubview(moveToNextStateBtn)
        contentView.addSubview(tableView)
        contentView.addSubview(widgetKindSelect)
        contentView.addSubview(widgetKindSelectLabel)
        contentView.addSubview(manualStateLabel)
        contentView.addSubview(stateToggle)

        NSLayoutConstraint.activate([
            getWidgetButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0),
            getWidgetButton.heightAnchor.constraint(equalToConstant: 50.0),
            getWidgetButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20.0),
            getWidgetButton.widthAnchor.constraint(equalToConstant: 150.0),

            widgetIDTextField.centerYAnchor.constraint(equalTo: getWidgetButton.centerYAnchor),
            widgetIDTextField.heightAnchor.constraint(equalToConstant: 50.0),
            widgetIDTextField.leadingAnchor.constraint(equalTo: getWidgetButton.trailingAnchor, constant: 10.0),
            widgetIDTextField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),

            widgetKindSelectLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            widgetKindSelectLabel.heightAnchor.constraint(equalToConstant: 20.0),
            widgetKindSelectLabel.topAnchor.constraint(equalTo: widgetIDTextField.bottomAnchor, constant: 10.0),

            widgetKindSelect.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            widgetKindSelect.heightAnchor.constraint(equalToConstant: 50.0),
            widgetKindSelect.topAnchor.constraint(equalTo: widgetKindSelectLabel.bottomAnchor, constant: 0.0),

            manualStateLabel.topAnchor.constraint(equalTo: widgetKindSelect.bottomAnchor, constant: 5.0),
            manualStateLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0),
            stateToggle.centerYAnchor.constraint(equalTo: manualStateLabel.centerYAnchor),
            stateToggle.leadingAnchor.constraint(equalTo: manualStateLabel.trailingAnchor, constant: 20.0),

            currentStateLabel.topAnchor.constraint(equalTo: manualStateLabel.bottomAnchor, constant: 20.0),
            currentStateLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0),

            moveToNextStateBtn.centerYAnchor.constraint(equalTo: currentStateLabel.centerYAnchor),
            moveToNextStateBtn.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),

            widgetView.topAnchor.constraint(equalTo: currentStateLabel.bottomAnchor, constant: 20),
            widgetView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            widgetView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),

            tableView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0),
            tableView.heightAnchor.constraint(equalToConstant: 100.0),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10)
        ])

        sdk = EngagementSDK(config: EngagementSDKConfig(clientID: clientID))
        session = sdk.contentSession(config: SessionConfiguration(programID: programID))
        session.delegate = self

        getWidgetButton.addTarget(self, action: #selector(getWidgetById), for: .touchUpInside)
        moveToNextStateBtn.addTarget(self, action: #selector(moveToNextState), for: .touchUpInside)
        stateToggle.addTarget(self, action: #selector(toggleManualState), for: UIControl.Event.valueChanged)

        widgetStateController = DefaultWidgetStateController(
            closeButtonAction: {},
            widgetFinishedCompletion: { [weak self] _ in
                self?.widgetView.isHidden = true
            }
        )
    }

    @objc func toggleManualState(mySwitch: UISwitch) {
        if stateToggle.isOn {
            currentStateLabel.isHidden = false
            moveToNextStateBtn.isHidden = false
        } else {
            currentStateLabel.isHidden = true
            moveToNextStateBtn.isHidden = true
        }
    }

    @objc func moveToNextState() {
        if let widget = currentWidget {
            widget.moveToNextState()
            currentStateLabel.text = "Current State: \(widget.currentState)"
        }
    }

    @objc func getWidgetById() {
        let selectedRow = widgetKindSelect.selectedRow(inComponent: 0)
        let pickedKind: WidgetKind = WidgetKind.allCases[selectedRow]

        sdk.getWidget(id: widgetIDTextField.text!, kind: pickedKind) { result in
            switch result {
            case let .success(widget):
                self.presentWidget(widget: widget)
            case let .failure(error):
                log.dev("\(error.localizedDescription)")
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    let alert = UIAlertController(
                        title: "Error",
                        message: "\(error.localizedDescription)",
                        preferredStyle: .alert
                    )

                    alert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }

    func saveShownWidgetToLocalStorage(id: String, kind: WidgetKind, data: Data) {
        var tempArray = [data]
        if let dbContents = UserDefaults.standard.array(forKey: localDBKey) as? [Data] {
            tempArray.insert(contentsOf: dbContents, at: 0)
        }

        UserDefaults.standard.set(tempArray, forKey: localDBKey)
        refreshTableView()
    }

    func refreshTableView() {
        /* Commenting this out until we create deserialization/serialization for widgets
         guard let widgetsFromDB = UserDefaults.standard.array(forKey: localDBKey) as? [Data] else { return }

         shownWidgets.removeAll()

           widgetsFromDB.forEach { widgetData in
              do {
                  let json = try JSONSerialization.jsonObject(with: widgetData, options: [])
                  sdk.createWidget(withJSONObject: json) { result in
                      switch result {
                      case let .success(widgets):
                          self.shownWidgets.append(widgets)
                          self.tableView.reloadData()
                      case .failure: break
                      }
                  }
              } catch {}
          } */
    }

    private func presentWidget(widget: Widget) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }

            self.currentWidget = widget
            self.widgetView.isHidden = false
            self.addChild(widget)

            widget.view.translatesAutoresizingMaskIntoConstraints = false
            self.widgetView.addSubview(widget.view)
            NSLayoutConstraint.activate([
                widget.view.topAnchor.constraint(equalTo: self.widgetView.topAnchor),
                widget.view.leadingAnchor.constraint(equalTo: self.widgetView.leadingAnchor),
                widget.view.trailingAnchor.constraint(equalTo: self.widgetView.trailingAnchor),
                widget.view.bottomAnchor.constraint(equalTo: self.widgetView.bottomAnchor, constant: 50)
            ])

            widget.didMove(toParent: self)

            if !self.stateToggle.isOn {
                widget.delegate = self.widgetStateController
                widget.moveToNextState()
            } else {
                widget.delegate = nil
            }
        }
    }
}

extension IntegratorTriggersWidgetViewController: ContentSessionDelegate {
    func widget(_ session: ContentSession, didBecomeReady jsonObject: Any) {}

    func widget(_ session: ContentSession, didBecomeReady widget: Widget) {
        presentWidget(widget: widget)
    }

    func playheadTimeSource(_ session: ContentSession) -> Date? {
        return nil
    }

    func session(_ session: ContentSession, didChangeStatus status: SessionStatus) {}

    func session(_ session: ContentSession, didReceiveError error: Error) {}

    func chat(session: ContentSession, roomID: String, newMessage message: ChatMessage) {}

    func contentSession(_ session: ContentSession, didReceiveWidget widget: WidgetModel) {}
}

extension IntegratorTriggersWidgetViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return WidgetKind.allCases.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return WidgetKind.allCases[row].displayName
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
}

extension IntegratorTriggersWidgetViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shownWidgets.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = shownWidgets[indexPath.row].widgetTitle
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {}
}
