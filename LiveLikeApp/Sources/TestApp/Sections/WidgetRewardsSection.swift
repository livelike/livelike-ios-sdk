//
//  WidgetRewardsSection.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 9/2/20.
//

// import EngagementSDK
// import UIKit
//
// class WidgetRewardsSection: TestCaseViewController {
//    let widgetViewController = WidgetPopupViewController()
//    let widgetView = UIView()
//    lazy var widgetCreateView = WidgetCreateView(
//        programID: self.programID,
//        apiToken: self.apiToken,
//        timeoutSeconds: self.timeoutSeconds,
//        apiOrigin: self.apiOrigin
//    )
//
//    var session: ContentSession?
//
//    let sdk: EngagementSDK
//    let apiToken: String
//    let programID: String
//    let theme: Theme
//    let apiOrigin: URL
//    private let timeoutSeconds: Int
//    private var timeoutSecondsISO8601: String {
//        return "P0DT00H00M\(timeoutSeconds)S"
//    }
//
//    init(
//        sdk: EngagementSDK,
//        apiToken: String,
//        programID: String,
//        theme: Theme,
//        timeoutSeconds: Int,
//        apiOrigin: URL
//    ) {
//        self.sdk = sdk
//        self.apiToken = apiToken
//        self.programID = programID
//        self.theme = theme
//        self.timeoutSeconds = timeoutSeconds
//        self.apiOrigin = apiOrigin
//        super.init()
//    }
//
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
//    override func viewDidLoad() {
//        vcTitle = "Widgets Rewards Test"
//        vcInfo = "Demonstrates custom UI look and feel for rewards methods and events."
//
//        super.viewDidLoad()
//
//        view.backgroundColor = .darkGray
//
//        widgetView.translatesAutoresizingMaskIntoConstraints = false
//        widgetCreateView.translatesAutoresizingMaskIntoConstraints = false
//        contentView.addSubview(widgetView)
//        contentView.addSubview(widgetCreateView)
//        addChild(viewController: widgetViewController, into: widgetView)
//        applyLayoutConstraints()
//        let config = SessionConfiguration(programID: programID)
//        session = sdk.contentSession(config: config)
//        widgetViewController.session = session
//        widgetViewController.setTheme(theme)
//
//        sdk.getCurrentUserProfile { result in
//            switch result {
//            case let .success(userProfile):
//                userProfile.delegate = self
//            case let .failure(error):
//                print(error)
//            }
//        }
//
//        session?.getRewardItems { result in
//            switch result {
//            case let .success(rewardItems):
//                let alert = UIAlertController(
//                    title: "Welcome!",
//                    message: "Interact with Widgets for a chance to win:\n\(rewardItems.map { $0.name }.joined(separator: "\n"))",
//                    preferredStyle: .alert
//                )
//                alert.addAction(.init(title: "OK!", style: .cancel, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//            case let .failure(error):
//                print(error)
//            }
//        }
//    }
//
//    func applyLayoutConstraints() {
//        NSLayoutConstraint.activate([
//            widgetView.topAnchor.constraint(equalTo: contentView.topAnchor),
//            widgetView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.4),
//            widgetView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
//            widgetView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
//
//            widgetCreateView.topAnchor.constraint(equalTo: widgetView.bottomAnchor),
//            widgetCreateView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
//            widgetCreateView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
//            widgetCreateView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
//        ])
//    }
// }
//
// extension WidgetRewardsSection: UserProfileDelegate {
//    func userProfile(_ userProfile: UserProfile, didEarnRewards rewards: [Reward], from rewardSource: RewardSource) {
//        var rewardsText = ""
//
//        switch rewardSource {
//        case let .widgetInteraction(info):
//            rewardsText += "You've earned rewards for interacting with the \(info.kind.displayName)"
//        }
//
//        rewards.forEach { reward in
//            rewardsText += "\nYou've earned \(reward.amount) \(reward.item.name)"
//        }
//
//        let alert = UIAlertController(
//            title: "Great Job!",
//            message: rewardsText,
//            preferredStyle: .alert
//        )
//        alert.addAction(.init(title: "Cool!", style: .cancel, handler: nil))
//        present(alert, animated: true, completion: nil)
//    }
// }
