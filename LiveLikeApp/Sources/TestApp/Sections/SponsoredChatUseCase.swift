//
//  SponsoredChatUseCase.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 6/9/21.
//

import EngagementSDK
import UIKit

class SponsoredChatUseCase: ChatTestCaseViewController {
    
    let sponsorsStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .gray
        return view
    }()
    
    override func viewDidLoad() {
        vcTitle = "Sponsored Chat"
        super.viewDidLoad()
        
        contentView.addSubview(sponsorsStackView)
        NSLayoutConstraint.activate([
            sponsorsStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10.0),
            sponsorsStackView.widthAnchor.constraint(equalToConstant: 250),
            sponsorsStackView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)
        ])
        
        sdk.sponsorship.getBy(programID: programID) { result in
            switch result {
            case .success(let sponsors):
                sponsors.forEach { sponsor in
                    let row = UIStackView()
                    row.axis = .horizontal
                    
                    if let mediaData = try? Data(contentsOf: sponsor.logoURL) {
                        let logo = UIImageView(image: UIImage(data: mediaData))
                        
                        logo.translatesAutoresizingMaskIntoConstraints = false
                        NSLayoutConstraint.activate([
                            logo.widthAnchor.constraint(equalToConstant: 50.0),
                            logo.heightAnchor.constraint(equalToConstant: 50.0)
                        ])
                        
                        row.addArrangedSubview(logo)
                    }
                    
                    let label = UILabel()
                    label.text = "Program: \(sponsor.name)"
                    row.addArrangedSubview(label)
                    row.backgroundColor = sponsor.brandColor
                    self.sponsorsStackView.addArrangedSubview(row)
                }
            case .failure(let error):
                log.dev(error.localizedDescription)
            }
        }
        
        sdk.sponsorship.getByApplication(page: .first) { result in
            switch result {
            case .success(let sponsors):
                sponsors.items.forEach { sponsor in
                    let row = UIStackView()
                    row.axis = .horizontal
                    
                    if let mediaData = try? Data(contentsOf: sponsor.logoURL) {
                        let logo = UIImageView(image: UIImage(data: mediaData))
                        
                        logo.translatesAutoresizingMaskIntoConstraints = false
                        NSLayoutConstraint.activate([
                            logo.widthAnchor.constraint(equalToConstant: 50.0),
                            logo.heightAnchor.constraint(equalToConstant: 50.0)
                        ])
                        
                        row.addArrangedSubview(logo)
                    }
                    
                    let label = UILabel()
                    label.text = "Application: \(sponsor.name)"
                    row.addArrangedSubview(label)
                    row.backgroundColor = sponsor.brandColor
                    self.sponsorsStackView.addArrangedSubview(row)
                }
            case .failure(let error):
                log.dev(error.localizedDescription)
            }
        }
        
        session?.getChatSession(completion: { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success(chatroom):
                self.sdk.sponsorship.getBy(chatRoomID: chatroom.roomID, page: .first, completion: { result in
                    switch result {
                    case .success(let sponsors):
                        sponsors.items.forEach { sponsor in
                            let row = UIStackView()
                            row.axis = .horizontal
                            
                            if let mediaData = try? Data(contentsOf: sponsor.logoURL) {
                                let logo = UIImageView(image: UIImage(data: mediaData))
                                
                                logo.translatesAutoresizingMaskIntoConstraints = false
                                NSLayoutConstraint.activate([
                                    logo.widthAnchor.constraint(equalToConstant: 50.0),
                                    logo.heightAnchor.constraint(equalToConstant: 50.0)
                                ])
                                
                                row.addArrangedSubview(logo)
                            }
                            
                            let label = UILabel()
                            label.text = "ChatRoom: \(sponsor.name)"
                            row.addArrangedSubview(label)
                            row.backgroundColor = sponsor.brandColor
                            self.sponsorsStackView.addArrangedSubview(row)
                        }
                    case .failure(let error):
                        log.dev(error.localizedDescription)
                    }
                })
            case .failure(let error):
                log.dev(error.localizedDescription)
            }
        })
    }
}
