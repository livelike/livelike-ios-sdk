//
//  CommentBoardUseCase.swift
//  LiveLikeTestApp
//
//  Created by Keval Shah on 14/12/22.
//

import Foundation
import EngagementSDK
import UIKit

class CommentBoardUseCase: TestCaseViewController {
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 30
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let buttonStack1: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let buttonStack2: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let allowCommentStack: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textColor = .black
        label.text = "Allow Comments: "
        return label
    }()
    
    private let commentBoardTitleTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Comment Board Title"
        return textField
    }()
    
    private let customIDTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Custom Board ID"
        return textField
    }()
    
    private let nestingDepthTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Nesting Depth Value"
        return textField
    }()
    
    private let allowCommentsSwitch: UISwitch = {
        let boolSwitch = UISwitch()
        boolSwitch.translatesAutoresizingMaskIntoConstraints = false
        return boolSwitch
    }()
    
    private let commentBoardIDTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Comment Board ID"
        return textField
    }()
    
    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()
    
    private let createCommentBoardButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Create Board", for: .normal)
        return button
    }()
    
    private let updateCommentBoardButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Update Board", for: .normal)
        return button
    }()
    
    private let deleteCommentBoardButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Delete Board", for: .normal)
        return button
    }()
    
    private let getListOfCommentBoardButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get List of Boards", for: .normal)
        return button
    }()
    
    private let getCommentBoardDetailButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get Details of Board", for: .normal)
        return button
    }()
    
    private let showMoreButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Show More Boards", for: .normal)
        return button
    }()

    private let accessToken: String
    private let sdk: EngagementSDK
    
    private var commentBoards = [CommentBoard]()

    init(sdk: EngagementSDK, accessToken: String) {
        self.sdk = sdk
        self.accessToken = accessToken
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        
        self.createCommentBoardButton.addTarget(self, action: #selector(self.createCommentBoard), for: .touchUpInside)
        self.deleteCommentBoardButton.addTarget(self, action: #selector(self.deleteCommentBoard), for: .touchUpInside)
        self.updateCommentBoardButton.addTarget(self, action: #selector(self.updateCommentBoard), for: .touchUpInside)
        self.getCommentBoardDetailButton.addTarget(self, action: #selector(self.getCommentBoardDetail), for: .touchUpInside)
        self.getListOfCommentBoardButton.addTarget(self, action: #selector(self.getCommentBoardList), for: .touchUpInside)
        self.showMoreButton.addTarget(self, action: #selector(self.showMoreSpaces), for: .touchUpInside)
        
        customIDTextField.delegate = self
        nestingDepthTextField.delegate = self
        commentBoardTitleTextField.delegate = self
        commentBoardIDTextField.delegate = self
        
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .dark:
                label.textColor = .white
                commentBoardTitleTextField.textColor = .white
                customIDTextField.textColor = .white
                nestingDepthTextField.textColor = .white
                commentBoardIDTextField.textColor = .white
            case .light:
                label.textColor = .black
                commentBoardTitleTextField.textColor = .black
                customIDTextField.textColor = .black
                nestingDepthTextField.textColor = .black
                commentBoardIDTextField.textColor = .black
            case .unspecified:
                label.textColor = .black
                commentBoardTitleTextField.textColor = .black
                customIDTextField.textColor = .black
                nestingDepthTextField.textColor = .black
                commentBoardIDTextField.textColor = .black
            @unknown default:
                label.textColor = .black
                commentBoardTitleTextField.textColor = .black
                customIDTextField.textColor = .black
                nestingDepthTextField.textColor = .black
                commentBoardIDTextField.textColor = .black
            }
        }
        
        contentView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 20),
        ])
        
        buttonStack1.addArrangedSubview(createCommentBoardButton)
        buttonStack1.addArrangedSubview(updateCommentBoardButton)
        buttonStack1.addArrangedSubview(deleteCommentBoardButton)
        buttonStack2.addArrangedSubview(getCommentBoardDetailButton)
        buttonStack2.addArrangedSubview(getListOfCommentBoardButton)
        allowCommentStack.addArrangedSubview(label)
        allowCommentStack.addArrangedSubview(allowCommentsSwitch)
        
        stackView.addArrangedSubview(commentBoardTitleTextField)
        stackView.addArrangedSubview(customIDTextField)
        stackView.addArrangedSubview(nestingDepthTextField)
        stackView.addArrangedSubview(allowCommentStack)
        stackView.addArrangedSubview(commentBoardIDTextField)
        stackView.addArrangedSubview(buttonStack1)
        stackView.addArrangedSubview(buttonStack2)
        stackView.addArrangedSubview(tableView)
        stackView.addArrangedSubview(showMoreButton)
    }
    
    @objc private func createCommentBoard() {
        guard let customID = customIDTextField.text else {
            self.showAlert(title: "Custom ID Missing", message: "Please Enter ID")
            return
        }
        
        guard let nestingDepthInt = Int(nestingDepthTextField.text ?? "1") else {
            self.showAlert(title: "Nesting Depth Missing or can't be converted to number", message: "Please Enter Depth")
            return
        }
        
        let allowComments = allowCommentsSwitch.isOn
        let title = commentBoardTitleTextField.text
        let createBoardOptions = CreateCommentBoardRequestOptions(
            allowComments: allowComments,
            repliesDepth: nestingDepthInt,
            title: title,
            customIdentifier: customID,
            customData: nil
        )
        
        sdk.commentBoards.createCommentBoard(options: createBoardOptions) { result in
            switch result {
            case .success(let commentBoard):
                self.showAlert(title: "Comment Board Created", message: "Comment Board Id: \(commentBoard.id)")
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func deleteCommentBoard() {
        guard let boardID = commentBoardIDTextField.text else {
            self.showAlert(title: "Comment Board ID Missing", message: "Please Enter ID")
            return
        }
        
        sdk.commentBoards.deleteCommentBoard(commentBoardID: boardID) { result in
            switch result {
            case .success:
                self.showAlert(title: "Completed", message: "Deleted Comment board with ID: \(boardID)")
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func updateCommentBoard() {
        guard let boardID = commentBoardIDTextField.text else {
            self.showAlert(title: "Comment Board ID Missing", message: "Please Enter ID")
            return
        }
        
        let customID = customIDTextField.text
        let depthEmpty: Bool = nestingDepthTextField.text?.isEmpty ?? true
        let nestingDepthInt: Int? = depthEmpty ? nil : Int(nestingDepthTextField.text!)
        let allowComments = allowCommentsSwitch.isOn
        let title = commentBoardTitleTextField.text
        
        let updateBoardOptions = UpdateCommentBoardRequestOptions(
            title: title,
            customID: customID,
            allowComments: allowComments,
            repliesDepth: nestingDepthInt,
            customData: nil
        )
        
        sdk.commentBoards.updateCommentBoard(commentBoardID: boardID, options: updateBoardOptions) { result in
            switch result {
            case .success(let commentBoard):
                self.showAlert(title: "Comment Board Updated", message: "Comment Board updated: \(commentBoard.id)")
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func getCommentBoardDetail() {
        guard let boardID = commentBoardIDTextField.text else {
            self.showAlert(title: "Comment Board ID Missing", message: "Please Enter ID")
            return
        }
        
        sdk.commentBoards.getCommentBoardDetails(commentBoardID: boardID) { result in
            switch result {
            case .success(let commentBoard):
                self.commentBoards = [commentBoard]
                self.tableView.reloadData()
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func getCommentBoardList() {
        sdk.commentBoards.getCommentBoards(page: .first) { result in
            switch result {
            case .success(let commentBoards):
                if commentBoards.count > 0 {
                    self.showAlert(
                        title: "No. of Boards:",
                        message: "\(commentBoards.count)"
                    )
                }
                self.commentBoards = commentBoards
                self.tableView.reloadData()
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func showMoreSpaces() {
        sdk.commentBoards.getCommentBoards(page: .next) { result in
            switch result {
            case .success(let commentBoards):
                if commentBoards.count > 0 {
                    self.showAlert(
                        title: "No. of Boards:",
                        message: "\(commentBoards.count)"
                    )
                }
                self.commentBoards.append(contentsOf: commentBoards)
                self.tableView.reloadData()
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    private func showAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: title,
                message: message,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension CommentBoardUseCase: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentBoards.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        var cellText: String = ""
        let rowData = commentBoards[indexPath.row]
        cellText = "Comment Board:\(String(describing: rowData.title))\nCustom Id: \(rowData.customID), \n Nesting Depth Count = \(rowData.repliesDepth), \n Allow Comments = \(rowData.allowComments)"

        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = cellText
        return cell
    }
}

extension CommentBoardUseCase: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowData = commentBoards[indexPath.row]
        UIPasteboard.general.string = rowData.id
        self.showAlert(title: "Comment Board Id Copied", message: rowData.id)
    }
}

extension CommentBoardUseCase: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
