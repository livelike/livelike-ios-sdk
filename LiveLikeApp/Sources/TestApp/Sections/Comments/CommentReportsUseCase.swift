//
//  CommentsUseCase.swift
//  LiveLikeTestApp
//
//  Created by Keval Shah on 14/12/22.
//

import Foundation
import EngagementSDK
import UIKit

class CommentReportsUseCase: TestCaseViewController {
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 30
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let buttonStack1: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let buttonStack2: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let buttonStack3: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let commentBoardIDTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Comment Board ID"
        return textField
    }()
    
    private let descriptionField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Description"
        return textField
    }()
    
    private let commentIDTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Comment ID"
        return textField
    }()
    
    private let commentReportIDTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Comment Report ID"
        return textField
    }()
    
    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()
    
    private let createCommentClientButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Create Comment Client", for: .normal)
        return button
    }()
    
    private let createCommentReportButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Create Report", for: .normal)
        return button
    }()
    
    private let dismissCommentReportButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Dismiss Report", for: .normal)
        return button
    }()
    
    private let deleteCommentReportButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Delete Report", for: .normal)
        return button
    }()
    
    private let dismissAllReportsButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Dismiss All", for: .normal)
        return button
    }()
    
    private let getListOfCommentReportsButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get List of Reports", for: .normal)
        return button
    }()
    
    private let getCommentReportDetailButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get Report", for: .normal)
        return button
    }()
    
    private let showMoreButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Show More Reports", for: .normal)
        return button
    }()

    private let accessToken: String
    private let sdk: EngagementSDK
    private var commentClient: CommentClient?
    
    private var commentReports = [CommentReport]()

    init(sdk: EngagementSDK, accessToken: String) {
        self.sdk = sdk
        self.accessToken = accessToken
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vcTitle = "Comments Use Case"
        
        tableView.delegate = self
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        
        self.createCommentClientButton.addTarget(self, action: #selector(self.createCommentClient), for: .touchUpInside)
        self.createCommentReportButton.addTarget(self, action: #selector(self.createCommentReport), for: .touchUpInside)
        self.deleteCommentReportButton.addTarget(self, action: #selector(self.deleteCommentReport), for: .touchUpInside)
        self.dismissCommentReportButton.addTarget(self, action: #selector(self.dismissReport), for: .touchUpInside)
        self.dismissAllReportsButton.addTarget(self, action: #selector(self.dismissAllReports), for: .touchUpInside)
        self.getCommentReportDetailButton.addTarget(self, action: #selector(self.getReport), for: .touchUpInside)
        self.getListOfCommentReportsButton.addTarget(self, action: #selector(self.getReportsList), for: .touchUpInside)
        self.showMoreButton.addTarget(self, action: #selector(self.showMoreReports), for: .touchUpInside)
        
        commentBoardIDTextField.delegate = self
        descriptionField.delegate = self
        commentIDTextField.delegate = self
        commentReportIDTextField.delegate = self
        
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .dark:
                commentBoardIDTextField.textColor = .white
                descriptionField.textColor = .white
                commentIDTextField.textColor = .white
                commentReportIDTextField.textColor = .white
            case .light:
                commentBoardIDTextField.textColor = .black
                descriptionField.textColor = .black
                commentIDTextField.textColor = .black
                commentReportIDTextField.textColor = .black
            case .unspecified:
                commentBoardIDTextField.textColor = .black
                descriptionField.textColor = .black
                commentIDTextField.textColor = .black
                commentReportIDTextField.textColor = .black
            @unknown default:
                commentBoardIDTextField.textColor = .black
                descriptionField.textColor = .black
                commentIDTextField.textColor = .black
                commentReportIDTextField.textColor = .black
            }
        }
        
        contentView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            stackView.bottomAnchor.constraint(equalTo: contentView.safeBottomAnchor, constant: -20),
        ])
        
        buttonStack1.addArrangedSubview(createCommentReportButton)
        buttonStack1.addArrangedSubview(deleteCommentReportButton)
        buttonStack2.addArrangedSubview(dismissCommentReportButton)
        buttonStack2.addArrangedSubview(dismissAllReportsButton)
        buttonStack3.addArrangedSubview(getCommentReportDetailButton)
        buttonStack3.addArrangedSubview(getListOfCommentReportsButton)
        
        stackView.addArrangedSubview(commentBoardIDTextField)
        stackView.addArrangedSubview(createCommentClientButton)
        stackView.addArrangedSubview(descriptionField)
        stackView.addArrangedSubview(commentIDTextField)
        stackView.addArrangedSubview(commentReportIDTextField)
        stackView.addArrangedSubview(buttonStack1)
        stackView.addArrangedSubview(buttonStack2)
        stackView.addArrangedSubview(buttonStack3)
        stackView.addArrangedSubview(tableView)
        stackView.addArrangedSubview(showMoreButton)
    }
    
    @objc private func createCommentClient() {
        guard let commentBoardID = commentBoardIDTextField.text else {
            self.showAlert(title: "Comment Board ID Missing", message: "Please Enter ID")
            return
        }
        
        sdk.createCommentClient(for: commentBoardID) { result in
            switch result {
            case .success(let commentClient):
                self.commentClient = commentClient
                self.showAlert(title: "Comment Session Created", message: "Comment Session for Board with id \(commentBoardID)")
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func createCommentReport() {
        guard let client = self.commentClient else {
            self.showAlert(title: "Comment Session Missing", message: "Please create Comment Session using Comment Board ID")
            return
        }
        
        guard let commentID = commentIDTextField.text else {
            self.showAlert(title: "Comment ID Missing", message: "Please Enter Comment ID")
            return
        }
        
        client.createCommentReport(
            commentID: commentID,
            options: CreateCommentReportRequestOptions(
                description: descriptionField.text
            )
        ) { result in
            switch result {
            case .success(let report):
                self.showAlert(title: "Report Created", message: "Report Id: \(report.id)")
                self.commentReports = [report]
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func deleteCommentReport() {
        guard let commentReportID = commentReportIDTextField.text else {
            self.showAlert(title: "Comment Report ID Missing", message: "Please Enter Comment Report ID")
            return
        }
        
        guard let client = self.commentClient else {
            self.showAlert(title: "Comment Session Missing", message: "Please create Comment Session using Comment Board ID")
            return
        }
        
        client.deleteCommentReport(commentReportID: commentReportID) { result in
            switch result {
            case .success:
                self.showAlert(title: "Report Deleted", message: "Report Id: \(commentReportID)")
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func dismissReport() {
        guard let client = self.commentClient else {
            self.showAlert(title: "Comment Session Missing", message: "Please create Comment Session using Comment Board ID")
            return
        }
        
        guard let commentReportID = commentReportIDTextField.text else {
            self.showAlert(title: "Comment Report ID Missing", message: "Please Enter Comment Report ID")
            return
        }
        
        client.dismissCommentReport(commentReportID: commentReportID) { result in
            switch result {
            case .success(let reportDetail):
                self.showAlert(title: "Report Dismissed", message: "Report Status: \(String(describing: reportDetail.reportStatus?.rawValue)) for ID: \(commentReportID)")
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func dismissAllReports() {
        guard let client = self.commentClient else {
            self.showAlert(title: "Comment Session Missing", message: "Please create Comment Session using Comment Board ID")
            return
        }
        
        guard let commentID = commentIDTextField.text else {
            self.showAlert(title: "Comment ID Missing", message: "Please Enter Comment ID")
            return
        }
        
        client.dismissAllCommentReports(commentID: commentID) { result in
            switch result {
            case .success(let reportDetail):
                self.showAlert(title: "Report Dismissed", message: "Report Status: \(String(describing: reportDetail.detail)) for ID: \(commentID)")
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func getReport() {
        guard let client = self.commentClient else {
            self.showAlert(title: "Comment Session Missing", message: "Please create Comment Session using Comment Board ID")
            return
        }
        
        guard let commentReportID = commentReportIDTextField.text else {
            self.showAlert(title: "Comment Report ID Missing", message: "Please Enter ID")
            return
        }
        
        client.getCommentReportDetails(commentReportID: commentReportID) { result in
            switch result {
            case .success(let report):
                self.commentReports = [report]
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func getReportsList() {
        getReports()
    }
    
    @objc private func getCommentsListByPending() {
        guard let commentBoardID = commentBoardIDTextField.text else {
            self.showAlert(title: "Comment Board ID Missing", message: "Please Enter ID")
            return
        }
        getReports(options: .init(commentBoardID: commentBoardID, commentID: nil, reportStatus: .pending))
    }
    
    @objc private func getCommentsListByDismissed() {
        guard let commentBoardID = commentBoardIDTextField.text else {
            self.showAlert(title: "Comment Board ID Missing", message: "Please Enter ID")
            return
        }
        getReports(options: .init(commentBoardID: commentBoardID, commentID: nil, reportStatus: .dismissed))
    }
    
    private func getReports(options: GetCommentReportsListRequestOptions? = nil) {
        guard let client = self.commentClient else {
            self.showAlert(title: "Comment Session Missing", message: "Please create Comment Session using Comment Board ID")
            return
        }
        
        client.getCommentReports(page: .first, options: options) { result in
            switch result {
            case .success(let reports):
                self.showAlert(title: "Reports Recd.", message: "Count: \(reports.count)")
                self.commentReports = reports
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func showMoreReports() {
        guard let client = self.commentClient else {
            self.showAlert(title: "Comment Session Missing", message: "Please create Comment Session using Comment Board ID")
            return
        }
        
        client.getCommentReports(page: .next, options: nil) { result in
            switch result {
            case .success(let reports):
                self.showAlert(title: "Reports Recd.", message: "Count: \(reports.count)")
                self.commentReports = reports
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    private func showAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: title,
                message: message,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension CommentReportsUseCase: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentReports.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        var cellText: String = ""
        let rowData = commentReports[indexPath.row]
        cellText = "Report ID:\(String(describing: rowData.id))\nComment Id: \(rowData.commentID), \n Comment Board ID = \(String(describing: rowData.commentBoardID)), \n Report Status = \(rowData.reportStatus.rawValue) \n Description: \(String(describing: rowData.description))"

        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = cellText
        return cell
    }
}

extension CommentReportsUseCase: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowData = commentReports[indexPath.row]
        UIPasteboard.general.string = rowData.id
        self.showAlert(title: "Comment Report Id Copied", message: rowData.id)
    }
}

extension CommentReportsUseCase: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
