//
//  CommentsUseCase.swift
//  LiveLikeTestApp
//
//  Created by Keval Shah on 14/12/22.
//

import Foundation
import EngagementSDK
import UIKit

class CommentsUseCase: TestCaseViewController {
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 30
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let buttonStack1: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let buttonStack2: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let buttonStack3: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let commentBoardIDTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Comment Board ID"
        return textField
    }()
    
    private let commentTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Comment Text"
        return textField
    }()
    
    private let parentCommentIDTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Parent Comment ID"
        return textField
    }()
    
    private let commentIDTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Comment ID"
        return textField
    }()
    
    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()
    
    private let createCommentClientButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Create Comment Client", for: .normal)
        return button
    }()
    
    private let createCommentButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Create Comment", for: .normal)
        return button
    }()
    
    private let editCommentButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Edit Comment", for: .normal)
        return button
    }()
    
    private let deleteCommentButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Delete Comment", for: .normal)
        return button
    }()
    
    private let createCommentReplyButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Create Reply", for: .normal)
        return button
    }()
    
    private let getListOfCommentsButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get List of Comments", for: .normal)
        return button
    }()
    
    private let getCommentDetailButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get Replies", for: .normal)
        return button
    }()
    
    private let getCommentsByNewestButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Sort By Newest", for: .normal)
        return button
    }()
    
    private let getCommentsByOldestButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Sort By Oldest", for: .normal)
        return button
    }()
    
    private let getCommentsByNewestRepliesButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Order By Newest Replies", for: .normal)
        return button
    }()
    
    private let getCommentsByOldestRepliesButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Order By Oldest Replies", for: .normal)
        return button
    }()
    
    private let showMoreButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Show More Comments", for: .normal)
        return button
    }()

    private let accessToken: String
    private let sdk: EngagementSDK
    private var commentClient: CommentClient?
    
    private var comments = [Comment]()

    init(sdk: EngagementSDK, accessToken: String) {
        self.sdk = sdk
        self.accessToken = accessToken
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vcTitle = "Comments Use Case"
        
        tableView.delegate = self
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        
        self.createCommentClientButton.addTarget(self, action: #selector(self.createCommentClient), for: .touchUpInside)
        self.createCommentButton.addTarget(self, action: #selector(self.createComment), for: .touchUpInside)
        self.createCommentReplyButton.addTarget(self, action: #selector(self.createCommentReply), for: .touchUpInside)
        self.deleteCommentButton.addTarget(self, action: #selector(self.deleteComment), for: .touchUpInside)
        self.editCommentButton.addTarget(self, action: #selector(self.editComment), for: .touchUpInside)
        self.getCommentDetailButton.addTarget(self, action: #selector(self.getReplies), for: .touchUpInside)
        self.getListOfCommentsButton.addTarget(self, action: #selector(self.getCommentsList), for: .touchUpInside)
        self.getCommentsByNewestButton.addTarget(self, action: #selector(self.getCommentsListByNewest), for: .touchUpInside)
        self.getCommentsByOldestButton.addTarget(self, action: #selector(self.getCommentsListByOldest), for: .touchUpInside)
        self.getCommentsByNewestRepliesButton.addTarget(self, action: #selector(self.getCommentsListByNewestReplies), for: .touchUpInside)
        self.getCommentsByOldestRepliesButton.addTarget(self, action: #selector(self.getCommentsListByOldestReplies), for: .touchUpInside)
        self.showMoreButton.addTarget(self, action: #selector(self.showMoreSpaces), for: .touchUpInside)
        
        commentBoardIDTextField.delegate = self
        commentTextField.delegate = self
        commentIDTextField.delegate = self
        parentCommentIDTextField.delegate = self
        
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .dark:
                commentBoardIDTextField.textColor = .white
                commentTextField.textColor = .white
                commentIDTextField.textColor = .white
                parentCommentIDTextField.textColor = .white
            case .light:
                commentBoardIDTextField.textColor = .black
                commentTextField.textColor = .black
                commentIDTextField.textColor = .black
                parentCommentIDTextField.textColor = .black
            case .unspecified:
                commentBoardIDTextField.textColor = .black
                commentTextField.textColor = .black
                commentIDTextField.textColor = .black
                parentCommentIDTextField.textColor = .black
            @unknown default:
                commentBoardIDTextField.textColor = .black
                commentTextField.textColor = .black
                commentIDTextField.textColor = .black
                parentCommentIDTextField.textColor = .black
            }
        }
        
        contentView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            stackView.bottomAnchor.constraint(equalTo: contentView.safeBottomAnchor, constant: -20),
        ])
        
        buttonStack1.addArrangedSubview(createCommentButton)
        buttonStack1.addArrangedSubview(editCommentButton)
        buttonStack1.addArrangedSubview(deleteCommentButton)
        buttonStack2.addArrangedSubview(createCommentReplyButton)
        buttonStack2.addArrangedSubview(getCommentDetailButton)
        buttonStack2.addArrangedSubview(getListOfCommentsButton)
        buttonStack3.addArrangedSubview(getCommentsByNewestButton)
        buttonStack3.addArrangedSubview(getCommentsByOldestButton)
        buttonStack3.addArrangedSubview(getCommentsByNewestRepliesButton)
        buttonStack3.addArrangedSubview(getCommentsByOldestRepliesButton)
        
        stackView.addArrangedSubview(commentBoardIDTextField)
        stackView.addArrangedSubview(createCommentClientButton)
        stackView.addArrangedSubview(commentTextField)
        stackView.addArrangedSubview(commentIDTextField)
        stackView.addArrangedSubview(buttonStack1)
        stackView.addArrangedSubview(buttonStack2)
        stackView.addArrangedSubview(buttonStack3)
        stackView.addArrangedSubview(tableView)
        stackView.addArrangedSubview(showMoreButton)
    }
    
    @objc private func createCommentClient() {
        guard let commentBoardID = commentBoardIDTextField.text else {
            self.showAlert(title: "Comment Board ID Missing", message: "Please Enter ID")
            return
        }
        
        sdk.createCommentClient(for: commentBoardID) { result in
            switch result {
            case .success(let commentClient):
                self.commentClient = commentClient
                self.showAlert(title: "Comment Session Created", message: "Comment Session for Board with id \(commentBoardID)")
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func createComment() {
        guard let commentText = commentTextField.text else {
            self.showAlert(title: "Comment Text Missing", message: "Please Enter Comment Text")
            return
        }
        
        guard let client = self.commentClient else {
            self.showAlert(title: "Comment Session Missing", message: "Please create Comment Session using Comment Board ID")
            return
        }
        
        client.addComment(text: commentText) { result in
            switch result {
            case .success(let comment):
                self.showAlert(title: "Comment Created", message: "Comment Id: \(comment.id)")
                self.comments = [comment]
                self.tableView.reloadData()
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func deleteComment() {
        guard let commentID = commentIDTextField.text else {
            self.showAlert(title: "Comment ID Missing", message: "Please Enter Comment ID")
            return
        }
        
        guard let client = self.commentClient else {
            self.showAlert(title: "Comment Session Missing", message: "Please create Comment Session using Comment Board ID")
            return
        }
        
        client.deleteComment(commentID: commentID) { result in
            switch result {
            case .success:
                self.showAlert(title: "Comment Deleted", message: "Comment Id: \(commentID)")
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func editComment() {
        guard let commentText = commentTextField.text else {
            self.showAlert(title: "Comment Text Missing", message: "Please Enter Comment Text")
            return
        }
        
        guard let client = self.commentClient else {
            self.showAlert(title: "Comment Session Missing", message: "Please create Comment Session using Comment Board ID")
            return
        }
        
        guard let commentID = commentIDTextField.text else {
            self.showAlert(title: "Comment ID Missing", message: "Please Enter Comment ID")
            return
        }
        
        client.editComment(commentID: commentID, text: commentText) { result in
            switch result {
            case .success(let comment):
                self.showAlert(title: "Comment Updated", message: "Comment Id: \(comment.id)")
                self.comments = [comment]
                self.tableView.reloadData()
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func createCommentReply() {
        guard let commentText = commentTextField.text else {
            self.showAlert(title: "Comment Text Missing", message: "Please Enter Comment Text")
            return
        }
        
        guard let client = self.commentClient else {
            self.showAlert(title: "Comment Session Missing", message: "Please create Comment Session using Comment Board ID")
            return
        }
        
        guard let commentID = commentIDTextField.text else {
            self.showAlert(title: "Parent Comment ID Missing", message: "Please Enter Comment ID")
            return
        }
        
        client.addCommentReply(parentCommentID: commentID, text: commentText) { result in
            switch result {
            case .success(let comment):
                self.showAlert(title: "Reply Created", message: "Reply Id: \(comment.id)")
                self.comments = [comment]
                self.tableView.reloadData()
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func getReplies() {
        guard let client = self.commentClient else {
            self.showAlert(title: "Comment Session Missing", message: "Please create Comment Session using Comment Board ID")
            return
        }
        
        guard let commentID = commentIDTextField.text else {
            self.showAlert(title: "Parent Comment ID Missing", message: "Please Enter Comment ID")
            return
        }
        
        guard let commentBoardID = commentBoardIDTextField.text else {
            self.showAlert(title: "Comment Board ID Missing", message: "Please Enter ID")
            return
        }
        
        let options = GetCommentRepliesRequestOptions(commentBoardID: commentBoardID, parentCommentID: commentID)
        
        client.getCommentReplies(page: .first, options: options) { result in
            switch result {
            case .success(let comments):
                self.showAlert(title: "Replies Recd.", message: "Count: \(comments.count)")
                self.comments = comments
                self.tableView.reloadData()
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func getCommentsList() {
        getComments()
    }
    
    @objc private func getCommentsListByNewest() {
        getComments(options: .init(orderBy: .newest, topLevel: true))
    }
    
    @objc private func getCommentsListByOldest() {
        getComments(options: .init(orderBy: .oldest, topLevel: true))
    }
    
    @objc private func getCommentsListByNewestReplies() {
        getComments(options: .init(orderBy: .newestReplies, topLevel: false))
    }
    
    @objc private func getCommentsListByOldestReplies() {
        getComments(options: .init(orderBy: .oldestReplies, topLevel: false))
    }
    
    private func getComments(options: GetCommentsRequestOptions? = nil) {
        guard let client = self.commentClient else {
            self.showAlert(title: "Comment Session Missing", message: "Please create Comment Session using Comment Board ID")
            return
        }
        
        client.getComments(page: .first, options: options) { result in
            switch result {
            case .success(let comments):
                self.showAlert(title: "Comments Recd.", message: "Count: \(comments.count)")
                self.comments = comments
                self.tableView.reloadData()
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func showMoreSpaces() {
        guard let client = self.commentClient else {
            self.showAlert(title: "Comment Session Missing", message: "Please create Comment Session using Comment Board ID")
            return
        }
        
        client.getComments(page: .next, options: nil) { result in
            switch result {
            case .success(let comments):
                self.showAlert(title: "Comments Recd.", message: "Count: \(comments.count)")
                self.comments = comments
                self.tableView.reloadData()
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    private func showAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: title,
                message: message,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension CommentsUseCase: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        var cellText: String = ""
        let rowData = comments[indexPath.row]
        cellText = "Comment:\(String(describing: rowData.text))\nComment Id: \(rowData.id), \n Parent Comment ID = \(String(describing: rowData.parentCommentID)), \n Thread Comment ID = \(rowData.threadCommentID) \nComment Depth: \(rowData.commentDepth), \nAuthor: \(rowData.authorID), \n isDeleted: \(rowData.isDeleted)"

        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = cellText
        return cell
    }
}

extension CommentsUseCase: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowData = comments[indexPath.row]
        UIPasteboard.general.string = rowData.id
        self.showAlert(title: "Comment Id Copied", message: rowData.id)
    }
}

extension CommentsUseCase: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
