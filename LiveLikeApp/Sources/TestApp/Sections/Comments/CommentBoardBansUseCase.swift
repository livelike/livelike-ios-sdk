//
//  CommentBoardBansUseCase.swift
//  LiveLikeTestApp
//
//  Created by Keval Shah on 09/03/23.
//

import Foundation
import EngagementSDK
import UIKit

class CommentBoardBansUseCase: TestCaseViewController {
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 30
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let buttonStack1: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let buttonStack2: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let profileIDTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Profile ID"
        return textField
    }()
    
    private let commentBoardIDTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Comment Board ID"
        return textField
    }()
    
    private let commentBoardBanIDTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Comment Board Ban ID"
        return textField
    }()
    
    private let descriptionTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Description"
        return textField
    }()
    
    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()
    
    private let createCommentBoardBanButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Create Ban", for: .normal)
        return button
    }()
    
    private let deleteCommentBoardBanButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Delete Ban", for: .normal)
        return button
    }()
    
    private let getListOfCommentBoardBansButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get List of Bans", for: .normal)
        return button
    }()
    
    private let getCommentBoardBanDetailButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get Details of Ban", for: .normal)
        return button
    }()
    
    private let showMoreButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Show More Bans", for: .normal)
        return button
    }()

    private let accessToken: String
    private let sdk: EngagementSDK
    
    private var commentBoardBans = [CommentBoardBanDetails]()

    init(sdk: EngagementSDK, accessToken: String) {
        self.sdk = sdk
        self.accessToken = accessToken
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        
        self.createCommentBoardBanButton.addTarget(self, action: #selector(self.createCommentBoardBan), for: .touchUpInside)
        self.deleteCommentBoardBanButton.addTarget(self, action: #selector(self.deleteCommentBoardBan), for: .touchUpInside)
        self.getCommentBoardBanDetailButton.addTarget(self, action: #selector(self.getCommentBoardBanDetail), for: .touchUpInside)
        self.getListOfCommentBoardBansButton.addTarget(self, action: #selector(self.getCommentBoardBanList), for: .touchUpInside)
        self.showMoreButton.addTarget(self, action: #selector(self.showMoreBans), for: .touchUpInside)
        
        profileIDTextField.delegate = self
        descriptionTextField.delegate = self
        commentBoardIDTextField.delegate = self
        
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .dark:
                profileIDTextField.textColor = .white
                descriptionTextField.textColor = .white
                commentBoardIDTextField.textColor = .white
                commentBoardBanIDTextField.textColor = .white
            case .light:
                profileIDTextField.textColor = .black
                descriptionTextField.textColor = .black
                commentBoardIDTextField.textColor = .black
                commentBoardBanIDTextField.textColor = .black
            case .unspecified:
                profileIDTextField.textColor = .black
                descriptionTextField.textColor = .black
                commentBoardIDTextField.textColor = .black
                commentBoardBanIDTextField.textColor = .black
            @unknown default:
                profileIDTextField.textColor = .black
                descriptionTextField.textColor = .black
                commentBoardIDTextField.textColor = .black
                commentBoardBanIDTextField.textColor = .black
            }
        }
        
        contentView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 20),
        ])
        
        buttonStack1.addArrangedSubview(createCommentBoardBanButton)
        buttonStack1.addArrangedSubview(deleteCommentBoardBanButton)
        buttonStack2.addArrangedSubview(getCommentBoardBanDetailButton)
        buttonStack2.addArrangedSubview(getListOfCommentBoardBansButton)
        
        stackView.addArrangedSubview(profileIDTextField)
        stackView.addArrangedSubview(commentBoardIDTextField)
        stackView.addArrangedSubview(descriptionTextField)
        stackView.addArrangedSubview(commentBoardBanIDTextField)
        stackView.addArrangedSubview(buttonStack1)
        stackView.addArrangedSubview(buttonStack2)
        stackView.addArrangedSubview(tableView)
        stackView.addArrangedSubview(showMoreButton)
    }
    
    @objc private func createCommentBoardBan() {
        guard let profileID = profileIDTextField.text else {
            self.showAlert(title: "Profile ID Missing", message: "Please Enter ID")
            return
        }
        
        let description = descriptionTextField.text
        let commentBoardID = commentBoardIDTextField.text
        let createBoardBanOptions = CommentBoardBanRequestOptions(
            commentBoardID: commentBoardID,
            description: description
        )
        
        sdk.commentBoards.createCommentBoardBan(profileID: profileID, options: createBoardBanOptions) { result in
            switch result {
            case .success(let commentBoardBan):
                self.showAlert(title: "Comment Board Ban Created", message: "Comment Board Ban Id: \(commentBoardBan.id)")
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func deleteCommentBoardBan() {
        guard let banID = commentBoardBanIDTextField.text else {
            self.showAlert(title: "Comment Board Ban ID Missing", message: "Please Enter ID")
            return
        }
        
        sdk.commentBoards.deleteCommentBoardBan(commentBoardBanID: banID) { result in
            switch result {
            case .success:
                self.showAlert(title: "Completed", message: "Deleted Comment board ban with ID: \(banID)")
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func getCommentBoardBanDetail() {
        guard let banID = commentBoardBanIDTextField.text else {
            self.showAlert(title: "Comment Board Ban ID Missing", message: "Please Enter ID")
            return
        }
        
        sdk.commentBoards.getCommentBoardBanDetails(commentBoardBanID: banID) { result in
            switch result {
            case .success(let commentBoardBan):
                self.commentBoardBans = [commentBoardBan]
                self.tableView.reloadData()
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func getCommentBoardBanList() {
        
        let profileID = profileIDTextField.text
        let commentBoardID = commentBoardIDTextField.text
        
        let options = GetCommentBoardBansListRequestOptions(
            profileID: profileID,
            commentBoardID: commentBoardID
        )
        
        sdk.commentBoards.getCommentBoardBans(
            page: .first,
            options: options
        ) { result in
            switch result {
            case .success(let commentBoardBans):
                if commentBoardBans.count > 0 {
                    self.showAlert(
                        title: "No. of Boards:",
                        message: "\(commentBoardBans.count)"
                    )
                }
                self.commentBoardBans = commentBoardBans
                self.tableView.reloadData()
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func showMoreBans() {
        
        let profileID = profileIDTextField.text
        let commentBoardID = commentBoardIDTextField.text
        
        let options = GetCommentBoardBansListRequestOptions(
            profileID: profileID,
            commentBoardID: commentBoardID
        )
        
        sdk.commentBoards.getCommentBoardBans(page: .next, options: options) { result in
            switch result {
            case .success(let commentBoardBans):
                if commentBoardBans.count > 0 {
                    self.showAlert(
                        title: "No. of Bans:",
                        message: "\(commentBoardBans.count)"
                    )
                }
                self.commentBoardBans.append(contentsOf: commentBoardBans)
                self.tableView.reloadData()
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    private func showAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: title,
                message: message,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension CommentBoardBansUseCase: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentBoardBans.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        var cellText: String = ""
        let rowData = commentBoardBans[indexPath.row]
        cellText = "Ban ID: \(rowData.id) \nComment Board:\(String(describing: rowData.commentBoardID))\nProfile Id: \(rowData.profileID)"

        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = cellText
        return cell
    }
}

extension CommentBoardBansUseCase: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowData = commentBoardBans[indexPath.row]
        UIPasteboard.general.string = rowData.id
        self.showAlert(title: "Comment Board Ban Id Copied", message: rowData.id)
    }
}

extension CommentBoardBansUseCase: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
