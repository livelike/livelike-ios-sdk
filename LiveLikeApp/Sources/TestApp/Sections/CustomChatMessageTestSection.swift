//
//  CustomChatMessageTestSection.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 4/28/21.
//

import EngagementSDK
import UIKit

class CustomChatMessageTestSection: TestCaseViewController {
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textColor = .black
        return label
    }()
    
    private let textField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Custom Text Message"
        return textField
    }()
    
    private let sendCustomMessageButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Send Custom Message", for: .normal)
        return button
    }()
    
    private let pinMessageButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Pin Message", for: .normal)
        return button
    }()

    private var chatSession: ChatSession!
    private let contentSession: ContentSession
    private let baseURL: URL
    private let accessToken: String

    init(contentSession: ContentSession, baseURL: URL, accessToken: String) {
        self.contentSession = contentSession
        self.baseURL = baseURL
        self.accessToken = accessToken
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vcTitle = "Verify Custom Chat Message Listener"
        vcInfo = "Send a custom message through API and this section will display a message when it is recieved."
        
        textField.delegate = self
        
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .dark:
                label.textColor = .white
                textField.textColor = .white
            case .light:
                label.textColor = .black
                textField.textColor = .black
            case .unspecified:
                label.textColor = .black
                textField.textColor = .black
            @unknown default:
                label.textColor = .black
                textField.textColor = .black
            }
        }
        
        contentSession.getChatSession { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                log.dev(error.localizedDescription)
            case .success(let chatSession):
                self.chatSession = chatSession
                chatSession.addDelegate(self)
                self.sendCustomMessageButton.addTarget(self, action: #selector(self.sendCustomMessagePressed), for: .touchUpInside)
                self.pinMessageButton.addTarget(self, action: #selector(self.pinMessageButtonPressed), for: .touchUpInside)
            }
        }
        
        contentView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
        ])
        
        stackView.addArrangedSubview(label)
        stackView.addArrangedSubview(textField)
        stackView.addArrangedSubview(sendCustomMessageButton)
        stackView.addArrangedSubview(pinMessageButton)
    }
    
    @objc private func sendCustomMessagePressed() {
        
        let customString: String = self.textField.text!.isEmpty ? "iOS Test Custom Chat Message" : textField.text!
        
        chatSession.sendCustomMessage(customString) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                self.showErrorAlert(message: "Failed to send custom message with error: \(error.localizedDescription)")
            case .success:
                DispatchQueue.main.async {
                    let alert = UIAlertController(
                        title: "Success!",
                        message: "Custom message sent successfully",
                        preferredStyle: .alert
                    )
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc private func pinMessageButtonPressed() {
        let messageString: String = self.textField.text!.isEmpty ? "iOS Test Pin Chat Message" : textField.text!
        
        chatSession.sendCustomMessage(messageString) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                self.showErrorAlert(message: "Failed to send custom message with error: \(error.localizedDescription)")
            case .success(let message):
                self.chatSession.pinMessage(message) { [weak self] result in
                    guard let self = self else { return }
                    switch result {
                    case .failure(let error):
                        self.showErrorAlert(message: "Failed to send custom message with error: \(error.localizedDescription)")
                    case .success:
                        DispatchQueue.main.async {
                            let alert = UIAlertController(
                                title: "Success!",
                                message: "Pinned message successfully",
                                preferredStyle: .alert
                            )
                            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    private func showErrorAlert(message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: "Error",
                message: message,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension CustomChatMessageTestSection: ChatSessionDelegate {
    func chatSession(_ chatSession: ChatSession, didDeleteMessage messageID: ChatMessageID) {
        print("Chat Message Deleted: \(messageID.asString)")
    }
    
    func chatSession(_ chatSession: ChatSession, didRecieveRoomUpdate chatRoom: ChatRoomInfo) {
        print("Received Chat Room Update: \nFilters: \(String(describing: chatRoom.contentFilter)) \nVisibility: \(chatRoom.visibility)")
    }
    
    func chatSession(_ chatSession: ChatSession, didRecieveNewMessage message: ChatMessage) {
        guard let customData = message.customData else { return }
        DispatchQueue.main.async {
            self.textField.text = nil
            self.label.text = "Recieved custom chat message:\nid: \(message.id.asString)\ndata: \(customData)"
        }
    }
    
    func chatSession(_ chatSession: ChatSession, didPinMessage message: PinMessageInfo) { }
    
    func chatSession(_ chatSession: ChatSession, didUnpinMessage pinMessageInfoID: String) { }
}

extension CustomChatMessageTestSection: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
