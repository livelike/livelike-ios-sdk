//
//  QuestsUseCase.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 6/29/22.
//

import UIKit
import EngagementSDK

class QuestsUseCase: TestCaseViewController {

    private var contentSession: ContentSession!
    private var sdk: EngagementSDK
    private var items: [Quest] = []
    private var questRequest = GetQuestsRequestOptions()

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.allowsSelection = true
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "myCell")
        return tableView
    }()

    private let loadMoreButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Load More", for: .normal)
        return button
    }()

    private let buttonStack: UIStackView = {
        let buttonStack = UIStackView()
        buttonStack.translatesAutoresizingMaskIntoConstraints = false
        buttonStack.axis = .vertical
        buttonStack.distribution = .fillEqually
        buttonStack.spacing = 10.0
        return buttonStack
    }()

    private let questIDInput: UITextField = {
        let profileInput: UITextField = UITextField()
        profileInput.placeholder = "Filter Quest IDs separated by comma"
        profileInput.translatesAutoresizingMaskIntoConstraints = false
        profileInput.borderStyle = .line
        profileInput.font = UIFont.systemFont(ofSize: 10.0)
        return profileInput
    }()

    private let sendButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Submit Filter", for: .normal)
        button.backgroundColor = .green
        return button
    }()

    init(sdk: EngagementSDK, programID: String) {
        self.sdk = sdk
        self.contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "Quests"

        super.viewDidLoad()

        buttonStack.addArrangedSubview(questIDInput)
        buttonStack.addArrangedSubview(sendButton)

        view.addSubview(tableView)
        view.addSubview(loadMoreButton)
        view.addSubview(buttonStack)

        NSLayoutConstraint.activate([
            buttonStack.topAnchor.constraint(equalTo: contentView.topAnchor),
            buttonStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            buttonStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            buttonStack.heightAnchor.constraint(equalToConstant: 50),

            tableView.topAnchor.constraint(equalTo: buttonStack.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: loadMoreButton.topAnchor),

            loadMoreButton.topAnchor.constraint(equalTo: tableView.bottomAnchor),
            loadMoreButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            loadMoreButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            loadMoreButton.heightAnchor.constraint(equalToConstant: 60.0),
            loadMoreButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])

        loadMoreButton.addTarget(self, action: #selector(loadMoreButtonSelected), for: .touchUpInside)
        sendButton.addTarget(self, action: #selector(sentButtonSelected), for: .touchUpInside)

        tableView.delegate = self
        tableView.dataSource = self

        sdk.quests.getQuests(
            page: .first,
            options: questRequest
        ) { [weak self] result in

            guard let self = self else { return }

            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(let quests):
                self.items.append(contentsOf: quests)

                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }

    private func resetAllButtons() {
    }

    @objc private func loadMoreButtonSelected() {
        sdk.quests.getQuests(
            page: .next,
            options: questRequest
        ) { [weak self] result in

            guard let self = self else { return }

            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(let quests):
                self.items.append(contentsOf: quests)

                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }

    @objc private func sentButtonSelected() {
        resetAllButtons()

        var stringQuestIDs = [String]()

        if let text = questIDInput.text {
            let attrInput = text.replacingOccurrences(of: " ", with: "").lowercased()
            stringQuestIDs = attrInput.components(separatedBy: ",")

            questRequest = GetQuestsRequestOptions(questIDs: stringQuestIDs)
        }

        sdk.quests.getQuests(
            page: .first,
            options: questRequest
        ) { [weak self] result in

            guard let self = self else { return }

            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(let quests):
                self.items.removeAll()
                self.items.append(contentsOf: quests)

                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
}

extension QuestsUseCase: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "myCell")
        else {
            return UITableViewCell()
        }

        let quest = self.items[indexPath.row]
        cell.textLabel?.text = "\(quest.name)"

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let quest = items[indexPath.row]
        let questDetailVC = QuestDetailUseCase(sdk: sdk, quest: quest)
        self.navigationController?.pushViewController(questDetailVC, animated: true)
    }
}

class QuestDetailUseCase: TestCaseViewController {

    private var sdk: EngagementSDK
    private var items: [QuestTask] = []
    private let quest: Quest

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.allowsSelection = true
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "myCell")
        return tableView
    }()

    private let startQuestButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Start Quest", for: .normal)
        button.backgroundColor = .green
        return button
    }()

    init(sdk: EngagementSDK, quest: Quest) {
        self.sdk = sdk
        self.quest = quest
        self.items = quest.questTasks
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "\(quest.name) Quest Tasks"

        super.viewDidLoad()

        view.addSubview(tableView)
        view.addSubview(startQuestButton)

        NSLayoutConstraint.activate([

            startQuestButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10.0),
            startQuestButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            startQuestButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            startQuestButton.heightAnchor.constraint(equalToConstant: 50.0),

            tableView.topAnchor.constraint(equalTo: startQuestButton.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
        ])

        startQuestButton.addTarget(self, action: #selector(startQuestAction), for: .touchUpInside)

        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }

    @objc private func startQuestAction() {

        let alert = UIAlertController(
            title: "Start a Quest?",
            message: "Would you like to start a Quest: \(quest.name)",
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "No", style: .destructive))
        alert.addAction(UIAlertAction(title: "Yes", style: .default) { [weak self] _ in

            guard let self = self else { return }
            self.sdk.quests.startUserQuest(
                questID: self.quest.id
            ) { result in

                switch result {
                case .success(_):

                    let alert = UIAlertController(
                        title: "Success!",
                        message: "Quest [\(self.quest.name)] Successfuly Started",
                        preferredStyle: .alert
                    )
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                    self.present(alert, animated: true, completion: nil)

                case .failure(let error):

                    let alert = UIAlertController(
                        title: "Error!",
                        message: "\(error.localizedDescription)",
                        preferredStyle: .alert
                    )
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                    self.present(alert, animated: true, completion: nil)
                }

            }
        })

        self.present(alert, animated: true, completion: nil)

    }
}

extension QuestDetailUseCase: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "myCell")
        else {
            return UITableViewCell()
        }

        let quest = self.items[indexPath.row]
        cell.textLabel?.text = "\(quest.name)"

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
