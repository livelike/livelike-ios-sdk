//
//  CustomWidgetsUITestCase.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 10/20/20.
//

import EngagementSDK
import UIKit

class CustomWidgetsUITestCase: TestCaseViewController {
    let widgetViewController = WidgetPopupViewController()
    let widgetView = UIView()
    lazy var widgetCreateView = WidgetCreateView(
        programID: self.programID,
        apiToken: self.apiToken,
        timeoutSeconds: self.timeoutSeconds,
        apiOrigin: self.apiOrigin
    )
    var session: ContentSession?

    let sdk: EngagementSDK
    let apiToken: String
    let programID: String
    let theme: Theme
    let apiOrigin: URL
    private let timeoutSeconds: Int
    private var timeoutSecondsISO8601: String {
        return "P0DT00H00M\(timeoutSeconds)S"
    }

    // MARK: Custom Widget Toggles

    private var customAlert: Bool = true
    private var customCheerMeter: Bool = true
    private var customQuizWidget: Bool = true
    private var customPollWidget: Bool = true
    private var customPredictionWidget: Bool = true
    private var customPredictionFollowupWidget: Bool = true
    private var customImageSliderWidget: Bool = true

    init(
        sdk: EngagementSDK,
        apiToken: String,
        programID: String,
        theme: Theme,
        timeoutSeconds: Int,
        apiOrigin: URL
    ) {
        self.sdk = sdk
        self.apiToken = apiToken
        self.programID = programID
        self.theme = theme
        self.timeoutSeconds = timeoutSeconds
        self.apiOrigin = apiOrigin
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "Widgets Test"
        vcInfo = "Use this view to test Widgets without having to use the produce site. This view replicates API calls that the produce site would be doing"

        super.viewDidLoad()

        view.backgroundColor = .darkGray

        widgetView.translatesAutoresizingMaskIntoConstraints = false
        widgetView.backgroundColor = .gray
        widgetCreateView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(widgetView)
        contentView.addSubview(widgetCreateView)
        addChild(viewController: widgetViewController, into: widgetView)
        applyLayoutConstraints()
        let config = SessionConfiguration(programID: programID)
        session = sdk.contentSession(config: config)
        widgetViewController.delegate = self
        widgetViewController.session = session
        widgetViewController.setTheme(theme)
    }

    func applyLayoutConstraints() {
        NSLayoutConstraint.activate([
            widgetView.topAnchor.constraint(equalTo: contentView.topAnchor),
            widgetView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.5),
            widgetView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            widgetCreateView.topAnchor.constraint(equalTo: widgetView.bottomAnchor),
            widgetCreateView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetCreateView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            widgetCreateView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }

    override func showViewInfo() {
        let alert = UIAlertController(
            title: "Info",
            message: "Toggle Custom Widgets to turn ON/OFF",
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "Custom Alert turned [\(customAlert == true ? "ON" : "OFF")]", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            if self.customAlert {
                self.customAlert = false
            } else {
                self.customAlert = true
            }
        }))

        alert.addAction(UIAlertAction(title: "Custom Cheer Meter turned [\(customCheerMeter == true ? "ON" : "OFF")]", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            if self.customCheerMeter {
                self.customCheerMeter = false
            } else {
                self.customCheerMeter = true
            }
        }))

        alert.addAction(UIAlertAction(title: "Custom Quiz Widget turned [\(customQuizWidget == true ? "ON" : "OFF")]", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            if self.customQuizWidget {
                self.customQuizWidget = false
            } else {
                self.customQuizWidget = true
            }
        }))

        alert.addAction(UIAlertAction(title: "Custom Poll Widget turned [\(customPollWidget == true ? "ON" : "OFF")]", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            if self.customPollWidget {
                self.customPollWidget = false
            } else {
                self.customPollWidget = true
            }
        }))

        alert.addAction(UIAlertAction(title: "Custom Prediction Widget turned [\(customPredictionWidget == true ? "ON" : "OFF")]", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            if self.customPredictionWidget {
                self.customPredictionWidget = false
            } else {
                self.customPredictionWidget = true
            }
        }))

        alert.addAction(UIAlertAction(title: "Custom Prediction Followup turned [\(customPredictionFollowupWidget == true ? "ON" : "OFF")]", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            if self.customPredictionFollowupWidget {
                self.customPredictionFollowupWidget = false
            } else {
                self.customPredictionFollowupWidget = true
            }
        }))

        alert.addAction(UIAlertAction(title: "Custom Image Slider turned [\(customImageSliderWidget == true ? "ON" : "OFF")]", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            if self.customImageSliderWidget {
                self.customImageSliderWidget = false
            } else {
                self.customImageSliderWidget = true
            }
        }))

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        present(alert, animated: true, completion: nil)
    }

    deinit {
        log.dev("deinit")
    }
}

extension CustomWidgetsUITestCase: WidgetPopupViewControllerDelegate {
    /// Called when a Widget is about to animate into the view
    func widgetViewController(_ widgetViewController: WidgetPopupViewController, willDisplay widget: Widget) {}
    /// Called immediately after a Widget has finished animating into the view
    func widgetViewController(_ widgetViewController: WidgetPopupViewController, didDisplay widget: Widget) {}
    /// Called when a Widget is about to animate out of the view
    func widgetViewController(_ widgetViewController: WidgetPopupViewController, willDismiss widget: Widget) {}
    /// Called immediately after a Widget has finished animating out of the view
    func widgetViewController(_ widgetViewController: WidgetPopupViewController, didDismiss widget: Widget) {}
    /// Called when the WidgetPopupViewController receives a widget and will enqueue it to be displayed
    /// Return nil to not display the Widget
    func widgetViewController(_ widgetViewController: WidgetPopupViewController, willEnqueueWidget widgetModel: WidgetModel) -> Widget? {
        switch widgetModel {
        case let .alert(model):
            if customAlert {
                return SponsoredAlertWidgetViewController(model: model)
            } else {
                return DefaultWidgetFactory.makeWidget(from: widgetModel)
            }
        case let .cheerMeter(model):
            if customCheerMeter {
                return CustomCheerMeterWidgetViewController(model: model)
            } else {
                return DefaultWidgetFactory.makeWidget(from: widgetModel)
            }
        case let .quiz(model):
            if customQuizWidget == true {
                if model.containsImages {
                    return CustomGSWImageQuiz(model: model) // CustomQuizWidget(model: model)
                } else {
                    return CustomTextQuizWidgetViewController(model: model)
                }
            } else {
                return DefaultWidgetFactory.makeWidget(from: widgetModel)
            }
        case let .poll(model):
            if customPollWidget {
                if model.containsImages {
                    return CustomGSWImagePoll(model: model)
                } else {
                    return CustomTextPollWidgetViewController(model: model)
                }
            } else {
                return DefaultWidgetFactory.makeWidget(from: widgetModel)
            }
        case let .prediction(model):
            if customPredictionWidget == true {
                if model.containsImages {
                    return CustomGSWImagePredictionWidget(model: model)
                }
                return CustomTextPredictionWidgetViewController(model: model)
            } else {
                return DefaultWidgetFactory.makeWidget(from: widgetModel)
            }
        case let .predictionFollowUp(model):
            if customPredictionFollowupWidget == true {
                if model.containsImages {
                    return CustomGSWImgPredictFollowUp(model: model)
                } else {
                    return CustomTextPredictionFollowUpWidgetViewController(model: model)
                }
            } else {
                return DefaultWidgetFactory.makeWidget(from: widgetModel)
            }
        case let .imageSlider(model):
            if customImageSliderWidget == true {
                return CustomImageSliderViewController(model: model)
            } else {
                return DefaultWidgetFactory.makeWidget(from: widgetModel)
            }
        case .socialEmbed:
            return DefaultWidgetFactory.makeWidget(from: widgetModel)
        case .videoAlert:
            return DefaultWidgetFactory.makeWidget(from: widgetModel)
        case .textAsk:
            return DefaultWidgetFactory.makeWidget(from: widgetModel)
        default:
            return DefaultWidgetFactory.makeWidget(from: widgetModel)
        }
    }
}
