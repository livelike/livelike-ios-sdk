//
//  WidgetLeaderboardDemo.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 9/21/20.
//

import EngagementSDK
import UIKit

class WidgetLeaderboardDemo: TestCaseViewController {
    let widgetViewController = WidgetPopupViewController()
    let widgetView = UIView()

    private let infoLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Loading..."
        return label
    }()

    private let nicknameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Nickname:"
        return label
    }()

    private let rankLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Rank:"
        return label
    }()

    private let scoreLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Score:"
        return label
    }()

    private let placementStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 0
        return stackView
    }()

    var session: ContentSession?

    private var leaderboard: LeaderboardClient?

    let sdk: EngagementSDK
    let programID: String

    init(
        sdk: EngagementSDK,
        programID: String
    ) {
        self.sdk = sdk
        self.programID = programID
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .darkGray

        widgetView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(widgetView)
        addChild(viewController: widgetViewController, into: widgetView)
        view.addSubview(placementStackView)

        placementStackView.addArrangedSubview(infoLabel)
        placementStackView.addArrangedSubview(nicknameLabel)
        placementStackView.addArrangedSubview(rankLabel)
        placementStackView.addArrangedSubview(scoreLabel)

        applyLayoutConstraints()
        let config = SessionConfiguration(programID: programID)
        session = sdk.contentSession(config: config)
        widgetViewController.session = session

        sdk.getUserDisplayName { result in
            switch result {
            case let .success(displayName):
                self.nicknameLabel.text = "Nickname: \(displayName)"
            case let .failure(error):
                print(error)
            }
        }

        session?.getLeaderboardClients { result in
            switch result {
            case let .success(leaderboardClients):
                guard let leaderboard = leaderboardClients.first else { return }
                self.leaderboard = leaderboard
                self.infoLabel.text = "Your standing in the \(leaderboard.name) Leaderboard"
                self.rankLabel.text = "Rank: #\(leaderboard.currentPosition?.rank.description ?? "-")"
                self.scoreLabel.text = "Score: \(leaderboard.currentPosition?.score.description ?? "-")"
                leaderboard.delegate = self
            case let .failure(error):
                print(error)
            }
        }
    }

    func applyLayoutConstraints() {
        NSLayoutConstraint.activate([
            widgetView.topAnchor.constraint(equalTo: contentView.topAnchor),
            widgetView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.8),
            widgetView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            placementStackView.topAnchor.constraint(equalTo: widgetView.bottomAnchor),
            placementStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            placementStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            placementStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
}

extension WidgetLeaderboardDemo: LeaderboardDelegate {
    func leaderboard(_ leaderboardClient: LeaderboardClient, currentPositionDidChange position: LeaderboardPosition) {
        guard leaderboardClient.id == leaderboard?.id else { return }
        rankLabel.text = "Rank: #\(position.rank)"
        scoreLabel.text = "Score: \(position.score)"
    }
}
