//
//  NilPlayheadTimeSourceViewController.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 1/22/20.
//

import EngagementSDK
import UIKit

/// Tests that sending nil as playheadTimeSource makes widgets and chat behave as unsynced
final class NilPlayheadTimeSourceViewController: TestCaseViewController {
    let widgetController = WidgetPopupViewController()
    let chatController = ChatViewController()

    let widgetContainer = UIView()
    let chatContainer = UIView()

    var session: ContentSession?

    private let sdk: EngagementSDK
    private let programID: String

    init(sdk: EngagementSDK, programID: String) {
        self.sdk = sdk
        self.programID = programID
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        widgetContainer.translatesAutoresizingMaskIntoConstraints = false
        chatContainer.translatesAutoresizingMaskIntoConstraints = false

        contentView.addSubview(widgetContainer)
        contentView.addSubview(chatContainer)

        NSLayoutConstraint.activate([
            widgetContainer.topAnchor.constraint(equalTo: contentView.topAnchor),
            widgetContainer.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetContainer.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            widgetContainer.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.3),

            chatContainer.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            chatContainer.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            chatContainer.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            chatContainer.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.7)
        ])

        navigationController?.setNavigationBarHidden(false, animated: true)
        addChild(viewController: chatController, into: chatContainer)
        addChild(viewController: widgetController, into: widgetContainer)

        view.backgroundColor = .white

        let config = SessionConfiguration(programID: programID)
        session = sdk.contentSession(config: config, delegate: self)
        chatController.session = session
        widgetController.session = session
    }
}

extension NilPlayheadTimeSourceViewController: ContentSessionDelegate {
    func session(_ session: ContentSession, didChangeStatus status: SessionStatus) {}

    func session(_ session: ContentSession, didReceiveError error: Error) {}

    func chat(session: ContentSession, roomID: String, newMessage message: ChatMessage) {}

    func widget(_ session: ContentSession, didBecomeReady jsonObject: Any) {}

    func widget(_ session: ContentSession, didBecomeReady widget: Widget) {}

    func playheadTimeSource(_ session: ContentSession) -> Date? {
        return nil
    }

    func contentSession(_ session: ContentSession, didReceiveWidget widget: WidgetModel) {}
}
