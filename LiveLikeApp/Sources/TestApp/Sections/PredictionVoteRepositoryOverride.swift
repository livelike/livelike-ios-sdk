//
//  PredictionVoteRepositoryOverride.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 9/11/20.
//

import EngagementSDK
import UIKit

class PredictionVoteRepositoryOverride: TestCaseViewController {
    private let clientID: String
    private let programID: String

    private var sdk: EngagementSDK!
    private var contentSession: ContentSession!

    private let widgetViewController: WidgetPopupViewController = WidgetPopupViewController()
    private let widgetCreateView: WidgetCreateView

    init(
        clientID: String,
        programID: String,
        apiToken: String,
        apiOrigin: URL
    ) {
        self.clientID = clientID
        self.programID = programID
        widgetCreateView = WidgetCreateView(
            programID: programID,
            apiToken: apiToken,
            timeoutSeconds: 10,
            apiOrigin: apiOrigin
        )
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        var sdkConfig = EngagementSDKConfig(clientID: clientID)
        sdkConfig.widget.predictionVoteRepository = self
        sdk = EngagementSDK(config: sdkConfig)
        contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))

        widgetViewController.session = contentSession

        vcTitle = "Prediction Vote Repository Override"
        vcInfo = "When you interact with a prediction, an alert will be raised when the prediction vote repository methods are called."

        widgetViewController.view.translatesAutoresizingMaskIntoConstraints = false
        addChild(widgetViewController)
        view.addSubview(widgetViewController.view)

        widgetCreateView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(widgetCreateView)

        NSLayoutConstraint.activate([
            widgetViewController.view.topAnchor.constraint(equalTo: contentView.topAnchor),
            widgetViewController.view.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.4),
            widgetViewController.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetViewController.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            widgetCreateView.topAnchor.constraint(equalTo: widgetViewController.view.bottomAnchor),
            widgetCreateView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetCreateView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            widgetCreateView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
}

extension PredictionVoteRepositoryOverride: PredictionVoteRepository {
    func get(by widgetID: String, completion: @escaping (PredictionVote?) -> Void) {
        let alert = UIAlertController(
            title: "PredictionVoteRepository.get called",
            message: "SDK is looking for prediction vote for widget with id \(widgetID)",
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: {
            completion(nil)
        })
    }

    func add(vote: PredictionVote, completion: @escaping (Bool) -> Void) {
        let alert = UIAlertController(
            title: "PredictionVoteRepository.add called",
            message: "SDK wants to store prediction vote: \(vote)",
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: {
            completion(true)
        })
    }
}
