//
//  DefaultChatOnlySection.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 1/30/20.
//

import EngagementSDK
import UIKit

/// Tests the default ChatViewController
final class DefaultChatOnlySection: ChatTestCaseViewController {
    override func viewDidLoad() {
        vcTitle = "Default Chat View"
        super.viewDidLoad()
    }
}
