//
//  ChatHiddenInputViewSection.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 7/17/20.
//

import EngagementSDK
import UIKit

final class ChatHiddenInputViewSection: TestCaseViewController {
    private let contentSession: ContentSession

    let chatViewController: ChatViewController = ChatViewController()

    lazy var inputViewSwitch: UISwitch = {
        let toggle = UISwitch()
        toggle.translatesAutoresizingMaskIntoConstraints = false
        toggle.addTarget(self, action: #selector(handleSwitch), for: .valueChanged)
        return toggle
    }()

    init(contentSession: ContentSession) {
        self.contentSession = contentSession
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let theme = Theme()
        theme.chatImageHeight = 20
        theme.chatImageWidth = 20

        chatViewController.setTheme(theme)

        // Turn off by default
        chatViewController.isChatInputVisible = false
        inputViewSwitch.isOn = false

        chatViewController.session = contentSession
        chatViewController.view.translatesAutoresizingMaskIntoConstraints = false
        addChild(chatViewController)
        view.addSubview(chatViewController.view)
        view.addSubview(inputViewSwitch)
        NSLayoutConstraint.activate([
            chatViewController.view.topAnchor.constraint(equalTo: contentView.topAnchor),
            chatViewController.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            chatViewController.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            chatViewController.view.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.9),

            inputViewSwitch.topAnchor.constraint(equalTo: chatViewController.view.bottomAnchor),
            inputViewSwitch.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            inputViewSwitch.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            inputViewSwitch.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.1)
        ])

        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.contentSession.getChatSession(completion: { result in
                switch result {
                case let .success(chatSession):
                    chatSession.avatarURL = URL(string: "https://livelike.com/wp-content/uploads/2018/11/Jelzon.jpg")!
                case let .failure(error):
                    print(error)
                }
            })
        }
    }

    @objc private func handleSwitch() {
        chatViewController.isChatInputVisible = inputViewSwitch.isOn
    }
}
