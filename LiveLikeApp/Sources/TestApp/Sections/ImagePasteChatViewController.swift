//
//  ImagePasteChatViewController.swift
//  LiveLikeDemoApp
//
//  Created by Mike Moloksher on 2/25/20.
//

import EngagementSDK
import UIKit

final class ImagePasteChatViewController: ChatTestCaseViewController {
    override func viewDidLoad() {
        vcTitle = "Paste Image In Chat without Keyboard"
        vcInfo = "No need to have an external keyboard with GIFs. Click on the input field and PASTE. An image should be posted for you."

        let image = UIImage(named: "logo_small")
        UIPasteboard.general.image = image

        super.viewDidLoad()
    }
}
