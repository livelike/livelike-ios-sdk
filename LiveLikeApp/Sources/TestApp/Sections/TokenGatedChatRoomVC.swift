//
//  TokenGatedChatRoomVC.swift
//  LiveLikeTestApp
//
//  Created by apple on 19/04/23.
//

import EngagementSDK
import UIKit

class TokenGatedChatRoomVC: TestCaseViewController {
    private let sdk: EngagementSDK
    private let envBaseURL: URL

    private let chatViewController = ChatViewController()

    private let checkTokenGatedAccessButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Check Chat Room Access", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private let chatIdField: UITextField = {
        let roomTitle: UITextField = UITextField()
        roomTitle.placeholder = "Enter Chat Room ID"
        roomTitle.translatesAutoresizingMaskIntoConstraints = false
        roomTitle.borderStyle = .line
        roomTitle.font = UIFont.systemFont(ofSize: 12.0)
        return roomTitle
    }()

    private let walletAddressField: UITextField = {
        let roomTitle: UITextField = UITextField()
        roomTitle.placeholder = "Enter Wallet Address"
        roomTitle.translatesAutoresizingMaskIntoConstraints = false
        roomTitle.borderStyle = .line
        roomTitle.font = UIFont.systemFont(ofSize: 12.0)
        return roomTitle
    }()

    private let chatroomInfoStack: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 0
        return stackView
    }()

    init(envBaseURL: URL, sdk: EngagementSDK) {
        self.envBaseURL = envBaseURL
        self.sdk = sdk
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        chatViewController.view.translatesAutoresizingMaskIntoConstraints = false

        addChild(chatViewController)

        contentView.addSubview(chatViewController.view)
        contentView.addSubview(checkTokenGatedAccessButton)
        contentView.addSubview(chatIdField)
        contentView.addSubview(walletAddressField)
        contentView.addSubview(chatroomInfoStack)

        NSLayoutConstraint.activate([
            chatViewController.view.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 150.0),
            chatViewController.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            chatViewController.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            chatViewController.view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

            chatIdField.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20.0),
            chatIdField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20.0),
            chatIdField.heightAnchor.constraint(equalToConstant: 30.0),
            chatIdField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),

            walletAddressField.topAnchor.constraint(equalTo: chatIdField.bottomAnchor, constant: 10.0),
            walletAddressField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20.0),
            walletAddressField.heightAnchor.constraint(equalToConstant: 30.0),
            walletAddressField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),

            checkTokenGatedAccessButton.topAnchor.constraint(equalTo: walletAddressField.bottomAnchor, constant: 10.0),
            checkTokenGatedAccessButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20.0),
            checkTokenGatedAccessButton.heightAnchor.constraint(equalToConstant: 40.0),
            checkTokenGatedAccessButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),

            chatroomInfoStack.topAnchor.constraint(equalTo: chatViewController.view.topAnchor, constant: 10.0),
            chatroomInfoStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0)
        ])

        checkTokenGatedAccessButton.addTarget(self, action: #selector(checkTokenGatedChatAccess), for: .touchUpInside)

        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc private func applicationWillEnterForeground() {}

    @objc private func checkTokenGatedChatAccess() {
        guard let chatRoomID = chatIdField.text,
              chatRoomID.count > 0
        else {
            showAlert(title: "Enter Room ID", msg: nil)
            return
        }
        guard let walletAddress = walletAddressField.text, walletAddress.count > 0 else {
            showAlert(title: "Enter Wallet Address", msg: nil)
            return
        }
        sdk.chat.getTokenGatedChatAccessDetails(roomID: chatRoomID, walletAddress: walletAddress) { result in
            switch result {
            case let .success(tokenGatedChatAccessInfo):
                self.showAlert(title: "Chat Room Access \(tokenGatedChatAccessInfo.access.rawValue)", msg: tokenGatedChatAccessInfo.details)
                log.dev("Chat Room Access : \(tokenGatedChatAccessInfo.access.rawValue) \n message : \(tokenGatedChatAccessInfo.details)")
            case let .failure(error):
                log.dev("Error: \(error.localizedDescription)")
                self.showAlert(title: "Chat Room Access Failed", msg: error.localizedDescription)
            }
        }
    }

    private func showAlert(title: String, msg: String?) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let alert = UIAlertController(
                title: title,
                message: msg,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

}
