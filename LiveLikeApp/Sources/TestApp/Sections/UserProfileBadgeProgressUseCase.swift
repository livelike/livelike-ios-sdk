//
//  UserProfileBadgeProgress.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 7/23/21.
//

import UIKit
import EngagementSDK

class UserProfileBadgeProgressUseCase: TestCaseViewController {

    private var contentSession: ContentSession!
    private var sdk: EngagementSDK
    private var appBadges: [Badge] = []

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "myCell")
        return tableView
    }()

    private let loadMoreButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Load More", for: .normal)
        return button
    }()

    private let mainStack: UIStackView = {
        let stack = UIStackView()
        stack.alignment = .fill
        stack.distribution = .fill
        stack.axis = .horizontal
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    private let profileInput: UITextField = {
        let profileInput: UITextField = UITextField()
        profileInput.placeholder = "Enter User Profile ID to use in Badge Progress"
        profileInput.translatesAutoresizingMaskIntoConstraints = false
        profileInput.borderStyle = .line
        profileInput.font = UIFont.systemFont(ofSize: 10.0)
        return profileInput
    }()

    init(sdk: EngagementSDK, programID: String) {
        self.sdk = sdk
        self.contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "User Profile Badges"

        super.viewDidLoad()

        mainStack.addArrangedSubview(profileInput)

        view.addSubview(mainStack)
        view.addSubview(tableView)
        view.addSubview(loadMoreButton)

        NSLayoutConstraint.activate([
            mainStack.topAnchor.constraint(equalTo: contentView.topAnchor),
            mainStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0),
            mainStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            mainStack.heightAnchor.constraint(equalToConstant: 60),

            tableView.topAnchor.constraint(equalTo: mainStack.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: loadMoreButton.topAnchor),

            loadMoreButton.topAnchor.constraint(equalTo: tableView.bottomAnchor),
            loadMoreButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            loadMoreButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            loadMoreButton.heightAnchor.constraint(equalToConstant: 60.0),
            loadMoreButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])

        loadMoreButton.addTarget(self, action: #selector(loadMoreButtonSelected), for: .touchUpInside)
        tableView.delegate = self
        tableView.dataSource = self

        sdk.getCurrentUserProfileID { result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let currentProfileID):
                self.profileInput.text = currentProfileID
            }
        }

        sdk.badges.getApplicationBadges(page: .first) { result in
            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(let receivedProfileBadges):
                self.appBadges.removeAll()
                self.appBadges.append(contentsOf: receivedProfileBadges.items)
                self.tableView.reloadData()
            }
        }
    }

    @objc private func loadMoreButtonSelected() {
        sdk.badges.getApplicationBadges(page: .next) { result in
            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(let receivedProfileBadges):
                self.appBadges.append(contentsOf: receivedProfileBadges.items)
                self.tableView.reloadData()
            }
        }
    }

    func getProfileBadgeProgress(profileID: String) {

    }
}

extension UserProfileBadgeProgressUseCase: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appBadges.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "myCell")
        else {
            return UITableViewCell()
        }

        let appBadge = self.appBadges[indexPath.row]
        cell.textLabel?.text = "\(indexPath.row) - \(appBadge.name)"
        cell.detailTextLabel?.text = appBadge.description ?? "No description"
        if let mediaData = try? Data(contentsOf: appBadge.badgeIconURL) {
            cell.imageView?.image = UIImage(data: mediaData)
        }

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let appBadge = self.appBadges[indexPath.row]

        sdk.badges.getProfileBadgeProgress(
            profileID: profileInput.text!,
            badgeIDs: [appBadge.id]
        ) { result in
            switch result {
            case .success(let badgeProgression):

                if let firstBadgeProgression = badgeProgression.first?.badgeProgression.first {
                    let alert = UIAlertController(
                        title: "Badge Progress",
                        message: "ID: \(firstBadgeProgression.rewardItemID) \n Reward Name: \(firstBadgeProgression.rewardItemName) \nProgress: \(firstBadgeProgression.currentRewardAmount) of \(firstBadgeProgression.rewardItemThreshold)",
                        preferredStyle: .alert
                    )
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                    self.present(alert, animated: true, completion: nil)
                }

            case .failure(let error):
                log.dev("error :\(error)")
            }
        }
    }
}
