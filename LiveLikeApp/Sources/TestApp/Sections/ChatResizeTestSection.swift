//
//  ChatResizeTestSection.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 7/16/20.
//

import EngagementSDK
import UIKit

final class ChatResizeTestSection: TestCaseViewController {
    private let contentSession: ContentSession

    let chatViewController: ChatViewController = ChatViewController()

    init(contentSession: ContentSession) {
        self.contentSession = contentSession
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        chatViewController.ignoreSizeRestrictions = true
        chatViewController.session = contentSession
        addChild(chatViewController)
        let gripFrame = CGRect(x: 0, y: 0, width: 300, height: 600)
        let resizeableContainer: RKUserResizableView = RKUserResizableView(frame: gripFrame)
        resizeableContainer.translatesAutoresizingMaskIntoConstraints = false
        resizeableContainer.contentView = chatViewController.view
        contentView.addSubview(resizeableContainer)
    }
}
