//
//  GroupChatSection.swift
//  LiveLikeTestApp
//
//  Created by Jelzon WORK on 2/19/20.
//

import EngagementSDK
import UIKit

class GroupChatSection: UIViewController {
    let chatController = ChatViewController()
    let changeButton = UIButton(type: .system)

    weak var currentChatSession: ChatSession?

    private let envBaseURL: URL
    private let theme: Theme
    private let sdk: EngagementSDK

    init(sdk: EngagementSDK, envBaseURL: URL, theme: Theme) {
        self.sdk = sdk
        self.envBaseURL = envBaseURL
        self.theme = theme
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white

        navigationController?.setNavigationBarHidden(false, animated: true)
        chatController.view.translatesAutoresizingMaskIntoConstraints = false
        changeButton.translatesAutoresizingMaskIntoConstraints = false

        addChild(chatController)
        view.addSubview(chatController.view)
        view.addSubview(changeButton)

        changeButton.setTitle("Set Room (Public Room)", for: .normal)
        changeButton.addTarget(self, action: #selector(changeButtonSelected), for: .touchUpInside)

        NSLayoutConstraint.activate([
            chatController.view.topAnchor.constraint(equalTo: view.topAnchor),
            chatController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            chatController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            chatController.view.bottomAnchor.constraint(equalTo: changeButton.topAnchor),

            changeButton.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            changeButton.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            changeButton.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            changeButton.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.1)
        ])

        chatController.setTheme(theme)
    }

    @objc private func changeButtonSelected() {
        LiveLikeAPI.getChatRoomsPage(baseURL: envBaseURL) { result in
            self.handleChatRoomsPageResult(result)
        }
    }

    private func handleChatRoomsPageResult(_ result: Result<ChatRoomResourcePage, Error>) {
        DispatchQueue.main.async {
            switch result {
            case let .success(chatRoomPage):
                let alert = UIAlertController(
                    title: "Select Chat Room",
                    message: "Select a chat room to enter.",
                    preferredStyle: .actionSheet
                )
                chatRoomPage.results.forEach { chatRoomResource in
                    alert.addAction(
                        UIAlertAction(
                            title: chatRoomResource.id,
                            style: .default,
                            handler: { _ in self.handleChatRoomSelected(chatRoomResource) }
                        )
                    )
                }
                if let nextPageURL = chatRoomPage.next {
                    alert.addAction(
                        UIAlertAction(title: "Load More...", style: .default, handler: { _ in
                            LiveLikeAPI.getChatRoomsPage(url: nextPageURL, completion: self.handleChatRoomsPageResult(_:))
                        })
                    )
                }
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case let .failure(error):
                let alert = UIAlertController(
                    title: "Error",
                    message: error.localizedDescription,
                    preferredStyle: .alert
                )
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    private func handleChatRoomSelected(_ chatRoomResource: ChatRoomResource) {
        var config = ChatSessionConfig(roomID: chatRoomResource.id)
        config.shouldDisplayAvatar = true
        sdk.connectChatRoom(config: config, completion: { [weak self] result in
            DispatchQueue.main.async {
                guard let self = self else { return }
                switch result {
                case let .success(chatSession):
                    self.currentChatSession = chatSession
                    self.chatController.setChatSession(chatSession)

                    self.sdk.getChatUserMutedStatus(roomID: chatSession.roomID) { result in
                        switch result {
                        case let .success(mutedStatus):
                            if mutedStatus.isMuted {
                                let alert = UIAlertController(title: "Hey!", message: "No offence, but you're muted!", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                                self.present(alert, animated: true, completion: nil)
                            }
                        case let .failure(error):
                            log.dev("error getting muted status \(error.localizedDescription)")
                        }
                    }
                case let .failure(error):
                    print(error)
                }
            }
        })
    }
}
