//
//  LoaderViewController.swift
//  LiveLikeTestApp
//
//  Created by Heinrich Dahms on 2019-03-01.
//

import EngagementSDK
import UIKit

class LoaderViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeEngagementSDK()
    }

    func initializeEngagementSDK() {
        let homeVC = HomeViewController()
        homeVC.modalPresentationStyle = .fullScreen
        present(homeVC, animated: true, completion: nil)
    }

    func segueHome() {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "loadingComplete", sender: self)
        }
    }
}
