//
//  BadgeProfilesUseCase.swift
//  LiveLikeTestApp
//
//  Created by Keval Shah on 22/11/22.
//

import UIKit
import EngagementSDK

class BadgeProfilesUseCase: TestCaseViewController {

    private var contentSession: ContentSession!
    private var sdk: EngagementSDK
    private var badgeProfiles: [BadgeProfile] = []

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.allowsSelection = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "myCell")
        return tableView
    }()

    private let loadMoreButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Load More", for: .normal)
        return button
    }()

    private let mainStack: UIStackView = {
        let stack = UIStackView()
        stack.alignment = .fill
        stack.distribution = .fill
        stack.axis = .horizontal
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    private let badgeIDInput: UITextField = {
        let badgeIDInput: UITextField = UITextField()
        badgeIDInput.placeholder = "Enter User Profile ID"
        badgeIDInput.translatesAutoresizingMaskIntoConstraints = false
        badgeIDInput.borderStyle = .line
        badgeIDInput.font = UIFont.systemFont(ofSize: 10.0)
        return badgeIDInput
    }()

    private let getProfilesBtn: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Get Profiles", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    init(sdk: EngagementSDK, programID: String) {
        self.sdk = sdk
        self.contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "User Profile Badges"

        super.viewDidLoad()

        mainStack.addArrangedSubview(badgeIDInput)
        mainStack.addArrangedSubview(getProfilesBtn)

        view.addSubview(mainStack)
        view.addSubview(tableView)
        view.addSubview(loadMoreButton)

        NSLayoutConstraint.activate([
            mainStack.topAnchor.constraint(equalTo: contentView.topAnchor),
            mainStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0),
            mainStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            mainStack.heightAnchor.constraint(equalToConstant: 60),

            tableView.topAnchor.constraint(equalTo: mainStack.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: loadMoreButton.topAnchor),

            loadMoreButton.topAnchor.constraint(equalTo: tableView.bottomAnchor),
            loadMoreButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            loadMoreButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            loadMoreButton.heightAnchor.constraint(equalToConstant: 60.0),
            loadMoreButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])

        loadMoreButton.addTarget(self, action: #selector(loadMoreButtonSelected), for: .touchUpInside)
        getProfilesBtn.addTarget(self, action: #selector(getBadges), for: .touchUpInside)
        tableView.delegate = self
        tableView.dataSource = self
    }

    @objc private func getBadges() {

        sdk.badges.getBadgeProfiles(
            badgeID: badgeIDInput.text!,
            page: .first
        ) { result in
            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(let receivedBadgeProfiles):
                print(receivedBadgeProfiles.items.count)
                self.badgeProfiles.removeAll()
                self.badgeProfiles.append(contentsOf: receivedBadgeProfiles.items)
                self.tableView.reloadData()
                self.badgeIDInput.endEditing(true)
            }
        }

    }

    @objc private func loadMoreButtonSelected() {
        if let badgeID = badgeIDInput.text {
            sdk.badges.getBadgeProfiles(badgeID: badgeID, page: .next) { result in
                switch result {
                case .failure(let error):
                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                    self.present(alert, animated: true, completion: nil)
                case .success(let receivedBadgeProfiles):
                    self.badgeProfiles.append(contentsOf: receivedBadgeProfiles.items)
                    self.tableView.reloadData()
                }
            }
        }
    }
}

extension BadgeProfilesUseCase: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return badgeProfiles.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            var cell = tableView.dequeueReusableCell(withIdentifier: "myCell")
        else {
            return UITableViewCell()
        }

        cell = UITableViewCell(style: .subtitle, reuseIdentifier: "myCell")

        let badgeProfile = self.badgeProfiles[indexPath.row]
        cell.textLabel?.text = "\(indexPath.row) - \(badgeProfile.badgeID)"

        cell.detailTextLabel?.adjustsFontSizeToFitWidth = true
        cell.detailTextLabel?.text = badgeProfile.profile.nickname

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
