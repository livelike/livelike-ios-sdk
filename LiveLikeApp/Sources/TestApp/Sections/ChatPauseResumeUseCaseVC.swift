//
//  ChatPauseResumeUseCaseVC.swift
//
//
//  Created by Mike Moloksher on 9/8/20.
//

import EngagementSDK
import UIKit

class ContentSessionChatPauseResumeUseCaseVC: TestCaseViewController {
    let chatController = ChatViewController()
    var session: ContentSession?

    private let sdk: EngagementSDK
    private let programID: String
    private let theme: Theme

    private let pauseToggleBTN: UIButton = {
        let pauseToggleBTN: UIButton = UIButton()
        pauseToggleBTN.setTitle("State: Active", for: .normal)
        pauseToggleBTN.backgroundColor = .blue
        pauseToggleBTN.translatesAutoresizingMaskIntoConstraints = false
        return pauseToggleBTN
    }()

    init(sdk: EngagementSDK, programID: String) {
        self.sdk = sdk
        self.programID = programID
        theme = Theme()
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        addChild(viewController: chatController, into: contentView)

        let config = SessionConfiguration(programID: programID)
        session = sdk.contentSession(config: config)

        chatController.session = session
        chatController.setTheme(theme)

        super.viewDidLoad()

        contentView.addSubview(pauseToggleBTN)
        let constraints = [
            pauseToggleBTN.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10.0),
            pauseToggleBTN.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
            pauseToggleBTN.widthAnchor.constraint(equalToConstant: 150),
            pauseToggleBTN.heightAnchor.constraint(equalToConstant: 44.0)
        ]
        NSLayoutConstraint.activate(constraints)

        pauseToggleBTN.addTarget(self, action: #selector(toggleChatPause), for: .touchUpInside)
    }

    @objc func toggleChatPause() {
        if pauseToggleBTN.titleLabel?.text == "State: Active" {
            pauseToggleBTN.setTitle("State: Paused", for: .normal)
            chatController.shouldShowIncomingMessages = false
            chatController.isChatInputVisible = false
        } else {
            pauseToggleBTN.setTitle("State: Active", for: .normal)
            chatController.shouldShowIncomingMessages = true
            chatController.isChatInputVisible = true
        }
    }
}

class GroupChatChatPauseResumeUseCaseVC: TestCaseViewController {
    let chatController = ChatViewController()
    let changeButton = UIButton(type: .system)

    weak var currentChatSession: ChatSession?

    private let pauseToggleBTN: UIButton = {
        let pauseToggleBTN: UIButton = UIButton()
        pauseToggleBTN.setTitle("State: Active", for: .normal)
        pauseToggleBTN.backgroundColor = .blue
        pauseToggleBTN.translatesAutoresizingMaskIntoConstraints = false
        return pauseToggleBTN
    }()

    private let envBaseURL: URL
    private let theme: Theme
    private let sdk: EngagementSDK

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    init(sdk: EngagementSDK, envBaseURL: URL) {
        self.sdk = sdk
        self.envBaseURL = envBaseURL
        theme = Theme()
        super.init()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white

        navigationController?.setNavigationBarHidden(false, animated: true)
        chatController.view.translatesAutoresizingMaskIntoConstraints = false
        changeButton.translatesAutoresizingMaskIntoConstraints = false

        addChild(viewController: chatController, into: contentView)

        contentView.addSubview(chatController.view)
        contentView.addSubview(changeButton)

        changeButton.setTitle("Set Room (Public Room)", for: .normal)
        changeButton.addTarget(self, action: #selector(changeButtonSelected), for: .touchUpInside)

        NSLayoutConstraint.activate([
            chatController.view.topAnchor.constraint(equalTo: contentView.topAnchor),
            chatController.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            chatController.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            chatController.view.bottomAnchor.constraint(equalTo: changeButton.topAnchor),

            changeButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            changeButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            changeButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
            changeButton.heightAnchor.constraint(equalToConstant: 44.0)
        ])

        chatController.setTheme(theme)

        contentView.addSubview(pauseToggleBTN)
        let constraints = [
            pauseToggleBTN.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10.0),
            pauseToggleBTN.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
            pauseToggleBTN.widthAnchor.constraint(equalToConstant: 150),
            pauseToggleBTN.heightAnchor.constraint(equalToConstant: 44.0)
        ]
        NSLayoutConstraint.activate(constraints)

        pauseToggleBTN.addTarget(self, action: #selector(toggleChatPause), for: .touchUpInside)
    }

    @objc private func changeButtonSelected() {
        LiveLikeAPI.getChatRoomsPage(baseURL: envBaseURL) { result in
            self.handleChatRoomsPageResult(result)
        }
    }

    @objc func toggleChatPause() {
        if pauseToggleBTN.titleLabel?.text == "State: Active" {
            pauseToggleBTN.setTitle("State: Paused", for: .normal)
            chatController.shouldShowIncomingMessages = false
            chatController.isChatInputVisible = false
        } else {
            pauseToggleBTN.setTitle("State: Active", for: .normal)
            chatController.shouldShowIncomingMessages = true
            chatController.isChatInputVisible = true
        }
    }

    private func handleChatRoomsPageResult(_ result: Result<ChatRoomResourcePage, Error>) {
        DispatchQueue.main.async {
            switch result {
            case let .success(chatRoomPage):
                let alert = UIAlertController(
                    title: "Select Chat Room",
                    message: "Select a chat room to enter.",
                    preferredStyle: .actionSheet
                )
                chatRoomPage.results.forEach { chatRoomResource in
                    alert.addAction(
                        UIAlertAction(
                            title: chatRoomResource.id,
                            style: .default,
                            handler: { _ in self.handleChatRoomSelected(chatRoomResource) }
                        )
                    )
                }
                if let nextPageURL = chatRoomPage.next {
                    alert.addAction(
                        UIAlertAction(title: "Load More...", style: .default, handler: { _ in
                            LiveLikeAPI.getChatRoomsPage(url: nextPageURL, completion: self.handleChatRoomsPageResult(_:))
                        })
                    )
                }
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case let .failure(error):
                let alert = UIAlertController(
                    title: "Error",
                    message: error.localizedDescription,
                    preferredStyle: .alert
                )
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    private func handleChatRoomSelected(_ chatRoomResource: ChatRoomResource) {
        let config = ChatSessionConfig(roomID: chatRoomResource.id)
        sdk.connectChatRoom(config: config, completion: { [weak self] result in
            DispatchQueue.main.async {
                guard let self = self else { return }
                switch result {
                case let .success(chatSession):
                    self.currentChatSession = chatSession
                    self.chatController.setChatSession(chatSession)
                case let .failure(error):
                    print(error)
                }
            }
        })
    }
}
