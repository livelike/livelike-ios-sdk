//
//  ChatAvatarUseCaseVC.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 9/1/20.
//

import EngagementSDK
import UIKit

class ContentSessionChatAvatarVC: TestCaseViewController {
    let chatController = ChatViewController()
    let widgetViewController = WidgetPopupViewController()
    var session: ContentSession?

    private let sdk: EngagementSDK
    private let programID: String?
    private let theme: Theme?
    private let chatAvatarURL: URL
    private let showFilteredMessages: Bool

    init(sdk: EngagementSDK, programID: String, avatarURL: URL, showFilteredMessages: Bool) {
        self.sdk = sdk
        self.programID = programID
        self.showFilteredMessages = showFilteredMessages

        theme = Theme()
        chatAvatarURL = avatarURL
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        addChild(viewController: chatController, into: contentView)
        addChild(viewController: widgetViewController, into: contentView)

        if let programID = programID {
            let config = SessionConfiguration(programID: programID)
            config.chatShouldDisplayAvatar = true
            config.includeFilteredChatMessages = showFilteredMessages
            session = sdk.contentSession(config: config)
        } else {
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return
            }

            let config = SessionConfiguration(programID: programID)
            config.chatShouldDisplayAvatar = true
            config.includeFilteredChatMessages = showFilteredMessages
            session = sdk.contentSession(config: config)
        }

        chatController.session = session
        widgetViewController.session = session
        session?.getChatSession(completion: { result in
            switch result {
            case let .success(chatSession):
                chatSession.avatarURL = self.chatAvatarURL
            case let .failure(error):
                print(error)
            }
        })

        if let theme = theme {
            chatController.setTheme(theme)
        }

        super.viewDidLoad()
    }
}

class GroupChatAvatarVC: UIViewController {
    let chatController = ChatViewController()
    let changeButton = UIButton(type: .system)

    weak var currentChatSession: ChatSession?

    private let envBaseURL: URL
    private let theme: Theme
    private let sdk: EngagementSDK
    private let chatAvatarURL: URL
    private let showFilteredMessages: Bool

    private let connectRoomButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Connect Room", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private let connectRoomField: UITextField = {
        let roomTitle: UITextField = UITextField()
        roomTitle.placeholder = "Enter Chat Room ID"
        roomTitle.translatesAutoresizingMaskIntoConstraints = false
        roomTitle.borderStyle = .line
        roomTitle.font = UIFont.systemFont(ofSize: 10.0)
        return roomTitle
    }()

    private let chatroomConnectStack: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.spacing = 5
        return stackView
    }()

    init(sdk: EngagementSDK, envBaseURL: URL, avatarURL: URL, showFilteredMessages: Bool) {
        self.sdk = sdk
        self.envBaseURL = envBaseURL
        self.showFilteredMessages = showFilteredMessages

        theme = Theme()
        chatAvatarURL = avatarURL
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white

        navigationController?.setNavigationBarHidden(false, animated: true)
        chatController.view.translatesAutoresizingMaskIntoConstraints = false
        changeButton.translatesAutoresizingMaskIntoConstraints = false

        addChild(chatController)
        view.addSubview(chatController.view)

        chatroomConnectStack.addArrangedSubview(connectRoomField)
        chatroomConnectStack.addArrangedSubview(connectRoomButton)
        view.addSubview(chatroomConnectStack)
        view.addSubview(changeButton)

        changeButton.setTitle("Pick Room (Public Room)", for: .normal)
        changeButton.addTarget(self, action: #selector(changeButtonSelected), for: .touchUpInside)
        connectRoomButton.addTarget(self, action: #selector(connectRoom), for: .touchUpInside)

        NSLayoutConstraint.activate([
            chatController.view.topAnchor.constraint(equalTo: view.topAnchor),
            chatController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            chatController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            chatController.view.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.8),

            chatroomConnectStack.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            chatroomConnectStack.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            chatroomConnectStack.topAnchor.constraint(equalTo: chatController.view.bottomAnchor),
            chatroomConnectStack.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.05),

            changeButton.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            changeButton.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            changeButton.topAnchor.constraint(equalTo: chatroomConnectStack.bottomAnchor),
            changeButton.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.15)
        ])

        chatController.setTheme(theme)
    }

    @objc private func changeButtonSelected() {
        LiveLikeAPI.getChatRoomsPage(baseURL: envBaseURL) { result in
            self.handleChatRoomsPageResult(result)
        }
    }

    @objc private func connectRoom() {
        guard let chatRoomID = connectRoomField.text,
              chatRoomID.count > 0
        else {
            showAlert(title: "Enter Room ID", msg: nil)
            return
        }
        var config = ChatSessionConfig(roomID: chatRoomID)
        config.shouldDisplayAvatar = true
        config.includeFilteredMessages = showFilteredMessages
        sdk.connectChatRoom(config: config, completion: { [weak self] result in
            DispatchQueue.main.async {
                guard let self = self else { return }
                switch result {
                case let .success(chatSession):
                    self.currentChatSession = chatSession
                    self.chatController.setChatSession(chatSession)
                    chatSession.avatarURL = self.chatAvatarURL
                case let .failure(error):
                    print(error)
                }
            }
        })
    }

    private func handleChatRoomsPageResult(_ result: Result<ChatRoomResourcePage, Error>) {
        DispatchQueue.main.async {
            switch result {
            case let .success(chatRoomPage):
                let alert = UIAlertController(
                    title: "Select Chat Room",
                    message: "Select a chat room to enter.",
                    preferredStyle: .actionSheet
                )
                chatRoomPage.results.forEach { chatRoomResource in
                    alert.addAction(
                        UIAlertAction(
                            title: chatRoomResource.id,
                            style: .default,
                            handler: { _ in self.handleChatRoomSelected(chatRoomResource) }
                        )
                    )
                }
                if let nextPageURL = chatRoomPage.next {
                    alert.addAction(
                        UIAlertAction(title: "Load More...", style: .default, handler: { _ in
                            LiveLikeAPI.getChatRoomsPage(url: nextPageURL, completion: self.handleChatRoomsPageResult(_:))
                        })
                    )
                }
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case let .failure(error):
                let alert = UIAlertController(
                    title: "Error",
                    message: error.localizedDescription,
                    preferredStyle: .alert
                )
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    private func handleChatRoomSelected(_ chatRoomResource: ChatRoomResource) {
        var config = ChatSessionConfig(roomID: chatRoomResource.id)
        config.shouldDisplayAvatar = true
        config.includeFilteredMessages = showFilteredMessages
        sdk.connectChatRoom(config: config, completion: { [weak self] result in
            DispatchQueue.main.async {
                guard let self = self else { return }
                switch result {
                case let .success(chatSession):
                    self.currentChatSession = chatSession
                    self.chatController.setChatSession(chatSession)
                    chatSession.avatarURL = self.chatAvatarURL
                case let .failure(error):
                    print(error)
                }
            }
        })
    }

    private func showAlert(title: String, msg: String?) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let alert = UIAlertController(
                title: title,
                message: msg ?? "",
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

}
