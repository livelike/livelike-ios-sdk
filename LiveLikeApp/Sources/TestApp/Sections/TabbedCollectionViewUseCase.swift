//
//  TabbedCollectionViewUseCase.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 5/7/21.
//

import EngagementSDK
import UIKit

class TabbedCollectionViewUseCase: TestCaseViewController {
    private let clientID: String
    private let programID: String

    private var sdk: EngagementSDK!
    private var chatPopupSession: ContentSession
    private var widgetTimelineSession: ContentSession

    private let collectionViewLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        return layout
    }()

    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: self.collectionViewLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(CustomTabbedCell.self, forCellWithReuseIdentifier: "myCell")
        collectionView.isPagingEnabled = true
        return collectionView
    }()

    private var chatWithPopupWidgets: ChatWithPopupWidgetsUseCase
    private var widgetTimeline: WidgetTimelineViewController

    init(
        clientID: String,
        programID: String
    ) {
        self.clientID = clientID
        self.programID = programID
        sdk = EngagementSDK(config: EngagementSDKConfig(clientID: clientID))

        chatPopupSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
        chatWithPopupWidgets = ChatWithPopupWidgetsUseCase(contentSession: chatPopupSession)

        widgetTimelineSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
        widgetTimeline = WidgetTimelineViewController(contentSession: widgetTimelineSession)
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "<- Swipe ->"
        super.viewDidLoad()

        collectionView.dataSource = self
        collectionView.delegate = self

        addChild(chatWithPopupWidgets)
        addChild(widgetTimeline)

        contentView.addSubview(collectionView)
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: contentView.topAnchor),
            collectionView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            collectionView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
}

extension TabbedCollectionViewUseCase: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myCell", for: indexPath) as? CustomTabbedCell else { return UICollectionViewCell() }

        if indexPath.row == 0 {
            cell.configure(controller: chatWithPopupWidgets)
        } else {
            cell.configure(controller: widgetTimeline)
        }

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(
            width: collectionView.frame.width,
            height: collectionView.frame.height
        )
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
}

class CustomTabbedCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)

        backgroundColor = .yellow
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configure(controller: UIViewController) {
        addSubview(controller.view)
        NSLayoutConstraint.activate([
            controller.view.topAnchor.constraint(equalTo: topAnchor),
            controller.view.trailingAnchor.constraint(equalTo: trailingAnchor),
            controller.view.leadingAnchor.constraint(equalTo: leadingAnchor),
            controller.view.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}
