//
//  MultipleSessionWidgetsUseCase.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 5/17/21.
//

import EngagementSDK
import UIKit

class MultipleSessionsWidgetsUseCase: TestCaseViewController {
    let widgetViewController = WidgetPopupViewController()
    let widgetView = UIView()
    lazy var widgetCreateView = WidgetCreateView(
        programID: self.programID,
        apiToken: self.apiToken,
        timeoutSeconds: self.timeoutSeconds,
        apiOrigin: self.apiOrigin
    )

    let sessionToggle: UISwitch = {
        let toggle = UISwitch()
        toggle.backgroundColor = .lightGray
        toggle.translatesAutoresizingMaskIntoConstraints = false
        toggle.isOn = true
        return toggle
    }()

    let sessionLabel: UILabel = {
        let label = UILabel()
        label.text = "Session:"
        label.backgroundColor = .lightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        return label
    }()

    var session: ContentSession?

    let sdk: EngagementSDK
    let apiToken: String
    let programID: String = "6834f1fd-f24d-4538-ba51-63544f9d78eb" // Pubnunb Prod
    let programID2: String = "09d93835-ee52-4757-976c-ea09d6a5798c" // LiveTest1
    let theme: Theme
    let apiOrigin: URL
    private let timeoutSeconds: Int
    private var timeoutSecondsISO8601: String {
        return "P0DT00H00M\(timeoutSeconds)S"
    }

    init(
        sdk: EngagementSDK,
        apiToken: String,
        programID: String,
        theme: Theme,
        timeoutSeconds: Int,
        apiOrigin: URL
    ) {
        self.sdk = sdk
        self.apiToken = apiToken
        self.theme = theme
        self.timeoutSeconds = timeoutSeconds
        self.apiOrigin = apiOrigin
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "Multiple Session Widgets"
        vcInfo = "This test case is create to ensure that when we switch sessions, we clean up the widgets queue"

        super.viewDidLoad()

        view.backgroundColor = .darkGray

        widgetView.translatesAutoresizingMaskIntoConstraints = false
        widgetCreateView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(widgetView)
        contentView.addSubview(widgetCreateView)
        contentView.addSubview(sessionLabel)
        contentView.addSubview(sessionToggle)
        addChild(viewController: widgetViewController, into: widgetView)
        applyLayoutConstraints()
        widgetViewController.setTheme(theme)

        widgetView.backgroundColor = .gray

        sessionToggle.addTarget(self, action: #selector(toggleSession), for: UIControl.Event.valueChanged)

        session?.close()
        widgetViewController.session = nil
        session = nil

        if self.sessionToggle.isOn {
            let config = SessionConfiguration(programID: programID)
            sdk.createContentSession(config: config) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let widgetSession):
                    self.session = widgetSession
                    self.widgetViewController.session = self.session
                    self.sessionLabel.text = "Session: Pubnub Prod"
                case .failure:
                    self.session = nil
                }
            }

        } else {
            let config = SessionConfiguration(programID: programID2)
            sdk.createContentSession(config: config) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let widgetSession):
                    self.session = widgetSession
                    self.widgetViewController.session = self.session
                    self.sessionLabel.text = "Session: LiveTest1"
                case .failure:
                    self.session = nil
                }
            }
        }
    }

    @objc func toggleSession(mySwitch: UISwitch) {

        session?.close()
        widgetViewController.session = nil
        session = nil

        if self.sessionToggle.isOn {
            let config = SessionConfiguration(programID: programID)
            sdk.createContentSession(config: config) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let widgetSession):
                    self.session = widgetSession
                    self.widgetViewController.session = self.session
                    self.sessionLabel.text = "Session: Pubnub Prod"
                case .failure:
                    self.session = nil
                }
            }

        } else {
            let config = SessionConfiguration(programID: programID2)
            sdk.createContentSession(config: config) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let widgetSession):
                    self.session = widgetSession
                    self.widgetViewController.session = self.session
                    self.sessionLabel.text = "Session: LiveTest1"
                case .failure:
                    self.session = nil
                }
            }
        }
    }

    func applyLayoutConstraints() {
        NSLayoutConstraint.activate([
            widgetView.topAnchor.constraint(equalTo: contentView.topAnchor),
            widgetView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.5),
            widgetView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            widgetCreateView.topAnchor.constraint(equalTo: widgetView.bottomAnchor),
            widgetCreateView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetCreateView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            widgetCreateView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

            sessionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            sessionLabel.bottomAnchor.constraint(equalTo: widgetCreateView.topAnchor, constant: -15),

            sessionToggle.centerYAnchor.constraint(equalTo: sessionLabel.centerYAnchor),
            sessionToggle.leadingAnchor.constraint(equalTo: sessionLabel.trailingAnchor, constant: 10.0)
        ])
    }

    deinit {
        log.dev("deinit")
    }
}
