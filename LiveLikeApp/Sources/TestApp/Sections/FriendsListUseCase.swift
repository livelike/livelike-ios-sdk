//
//  FriendsListUseCase.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 3/30/23.
//

import UIKit
import LiveLikeSwift

enum JoinState: String {
    case joining = "Joining"
    case joined = "Joined"
    case leaving = "Leaving"
    case timeout = "Timed Out"
    case offline = "Offline"
}

@available(iOS 13.0, *)
class FriendsListUseCase: UIViewController {
    
    private let textField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = .systemBackground
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = "Channel ID"
        return textField
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .systemBackground
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.dataSource = self
        return tableView
        
    }()
    
    private lazy var subscribeButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .systemBackground
        button.setTitle("Subscribe", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(
            self,
            action: #selector(subscribeButtonPressed),
            for: .touchUpInside
        )
        return button
    }()
    
    private lazy var unsubscribeButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .systemBackground
        button.setTitle("Unsubscribe", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(
            self,
            action: #selector(unsubscribeButtonPressed),
            for: .touchUpInside
        )
        return button
    }()
    
    private lazy var joinButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .systemBackground
        button.setTitle("Join", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(
            self,
            action: #selector(joinButtonPressed),
            for: .touchUpInside
        )
        return button
    }()
    
    private lazy var leaveButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .systemBackground
        button.setTitle("Leave", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(
            self,
            action: #selector(leaveButtonPressed),
            for: .touchUpInside
        )
        return button
    }()
    
    private lazy var statusLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .systemBackground
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var occupantCountLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .systemBackground
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let livelike: LiveLike
    private var profileID: String?
    
    private var isSubscribed: Bool = false {
        didSet {
            self.updateStatusText()
        }
    }
    
    private var joinState: JoinState = .offline {
        didSet {
            self.updateStatusText()
        }
    }
    
    private var occupants: Set<String> = Set() {
        didSet {
            self.updateOccupantsCountText()
        }
    }
    
    init(livelike: LiveLike) {
        self.livelike = livelike
        super.init(nibName: nil, bundle: nil)
        
        livelike.getCurrentUserProfileID { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                print(error)
            case .success(let profileID):
                self.profileID = profileID
            }
        }
        livelike.presenceClient.setDelegate(self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        updateStatusText()
        updateOccupantsCountText()
    }
    
    private func setupLayout() {
        let controlsStackView: UIStackView = {
            let stackView = UIStackView(arrangedSubviews: [
                textField,
                subscribeButton,
                unsubscribeButton,
                joinButton,
                leaveButton,
                statusLabel,
                occupantCountLabel
            ])
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.axis = .vertical
            stackView.distribution = .fillEqually
            stackView.alignment = .fill
            return stackView
        }()
        view.addSubview(controlsStackView)
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            controlsStackView.topAnchor.constraint(equalTo: view.safeTopAnchor),
            controlsStackView.leadingAnchor.constraint(equalTo: view.safeLeadingAnchor),
            controlsStackView.trailingAnchor.constraint(equalTo: view.safeTrailingAnchor),
            controlsStackView.heightAnchor.constraint(equalToConstant: 200),
            
            tableView.topAnchor.constraint(equalTo: controlsStackView.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.safeLeadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeTrailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeBottomAnchor)
        ])
    }
    
    @objc private func subscribeButtonPressed() {
        guard
            let channel = textField.text,
            !channel.isEmpty
        else { return }
        self.isSubscribed = true
        self.livelike.presenceClient.subscribe(
            to: [channel]
        )
        
        self.livelike.presenceClient.hereNow(
            in: [channel]
        ) { result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let userIDsByChannel):
                guard let userIDs = userIDsByChannel[channel] else {
                    return
                }
                self.occupants = Set(userIDs)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    @objc private func unsubscribeButtonPressed() {
        guard
            let channel = textField.text,
            !channel.isEmpty
        else { return }
        self.isSubscribed = false
        self.occupants = []
        self.livelike.presenceClient.unsubscribe(
            from: [channel]
        )
        self.tableView.reloadData()
    }
    
    @objc private func joinButtonPressed() {
        guard
            let channel = textField.text,
            !channel.isEmpty
        else { return }
        self.joinState = .joining
        self.livelike.presenceClient.joinChannels([channel])
    }
    
    @objc private func leaveButtonPressed() {
        guard
            let channel = textField.text,
            !channel.isEmpty
        else { return }
        self.joinState = .leaving
        self.livelike.presenceClient.leaveChannels([channel])
    }
    
    private func updateStatusText() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.statusLabel.text = "STATUS: \(self.isSubscribed ? "subscribed" : "not subscribed") | \(self.joinState)"
        }
    }
    
    private func updateOccupantsCountText() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.occupantCountLabel.text = {
                if self.isSubscribed {
                    return "OCCUPANCY: \(self.occupants.count)"
                } else {
                    return "OCCUPANCY: n/a"
                }
            }()
        }
    }
}

@available(iOS 13.0, *)
extension FriendsListUseCase: PresenceClientDelegate {
    func presenceClient(
        _ presenceClient: PresenceClient,
        didReceivePresenceEvents presenceEvents: [PresenceEvent]
    ) {
        presenceEvents.forEach { event in
            switch event.action {
            case .join:
                if event.userID == self.profileID {
                    self.joinState = .joined
                }
                self.occupants.insert(event.userID)
            case .leave:
                if event.userID == self.profileID {
                    self.joinState = .offline
                }
                self.occupants.remove(event.userID)
            default:
                break
            }
        }
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.tableView.reloadData()
        }
    }
}

@available(iOS 13.0, *)
extension FriendsListUseCase: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.occupants.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        let occupant = self.occupants.allObjects[indexPath.row]
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = {
            if occupant == self.profileID {
                return "(You)\(occupant)"
            } else {
                return occupant
            }
        }()
        
        cell.contentView.addSubview(label)
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: cell.contentView.topAnchor),
            label.leadingAnchor.constraint(equalTo: cell.contentView.leadingAnchor),
            label.trailingAnchor.constraint(equalTo: cell.contentView.trailingAnchor),
            label.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor),
        ])
        
        return cell
        
    }
}
