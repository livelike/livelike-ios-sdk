//
//  UnreadMessageCountSection.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 2/25/20.
//

import EngagementSDK
import UIKit

class UnreadMessageCountSection: TestCaseViewController {
    /// The stored array of chat room IDs to automatically join on load
    private let autoConnectChatRoomIDsKey = "EngagementSDK.TestApp.joinedRooms"
    private let earliestUnreadMessageDateByIDKey = "EngagementSDK.TestApp.latestMessageDateByRoomID"

    // The list of chat sessions that are currently open
    // This is stored in user defaults to automatically join these rooms in the next app session
    private var chatSessions: [ChatSession] = [] {
        didSet {
            UserDefaults.standard.set(chatSessions.map { $0.roomID }, forKey: autoConnectChatRoomIDsKey)
        }
    }

    // The list of all possible chat rooms to connect to
    private var allChatRooms = [String]()

    // A dictionary of the count of unread messages keyed by room id
    private var unreadCountsByRoomID: [String: Int] = [:]

    // A dictionary of the timetoken of the newest message that the user has received keyed by room id
    // We store this in user defaults to fetch the count of messages that we've missed since the last app session
    private var timetokenOfNewestMessageByRoomID: [String: TimeToken] = [:] {
        didSet {
            let data = NSKeyedArchiver.archivedData(withRootObject: timetokenOfNewestMessageByRoomID)
            UserDefaults.standard.set(data, forKey: earliestUnreadMessageDateByIDKey)
        }
    }

    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "unreadMessageCountCell")

        let longPressGresture = UILongPressGestureRecognizer(target: self, action: #selector(handleCellLongPress))
        longPressGresture.minimumPressDuration = 0.5
        tableView.addGestureRecognizer(longPressGresture)
        return tableView
    }()

    private let chatViewController = ChatViewController()
    private var chatContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private let sdk: EngagementSDK
    private let envBaseURL: URL

    init(envBaseURL: URL, sdk: EngagementSDK) {
        self.envBaseURL = envBaseURL
        self.sdk = sdk
        super.init()
        timetokenOfNewestMessageByRoomID = {
            if let data = UserDefaults.standard.data(forKey: earliestUnreadMessageDateByIDKey) {
                return NSKeyedUnarchiver.unarchiveObject(with: data) as? [String: TimeToken] ?? [:]
            } else {
                return [:]
            }
        }()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        vcInfo = """
        Green Background = Joined
        Red Background = Not Joined
        White Text = Entered

        Long Press to Join/Leave a Chat Room
        Tap to Enter a Chat Room

        The joined rooms are stored in
        local storage and will be automatically
        re-joined when app restarts
        """

        view.backgroundColor = .black

        addChild(viewController: chatViewController, into: chatContainer)

        contentView.addSubview(chatContainer)
        contentView.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: contentView.topAnchor),
            tableView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.4),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            chatContainer.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            chatContainer.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.6),
            chatContainer.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            chatContainer.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
        ])

        // Disable tableView interactions until we've auto connected to chat rooms
        tableView.allowsSelection = false
        tableView.isHidden = true

        allChatRooms = GroupChatManager.shared.chatRoomIds
        tableView.reloadData()
        autoJoinRoomsFromUserDefaults { _ in
            DispatchQueue.main.async {
                self.tableView.allowsSelection = true
                self.tableView.isHidden = false
                self.tableView.reloadData()
            }
        }
    }

    @objc private func handleCellLongPress(longPressGesture: UILongPressGestureRecognizer) {
        let p = longPressGesture.location(in: tableView)
        guard let indexPath = tableView.indexPathForRow(at: p) else {
            print("Long press on table view, not row.")
            return
        }
        guard longPressGesture.state == .began else { return }
        let chatRoomID = allChatRooms[indexPath.row]
        joinOrLeaveRoom(chatRoomID: chatRoomID)
    }

    /// Connects to chat rooms that were previously connected to in the previous app session
    private func autoJoinRoomsFromUserDefaults(completion: @escaping (Result<Void, Error>) -> Void) {
        guard let autoJoinRoomIDs = UserDefaults.standard.array(forKey: autoConnectChatRoomIDsKey) as? [String] else {
            print("Found no rooms to auto join from User Defaults.")
            return completion(.success(()))
        }

        DispatchQueue.global().async { [weak self] in
            guard let self = self else { return }
            var connectionsComplete: Int = 0 {
                didSet {
                    if connectionsComplete == autoJoinRoomIDs.count {
                        // All connections complete
                        completion(.success(()))
                    }
                }
            }

            autoJoinRoomIDs.forEach { roomID in

                let config = ChatSessionConfig(roomID: roomID)
                self.sdk.connectChatRoom(config: config, completion: { [weak self] result in
                    guard let self = self else { return }
                    switch result {
                    case let .success(chatSession):
                        chatSession.addDelegate(self)
                        self.chatSessions.append(chatSession)

                        // Get latest messages if there is an unread message TimeToken
                        // Otherwise set the unread message date to last message's TimeToken
                        // If no messages exist in the chat room, use TimeToken.now as an approximate earliest unread message TimeToken
                        if let latestMessageDate = self.timetokenOfNewestMessageByRoomID[roomID] {
                            chatSession.getMessageCount(since: latestMessageDate, completion: { result in
                                switch result {
                                case let .success(count):
                                    DispatchQueue.main.async {
                                        self.unreadCountsByRoomID[roomID] = count > 0 ? (count - 1) : 0
                                        self.tableView.reloadData()
                                    }
                                case let .failure(error):
                                    self.showErrorAlert(message: error.localizedDescription)
                                }
                                connectionsComplete += 1
                            })
                        } else {
                            self.updateTimetokenOfNewestMessageIfOlder(
                                roomID: roomID,
                                date: chatSession.messages.last?.createdAt ?? TimeToken.now
                            )
                        }

                    case let .failure(error):
                        self.showErrorAlert(message: error.localizedDescription)
                        connectionsComplete += 1
                    }
                })
            }
        }
    }

    private func joinOrLeaveRoom(chatRoomID: String) {
        if let index = chatSessions.firstIndex(where: { $0.roomID == chatRoomID }) {
            // Cleanup data
            chatSessions.remove(at: index)
            timetokenOfNewestMessageByRoomID.removeValue(forKey: chatRoomID)
            unreadCountsByRoomID.removeValue(forKey: chatRoomID)

            // Clear displayed chat session if it is currently displayed
            if chatViewController.currentChatSession?.roomID == chatRoomID {
                chatViewController.clearChatSession()
            }
            tableView.reloadData()
        } else {
            connectChatRoom(chatRoomID) { result in
                switch result {
                case .success:
                    // Nothing additional to do
                    break
                case let .failure(error):
                    self.showErrorAlert(message: error.localizedDescription)
                }
            }
        }
    }

    /// Connects to the the chat room if a connection doesn't exist then displays it on the ChatViewController
    private func connectToAndDisplayChatRoom(chatRoomID: String) {
        if let chatSession = chatSessions.first(where: { $0.roomID == chatRoomID }) {
            displayChatRoom(chatSession)
        } else {
            connectChatRoom(chatRoomID) { result in
                // Display chat room after connecting
                switch result {
                case let .success(chatSession):
                    self.displayChatRoom(chatSession)
                case let .failure(error):
                    self.showErrorAlert(message: error.localizedDescription)
                }
            }
        }
    }

    /// Displays the chat room on the ChatViewController
    private func displayChatRoom(_ chatSession: ChatSession) {
        // Display chat session on ChatViewController
        chatViewController.setChatSession(chatSession)

        // Updates the timetoken to the latest message in the room
        timetokenOfNewestMessageByRoomID[chatSession.roomID] = chatSession.messages.last?.createdAt ?? TimeToken.now

        // Reset the unread message count
        unreadCountsByRoomID.removeValue(forKey: chatSession.roomID)
        tableView.reloadData()
    }

    /// Performs connection to a chat room
    private func connectChatRoom(_ chatRoomID: String, completion: @escaping (Result<ChatSession, Error>) -> Void) {
        let config = ChatSessionConfig(roomID: chatRoomID)
        sdk.connectChatRoom(config: config, completion: { [weak self] result in
            DispatchQueue.main.async {
                guard let self = self else { return }
                switch result {
                case let .success(chatSession):
                    // Avoid connecting multiple times
                    guard !self.chatSessions.contains(where: { $0.roomID == chatSession.roomID }) else { return }

                    // Subscribe to chat room events
                    chatSession.addDelegate(self)

                    // Updates the timetoken to the latest message in the room
                    self.timetokenOfNewestMessageByRoomID[chatRoomID] = chatSession.messages.last?.createdAt ?? TimeToken.now

                    // Stores chat session to display or leave
                    self.chatSessions.append(chatSession)
                    self.tableView.reloadData()

                    completion(.success(chatSession))
                case let .failure(error):
                    completion(.failure(error))
                }
            }
        })
    }

    /// Incrememnts the unread message count if it is not the displayed chat room
    private func incrementUnreadMessageCount(chatRoomID: String) {
        if let currentRoomID = chatViewController.currentChatSession?.roomID, currentRoomID == chatRoomID {
            unreadCountsByRoomID.removeValue(forKey: chatRoomID)
            tableView.reloadData()
        } else {
            if let unreadCount = unreadCountsByRoomID[chatRoomID] {
                unreadCountsByRoomID[chatRoomID] = unreadCount
            } else {
                unreadCountsByRoomID[chatRoomID] = 0
            }
            tableView.reloadData()
        }
    }

    /// Replaces the unread message date if it is older
    private func updateTimetokenOfNewestMessageIfOlder(roomID: String, date: TimeToken) {
        if roomID == chatViewController.currentChatSession?.roomID {
            // If room of new message is currently displayed then update timetoken of newestMessage to latest
            timetokenOfNewestMessageByRoomID[roomID] = date
        } else if let currentUnreadMessageDate = timetokenOfNewestMessageByRoomID[roomID] {
            if date < currentUnreadMessageDate {
                timetokenOfNewestMessageByRoomID[roomID] = date
            }
        } else {
            timetokenOfNewestMessageByRoomID[roomID] = date
        }
    }

    private func handleNewMessage(roomID: String, newMessage message: ChatMessage) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.updateTimetokenOfNewestMessageIfOlder(roomID: roomID, date: message.createdAt)
            self.incrementUnreadMessageCount(chatRoomID: roomID)
        }
    }

    private func showErrorAlert(message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: "Error",
                message: message,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension UnreadMessageCountSection: ChatSessionDelegate {
    func chatSession(_ chatSession: ChatSession, didPinMessage message: PinMessageInfo) { }

    func chatSession(_ chatSession: ChatSession, didUnpinMessage pinMessageInfoID: String) { }

    func chatSession(_ chatSession: ChatSession, didRecieveNewMessage message: ChatMessage) {
        handleNewMessage(roomID: chatSession.roomID, newMessage: message)
    }

    func chatSession(_ chatSession: ChatSession, didDeleteMessage messageID: ChatMessageID) {
        print("Chat Message Deleted: \(messageID.asString)")
    }

    func chatSession(_ chatSession: ChatSession, didRecieveRoomUpdate chatRoom: ChatRoomInfo) {
        if let filters = chatRoom.contentFilter {
            self.chatViewController.isChatInputVisible = !(filters == ChatFilter.producer)
        }
        print("Received Chat Room Update: \nFilters: \(String(describing: chatRoom.contentFilter)) \nVisibility: \(chatRoom.visibility)")
    }
}

extension UnreadMessageCountSection: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allChatRooms.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "unreadMessageCountCell")!
        let roomID = allChatRooms[indexPath.row]
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = "[\(unreadCountsByRoomID[roomID] ?? 0)] \(allChatRooms[indexPath.row])"
        cell.backgroundColor = chatSessions.contains(where: { $0.roomID == roomID }) ? .systemGreen : .systemRed
        cell.textLabel?.textColor = {
            if let currentRoomID = chatViewController.currentChatSession?.roomID, currentRoomID == roomID {
                return .white
            } else {
                return .black
            }
        }()
        cell.selectionStyle = .none
        return cell
    }
}

extension UnreadMessageCountSection: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chatRoomID = allChatRooms[indexPath.row]
        connectToAndDisplayChatRoom(chatRoomID: chatRoomID)
    }
}
