//
//  SDKInitWithoutAccessTokenSection.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 9/25/20.
//

import EngagementSDK
import UIKit

class SDKInitWithoutAccessTokenSection: TestCaseViewController {
    private var sdk: EngagementSDK!
    private let logViewController: LogViewController = {
        let vc = LogViewController()
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        return vc
    }()

    init(clientID: String) {
        super.init()

        var config = EngagementSDKConfig(clientID: clientID)
        config.accessTokenStorage = self
        sdk = EngagementSDK(config: config)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        addChild(logViewController)
        view.addSubview(logViewController.view)

        NSLayoutConstraint.activate([
            logViewController.view.topAnchor.constraint(equalTo: contentView.topAnchor),
            logViewController.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            logViewController.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            logViewController.view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
}

extension SDKInitWithoutAccessTokenSection: AccessTokenStorage {
    func fetchAccessToken() -> String? {
        return nil
    }

    func storeAccessToken(accessToken: String) {
        // do nothing
    }
}
