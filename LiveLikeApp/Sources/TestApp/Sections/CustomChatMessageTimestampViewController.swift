//
//  CustomChatMessageTimestampViewController.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 1/7/20.
//

import EngagementSDK
import UIKit

/// Tests the use-case of using a custom timestamp format for chat messages
final class CustomChatMessageTimestampViewController: ChatTestCaseViewController {
    override func viewDidLoad() {
        vcTitle = "Custom Chat Message Time"
        super.viewDidLoad()

        chatController.messageTimestampFormatter = { date in
            let timeIntervalDistanceFromNow = Date().timeIntervalSince1970 - date.timeIntervalSince1970
            return "\(Int(ceil(timeIntervalDistanceFromNow / 60))) mins"
        }
    }
}
