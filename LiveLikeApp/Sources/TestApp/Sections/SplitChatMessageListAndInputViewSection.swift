//
//  SplitChatMessageListAndInputViewSection.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 7/20/20.
//

import EngagementSDK
import UIKit

class SplitChatMessageListAndInputViewSection: TestCaseViewController {
    private let contentSession: ContentSession

    private let messageViewController: MessageViewController = {
        let messageVC = MessageViewController()
        messageVC.view.translatesAutoresizingMaskIntoConstraints = false
        return messageVC
    }()

    private let chatInputView: ChatInputView = ChatInputView.instanceFromNib()

    init(contentSession: ContentSession) {
        self.contentSession = contentSession
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        addChild(messageViewController)
        contentView.addSubview(messageViewController.view)
        contentView.addSubview(chatInputView)

        NSLayoutConstraint.activate([
            chatInputView.topAnchor.constraint(equalTo: contentView.topAnchor),
            chatInputView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            chatInputView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            chatInputView.heightAnchor.constraint(equalToConstant: 50),

            messageViewController.view.topAnchor.constraint(equalTo: chatInputView.bottomAnchor),
            messageViewController.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            messageViewController.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            messageViewController.view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])

        messageViewController.setContentSession(contentSession)
        chatInputView.setContentSession(contentSession)
    }
}
