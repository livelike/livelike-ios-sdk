//
//  UserProfileBadgesUseCase.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 7/13/21.
//

import UIKit
import EngagementSDK

class UserProfileBadgesUseCase: TestCaseViewController {

    private var contentSession: ContentSession!
    private var sdk: EngagementSDK
    private var profileBadges: [ProfileBadge] = []

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.allowsSelection = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "myCell")
        return tableView
    }()

    private let loadMoreButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Load More", for: .normal)
        return button
    }()

    private let mainStack: UIStackView = {
        let stack = UIStackView()
        stack.alignment = .fill
        stack.distribution = .fill
        stack.axis = .horizontal
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    private let profileInput: UITextField = {
        let profileInput: UITextField = UITextField()
        profileInput.placeholder = "Enter User Profile ID"
        profileInput.translatesAutoresizingMaskIntoConstraints = false
        profileInput.borderStyle = .line
        profileInput.font = UIFont.systemFont(ofSize: 10.0)
        return profileInput
    }()

    private let getBadgesBTN: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Get Badges", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    init(sdk: EngagementSDK, programID: String) {
        self.sdk = sdk
        self.contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "User Profile Badges"

        super.viewDidLoad()

        mainStack.addArrangedSubview(profileInput)
        mainStack.addArrangedSubview(getBadgesBTN)

        view.addSubview(mainStack)
        view.addSubview(tableView)
        view.addSubview(loadMoreButton)

        NSLayoutConstraint.activate([
            mainStack.topAnchor.constraint(equalTo: contentView.topAnchor),
            mainStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0),
            mainStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            mainStack.heightAnchor.constraint(equalToConstant: 60),

            tableView.topAnchor.constraint(equalTo: mainStack.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: loadMoreButton.topAnchor),

            loadMoreButton.topAnchor.constraint(equalTo: tableView.bottomAnchor),
            loadMoreButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            loadMoreButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            loadMoreButton.heightAnchor.constraint(equalToConstant: 60.0),
            loadMoreButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])

        loadMoreButton.addTarget(self, action: #selector(loadMoreButtonSelected), for: .touchUpInside)
        getBadgesBTN.addTarget(self, action: #selector(getBadges), for: .touchUpInside)
        tableView.delegate = self
        tableView.dataSource = self

        sdk.getCurrentUserProfileID { result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let currentProfileID):
                self.profileInput.text = currentProfileID
            }
        }
    }

    @objc private func getBadges() {

        sdk.badges.getProfileBadgesBy(
            profileID: profileInput.text!,
            page: .first
        ) { result in
            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(let receivedProfileBadges):
                self.profileBadges.removeAll()
                self.profileBadges.append(contentsOf: receivedProfileBadges.items)
                self.tableView.reloadData()
                self.profileInput.endEditing(true)
            }
        }

    }

    @objc private func loadMoreButtonSelected() {
        if let profileID = profileInput.text {
            sdk.badges.getProfileBadgesBy(profileID: profileID, page: .next) { result in
                switch result {
                case .failure(let error):
                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                    self.present(alert, animated: true, completion: nil)
                case .success(let receivedProfileBadges):
                    self.profileBadges.append(contentsOf: receivedProfileBadges.items)
                    self.tableView.reloadData()
                }
            }
        }
    }
}

extension UserProfileBadgesUseCase: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profileBadges.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            var cell = tableView.dequeueReusableCell(withIdentifier: "myCell")
        else {
            return UITableViewCell()
        }

        cell = UITableViewCell(style: .subtitle, reuseIdentifier: "myCell")

        let profileBadge = self.profileBadges[indexPath.row]
        cell.textLabel?.text = "\(indexPath.row) - \(profileBadge.badge.name)"

        if let mediaData = try? Data(contentsOf: profileBadge.badge.badgeIconURL) {
            cell.imageView?.image = UIImage(data: mediaData)
        }

        cell.detailTextLabel?.adjustsFontSizeToFitWidth = true
        cell.detailTextLabel?.text = profileBadge.badge.registeredLinks.first?.linkURL.absoluteString ?? "No Registered Links"

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
