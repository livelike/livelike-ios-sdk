//
//  InvokeActionsAutomations.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 8/28/22.
//

import Foundation
import EngagementSDK
import UIKit

class InvokeActionsSection: TestCaseViewController {
        
    private let sdk: EngagementSDK
        
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        return stackView
    }()
        
    init(sdk: EngagementSDK) {
        self.sdk = sdk
        super.init()
    }
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
            
        stackView.addArrangedSubview({
            let button = UIButton(type: .system)
            button.setTitle("Get Invoked Reward Action", for: .normal)
            button.addTarget(self, action: #selector(getInvokedRewardActions), for: .touchUpInside)
            button.translatesAutoresizingMaskIntoConstraints = false
            return button
        }())
            
        stackView.addArrangedSubview({
            let button = UIButton(type: .system)
            button.setTitle("Get Reward Actions", for: .normal)
            button.addTarget(self, action: #selector(getRewardActions), for: .touchUpInside)
            button.translatesAutoresizingMaskIntoConstraints = false
            return button
        }())
        
        stackView.addArrangedSubview({
            let button = UIButton(type: .system)
            button.setTitle("Get Application Reward Items", for: .normal)
            button.addTarget(self, action: #selector(getApplicationRewardItems), for: .touchUpInside)
            button.translatesAutoresizingMaskIntoConstraints = false
            return button
        }())
            
        contentView.addSubview(stackView)
            
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
            
    }
    
    @objc private func getInvokedRewardActions() {
        let ac = UIAlertController(title: "Options", message: "Set options", preferredStyle: .alert)
        ac.addTextField { textField in
            textField.placeholder = "Program ID"
        }
        ac.addTextField { textField in
            textField.placeholder = "Reward Action Key(s) - comma separated"
        }
        ac.addAction(UIAlertAction(title: "Go", style: .default, handler: { _ in
            let programID = ac.textFields?[0].text
            let rewardActionKeys = ac.textFields?[1].text?.components(separatedBy: ",")
            self.sdk.rewards.getInvokedRewardActions(
                page: .first,
                options: GetInvokedRewardActionsOptions(
                    programID: programID,
                    rewardActionKeys: rewardActionKeys
                )
            ) { result in
                switch result {
                case .failure(let error):
                    let ac = UIAlertController(
                        title: "Error",
                        message: error.localizedDescription,
                        preferredStyle: .alert
                    )
                    ac.addAction(UIAlertAction(title: "OK", style: .cancel))
                    self.present(ac, animated: true, completion: nil)
                case .success(let invokedActions):
                    let ac = UIAlertController(
                        title: "Success",
                        message: invokedActions.map { $0.description }.joined(),
                        preferredStyle: .alert
                    )
                    ac.addAction(UIAlertAction(title: "OK", style: .cancel))
                    self.present(ac, animated: true, completion: nil)
                }
            }
        }))
        self.present(ac, animated: true, completion: nil)
    }
    
    @objc private func getRewardActions() {
        self.sdk.rewards.getRewardActions(
            page: .first
        ) { result in
            switch result {
            case .failure(let error):
                let ac = UIAlertController(
                    title: "Error",
                    message: error.localizedDescription,
                    preferredStyle: .alert
                )
                ac.addAction(UIAlertAction(title: "OK", style: .cancel))
                self.present(ac, animated: true, completion: nil)
            case .success(let rewardActions):
                let ac = UIAlertController(
                    title: "Success",
                    message: rewardActions.map { $0.description }.joined(),
                    preferredStyle: .alert
                )
                ac.addAction(UIAlertAction(title: "OK", style: .cancel))
                self.present(ac, animated: true, completion: nil)
            }
        }
    }
    
    @objc private func getApplicationRewardItems() {
        self.sdk.rewards.getApplicationRewardItems(
            page: .first,
            options: GetApplicationRewardItemsRequestOptions()
        ) { result in
            DispatchQueue.main.async {
                switch result {
                case .failure(let error):
                    let ac = UIAlertController(
                        title: "Error",
                        message: error.localizedDescription,
                        preferredStyle: .alert
                    )
                    ac.addAction(UIAlertAction(title: "OK", style: .cancel))
                    self.present(ac, animated: true, completion: nil)
                case .success(let rewardItems):
                    let ac = UIAlertController(
                        title: "Success",
                        message: rewardItems.map { $0.id }.joined(separator: ","),
                        preferredStyle: .alert
                    )
                    ac.addAction(UIAlertAction(title: "OK", style: .cancel))
                    self.present(ac, animated: true, completion: nil)
                }
            }
        }
    }
    
}

extension InvokedRewardAction {
    var description: String {
        return """
        id: \(id)
        createdAt: \(createdAt)
        programID: \(programID)
        profileID: \(profileID)
        rewardActionKey: \(rewardActionKey)
        code: \(code ?? "")
        rewards: \(rewards.map { $0.description }.joined())"))
        """
    }
}

extension InvokedRewardAction.Reward {
    var description: String {
        return """
        rewardItemID: \(rewardItemID)
        rewardItemName: \(rewardItemName)
        rewardItemAmount: \(rewardItemAmount)
        """
    }
}

extension RewardActionInfo {
    var description: String {
        return """
        id: \(id)
        clientID: \(clientID)
        key: \(key)
        name: \(name)
        description: \(text ?? "")
        """
    }
}
