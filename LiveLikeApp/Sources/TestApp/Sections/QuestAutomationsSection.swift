//
//  QuestAutomationsSection.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 8/17/22.
//

import Foundation
import EngagementSDK
import UIKit

class QuestAutomationsSections: TestCaseViewController {
        
    private let sdk: EngagementSDK
        
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        return stackView
    }()
        
    private let questIDTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Enter Quest ID"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
        
    private let outputTextView: UITextView = {
        let textView = UITextView()
        textView.isEditable = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
        
    init(sdk: EngagementSDK) {
        self.sdk = sdk
        super.init()
    }
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
            
        stackView.addArrangedSubview({
            let button = UIButton(type: .system)
            button.setTitle("Start, Complete, and Claim Rewards", for: .normal)
            button.addTarget(self, action: #selector(startCompleteAndClaimRewards), for: .touchUpInside)
            button.translatesAutoresizingMaskIntoConstraints = false
            return button
        }())
            
        stackView.addArrangedSubview({
            let button = UIButton(type: .system)
            button.setTitle("List Quest Rewards", for: .normal)
            button.addTarget(self, action: #selector(listQuestRewards), for: .touchUpInside)
            button.translatesAutoresizingMaskIntoConstraints = false
            return button
        }())
            
        contentView.addSubview(stackView)
        contentView.addSubview(questIDTextField)
        contentView.addSubview(outputTextView)
            
        NSLayoutConstraint.activate([
            questIDTextField.topAnchor.constraint(equalTo: contentView.topAnchor),
            questIDTextField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            questIDTextField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            questIDTextField.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.1),
            
            stackView.topAnchor.constraint(equalTo: questIDTextField.bottomAnchor),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            stackView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.2),
            
            outputTextView.topAnchor.constraint(equalTo: stackView.bottomAnchor),
            outputTextView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            outputTextView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            outputTextView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.6),
                
        ])
            
    }
        
    @objc private func startCompleteAndClaimRewards() {
        self.outputTextView.text = nil
            
        guard
            let questID = self.questIDTextField.text,
            !questID.isEmpty
        else {
            self.outputError("quest id not input")
            return
        }
            
        self.outputTextView.text += "Starting quest \(questID)..."
            
        sdk.quests.startUserQuest(
            questID: questID
        ) { result in
            switch result {
            case .failure(let error):
                self.outputError(error.localizedDescription)
            case .success(let userQuest):
                
                self.outputTextView.text += "\n\nGettings the available quest rewards..."
                self.sdk.quests.getQuestRewards(
                    page: .first,
                    questID: userQuest.quest.id
                ) { result in
                    switch result {
                    case .failure(let error):
                        self.outputError(error.localizedDescription)
                    case .success(let questRewards):
                        self.outputTextView.text += "\nThis quest has \(questRewards.count) rewards available:"
                        questRewards.forEach {
                            self.outputTextView.text += "\n\($0.rewardItemID) (\($0.rewardItemAmount))"
                        }
                        self.outputTextView.text += "\n\nSuccessfully started quest.\nThis quest has \(userQuest.userQuestTasks.count) tasks.\nCompleting tasks..."
                        self.sdk.quests.updateUserQuestTasks(
                            userQuestID: userQuest.id,
                            userQuestTaskIDs: userQuest.userQuestTasks.map { $0.id },
                            status: .completed
                        ) { result in
                            switch result {
                            case .failure(let error):
                                self.outputError(error.localizedDescription)
                            case .success(let userQuest):
                                self.outputTextView.text += "\n\nAll tasks completed!\nClaiming rewards..."
                                self.sdk.quests.claimUserQuestRewards(
                                    userQuestID: userQuest.id
                                ) { result in
                                    switch result {
                                    case .failure(let error):
                                        self.outputError(error.localizedDescription)
                                    case .success:
                                        self.outputTextView.text += "\n\nQuest rewards have been claimed"
                                    }
                                        
                                }
                            }
                        }
                    }
                }
            }
        }
    }
        
    @objc private func listQuestRewards() {
        self.outputTextView.text = nil
            
        guard
            let questID = self.questIDTextField.text,
            !questID.isEmpty
        else {
            self.outputError("quest id not input")
            return
        }
            
        self.outputTextView.text += "Getting first page of quest rewards..."
        sdk.quests.getQuestRewards(
            page: .first,
            questID: questID
        ) { result in
            switch result {
            case .failure(let error):
                self.outputError(error.localizedDescription)
            case .success(let questRewards):
                self.outputTextView.text += "\n\nQuest has rewards:"
                questRewards.forEach {
                    self.outputTextView.text += "\n\($0.rewardItemID) (\($0.rewardItemAmount))"
                }
                self.outputTextView.text += "\n\nGetting next page of quest rewards..."
            }
            
            self.sdk.quests.getQuestRewards(
                page: .next,
                questID: questID
            ) { result in
                switch result {
                case .failure(let error):
                    self.outputError(error.localizedDescription)
                case .success(let questRewards):
                    self.outputTextView.text += "\n\nQuest has rewards: \(questRewards.map { "\n\($0.rewardItemID)|\($0.rewardItemAmount)" })"
                    self.outputTextView.text += "\n\nGetting first page of quest rewards..."
                    
                    self.sdk.quests.getQuestRewards(
                        page: .first,
                        questID: questID
                    ) { result in
                        switch result {
                        case .failure(let error):
                            self.outputError(error.localizedDescription)
                        case .success(let questRewards):
                            self.outputTextView.text += "\n\nQuest has rewards: \(questRewards.map { "\n\($0.rewardItemID)|\($0.rewardItemAmount)" })"
                        }
                    }
                }
            }
        }
    }
        
    private func outputError(_ errorMessage: String) {
        self.outputTextView.text += "\n\nFailed with error \(errorMessage)"
    }

}
