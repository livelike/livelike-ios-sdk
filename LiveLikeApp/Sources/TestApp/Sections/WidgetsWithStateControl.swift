//
//  WidgetsWithStateControl.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 4/15/20.
//

import EngagementSDK
import UIKit

class WidgetWithStateControlVC: TestCaseViewController {
    private var sdk: EngagementSDK!
    private var session: ContentSession!

    private let clientID: String
    private let programID: String
    private let apiToken: String
    private let apiOrigin: URL

    private var currentWidget: Widget?

    // MARK: - UI

    private let widgetContainer: UIView = {
        let widgetContainer = UIView()
        widgetContainer.translatesAutoresizingMaskIntoConstraints = false
        return widgetContainer
    }()

    private lazy var widgetCreateView: WidgetCreateView = {
        let widgetCreateView = WidgetCreateView(
            programID: self.programID,
            apiToken: self.apiToken,
            timeoutSeconds: 5,
            apiOrigin: self.apiOrigin
        )
        widgetCreateView.translatesAutoresizingMaskIntoConstraints = false
        return widgetCreateView
    }()

    private let currentStateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        return label
    }()

    private lazy var moveToNextStateButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Move To Next State", for: .normal)
        button.addTarget(self, action: #selector(moveToNextStateButtonSelected), for: .touchUpInside)
        return button
    }()

    init(clientID: String, programID: String, apiToken: String, apiOrigin: URL) {
        self.clientID = clientID
        self.programID = programID
        self.apiToken = apiToken
        self.apiOrigin = apiOrigin
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        contentView.addSubview(widgetContainer)
        contentView.addSubview(widgetCreateView)
        contentView.addSubview(currentStateLabel)
        contentView.addSubview(moveToNextStateButton)

        NSLayoutConstraint.activate([
            widgetContainer.topAnchor.constraint(equalTo: contentView.topAnchor),
            widgetContainer.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.4),
            widgetContainer.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetContainer.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            currentStateLabel.topAnchor.constraint(equalTo: widgetContainer.bottomAnchor),
            currentStateLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            currentStateLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            currentStateLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.05),

            moveToNextStateButton.topAnchor.constraint(equalTo: currentStateLabel.bottomAnchor),
            moveToNextStateButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            moveToNextStateButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            moveToNextStateButton.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.05),

            widgetCreateView.topAnchor.constraint(equalTo: moveToNextStateButton.bottomAnchor),
            widgetCreateView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetCreateView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            widgetCreateView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])

        var config = EngagementSDKConfig(clientID: clientID)
        config.apiOrigin = apiOrigin
        sdk = EngagementSDK(config: config)
        session = sdk.contentSession(config: SessionConfiguration(programID: programID))
        session.delegate = self
    }

    @objc private func moveToNextStateButtonSelected() {
        currentWidget?.moveToNextState()
        if let state = currentWidget?.currentState {
            updateCurrentStateLabel(state)
        }
    }

    private func updateCurrentStateLabel(_ state: WidgetState) {
        currentStateLabel.text = "Current State: \(String(describing: state))"
    }

    private func clearCurrentWidget() {
        currentWidget?.view.removeFromSuperview()
        currentWidget?.removeFromParent()
        currentWidget = nil
    }
}

extension WidgetWithStateControlVC: ContentSessionDelegate {
    func widget(_ session: ContentSession, didBecomeReady widget: Widget) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.clearCurrentWidget()
            self.updateCurrentStateLabel(widget.currentState)
            self.currentWidget = widget
            self.addChild(widget)
            widget.view.translatesAutoresizingMaskIntoConstraints = false
            self.widgetContainer.addSubview(widget.view)
            widget.didMove(toParent: self)

            NSLayoutConstraint.activate([
                widget.view.topAnchor.constraint(equalTo: self.widgetContainer.topAnchor),
                widget.view.leadingAnchor.constraint(equalTo: self.widgetContainer.leadingAnchor),
                widget.view.trailingAnchor.constraint(equalTo: self.widgetContainer.trailingAnchor),
                widget.view.heightAnchor.constraint(lessThanOrEqualTo: self.widgetContainer.heightAnchor)
            ])
        }
    }

    func playheadTimeSource(_ session: ContentSession) -> Date? {
        return nil
    }

    func session(_ session: ContentSession, didChangeStatus status: SessionStatus) {}

    func session(_ session: ContentSession, didReceiveError error: Error) {}

    func chat(session: ContentSession, roomID: String, newMessage message: ChatMessage) {}

    func widget(_ session: ContentSession, didBecomeReady jsonObject: Any) {}

    func contentSession(_ session: ContentSession, didReceiveWidget widget: WidgetModel) {}
}
