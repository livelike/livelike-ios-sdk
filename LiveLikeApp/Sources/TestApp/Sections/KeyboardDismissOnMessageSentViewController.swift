//
//  KeyboardDismissOnMessageSentViewController.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 12/11/19.
//

import EngagementSDK
import UIKit

/// Tests the use-case of automatically dismissing they keyboard when the user sends a message
final class KeyboardDismissOnMessageSentViewController: ChatTestCaseViewController {
    override func viewDidLoad() {
        vcTitle = "Dismiss Keyboard After Message Sent"
        super.viewDidLoad()
        chatController.didSendMessage = { [weak self] _ in
            self?.chatController.dismissKeyboard()
        }
    }
}
