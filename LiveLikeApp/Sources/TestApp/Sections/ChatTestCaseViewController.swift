//
//  ChatTestCaseViewController.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 2/25/20.
//

import EngagementSDK
import UIKit

class ChatTestCaseViewController: TestCaseViewController {
    let chatController = ChatViewController()
    var session: ContentSession?

    let sdk: EngagementSDK
    let programID: String
    private let theme: Theme?

    init(sdk: EngagementSDK, programID: String, theme: Theme) {
        self.sdk = sdk
        self.programID = programID
        self.theme = theme
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        addChild(viewController: chatController, into: contentView)

        let config = SessionConfiguration(programID: programID)
        config.chatShouldDisplayAvatar = true
        config.enableChatMessageURLs = true
        session = sdk.contentSession(config: config)

        chatController.session = session
        if let theme = theme {
            chatController.setTheme(theme)
        }

        chatController.didSendMessage = { message in
            log.dev("User did send message: \(message.id)")
        }

        super.viewDidLoad()

        session?.getChatSession(completion: { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success(chatroom):
                self.sdk.getChatUserMutedStatus(roomID: chatroom.roomID) { result in
                    switch result {
                    case let .success(mutedStatus):
                        if mutedStatus.isMuted {
                            let alert = UIAlertController(title: "Hey!", message: "No offence, but you're muted!", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                            self.present(alert, animated: true, completion: nil)
                        }
                    case let .failure(error):
                        log.dev("error getting muted status \(error.localizedDescription)")
                    }
                }
            case let .failure(error):
                log.dev("error getting muted status \(error.localizedDescription)")
            }
        })
    }
}
