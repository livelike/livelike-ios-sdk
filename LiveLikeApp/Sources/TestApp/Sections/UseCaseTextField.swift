//
//  UseCaseTextField.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 3/10/23.
//

import UIKit

class UseCaseTextField: UITextField {
    init(placeholder: String) {
        super.init(frame: .zero)

        self.translatesAutoresizingMaskIntoConstraints = false
        self.textColor = .black
        self.placeholder = placeholder
        self.borderStyle = .roundedRect
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
