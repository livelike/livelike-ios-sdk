//
//  ProfileRelationshipTypeUseCase.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 2/28/23.
//

import Foundation
import EngagementSDK
import UIKit

class ProfileRelationshipTypeUseCase: TestCaseViewController {
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 30
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let buttonStack1: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()
    
    private let getRelationshipTypesButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get Profile Relationship Types", for: .normal)
        return button
    }()
    
    private let sdk: EngagementSDK
    private var commentClient: CommentClient?
    
    private var profileRelationshipTypes = [ProfileRelationshipType]()

    init(sdk: EngagementSDK) {
        self.sdk = sdk
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vcTitle = "Comments Use Case"
        
        tableView.delegate = self
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        
        self.getRelationshipTypesButton.addTarget(self, action: #selector(self.getRelationshipTypesList), for: .touchUpInside)
        
        contentView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            stackView.bottomAnchor.constraint(equalTo: contentView.safeBottomAnchor, constant: -20),
        ])
        
        buttonStack1.addArrangedSubview(getRelationshipTypesButton)
        
        stackView.addArrangedSubview(buttonStack1)
        stackView.addArrangedSubview(tableView)
    }
    
    @objc private func getRelationshipTypesList() {
        
        self.sdk.socialGraphClient.getProfileRelationshipTypes(
            page: .first,
            options: GetProfileRelationshipTypesOptions()
        ) { result in
            switch result {
            case .success(let relationships):
                log.dev("found: \(relationships.count) relationships")
                DispatchQueue.main.async {
                    self.profileRelationshipTypes.removeAll()
                    self.profileRelationshipTypes = relationships
                    self.tableView.reloadData()
                }
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    func showAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: title,
                message: message,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension ProfileRelationshipTypeUseCase: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profileRelationshipTypes.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        var cellText: String = ""
        let rowData = profileRelationshipTypes[indexPath.row]
        cellText = "ID: \(rowData.id)\nName: \(rowData.name) \nKey: \(rowData.key)\nClient ID: \(rowData.clientID)"

        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = cellText
        cell.textLabel?.font = UIFont.systemFont(ofSize: 12.0)
        return cell
    }
}

extension ProfileRelationshipTypeUseCase: UITableViewDelegate {
}

extension ProfileRelationshipTypeUseCase: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
