//
//  CreateChatRoomVC.swift
//  LiveLikeDemoApp
//
//  Created by Mike Moloksher on 5/15/20.
//

import EngagementSDK
import UIKit

class CreateChatRoomVC: TestCaseViewController {
    private let sdk: EngagementSDK
    private let envBaseURL: URL
    private var chatSession: ChatSession? {
        didSet {
            if chatSession != nil {
                sdk.getChatRoomInfo(roomID: connectRoomField.text!) { [weak self] result in
                    guard let self = self else { return }
                    switch result {
                    case let .success(chatInfo):
                        self.updateChatRoomInfo(chatRoomInfo: chatInfo)
                    case let .failure(error):
                        log.dev("Error: \(error.localizedDescription)")
                    }
                }
            }
        }
    }

    private let chatViewController = ChatViewController()
    private let createRoomButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Create Chat Room", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private let connectRoomButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .groupTableViewBackground
        button.setTitle("Connect Room", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private let connectRoomField: UITextField = {
        let roomTitle: UITextField = UITextField()
        roomTitle.placeholder = "Enter Chat Room ID"
        roomTitle.translatesAutoresizingMaskIntoConstraints = false
        roomTitle.borderStyle = .line
        roomTitle.font = UIFont.systemFont(ofSize: 10.0)
        return roomTitle
    }()

    private let roomTitle: UITextField = {
        let roomTitle: UITextField = UITextField()
        roomTitle.placeholder = "Chat Room Title"
        roomTitle.translatesAutoresizingMaskIntoConstraints = false
        roomTitle.borderStyle = .line
        roomTitle.font = UIFont.systemFont(ofSize: 12.0)
        return roomTitle
    }()

    private let visiblityPicker: UIPickerView = {
        let picker = UIPickerView()
        picker.translatesAutoresizingMaskIntoConstraints = false
        return picker
    }()

    private let chatroomInfoStack: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 0
        return stackView
    }()

    init(envBaseURL: URL, sdk: EngagementSDK) {
        self.envBaseURL = envBaseURL
        self.sdk = sdk
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        visiblityPicker.delegate = self
        visiblityPicker.dataSource = self

        chatViewController.view.translatesAutoresizingMaskIntoConstraints = false

        addChild(chatViewController)

        contentView.addSubview(chatViewController.view)
        contentView.addSubview(createRoomButton)
        contentView.addSubview(roomTitle)
        contentView.addSubview(connectRoomButton)
        contentView.addSubview(connectRoomField)
        contentView.addSubview(visiblityPicker)
        contentView.addSubview(chatroomInfoStack)

        NSLayoutConstraint.activate([
            chatViewController.view.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 150.0),
            chatViewController.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            chatViewController.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            chatViewController.view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

            createRoomButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20.0),
            createRoomButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20.0),
            createRoomButton.heightAnchor.constraint(equalToConstant: 60.0),
            createRoomButton.widthAnchor.constraint(equalToConstant: 150.0),
            roomTitle.leadingAnchor.constraint(equalTo: createRoomButton.trailingAnchor, constant: 10.0),
            roomTitle.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            roomTitle.topAnchor.constraint(equalTo: createRoomButton.topAnchor),

            visiblityPicker.leadingAnchor.constraint(equalTo: createRoomButton.trailingAnchor, constant: 10.0),
            visiblityPicker.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            visiblityPicker.topAnchor.constraint(equalTo: roomTitle.bottomAnchor, constant: 0.0),
            visiblityPicker.heightAnchor.constraint(equalToConstant: 40.0),

            connectRoomButton.topAnchor.constraint(equalTo: createRoomButton.bottomAnchor, constant: 5.0),
            connectRoomButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20.0),
            connectRoomButton.heightAnchor.constraint(equalToConstant: 50.0),
            connectRoomButton.widthAnchor.constraint(equalToConstant: 100.0),
            connectRoomField.leadingAnchor.constraint(equalTo: connectRoomButton.trailingAnchor, constant: 10.0),
            connectRoomField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            connectRoomField.centerYAnchor.constraint(equalTo: connectRoomButton.centerYAnchor),

            chatroomInfoStack.topAnchor.constraint(equalTo: chatViewController.view.topAnchor, constant: 10.0),
            chatroomInfoStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0)
        ])

        createRoomButton.addTarget(self, action: #selector(createChatRoom), for: .touchUpInside)
        connectRoomButton.addTarget(self, action: #selector(connectRoom), for: .touchUpInside)

        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc private func applicationWillEnterForeground() {}

    @objc private func createChatRoom() {
        let selectedRow = visiblityPicker.selectedRow(inComponent: 0)
        let visibility: ChatRoomVisibilty = ChatRoomVisibilty.allCases[selectedRow]
        sdk.createChatRoom(
            title: roomTitle.text!.count > 0 ? roomTitle.text : nil,
            visibility: visibility
        ) { result in
            switch result {
            case let .success(chatRoomID):
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    let alert = UIAlertController(
                        title: "Room Created",
                        message: "ID: \(chatRoomID)",
                        preferredStyle: .alert
                    )

                    alert.addAction(UIAlertAction(title: "Connect Created Room", style: .default, handler: { _ in
                        self.connectRoomField.text = chatRoomID
                        self.connectRoom()
                    }))
                    alert.addAction(UIAlertAction(title: "Test Get Info API", style: .default, handler: { _ in
                        self.connectRoomField.text = chatRoomID
                        self.getInfoOnChatRoom()
                    }))
                    alert.addAction(UIAlertAction(title: "Copy Id To Clipboard", style: .default, handler: { _ in
                        UIPasteboard.general.string = chatRoomID
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            case let .failure(error):
                log.dev("Error: \(error.localizedDescription)")
            }
        }
    }

    private func getInfoOnChatRoom() {
        sdk.getChatRoomInfo(roomID: connectRoomField.text!) { result in
            switch result {
            case let .success(chatInfo):
                self.showAlert(title: "Chat Room Info", msg: "title: \(chatInfo.title ?? "No Title") \nid:\(chatInfo.id) \nVisibility: \(chatInfo.visibility)")
            case let .failure(error):
                log.dev("Error: \(error.localizedDescription)")
            }
        }
    }

    @objc private func connectRoom() {
        guard let chatRoomID = connectRoomField.text,
              chatRoomID.count > 0
        else {
            showAlert(title: "Enter Room ID", msg: nil)
            return
        }
        let config = ChatSessionConfig(roomID: chatRoomID)
        sdk.connectChatRoom(config: config, completion: { [weak self] result in
            DispatchQueue.main.async {
                guard let self = self else { return }
                switch result {
                case let .success(chatSession):
                    self.chatSession = chatSession
                    self.chatViewController.setChatSession(chatSession)
                    self.chatSession?.addDelegate(self)
                    self.showAlert(title: "Connected Room ID", msg: chatSession.roomID)
                case let .failure(error):
                    self.showAlert(title: "Error", msg: error.localizedDescription)
                }
            }
        })
    }

    private func showAlert(title: String, msg: String?) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let alert = UIAlertController(
                title: title,
                message: msg ?? "",
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    private func updateChatRoomInfo(chatRoomInfo: ChatRoomInfo) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }

            self.chatroomInfoStack.arrangedSubviews.forEach {
                self.chatroomInfoStack.removeArrangedSubview($0)
                $0.removeFromSuperview()
            }

            self.chatroomInfoStack.addArrangedSubview(ChatInfoLabel("id: \(chatRoomInfo.id)"))
            self.chatroomInfoStack.addArrangedSubview(ChatInfoLabel("title: \(chatRoomInfo.title ?? "No Title")"))
            self.chatroomInfoStack.addArrangedSubview(ChatInfoLabel("visibility: \(chatRoomInfo.visibility.rawValue)"))
            chatRoomInfo.tokenGates.forEach { tokenGate in
                self.chatroomInfoStack.addArrangedSubview(ChatInfoLabel("gate: \(tokenGate.contractAddress)"))
            }
        }
    }
}

private class ChatInfoLabel: UILabel {
    init(_ label: String) {
        super.init(frame: CGRect.zero)
        self.textColor = .white
        self.font = UIFont.systemFont(ofSize: 8.0)
        self.text = label
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CreateChatRoomVC: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return ChatRoomVisibilty.allCases.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "Visibility: \(ChatRoomVisibilty.allCases[row].rawValue)"
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var title = UILabel()
        if let view = view as? UILabel {
            title = view
        }
        title.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold)
        title.textColor = UIColor.blue
        title.text = "Visibility: \(ChatRoomVisibilty.allCases[row].rawValue)"
        title.textAlignment = .center

        return title
    }
}

extension CreateChatRoomVC: ChatSessionDelegate {
    func chatSession(_ chatSession: ChatSession, didDeleteMessage messageID: ChatMessageID) {
        print(messageID)
    }

    func chatSession(_ chatSession: ChatSession, didRecieveNewMessage message: ChatMessage) {

    }

    func chatSession(_ chatSession: ChatSession, didRecieveRoomUpdate chatRoom: ChatRoomInfo) {
        updateChatRoomInfo(chatRoomInfo: chatRoom)
    }

    func chatSession(_ chatSession: ChatSession, didPinMessage message: PinMessageInfo) {
    }

    func chatSession(_ chatSession: ChatSession, didUnpinMessage pinMessageInfoID: String) {
    }
}
