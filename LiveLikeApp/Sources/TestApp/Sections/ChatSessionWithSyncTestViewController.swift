//
//  ChatSessionWithSyncTestViewController.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 3/6/20.
//

import AVKit
import EngagementSDK
import UIKit

class ChatSessionWithSyncTestViewController: TestCaseViewController {
    private let sdk: EngagementSDK
    private let envBaseURL: URL
    private var chatSession: ChatSession?
    private let chatViewController = ChatViewController()
    private let avPlayer: AVPlayerViewController = .init()
    private let changeButton = UIButton(type: .system)

    init(envBaseURL: URL, sdk: EngagementSDK) {
        self.envBaseURL = envBaseURL
        self.sdk = sdk
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        chatViewController.view.translatesAutoresizingMaskIntoConstraints = false
        avPlayer.view.translatesAutoresizingMaskIntoConstraints = false
        changeButton.translatesAutoresizingMaskIntoConstraints = false

        addChild(chatViewController)
        addChild(avPlayer)

        contentView.addSubview(chatViewController.view)
        contentView.addSubview(avPlayer.view)
        contentView.addSubview(changeButton)

        NSLayoutConstraint.activate([
            avPlayer.view.topAnchor.constraint(equalTo: contentView.topAnchor),
            avPlayer.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            avPlayer.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            avPlayer.view.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.3),

            chatViewController.view.topAnchor.constraint(equalTo: avPlayer.view.bottomAnchor),
            chatViewController.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            chatViewController.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            chatViewController.view.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.6),

            changeButton.topAnchor.constraint(equalTo: chatViewController.view.bottomAnchor),
            changeButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            changeButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            changeButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)

        ])

        changeButton.setTitle("Set Room", for: .normal)
        changeButton.addTarget(self, action: #selector(changeButtonSelected), for: .touchUpInside)

        // Play stream
        avPlayer.player = AVPlayer(url: URL(string: "https://cf-streams.livelikecdn.com/live/colorbars-angle1/index.m3u8")!)
        avPlayer.player?.play()

        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc private func applicationWillEnterForeground() {
        avPlayer.player?.play()
    }

    @objc private func changeButtonSelected() {
        LiveLikeAPI.getChatRoomsPage(baseURL: envBaseURL) { result in
            self.handleChatRoomsPageResult(result)
        }
    }

    private func handleChatRoomsPageResult(_ result: Result<ChatRoomResourcePage, Error>) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let alert = UIAlertController(
                title: "Select Chat Room",
                message: "Select a chat room to enter.",
                preferredStyle: .actionSheet
            )

            GroupChatManager.shared.chatRoomIds.forEach { chatRoomId in
                alert.addAction(
                    UIAlertAction(
                        title: chatRoomId,
                        style: .default,
                        handler: { _ in self.handleChatRoomSelected(chatRoomId) }
                    )
                )
            }

            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    private func handleChatRoomSelected(_ chatRoomId: String) {
        var config = ChatSessionConfig(roomID: chatRoomId)
        config.syncTimeSource = { [weak self] in
            self?.avPlayer.player?.programDateAndTime?.timeIntervalSince1970
        }
        sdk.connectChatRoom(config: config, completion: { [weak self] result in
            DispatchQueue.main.async {
                guard let self = self else { return }
                switch result {
                case let .success(chatSession):
                    self.chatSession = chatSession
                    self.chatViewController.setChatSession(chatSession)
                case let .failure(error):
                    print(error)
                }
            }
        })
    }
}
