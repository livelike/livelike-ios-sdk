//
//  ReactionPackUseCase.swift
//  LiveLikeTestApp
//
//  Created by Keval Shah on 21/07/22.
//

import Foundation
import EngagementSDK
import UIKit

class ReactionPackUseCase: TestCaseViewController {
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 30
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textColor = .black
        return label
    }()
    
    private let reactionPackIDTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Reaction Pack ID"
        return textField
    }()
    
    private let targetIDTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Target ID"
        return textField
    }()
    
    private let reactionSpaceIDTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Reaction Space ID"
        return textField
    }()
    
    private let userReactionIDTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter User Reaction ID"
        return textField
    }()
    
    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()
    
    private let createUserReactionButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Add User Reaction", for: .normal)
        return button
    }()
    
    private let getReactionCountButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get Reaction Count", for: .normal)
        return button
    }()
    
    private let deleteUserReactionButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Remove User Reaction", for: .normal)
        return button
    }()
    
    private let getListOfReactionPacksButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get List of Reaction Packs", for: .normal)
        return button
    }()
    
    private let getReactionPackDetailButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get Details of Reaction Pack", for: .normal)
        return button
    }()
    
    private let createReactionSessionButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Create Reaction Session", for: .normal)
        return button
    }()

    private var reactionSession: ReactionSession?
    private let accessToken: String
    private let sdk: EngagementSDK
    
    private var reactionPacks = [ReactionPack]()
    private var reactionCounts = [UserReactionCount]()

    init(sdk: EngagementSDK, accessToken: String) {
        self.sdk = sdk
        self.accessToken = accessToken
        self.reactionSession = nil
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vcTitle = "Add User to Chat Room"
        vcInfo = "Add another user to chatroom using member id and chatroom id"
        
        tableView.delegate = self
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        
        self.createUserReactionButton.addTarget(self, action: #selector(self.createReaction), for: .touchUpInside)
        self.deleteUserReactionButton.addTarget(self, action: #selector(self.deleteReaction), for: .touchUpInside)
        self.getListOfReactionPacksButton.addTarget(self, action: #selector(self.getReactionPackList), for: .touchUpInside)
        self.getReactionPackDetailButton.addTarget(self, action: #selector(self.getReactionPackDetail), for: .touchUpInside)
        self.getReactionCountButton.addTarget(self, action: #selector(self.getReactionCount), for: .touchUpInside)
        self.createReactionSessionButton.addTarget(self, action: #selector(self.createSession), for: .touchUpInside)
        
        targetIDTextField.delegate = self
        reactionSpaceIDTextField.delegate = self
        reactionPackIDTextField.delegate = self
        userReactionIDTextField.delegate = self
        
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .dark:
                label.textColor = .white
                reactionSpaceIDTextField.textColor = .white
                targetIDTextField.textColor = .white
                userReactionIDTextField.textColor = .white
                reactionPackIDTextField.textColor = .white
            case .light:
                label.textColor = .black
                reactionSpaceIDTextField.textColor = .black
                targetIDTextField.textColor = .black
                userReactionIDTextField.textColor = .black
                reactionPackIDTextField.textColor = .black
            case .unspecified:
                label.textColor = .black
                reactionSpaceIDTextField.textColor = .black
                targetIDTextField.textColor = .black
                userReactionIDTextField.textColor = .black
                reactionPackIDTextField.textColor = .black
            @unknown default:
                label.textColor = .black
                reactionSpaceIDTextField.textColor = .black
                targetIDTextField.textColor = .black
                userReactionIDTextField.textColor = .black
                reactionPackIDTextField.textColor = .black
            }
        }
        
        contentView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 20),
        ])
        
        stackView.addArrangedSubview(label)
        stackView.addArrangedSubview(reactionPackIDTextField)
        stackView.addArrangedSubview(reactionSpaceIDTextField)
        stackView.addArrangedSubview(targetIDTextField)
        stackView.addArrangedSubview(userReactionIDTextField)
        stackView.addArrangedSubview(createReactionSessionButton)
        stackView.addArrangedSubview(createUserReactionButton)
        stackView.addArrangedSubview(deleteUserReactionButton)
        stackView.addArrangedSubview(getReactionPackDetailButton)
        stackView.addArrangedSubview(getListOfReactionPacksButton)
        stackView.addArrangedSubview(getReactionCountButton)
        stackView.addArrangedSubview(tableView)
    }
    
    @objc private func createSession() {
        guard let reactionSpaceID = reactionSpaceIDTextField.text else {
            self.showAlert(title: "Reaction Space ID Missing", message: "Please Enter ID")
            return
        }
        
        sdk.reaction.getReactionSpaceInfo(reactionSpaceID: reactionSpaceID) { result in
            switch result {
            case .success(let space):
                self.reactionSession = self.sdk.reaction.createReactionSession(reactionSpace: space)
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func createReaction() {
        guard let targetID = targetIDTextField.text else {
            self.showAlert(title: "Target ID Missing", message: "Please Enter ID")
            return
        }
        
        guard let reactionID = userReactionIDTextField.text else {
            self.showAlert(title: "Reaction ID Missing", message: "Please Enter ID")
            return
        }
        
        guard let reactionSession = self.reactionSession else {
            self.showAlert(title: "Reaction Session Missing", message: "Please Create Session")
            return
        }
        
        reactionSession.addUserReaction(targetID: targetID, reactionID: reactionID, customData: nil) { result in
            switch result {
            case .success(let userReaction):
                self.showAlert(title: "User Reaction Added", message: "Reaction: \(userReaction.reactionID) Target: \(userReaction.targetID)")
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func deleteReaction() {
        guard let reactionID = userReactionIDTextField.text else {
            self.showAlert(title: "Reaction ID Missing", message: "Please Enter ID")
            return
        }
        
        guard let reactionSession = self.reactionSession else {
            self.showAlert(title: "Reaction Session Missing", message: "Please Create Session")
            return
        }
        
        reactionSession.removeUserReaction(userReactionID: reactionID) { result in
            switch result {
            case .success:
                self.showAlert(title: "Completed", message: "Deleted Reaction with ID: \(reactionID)")
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func getReactionCount() {
        guard let spaceID = reactionSpaceIDTextField.text else {
            self.showAlert(title: "Reaction Space ID Missing", message: "Please Enter ID")
            return
        }
        
        guard let targetID = targetIDTextField.text else {
            self.showAlert(title: "Target ID Missing", message: "Please Enter ID")
            return
        }
        
        guard let reactionSession = self.reactionSession else {
            self.showAlert(title: "Reaction Session Missing", message: "Please Create Session")
            return
        }
        
        reactionSession.getUserReactionsCount(reactionSpaceID: spaceID, targetID: [targetID], page: .first) { result in
            switch result {
            case .success(let reactionCount):
                self.showAlert(title: "Emoji Count", message: String(describing: reactionCount.first?.reactions.first?.count))
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func getReactionPackDetail() {
        guard let packID = reactionPackIDTextField.text else {
            self.showAlert(title: "Reaction Space ID Missing", message: "Please Enter ID")
            return
        }
        
        sdk.reaction.getReactionPackInfo(reactionPackID: packID) { result in
            switch result {
            case .success(let reactionPack):
                self.reactionPacks = [reactionPack]
                self.tableView.reloadData()
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func getReactionPackList() {
        sdk.reaction.getReactionPacks(page: .first) { result in
            switch result {
            case .success(let reactionPacks):
                self.reactionPacks = reactionPacks
                self.tableView.reloadData()
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    private func showAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: title,
                message: message,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension ReactionPackUseCase: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reactionPacks.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        var cellText: String = ""
        let rowData = reactionPacks[indexPath.row]
        cellText = "Reaction Pack Emojis:\(String(describing: rowData.emojis.count))"

        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = cellText
        return cell
    }
}

extension ReactionPackUseCase: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowData = reactionPacks[indexPath.row]
        UIPasteboard.general.string = rowData.id
        self.showAlert(title: "Reaction Pack Id Copied", message: rowData.id)
    }
}

extension ReactionPackUseCase: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
