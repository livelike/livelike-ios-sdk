//
//  ReactionSpaceUseCase.swift
//  LiveLikeTestApp
//
//  Created by Keval Shah on 20/07/22.
//

import Foundation
import EngagementSDK
import UIKit

class ReactionSpaceUseCase: TestCaseViewController {
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 30
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textColor = .black
        return label
    }()
    
    private let reactionSpaceNameTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Reaction Space Name"
        return textField
    }()
    
    private let targetGroupIDTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Target Group ID"
        return textField
    }()
    
    private let reactionPackIDsTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Reaction Pack IDs"
        return textField
    }()
    
    private let reactionSpaceIDTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Enter Reaction Space ID"
        return textField
    }()
    
    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()
    
    private let createReactionSpaceButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Create Reaction Space", for: .normal)
        return button
    }()
    
    private let updateReactionSpaceButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Update Reaction Space", for: .normal)
        return button
    }()
    
    private let deleteReactionSpaceButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Delete Reaction Space", for: .normal)
        return button
    }()
    
    private let getListOfReactionSpaceButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get List of Reaction Spaces", for: .normal)
        return button
    }()
    
    private let getReactionSpaceDetailButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get Details of Reaction Space", for: .normal)
        return button
    }()
    
    private let showMoreButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Show More Reaction Spaces", for: .normal)
        return button
    }()

    private let reactionSession: ReactionSession?
    private let accessToken: String
    private let sdk: EngagementSDK
    
    private var reactionSpaces = [ReactionSpace]()

    init(sdk: EngagementSDK, accessToken: String) {
        self.sdk = sdk
        self.accessToken = accessToken
        self.reactionSession = nil
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vcTitle = "Add User to Chat Room"
        vcInfo = "Add another user to chatroom using member id and chatroom id"
        
        tableView.delegate = self
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        
        self.createReactionSpaceButton.addTarget(self, action: #selector(self.createReactionSpace), for: .touchUpInside)
        self.deleteReactionSpaceButton.addTarget(self, action: #selector(self.deleteReactionSpace), for: .touchUpInside)
        self.updateReactionSpaceButton.addTarget(self, action: #selector(self.updateReactionSpace), for: .touchUpInside)
        self.getReactionSpaceDetailButton.addTarget(self, action: #selector(self.getReactionSpaceDetail), for: .touchUpInside)
        self.getListOfReactionSpaceButton.addTarget(self, action: #selector(self.getReactionSpaceList), for: .touchUpInside)
        self.showMoreButton.addTarget(self, action: #selector(self.showMoreSpaces), for: .touchUpInside)
        
        reactionSpaceIDTextField.delegate = self
        targetGroupIDTextField.delegate = self
        reactionSpaceNameTextField.delegate = self
        reactionPackIDsTextField.delegate = self
        
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .dark:
                label.textColor = .white
                reactionSpaceNameTextField.textColor = .white
                targetGroupIDTextField.textColor = .white
                reactionPackIDsTextField.textColor = .white
                reactionSpaceIDTextField.textColor = .white
            case .light:
                label.textColor = .black
                reactionSpaceNameTextField.textColor = .black
                targetGroupIDTextField.textColor = .black
                reactionPackIDsTextField.textColor = .black
                reactionSpaceIDTextField.textColor = .black
            case .unspecified:
                label.textColor = .black
                reactionSpaceNameTextField.textColor = .black
                targetGroupIDTextField.textColor = .black
                reactionPackIDsTextField.textColor = .black
                reactionSpaceIDTextField.textColor = .black
            @unknown default:
                label.textColor = .black
                reactionSpaceNameTextField.textColor = .black
                targetGroupIDTextField.textColor = .black
                reactionPackIDsTextField.textColor = .black
                reactionSpaceIDTextField.textColor = .black
            }
        }
        
        contentView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 20),
        ])
        
        stackView.addArrangedSubview(label)
        stackView.addArrangedSubview(reactionSpaceNameTextField)
        stackView.addArrangedSubview(targetGroupIDTextField)
        stackView.addArrangedSubview(reactionPackIDsTextField)
        stackView.addArrangedSubview(reactionSpaceIDTextField)
        stackView.addArrangedSubview(createReactionSpaceButton)
        stackView.addArrangedSubview(updateReactionSpaceButton)
        stackView.addArrangedSubview(deleteReactionSpaceButton)
        stackView.addArrangedSubview(getReactionSpaceDetailButton)
        stackView.addArrangedSubview(getListOfReactionSpaceButton)
        stackView.addArrangedSubview(tableView)
        stackView.addArrangedSubview(showMoreButton)
    }
    
    @objc private func createReactionSpace() {
        guard let targetGroupID = targetGroupIDTextField.text else {
            self.showAlert(title: "Target Group ID Missing", message: "Please Enter ID")
            return
        }
        
        guard let reactionPackIDs = reactionPackIDsTextField.text else {
            self.showAlert(title: "Reaction Pack ID Missing", message: "Please Enter ID")
            return
        }
        
        sdk.reaction.createReactionSpace(name: reactionSpaceNameTextField.text, targetGroupID: targetGroupID, reactionPackIDs: [reactionPackIDs]) { result in
            switch result {
            case .success(let reactionSpace):
                self.showAlert(title: "Reaction Space Created", message: "Reaction Space Id: \(reactionSpace.id)")
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func deleteReactionSpace() {
        guard let spaceID = reactionSpaceIDTextField.text else {
            self.showAlert(title: "Reaction Space ID Missing", message: "Please Enter ID")
            return
        }
        sdk.reaction.deleteReactionSpace(reactionSpaceID: spaceID) { result in
            switch result {
            case .success(let success):
                self.showAlert(title: "Completed", message: "Deleted Reaction Space with ID: \(spaceID)")
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func updateReactionSpace() {
        guard let spaceID = reactionSpaceIDTextField.text else {
            self.showAlert(title: "Reaction Space ID Missing", message: "Please Enter ID")
            return
        }
        
        guard let reactionPackID = reactionPackIDsTextField.text else {
            self.showAlert(title: "Reaction Pack ID Missing", message: "Please Enter ID")
            return
        }
        
        sdk.reaction.updateReactionSpace(reactionSpaceID: spaceID, reactionPackIDs: [reactionPackID]) { result in
            switch result {
            case .success(let reactionPackIDList):
                self.showAlert(title: "Reaction Space Updated", message: "Reaction Pack Ids: \(reactionPackIDList)")
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func getReactionSpaceDetail() {
        guard let spaceID = reactionSpaceIDTextField.text else {
            self.showAlert(title: "Reaction Space ID Missing", message: "Please Enter ID")
            return
        }
        sdk.reaction.getReactionSpaceInfo(reactionSpaceID: spaceID) { result in
            switch result {
            case .success(let reactionSpace):
                self.reactionSpaces = [reactionSpace]
                self.tableView.reloadData()
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func getReactionSpaceList() {
        sdk.reaction.getReactionSpaces(reactionSpaceID: nil, targetGroupID: nil, page: .first, completion: { result in
            switch result {
            case .success(let reactionSpaces):
                if reactionSpaces.count > 0 {
                    self.showAlert(
                        title: "No. of Spaces:",
                        message: "\(reactionSpaces.count)"
                    )
                }
                self.reactionSpaces = reactionSpaces
                self.tableView.reloadData()
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        })
    }
    
    @objc private func showMoreSpaces() {
        sdk.reaction.getReactionSpaces(reactionSpaceID: nil, targetGroupID: nil, page: .next) { result in
            switch result {
            case .success(let reactionSpaces):
                if reactionSpaces.count > 0 {
                    self.showAlert(
                        title: "No. of Spaces:",
                        message: "\(reactionSpaces.count)"
                    )
                }
                self.reactionSpaces.append(contentsOf: reactionSpaces)
                self.tableView.reloadData()
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    private func showAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: title,
                message: message,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension ReactionSpaceUseCase: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reactionSpaces.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        var cellText: String = ""
        let rowData = reactionSpaces[indexPath.row]
        cellText = "Reaction Space:\(String(describing: rowData.name))\nTarget Group Id: \(rowData.targetGroupID), \n Reaction Pack Count = \(rowData.reactionPackIDs.count)"

        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = cellText
        return cell
    }
}

extension ReactionSpaceUseCase: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowData = reactionSpaces[indexPath.row]
        UIPasteboard.general.string = rowData.id
        self.showAlert(title: "Reaction Space Id Copied", message: rowData.id)
    }
}

extension ReactionSpaceUseCase: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
