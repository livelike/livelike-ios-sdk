//
//  ChatWithNewMessageSimulation.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 2/12/20.
//

import EngagementSDK
import UIKit

/// Tests chat with a simulation of receiving new messages
final class ChatWithNewMessageSimulation: ChatTestCaseViewController {
    var timer: DispatchSourceTimer?

    private let startDelay: TimeInterval
    private let messageFrequency: DispatchTimeInterval

    init(
        sdk: EngagementSDK,
        programID: String,
        startDelay: TimeInterval,
        messageFrequency: DispatchTimeInterval
    ) {
        self.startDelay = startDelay
        self.messageFrequency = messageFrequency
        super.init(sdk: sdk, programID: programID, theme: Theme())
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "Simulated Chat Messages"
        vcInfo = "Simulated Messages will appear after \(startDelay) delay and every \(messageFrequency)"

        super.viewDidLoad()

        // Start simulator
        DispatchQueue.global().asyncAfter(deadline: .now() + startDelay) { [weak self] in
            var index = 0

            guard let self = self else { return }
            self.timer = DispatchSource.makeTimerSource(flags: [], queue: .main)
            self.timer?.schedule(deadline: .now(), repeating: self.messageFrequency)
            self.timer?.setEventHandler {
                DispatchQueue.main.async {
                    index += 1
//                    self?.chatController.appendMessageToMessagesList("SIMULATED MESSAGE \(index)")
                }
            }
            self.timer?.resume()
        }
    }
}
