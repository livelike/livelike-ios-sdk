//
//  ChatWithPopupWidgetsUseCase.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 5/10/21.
//

import EngagementSDK
import UIKit

class ChatWithPopupWidgetsUseCase: UIViewController {
    private var sdk: EngagementSDK!
    private weak var session: ContentSession!

    private let widgetPopupViewController = WidgetPopupViewController()
    private let chatViewController = ChatViewController()

    init(contentSession: ContentSession) {
        session = contentSession
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Chat with Widget Popup Controller"
        setupUI()
        setupEngagementSDK()
    }

    private func setupUI() {
        addChild(chatViewController)
        chatViewController.didMove(toParent: self)
        chatViewController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(chatViewController.view)

        addChild(widgetPopupViewController)
        widgetPopupViewController.didMove(toParent: self)
        widgetPopupViewController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(widgetPopupViewController.view)

        NSLayoutConstraint.activate([
            chatViewController.view.topAnchor.constraint(equalTo: view.topAnchor),
            chatViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            chatViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            chatViewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -120),

            widgetPopupViewController.view.topAnchor.constraint(equalTo: view.topAnchor),
            widgetPopupViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            widgetPopupViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            widgetPopupViewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -120)
        ])
    }

    private func setupEngagementSDK() {
        chatViewController.session = session
        widgetPopupViewController.session = session

        // Theming for WidgetPopup
        do {
            if let jsonUrl = Bundle(for: ChatWithPopupWidgetsUseCase.self).url(forResource: "yinzcamTheme", withExtension: "json") {
                let jsonData = try Data(contentsOf: jsonUrl, options: .dataReadingMapped)
                let jsonObject = try JSONSerialization.jsonObject(with: jsonData, options: [])
                let sampleTheme = try Theme.create(fromJSONObject: jsonObject)
                widgetPopupViewController.setTheme(sampleTheme)
            }
        } catch {
            print(error)
        }
    }
}
