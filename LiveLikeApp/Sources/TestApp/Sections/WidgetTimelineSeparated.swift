//
//  WidgetTimelineSeparated.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 5/12/21.
//

import EngagementSDK
import UIKit

class WidgetTimelineSeparatedUseCase: TestCaseViewController {
    private let contentSession: ContentSession

    private lazy var timelineVC: WidgetTimelineSeparated = {
        let vc = WidgetTimelineSeparated(contentSession: self.contentSession)
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        return vc
    }()

    init(sdk: EngagementSDK, programID: String) {
        contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        addChild(timelineVC)
        timelineVC.didMove(toParent: self)
        view.addSubview(timelineVC.view)

        NSLayoutConstraint.activate([
            timelineVC.view.topAnchor.constraint(equalTo: contentView.topAnchor),
            timelineVC.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            timelineVC.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            timelineVC.view.heightAnchor.constraint(equalTo: contentView.heightAnchor)
        ])
    }
}

class WidgetTimelineSeparated: WidgetTimelineViewController {

    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40
    }

    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let separatorView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 40))
        let separatorImage = UIImageView(image: UIImage(named: "chat_ic_default")!)
        let interactiveUntilLabel: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            let interactiveUntilString: String = {
                if let interactiveUntil = self.widgetModels[section].interactiveUntil {
                    return ISO8601DateFormatter().string(from: interactiveUntil)
                } else {
                    return "nil"
                }
            }()
            label.text = "interactive_until: \(interactiveUntilString)"
            return label
        }()
        if section % 2 != 0 {
            separatorImage.backgroundColor = .blue
        } else {
            separatorImage.backgroundColor = .red
        }

        separatorImage.translatesAutoresizingMaskIntoConstraints = false
        separatorView.addSubview(separatorImage)
        separatorView.addSubview(interactiveUntilLabel)
        NSLayoutConstraint.activate([
            separatorImage.centerXAnchor.constraint(equalTo: separatorView.centerXAnchor),
            separatorImage.topAnchor.constraint(equalTo: separatorView.topAnchor),
            interactiveUntilLabel.topAnchor.constraint(equalTo: separatorImage.bottomAnchor),
            interactiveUntilLabel.leadingAnchor.constraint(equalTo: separatorView.leadingAnchor),
            interactiveUntilLabel.trailingAnchor.constraint(equalTo: separatorView.trailingAnchor),
            interactiveUntilLabel.bottomAnchor.constraint(equalTo: separatorView.bottomAnchor),
        ])

        return separatorView
    }
}
