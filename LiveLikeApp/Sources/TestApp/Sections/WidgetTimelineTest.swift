//
//  WidgetTimelineTest.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 2/18/21.
//

import EngagementSDK
import UIKit

class WidgetTimelineTest: TestCaseViewController {
    private let contentSession: ContentSession

    private lazy var timelineVC: WidgetTimelineViewController = {
        let vc = WidgetTimelineViewController(contentSession: self.contentSession)
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        return vc
    }()

    private let widgetRemote: WidgetCreateView

    init(
        sdk: EngagementSDK,
        programID: String,
        apiToken: String,
        timeoutSeconds: Int,
        apiOrigin: URL
    ) {
        widgetRemote = WidgetCreateView(programID: programID, apiToken: apiToken, timeoutSeconds: timeoutSeconds, apiOrigin: apiOrigin)
        contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        addChild(timelineVC)
        timelineVC.didMove(toParent: self)
        view.addSubview(timelineVC.view)

        widgetRemote.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(widgetRemote)

        NSLayoutConstraint.activate([
            timelineVC.view.topAnchor.constraint(equalTo: contentView.topAnchor),
            timelineVC.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            timelineVC.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            timelineVC.view.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.7),

            widgetRemote.topAnchor.constraint(equalTo: timelineVC.view.bottomAnchor),
            widgetRemote.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetRemote.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            widgetRemote.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
}

class InteractiveWidgetTimelineTest: TestCaseViewController {
    private let contentSession: ContentSession

    private lazy var timelineVC: InteractiveWidgetTimelineViewController = {
        let vc = InteractiveWidgetTimelineViewController(contentSession: self.contentSession)
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        return vc
    }()

    private let widgetRemote: WidgetCreateView

    init(
        sdk: EngagementSDK,
        programID: String,
        apiToken: String,
        timeoutSeconds: Int,
        apiOrigin: URL
    ) {
        widgetRemote = WidgetCreateView(programID: programID, apiToken: apiToken, timeoutSeconds: timeoutSeconds, apiOrigin: apiOrigin)
        contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        addChild(timelineVC)
        timelineVC.didMove(toParent: self)
        view.addSubview(timelineVC.view)

        widgetRemote.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(widgetRemote)

        NSLayoutConstraint.activate([
            timelineVC.view.topAnchor.constraint(equalTo: contentView.topAnchor),
            timelineVC.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            timelineVC.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            timelineVC.view.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.7),

            widgetRemote.topAnchor.constraint(equalTo: timelineVC.view.bottomAnchor),
            widgetRemote.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetRemote.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            widgetRemote.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
}
