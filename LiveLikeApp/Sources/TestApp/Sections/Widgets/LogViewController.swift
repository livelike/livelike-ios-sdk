//
//  LogViewController.swift
//  LiveLikeDemo
//
//  Created by Heinrich Dahms on 2019-02-08.
//

import UIKit

class LogViewController: UIViewController {
    // MARK: Private Properties

    private var fileObserver = FileObserver()

    // MARK: Private Properties

    private let logTextView: UITextView = {
        let logTextView: UITextView = UITextView(frame: .zero)
        logTextView.translatesAutoresizingMaskIntoConstraints = false
        logTextView.backgroundColor = .clear
        logTextView.isEditable = false
        return logTextView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        observeLog(file: "livelike.log")
        setUpUI()
    }

    func setUpUI() {
        view.addSubview(logTextView)

        let constraints = [
            logTextView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0.0),
            logTextView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0.0),
            logTextView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0.0),
            logTextView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0.0)
        ]

        NSLayoutConstraint.activate(constraints)
    }

    func observeLog(file: String) {
        guard let docsDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileURL = docsDir.appendingPathComponent(file)
        logTextView.text = try? String(contentsOf: fileURL) // set initial log values
        fileObserver.observeFile(at: fileURL.path) { [weak self] result in
            guard let self = self else { return }
            self.logTextView.text = result
            if !self.logTextView.text.isEmpty {
                let location = self.logTextView.text.count - 1
                let bottom = NSRange(location: location, length: 1)
                self.logTextView.scrollRangeToVisible(bottom)
            }
        }
    }
}
