//
//  WidgetInteractionHistoryUseCase.swift
//  LiveLikeTestApp
//
//  Created by Keval Shah on 26/09/22.
//

import UIKit
import EngagementSDK

class WidgetInteractionHistoryUseCase: TestCaseViewController {

    private var contentSession: ContentSession!

    private var interactionList: [WidgetInteraction] = []

    private var widgetKinds: [WidgetKind] = []

    private let widgetKindsTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(WidgetKindCell.self, forCellReuseIdentifier: "myCell")
        return tableView
    }()

    private let interactionsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        return label
    }()

    private let getHistoryButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get Interaction History", for: .normal)
        return button
    }()

    init(sdk: EngagementSDK, programID: String) {
        self.contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        vcTitle = "Widget Reward Claim Center"

        view.addSubview(widgetKindsTableView)
        view.addSubview(interactionsLabel)
        view.addSubview(getHistoryButton)

        NSLayoutConstraint.activate([
            widgetKindsTableView.topAnchor.constraint(equalTo: contentView.topAnchor),
            widgetKindsTableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetKindsTableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            widgetKindsTableView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.5),

            interactionsLabel.topAnchor.constraint(equalTo: widgetKindsTableView.bottomAnchor),
            interactionsLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            interactionsLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            interactionsLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.45),

            getHistoryButton.topAnchor.constraint(equalTo: interactionsLabel.bottomAnchor),
            getHistoryButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            getHistoryButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            getHistoryButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])

        getHistoryButton.addTarget(self, action: #selector(getHistoryButtonSelected), for: .touchUpInside)
        self.widgetKindsTableView.delegate = self
        self.widgetKindsTableView.dataSource = self
        self.widgetKindsTableView.allowsSelection = true
        self.widgetKindsTableView.allowsMultipleSelection = true

        self.interactionsLabel.numberOfLines = 0
        self.interactionsLabel.adjustsFontSizeToFitWidth = true
    }

    @objc private func getHistoryButtonSelected() {
        self.interactionList = []
        let requestOptions = GetWidgetInteractionsRequestOptions(widgetKind: self.widgetKinds)
        contentSession.getWidgetInteractions(options: requestOptions) { result in
            switch result {
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            case .success(let widgetInteractions):
                log.dev("Found \(widgetInteractions.count) widgets with interactions.")
                self.interactionList.append(contentsOf: widgetInteractions)
                self.showAlert(title: "Interactions: \(widgetInteractions.count)", message: "\(self.interactionList)")
                self.interactionsLabel.text = String(describing: self.interactionList)
            }
        }
    }

    private func showAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: title,
                message: message,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension WidgetInteractionHistoryUseCase: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return WidgetKind.allCases.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "myCell") as? WidgetKindCell
        else {
            return UITableViewCell()
        }

        let widgetKind = WidgetKind.allCases[indexPath.row]
        if widgetKinds.contains(widgetKind) {
            cell.selectSwitch.isOn = true
        }
        cell.tag = indexPath.row
        cell.titleLabel.text = widgetKind.displayName
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.widgetKindsTableView {
            widgetKinds.append(WidgetKind.allCases[indexPath.row])
            tableView.reloadData()
        }
    }
}

class WidgetKindCell: UITableViewCell {

    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()

    let selectSwitch: UISwitch = {
        let selectSwitch = UISwitch()
        selectSwitch.translatesAutoresizingMaskIntoConstraints = false
        selectSwitch.isOn = false
        return selectSwitch
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.addSubview(titleLabel)
        self.addSubview(selectSwitch)

        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            titleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

            selectSwitch.topAnchor.constraint(equalTo: titleLabel.topAnchor, constant: 5),
            selectSwitch.leadingAnchor.constraint(equalTo: self.trailingAnchor, constant: -70),
            selectSwitch.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            selectSwitch.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            selectSwitch.widthAnchor.constraint(equalToConstant: 40)
        ])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        self.titleLabel.text = ""
        self.selectSwitch.isOn = false
    }
}

class InteractionsListCell: UITableViewCell {

    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.addSubview(titleLabel)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        self.titleLabel.text = ""
    }
}
