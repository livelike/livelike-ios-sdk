//
//  CustomPollWidget.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 11/25/20.
//

import EngagementSDK
import UIKit

class CustomPollWidget: Widget, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    private var verticalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 10
        stackView.axis = .vertical
        return stackView
    }()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 17)
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let collectionViewLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        return layout
    }()

    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: self.collectionViewLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(CustomPollWidgetOptionCellView.self, forCellWithReuseIdentifier: "myCell")
        collectionView.backgroundColor = .clear
        return collectionView
    }()

    private let dontCareButton: UIButton = {
        let button = UIButton()
        button.setTitle("Don't Care", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = #colorLiteral(red: 0.07794103771, green: 0.05319341272, blue: 0.3424555659, alpha: 1)
        return button
    }()

    private let model: PollWidgetModel
    private var selectedOptionID: String?

    override init(model: PollWidgetModel) {
        self.model = model
        super.init(model: model)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        dontCareButton.addTarget(self, action: #selector(dontCareButtonSelected), for: .touchUpInside)
        collectionView.dataSource = self
        collectionView.delegate = self

        view.addSubview(verticalStackView)
        verticalStackView.addArrangedSubview(titleLabel)
        verticalStackView.addArrangedSubview(collectionView)
        verticalStackView.addArrangedSubview(dontCareButton)

        NSLayoutConstraint.activate([
            verticalStackView.topAnchor.constraint(equalTo: view.topAnchor, constant: 20),
            verticalStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            verticalStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            verticalStackView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20),

            collectionView.heightAnchor.constraint(equalToConstant: 150)
        ])

        titleLabel.text = model.question
        model.delegate = self

        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(Int(model.interactionTimeInterval))) { [weak self] in
            guard let self = self else { return }
            self.delegate?.widgetDidEnterState(widget: self, state: .finished)
        }
    }

    @objc private func dontCareButtonSelected() {
        delegate?.widgetDidEnterState(widget: self, state: .finished)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.options.count
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myCell", for: indexPath) as? CustomPollWidgetOptionCellView else { return UICollectionViewCell() }
        let option = model.options[indexPath.row]

        cell.percentLabel.text = option.voteCount.description

        if let imageURL = option.imageURL, let imageData = try? Data(contentsOf: imageURL) {
            cell.imageView.image = UIImage(data: imageData)
        }

        cell.imageView.layer.borderWidth = 2
        cell.imageView.layer.cornerRadius = 5
        cell.imageView.clipsToBounds = true
        cell.percentLabel.layer.cornerRadius = 10
        cell.percentLabel.clipsToBounds = true
        if selectedOptionID == option.id {
            cell.imageView.layer.borderColor = UIColor.white.cgColor
        } else {
            cell.imageView.layer.borderColor = UIColor.clear.cgColor
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(
            width: (collectionView.frame.width / 3) - 5,
            height: collectionView.frame.height / CGFloat(model.options.count / 2)
        )
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let option = model.options[indexPath.row]
        model.submitVote(optionID: option.id)

        selectedOptionID = option.id
        collectionView.reloadData()
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
}

extension CustomPollWidget: PollWidgetModelDelegate {
    func pollWidgetModel(
        _ model: PollWidgetModel,
        voteCountDidChange voteCount: Int,
        forOption choiceID: String
    ) {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
}

class CustomPollWidgetOptionCellView: UICollectionViewCell {
    let percentLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .white
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        return label
    }()

    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .red
        return imageView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(imageView)
        addSubview(percentLabel)

        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.9),
            imageView.widthAnchor.constraint(equalTo: widthAnchor),
            imageView.bottomAnchor.constraint(equalTo: bottomAnchor),
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor),

            percentLabel.centerYAnchor.constraint(equalTo: imageView.topAnchor),
            percentLabel.trailingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: -10),
            percentLabel.widthAnchor.constraint(equalToConstant: 20),
            percentLabel.heightAnchor.constraint(equalToConstant: 20)
        ])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}
