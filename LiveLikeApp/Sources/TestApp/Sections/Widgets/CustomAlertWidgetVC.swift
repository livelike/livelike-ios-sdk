//
//  CustomAlertWidgetVC.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 10/20/20.
//

import EngagementSDK
import UIKit

class CustomAlertWidgetVC: Widget {
    private let viewModel: AlertWidgetModel

    var widgetText: UILabel = {
        let content = UILabel()
        content.translatesAutoresizingMaskIntoConstraints = false
        content.backgroundColor = .lightGray
        content.textAlignment = .center
        content.adjustsFontSizeToFitWidth = true
        return content
    }()

    let alertImage: GIFImageView = {
        let image = GIFImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()

    let linkButton: UIButton = {
        let button = UIButton()
        button.setTitle("widget link", for: .normal)
        button.backgroundColor = .red
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    let header: UILabel = {
        let header = UILabel()
        header.backgroundColor = .green
        header.translatesAutoresizingMaskIntoConstraints = false
        header.textAlignment = .center
        header.adjustsFontSizeToFitWidth = true
        return header
    }()

    var timer: Timer = Timer()

    var timerMessage: UILabel = {
        var timerMessage = UILabel()
        timerMessage.translatesAutoresizingMaskIntoConstraints = false
        timerMessage.text = "Widget Will Now Disappear!"
        timerMessage.adjustsFontSizeToFitWidth = true
        timerMessage.backgroundColor = .red
        timerMessage.textColor = .white
        timerMessage.textAlignment = .center
        timerMessage.isHidden = true
        return timerMessage
    }()

    init(alertWidgetViewModel: AlertWidgetModel) {
        viewModel = alertWidgetViewModel
        super.init(model: alertWidgetViewModel)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .gray
        view.addSubview(header)
        view.addSubview(linkButton)
        view.addSubview(alertImage)
        view.addSubview(widgetText)
        view.addSubview(timerMessage)
        NSLayoutConstraint.activate([
            header.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30),
            header.heightAnchor.constraint(equalToConstant: 50.0),
            header.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            header.topAnchor.constraint(equalTo: view.topAnchor),

            alertImage.widthAnchor.constraint(equalToConstant: 100.0),
            alertImage.heightAnchor.constraint(equalToConstant: 100.0),
            alertImage.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            alertImage.topAnchor.constraint(equalTo: header.bottomAnchor),

            linkButton.widthAnchor.constraint(equalToConstant: 100.0),
            linkButton.heightAnchor.constraint(equalToConstant: 50.0),
            linkButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            linkButton.topAnchor.constraint(equalTo: alertImage.bottomAnchor),

            widgetText.widthAnchor.constraint(equalTo: view.widthAnchor),
            widgetText.heightAnchor.constraint(equalToConstant: 50.0),
            widgetText.topAnchor.constraint(equalTo: linkButton.bottomAnchor, constant: 10.0),
            widgetText.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            widgetText.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0.0),

            timerMessage.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -50),
            timerMessage.heightAnchor.constraint(equalToConstant: 20.0),
            timerMessage.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            timerMessage.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])

        header.text = "Title: \(viewModel.title ?? "No title provided")"
        widgetText.text = viewModel.text ?? "No text was provided"
        linkButton.addTarget(self, action: #selector(showLink), for: .touchUpInside)

        if let imageURL = viewModel.imageURL {
            do {
                let data = try Data(contentsOf: imageURL)
                alertImage.image = UIImage(data: data)
            } catch {
                print(error)
            }
        }

        if viewModel.linkURL == nil {
            linkButton.isHidden = true
        }

        timer = Timer.scheduledTimer(withTimeInterval: viewModel.interactionTimeInterval, repeats: false) { [weak self] _ in
            guard let self = self else { return }
            self.timerMessage.isHidden = false
            self.delegate?.widgetDidEnterState(widget: self, state: .finished)
        }
    }

    @objc func showLink() {
        viewModel.openLinkUrl()
    }
}
