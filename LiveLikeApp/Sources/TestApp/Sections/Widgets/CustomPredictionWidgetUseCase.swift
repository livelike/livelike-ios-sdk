//
//  PredictionWidgetUseCase.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 11/30/20.
//

import EngagementSDK
import UIKit

class CustomPredictionWidgetUseCase: Widget {
    @IBOutlet var question: UILabel!
    @IBOutlet var lhsButton: UIButton!
    @IBOutlet var lhsVotes: UILabel!
    @IBOutlet var rhsButton: UIButton!
    @IBOutlet var rhsVotes: UILabel!

    private let model: PredictionWidgetModel
    private let firstChoice: PredictionWidgetModel.Option
    private let secondChoice: PredictionWidgetModel.Option
    private var timer: Timer = Timer()

    override init(model: PredictionWidgetModel) {
        self.model = model
        firstChoice = model.options.first!
        secondChoice = model.options[1]
        super.init(model: model)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        model.delegate = self

        question.text = model.question

        lhsButton.setTitle(firstChoice.text, for: .normal)
        if let imageURL = firstChoice.imageURL {
            do {
                let data = try Data(contentsOf: imageURL)
                lhsButton.setImage(UIImage(data: data), for: .normal)
                lhsButton.contentHorizontalAlignment = .fill
                lhsButton.contentVerticalAlignment = .fill
                lhsButton.imageView?.contentMode = .scaleAspectFit
            } catch {
                print(error)
            }
        }

        rhsButton.setTitle(secondChoice.text, for: .normal)
        if let imageURL = secondChoice.imageURL {
            do {
                let data = try Data(contentsOf: imageURL)
                rhsButton.setImage(UIImage(data: data), for: .normal)
                rhsButton.contentHorizontalAlignment = .fill
                rhsButton.contentVerticalAlignment = .fill
                rhsButton.imageView?.contentMode = .scaleAspectFit
            } catch {
                print(error)
            }
        }

        timer = Timer.scheduledTimer(withTimeInterval: model.interactionTimeInterval + 3, repeats: false) { [weak self] _ in
            guard let self = self else { return }
            self.delegate?.widgetDidEnterState(widget: self, state: .finished)
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @IBAction func lhsAction(_ sender: Any) {
        model.submitVote(optionID: firstChoice.id) { result in
            switch result {
            case .success:
                self.lhsButton.backgroundColor = .black
                self.lhsButton.alpha = 0.5
                self.view.isUserInteractionEnabled = false
            case let .failure(error):
                print(error)
            }
        }
    }

    @IBAction func rhsAction(_ sender: Any) {
        model.submitVote(optionID: secondChoice.id) { result in
            switch result {
            case .success:
                self.rhsButton.backgroundColor = .black
                self.rhsButton.alpha = 0.5
                self.view.isUserInteractionEnabled = false
            case let .failure(error):
                print(error)
            }
        }
    }
}

extension CustomPredictionWidgetUseCase: PredictionWidgetModelDelegate {
    func predictionWidgetModel(_ model: PredictionWidgetModel, didReceiveFollowUp id: String, kind: WidgetKind) {

    }

    func predictionWidgetModel(_ model: PredictionWidgetModel, voteCountDidChange voteCount: Int, forOption optionID: String) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }

            if self.firstChoice.id == optionID {
                self.lhsVotes.text = "Votes: \(voteCount)"
            } else if self.secondChoice.id == optionID {
                self.rhsVotes.text = "Votes: \(voteCount)"
            }
        }
    }
}
