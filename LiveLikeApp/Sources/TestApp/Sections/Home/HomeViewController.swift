//
//  HomeViewController.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 1/30/20.
//

import EngagementSDK
import MobileCoreServices
import UIKit

class HomeViewController: UIViewController {
    class Section {
        var text: String
        var rows: [Row]

        init(text: String, rows: [Row]) {
            self.text = text
            self.rows = rows
        }
    }

    class Row {
        var getText: () -> String
        var onClick: () -> Void

        init(text: String, onClick: @escaping () -> Void) {
            getText = { text }
            self.onClick = onClick
        }

        init(getText: @escaping () -> String, onClick: @escaping () -> Void) {
            self.getText = getText
            self.onClick = onClick
        }
    }

    private let filterUseCasesKey: String = "LiveLike.TestApp.FilterUseCases"
    private var useCaseFilter: String? {
        get {
            return UserDefaults.standard.value(forKey: filterUseCasesKey) as? String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: filterUseCasesKey)
        }
    }

    private let tableView: UITableView = UITableView(frame: .zero, style: .grouped)

    private let sdkVersionString: String = {
        let gAppVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") ?? "0"
        let gAppBuild = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") ?? "0"
        return "v\(EngagementSDK.version) - v\(gAppVersion)(\(gAppBuild))"
    }()

    private let documentPicker = DocumentPickerDelegate()

    private lazy var configSection: Section = Section(text: "Config", rows: [
        Row(text: "sdk: \(self.sdkVersionString)", onClick: {}),
        Row(
            getText: { "env: \(EngagementSDKConfigManager.shared.selectedEnvironment.name)" },
            onClick: {
                self.tableView.reloadData()
                let apiEndpointsView = UIAlertController.apiEndpointsAlertController(apiEndpoints: [.dev, .qa, .qaGameChangers, .qaIconic, .qaDig, .staging, .prod, .fiftyfive], completion: { [weak self] in
                    self?.tableView.reloadData()
                })
                if let presenter = apiEndpointsView.popoverPresentationController {
                    presenter.sourceView = self.view
                    presenter.permittedArrowDirections = []
                    presenter.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                }
                self.present(apiEndpointsView, animated: true, completion: nil)
            }
        ),
        Row(
            getText: { "app: \(EngagementSDKConfigManager.shared.selectedClientApp?.name ?? "")" },
            onClick: {
                let clientIDs = EngagementSDKConfigManager.shared.selectedEnvironment.availableClientApps
                let eventSelectView = UIAlertController.clientIdAlertController(clientIDs: clientIDs, completion: { [weak self] in
                    self?.tableView.reloadData()
                })

                if let presenter = eventSelectView.popoverPresentationController {
                    presenter.sourceView = self.view
                    presenter.permittedArrowDirections = []
                    presenter.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                }
                self.present(eventSelectView, animated: true, completion: nil)
            }
        ),
        Row(
            getText: { "prog: \(EngagementSDKConfigManager.shared.selectedProgram?.title ?? "")" },
            onClick: {
                guard let clientId = EngagementSDKConfigManager.shared.selectedClientApp else { return }
                LiveLikeAPI.getAllPrograms(
                    apiEndpoint: EngagementSDKConfigManager.shared.selectedEnvironment.url,
                    clientID: clientId.id,
                    completion: { [weak self] programs in
                        guard let self = self else { return }
                        DispatchQueue.main.async {
                            let eventSelectView = UIAlertController.eventsAlertController(events: programs, programNumber: 1) {
                                guard let program = EngagementSDKConfigManager.shared.selectedProgram else { return }
                                let message = """
                                id: \(program.id)
                                name: \(program.title)
                                """
                                let paragraphStyle = NSMutableParagraphStyle()
                                paragraphStyle.alignment = .left
                                let messageText = NSAttributedString(
                                    string: message,
                                    attributes: [
                                        NSAttributedString.Key.paragraphStyle: paragraphStyle,
                                        NSAttributedString.Key.font: UIFont.systemFont(ofSize: 10)
                                    ]
                                )
                                let alert = UIAlertController(title: "Program Details", message: nil, preferredStyle: .alert)
                                alert.setValue(messageText, forKey: "attributedMessage")
                                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                self.tableView.reloadData()
                            }
                            if let presenter = eventSelectView.popoverPresentationController {
                                presenter.sourceView = self.view
                                presenter.permittedArrowDirections = []
                                presenter.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                            }
                            self.present(eventSelectView, animated: true, completion: nil)
                        }
                    }, failure: { error in
                        print(error.localizedDescription)
                    }
                )
            }
        ),
        Row(
            getText: {
                if let apiToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) {
                    return "api token: \(apiToken.prefix(5))..."
                } else {
                    return "api token: Not Set"
                }
            },
            onClick: {
                let alert = UIAlertController(
                    title: "Set API Token",
                    message: "Paste your API Token for \(EngagementSDKConfigManager.shared.selectedEnvironment.name) environment",
                    preferredStyle: .alert
                )
                alert.addTextField(configurationHandler: nil)
                alert.addAction(
                    UIAlertAction(title: "Set", style: .default, handler: { _ in
                        let token = alert.textFields?[0].text ?? ""
                        EngagementSDKConfigManager.shared.setAPIToken(
                            token,
                            environment: EngagementSDKConfigManager.shared.selectedEnvironment
                        )
                        self.tableView.reloadData()
                    })
                )
                alert.addAction(
                    UIAlertAction(title: "Clear", style: .default, handler: { _ in
                        EngagementSDKConfigManager.shared.clearAPIToken(
                            for: EngagementSDKConfigManager.shared.selectedEnvironment
                        )
                        self.tableView.reloadData()
                    })
                )
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        ),
        Row(
            getText: {
                if let useCaseFilter = self.useCaseFilter {
                    return "Filter: \(useCaseFilter) (Clear Filter)"
                } else {
                    return "Filter: No Filter"
                }
            },
            onClick: {
                let alert = UIAlertController(title: "Filter", message: "Enter string to filter use cases by", preferredStyle: .alert)
                alert.addTextField(configurationHandler: nil)
                alert.addAction(
                    UIAlertAction(title: "Set", style: .default, handler: { _ in
                        self.useCaseFilter = alert.textFields?[0].text ?? ""
                        self.tableView.reloadData()
                    })
                )
                alert.addAction(
                    UIAlertAction(title: "Clear", style: .default, handler: { _ in
                        self.useCaseFilter = nil
                        self.tableView.reloadData()
                    })
                )
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        )
    ])

    // MARK: - Use Cases

    private lazy var useCaseSection: Section = Section(text: "Use Cases", rows: [

        // MARK: "Video Layout"

        Row(text: "Video Layout", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else {
                return
            }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }
            let separatedVC = SeparatedVideoViewController(
                sdk: EngagementSDK(config: config),
                programID: programID
            )
            separatedVC.modalPresentationStyle = .fullScreen
            self.present(separatedVC, animated: true, completion: nil)
        }),

        // MARK: "Content Session Chat"

        Row(text: "Content Session Chat", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let sdk = EngagementSDK(config: config)
            let vc = DefaultChatOnlySection(sdk: sdk, programID: programID, theme: Theme())
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Widgets"

        Row(text: "Widgets", onClick: {
            let alert = UIAlertController(title: "Widget Settings", message: "Set widgets timeout (s)", preferredStyle: .alert)
            alert.addTextField { textField in
                textField.text = "5"
                textField.keyboardType = .numberPad
            }
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                guard let config = self.createEngagementSDKFromHomeSettings() else {
                    return
                }
                guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                    self.showNoAPITokenAlert()
                    return
                }
                guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                    self.showNoProgramIDAlert()
                    return
                }
                guard let timeout = Int(alert.textFields?[0].text ?? "5") else {
                    print("Couldn't parse textfield as Int")
                    return
                }
                let vc = WidgetsTestingViewController(
                    sdk: EngagementSDK(config: config),
                    apiToken: accessToken,
                    programID: programID,
                    theme: Theme(),
                    timeoutSeconds: timeout,
                    apiOrigin: EngagementSDKConfigManager.shared.selectedEnvironment.url
                )
                self.present(vc, animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }),

        // MARK: "Randomized Widgets"

        Row(text: "Randomized Widgets", onClick: {
            let alert = UIAlertController(title: "Widget Settings", message: "Set widgets timeout (s)     Widgets Interval (s)", preferredStyle: .alert)
            alert.addTextField { textField in
                textField.text = "5"
                textField.keyboardType = .numberPad
            }
            alert.addTextField { textField in
                textField.text = "10"
                textField.keyboardType = .numberPad
            }
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                guard let config = self.createEngagementSDKFromHomeSettings() else {
                    return
                }
                guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                    self.showNoAPITokenAlert()
                    return
                }
                guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                    self.showNoProgramIDAlert()
                    return
                }
                guard let timeout = Int(alert.textFields?[0].text ?? "5") else {
                    print("Couldn't parse textfield as Int")
                    return
                }
                guard let widgetInterval = Int(alert.textFields?[1].text ?? "5") else {
                    print("Couldn't parse textfield as Int")
                    return
                }
                let vc = WidgetsTestingViewController(
                    sdk: EngagementSDK(config: config),
                    apiToken: accessToken,
                    programID: programID,
                    theme: Theme(),
                    timeoutSeconds: timeout,
                    apiOrigin: EngagementSDKConfigManager.shared.selectedEnvironment.url,
                    widgetsInterval: Double(widgetInterval)
                )
                self.present(vc, animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }),

        // MARK: "Chat Session With Sync"

        Row(text: "Chat Session With Sync", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            let vc = ChatSessionWithSyncTestViewController(
                envBaseURL: EngagementSDKConfigManager.shared.selectedEnvironment.url,
                sdk: sdk
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Chat Session Unread Message Count"

        Row(text: "Chat Session Unread Message Count", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let vc = UnreadMessageCountSection(
                envBaseURL: EngagementSDKConfigManager.shared.selectedEnvironment.url,
                sdk: EngagementSDK(config: config)
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Chat Session With Theme"

        Row(text: "Chat Session With Theme", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }

            let alert = UIAlertController.chooseThemeAlert(completion: { theme in
                let vc = GroupChatSection(
                    sdk: EngagementSDK(config: config),
                    envBaseURL: EngagementSDKConfigManager.shared.selectedEnvironment.url,
                    theme: theme
                )
                self.present(vc, animated: true, completion: nil)
            })
            self.present(alert, animated: true, completion: nil)

        }),

        // MARK: "Chat With Simulated New Messages"

        Row(text: "Chat With Simulated New Messages", onClick: {
            let alert = UIAlertController(title: "Simulator Settings", message: "Set a Start Delay (s) and Message Interval (ms).", preferredStyle: .alert)
            alert.addTextField(configurationHandler: { textField in
                textField.text = "3"
                textField.keyboardType = .numberPad
            })
            alert.addTextField(configurationHandler: { textField in
                textField.text = "200"
                textField.keyboardType = .numberPad
            })
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                guard let delay = TimeInterval(alert.textFields?[0].text ?? "") else { return }
                guard let freq = Int(alert.textFields?[1].text ?? "") else { return }
                guard let config = self.createEngagementSDKFromHomeSettings() else { return }
                guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }
                let vc = ChatWithNewMessageSimulation(
                    sdk: EngagementSDK(config: config),
                    programID: programID,
                    startDelay: delay,
                    messageFrequency: .milliseconds(freq)
                )
                self.present(vc, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }),

        // MARK: "Content Session Chat With Theme"

        Row(text: "Content Session Chat With Theme", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                self.showNoProgramIDAlert()
                return
            }

            let alert = UIAlertController.chooseThemeAlert(completion: { theme in
                let chatSection = DefaultChatOnlySection(
                    sdk: EngagementSDK(config: config),
                    programID: programID,
                    theme: theme
                )
                self.present(chatSection, animated: true, completion: nil)
            })
            self.present(alert, animated: true, completion: nil)
        }),

        // MARK: "Widgets With Theme"

        Row(text: "Widgets With Theme", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                self.showNoProgramIDAlert()
                return
            }
            guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                self.showNoAPITokenAlert()
                return
            }

            let alert = UIAlertController.chooseThemeAlert(completion: { [weak self] theme in
                guard let self = self else { return }
                let widgetSection = WidgetsTestingViewController(
                    sdk: EngagementSDK(config: config),
                    apiToken: accessToken,
                    programID: programID,
                    theme: theme,
                    timeoutSeconds: 5,
                    apiOrigin: EngagementSDKConfigManager.shared.selectedEnvironment.url
                )
                self.present(widgetSection, animated: true, completion: nil)
            })
            self.present(alert, animated: true, completion: nil)
        }),

        // MARK: "Widgets With JSON Theme"

        Row(text: "Widgets With JSON Theme", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                self.showNoProgramIDAlert()
                return
            }
            guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                self.showNoAPITokenAlert()
                return
            }

            let vc = JSONThemeInputViewController()
            vc.didParseJSONIntoTheme = { [weak self] theme in
                guard let self = self else { return }
                let widgetSection = WidgetsTestingViewController(
                    sdk: EngagementSDK(config: config),
                    apiToken: accessToken,
                    programID: programID,
                    theme: theme,
                    timeoutSeconds: 5,
                    apiOrigin: EngagementSDKConfigManager.shared.selectedEnvironment.url
                )
                vc.dismiss(animated: true, completion: {
                    self.present(widgetSection, animated: true, completion: nil)
                })
            }
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Widget With JSON Theme From File"

        Row(text: "Widget With JSON Theme From File", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                self.showNoProgramIDAlert()
                return
            }
            guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                self.showNoAPITokenAlert()
                return
            }

            let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypeJSON as String], in: .import)
            if #available(iOS 11.0, *) {
                documentPicker.allowsMultipleSelection = false
            } else {
                // Fallback on earlier versions
            }
            self.documentPicker.didPickDocumentsAtCompletion = { [weak self] urls in
                guard let self = self else { return }
                guard let url = urls.first else { return }

                do {
                    let data = try Data(contentsOf: url)
                    let jsonObject = try JSONSerialization.jsonObject(with: data, options: [])
                    let theme = try Theme.create(fromJSONObject: jsonObject)

                    let widgetSection = WidgetsTestingViewController(
                        sdk: EngagementSDK(config: config),
                        apiToken: accessToken,
                        programID: programID,
                        theme: theme,
                        timeoutSeconds: 5,
                        apiOrigin: EngagementSDKConfigManager.shared.selectedEnvironment.url
                    )
                    self.present(widgetSection, animated: true, completion: nil)
                } catch {
                    print("Failed to create theme from file")
                }
            }
            documentPicker.delegate = self.documentPicker
            self.present(documentPicker, animated: true, completion: nil)

        }),

        // MARK: "Chat with Pasted Image"

        Row(text: "Chat with Pasted Image", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }
            let vc = ImagePasteChatViewController(
                sdk: EngagementSDK(config: config),
                programID: programID,
                theme: Theme()
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Keyboard Dismiss After Message Sent"

        Row(text: "Keyboard Dismiss After Message Sent", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }
            let vc = KeyboardDismissOnMessageSentViewController(
                sdk: EngagementSDK(config: config),
                programID: programID,
                theme: Theme()
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Custom Chat Message Timestamp Format"

        Row(text: "Custom Chat Message Timestamp Format", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }
            let vc = CustomChatMessageTimestampViewController(
                sdk: EngagementSDK(config: config),
                programID: programID,
                theme: Theme()
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Chat Timestamp Disabled"

        Row(text: "Chat Timestamp Disabled", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }
            let vc = DisableChatMessageTimestampViewController(
                sdk: EngagementSDK(config: config),
                programID: programID,
                theme: Theme()
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Nil PlayheadTimeSource"

        Row(text: "Nil PlayheadTimeSource", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }
            let vc = NilPlayheadTimeSourceViewController(
                sdk: EngagementSDK(config: config),
                programID: programID
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "No Chat Profile Status Bar"

        Row(text: "No Chat Profile Status Bar", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }
            let vc = NoProfileStatusBarChat(
                sdk: EngagementSDK(config: config),
                programID: programID,
                theme: Theme()
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "On Demand Widget Dynamic Height Endless List"

        Row(text: "On Demand Widget Dynamic Height Endless List", onClick: {
            guard let clientID = EngagementSDKConfigManager.shared.selectedClientApp?.id else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }
            guard let apiToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else { return }
            let vc = OnDemandWidgetDynamicListView(
                clientID: clientID,
                programID: programID,
                apiToken: apiToken,
                apiOrigin: EngagementSDKConfigManager.shared.selectedEnvironment.url
            )
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Widget With State Control"

        Row(text: "Widget With State Control", onClick: {
            guard let clientID = EngagementSDKConfigManager.shared.selectedClientApp?.id else { return }

            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }

            guard let apiToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                return
            }

            let vc = WidgetWithStateControlVC(
                clientID: clientID,
                programID: programID,
                apiToken: apiToken,
                apiOrigin: EngagementSDKConfigManager.shared.selectedEnvironment.url
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Create Chat Room"

        Row(text: "Create Chat Room", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            let vc = CreateChatRoomVC(
                envBaseURL: EngagementSDKConfigManager.shared.selectedEnvironment.url,
                sdk: sdk
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Create Chat Room"

        Row(text: "Create Token Gated Chat Room", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            let vc = CreateTokenGatedChatRoomVC(
                envBaseURL: EngagementSDKConfigManager.shared.selectedEnvironment.url,
                sdk: sdk
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Token Gated Chat"

        Row(text: "Token Gated Chat", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            let vc = TokenGatedChatRoomVC(
                envBaseURL: EngagementSDKConfigManager.shared.selectedEnvironment.url,
                sdk: sdk
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Chat Room Membership"

        Row(text: "Chat Room Membership", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            let vc = ChatRoomMembershipUseCase(
                envBaseURL: EngagementSDKConfigManager.shared.selectedEnvironment.url,
                sdk: sdk
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Widget Theme Randomizer"

        Row(text: "Widget Theme Randomizer", onClick: {
            guard let clientID = EngagementSDKConfigManager.shared.selectedClientApp?.id else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }
            let environment = EngagementSDKConfigManager.shared.selectedEnvironment
            guard let apiToken = EngagementSDKConfigManager.shared.getAPIToken(for: environment) else { return }
            let vc = WidgetThemeRandomizeSection(
                clientID: clientID,
                programID: programID,
                apiToken: apiToken,
                apiOrigin: environment.url,
                timeoutSeconds: 10
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "WidgetPopupViewControllerDelegate method order tests"

        Row(text: "WidgetPopupViewControllerDelegate method order tests", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            guard let clientID = EngagementSDKConfigManager.shared.selectedClientApp?.id else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }
            let environment = EngagementSDKConfigManager.shared.selectedEnvironment
            guard let apiToken = EngagementSDKConfigManager.shared.getAPIToken(for: environment) else { return }
            let vc = WidgetViewControllerDelegateTests(
                sdk: sdk,
                apiToken: apiToken,
                programID: programID,
                timeoutSeconds: 10,
                apiOrigin: environment.url
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Resizeable Chat"

        Row(text: "Resizeable Chat", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }
            let sdk = EngagementSDK(config: config)
            let session = sdk.contentSession(config: SessionConfiguration(programID: programID))
            let vc = ChatResizeTestSection(contentSession: session)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Integrator Triggers Widget"

        Row(text: "Integrator Triggers Widget", onClick: {
            guard let clientID = EngagementSDKConfigManager.shared.selectedClientApp?.id else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }
            let vc = IntegratorTriggersWidgetViewController(clientID: clientID, programID: programID)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Split Message View Controller and Input View"

        Row(text: "Split Message View Controller and Input View", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }
            let sdk = EngagementSDK(config: config)
            let session = sdk.contentSession(config: SessionConfiguration(programID: programID))
            let vc = SplitChatMessageListAndInputViewSection(contentSession: session)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Chat hidden input view"

        Row(text: "Chat hidden input view", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }
            let sdk = EngagementSDK(config: config)
            let session = sdk.contentSession(config: SessionConfiguration(programID: programID))
            let vc = ChatHiddenInputViewSection(contentSession: session)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Influencer Chat Demo - Fan Perspective"

        Row(text: "Influencer Chat Demo - Fan Perspective", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }
            let sdk = EngagementSDK(config: config)
            let session = sdk.contentSession(config: SessionConfiguration(programID: programID))
            let vc = InfluencerChatFanDemo(contentSession: session)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Leaderboards"

        Row(text: "Leaderboards", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }
            let sdk = EngagementSDK(config: config)
            let vc = LeaderboardsUseCaseVC(sdk: sdk, programID: programID)
            let navigationControllerVC = UINavigationController(rootViewController: vc)
            navigationControllerVC.modalPresentationStyle = .fullScreen
            self.present(navigationControllerVC, animated: true, completion: nil)
        }),

        // MARK: "Automatic Chat Reactions"

        Row(text: "Automatic Chat Reactions", onClick: {
            let alert = UIAlertController(title: "Settings", message: "Set a Start Delay (s) and Message Interval (ms).", preferredStyle: .alert)
            alert.addTextField(configurationHandler: { textField in
                textField.text = "3"
                textField.keyboardType = .numberPad
            })
            alert.addTextField(configurationHandler: { textField in
                textField.text = "200"
                textField.keyboardType = .numberPad
            })
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                guard let delay = TimeInterval(alert.textFields?[0].text ?? "") else { return }
                guard let freq = Int(alert.textFields?[1].text ?? "") else { return }
                guard let config = self.createEngagementSDKFromHomeSettings() else { return }
                guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }
                let sdk = EngagementSDK(config: config)
                let vc = ChatReactionSimulatorSection(
                    sdk: sdk,
                    programID: programID,
                    startDelay: delay,
                    messageFrequency: .milliseconds(freq)
                )
                self.present(vc, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }),

        // MARK: "Chat With Avatar"

        Row(text: "Chat With Avatar", onClick: {
            self.showAvatarPicker { imageURL in
                guard let imageURL = imageURL else { return }

                self.showChatTypePicker { chatType in

                    switch chatType {
                    case .contentSession:

                        self.showChatContentFilterPicker { showFilteredMessages in

                            DispatchQueue.main.async {
                                guard let config = self.createEngagementSDKFromHomeSettings() else { return }
                                guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                                    return self.showNoProgramIDAlert()
                                }
                                let sdk = EngagementSDK(config: config)
                                let vc = ContentSessionChatAvatarVC(
                                    sdk: sdk,
                                    programID: programID,
                                    avatarURL: imageURL,
                                    showFilteredMessages: showFilteredMessages
                                )
                                self.present(vc, animated: true, completion: nil)
                            }

                        }
                    case .chatSession:

                        self.showChatContentFilterPicker { showFilteredMessages in

                            DispatchQueue.main.async {
                                guard let config = self.createEngagementSDKFromHomeSettings() else { return }
                                let vc = GroupChatAvatarVC(
                                    sdk: EngagementSDK(config: config),
                                    envBaseURL: EngagementSDKConfigManager.shared.selectedEnvironment.url,
                                    avatarURL: imageURL,
                                    showFilteredMessages: showFilteredMessages
                                )

                                self.present(vc, animated: true, completion: nil)
                            }

                        }
                    }
                }
            }
        }),

        // MARK: "Chat Message Deleted Behavior Remove"

        Row(text: "Chat Message Deleted Behavior Remove", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let sdk = EngagementSDK(config: config)
            let vc = DefaultChatOnlySection(sdk: sdk, programID: programID, theme: Theme())
            vc.chatController.hideDeletedMessage = true
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Chat Pause / Resume"

        Row(text: "Chat Pause / Resume", onClick: {
            self.showChatTypePicker { chatType in

                switch chatType {
                case .contentSession:
                    DispatchQueue.main.async {
                        guard let config = self.createEngagementSDKFromHomeSettings() else { return }
                        guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                            return self.showNoProgramIDAlert()
                        }
                        let sdk = EngagementSDK(config: config)
                        let vc = ContentSessionChatPauseResumeUseCaseVC(
                            sdk: sdk,
                            programID: programID
                        )
                        self.present(vc, animated: true, completion: nil)
                    }

                case .chatSession:
                    guard let config = self.createEngagementSDKFromHomeSettings() else { return }
                    let vc = GroupChatChatPauseResumeUseCaseVC(
                        sdk: EngagementSDK(config: config),
                        envBaseURL: EngagementSDKConfigManager.shared.selectedEnvironment.url
                    )

                    self.present(vc, animated: true, completion: nil)
                }
            }
        }),

        // MARK: "Prediction Vote Repository Override"

        Row(text: "Prediction Vote Repository Override", onClick: {
            guard let clientID = EngagementSDKConfigManager.shared.selectedClientApp?.id else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }
            guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                self.showNoAPITokenAlert()
                return
            }
            let vc = PredictionVoteRepositoryOverride(
                clientID: clientID,
                programID: programID,
                apiToken: accessToken,
                apiOrigin: EngagementSDKConfigManager.shared.selectedEnvironment.url
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Program Leaderboard Clients"

        Row(text: "Program Leaderboard Clients", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else {
                return
            }
            guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                self.showNoAPITokenAlert()
                return
            }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                self.showNoProgramIDAlert()
                return
            }
            let vc = LeaderboardClientTestSection(
                sdk: EngagementSDK(config: config),
                apiToken: accessToken,
                programID: programID,
                apiOrigin: EngagementSDKConfigManager.shared.selectedEnvironment.url
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Leaderboard Demo"

        Row(text: "Leaderboard Demo", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else {
                return
            }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                self.showNoProgramIDAlert()
                return
            }
            let sdk = EngagementSDK(config: config)
            let vc = WidgetLeaderboardDemo(sdk: sdk, programID: programID)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Always Init SDK without Access Token"

        Row(text: "Always Init SDK without Access Token", onClick: {
            guard let clientID = EngagementSDKConfigManager.shared.selectedClientApp?.id else { return }
            let vc = SDKInitWithoutAccessTokenSection(clientID: clientID)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Get Message Count Since Test"

        Row(text: "Get Message Count Since Test", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else {
                return
            }
            let chatRoomID = GroupChatManager.shared.chatRoomIds[0]
            let vc = GetMessagesSinceTest(sdk: EngagementSDK(config: config), chatRoomID: chatRoomID)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Widget State Control w/ Interaction Log"

        Row(text: "Widget State Control w/ Interaction Log", onClick: {
            guard let clientID = EngagementSDKConfigManager.shared.selectedClientApp?.id else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }
            guard let apiToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                return
            }
            let vc = WidgetStateControlWithInteractionLog(
                clientID: clientID,
                programID: programID,
                apiToken: apiToken,
                apiOrigin: EngagementSDKConfigManager.shared.selectedEnvironment.url
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Custom Widget UI"

        Row(text: "Custom Widget UI", onClick: {
            let alert = UIAlertController(title: "Widget Settings", message: "Set widgets timeout (s)", preferredStyle: .alert)
            alert.addTextField { textField in
                textField.text = "5"
                textField.keyboardType = .numberPad
            }
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                guard let config = self.createEngagementSDKFromHomeSettings() else {
                    return
                }
                guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                    self.showNoAPITokenAlert()
                    return
                }
                guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                    self.showNoProgramIDAlert()
                    return
                }
                guard let timeout = Int(alert.textFields?[0].text ?? "5") else {
                    print("Couldn't parse textfield as Int")
                    return
                }
                let vc = CustomWidgetsUITestCase(
                    sdk: EngagementSDK(config: config),
                    apiToken: accessToken,
                    programID: programID,
                    theme: Theme(),
                    timeoutSeconds: timeout,
                    apiOrigin: EngagementSDKConfigManager.shared.selectedEnvironment.url
                )
                self.present(vc, animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }),

        // MARK: "WidgetTimelineViewController Test"

        Row(text: "WidgetTimelineViewController Test", onClick: {
            let alert = UIAlertController(title: "Widget Settings", message: "Set widgets timeout (s)", preferredStyle: .alert)
            alert.addTextField { textField in
                textField.text = "10"
                textField.keyboardType = .numberPad
            }
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                guard let config = self.createEngagementSDKFromHomeSettings() else {
                    return
                }
                guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                    self.showNoAPITokenAlert()
                    return
                }
                guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                    self.showNoProgramIDAlert()
                    return
                }
                guard let timeout = Int(alert.textFields?[0].text ?? "5") else {
                    print("Couldn't parse textfield as Int")
                    return
                }
                let vc = WidgetTimelineTest(
                    sdk: EngagementSDK(config: config),
                    programID: programID,
                    apiToken: accessToken,
                    timeoutSeconds: timeout,
                    apiOrigin: EngagementSDKConfigManager.shared.selectedEnvironment.url
                )
                self.present(vc, animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }),

        // MARK: "WidgetTimeline w/ Separators"

        Row(text: "WidgetTimeline w/ Separators", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else {
                return
            }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                self.showNoProgramIDAlert()
                return
            }

            let vc = WidgetTimelineSeparatedUseCase(sdk: EngagementSDK(config: config), programID: programID)           
            self.present(vc, animated: true, completion: nil)
        }),

        Row(text: "InteractiveWidgetTimelineViewController Test", onClick: {
            let alert = UIAlertController(title: "Widget Settings", message: "Set widgets timeout (s)", preferredStyle: .alert)
            alert.addTextField { textField in
                textField.text = "10"
                textField.keyboardType = .numberPad
            }
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                guard let config = self.createEngagementSDKFromHomeSettings() else {
                    return
                }
                guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                    self.showNoAPITokenAlert()
                    return
                }
                guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                    self.showNoProgramIDAlert()
                    return
                }
                guard let timeout = Int(alert.textFields?[0].text ?? "5") else {
                    print("Couldn't parse textfield as Int")
                    return
                }
                let vc = InteractiveWidgetTimelineTest(
                    sdk: EngagementSDK(config: config),
                    programID: programID,
                    apiToken: accessToken,
                    timeoutSeconds: timeout,
                    apiOrigin: EngagementSDKConfigManager.shared.selectedEnvironment.url
                )
                self.present(vc, animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }),

        // MARK: "Custom Chat Data"

        Row(text: "Custom Chat Data", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            let vc = CustomChatDataUseCaseVC(
                envBaseURL: EngagementSDKConfigManager.shared.selectedEnvironment.url,
                sdk: sdk
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Custom Chat Message Data"
        Row(text: "Custom Chat Message Data", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                self.showNoProgramIDAlert()
                return
            }
            guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                self.showNoAPITokenAlert()
                return
            }

            let contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
            let vc = CustomChatMessageTestSection(
                contentSession: contentSession,
                baseURL: config.apiOrigin,
                accessToken: accessToken
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Widgets"

        Row(text: "Multiple Session Widgets", onClick: {
            let alert = UIAlertController(title: "Widget Settings", message: "Set widgets timeout (s)", preferredStyle: .alert)
            alert.addTextField { textField in
                textField.text = "5"
                textField.keyboardType = .numberPad
            }
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                guard let config = self.createEngagementSDKFromHomeSettings() else {
                    return
                }
                guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                    self.showNoAPITokenAlert()
                    return
                }
                guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                    self.showNoProgramIDAlert()
                    return
                }
                guard let timeout = Int(alert.textFields?[0].text ?? "5") else {
                    print("Couldn't parse textfield as Int")
                    return
                }
                let vc = MultipleSessionsWidgetsUseCase(
                    sdk: EngagementSDK(config: config),
                    apiToken: accessToken,
                    programID: programID,
                    theme: Theme(),
                    timeoutSeconds: timeout,
                    apiOrigin: EngagementSDKConfigManager.shared.selectedEnvironment.url
                )
                self.present(vc, animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }),

        // MARK: "Swipable Chat, Popup, Timeline"
        Row(text: "Swipable Chat, Popup, Timeline", onClick: {
            guard let clientID = EngagementSDKConfigManager.shared.selectedClientApp?.id else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }

            let vc = TabbedCollectionViewUseCase(clientID: clientID, programID: programID)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Sponsored Chat"

        Row(text: "Sponsored Chat", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let sdk = EngagementSDK(config: config)
            let vc = SponsoredChatUseCase(sdk: sdk, programID: programID, theme: Theme())
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Widget Reward Claim Center"
        Row(text: "Widget Reward Claim Center", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let sdk = EngagementSDK(config: config)
            let vc = WidgetRewardClaimCenterViewController(sdk: sdk, programID: programID)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "User Profile Badge List"
        Row(text: "User Profile Badge List", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let sdk = EngagementSDK(config: config)
            let vc = UserProfileBadgesUseCase(sdk: sdk, programID: programID)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "User Badge Progress"
        Row(text: "User Badge Progress", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let sdk = EngagementSDK(config: config)
            let vc = UserProfileBadgeProgressUseCase(sdk: sdk, programID: programID)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Profiles For Badge List"
        Row(text: "Profiles For Badge List", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let sdk = EngagementSDK(config: config)
            let vc = BadgeProfilesUseCase(sdk: sdk, programID: programID)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Widget Sponsors"
        Row(text: "Widget Sponsors", onClick: {
            let alert = UIAlertController(title: "Widget Settings", message: "Set widgets timeout (s)", preferredStyle: .alert)
            alert.addTextField { textField in
                textField.text = "5"
                textField.keyboardType = .numberPad
            }
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                guard let config = self.createEngagementSDKFromHomeSettings() else {
                    return
                }
                guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                    self.showNoAPITokenAlert()
                    return
                }
                guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                    self.showNoProgramIDAlert()
                    return
                }
                guard let timeout = Int(alert.textFields?[0].text ?? "5") else {
                    print("Couldn't parse textfield as Int")
                    return
                }
                let vc = WidgetSponsorsUseCase(
                    sdk: EngagementSDK(config: config),
                    apiToken: accessToken,
                    programID: programID,
                    theme: Theme(),
                    timeoutSeconds: timeout,
                    apiOrigin: EngagementSDKConfigManager.shared.selectedEnvironment.url
                )
                self.present(vc, animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }),

        // MARK: "Widget Attributes"
        Row(text: "Widget Attributes", onClick: {
            let alert = UIAlertController(title: "Widget Settings", message: "Set widgets timeout (s)", preferredStyle: .alert)
            alert.addTextField { textField in
                textField.text = "5"
                textField.keyboardType = .numberPad
            }
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                guard let config = self.createEngagementSDKFromHomeSettings() else {
                    return
                }
                guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                    self.showNoAPITokenAlert()
                    return
                }
                guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                    self.showNoProgramIDAlert()
                    return
                }
                guard let timeout = Int(alert.textFields?[0].text ?? "5") else {
                    print("Couldn't parse textfield as Int")
                    return
                }
                let vc = WidgetAttributesUseCase(
                    sdk: EngagementSDK(config: config),
                    apiToken: accessToken,
                    programID: programID,
                    theme: Theme(),
                    timeoutSeconds: timeout,
                    apiOrigin: EngagementSDKConfigManager.shared.selectedEnvironment.url
                )
                self.present(vc, animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }),

        Row(text: "Get Widget Interaction History", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else {
                return
            }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                self.showNoProgramIDAlert()
                return
            }
            let sdk = EngagementSDK(config: config)
            let session = sdk.contentSession(config: SessionConfiguration(programID: programID))
            let vc = GetWidgetInteractionHistoryTestSection(session: session)
            self.present(vc, animated: true, completion: nil)
        }),

        Row(text: "Widget Tag UI Enabled", onClick: {
            let alert = UIAlertController(title: "Widget Settings", message: "Set widgets timeout (s)", preferredStyle: .alert)
            alert.addTextField { textField in
                textField.text = "10"
                textField.keyboardType = .numberPad
            }
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                guard let config = self.createEngagementSDKFromHomeSettings() else {
                    return
                }
                guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                    self.showNoAPITokenAlert()
                    return
                }
                guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                    self.showNoProgramIDAlert()
                    return
                }
                guard let timeout = Int(alert.textFields?[0].text ?? "5") else {
                    print("Couldn't parse textfield as Int")
                    return
                }
                let theme = Theme()
                theme.widgetTagMargins = UIEdgeInsets(top: 10, left: 16, bottom: 0, right: 0)
                theme.choiceWidgetTitleMargins = UIEdgeInsets(top: 0, left: 16, bottom: -10, right: 0)
                let titleFont: UIFont = .systemFont(ofSize: 16, weight: .bold)
                theme.widgets.poll.title.font = titleFont
                theme.widgets.quiz.title.font = titleFont
                theme.widgets.prediction.title.font = titleFont

                let vc = WidgetsTestingViewController(
                    sdk: EngagementSDK(config: config),
                    apiToken: accessToken,
                    programID: programID,
                    theme: theme,
                    timeoutSeconds: timeout,
                    apiOrigin: EngagementSDKConfigManager.shared.selectedEnvironment.url
                )

                vc.vcTitle = "Widget Tag UI Enabled"
                vc.vcInfo = "This applies themeing options to display widget tags according to https://app.zeplin.io/project/5bd1da4382809e0a31b9ad50/screen/612392e1c2bd7e16c8fa3c8b"
                self.present(vc, animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }),

        Row(text: "Change Name Before Content Session Chat", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let changeNameAlert = UIAlertController(title: "Enter Name", message: nil, preferredStyle: .alert)
            changeNameAlert.addTextField()
            changeNameAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                guard let name = changeNameAlert.textFields?[0].text else { return }
                let sdk = EngagementSDK(config: config)

                sdk.setUserDisplayName(name) { result in
                    switch result {
                    case .failure(let error):
                        log.dev(error.localizedDescription)
                    case .success:
                        let vc = DefaultChatOnlySection(sdk: sdk, programID: programID, theme: Theme())
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }))
            self.present(changeNameAlert, animated: true, completion: nil)
        }),

        // MARK: "Add User to Chatroom"
        Row(text: "Add User to Chatroom", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                self.showNoProgramIDAlert()
                return
            }
            guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                self.showNoAPITokenAlert()
                return
            }

            let contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
            let vc = AddToChatroomSection(
                sdk: sdk,
                contentSession: contentSession,
                baseURL: config.apiOrigin,
                accessToken: accessToken
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Get List Of Invitations"
        Row(text: "Get List of Invitations", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                self.showNoProgramIDAlert()
                return
            }
            guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                self.showNoAPITokenAlert()
                return
            }

            let contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
            let vc = GetListOfInvitationsSection(
                sdk: sdk,
                contentSession: contentSession,
                baseURL: config.apiOrigin,
                accessToken: accessToken
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Reward Item Queries"
        Row(text: "Reward Item Queries", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let sdk = EngagementSDK(config: config)
            let vc = RewardItemsListUseCase(sdk: sdk, programID: programID)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Reward Item Queries"
        Row(text: "Reward Item Transfers", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let sdk = EngagementSDK(config: config)
            let vc = RewardItemTransfersUseCase(sdk: sdk, programID: programID)
            self.present(vc, animated: true, completion: nil)
        }),

        Row(text: "Mock Number Preds", onClick: {
            let vc = MockedNumberPredictionWidgetSection()
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Block Profiles"
        Row(text: "Block Profiles", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                self.showNoProgramIDAlert()
                return
            }
            guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                self.showNoAPITokenAlert()
                return
            }

            let contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
            let vc = BlockProfilesSection(
                sdk: sdk,
                contentSession: contentSession,
                baseURL: config.apiOrigin,
                accessToken: accessToken
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Reward Transaction Queries"
        Row(text: "Reward Transaction", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let sdk = EngagementSDK(config: config)

            let vc = RewardTransactionsRequestUseCase(sdk: sdk, programID: programID)
            let navigationControllerVC = UINavigationController(rootViewController: vc)
            navigationControllerVC.modalPresentationStyle = .fullScreen
            self.present(navigationControllerVC, animated: true, completion: nil)
        }),

        // MARK: "User Profile Badge List"
        Row(text: "Get Widgets Request", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let sdk = EngagementSDK(config: config)
            let vc = GetWidgetsUseCase(sdk: sdk, programID: programID)
            let navigationControllerVC = UINavigationController(rootViewController: vc)
            navigationControllerVC.modalPresentationStyle = .fullScreen
            self.present(navigationControllerVC, animated: true, completion: nil)
        }),

        // MARK: "Pin Messages In Chat"
        Row(text: "Pin Messages", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            let vc = PinMessagesTestSection(
                envBaseURL: EngagementSDKConfigManager.shared.selectedEnvironment.url,
                sdk: sdk
            )
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Code Redemption"
        Row(text: "Code Redemption", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let sdk = EngagementSDK(config: config)
            let vc = CodeRedemptionUseCase(sdk: sdk, programID: programID)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Query Leaderboard Entries"
        Row(text: "Query Leaderboard Entries", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let sdk = EngagementSDK(config: config)
            let vc = QueryLeaderboardEntriesUseCase(sdk: sdk, programID: programID)
            self.present(vc, animated: true, completion: nil)
        }),

        Row(text: "Quests", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let sdk = EngagementSDK(config: config)
            let vc = QuestsUseCase(sdk: sdk, programID: programID)
            let navigationControllerVC = UINavigationController(rootViewController: vc)
            navigationControllerVC.modalPresentationStyle = .fullScreen
            self.present(navigationControllerVC, animated: true, completion: nil)
        }),

        Row(text: "User Quests", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let sdk = EngagementSDK(config: config)
            let vc = UserQuestsUseCase(sdk: sdk, programID: programID)
            let navigationControllerVC = UINavigationController(rootViewController: vc)
            navigationControllerVC.modalPresentationStyle = .fullScreen
            self.present(navigationControllerVC, animated: true, completion: nil)
        }),

        Row(text: "Quest Automations", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let sdk = EngagementSDK(config: config)
            let vc = QuestAutomationsSections(sdk: sdk)
            self.present(vc, animated: true, completion: nil)
        }),

        Row(text: "Invoke Actions Section", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let sdk = EngagementSDK(config: config)
            let vc = InvokeActionsSection(sdk: sdk)
            self.present(vc, animated: true, completion: nil)
        }),

        Row(text: "Basic EngagementSDK Init", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let vc = BasicEngagementSDKInitSection(clientID: config.clientID, programID: programID)
            self.present(vc, animated: true, completion: nil)
        }),

        Row(text: "Reaction Space", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                self.showNoAPITokenAlert()
                return
            }
            let vc = ReactionSpaceUseCase(sdk: sdk, accessToken: accessToken)
            self.present(vc, animated: true, completion: nil)
        }),

        Row(text: "Reaction Pack", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                self.showNoAPITokenAlert()
                return
            }
            let vc = ReactionPackUseCase(sdk: sdk, accessToken: accessToken)
            self.present(vc, animated: true, completion: nil)
        }),

        Row(text: "Chat Message Metadata Tests", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
            contentSession.getChatSession { result in
                switch result {
                case .success(let chatSession):
                    let vc = ChatMessageMetadataTests(chatSession: chatSession)
                    self.present(vc, animated: false, completion: nil)
                case .failure:
                    return
                }
            }
        }),

        // MARK: "Get Widget Interaction"
        Row(text: "Get Widget Interactions", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let sdk = EngagementSDK(config: config)

            let vc = WidgetInteractionHistoryUseCase(sdk: sdk, programID: programID)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Query Leaderboard Entries"
        Row(text: "Profile Leaderboard Entries", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else {
                return self.showNoProgramIDAlert()
            }
            let sdk = EngagementSDK(config: config)
            let vc = LeaderboardProfileViewsUseCase(sdk: sdk, programID: programID)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Comment Board CRUD"
        Row(text: "Comment Board CRUD", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                self.showNoAPITokenAlert()
                return
            }
            let vc = CommentBoardUseCase(sdk: sdk, accessToken: accessToken)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Comments Use Case"
        Row(text: "Comments Use Case", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                self.showNoAPITokenAlert()
                return
            }
            let vc = CommentsUseCase(sdk: sdk, accessToken: accessToken)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Comment Board Bans Use Case"
        Row(text: "Comment Board Bans", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                self.showNoAPITokenAlert()
                return
            }
            let vc = CommentBoardBansUseCase(sdk: sdk, accessToken: accessToken)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Profile Relationship Use Case"
        Row(text: "Profile Relationship Use Case", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            let vc = ProfileRelationshipUseCase(sdk: sdk)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Profile Relationship Types Use Case"
        Row(text: "Profile Relationship Types Use Case", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            let vc = ProfileRelationshipTypeUseCase(sdk: sdk)
            self.present(vc, animated: true, completion: nil)
        }),

        // MARK: "Presence Client Use Case"
        Row(text: "Presence Client Use Case", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            let vc = PresenceUseCase(sdk: sdk)
            self.present(vc, animated: true, completion: nil)
        }),

        Row(text: "Friends List (Presence)", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else {
                return
            }
            guard let programID = EngagementSDKConfigManager.shared.selectedProgram?.id else { return }
            if #available(iOS 13.0, *) {
                let vc = FriendsListUseCase(livelike: EngagementSDK(config: config))
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }

        }),

        Row(text: "Report Comments", onClick: {
            guard let config = self.createEngagementSDKFromHomeSettings() else { return }
            let sdk = EngagementSDK(config: config)
            guard let accessToken = EngagementSDKConfigManager.shared.getAPIToken(for: EngagementSDKConfigManager.shared.selectedEnvironment) else {
                self.showNoAPITokenAlert()
                return
            }
            let vc = CommentReportsUseCase(sdk: sdk, accessToken: accessToken)
            self.present(vc, animated: true, completion: nil)
        }),
    ])

    // MARK: -

    private var sections: [Section] {
        return [
            configSection,
            useCaseSection
        ]
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        EngagementSDKConfigManager.shared.delegates.append(WeakBox(self))

        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "myCell")
        tableView.dataSource = self
        tableView.delegate = self
        view.addSubview(tableView)

        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }

    private enum chatType {
        case contentSession
        case chatSession
    }

    private func showAvatarPicker(completion: @escaping (URL?) -> Void) {
        let alert = UIAlertController(
            title: "Pick Image",
            message: "Pick the image that suits your personality",
            preferredStyle: .alert
        )

        alert.addAction(UIAlertAction(title: "Beiber", style: .default, handler: { _ in
            completion(URL(string: "https://emoji.slack-edge.com/T04G26FHU/bieber/313115e5e141411b.png"))
        }))

        alert.addAction(UIAlertAction(title: "Doom", style: .default, handler: { _ in
            completion(URL(string: "https://emoji.slack-edge.com/T04G26FHU/godmode/1bd6476fbb.png"))
        }))

        alert.addAction(UIAlertAction(title: "Steve Ballmer", style: .default, handler: { _ in
            completion(URL(string: "https://emoji.slack-edge.com/T04G26FHU/swag/7ea1a176eeeb9a4c.jpg"))
        }))

        alert.addAction(UIAlertAction(title: "Borat", style: .default, handler: { _ in
            completion(URL(string: "https://emoji.slack-edge.com/T04G26FHU/very_nice/46839ae081839616.jpg"))
        }))

        alert.addAction(UIAlertAction(title: "Bad Avatar URL", style: .default, handler: { _ in
            completion(URL(string: "https://emoji.slack-edge.com/T04G26FHU/very_nice/46839ae08183961.jpg"))
        }))

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        present(alert, animated: true, completion: nil)
    }

    private func showChatTypePicker(completion: @escaping (chatType) -> Void) {
        let alert = UIAlertController(title: "Pick Chat Type", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Content Session", style: .default, handler: { _ in
            completion(.contentSession)
        }))
        alert.addAction(UIAlertAction(title: "Chat Session", style: .default, handler: { _ in
            completion(.chatSession)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }

    private func showChatContentFilterPicker(completion: @escaping (Bool) -> Void) {
        let alert = UIAlertController(title: "Chat Includes Filtered Messages", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "True", style: .default, handler: { _ in
            completion(true)
        }))
        alert.addAction(UIAlertAction(title: "False", style: .default, handler: { _ in
            completion(false)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }

    func showNoAPITokenAlert() {
        let alert = UIAlertController(
            title: "No API Token",
            message: "Set an API Token for \(EngagementSDKConfigManager.shared.selectedEnvironment.name) environment.",
            preferredStyle: .alert
        )
        alert.addAction(.init(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }

    func showNoProgramIDAlert() {
        let alert = UIAlertController(
            title: "No Program ID",
            message: "You need to select a program first.",
            preferredStyle: .alert
        )
        alert.addAction(.init(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }

    func showNoAppAlert() {
        let alert = UIAlertController(
            title: "No App",
            message: "You need to select an app first.",
            preferredStyle: .alert
        )
        alert.addAction(.init(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }

    private func createEngagementSDKFromHomeSettings() -> EngagementSDKConfig? {
        guard let clientID = EngagementSDKConfigManager.shared.selectedClientApp?.id else {
            showNoAppAlert()
            return nil
        }
        var config = EngagementSDKConfig(clientID: clientID)
        config.apiOrigin = EngagementSDKConfigManager.shared.selectedEnvironment.url
        config.accessTokenStorage = EngagementSDKConfigManager.shared
        return config
    }
}

extension HomeViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            if let filter = useCaseFilter {
                return sections[section].rows.filter { $0.getText().lowercased().contains(filter.lowercased()) }.count
            }
        }

        return sections[section].rows.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "myCell") else {
            assertionFailure()
            return UITableViewCell()
        }

        if indexPath.section == 1 {
            if let filter = useCaseFilter {
                cell.textLabel?.text = sections[indexPath.section].rows.filter { $0.getText().lowercased().contains(filter.lowercased()) }[indexPath.row].getText()
                return cell
            }
        }

        cell.textLabel?.text = sections[indexPath.section].rows[indexPath.row].getText()
        return cell
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var row = sections[indexPath.section].rows[indexPath.row]

        if indexPath.section == 1 {
            if let filter = useCaseFilter {
                row = sections[indexPath.section].rows.filter { $0.getText().lowercased().contains(filter.lowercased()) }[indexPath.row]
            }
        }

        row.onClick()
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].text
    }
}

extension HomeViewController: EngagementSDKConfigDelegate {
    func apiEndpointDidChange(apiEnvironment: Environment, clientApp: ClientApp?) {
        tableView.reloadData()
    }

    func selectedProgramDidChange(program: Program?, programNumber: Int) {
        tableView.reloadData()
    }

    func didBeginLoading() {
        tableView.alpha = 0.5
        tableView.allowsSelection = false
    }

    func didFinishLoading() {
        tableView.alpha = 1
        tableView.allowsSelection = true
    }
}

class DocumentPickerDelegate: NSObject, UIDocumentPickerDelegate {
    var didPickDocumentsAtCompletion: (([URL]) -> Void)?

    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        didPickDocumentsAtCompletion?(urls)
    }
}
