//
//  WidgetThemeRandomizeSection.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 6/10/20.
//

import EngagementSDK
import UIKit

class WidgetThemeRandomizeSection: TestCaseViewController {
    private var sdk: EngagementSDK!
    private var session: ContentSession!

    private let clientID: String
    private let programID: String
    private let apiToken: String
    private let timeoutSeconds: Int
    private let apiOrigin: URL

    private let widgetViewController: WidgetPopupViewController = {
        let vc = WidgetPopupViewController()
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        return vc
    }()

    private lazy var widgetCreateView: WidgetCreateView = {
        let view = WidgetCreateView(
            programID: self.programID,
            apiToken: self.apiToken,
            timeoutSeconds: self.timeoutSeconds,
            apiOrigin: self.apiOrigin
        )
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private let randomizeAndApplyButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Randomize Theme and Apply", for: .normal)
        return button
    }()

    init(
        clientID: String,
        programID: String,
        apiToken: String,
        apiOrigin: URL,
        timeoutSeconds: Int
    ) {
        self.clientID = clientID
        self.programID = programID
        self.apiToken = apiToken
        self.apiOrigin = apiOrigin
        self.timeoutSeconds = timeoutSeconds
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        vcTitle = "Widget Theme Randomizer"
        vcInfo = """
        This section is designed to visualize many combinations of theme properties.
        Also it can be used to test how widget behave when you apply a theme in the middle of its lifecycle.
        """

        randomizeAndApplyButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)

        addChild(widgetViewController)
        widgetViewController.didMove(toParent: self)

        view.addSubview(widgetViewController.view)
        view.addSubview(widgetCreateView)
        view.addSubview(randomizeAndApplyButton)

        NSLayoutConstraint.activate([
            widgetViewController.view.topAnchor.constraint(equalTo: contentView.topAnchor),
            widgetViewController.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetViewController.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            widgetViewController.view.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.5),

            widgetCreateView.topAnchor.constraint(equalTo: widgetViewController.view.bottomAnchor),
            widgetCreateView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetCreateView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            widgetCreateView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.4),

            randomizeAndApplyButton.topAnchor.constraint(equalTo: widgetCreateView.bottomAnchor),
            randomizeAndApplyButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            randomizeAndApplyButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            randomizeAndApplyButton.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.1)
        ])

        var config = EngagementSDKConfig(clientID: clientID)
        config.apiOrigin = apiOrigin
        sdk = EngagementSDK(config: config)
        session = sdk.contentSession(config: SessionConfiguration(programID: programID))

        widgetViewController.session = session
    }

    @objc private func buttonPressed() {
        widgetViewController.setTheme(Theme.random())
    }
}

private extension UIColor {
    static func random() -> UIColor {
        let redValue = CGFloat(arc4random_uniform(255)) / 255.0
        let greenValue = CGFloat(arc4random_uniform(255)) / 255.0
        let blueValue = CGFloat(arc4random_uniform(255)) / 255.0
        let alphaValue = CGFloat(arc4random_uniform(255)) / 255.0

        return UIColor(red: redValue, green: greenValue, blue: blueValue, alpha: alphaValue)
    }
}

private extension UIFont {
    static func random() -> UIFont {
        return .systemFont(ofSize: CGFloat.random(in: 5 ... 20))
    }
}

private extension Theme.Background {
    static func random() -> Theme.Background {
        return Bool.random() ? randomFill() : randomGradient()
    }

    static func randomFill() -> Theme.Background {
        return .fill(color: .random())
    }

    static func randomGradient() -> Theme.Background {
        return .gradient(
            gradient: Theme.Background.Gradient(
                colors: [.random(), .random()],
                start: CGPoint(x: 0, y: 0),
                end: CGPoint(x: 1, y: 1)
            )
        )
    }
}

private extension Theme {
    static func random() -> Theme {
        let theme = Theme()

        theme.widgets.quiz = .random()
        theme.widgets.prediction = .random()
        theme.widgets.poll = .random()
        theme.widgets.alert = .random()
        theme.widgets.videoAlert = .random()
        theme.widgets.textAsk = .random()
        theme.widgets.numberPrediction = .random()
        theme.widgets.imageSlider = .random()
        theme.widgets.cheerMeter = .random()

        return theme
    }

    static func randomBorderWidth() -> CGFloat {
        return CGFloat.random(in: 1 ... 3)
    }

    static func randomCornerRadius() -> CGFloat {
        return CGFloat.random(in: 0 ... 5)
    }
}

private extension Theme.ChoiceWidget {
    static func random() -> Theme.ChoiceWidget {
        return Theme.ChoiceWidget(
            main: .random(),
            header: .random(),
            body: .random(),
            title: .random(),
            footer: .random(),
            correctOption: .random(),
            incorrectOption: .random(),
            selectedOption: .random(),
            unselectedOption: .random(),
            submitButton: .random()
        )
    }
}

private extension Theme.Text {
    static func random() -> Theme.Text {
        return Theme.Text(
            color: .random(),
            font: .random()
        )
    }
}

private extension Theme.Container {
    static func random() -> Theme.Container {
        return Theme.Container(
            background: .random(),
            borderColor: .random(),
            borderWidth: Theme.randomBorderWidth(),
            cornerRadii: .random()
        )
    }
}

private extension Theme.CornerRadii {
    static func random() -> Theme.CornerRadii {
        return Theme.CornerRadii(
            topLeft: Theme.randomCornerRadius(),
            topRight: Theme.randomCornerRadius(),
            bottomLeft: Theme.randomCornerRadius(),
            bottomRight: Theme.randomCornerRadius()
        )
    }
}

private extension Theme.ChoiceWidget.Option {
    static func random() -> Theme.ChoiceWidget.Option {
        return Theme.ChoiceWidget.Option(
            container: .random(),
            description: .random(),
            percentage: .random(),
            progressBar: .random()
        )
    }
}

private extension Theme.ProgressBar {
    static func random() -> Theme.ProgressBar {
        return Theme.ProgressBar(
            background: .random(),
            borderColor: .random(),
            borderWidth: Theme.randomBorderWidth(),
            cornerRadii: .random()
        )
    }
}

private extension Theme.AlertWidget {
    static func random() -> Theme.AlertWidget {
        return Theme.AlertWidget(
            main: .random(),
            header: .random(),
            title: .random(),
            body: .random(),
            description: .random(),
            footer: .random(),
            link: .random()
        )
    }
}

private extension Theme.VideoAlertWidget {
    static func random() -> Theme.VideoAlertWidget {
        return Theme.VideoAlertWidget(
            main: .random(),
            header: .random(),
            title: .random(),
            body: .random(),
            description: .random(),
            footer: .random(),
            link: .random()
        )
    }
}

private extension Theme.TextAskWidget {
    static func random() -> Theme.TextAskWidget {
        return Theme.TextAskWidget(
            main: .random(),
            header: .random(),
            body: .random(),
            title: .random(),
            prompt: .random(),
            confirmation: .random(),
            submitButtonEnabled: .random(),
            submitTextEnabled: .random(),
            submitButtonDisabled: .random(),
            submitTextDisabled: .random(),
            placeholder: .random(),
            response: .random(),
            responseTextDisabled: .random(),
            textView: .random(),
            characterCount: .random()
        )
    }
}

private extension Theme.NumberPredictionWidget {
    static func random() -> Theme.NumberPredictionWidget {
        return .init(
            main: .random(),
            header: .random(),
            title: .random(),
            body: .random(),
            footer: .random(),
            optionContainer: .random(),
            optionText: .random(),
            optionInputFieldContainerEnabled: .random(),
            optionInputFieldContainerDisabled: .random(),
            optionInputFieldTextEnabled: .random(),
            optionInputFieldTextDisabled: .random(),
            optionInputFieldPlaceholder: .random(),
            submitButton: .random(),
            correctOptionContainer: .random(),
            incorrectOptionContainer: .random(),
            correctOptionText: .random(),
            incorrectOptionText: .random()
        )
    }
}

private extension Theme.SubmitButton {
    static func random() -> Theme.SubmitButton {
        var randomSubmitButton = Theme.SubmitButton()
        randomSubmitButton.buttonDisabled = .random()
        randomSubmitButton.textDisabled = .random()
        randomSubmitButton.buttonEnabled = .random()
        randomSubmitButton.textEnabled = .random()
        randomSubmitButton.confirmation = .random()
        return randomSubmitButton
    }
}

private extension Theme.ImageSlider {
    static func random() -> Theme.ImageSlider {
        var theme = Theme.ImageSlider()
        theme.main = .random()
        theme.header = .random()
        theme.title = .random()
        theme.body = .random()
        theme.footer = .random()
        theme.interactiveTrackLeft = .random()
        theme.interactiveTrackRight = .random()
        theme.resultsTrackLeft = .random()
        theme.resultsTrackRight = .random()
        theme.submitButton = .random()
        return theme
    }
}

private extension Theme.CheerMeter {
    static func random() -> Theme.CheerMeter {

        var theme = Theme.CheerMeter()
        theme.main = .random()
        theme.header = .random()
        theme.title = .random()
        theme.body = .random()
        theme.score = .random()
        theme.versus = .random()
        theme.sideABar = .random()
        theme.sideAText = .random()
        theme.sideAButton = .random()
        theme.sideBBar = .random()
        theme.sideBText = .random()
        theme.sideBButton = .random()
        return theme
    }
}
