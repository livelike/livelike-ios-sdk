//
//  RewardItemsListUseCase.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 9/17/21.
//

import UIKit
import EngagementSDK

class RewardItemsListUseCase: TestCaseViewController {

    struct RewardItemsData {
        var rewardItem: RewardItem
        var rewardBalance: Int
    }

    private var contentSession: ContentSession!
    private var sdk: EngagementSDK
    private var items: [RewardItemsData] = []
    private var requestOptions = GetApplicationRewardItemsRequestOptions()

    private let autoClaimEnabledLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let submitAttributesBTN: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Submit Attributes", for: .normal)
        return button
    }()

    private let attributesInput: UITextField = {
        let attributesInput: UITextField = UITextField()
        attributesInput.placeholder = "Enter attributes Separated by a '|' and ':' ex.key1:val1|key2:val2"
        attributesInput.translatesAutoresizingMaskIntoConstraints = false
        attributesInput.borderStyle = .line
        attributesInput.font = UIFont.systemFont(ofSize: 10.0)
        return attributesInput
    }()

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.allowsSelection = true
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "myCell")
        return tableView
    }()

    private let loadMoreButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Load More", for: .normal)
        return button
    }()

    init(sdk: EngagementSDK, programID: String) {
        self.sdk = sdk
        self.contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "Application Reward Items"

        super.viewDidLoad()

        view.addSubview(autoClaimEnabledLabel)
        view.addSubview(attributesInput)
        view.addSubview(submitAttributesBTN)
        view.addSubview(tableView)
        view.addSubview(loadMoreButton)

        NSLayoutConstraint.activate([

            autoClaimEnabledLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            autoClaimEnabledLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0),
            autoClaimEnabledLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            autoClaimEnabledLabel.heightAnchor.constraint(equalToConstant: 50.0),

            attributesInput.topAnchor.constraint(equalTo: autoClaimEnabledLabel.bottomAnchor),
            attributesInput.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10.0),
            attributesInput.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10.0),
            attributesInput.heightAnchor.constraint(equalToConstant: 50.0),

            submitAttributesBTN.topAnchor.constraint(equalTo: attributesInput.bottomAnchor),
            submitAttributesBTN.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            submitAttributesBTN.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            submitAttributesBTN.heightAnchor.constraint(equalToConstant: 60.0),

            tableView.topAnchor.constraint(equalTo: submitAttributesBTN.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: loadMoreButton.topAnchor),

            loadMoreButton.topAnchor.constraint(equalTo: tableView.bottomAnchor),
            loadMoreButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            loadMoreButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            loadMoreButton.heightAnchor.constraint(equalToConstant: 60.0),
            loadMoreButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])

        submitAttributesBTN.addTarget(self, action: #selector(submitAttributesBTNSelected), for: .touchUpInside)
        loadMoreButton.addTarget(self, action: #selector(loadMoreButtonSelected), for: .touchUpInside)

        tableView.delegate = self
        tableView.dataSource = self

        sdk.rewards.delegate = self

        loadInitialData()
    }

    private func loadInitialData() {
        self.items.removeAll()
        sdk.getAutoclaimInteractionRewardsStatus(completion: { result in
            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(let status):
                self.autoClaimEnabledLabel.text = String(describing: status)
            }
        })

        sdk.rewards.getApplicationRewardItems(
            page: .first,
            options: requestOptions
        ) { [weak self] result in

            guard let self = self else { return }

            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(let rewardItems):
                let rewardItemIDs = rewardItems.map { $0.id }

                self.sdk.rewards.getRewardItemBalances(
                    page: .first,
                    rewardItemIDs: rewardItemIDs
                ) { result in

                    switch result {
                    case .failure(let error):
                        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                        self.present(alert, animated: true, completion: nil)
                    case .success(let balances):
                        balances.forEach { balance in
                            if let matchedRewardItem = rewardItems.first(where: { $0.id == balance.rewardItemID}) {
                                let data = RewardItemsData(
                                    rewardItem: matchedRewardItem,
                                    rewardBalance: balance.rewardItemBalance
                                )

                                self.items.append(data)
                            }
                        }

                        DispatchQueue.main.async {

                            self.tableView.reloadData()
                        }
                    }
                }

            }
        }
    }

    @objc private func submitAttributesBTNSelected() {
        var stringAttributes = [String]()
        var attributes = [RewardItemAttribute]()

        if let text = attributesInput.text {
            let attrInput = text.replacingOccurrences(of: " ", with: "").lowercased()
            if attrInput.count > 1 {
                stringAttributes = attrInput.components(separatedBy: "|")
            }

            stringAttributes.forEach { attribute in
                let attr = attribute.components(separatedBy: ":")
                attributes.append(RewardItemAttribute(key: attr[0], value: attr[1]))
            }

            requestOptions = GetApplicationRewardItemsRequestOptions(attributes: attributes)
            loadInitialData()
        }
    }

    @objc private func loadMoreButtonSelected() {
        sdk.rewards.getApplicationRewardItems(
            page: .next
        ) { result in

            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(_):
                self.items.removeAll()
                // self.items.append(contentsOf: receivedProfileBadges)
                self.tableView.reloadData()
            }
        }
    }
}

extension RewardItemsListUseCase: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            var cell = tableView.dequeueReusableCell(withIdentifier: "myCell")
        else {
            return UITableViewCell()
        }

        cell = UITableViewCell(style: .subtitle, reuseIdentifier: "myCell")
        let rewardItem = self.items[indexPath.row]
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        cell.textLabel?.text = "Balance: \(rewardItem.rewardBalance) | Name: \(rewardItem.rewardItem.name) | Img Count: \(String(describing: rewardItem.rewardItem.images.count))"
        log.dev("\(rewardItem.rewardItem.images.count)")

        var attributes = "attr: "
        rewardItem.rewardItem.attributes.forEach { attribute in
            attributes += "key: \(attribute.key), val: \(attribute.value) | "
        }

        cell.detailTextLabel?.text = attributes
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let rewardItem = self.items[indexPath.row]

        let alert = UIAlertController(
            title: "Send Reward Item Amount",
            message: "Send Reward Item Amount",
            preferredStyle: .alert
        )

        alert.addTextField { textField in
            textField.placeholder = "Amount"
            textField.keyboardType = .numberPad
        }
        alert.addTextField { textField in
            textField.placeholder = "Receiver Profile ID"
            textField.keyboardType = .default
        }

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }

            guard let amount = Int(alert.textFields?[0].text ?? "5"),
                  let receiverProfileID = alert.textFields?[1].text
            else {
                print("Couldn't parse textfield as Int")
                return
            }

            self.sdk.rewards.transferRewardItemAmount(
                amount,
                recipientProfileID: receiverProfileID,
                rewardItemID: rewardItem.rewardItem.id
            ) { result in

                switch result {
                case .failure(let error):
                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                    self.present(alert, animated: true, completion: nil)
                case .success(let rewardItemTransfer):

                    let alert = UIAlertController(title: "Success", message: "Transfer ID: \(rewardItemTransfer.id)", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak self] _ in
                        guard let self = self else { return }
                        self.tableView.reloadData()
                    }))
                    self.present(alert, animated: true, completion: nil)
                }

            }

        }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension RewardItemsListUseCase: RewardsClientDelegate {
    func rewardsClient(_ rewardsClient: RewardsClient, userDidReceiveRewardItemTransfer rewardItemTransfer: RewardItemTransfer) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let alert = UIAlertController(
                title: "\(rewardItemTransfer.recipientProfileName), you Received Items!",
                message: """
                Reward Amount: \(rewardItemTransfer.rewardItemAmount)
                Reward Item: \(rewardItemTransfer.rewardItemName)
                From: \(rewardItemTransfer.senderProfileName)
                """,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
