//
//  RewardTransactionsUseCase.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 11/30/21.
//

import UIKit
import EngagementSDK

class RewardTransactionsUseCase: TestCaseViewController {

    private var contentSession: ContentSession!
    private var sdk: EngagementSDK
    private var items: [RewardTransaction] = []

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.allowsSelection = true
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "myCell")
        return tableView
    }()

    private let loadMoreButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Load More", for: .normal)
        return button
    }()

    private let options: RewardTransactionsRequestOptions

    init(sdk: EngagementSDK, programID: String, options: RewardTransactionsRequestOptions) {
        self.sdk = sdk
        self.contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
        self.options = options
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "Reward Transactions"

        super.viewDidLoad()

        view.addSubview(tableView)
        view.addSubview(loadMoreButton)

        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: contentView.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: loadMoreButton.topAnchor),

            loadMoreButton.topAnchor.constraint(equalTo: tableView.bottomAnchor),
            loadMoreButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            loadMoreButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            loadMoreButton.heightAnchor.constraint(equalToConstant: 60.0),
            loadMoreButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])

        loadMoreButton.addTarget(self, action: #selector(loadMoreButtonSelected), for: .touchUpInside)

        tableView.delegate = self
        tableView.dataSource = self

        sdk.rewards.getRewardTransactions(
            page: .first,
            options: options
        ) { [weak self] result in
            guard let self = self else { return }

            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(let rewardItems):

                if rewardItems.count == 0 {
                    let alert = UIAlertController(title: ":(", message: "Empty Result Set", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                    self.present(alert, animated: true, completion: nil)
                }

                self.items.append(contentsOf: rewardItems)

                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }

            }
        }
    }

    @objc private func loadMoreButtonSelected() {
        sdk.rewards.getRewardTransactions(
            page: .next,
            options: options
        ) { [weak self] result in
            guard let self = self else { return }

            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                self.present(alert, animated: true, completion: nil)
            case .success(let rewardItems):
                self.items.append(contentsOf: rewardItems)

                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }

            }
        }
    }
}

extension RewardTransactionsUseCase: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            var cell = tableView.dequeueReusableCell(withIdentifier: "myCell")
        else {
            return UITableViewCell()
        }

        cell = UITableViewCell(style: .subtitle, reuseIdentifier: "myCell")

        let rewardItem = self.items[indexPath.row]
        cell.textLabel?.text = "Reward Item Name: \(rewardItem.rewardItemName)"
        cell.detailTextLabel?.text = "Amount: \(rewardItem.rewardItemAmount) | Trans ID: \(rewardItem.id)"

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

class RewardTransactionsRequestUseCase: TestCaseViewController {

    private var contentSession: ContentSession!
    private var sdk: EngagementSDK
    private var items: [WidgetKind] = WidgetKind.allCases
    var selectedItems: Set<WidgetKind> = Set()

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.allowsSelection = true
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "myCell")
        return tableView
    }()

    private let submitRewardTransactionsButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Submit Request", for: .normal)
        return button
    }()

    private let widgetIDInput: UITextField = {
        let profileInput: UITextField = UITextField()
        profileInput.placeholder = "Enter Widget IDs Separated by a comma"
        profileInput.translatesAutoresizingMaskIntoConstraints = false
        profileInput.borderStyle = .line
        profileInput.font = UIFont.systemFont(ofSize: 10.0)
        return profileInput
    }()

    private let profileIDsInput: UITextField = {
        let profileInput: UITextField = UITextField()
        profileInput.placeholder = "Enter Profile IDs Separated by a comma"
        profileInput.translatesAutoresizingMaskIntoConstraints = false
        profileInput.borderStyle = .line
        profileInput.font = UIFont.systemFont(ofSize: 10.0)
        return profileInput
    }()

    private let rewardItemIDsInput: UITextField = {
        let profileInput: UITextField = UITextField()
        profileInput.placeholder = "Enter Reward Item IDs Separated by a comma"
        profileInput.translatesAutoresizingMaskIntoConstraints = false
        profileInput.borderStyle = .line
        profileInput.font = UIFont.systemFont(ofSize: 10.0)
        return profileInput
    }()

    private let rewardActionKeysInput: UITextField = {
        let profileInput: UITextField = UITextField()
        profileInput.placeholder = "Enter Reward Action Keys Separated by a comma"
        profileInput.translatesAutoresizingMaskIntoConstraints = false
        profileInput.borderStyle = .line
        profileInput.font = UIFont.systemFont(ofSize: 10.0)
        return profileInput
    }()

    private let textfieldInputStack: UIStackView = {
        let buttonStack = UIStackView()
        buttonStack.translatesAutoresizingMaskIntoConstraints = false
        buttonStack.axis = .vertical
        buttonStack.distribution = .fillEqually
        buttonStack.spacing = 10.0
        return buttonStack
    }()

    private let createdSinceStack: UIStackView = {
        let buttonStack = UIStackView()
        buttonStack.translatesAutoresizingMaskIntoConstraints = false
        buttonStack.axis = .horizontal
        buttonStack.distribution = .fillEqually
        return buttonStack
    }()

    private let createSinceLabel: UILabel = {
        let label = UILabel()
        label.text = "Created Since:"
        label.textColor = .black
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let createdSincePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        return datePicker
    }()

    private let createdUntilStack: UIStackView = {
        let buttonStack = UIStackView()
        buttonStack.translatesAutoresizingMaskIntoConstraints = false
        buttonStack.axis = .horizontal
        buttonStack.distribution = .fillEqually
        return buttonStack
    }()

    private let createUntilLabel: UILabel = {
        let label = UILabel()
        label.text = "Created Until:"
        label.textColor = .black
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let createdUntilPicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        return datePicker
    }()

    private let programID: String

    init(sdk: EngagementSDK, programID: String) {
        self.sdk = sdk
        self.programID = programID
        self.contentSession = sdk.contentSession(config: SessionConfiguration(programID: programID))
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "Create Reward Transaction Request"

        super.viewDidLoad()

        textfieldInputStack.addArrangedSubview(widgetIDInput)
        textfieldInputStack.addArrangedSubview(profileIDsInput)
        textfieldInputStack.addArrangedSubview(rewardItemIDsInput)
        textfieldInputStack.addArrangedSubview(rewardActionKeysInput)
        textfieldInputStack.addArrangedSubview(createdSincePicker)
        textfieldInputStack.addArrangedSubview(createdUntilPicker)

        createdSinceStack.addArrangedSubview(createSinceLabel)
        createdSinceStack.addArrangedSubview(createdSincePicker)
        textfieldInputStack.addArrangedSubview(createdSinceStack)

        createdUntilStack.addArrangedSubview(createUntilLabel)
        createdUntilStack.addArrangedSubview(createdUntilPicker)
        textfieldInputStack.addArrangedSubview(createdUntilStack)

        view.addSubview(textfieldInputStack)
        view.addSubview(tableView)
        view.addSubview(submitRewardTransactionsButton)

        NSLayoutConstraint.activate([
            textfieldInputStack.topAnchor.constraint(equalTo: contentView.topAnchor),
            textfieldInputStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            textfieldInputStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            textfieldInputStack.heightAnchor.constraint(equalToConstant: 250),

            tableView.topAnchor.constraint(equalTo: textfieldInputStack.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: submitRewardTransactionsButton.topAnchor),

            submitRewardTransactionsButton.topAnchor.constraint(equalTo: tableView.bottomAnchor),
            submitRewardTransactionsButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            submitRewardTransactionsButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            submitRewardTransactionsButton.heightAnchor.constraint(equalToConstant: 60.0),
            submitRewardTransactionsButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])

        submitRewardTransactionsButton.addTarget(self, action: #selector(submitRewardTransactionsButtonSelected), for: .touchUpInside)

        tableView.delegate = self
        tableView.dataSource = self
    }

    @objc private func submitRewardTransactionsButtonSelected() {
        let widgetIDs: Set<String>? = widgetIDInput.getCommaSeparatedValues()
        let profileIDs: Set<String>? = profileIDsInput.getCommaSeparatedValues()
        let rewardItemIDs: Set<String>? = rewardItemIDsInput.getCommaSeparatedValues()
        let rewardActionKeys: Set<String>? = rewardActionKeysInput.getCommaSeparatedValues()

        let request = RewardTransactionsRequestOptions(
            widgetIDs: widgetIDs,
            widgetKind: selectedItems.count == 0 ? nil : Set(selectedItems),
            profileIDs: profileIDs,
            rewardItemIDs: rewardItemIDs,
            rewardActionKeys: rewardActionKeys,
            createdSince: createdSincePicker.date,
            createdUntil: createdUntilPicker.date
        )

        self.navigationController?.pushViewController(
            RewardTransactionsUseCase(
                sdk: sdk,
                programID: programID,
                options: request
            ),
            animated: true
        )
    }
}

extension RewardTransactionsRequestUseCase: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "myCell")
        else {
            return UITableViewCell()
        }

        let rewardItem = self.items[indexPath.row]
        cell.textLabel?.text = "\(rewardItem.displayName)"

        let kind = items[indexPath.row]
        if selectedItems.contains(kind) {
            cell.accessoryType = UITableViewCell.AccessoryType.checkmark
        } else {
            cell.accessoryType = UITableViewCell.AccessoryType.none
        }

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let kind = items[indexPath.row]
        if selectedItems.contains(kind) {
            selectedItems.remove(kind)
        } else {
            selectedItems.insert(kind)
        }
        self.widgetIDInput.endEditing(true)
        tableView.reloadData()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.widgetIDInput.endEditing(true)
    }
}

extension UITextField {
    func getCommaSeparatedValues() -> Set<String>? {
        if let text = self.text {
            if text.count > 1 {
                return Set(text.components(separatedBy: ",").filter({ $0.count > 0}))
            }
        }

        return nil
    }
}
