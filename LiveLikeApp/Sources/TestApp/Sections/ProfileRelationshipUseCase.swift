//
//  ProfileRelationshipUseCase.swift
//  LiveLikeTestApp
//
//  Created by Mike Moloksher on 2/24/23.
//

import Foundation
import EngagementSDK
import UIKit

class ProfileRelationshipUseCase: TestCaseViewController {
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 30
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let buttonStack1: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let currentProfileIDLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "Current Profile ID"
        return label
    }()
    
    private let currentProfileID: UITextView = {
        let textView = UITextView()
        textView.textColor = .black
        return textView
    }()
    
    private let relationshipTypeTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Relationship Type Key"
        textField.borderStyle = .roundedRect
        return textField
    }()
    
    private let toProfileIDTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "To Profile ID"
        textField.borderStyle = .roundedRect
        return textField
    }()
    
    private let fromProfileIDTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "From Profile ID"
        textField.borderStyle = .roundedRect
        return textField
    }()
    
    private let relationshipIDTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = .black
        textField.placeholder = "Relationship ID to use for Deletion"
        textField.borderStyle = .roundedRect
        return textField
    }()
    
    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()
    
    private let getRelationshipListButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get Relationships", for: .normal)
        return button
    }()
    
    private let createRelationshipButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Create Relationship", for: .normal)
        return button
    }()
    
    private let deleteRelationshipButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Delete Relationship", for: .normal)
        return button
    }()

    private let sdk: EngagementSDK
    private var commentClient: CommentClient?
    
    private var profileRelationships = [ProfileRelationship]()

    init(sdk: EngagementSDK) {
        self.sdk = sdk
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vcTitle = "Comments Use Case"
        
        tableView.delegate = self
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        
        self.getRelationshipListButton.addTarget(self, action: #selector(self.getRelationshipList), for: .touchUpInside)
        self.createRelationshipButton.addTarget(self, action: #selector(self.createRelationship), for: .touchUpInside)
        self.deleteRelationshipButton.addTarget(self, action: #selector(self.deleteRelationship), for: .touchUpInside)
        
        contentView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            stackView.bottomAnchor.constraint(equalTo: contentView.safeBottomAnchor, constant: -20),
        ])
        
        buttonStack1.addArrangedSubview(currentProfileIDLabel)
        buttonStack1.addArrangedSubview(currentProfileID)
        buttonStack1.addArrangedSubview(relationshipTypeTextField)
        buttonStack1.addArrangedSubview(fromProfileIDTextField)
        buttonStack1.addArrangedSubview(toProfileIDTextField)
        buttonStack1.addArrangedSubview(relationshipIDTextField)
        buttonStack1.addArrangedSubview(getRelationshipListButton)
        buttonStack1.addArrangedSubview(createRelationshipButton)
        buttonStack1.addArrangedSubview(deleteRelationshipButton)
        
        stackView.addArrangedSubview(buttonStack1)
        stackView.addArrangedSubview(tableView)
        
        sdk.getCurrentUserProfileID { result in
            switch result {
            case .success(let id):
                self.currentProfileID.text = id
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func getRelationshipList() {
        
        self.sdk.socialGraphClient.getProfileRelationships(
            page: .first,
            options: GetProfileRelationshipsOptions(
                relationshipTypeKey: relationshipTypeTextField.text?.lowercased(),
                fromProfileID: fromProfileIDTextField.text?.count == 0 ? nil : fromProfileIDTextField.text,
                toProfileID: toProfileIDTextField.text?.count == 0 ? nil : toProfileIDTextField.text
            ),
            completion: { result in
                switch result {
                case .success(let relationships):
                    log.dev("found: \(relationships.count) relationships")
                    DispatchQueue.main.async {
                        self.profileRelationships.removeAll()
                        self.profileRelationships = relationships
                        self.tableView.reloadData()
                    }
                case .failure(let error):
                    self.showAlert(title: "Error", message: error.localizedDescription)
                }
            }
        )
    }
    
    @objc private func createRelationship() {
        
        guard let fromProfileID = fromProfileIDTextField.text,
              let toProfileID = toProfileIDTextField.text,
              let type = relationshipTypeTextField.text,
              fromProfileID.count != 0,
              toProfileID.count != 0,
              type.count != 0
        else {
            
            self.showAlert(
                title: "Hey!",
                message: "All text fields are required to be filled in for this interface!"
            )
            
            return
        }
        
        self.sdk.socialGraphClient.createProfileRelationship(
            options: CreateProfileRelationshipOptions(
                fromProfileID: fromProfileID,
                toProfileID: toProfileID,
                relationshipTypeKey: type.lowercased()
            )
        ) { result in
                
            switch result {
            case .success(let relationship):
                self.showAlert(
                    title: "Relationship Created",
                    message: "Relationship ID: \(relationship.id)"
                )
                    
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func deleteRelationship() {
        
        guard let relationshipID = relationshipIDTextField.text,
              relationshipID.count != 0
        else {
            
            self.showAlert(
                title: "Hey!",
                message: "Relationship ID text field needs to be filled in"
            )
            
            return
        }
        
        self.sdk.socialGraphClient.deleteProfileRelationship(
            options: DeleteProfileRelationshipOptions(profileRelationshipID: relationshipID)
        ) { result in
            switch result {
            case .success():
                self.showAlert(
                    title: "Relationship Deleted",
                    message: "Bye relationship"
                )
                        
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    func showAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: title,
                message: message,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showCopyAlert(rowData: ProfileRelationship) {
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: "Copy?",
                message: "What you like to copy to clipboard?",
                preferredStyle: .alert
            )
            alert.addAction(
                UIAlertAction(
                    title: "Relationship ID",
                    style: .default,
                    handler: { _ in
                        UIPasteboard.general.string = rowData.id
                    }
                ))
            
            alert.addAction(
                UIAlertAction(
                    title: "From Profile ID",
                    style: .default,
                    handler: { _ in
                        UIPasteboard.general.string = rowData.fromProfile.id
                    }
                ))
            
            alert.addAction(
                UIAlertAction(
                    title: "To Profile ID",
                    style: .default,
                    handler: { _ in
                        UIPasteboard.general.string = rowData.toProfile.id
                    }
                ))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension ProfileRelationshipUseCase: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profileRelationships.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        var cellText: String = ""
        let rowData = profileRelationships[indexPath.row]
        cellText = "ID: \(rowData.id)\nFrom Profile: \(rowData.fromProfile.id) \nTo Profile: \(rowData.toProfile.id)\nType: \(rowData.relationshipType.key)"

        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = cellText
        cell.textLabel?.font = UIFont.systemFont(ofSize: 12.0)
        return cell
    }
}

extension ProfileRelationshipUseCase: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowData = profileRelationships[indexPath.row]
        showCopyAlert(rowData: rowData)
    }
}

extension ProfileRelationshipUseCase: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
