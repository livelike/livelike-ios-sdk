//
//  TestCaseViewController.swift
//  LiveLikeDemoApp
//
//  Created by Mike Moloksher on 2/25/20.
//

import UIKit

class TestCaseViewController: UIViewController {
    var contentView = UIView()
    var vcTitle: String? {
        didSet {
            let label = UILabel(frame: .zero)
            label.text = vcTitle
            label.sizeToFit()
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: 18.0)
            label.adjustsFontSizeToFitWidth = true
            toolBarTitle = UIBarButtonItem(customView: label)
        }
    }

    var vcInfo: String?

    private let toolBar: UIToolbar = {
        let toolBar: UIToolbar = UIToolbar()
        toolBar.backgroundColor = .white
        toolBar.translatesAutoresizingMaskIntoConstraints = false
        return toolBar
    }()

    private var toolBarTitle = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

    init() {
        super.init(nibName: nil, bundle: nil)
        definesPresentationContext = true
        modalPresentationStyle = .overCurrentContext
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        var items = [UIBarButtonItem]()
        items.append(
            UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(closeViewController))
        )
        items.append(
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        )
        items.append(
            toolBarTitle
        )
        items.append(
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        )
        items.append(
            UIBarButtonItem(title: "ℹ️", style: .plain, target: nil, action: #selector(showViewInfo))
        )
        toolBar.setItems(items, animated: true)
        toolBar.tintColor = .red

        view.addSubview(toolBar)
        view.addSubview(contentView)

        contentView.translatesAutoresizingMaskIntoConstraints = false

        let constraints = [
            toolBar.topAnchor.constraint(equalTo: view.safeTopAnchor),
            toolBar.trailingAnchor.constraint(equalTo: view.safeTrailingAnchor),
            toolBar.leadingAnchor.constraint(equalTo: view.safeLeadingAnchor),
            toolBar.heightAnchor.constraint(equalToConstant: 44.0),
            contentView.topAnchor.constraint(equalTo: toolBar.bottomAnchor),
            contentView.trailingAnchor.constraint(equalTo: view.safeTrailingAnchor),
            contentView.leadingAnchor.constraint(equalTo: view.safeLeadingAnchor),
            contentView.bottomAnchor.constraint(equalTo: view.safeBottomAnchor)
        ]

        NSLayoutConstraint.activate(constraints)
    }

    @objc func closeViewController() {
        dismiss(animated: true, completion: nil)
    }

    @objc func showViewInfo() {
        let alert = UIAlertController(
            title: "Info",
            message: vcInfo ?? "No info added by developer, probably due to laziness.",
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))

        present(alert, animated: true, completion: nil)
    }
}
