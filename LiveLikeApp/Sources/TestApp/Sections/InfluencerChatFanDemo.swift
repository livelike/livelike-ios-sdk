//
//  InfluencerChatFanDemo.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 7/28/20.
//

import Foundation

import AVKit
import EngagementSDK
import UIKit

class InfluencerChatFanDemo: TestCaseViewController {
    private let contentSession: ContentSession

    private let messageViewController: MessageViewController = .init()
    private let widgetViewController: WidgetPopupViewController = .init()
    private let avPlayer: AVPlayerViewController = .init()

    init(contentSession: ContentSession) {
        self.contentSession = contentSession
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        messageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        avPlayer.view.translatesAutoresizingMaskIntoConstraints = false
        widgetViewController.view.translatesAutoresizingMaskIntoConstraints = false

        addChild(messageViewController)
        addChild(avPlayer)
        addChild(widgetViewController)

        contentView.addSubview(messageViewController.view)
        contentView.addSubview(avPlayer.view)
        contentView.addSubview(widgetViewController.view)

        NSLayoutConstraint.activate([
            avPlayer.view.topAnchor.constraint(equalTo: contentView.topAnchor),
            avPlayer.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            avPlayer.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            avPlayer.view.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.3),

            messageViewController.view.topAnchor.constraint(equalTo: avPlayer.view.bottomAnchor),
            messageViewController.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            messageViewController.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            messageViewController.view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

            widgetViewController.view.topAnchor.constraint(equalTo: avPlayer.view.bottomAnchor),
            widgetViewController.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetViewController.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            widgetViewController.view.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.4)
        ])

        // Play stream
        avPlayer.player = AVPlayer(url: URL(string: "https://cf-streams.livelikecdn.com/live/colorbars-angle1/index.m3u8")!)
        avPlayer.player?.play()

        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)

        messageViewController.setContentSession(contentSession)
        widgetViewController.session = contentSession
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc private func applicationWillEnterForeground() {
        avPlayer.player?.play()
    }
}
