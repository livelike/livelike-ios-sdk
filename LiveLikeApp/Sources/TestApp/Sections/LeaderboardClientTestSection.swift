//
//  LeaderboardClientTestSection.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 9/15/20.
//

import EngagementSDK
import UIKit

class LeaderboardClientTestSection: TestCaseViewController {
    let widgetViewController = WidgetPopupViewController()
    let widgetView = UIView()
    lazy var widgetCreateView = WidgetCreateView(
        programID: self.programID,
        apiToken: self.apiToken,
        timeoutSeconds: 10,
        apiOrigin: self.apiOrigin
    )

    private let placementScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()

    private let placementStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 0
        return stackView
    }()

    var session: ContentSession?

    let sdk: EngagementSDK
    let apiToken: String
    let programID: String
    let apiOrigin: URL

    init(
        sdk: EngagementSDK,
        apiToken: String,
        programID: String,
        apiOrigin: URL
    ) {
        self.sdk = sdk
        self.apiToken = apiToken
        self.programID = programID
        self.apiOrigin = apiOrigin
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "Widgets Test"
        vcInfo = "Use this view to test Widgets without having to use the produce site. This view replicates API calls that the produce site would be doing"

        super.viewDidLoad()

        view.backgroundColor = .darkGray

        widgetView.translatesAutoresizingMaskIntoConstraints = false
        widgetCreateView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(widgetView)
        contentView.addSubview(widgetCreateView)
        addChild(viewController: widgetViewController, into: widgetView)
        contentView.addSubview(placementScrollView)
        placementScrollView.addSubview(placementStackView)
        applyLayoutConstraints()
        let config = SessionConfiguration(programID: programID)
        session = sdk.contentSession(config: config)
        widgetViewController.session = session

        session?.getLeaderboardClients { result in
            switch result {
            case let .success(leaderboardClients):
                leaderboardClients.forEach {
                    let leaderboardView = LeaderboardPlacementView(leaderboardClient: $0)
                    self.placementStackView.addArrangedSubview(leaderboardView)
                }
            case let .failure(error):
                print(error)
            }
        }
    }

    func applyLayoutConstraints() {
        NSLayoutConstraint.activate([
            widgetView.topAnchor.constraint(equalTo: contentView.topAnchor),
            widgetView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.4),
            widgetView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            placementScrollView.topAnchor.constraint(equalTo: widgetView.bottomAnchor),
            placementScrollView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            placementScrollView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            placementScrollView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.3),

            placementStackView.topAnchor.constraint(equalTo: placementScrollView.topAnchor),
            placementStackView.leadingAnchor.constraint(equalTo: placementScrollView.leadingAnchor),
            placementStackView.trailingAnchor.constraint(equalTo: placementScrollView.trailingAnchor),
            placementStackView.bottomAnchor.constraint(equalTo: placementScrollView.bottomAnchor),
            placementStackView.widthAnchor.constraint(equalTo: placementScrollView.widthAnchor),

            widgetCreateView.topAnchor.constraint(equalTo: placementScrollView.bottomAnchor),
            widgetCreateView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetCreateView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            widgetCreateView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
}

private class LeaderboardPlacementView: UIView {
    private let leaderboardClient: LeaderboardClient

    private var leaderboardNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private var rankLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private var rankPercentileLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private var scoreLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        return stackView
    }()

    init(leaderboardClient: LeaderboardClient) {
        self.leaderboardClient = leaderboardClient
        super.init(frame: .zero)
        leaderboardNameLabel.text = leaderboardClient.name
        setupLayout()
        setPosition(leaderboardClient.currentPosition)
        self.leaderboardClient.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupLayout() {
        addSubview(stackView)

        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])

        stackView.addArrangedSubview(leaderboardNameLabel)
        stackView.addArrangedSubview(rankLabel)
        stackView.addArrangedSubview(rankPercentileLabel)
        stackView.addArrangedSubview(scoreLabel)
    }

    private func setPosition(_ position: LeaderboardPosition?) {
        rankLabel.text = "|Rank: \(position?.rank.description ?? "-")"
        rankPercentileLabel.text = "|%: \(position?.percentileRank.description ?? "-")"
        scoreLabel.text = "|Score: \(position?.score.description ?? "-")"
    }
}

extension LeaderboardPlacementView: LeaderboardDelegate {
    func leaderboard(_ leaderboardClient: LeaderboardClient, currentPositionDidChange position: LeaderboardPosition) {
        setPosition(position)
    }
}
