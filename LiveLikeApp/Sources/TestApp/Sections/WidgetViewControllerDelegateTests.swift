//
//  WidgetViewControllerDelegateTests.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 7/14/20.
//

import EngagementSDK
import UIKit

class WidgetViewControllerDelegateTests: TestCaseViewController {
    let widgetViewController = WidgetPopupViewController()
    let widgetView = UIView()
    lazy var widgetCreateView = WidgetCreateView(
        programID: self.programID,
        apiToken: self.apiToken,
        timeoutSeconds: self.timeoutSeconds,
        apiOrigin: self.apiOrigin
    )
    var session: ContentSession?

    let sdk: EngagementSDK
    let apiToken: String
    let programID: String
    let apiOrigin: URL
    private let timeoutSeconds: Int
    private var timeoutSecondsISO8601: String {
        return "P0DT00H00M\(timeoutSeconds)S"
    }

    private var widgetWillDisplayCalled: Bool = false
    private var widgetDidDisplayCalled: Bool = false
    private var widgetWillDismissCalled: Bool = false
    private var widgetDidDismissCalled: Bool = false

    init(
        sdk: EngagementSDK,
        apiToken: String,
        programID: String,
        timeoutSeconds: Int,
        apiOrigin: URL
    ) {
        self.sdk = sdk
        self.apiToken = apiToken
        self.programID = programID
        self.timeoutSeconds = timeoutSeconds
        self.apiOrigin = apiOrigin
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        vcTitle = "WidgetPopupViewController Delegate Call Order Test"
        vcInfo = "Tests that the delegate methods of the WidgetPopupViewController are getting called in order and without duplicates."

        super.viewDidLoad()

        view.backgroundColor = .darkGray

        widgetView.translatesAutoresizingMaskIntoConstraints = false
        widgetCreateView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(widgetView)
        contentView.addSubview(widgetCreateView)
        addChild(viewController: widgetViewController, into: widgetView)
        applyLayoutConstraints()
        let config = SessionConfiguration(programID: programID)
        session = sdk.contentSession(config: config)
        widgetViewController.session = session
        widgetViewController.delegate = self
    }

    func applyLayoutConstraints() {
        NSLayoutConstraint.activate([
            widgetView.topAnchor.constraint(equalTo: contentView.topAnchor),
            widgetView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.5),
            widgetView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            widgetCreateView.topAnchor.constraint(equalTo: widgetView.bottomAnchor),
            widgetCreateView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            widgetCreateView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            widgetCreateView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }

    deinit {
        log.dev("deinit")
    }

    private func resetTest() {
        widgetWillDisplayCalled = false
        widgetDidDisplayCalled = false
        widgetWillDismissCalled = false
        widgetDidDismissCalled = false
    }

    private func showErrorAlert(message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: "Error",
                message: message,
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    private func verifyDelegateCallOrder(
        widgetWillDisplay: Bool,
        widgetDidDisplay: Bool,
        widgetWillDismiss: Bool,
        widgetDidDismiss: Bool
    ) {
        guard widgetWillDisplayCalled == widgetWillDisplay else {
            showErrorAlert(message: "Test failed: expected widgetWillDisplayCalled to be \(widgetWillDisplay)")
            return
        }

        guard widgetDidDisplayCalled == widgetDidDisplay else {
            showErrorAlert(message: "Test failed: expected widgetDidDisplayCalled to be \(widgetDidDisplay)")
            return
        }

        guard widgetWillDismissCalled == widgetWillDismiss else {
            showErrorAlert(message: "Test failed: expected widgetWillDismissCalled to be \(widgetWillDismiss)")
            return
        }

        guard widgetDidDismissCalled == widgetDidDismiss else {
            showErrorAlert(message: "Test failed: expected widgetDidDismissCalled to be \(widgetDidDismiss)")
            return
        }

        return
    }
}

extension WidgetViewControllerDelegateTests: WidgetPopupViewControllerDelegate {
    func widgetViewController(_ widgetViewController: WidgetPopupViewController, willDisplay widget: Widget) {
        verifyDelegateCallOrder(widgetWillDisplay: false, widgetDidDisplay: false, widgetWillDismiss: false, widgetDidDismiss: false)
        widgetWillDisplayCalled = true
    }

    func widgetViewController(_ widgetViewController: WidgetPopupViewController, didDisplay widget: Widget) {
        verifyDelegateCallOrder(widgetWillDisplay: true, widgetDidDisplay: false, widgetWillDismiss: false, widgetDidDismiss: false)
        widgetDidDisplayCalled = true
    }

    func widgetViewController(_ widgetViewController: WidgetPopupViewController, willDismiss widget: Widget) {
        verifyDelegateCallOrder(widgetWillDisplay: true, widgetDidDisplay: true, widgetWillDismiss: false, widgetDidDismiss: false)
        widgetWillDismissCalled = true
    }

    func widgetViewController(_ widgetViewController: WidgetPopupViewController, didDismiss widget: Widget) {
        verifyDelegateCallOrder(widgetWillDisplay: true, widgetDidDisplay: true, widgetWillDismiss: true, widgetDidDismiss: false)
        widgetDidDismissCalled = true

        // After widget did dismiss we can reset the test for the next widget
        resetTest()
    }
}
