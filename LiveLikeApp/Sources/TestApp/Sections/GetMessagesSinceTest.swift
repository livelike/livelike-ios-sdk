//
//  GetMessagesSinceTest.swift
//  LiveLikeTestApp
//
//  Created by Jelzon Monzon on 9/23/20.
//

import EngagementSDK
import UIKit

class GetMessagesSinceTest: TestCaseViewController {
    private let sdk: EngagementSDK
    private let chatRoomID: String
    private var chatSession: ChatSession?

    private let chatViewController: ChatViewController = {
        let chatViewController = ChatViewController()
        chatViewController.view.translatesAutoresizingMaskIntoConstraints = false
        chatViewController.shouldDisplayDebugVideoTime = true
        return chatViewController
    }()

    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .equalCentering
        return stackView
    }()

    private let secondsTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.keyboardType = .decimalPad
        textField.borderStyle = .roundedRect
        textField.placeholder = "Enter Seconds"
        textField.textAlignment = .center
        return textField
    }()

    private let getMessageCountButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get Message Count Since Seconds", for: .normal)
        return button
    }()

    private let getMessagesButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Get Messages Since Seconds", for: .normal)
        return button
    }()

    init(sdk: EngagementSDK, chatRoomID: String) {
        self.sdk = sdk
        self.chatRoomID = chatRoomID
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        getMessageCountButton.addTarget(self, action: #selector(getMessageCountButtonSelected), for: .touchUpInside)
        getMessagesButton.addTarget(self, action: #selector(getMessagesButtonSelected), for: .touchUpInside)

        addChild(chatViewController)
        view.addSubview(stackView)
        view.addSubview(chatViewController.view)
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            stackView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.1),

            chatViewController.view.topAnchor.constraint(equalTo: stackView.bottomAnchor),
            chatViewController.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            chatViewController.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            chatViewController.view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)

        ])

        stackView.addArrangedSubview(secondsTextField)
        stackView.addArrangedSubview(getMessageCountButton)
        stackView.addArrangedSubview(getMessagesButton)

        sdk.connectChatRoom(config: ChatSessionConfig(roomID: chatRoomID)) { result in
            switch result {
            case let .success(chatSession):
                self.chatSession = chatSession
                self.chatViewController.setChatSession(chatSession)
            case let .failure(error):
                print(error)
            }
        }
    }

    @objc private func getMessageCountButtonSelected() {
        guard
            let secondsText = secondsTextField.text,
            let seconds = TimeInterval(secondsText)
        else {
            let alert = UIAlertController(
                title: "Error",
                message: "Enter a valid value for Seconds",
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }

        let timetoken = TimeToken(date: Date().addingTimeInterval(-seconds))

        if let session = chatSession {
            session.getMessageCount(since: timetoken) { result in
                DispatchQueue.main.async {
                    switch result {
                    case let .success(count):
                        let alert = UIAlertController(
                            title: "Message Count",
                            message: "Found \(count) messages since \(timetoken.approximateDate)",
                            preferredStyle: .alert
                        )
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    case let .failure(error):
                        let alert = UIAlertController(
                            title: "Error",
                            message: "\(error)",
                            preferredStyle: .alert
                        )
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }

    @objc private func getMessagesButtonSelected() {
        guard
            let secondsText = secondsTextField.text,
            let seconds = TimeInterval(secondsText)
        else {
            let alert = UIAlertController(
                title: "Error",
                message: "Enter a valid value for Seconds",
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }

        let sinceDate = Date().addingTimeInterval(-seconds)
        let timetoken = TimeToken(date: sinceDate)

        chatSession?.getMessages(since: timetoken) { result in
            DispatchQueue.main.async {
                switch result {
                case let .success(messages):
                    var messageString = "Message since \(sinceDate)\n\n"
                    messages.enumerated().forEach { index, message in
                        messageString += "\(index). \(message.createdAt)\n"
                    }

                    let alert = UIAlertController(
                        title: "Message Count",
                        message: messageString,
                        preferredStyle: .alert
                    )

                    print(messageString)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                case let .failure(error):
                    let alert = UIAlertController(
                        title: "Error",
                        message: "\(error)",
                        preferredStyle: .alert
                    )
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
}
